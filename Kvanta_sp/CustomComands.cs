﻿namespace K7_sp
{
    public enum CustomCommands
    {
        SendWebuserRegistrationEMail = 11,
        SendWebuserUpdateEMail = 12,

        RegisterWebUser = 71,
        RequestEmailConfirm = 72,
        ConfirmEmailAddress = 73,

        RequestPasswordReset = 74,
        ConfirmPasswordReset = 75,

        UpdateWebUserProfile = 76,
        ChangePassword = 78,
        RejectWebRequest = 87,

        ProcessBudjetDocHeader = 121,
        ProcessBudjetDocBody = 122,
        ProcessBudjetExecutionHeader = 123,
        ProcessBudjetExecutionBody = 124,
        WriteCommissionMembersRowBJ = 133,
        UpdateCommissionMembersRowBJ = 134,
        AcceptGroupJoiningRequest = 135,
        UpdateWebAccountPerson = 136,
        RequestForGroupJoining = 141,
        SetProposalAcceptStatus = 144,

        SaveWebSession = 145,
        AddContactPerson = 150,
        AddExtContactPerson = 151,
        SetInvitation = 154,
        GetInvitationInfo = 155,
        AcceptInvitation = 156
    }

    public enum ResultCodes
    {
        OK = 1,
        Error = 2,
        DuplicateOperation = 101,
        InactualOperation = 102,
        LimitHasExceeded = 103,
        IncorrectNumerOfRows = 104,
        IncorrectBaseTable = 105,
        IncorrectEmail = 106,
        IncorrectRequestCode = 107,
        PhoneNumberIsEmpty = 108,
        EmailIsEmpty = 109,
        SurnameIsEmpty = 110,
        NameIsEmpty = 111,
        PhoneNumberIsAlreadyRegistered = 112,
        EmailIsAlreadyRegistered = 113,
        BadRequest = 400,
        Unauthorized = 401
    }
}
