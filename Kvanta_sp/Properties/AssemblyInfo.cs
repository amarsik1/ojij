﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using appcore;

// Управление общими сведениями о сборке осуществляется с помощью 
// набора атрибутов. Измените значения этих атрибутов, чтобы изменить сведения,
// связанные со сборкой.
[assembly: AssemblyTitle("ATLAS-Kvanta: Бібліотека схем системи")]
[assembly: AssemblyDescription("-")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany(AppDefinition.DEVELOPER_COMPANY)]
[assembly: AssemblyProduct(AppDefinition.PRODUCT_NAME)]
[assembly: AssemblyCopyright("Copyright © АБСОЛЮТ СЕРВІС 2010-2012")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Параметр ComVisible со значением FALSE делает типы в сборке невидимыми 
// для COM-компонентов.  Если требуется обратиться к типу в этой сборке через 
// COM, задайте атрибуту ComVisible значение TRUE для этого типа.
[assembly: ComVisible(false)]

// Следующий GUID служит для идентификации библиотеки типов, если этот проект будет видимым для COM
[assembly: Guid("126B60B5-11F0-4658-98BB-45A02188F6F3")]

// Сведения о версии сборки состоят из следующих четырех значений:
//
//      Основной номер версии
//      Дополнительный номер версии 
//      Номер построения
//      Редакция
//
// Можно задать все значения или принять номер построения и номер редакции по умолчанию, 
// используя "*", как показано ниже:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion(AppDefinition.PRODUCT_VERSION)]
[assembly: AssemblyFileVersion(AppDefinition.PRODUCT_VERSION)]
