using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using appcore.DataProcessing;
using ADE.EntityModel;

namespace Entity_Schema.Test1 {

	public class Root : EntityBase<Root>
	{
		public static class S
		{
			[GProp("mdTable", "EduLessons")]
			[GProp("mdTableUserName", "��������� �����")]
			[GProp("mdTableKind", "2")]
			[GProp("mdTableGid", "15901")]
			public const string _EduLessons = "EduLessons";

			[GProp("mdTable", "EduStudents")]
			[GProp("mdTableUserName", "���� ���������� �����")]
			[GProp("mdTableKind", "2")]
			[GProp("mdTableGid", "15903")]
			public const string _EduStudents = "EduStudents";

			[GProp("mdTable", "EduTasksList")]
			[GProp("mdTableUserName", "���� �������")]
			[GProp("mdTableKind", "2")]
			[GProp("mdTableGid", "15932")]
			public const string _EduTasksList = "EduTasksList";

			public class EduLessons : EntityBase<EduLessons>
			{
				public static class S
				{
					[GProp("mdFieldGid", "34074")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "�������������")]
					[GProp("mdDataType", "int")]
					public const string _gid = "gid";

					[GProp("mdFieldGid", "34075")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "���� �����")]
					[GProp("mdDataType", "varchar(2000)")]
					[GProp("DataLength", "2000")]
					public const string _LessonTopic = "LessonTopic";

					[GProp("mdFieldGid", "34081")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "������")]
					[GProp("mdDataType", "Object_statuses_reference")]
					[GProp("mdReference", "Object_statuses")]
					public const string _object_status = "object_status";

					[GProp("mdFieldGid", "34076")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "��� �����")]
					[GProp("mdDataType", "datetime")]
					public const string _LessonTime = "LessonTime";

					[GProp("mdFieldGid", "34077")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "���������� ����")]
					[GProp("mdDataType", "EduClasses_reference")]
					[GProp("mdReference", "EduClasses")]
					public const string _Class = "Class";

					[GProp("mdFieldGid", "34078")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "����")]
					[GProp("mdDataType", "nvarchar(MAX)")]
					[GProp("DataLength", "0")]
					public const string _Description = "Description";

					[GProp("mdFieldGid", "34079")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "����� ����")]
					[GProp("mdDataType", "int")]
					public const string _version_no = "version_no";

					[GProp("mdFieldGid", "34080")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "��������")]
					[GProp("mdDataType", "varchar(MAX)")]
					[GProp("DataLength", "0")]
					public const string _comments = "comments";

					[GProp("mdFieldGid", "34082")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "�������� ��������")]
					[GProp("mdDataType", "Persons_reference")]
					[GProp("mdReference", "Persons")]
					public const string _ResponsibleTeacher = "ResponsibleTeacher";
				}

				int m_gid_;
				public int gid {
					get { return m_gid_; }
					set { SetPropValue(value, ref m_gid_, S._gid); }
				}

				string m_LessonTopic_;
				public string LessonTopic {
					get { return m_LessonTopic_; }
					set { SetPropValue(value, ref m_LessonTopic_, S._LessonTopic); }
				}

				int m_object_status_;
				public int object_status {
					get { return m_object_status_; }
					set { SetPropValue(value, ref m_object_status_, S._object_status); }
				}

				DateTime m_LessonTime_;
				public DateTime LessonTime {
					get { return m_LessonTime_; }
					set { SetPropValue(value, ref m_LessonTime_, S._LessonTime); }
				}

				int m_Class_;
				public int Class {
					get { return m_Class_; }
					set { SetPropValue(value, ref m_Class_, S._Class); }
				}

				string m_Description_;
				public string Description {
					get { return m_Description_; }
					set { SetPropValue(value, ref m_Description_, S._Description); }
				}

				int m_version_no_;
				public int version_no {
					get { return m_version_no_; }
					set { SetPropValue(value, ref m_version_no_, S._version_no); }
				}

				string m_comments_;
				public string comments {
					get { return m_comments_; }
					set { SetPropValue(value, ref m_comments_, S._comments); }
				}

				int m_ResponsibleTeacher_;
				public int ResponsibleTeacher {
					get { return m_ResponsibleTeacher_; }
					set { SetPropValue(value, ref m_ResponsibleTeacher_, S._ResponsibleTeacher); }
				}
			}

			public class EduStudents : EntityBase<EduStudents>
			{
				public static class S
				{
					[GProp("mdFieldGid", "34091")]
					[GProp("mdFieldPath", "Person.Full_Name")]
					[GProp("mdFieldKind", "4")]
					[GProp("mdFieldUserName", "�������, ��'�, ��-�������")]
					[GProp("mdDataType", "varchar(255)")]
					[GProp("DataLength", "255")]
					public const string _Full_Name = "Full_Name";

					[GProp("mdFieldGid", "34444")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "������")]
					[GProp("mdDataType", "Object_statuses_reference")]
					[GProp("mdReference", "Object_statuses")]
					public const string _object_status = "object_status";

					[GProp("mdFieldGid", "34089")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "����� ����")]
					[GProp("mdDataType", "Persons_reference")]
					[GProp("mdReference", "Persons")]
					public const string _Person = "Person";

					[GProp("mdFieldGid", "34090")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "���������� ����")]
					[GProp("mdDataType", "EduClasses_reference")]
					[GProp("mdReference", "EduClasses")]
					public const string _Class = "Class";

					[GProp("mdFieldGid", "34092")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "����� ����")]
					[GProp("mdDataType", "int")]
					public const string _version_no = "version_no";

					[GProp("mdFieldGid", "34093")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "��������")]
					[GProp("mdDataType", "varchar(255)")]
					[GProp("DataLength", "255")]
					public const string _comments = "comments";

					[GProp("mdFieldGid", "34445")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "�������������")]
					[GProp("mdDataType", "int")]
					public const string _gid = "gid";
				}

				string m_Full_Name_;
				public string Full_Name {
					get { return m_Full_Name_; }
					set { SetPropValue(value, ref m_Full_Name_, S._Full_Name); }
				}

				int m_object_status_;
				public int object_status {
					get { return m_object_status_; }
					set { SetPropValue(value, ref m_object_status_, S._object_status); }
				}

				int m_Person_;
				public int Person {
					get { return m_Person_; }
					set { SetPropValue(value, ref m_Person_, S._Person); }
				}

				int m_Class_;
				public int Class {
					get { return m_Class_; }
					set { SetPropValue(value, ref m_Class_, S._Class); }
				}

				int m_version_no_;
				public int version_no {
					get { return m_version_no_; }
					set { SetPropValue(value, ref m_version_no_, S._version_no); }
				}

				string m_comments_;
				public string comments {
					get { return m_comments_; }
					set { SetPropValue(value, ref m_comments_, S._comments); }
				}

				int m_gid_;
				public int gid {
					get { return m_gid_; }
					set { SetPropValue(value, ref m_gid_, S._gid); }
				}
			}

			public class EduTasksList : EntityBase<EduTasksList>
			{
				public static class S
				{
					[GProp("mdFieldGid", "34458")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "�����")]
					[GProp("mdDataType", "Persons_reference")]
					[GProp("mdReference", "Persons")]
					public const string _Author = "Author";

					[GProp("mdFieldGid", "34459")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "����")]
					[GProp("mdDataType", "EduLessons_reference")]
					[GProp("mdReference", "EduLessons")]
					public const string _Lesson = "Lesson";

					[GProp("mdFieldGid", "34460")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "���")]
					[GProp("mdDataType", "EduTasksListKinds_reference")]
					[GProp("mdReference", "EduTasksListKinds")]
					public const string _Type = "Type";

					[GProp("mdFieldGid", "34453")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "�������������")]
					[GProp("mdDataType", "int")]
					public const string _gid = "gid";

					[GProp("mdFieldGid", "34455")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "������")]
					[GProp("mdDataType", "Object_statuses_reference")]
					[GProp("mdReference", "Object_statuses")]
					public const string _object_status = "object_status";

					[GProp("mdFieldGid", "34454")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "����")]
					[GProp("mdDataType", "varchar(2000)")]
					[GProp("DataLength", "2000")]
					public const string _Topic = "Topic";

					[GProp("mdFieldGid", "34456")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "��� ���������")]
					[GProp("mdDataType", "datetime")]
					public const string _CreatingTime = "CreatingTime";

					[GProp("mdFieldGid", "34457")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "���������� ����")]
					[GProp("mdDataType", "EduClasses_reference")]
					[GProp("mdReference", "EduClasses")]
					public const string _Class = "Class";

					[GProp("mdTable", "EduTasksInTasksList")]
					[GProp("mdTableUserName", "������ ����������� �����")]
					[GProp("mdTableKind", "5")]
					[GProp("mdTableGid", "15902")]
					public const string _Tasks = "Tasks";

					public class Tasks : EntityBase<Tasks>
					{
						public static class S
						{
							[GProp("mdFieldGid", "34420")]
							[GProp("mdFieldKind", "0")]
							[GProp("mdFieldUserName", "�������������")]
							[GProp("mdDataType", "int")]
							public const string _gid = "gid";

							[GProp("mdFieldGid", "34083")]
							[GProp("mdFieldKind", "0")]
							[GProp("mdFieldUserName", "���������� �����")]
							[GProp("mdDataType", "int")]
							public const string _Ordinal = "Ordinal";

							[GProp("mdFieldGid", "34084")]
							[GProp("mdFieldKind", "0")]
							[GProp("mdFieldUserName", "������")]
							[GProp("mdDataType", "EduTasks_reference")]
							[GProp("mdReference", "EduTasks")]
							public const string _Task = "Task";

							[GProp("mdFieldGid", "34087")]
							[GProp("mdFieldKind", "0")]
							[GProp("mdFieldUserName", "��������")]
							[GProp("mdDataType", "varchar(MAX)")]
							[GProp("DataLength", "0")]
							public const string _comments = "comments";
						}

						int m_gid_;
						public int gid {
							get { return m_gid_; }
							set { SetPropValue(value, ref m_gid_, S._gid); }
						}

						int m_Ordinal_;
						public int Ordinal {
							get { return m_Ordinal_; }
							set { SetPropValue(value, ref m_Ordinal_, S._Ordinal); }
						}

						int m_Task_;
						public int Task {
							get { return m_Task_; }
							set { SetPropValue(value, ref m_Task_, S._Task); }
						}

						string m_comments_;
						public string comments {
							get { return m_comments_; }
							set { SetPropValue(value, ref m_comments_, S._comments); }
						}
					}
				}

				int m_Author_;
				public int Author {
					get { return m_Author_; }
					set { SetPropValue(value, ref m_Author_, S._Author); }
				}

				int m_Lesson_;
				public int Lesson {
					get { return m_Lesson_; }
					set { SetPropValue(value, ref m_Lesson_, S._Lesson); }
				}

				int m_Type_;
				public int Type {
					get { return m_Type_; }
					set { SetPropValue(value, ref m_Type_, S._Type); }
				}

				int m_gid_;
				public int gid {
					get { return m_gid_; }
					set { SetPropValue(value, ref m_gid_, S._gid); }
				}

				int m_object_status_;
				public int object_status {
					get { return m_object_status_; }
					set { SetPropValue(value, ref m_object_status_, S._object_status); }
				}

				string m_Topic_;
				public string Topic {
					get { return m_Topic_; }
					set { SetPropValue(value, ref m_Topic_, S._Topic); }
				}

				DateTime m_CreatingTime_;
				public DateTime CreatingTime {
					get { return m_CreatingTime_; }
					set { SetPropValue(value, ref m_CreatingTime_, S._CreatingTime); }
				}

				int m_Class_;
				public int Class {
					get { return m_Class_; }
					set { SetPropValue(value, ref m_Class_, S._Class); }
				}

				EntitiesCollection<S.Tasks> m_Tasks_;
				public EntitiesCollection<S.Tasks> Tasks {
					get { 
						 if (m_Tasks_ == null) {
							 lock(this) {
							 if (m_Tasks_ == null)
								 m_Tasks_ = new EntitiesCollection<S.Tasks>();
							 }
						 }
						 return m_Tasks_;
					 }
				}
			}
		}

		EntitiesCollection<S.EduLessons> m_EduLessons_;
		public EntitiesCollection<S.EduLessons> EduLessons {
			get { 
				 if (m_EduLessons_ == null) {
					 lock(this) {
					 if (m_EduLessons_ == null)
						 m_EduLessons_ = new EntitiesCollection<S.EduLessons>();
					 }
				 }
				 return m_EduLessons_;
			 }
		}

		EntitiesCollection<S.EduStudents> m_EduStudents_;
		public EntitiesCollection<S.EduStudents> EduStudents {
			get { 
				 if (m_EduStudents_ == null) {
					 lock(this) {
					 if (m_EduStudents_ == null)
						 m_EduStudents_ = new EntitiesCollection<S.EduStudents>();
					 }
				 }
				 return m_EduStudents_;
			 }
		}

		EntitiesCollection<S.EduTasksList> m_EduTasksList_;
		public EntitiesCollection<S.EduTasksList> EduTasksList {
			get { 
				 if (m_EduTasksList_ == null) {
					 lock(this) {
					 if (m_EduTasksList_ == null)
						 m_EduTasksList_ = new EntitiesCollection<S.EduTasksList>();
					 }
				 }
				 return m_EduTasksList_;
			 }
		}
	}
}

