﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using asb_ipc;
using MD_Schema;
using System.Globalization;
using appcore;
using appcore.DataProcessing;
using AS_MetaData;

namespace MR_sp.Classes
{
    public class QnT_Cache
    {
        private const string RespondentsParametersAuthCodeParamName = "AuthCode";
        public static QnT_Cache Instance { get; private set; }

        #region Static ValueTableSchemas && Models

        private static ValueTableSchema s_QGroupsSchema;
        private static int s_QItemsCount_FI;
        public static ValueTableSchema QGroupsSchema
        {
            get
            {
                if (s_QGroupsSchema != null) return s_QGroupsSchema;
                s_QGroupsSchema = new ValueTableSchema(QuestionGroups_TS_.__MD,
                    QuestionGroups_TS_.QuestionnaireTemplate,
                    QuestionGroups_TS_.gid,
                    QuestionGroups_TS_.name,
                    QuestionGroups_TS_.ordinal,
                    QuestionGroups_TS_.InterviewersComment,
                    QuestionGroups_TS_.RespondentsComment,
                    QuestionGroups_TS_.GroupCaption,
                    QuestionGroups_TS_.parent_group,
                    QuestionGroups_TS_.ViewStyle,
                    QuestionGroups_TS_.ViewTemplate,
                    QuestionGroups_TS_.AlternativesGroup,
                    QuestionGroups_TS_.QItemsCount);
                s_QItemsCount_FI = s_QGroupsSchema.IndexOf(QuestionGroups_TS_.QItemsCount);
                return s_QGroupsSchema;
            }
        }

        private static VRG_Tree.Model s_QGroupsTreeModel;
        private static VRG_Tree.Model QGroupsTreeModel
        {
            get
            {
                if (s_QGroupsTreeModel == null)
                {
                    s_QGroupsTreeModel = new VRG_Tree.Model(QuestionGroups_TS_.__MD, QuestionGroups_TS_.gid, QuestionGroups_TS_.parent_group);
                    s_QGroupsTreeModel.WeightField = QuestionGroups_TS_.QItemsCount;
                }
                return s_QGroupsTreeModel;
            }
        }

        private static ValueTableSchema s_QnTItemsSchema;
        public static ValueTableSchema QnTItemsSchema
        {
            get
            {
                return s_QnTItemsSchema ?? (s_QnTItemsSchema = new ValueTableSchema(QnT_Items_TS_.__MD,
                    QnT_Items_TS_.QuestionnaireTemplate,
                    QnT_Items_TS_.QuestionGroup,
                    QnT_Items_TS_.gid,
                    QnT_Items_TS_.ordinal,
                    QnT_Items_TS_.QuestionText,
                    QnT_Items_TS_.AlternativesGroup,
                    QnT_Items_TS_.AlternativesGroup2,
                    QnT_Items_TS_.InterviewersComment,
                    QnT_Items_TS_.RespondentsComment,
                    QnT_Items_TS_.IsOpenQuestion,
                    QnT_Items_TS_.AnswerRequiered,
                    QnT_Items_TS_.ItemCode,
                    QnT_Items_TS_.QuestionKind,
                    QnT_Items_TS_.QParams,
                    QnT_Items_TS_.QuestionDataType,
                    QnT_Items_TS_.AvailabilityCondition,
                    QnT_Items_TS_.AvConditionType));
            }
        }

        private static ValueTableSchema s_QKindSchema;
        public static ValueTableSchema QKindSchema
        {
            get
            {
                if (s_QKindSchema == null)
                {
                    s_QKindSchema = new ValueTableSchema(QuestionKinds_TS_.__MD, QuestionKinds_TS_.gid, QuestionKinds_TS_.name, QuestionKinds_TS_.ViewTemplate,
                        QuestionKinds_TS_.Code, QuestionKinds_TS_.AlternativesGroup);
                }
                return s_QKindSchema;
            }
        }
        private static ValueTableSchema s_questionnaireTemplateSchema;
        public static ValueTableSchema QuestionnaireTemplateSchema
        {
            get
            {
                return s_questionnaireTemplateSchema ?? (s_questionnaireTemplateSchema = new ValueTableSchema(
                    QuestionnaireTemplates_TS_.__MD,
                    QuestionnaireTemplates_TS_.gid,
                    QuestionnaireTemplates_TS_.name,
                    QuestionnaireTemplates_TS_.Description,
                    QuestionnaireTemplates_TS_.Project,
                    QuestionnaireTemplates_TS_.object_status,
                    QuestionnaireTemplates_TS_.StartDate,
                    QuestionnaireTemplates_TS_.EndDate,
                    QuestionnaireTemplates_TS_.HeaderHTML,
                    QuestionnaireTemplates_TS_.Start_text,
                    QuestionnaireTemplates_TS_.End_text,
                    QuestionnaireTemplates_TS_.refusal_text,
                    QuestionnaireTemplates_TS_.QOptions,
                    QuestionnaireTemplates_TS_.comments,
                    QuestionnaireTemplates_TS_.URLName));
            }
        }

        private static ValueTableSchema s_ConditionsSchema;
        public static ValueTableSchema ConditionsSchema
        {
            get
            {
                if (s_ConditionsSchema == null)
                {
                    s_ConditionsSchema = new ValueTableSchema(QnT_Conditions_TS_.__MD, QnT_Conditions_TS_.gid, QnT_Conditions_TS_.name, QnT_Conditions_TS_.expression,
                        QnT_Conditions_TS_.ParamsCount, QnT_Conditions_TS_.ordinal, QnT_Conditions_TS_.QuestionnaireTemplate);
                }
                return s_ConditionsSchema;
            }
        }

        private static ValueTableSchema s_PassConditionsSchema;
        public static ValueTableSchema PassConditionsSchema
        {
            get
            {
                if (s_PassConditionsSchema == null)
                {
                    s_PassConditionsSchema = new ValueTableSchema(QnT_PassConditions_TS_.__MD, QnT_PassConditions_TS_.QuestionnaireTemplate,
                        QnT_PassConditions_TS_.BaseQuestion, QnT_PassConditions_TS_.QnT_Condition,
                        QnT_PassConditions_TS_.NextQuestion, QnT_PassConditions_TS_.ordinal, QnT_PassConditions_TS_.RowSubKey, QnT_PassConditions_TS_.NextQGroup);
                }
                return s_PassConditionsSchema;
            }
        }

        private static ValueTableSchema s_ConditionsParamsSchema;
        public static ValueTableSchema ConditionsParamsSchema
        {
            get
            {
                if (s_ConditionsParamsSchema == null)
                {
                    s_ConditionsParamsSchema = new ValueTableSchema(QnT_ConditionsParams_TS_.__MD, QnT_ConditionsParams_TS_.QuestionnaireTemplate,
                        QnT_ConditionsParams_TS_.QnT_Condition, QnT_ConditionsParams_TS_.Ordinal, QnT_ConditionsParams_TS_.ParamValue, QnT_ConditionsParams_TS_.ParamValue_tid);
                }
                return s_ConditionsParamsSchema;
            }
        }

        private static ValueTableSchema s_RespondentsParamsSchema;
        public static ValueTableSchema RespondentsParamsSchema
        {
            get
            {
                if (s_RespondentsParamsSchema == null)
                    s_RespondentsParamsSchema = new ValueTableSchema(RespondentsParameters_TS_.__MD, RespondentsParameters_TS_.Project,
                        RespondentsParameters_TS_.Respondent, RespondentsParameters_TS_.ParamName, RespondentsParameters_TS_.ParamValue);

                return s_RespondentsParamsSchema;
            }
        }
        #endregion

        private IDataManager m_DM;
        private ValueTableSet m_DataCache;
        public CultureInfo Culture { get; private set; }

        private Dictionary<int, VRG_Tree> m_GroupTrees;

        // Indexes:
        private ValueTable.VTIntUniqueIndex m_QuestionnaireTemplatesIndex;
        private ValueTable.VTIntIndex m_GroupsIndex;
        private ValueTable.VTIntIndex m_QnItemsIndex;
        private ValueTable.VTIntIndex m_ConditionsIndex;
        private ValueTable.VTIntIndex m_PassRulesIndex;
        private ValueTable.VTIntIndex m_CndParamsIndex;

        private Dictionary<int, QnTemplateModel> m_QnTModels = new Dictionary<int, QnTemplateModel>();

        public static Action<int> onQnTemplateReset { get; set; }


        public QnT_Cache(IDataManager pDM, ValueTableSet pDataCache, CultureInfo pCulture)
        {
            m_DM = pDM;
            m_DataCache = pDataCache;
            Culture = pCulture;

            m_GroupTrees = new Dictionary<int, VRG_Tree>();
            // m_ItemInitor = pItemInitor;
            if (Instance == null)
                Instance = this;

            m_QuestionnaireTemplatesIndex = m_DataCache.GetAutoRefreshingIndex(QuestionnaireTemplateSchema, QuestionnaireTemplates_TS_.gid) as ValueTable.VTIntUniqueIndex;
            m_QuestionnaireTemplatesIndex?.AddDBUpdateCallBack(onQGroupUpdated);
            m_ConditionsIndex = m_DataCache.GetAutoRefreshingIndex(ConditionsSchema, QnT_Conditions_TS_.QuestionnaireTemplate) as ValueTable.VTIntIndex;
            m_ConditionsIndex?.AddDBUpdateCallBack(onQuestionnareConditionsUpdated);

            m_PassRulesIndex = m_DataCache.GetAutoRefreshingIndex(PassConditionsSchema, QnT_PassConditions_TS_.QuestionnaireTemplate) as ValueTable.VTIntIndex;
            m_PassRulesIndex?.AddDBUpdateCallBack(onQuestionnareConditionsUpdated);
            m_CndParamsIndex = m_DataCache.GetAutoRefreshingIndex(ConditionsParamsSchema, QnT_ConditionsParams_TS_.QuestionnaireTemplate) as ValueTable.VTIntIndex;
            m_CndParamsIndex?.AddDBUpdateCallBack(onQuestionnareConditionsUpdated);

            m_GroupsIndex = m_DataCache.GetAutoRefreshingIndex(QGroupsSchema, QuestionGroups_TS_.QuestionnaireTemplate) as ValueTable.VTIntIndex;
            m_GroupsIndex?.AddDBUpdateCallBack(onQGroupUpdated);

            m_QnItemsIndex = m_DataCache.GetAutoRefreshingIndex(QnTItemsSchema, QnT_Items_TS_.QuestionnaireTemplate) as ValueTable.VTIntIndex;
            m_QnItemsIndex?.AddDBUpdateCallBack(onQItemUpdated);
        }

        public QnTemplateModel GetQnTemplate(int pID)
        {
            QnTemplateModel lRes;
            if (m_QnTModels.TryGetValue(pID, out lRes) && lRes != null)
            {
                if (!lRes.IsActual)
                    lRes.refreshData();
            }
            else
            {
                var lVTR = GetQuestionnaireTemplateVTR(pID);
                if (lVTR == null) return null;
                lRes = new QnTemplateModel(this, lVTR);
                m_QnTModels[pID] = lRes;
            }
            return lRes;
        }

        public ValueTableRow GetQuestionnaireTemplateVTR(int pID)
        {
            return m_QuestionnaireTemplatesIndex?.GetRowByKey(pID);
        }

        private void onQuestionnareConditionsUpdated(ValueTable pVT)
        {
            if (pVT == null) return;
            foreach (var r in pVT)
            {
                int lQtID = r.GetInt(0);
                QnTemplateModel lModel;
                if (m_QnTModels.TryGetValue(lQtID, out lModel))
                    lModel?.resetConditions();
            }
        }
        private void onQGroupUpdated(ValueTable pData)
        {
            if (pData == null) return;
            foreach (var r in pData)
            {
                var id = r.GetInt(0);
                QnTemplateModel lRes;
                if (m_QnTModels.TryGetValue(id, out lRes))
                    lRes?.resetGroups();
            }
        }
        public ValueTableRow GetConditionVTR(int pID)
        {
            var lItemsIndex = m_DataCache.GetAutoRefreshingIndex(ConditionsSchema, QnT_Conditions_TS_.gid);
            return m_ConditionsIndex.GetRowByKey(pID);
        }
        private void onQItemUpdated(ValueTable pData)
        {
            if (pData != null)
            {
                foreach (var r in pData)
                {
                    var id = r.GetInt(0);
                    QnTemplateModel lRes;
                    if (m_QnTModels.TryGetValue(id, out lRes) && lRes != null)
                        lRes.resetQuestions();
                }
            }
        }

        public ValueTableRow[] GetQuestionnaireConditions(int pID)
        {
            var lItemsIndex = m_DataCache.GetAutoRefreshingIndex(ConditionsSchema, QnT_Conditions_TS_.QuestionnaireTemplate);
            return m_QuestionnaireTemplatesIndex?.GetRowsByKey(pID);
        }

        public ValueTableRow GetQuestionItemVTR(int pID)
        {
            var lIndex = m_DataCache.GetIndex(QnTItemsSchema, QnT_Items_TS_.gid);
            return lIndex.GetRowByKey(pID);
        }

        public string GetQHTML(int pQKindID)
        {
            var lQTIndex = m_DataCache.GetAutoRefreshingIndex(QKindSchema, QuestionKinds_TS_.gid);
            var lRow = lQTIndex.GetRowByKey(pQKindID);
            return lRow != null ? lRow.GetString(QuestionKinds_TS_.ViewTemplate) : string.Empty;
        }
        public bool Inited { get { return m_DM != null && !m_DM.IsDisposed; } }

        public ValueTableRow GetQGroupVTR(int pQnTemplateID, int pQGroupID)
        {
            var m = GetQnTemplate(pQnTemplateID);
            return m?.GetGroupVTRByID(pQGroupID);
        }

        public class QnTemplateModel
        {
            private QnT_Cache m_Root;
            public int ID { get; private set; }
            public string Name { get; private set; }
            public QnT_Cache QnTCache { get { return m_Root; } }

            public bool NoMenu { get; private set; }

            private ValueTableRow m_HeaderVTR;
            private VRG_Tree m_GroupsTree = null;
            public CultureInfo Culture { get { return m_Root.Culture; } }

            private ValueTable m_GroupsVT;
            private int m_QItemsCount;
            private int m_QGCount;
            private ValueTable m_QntItemsVT;

            private Dictionary<int, QnT_Model.QGroup> m_GroupModels = new Dictionary<int, QnT_Model.QGroup>();
            private Dictionary<int, QnT_Model.QnT_Item> m_ItemsMap = new Dictionary<int, QnT_Model.QnT_Item>();
            private Dictionary<int, ExpressionController<DataContainer>> m_ConditionControllers = new Dictionary<int, ExpressionController<DataContainer>>();
            private Dictionary<string, string> m_Options = new Dictionary<string, string>();

            private ValueTable m_ConditionsVT;
            private ValueTable m_PassRulesVT;
            private ValueTable m_ConditionsParamsVT;
            private ValueTable.VTIntUniqueIndex m_GroupsKey = null;
            private ValueTable.VTIntUniqueIndex m_ItemsKey = null;
            private ValueTable.VTIntUniqueIndex m_ConditionsKey = null;
            private ValueTable.VTIndex m_ConditionsByNameIndex = null;
            private ValueTable.VTIntIndex m_ItemsByGroupIndex = null;
            private ValueTable.VTIndex m_ItemsByCodeIndex = null;
            private ValueTable.VTIntIndex m_PassRulesByQuestionIndex = null;
            private ValueTable.VTIndex m_ParamsIndex = null;
            private ValueTable.VTIntIndex m_ParamsByConditionIndex = null;

            private bool m_GroupsActual = false;
            private bool m_QuestionsActual = false;
            private bool m_ConditionsActual = false;
            private bool m_ProcessActual = false;
            private bool m_IsActual = false;
            private object m_LockObj = new object();

            public int QItemsCount { get { return m_QItemsCount; } }
            public int QGCount { get { return m_QGCount; } }
            public bool IsActual { get { return m_IsActual; } }
            public int ProjectID { get { return m_HeaderVTR.GetInt(QuestionnaireTemplates_TS_.Project); } }
            public string UrlName { get { return m_HeaderVTR.GetString(QuestionnaireTemplates_TS_.URLName); } }
            public string HeaderHTML { get { return m_HeaderVTR.GetString(QuestionnaireTemplates_TS_.HeaderHTML); } }
            public string StartText { get { return m_HeaderVTR.GetString(QuestionnaireTemplates_TS_.Start_text); } }
            public string EndText { get { return m_HeaderVTR.GetString(QuestionnaireTemplates_TS_.End_text); } }
            public Dictionary<string, string> RParamsSetMap { get; private set; } = null;
            public string FinalRedirectOption { get; private set; } = null;

            private Dictionary<string, int> m_AuthCodesMap = new Dictionary<string, int>();
            private bool m_AuthInfoInited = false;

            public string GetOption(string pOptionName)
            {
                string lRes = null;
                if (m_Options.TryGetValue(pOptionName, out lRes))
                    return lRes;
                return null;
            }
            public bool ContainsOption(string pOptionName)
            {
                return m_Options.ContainsKey(pOptionName);
            }

            private void updateGroups()
            {
                lock (m_LockObj)
                {
                    if (m_GroupsActual && m_GroupsTree != null)
                        return;

                    var lRows = m_Root.m_GroupsIndex.GetRowsByKey(ID);
                    if (lRows != null)
                    {
                        int lQCount = 0;
                        int lQGCount = 0;
                        var lVT = new ValueTable(QGroupsSchema);
                        foreach (var r in lRows)
                        {
                            var lQC = r.GetInt(s_QItemsCount_FI);
                            lQCount += lQC;
                            if (lQC > 0)
                                lQGCount++;
                            lVT.AddRow(r);
                        }
                        m_GroupsVT = lVT;
                        m_QItemsCount = lQCount;
                        m_QGCount = lQGCount;

                        m_GroupsTree = new VRG_Tree(lVT, QGroupsTreeModel);
                        m_GroupsActual = true;
                        m_GroupsKey = m_GroupsVT.SetPrimaryKey(QuestionGroups_TS_.gid) as ValueTable.VTIntUniqueIndex;
                        m_GroupModels.Clear();
                    }
                    else
                    {
                        m_GroupsKey = null;
                        m_GroupsVT = null;
                        m_GroupsTree = null;
                    }
                }
            }
            private void updateQuestions()
            {
                var lRows = m_Root.m_QnItemsIndex.GetRowsByKey(ID);
                if (lRows != null)
                {
                    var lVT = new ValueTable(QnTItemsSchema);
                    foreach (var r in lRows)
                        lVT.AddRow(r);

                    m_QntItemsVT = lVT;
                    m_QuestionsActual = true;
                    m_ItemsKey = m_QntItemsVT.GetIntUniqueIndex(QnT_Items_TS_.gid);
                    m_ItemsByGroupIndex = m_QntItemsVT.GetIntIndex(QnT_Items_TS_.QuestionGroup);
                    m_ItemsByCodeIndex = m_QntItemsVT.GetIndex(QnT_Items_TS_.ItemCode);
                    m_ItemsMap.Clear();
                    m_GroupModels.Clear();
                }
            }
            public void updateConditions()
            {
                m_ProcessActual = true;
                var lRows = m_Root.m_ConditionsIndex.GetRowsByKey(ID);
                if (lRows != null)
                {
                    var lVT = new ValueTable(ConditionsSchema);
                    foreach (var r in lRows)
                        lVT.AddRow(r);

                    m_ConditionsVT = lVT;
                    m_ConditionsKey = m_ConditionsVT.GetIntUniqueIndex(QnT_Conditions_TS_.gid);
                    m_ConditionsByNameIndex = m_ConditionsVT.GetIndex(QnT_Conditions_TS_.name);
                }
                else
                {
                    m_ConditionsByNameIndex = null;
                    m_ConditionsKey = null;
                }

                lRows = m_Root.m_CndParamsIndex.GetRowsByKey(ID);
                if (lRows != null)
                {
                    var lVT = new ValueTable(ConditionsParamsSchema);
                    foreach (var r in lRows)
                        lVT.AddRow(r);

                    m_ConditionsParamsVT = lVT;
                    m_ParamsIndex = m_ConditionsParamsVT.GetIndex(QnT_ConditionsParams_TS_.ParamValue, QnT_ConditionsParams_TS_.ParamValue_tid);
                    m_ParamsByConditionIndex = m_ConditionsParamsVT.GetIntIndex(QnT_ConditionsParams_TS_.QnT_Condition);
                }

                lRows = m_Root.m_PassRulesIndex.GetRowsByKey(ID);
                if (lRows != null)
                {
                    var lVT = new ValueTable(PassConditionsSchema);
                    foreach (var r in lRows)
                        lVT.AddRow(r);

                    m_PassRulesVT = lVT;
                    m_PassRulesByQuestionIndex = m_PassRulesVT.GetIntIndex(QnT_PassConditions_TS_.BaseQuestion);
                }
                if (m_ProcessActual)
                    m_ConditionsActual = true;
                m_ConditionControllers.Clear();
                m_ProcessActual = false;
            }

            public ValueTableRow GetGroupVTRByID(int pGroupID)
            {
                if (!m_GroupsActual)
                    updateGroups();
                if (m_GroupsKey == null)
                    return null;
                return m_GroupsKey.GetRowByKey(pGroupID);
            }

            public ValueTableRow[] GetRespondentsParams(int pRespondentID)
            {
                if (pRespondentID == 0)
                    return null;

                var lIndex = m_Root.m_DataCache.GetAutoRefreshingIndex(RespondentsParamsSchema, RespondentsParameters_TS_.Project, RespondentsParameters_TS_.Respondent);
                return lIndex.GetRowsByKey(ProjectID, pRespondentID);
            }
            private void initAuthInfo()
            {
                lock (m_AuthCodesMap)
                {
                    if (m_AuthInfoInited) return;
                    var dm = m_Root.m_DM;
                    if (dm == null || !dm.IsAuthorized)
                    {
                        dm = asb_ipc.DataManagment.DMHelper.DataManager;
                        if (dm == null || !dm.IsAuthorized)
                            return;
                    }
                    var lVT = dm.ReadValueTable(new ValueTableSchema(RespondentsParameters_TS_.__MD,
                        RespondentsParameters_TS_.Respondent,
                        RespondentsParameters_TS_.ParamValue,
                        RespondentsParameters_TS_.Respondent_status_),
                        RespondentsParameters_TS_.__MD.NewFilter(RespondentsParameters_TS_.Project, ProjectID,
                        RespondentsParameters_TS_.ParamName, RespondentsParametersAuthCodeParamName, dm));
                    if (lVT != null)
                        foreach (var r in lVT)
                        {
                            if (m_AuthCodesMap.ContainsKey(r.GetString(1)))
                                dm.logEvent(SysEventTypes.ErrorOnClient, "dublicate auth code:" + r.GetString(1), null, null);
                            else if (r.GetByte(2) <= Metadata.MaxActiveStatus)
                                m_AuthCodesMap[r.GetString(1)] = r.GetInt(0);
                        }
                    m_AuthInfoInited = true;
                }
            }
            public int GetRespondentIDbyAuthCode(string pAuthCode)
            {
                int res;
                if (!m_AuthInfoInited)
                    initAuthInfo();
                m_AuthCodesMap.TryGetValue(pAuthCode, out res);
                return res;
            }

            public ValueTableRow[] GetItemsByGroup(int pGroupID)
            {
                if (!m_QuestionsActual)
                    updateQuestions();
                if (m_ItemsByGroupIndex == null)
                    return null;
                return m_ItemsByGroupIndex.GetRowsByKey(pGroupID);
            }
            public ValueTableRow GetItemByID(int pItemID)
            {
                if (!m_QuestionsActual)
                    updateQuestions();
                if (m_ItemsKey == null)
                    return null;
                return m_ItemsKey.GetRowByKey(pItemID);
            }
            public QnT_Model.QnT_Item GetQnItemByID(int pItemID)
            {
                QnT_Model.QnT_Item lRes = null;
                m_ItemsMap.TryGetValue(pItemID, out lRes);
                return lRes;
            }
            public ValueTableRow GetItemByCode(string pItemCode)
            {
                if (!m_QuestionsActual)
                    updateQuestions();
                if (m_ItemsByCodeIndex == null)
                    return null;
                return m_ItemsByCodeIndex.GetRowByKey(pItemCode);
            }
            public ValueTableRow GetConditionVTRByID(int pID)
            {
                if (!m_ConditionsActual)
                    updateConditions();
                if (m_ConditionsKey == null)
                    return null;
                return m_ConditionsKey.GetRowByKey(pID);
            }
            public ValueTableRow GetConditionVTRByName(string pName)
            {
                if (!m_ConditionsActual)
                    updateConditions();
                return m_ConditionsByNameIndex?.GetRowByKey(pName);
            }
            public ValueTableRow[] GetCndParams(int pConditionID)
            {
                if (!m_ConditionsActual)
                    updateConditions();
                if (m_ParamsByConditionIndex == null)
                    return null;
                return m_ParamsByConditionIndex.GetRowsByKey(pConditionID);
            }
            public ValueTableRow[] GetPassRulesVTRsByQuestion(int pID)
            {
                if (!m_ConditionsActual)
                    updateConditions();
                if (m_PassRulesByQuestionIndex == null)
                    return null;
                return m_PassRulesByQuestionIndex.GetRowsByKey(pID);
            }

            public VRG_Tree GroupsTree
            {
                get
                {
                    if (!m_GroupsActual || m_GroupsTree == null)
                    {
                        updateGroups();
                    }
                    return m_GroupsTree;
                }
            }

            public string Comment { get; private set; }

            public List<QnT_Model.QnT_Item> GetQuestionItems(int pQGroupID)
            {
                var lItemVTRs = GetItemsByGroup(pQGroupID);

                var lRes = new List<QnT_Model.QnT_Item>();
                foreach (var r in lItemVTRs)
                {
                    int lID = r.GetInt(QnT_Items_TS_.gid);
                    var lItem = new QnT_Model.QnT_Item(lID)
                    {
                        QText = r.GetString(QnT_Items_TS_.QuestionText),
                        QType = r.GetByte(QnT_Items_TS_.QuestionDataType),
                        Ordinal = r.GetInt(QnT_Items_TS_.ordinal),
                        IVComment = r.GetString(QnT_Items_TS_.InterviewersComment),
                        RComment = r.GetString(QnT_Items_TS_.RespondentsComment),
                        QParams = r.GetString(QnT_Items_TS_.QParams),
                        IsOpen = r.GetByte(QnT_Items_TS_.IsOpenQuestion),
                        Required = r.GetByte(QnT_Items_TS_.AnswerRequiered),
                        QCode = r.GetString(QnT_Items_TS_.ItemCode),
                        ATNGroupID = r.GetInt(QnT_Items_TS_.AlternativesGroup),
                        ATNGroup2ID = r.GetInt(QnT_Items_TS_.AlternativesGroup2),
                        AvCndID = r.GetInt(QnT_Items_TS_.AvailabilityCondition),
                        CndType = (byte)((m_PassRulesByQuestionIndex != null && m_PassRulesByQuestionIndex.ContainsKey(lID) ? 1 : 0)
                        + (r.GetByte(QnT_Items_TS_.AvConditionType) == _DataTypes_SD_.EvalResultTypes_enum_.IntArray ? 4 : 0))
                    };
                    lRes.Add(lItem);
                    m_ItemsMap[lID] = lItem;
                }

                int lParamValue_tid_FI = ConditionsParamsSchema.IndexOf(QnT_ConditionsParams_TS_.ParamValue_tid);
                int lParamValue_FI = ConditionsParamsSchema.IndexOf(QnT_ConditionsParams_TS_.ParamValue);
                //int QnT_ConditionsParams_TS_.ParamValue, QnT_ConditionsParams_TS_.ParamValue_tid
                foreach (var lItem in lRes)
                {
                    if (lItem.AvCndID > 0)
                    {
                        bool lIsInnerDependent = false;
                        var lParamRows = GetCndParams(lItem.AvCndID);
                        foreach (var r in lParamRows)
                        {
                            if (r.GetInt(lParamValue_tid_FI) == QnT_Items_TS_.__tGid)
                            {
                                var lItemVTR = m_ItemsKey.GetRowByKey(r.GetInt(lParamValue_FI));
                                if (lItemVTR != null && lItemVTR.GetInt(QnT_Items_TS_.QuestionGroup) == pQGroupID)
                                {
                                    lIsInnerDependent = true;
                                    break;
                                }
                            }
                        }
                        if (lIsInnerDependent)
                            lItem.CndType += 2;
                    }
                }

                return lRes;
            }

            /*public ExpressionController GetExpressionController(int pID)
            {
                ExpressionController lRes;
                if (m_ConditionControllers.TryGetValue(pID, out lRes) && lRes != null) return lRes;
                var lConditionVTR = GetConditionVTRByID(pID);
                var lExpressionString = lConditionVTR?.GetString(QnT_Conditions_TS_.expression);
                if (string.IsNullOrEmpty(lExpressionString)) return null;
                lRes = new ExpressionController(lExpressionString);
                m_ConditionControllers[pID] = lRes;
                return lRes;
            }*/
            public ExpressionController<DataContainer> GetExpressionController(int pID)
            {
                ExpressionController<DataContainer> lRes;
                if (m_ConditionControllers.TryGetValue(pID, out lRes) && lRes != null) return lRes;
                var lConditionVTR = GetConditionVTRByID(pID);
                var lExpressionString = lConditionVTR?.GetString(QnT_Conditions_TS_.expression);
                if (string.IsNullOrEmpty(lExpressionString)) return null;
                lRes = new ExpressionController<DataContainer>(lExpressionString, ASEvaluationsContext.Instance);
                m_ConditionControllers[pID] = lRes;
                return lRes;
            }
            public ExpressionController<DataContainer> GetExpressionController(string pName)
            {
                var lConditionVTR = GetConditionVTRByName(pName);
                if (lConditionVTR == null)
                    return null;
                int lId = lConditionVTR.GetInt(QnT_Conditions_TS_.gid);
                ExpressionController<DataContainer> lRes;
                if (m_ConditionControllers.TryGetValue(lId, out lRes) && lRes != null) return lRes;

                var lExpressionString = lConditionVTR.GetString(QnT_Conditions_TS_.expression);
                if (string.IsNullOrEmpty(lExpressionString)) return null;
                lRes = new ExpressionController<DataContainer>(lExpressionString, ASEvaluationsContext.Instance);
                m_ConditionControllers[lId] = lRes;
                return lRes;
            }

            private void onReset()
            {
                if (m_IsActual)
                {
                    m_IsActual = false;
                    onQnTemplateReset?.Invoke(ID);
                }
            }

            internal void resetConditions()
            {
                m_ConditionsActual = false;
                m_ProcessActual = false;

                onReset();
                m_ConditionControllers.Clear();
            }
            internal void resetGroups()
            {
                m_GroupsActual = false;
                onReset();
                m_ProcessActual = false;
            }
            internal void resetQuestions()
            {
                m_QuestionsActual = false;
                onReset();
                m_ProcessActual = false;
            }

            private void setOptions()
            {
                Dictionary<string, string> lRparamsSetMap = null;
                bool lNoMenu = false;
                string lFinalRdrOption = null;

                var lOptions = m_HeaderVTR.GetString(QuestionnaireTemplates_TS_.QOptions);
                var lParts = TextFormatter.SplitList(lOptions, ';');
                var lRparamsSetOption = lParts.FirstOrDefault(x => x.StartsWith("SetRParams"));
                if (!string.IsNullOrEmpty(lRparamsSetOption))
                {
                    var lOptionBody = lRparamsSetOption.Substring("SetRParams".Length).Trim();
                    if (!string.IsNullOrEmpty(lOptionBody))
                    {
                        if (lOptionBody.StartsWith("="))
                        {
                            lOptionBody = lOptionBody.Substring(1).Trim();
                            var lOptionsParts = lOptionBody.Split(',');
                            lRparamsSetMap = new Dictionary<string, string>();
                            foreach (var lOpt in lOptionsParts)
                            {
                                var lSplitPos = lOpt.IndexOf(':');
                                string lOpName = string.Empty;
                                string lOpVal = string.Empty;
                                if (lSplitPos >= 0)
                                {
                                    lOpName = lOpt.Substring(0, lSplitPos).Trim();
                                    lOpVal = lOpt.Substring(lSplitPos + 1).Trim();
                                }
                                else
                                    lOpName = lOpt;
                                lRparamsSetMap[lOpName] = lOpVal;
                            }
                        }
                    }
                }

                m_Options.Clear();
                foreach (var lOption in lParts)
                {
                    var lIndex = lOption.IndexOf('=');
                    if (lIndex > 0)
                    {
                        m_Options[lOption.Substring(0, lIndex).Trim()] = lOption.Substring(lIndex + 1).Trim();
                    }
                    else
                        m_Options[lOption] = string.Empty;
                }
                lNoMenu = lParts.Any(x => x == "#NoMenu#" || x == "NoMenu") || m_HeaderVTR.GetString(QuestionnaireTemplates_TS_.refusal_text) == "#NoMenu#";
                var lFinalRedirectOption = lParts.FirstOrDefault(x => x.StartsWith("FinalRedirect"));
                if (!string.IsNullOrEmpty(lFinalRedirectOption))
                {
                    var lOptionBody = lFinalRedirectOption.Substring("FinalRedirect".Length).Trim();
                    if (lOptionBody.StartsWith(":") || lOptionBody.StartsWith("="))
                        lFinalRdrOption = lOptionBody.Substring(1).Trim();
                }
                NoMenu = lNoMenu;
                RParamsSetMap = lRparamsSetMap;
                FinalRedirectOption = lFinalRdrOption;
            }

            internal QnTemplateModel(QnT_Cache pRoot, ValueTableRow pHeaderVTR)
            {
                m_Root = pRoot;
                ID = pHeaderVTR.GetInt(QuestionnaireTemplates_TS_.gid);
                Name = pHeaderVTR.GetString(QuestionnaireTemplates_TS_.name);
                Comment = pHeaderVTR.GetString(QuestionnaireTemplates_TS_.comments);
                m_HeaderVTR = pHeaderVTR;
                setOptions();
            }

            internal void refreshData()
            {
                m_AuthInfoInited = false;
                m_IsActual = true;
                var lHR = m_Root.GetQuestionnaireTemplateVTR(ID);
                if (lHR != null)
                {
                    m_AuthInfoInited = false;
                    m_AuthCodesMap.Clear();
                    m_HeaderVTR = lHR;
                    setOptions();
                }
                if (!m_GroupsActual)
                    updateGroups();
                if (!m_QuestionsActual)
                    updateQuestions();
                if (!m_ConditionsActual)
                    updateConditions();
            }

            public QnT_Model.QGroup GetQGroup(int pGroupID)
            {
                if (!m_IsActual)
                    refreshData();

                QnT_Model.QGroup lModel;
                if (m_GroupModels.TryGetValue(pGroupID, out lModel) && lModel != null)
                    return lModel;

                var lQGroup = GetGroupVTRByID(pGroupID);
                if (lQGroup == null)
                    return null;
                lModel = new QnT_Model.QGroup(
                    lQGroup.GetInt(QuestionGroups_TS_.gid), ID,
                    lQGroup.GetString(QuestionGroups_TS_.name),
                    lQGroup.GetInt(QuestionGroups_TS_.parent_group))
                {
                    AltGroupID = lQGroup.GetInt(QuestionGroups_TS_.AlternativesGroup),
                    IVComment = lQGroup.GetString(QuestionGroups_TS_.InterviewersComment),
                    RComment = lQGroup.GetString(QuestionGroups_TS_.RespondentsComment),
                    ViewStyle = lQGroup.GetString(QuestionGroups_TS_.ViewStyle),
                    Caption = lQGroup.GetString(QuestionGroups_TS_.GroupCaption)
                };

                var lItems = GetQuestionItems(pGroupID);
                lModel.QItems.AddRange(lItems);
                m_GroupModels[lModel.ID] = lModel;
                return lModel;
            }
        }

        public bool RemoveTemplateById(int pTemplateId)
        {
            return m_QnTModels.Remove(pTemplateId);
        }
    }
}
