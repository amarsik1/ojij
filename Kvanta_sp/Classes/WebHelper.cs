﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace K7_sp.Classes
{
    public static class WebHelper
    {
        public static string BuildRequestCode(int pWebAccountID, string pPrefix, int pDataItemID = 0)
        {
            if (pWebAccountID <= 0)
                throw new ArgumentException("Bad account ID");
            if (pPrefix == null || pPrefix.Length != 2)
                throw new ArgumentException("Bad Prefix. Mast be string of 2 chars");

            var lGuid = Guid.NewGuid();
            byte[] lRequestBytes = new byte[pDataItemID > 0 ? 26 : 22];
            var lPrefix = Encoding.ASCII.GetBytes(pPrefix); // the prefix like 'Mail Confirmation';
            lRequestBytes[0] = lPrefix[0];
            lRequestBytes[1] = lPrefix[1];

            Array.Copy(BitConverter.GetBytes(pWebAccountID), 0, lRequestBytes, 2, 4); // adding the webb Account's gid
            Array.Copy(lGuid.ToByteArray(), 0, lRequestBytes, 6, 16);
            if (pDataItemID > 0)
                Array.Copy(BitConverter.GetBytes(pDataItemID), 0, lRequestBytes, 22, 4);
            var lRequestCode = Convert.ToBase64String(lRequestBytes).TrimEnd('=').Replace("/", "_").Replace("+", "-");
            return lRequestCode;
        }

        public static bool TryParseRequestCode(string pCode, out string pPrefix, out int pDataItemID)
        {
            pDataItemID = 0;
            if (string.IsNullOrEmpty(pCode) || pCode.Length < 10)
            {
                pPrefix = null;
                return false;
            }
            pCode = pCode.Replace("-", "+").Replace("_", "/");
            while (pCode.Length % 4 != 0)
                pCode += "=";
            byte[] lCodeBytes = null;
            try
            {
                lCodeBytes = Convert.FromBase64String(pCode);
            }
            catch
            {
                pPrefix = null;
                return false;
            }
            if (lCodeBytes == null || lCodeBytes.Length < 10)
            {
                pPrefix = null;
                return false;
            }
            pPrefix = Encoding.ASCII.GetString(lCodeBytes, 0, 2);
            if (lCodeBytes.Length >= 26)
            {
                pDataItemID = BitConverter.ToInt32(lCodeBytes, 22);
            }
            return true;
        }

        public static bool TryParseRequestCode(string pRequestCode, out string pPrefix, out int pAccountID, out Guid pGuid)
        {
            if (string.IsNullOrEmpty(pRequestCode))
            {
                pPrefix = null;
                pAccountID = 0;
                pGuid = Guid.Empty;
                return false;
            }
            var lCodedString = pRequestCode.Replace("-", "+").Replace("_", "/");
            int lCSLength = lCodedString.Length;
            if (lCSLength % 4 != 0)
                lCodedString = lCodedString.PadRight(lCSLength + (4 - (lCSLength % 4)), '=');
            byte[] lBytes;
            try
            {
                lBytes = Convert.FromBase64String(lCodedString);
                if (lBytes.Length < 6)
                {
                    pPrefix = null;
                    pAccountID = 0;
                    pGuid = Guid.Empty;
                    return false;
                }
            }
            catch
            {
                pPrefix = null;
                pAccountID = 0;
                pGuid = Guid.Empty;
                return false;
            }
            var lPrefBytes = new byte[2];
            lPrefBytes[0] = lBytes[0];
            lPrefBytes[1] = lBytes[1];
            pPrefix = Encoding.ASCII.GetString(lPrefBytes);
            pAccountID = BitConverter.ToInt32(lBytes, 2);
            var lGuidBytes = new byte[16];
            Array.Copy(lBytes, 6, lGuidBytes, 0, 16);
            pGuid = new Guid(lGuidBytes);
            return true;
        }

        public static string ProcessTemplateReplacement(string pTemplate, Dictionary<string, string> pReplacement)
        {
            if (pReplacement == null || pReplacement.Count == 0)
                return pTemplate;
            var lSB = new StringBuilder(pTemplate);
            foreach (var kvp in pReplacement)
                lSB.Replace(kvp.Key, kvp.Value);
            return lSB.ToString();
        }
    }
}
