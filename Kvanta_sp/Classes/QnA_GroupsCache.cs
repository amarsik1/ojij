﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using asb_ipc;
using asb_ipc.DataManagment;
using MD_Schema;
using AS_MetaData;

namespace MR_sp.Classes
{

    public class QnA_GroupsCache
    {
        private IDataManager m_DM;
        private static ValueTableSchema s_QnAGroupsItemsSchema;
        private static ValueTableSchema s_QnAGroupsSchema;
        private Dictionary<int, ATN_Group> m_ATN_Groups = new Dictionary<int, ATN_Group>();
        private Dictionary<int, ATN_Group.Item> m_ItemsMap = new Dictionary<int, ATN_Group.Item>();
        private Action<ATN_Group.Item> m_ItemInitor;
        private mdSessionParameter m_GroupIDSP;
        private ReadCommandBase m_ReadHeaderCommand;
        private ReadCommandBase m_ReadItemsCommand;
        private List<int> m_AbsentInDBGroups = new List<int>();

        #region Constructor
        public static QnA_GroupsCache Instance { get; private set; }
        public QnA_GroupsCache(IDataManager pDM, Action<ATN_Group.Item> pItemInitor)
        {
            m_DM = pDM;
            m_ItemInitor = pItemInitor;
            if (Instance == null)
                Instance = this;
        }
        #endregion
        #region Properties
        public static ValueTableSchema QnAGroupsItemsSchema
        {
            get
            {
                return s_QnAGroupsItemsSchema ?? (s_QnAGroupsItemsSchema = new ValueTableSchema(QnA_GroupsItems_TS_.__MD, 
                    QnA_GroupsItems_TS_.AlternativesGroup,
                    QnA_GroupsItems_TS_.Item, 
                    QnA_GroupsItems_TS_.ordinal, 
                    QnA_GroupsItems_TS_.Item_name, 
                    QnA_GroupsItems_TS_.IntValue, 
                    QnA_GroupsItems_TS_.ImageFile,
                    QnA_GroupsItems_TS_.ImageFile_view_,
                    QnA_GroupsItems_TS_.comments));
            }
        }
        public static ValueTableSchema QnAGroupsSchema
        {
            get
            {
                return s_QnAGroupsSchema ?? (s_QnAGroupsSchema = new ValueTableSchema(QnAlternativesGroups_TS_.__MD, 
                    QnAlternativesGroups_TS_.gid,
                    QnAlternativesGroups_TS_.name, 
                    QnAlternativesGroups_TS_.parent_group, 
                    QnAlternativesGroups_TS_.Is4ServiceUse, 
                    QnAlternativesGroups_TS_.UseCommentsInUI));
            }
        }

        public bool Inited { get { return m_DM != null && !m_DM.IsDisposed; } }
        #endregion

        #region Methodes
        private void onATNGroupChanged(ValueTable pVT)
        {
            foreach (var r in pVT)
            {
                int lID = r.GetInt(0);
                if (m_AbsentInDBGroups.Contains(lID))
                    m_AbsentInDBGroups.Remove(lID);
                if (m_ATN_Groups != null && m_ATN_Groups.ContainsKey(lID))
                    m_ATN_Groups.Remove(lID);
            }
        }

        public ATN_Group.Item GetATNItem(int pItemID)
        {
            ATN_Group.Item lRes;
            m_ItemsMap.TryGetValue(pItemID, out lRes);
            return lRes;
        }

        public ATN_Group GetATNGroup(int pID)
        {
            ATN_Group lResGroup;
            if (m_ATN_Groups.TryGetValue(pID, out lResGroup) && lResGroup != null)
                return lResGroup;
            if (m_AbsentInDBGroups.Contains(pID))
                return null;
            lock (m_ATN_Groups)
            {
                if (m_ATN_Groups.TryGetValue(pID, out lResGroup) && lResGroup != null)
                    return lResGroup;
                if (m_DM == null || m_DM.IsDisposed)
                    m_DM = DMHelper.DataManager;

                if (m_GroupIDSP == null)
                    m_GroupIDSP = m_DM.SessionParameters.Add("CurATNGroupID_", _DataTypes_SD_.QnAlternativesGroups_reference_dt);
                if (m_ReadHeaderCommand == null)
                {
                    m_ReadHeaderCommand = m_DM.allocateReadCommand(QnAGroupsSchema,
                          QnAlternativesGroups_TS_.__MD.NewFilter(QnAlternativesGroups_TS_.gid, m_GroupIDSP, m_DM), 0, new[] { m_GroupIDSP });
                    m_DM.SubscribeDataUpdate(new ValueTableSchema(QnAlternativesGroups_TS_.__MD, QnAlternativesGroups_TS_.gid), onATNGroupChanged, DBSubscripctionModes.General);
                }
                if (m_ReadItemsCommand == null)
                {
                    m_ReadItemsCommand = m_DM.allocateReadCommand(QnAGroupsItemsSchema,
                        QnA_GroupsItems_TS_.__MD.NewFilter(QnA_GroupsItems_TS_.AlternativesGroup, m_GroupIDSP, m_DM), 0, new[] { m_GroupIDSP });
                    m_DM.SubscribeDataUpdate(new ValueTableSchema(QnA_GroupsItems_TS_.__MD, QnA_GroupsItems_TS_.AlternativesGroup), onATNGroupChanged, DBSubscripctionModes.General);
                }
                m_DM.SetSPValue(m_GroupIDSP, pID);
                var lHeaderVT = m_ReadHeaderCommand.ReadVT();
                if (lHeaderVT == null || lHeaderVT.CountOfRows == 0)
                {
                    m_AbsentInDBGroups.Add(pID);
                    return null;
                }
                var lGroupVTR = lHeaderVT[0];
                lResGroup = new ATN_Group(pID)
                {
                    Name = lGroupVTR.GetString(QnAlternativesGroups_TS_.name),
                    ParentID = lGroupVTR.GetInt(QnAlternativesGroups_TS_.parent_group)
                };
                var lWithComments = lGroupVTR.GetBool(QnAlternativesGroups_TS_.UseCommentsInUI);
                var lItemsVT = m_ReadItemsCommand.ReadVT();
                if (lItemsVT != null)
                    foreach (var r in lItemsVT)
                    {
                        var lItem = new ATN_Group.Item(r.GetInt(QnA_GroupsItems_TS_.Item))
                        {
                            Name = r.GetString(QnA_GroupsItems_TS_.Item_name),
                            Ordinal = r.GetInt(QnA_GroupsItems_TS_.ordinal),
                            IntValue = r.GetInt(QnA_GroupsItems_TS_.IntValue),
                            ImageFileID = r.GetInt(QnA_GroupsItems_TS_.ImageFile),
                            ImageFileName = r.GetString(QnA_GroupsItems_TS_.ImageFile_view_)
                        };
                        if (lWithComments && !r.IsEmpty(QnA_GroupsItems_TS_.comments))
                            lItem.Comments = r.GetString(QnA_GroupsItems_TS_.comments);

                        m_ItemInitor?.Invoke(lItem);
                        lResGroup.Items.Add(lItem);

                        m_ItemsMap[lItem.ID] = lItem;
                    }
                m_ATN_Groups[pID] = lResGroup;
                return lResGroup;
            }
        }
        #endregion







    }
}
