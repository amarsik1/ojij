﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace MR_sp.Classes
{
    public class ATN_Group
    {
        private static ATN_Group s_DummyATNGroup;
        public int ID { get; }
        public int ParentID { get; set; }
        public string Name { get; set; }
        public List<Item> Items { get; }
        public ATN_Group(int pID)
        {
            ID = pID;
            Items = new List<Item>();
        }

        public class Item
        {
            public int ID { get; private set; }
            public int IntValue { get; set; }
            public int Ordinal { get; set; }
            public string Name { get; set; }
            [DefaultValue(0)]
            public int ImageFileID { get; set; }
            [DefaultValue("")]
            public string ImageFileName { get; set; }
            [DefaultValue("")]
            public string ImgPath { get; set; }

            public string Comments { get; set; }

            public Item(int pID)
            {
                ID = pID;
            }
        }

        public static ATN_Group DummyATNGroup
        {
            get
            {
                if (s_DummyATNGroup != null) return s_DummyATNGroup;
                s_DummyATNGroup = new ATN_Group(-997) { Name = "[Some group of alternatives]" };
                for (int i = 1; i < 8; i++)
                {
                    s_DummyATNGroup.Items.Add(new Item(-9100 - i) { Name = "Some item " + i, IntValue = i, Ordinal = i });
                }
                return s_DummyATNGroup;
            }
        }

        private static readonly JsonSerializerSettings s_serializeSettings = new JsonSerializerSettings
        {
            NullValueHandling = NullValueHandling.Ignore,
            DefaultValueHandling = DefaultValueHandling.Ignore
        };

        public string ToJson()
        {
            var result = JsonConvert.SerializeObject(this, Formatting.Indented, s_serializeSettings);
            return result;
        }
    }

    public class QnT_Model
    {
        private Dictionary<int, QGroup> m_AllGroups = new Dictionary<int, QGroup>();
        private List<QGroup> m_RootGroups = new List<QGroup>();
        private List<QnT_Item> m_RootItems = new List<QnT_Item>();

        public int ID { get; private set; }
        public QnTHeader Header { get; private set; }
        public QnT_Model(int pID)
        {
            ID = pID;
            Header = new QnTHeader(pID);
        }
        public class QnTHeader
        {
            public int ID { get; private set; }
            public string Name { get; set; }
            public string Description { get; set; }

            public QnTHeader(int pID)
            {
                ID = pID;
            }
        }

        public class QGroup
        {
            private string m_IVComment = "";
            public int ID { get; private set; }
            public int TemplateId { get; private set; }
            [DefaultValue("")]
            public string Name { get; private set; }
            [DefaultValue("")]
            public string Caption { get; set; }
            [DefaultValue("")]
            public string ViewStyle { get; set; }
            [DefaultValue("")]
            public string RComment { get; set; }
            [DefaultValue("")]
            public string IVComment { get; set; }
            public int AltGroupID { get; set; }
            public int ParentQGroupID { get; private set; }
            public Dictionary<int, string> ChildGroups { get; }
            public List<QnT_Item> QItems { get; }
            public bool IsActual { get; set; }
            public bool ContainsInTextReferences { get; set; }

            [JsonIgnore]
            public object HTMLResult { get; set; }

            public QGroup(int pID, int pTemplateID, string pName, int pParentGroupID)
            {
                ID = pID;
                TemplateId = pTemplateID;
                Name = pName;
                ParentQGroupID = pParentGroupID;
                ChildGroups = new Dictionary<int, string>();
                QItems = new List<QnT_Item>();
                IsActual = true;
            }

            public bool ShouldSerializeChildGroups()
            {
                return ChildGroups.Count > 0;
            }
            public bool ShouldSerializeQItems()
            {
                return QItems.Count > 0;
            }

            private static readonly JsonSerializerSettings s_serialSettings = new JsonSerializerSettings()
            {
                ContractResolver = new IVCommentContractResolver(false),
                NullValueHandling = NullValueHandling.Ignore,
                DefaultValueHandling = DefaultValueHandling.Ignore
            };
            private static readonly JsonSerializerSettings s_serialSettingsAdmin = new JsonSerializerSettings()
            {
                ContractResolver = new IVCommentContractResolver(true),
                NullValueHandling = NullValueHandling.Ignore,
                DefaultValueHandling = DefaultValueHandling.Ignore
            };

            public string ToJson(bool pIsDbAdminOrInterviewer)
            {
                string result = JsonConvert.SerializeObject(this, Formatting.Indented, pIsDbAdminOrInterviewer ? s_serialSettingsAdmin : s_serialSettings);
                return result;
            }
            private class IVCommentContractResolver : DefaultContractResolver
            {
                private readonly bool m_isIVCommentsAllowed;

                public IVCommentContractResolver(bool isIVCommentsAllowed)
                {
                    m_isIVCommentsAllowed = isIVCommentsAllowed;
                }
                protected override IList<JsonProperty> CreateProperties(Type type, MemberSerialization memberSerialization)
                {
                    IList<JsonProperty> res = new List<JsonProperty>();
                    var properties = base.CreateProperties(type, memberSerialization);
                    foreach (var p in properties)
                    {
                        if (p.PropertyName == "IVComment" && !m_isIVCommentsAllowed)
                            continue;
                        res.Add(p);
                    }
                    return res;
                }
            }
        }

        public class QnT_Item
        {
            public int ID { get; private set; }
            public int Ordinal { get; set; }
            public string QText { get; set; }
            [DefaultValue("")]
            public string QParams { get; set; }
            public byte QType { get; set; }
            [DefaultValue("")]
            public string QCode { get; set; }
            [DefaultValue("")]
            public string IVComment { get; set; }
            [DefaultValue("")]
            public string RComment { get; set; }
            public int ATNGroupID { get; set; }
            public int ATNGroup2ID { get; set; }
            [DefaultValue(0)]
            public byte IsOpen { get; set; }
            public byte Required { get; set; }

            /// <summary>
            /// Availability condition ID
            /// </summary>
            [DefaultValue(0)]
            public int AvCndID { get; set; }
            /// <summary>
            /// This signals about existance of pass redirection conditions on answer
            /// </summary>
            [DefaultValue(0)]
            public byte CndType { get; set; } // if CndType % 4 == 1: contains pass rules; if CndType>3: contains rule for alternatives
            public QnT_Item(int pID)
            {
                ID = pID;
            }
        }

    }
}

