﻿using ADE.Core.Components;
using Entity_Schema.Education;
using Entity_Schema.MailingInfo;
using Entity_Schema.RegInfo;
using K7P_sp.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace K7P_sp
{
    public static class ModulesLoader
    {

        public static void LoadModules()
        {
            var lManager = DataModulesManager.Instance;
            lManager.RegisterModule(new DataModule("KvantaApp",
                new Type[] {
                    typeof(GroupsInfoSet),
                    typeof(MaterialsBook),
                    typeof(AppRoot),
                    typeof(RegStart),
                    typeof(LessonsInfo)
                }, 
                new [] {
                    typeof(AccountService),
                    typeof(GroupsService),
                    typeof(AppRootService),
                    typeof(LessonsService),
                    typeof(StudentChallageService),
                    typeof(TasksService)
                }));

            lManager.RegisterModule(new DataModule(asb_ipc.GraphManagement.GModuleBase.MAILING_MODULE_NAME, 
                new[] {typeof(MailBoxesSet)}, 
                null));

        }
    }
}
