using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using appcore.DataProcessing;
using ADE.EntityModel;

namespace Entity_Schema.Education {

	public partial class MaterialsBook: EntityBase<MaterialsBook>
	{
		public static class S
		{
			[GProp("mdTable", "EduTopics")]
			[GProp("mdTableUserName", "��������� ����")]
			[GProp("mdTableKind", "2")]
			[GProp("mdTableGid", "15898")]
			public const string _EduTopic = "EduTopic";

			[GProp("mdTable", "EduMaterials")]
			[GProp("mdTableUserName", "��������� ��������")]
			[GProp("mdTableKind", "2")]
			[GProp("mdTableGid", "15933")]
			public const string _EduMaterial = "EduMaterial";

			public partial class EduTopic: RefEntityBase<EduTopic,int>
			{
				[GProp("mdTable", "EduTopics")]
				[GProp("mdTableUserName", "��������� ����")]
				[GProp("mdTableKind", "2")]
				[GProp("mdTableGid", "15898")]
				public static class S
				{
					[GProp("mdFieldGid", "34051")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "�������������")]
					[GProp("mdDataType", "int")]
					public const string _ID = "gid";

					[GProp("mdFieldGid", "34052")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "�����")]
					[GProp("mdDataType", "varchar(255)")]
					[GProp("DataLength", "255")]
					public const string _name = "name";

					[GProp("mdFieldGid", "34054")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "������")]
					[GProp("mdDataType", "Object_statuses_reference")]
					[GProp("mdReference", "Object_statuses")]
					public const string _object_status = "object_status";

					[GProp("mdFieldGid", "34057")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "������ ����")]
					[GProp("mdDataType", "EduTopics_reference")]
					[GProp("mdReference", "EduTopics")]
					[GProp("Reference", "EduTopic")]
					public const string _parent_topic = "parent_topic";

					[GProp("mdFieldGid", "34056")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "����")]
					[GProp("mdDataType", "nvarchar(MAX)")]
					[GProp("DataLength", "0")]
					public const string _Description = "Description";

					[GProp("mdFieldGid", "34053")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "��������")]
					[GProp("mdDataType", "varchar(255)")]
					[GProp("DataLength", "255")]
					public const string _comments = "comments";

					[GProp("mdFieldGid", "34055")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "����� ����")]
					[GProp("mdDataType", "int")]
					public const string _version_no = "version_no";
				}

				int m_gid_;
				public override int ID {
					get { return m_gid_; }
					set {
						if (m_gid_>0 && m_gid_ != value)
							throw new AccessViolationException("						Changing persistent ID value is denied!");
SetPropValue(value, ref m_gid_, "ID"); }
				}

				string m_name_;
				public string name {
					get { return m_name_; }
					set { SetPropValue(value, ref m_name_, S._name); }
				}

				int m_object_status_;
				public int object_status {
					get { return m_object_status_; }
					set { SetPropValue(value, ref m_object_status_, S._object_status); }
				}

				int m_parent_topic_;
				public int parent_topic {
					get { return m_parent_topic_; }
					set { SetPropValue(value, ref m_parent_topic_, S._parent_topic); }
				}
				public EduTopic parent_topic_Ref()
				{
					return GetRefEntity<EduTopic,int>(m_parent_topic_);
				}

				string m_Description_;
				public string Description {
					get { return m_Description_; }
					set { SetPropValue(value, ref m_Description_, S._Description); }
				}

				string m_comments_;
				public string comments {
					get { return m_comments_; }
					set { SetPropValue(value, ref m_comments_, S._comments); }
				}

				int m_version_no_;
				public int version_no {
					get { return m_version_no_; }
					set { SetPropValue(value, ref m_version_no_, S._version_no); }
				}

				public class Collection_ : RefEntitiesCollection<EduTopic,int> 
				{
					public MultiDataIndex<int> parent_topic { get; private set; }

					public Collection_(IEntityHost<EduTopic> pHost)
						:base(pHost)					{
						parent_topic = AddMultiDataIndex(x=>x.parent_topic, S._parent_topic);
					}
				}
			}

			public partial class EduMaterial: RefEntityBase<EduMaterial,int>
			{
				[GProp("mdTable", "EduMaterials")]
				[GProp("mdTableUserName", "��������� ��������")]
				[GProp("mdTableKind", "2")]
				[GProp("mdTableGid", "15933")]
				public static class S
				{
					[GProp("mdFieldGid", "34464")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "�������������")]
					[GProp("mdDataType", "int")]
					public const string _ID = "gid";

					[GProp("mdFieldGid", "34470")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "���������")]
					[GProp("mdDataType", "varchar(100)")]
					[GProp("DataLength", "100")]
					public const string _Caption = "Caption";

					[GProp("mdFieldGid", "34497")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "����")]
					[GProp("mdDataType", "EduTopics_reference")]
					[GProp("mdReference", "EduTopics")]
					[GProp("Reference", "EduTopic")]
					public const string _Topic = "Topic";

					[GProp("mdFieldGid", "34468")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "����� ��������")]
					[GProp("mdDataType", "varchar(MAX)")]
					[GProp("DataLength", "0")]
					public const string _MaterialsText = "MaterialsText";

					[GProp("mdFieldGid", "34496")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "��� ��������")]
					[GProp("mdDataType", "EduMaterialTypes")]
					public const string _MaterialType = "MaterialType";

					[GProp("mdFieldGid", "34065")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "������ � �����")]
					[GProp("mdDataType", "varchar(2000)")]
					[GProp("DataLength", "2000")]
					public const string _MaterialURL = "MaterialURL";

					[GProp("mdFieldGid", "34471")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "�������")]
					[GProp("mdDataType", "varchar(255)")]
					[GProp("DataLength", "255")]
					public const string _Source = "Source";

					[GProp("mdFieldGid", "34465")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "������")]
					[GProp("mdDataType", "Object_statuses_reference")]
					[GProp("mdReference", "Object_statuses")]
					public const string _object_status = "object_status";

					[GProp("mdFieldGid", "34498")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "��������")]
					[GProp("mdDataType", "varchar(MAX)")]
					[GProp("DataLength", "0")]
					public const string _comments = "comments";

					[GProp("mdFieldGid", "34477")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "����� ����")]
					[GProp("mdDataType", "int")]
					public const string _version_no = "version_no";
				}

				int m_gid_;
				public override int ID {
					get { return m_gid_; }
					set {
						if (m_gid_>0 && m_gid_ != value)
							throw new AccessViolationException("						Changing persistent ID value is denied!");
SetPropValue(value, ref m_gid_, "ID"); }
				}

				string m_Caption_;
				public string Caption {
					get { return m_Caption_; }
					set { SetPropValue(value, ref m_Caption_, S._Caption); }
				}

				int m_Topic_;
				public int Topic {
					get { return m_Topic_; }
					set { SetPropValue(value, ref m_Topic_, S._Topic); }
				}
				public EduTopic Topic_Ref()
				{
					return GetRefEntity<EduTopic,int>(m_Topic_);
				}

				string m_MaterialsText_;
				public string MaterialsText {
					get { return m_MaterialsText_; }
					set { SetPropValue(value, ref m_MaterialsText_, S._MaterialsText); }
				}

				int m_MaterialType_;
				public int MaterialType {
					get { return m_MaterialType_; }
					set { SetPropValue(value, ref m_MaterialType_, S._MaterialType); }
				}

				string m_MaterialURL_;
				public string MaterialURL {
					get { return m_MaterialURL_; }
					set { SetPropValue(value, ref m_MaterialURL_, S._MaterialURL); }
				}

				string m_Source_;
				public string Source {
					get { return m_Source_; }
					set { SetPropValue(value, ref m_Source_, S._Source); }
				}

				int m_object_status_;
				public int object_status {
					get { return m_object_status_; }
					set { SetPropValue(value, ref m_object_status_, S._object_status); }
				}

				string m_comments_;
				public string comments {
					get { return m_comments_; }
					set { SetPropValue(value, ref m_comments_, S._comments); }
				}

				int m_version_no_;
				public int version_no {
					get { return m_version_no_; }
					set { SetPropValue(value, ref m_version_no_, S._version_no); }
				}

				public class Collection_ : RefEntitiesCollection<EduMaterial,int> 
				{
					public MultiDataIndex<int> Topic { get; private set; }

					public Collection_(IEntityHost<EduMaterial> pHost)
						:base(pHost)					{
						Topic = AddMultiDataIndex(x=>x.Topic, S._Topic);
					}
				}
			}
		}

		S.EduTopic.Collection_ m_EduTopic_;
		public S.EduTopic.Collection_ EduTopic {
			get { 
				 if (m_EduTopic_ == null) {
					 lock(this) {
					 if (m_EduTopic_ == null)
						 m_EduTopic_ = new S.EduTopic.Collection_(GetHost<S.EduTopic>());
					 }
				 }
				 return m_EduTopic_;
			 }
		}

		S.EduMaterial.Collection_ m_EduMaterial_;
		public S.EduMaterial.Collection_ EduMaterial {
			get { 
				 if (m_EduMaterial_ == null) {
					 lock(this) {
					 if (m_EduMaterial_ == null)
						 m_EduMaterial_ = new S.EduMaterial.Collection_(GetHost<S.EduMaterial>());
					 }
				 }
				 return m_EduMaterial_;
			 }
		}
	}
}

