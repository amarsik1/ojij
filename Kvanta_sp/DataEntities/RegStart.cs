﻿using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using appcore.DataProcessing;
using ADE.EntityModel;

namespace Entity_Schema.RegInfo {

	[DataCacheMode("Domain")]
	public partial class RegStart: EntityBase<RegStart>, ICachedEntity
	{
		public RegStart():base() {}
		public RegStart(IEntityHost<RegStart> pHost):base(pHost) {}

		public static class S
		{
			[GProp("mdTable", "PublicInvitations")]
			[GProp("mdTableUserName", "Публічні запрошення")]
			[GProp("mdTableKind", "2")]
			[GProp("mdTableGid", "15944")]
			[GProp("AutoLoad", "1")]
			[GProp("CacheMode", "Domain")]
			[GProp("Filter", "Active")]
			public const string _PublicInvitations = "PublicInvitations";

			[GProp("mdTable", "EduGroupKinds")]
			[GProp("mdTableUserName", "Види навчальних груп")]
			[GProp("mdTableKind", "2")]
			[GProp("mdTableGid", "15941")]
			[GProp("AutoLoad", "1")]
			[GProp("CacheMode", "Domain")]
			[GProp("Filter", "Active")]
			public const string _EduGroupKinds = "EduGroupKinds";

			public partial class PublicInvitation: RefEntityBase<PublicInvitation,int>
			{
				public PublicInvitation():base() {}
				public PublicInvitation(IEntityHost<PublicInvitation> pHost):base(pHost) {}

				[GProp("mdTable", "PublicInvitations")]
				[GProp("mdTableUserName", "Публічні запрошення")]
				[GProp("mdTableKind", "2")]
				[GProp("mdTableGid", "15944")]
				[GProp("AutoLoad", "1")]
				[GProp("CacheMode", "Domain")]
				[GProp("Filter", "Active")]
				public static class S
				{
					[GProp("mdFieldGid", "34603")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Ідентифікатор")]
					[GProp("mdDataType", "smallint")]
					public const string _ID = "gid";

					[GProp("mdFieldGid", "34606")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Статус")]
					[GProp("mdDataType", "Object_statuses_reference")]
					[GProp("mdReference", "Object_statuses")]
					public const string _object_status = "object_status";

					[GProp("mdFieldGid", "34612")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Назва")]
					[GProp("mdDataType", "varchar(255)")]
					[GProp("DataLength", "255")]
					public const string _name = "name";

					[GProp("mdFieldGid", "34607")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Група осіб")]
					[GProp("mdDataType", "PersonGroups_reference")]
					[GProp("mdReference", "PersonsGroups")]
					public const string _PersonsGroup = "PersonsGroup";

					[GProp("mdFieldGid", "34610")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Вид освітньої групи")]
					[GProp("mdDataType", "EduGroupKinds_reference")]
					[GProp("mdReference", "EduGroupKinds")]
					public const string _EduGroupKind = "EduGroupKind";

					[GProp("mdFieldGid", "34611")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Роль користувачів")]
					[GProp("mdDataType", "mdRolesReference")]
					public const string _UsersRole = "UsersRole";

					[GProp("mdFieldGid", "34615")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Адміністратор реєстрацій")]
					[GProp("mdDataType", "Persons_reference")]
					[GProp("mdReference", "Persons")]
					public const string _RegistrationAdmin = "RegistrationAdmin";

					[GProp("mdFieldGid", "34608")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Дата початку реєстрації")]
					[GProp("mdDataType", "date")]
					public const string _start_date = "start_date";

					[GProp("mdFieldGid", "34609")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Дата завершення реєстрації")]
					[GProp("mdDataType", "date")]
					public const string _end_date = "end_date";

					[GProp("mdFieldGid", "34604")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Коментар")]
					[GProp("mdDataType", "varchar(MAX)")]
					[GProp("DataLength", "0")]
					public const string _comments = "comments";

					[GProp("mdFieldGid", "34605")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Номер версії")]
					[GProp("mdDataType", "int")]
					public const string _version_no = "version_no";
				}

				#region fields of PublicInvitation
				int m_gid_;
				int m_object_status_;
				string m_name_;
				int m_PersonsGroup_;
				int m_EduGroupKind_;
				int m_UsersRole_;
				int m_RegistrationAdmin_;
				DateTime m_start_date_;
				DateTime m_end_date_;
				string m_comments_;
				int m_version_no_;
				#endregion fields of PublicInvitation

				public override int ID
				{
					get { return m_gid_; }
					set
					{
						if (m_gid_>0 && m_gid_ != value)
							throw new AccessViolationException("Changing persistent ID value is denied!");
						SetPropValue(value, ref m_gid_, S._ID); }
				}

				public int object_status
				{
					get { return m_object_status_; }
					set { SetPropValue(value, ref m_object_status_, S._object_status); }
				}

				public string name
				{
					get { return m_name_; }
					set { SetPropValue(value, ref m_name_, S._name); }
				}

				public int PersonsGroup
				{
					get { return m_PersonsGroup_; }
					set { SetPropValue(value, ref m_PersonsGroup_, S._PersonsGroup); }
				}

				public int EduGroupKind
				{
					get { return m_EduGroupKind_; }
					set { SetPropValue(value, ref m_EduGroupKind_, S._EduGroupKind); }
				}

				public int UsersRole
				{
					get { return m_UsersRole_; }
					set { SetPropValue(value, ref m_UsersRole_, S._UsersRole); }
				}

				public int RegistrationAdmin
				{
					get { return m_RegistrationAdmin_; }
					set { SetPropValue(value, ref m_RegistrationAdmin_, S._RegistrationAdmin); }
				}

				public DateTime start_date
				{
					get { return m_start_date_; }
					set { SetPropValue(value, ref m_start_date_, S._start_date); }
				}

				public DateTime end_date
				{
					get { return m_end_date_; }
					set { SetPropValue(value, ref m_end_date_, S._end_date); }
				}

				public string comments
				{
					get { return m_comments_; }
					set { SetPropValue(value, ref m_comments_, S._comments); }
				}

				public int version_no
				{
					get { return m_version_no_; }
					set { SetPropValue(value, ref m_version_no_, S._version_no); }
				}


				public class Collection_ : AutoReLoadCollection<PublicInvitation,int> 
				{
					public Collection_(IEntityHost<PublicInvitation> pHost, IDataProxy pProxy = null, bool pActiveItemsOnly = true)
						:base(pHost, pProxy, pActiveItemsOnly)
					{
					}
				}
			}

			public partial class EduGroupKind: EntityBase<EduGroupKind>
			{
				public EduGroupKind():base() {}
				public EduGroupKind(IEntityHost<EduGroupKind> pHost):base(pHost) {}

				[GProp("mdTable", "EduGroupKinds")]
				[GProp("mdTableUserName", "Види навчальних груп")]
				[GProp("mdTableKind", "2")]
				[GProp("mdTableGid", "15941")]
				[GProp("AutoLoad", "1")]
				[GProp("CacheMode", "Domain")]
				[GProp("Filter", "Active")]
				public static class S
				{
					[GProp("mdFieldGid", "34555")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Ідентифікатор")]
					[GProp("mdDataType", "int")]
					public const string _gid = "gid";

					[GProp("mdFieldGid", "34556")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Назва")]
					[GProp("mdDataType", "varchar(255)")]
					[GProp("DataLength", "255")]
					public const string _name = "name";

					[GProp("mdFieldGid", "34557")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Статус")]
					[GProp("mdDataType", "Object_statuses_reference")]
					[GProp("mdReference", "Object_statuses")]
					public const string _object_status = "object_status";

					[GProp("mdFieldGid", "34562")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Клас навчання")]
					[GProp("mdDataType", "tinyint")]
					public const string _GroupLevel = "GroupLevel";

					[GProp("mdFieldGid", "34616")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Старша група")]
					[GProp("mdDataType", "EduGroupKinds_reference")]
					[GProp("mdReference", "EduGroupKinds")]
					public const string _parent_group = "parent_group";

					[GProp("mdFieldGid", "34617")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Опис")]
					[GProp("mdDataType", "nvarchar(MAX)")]
					[GProp("DataLength", "0")]
					public const string _Description = "Description";

					[GProp("mdFieldGid", "34558")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Коментар")]
					[GProp("mdDataType", "varchar(MAX)")]
					[GProp("DataLength", "0")]
					public const string _comments = "comments";

					[GProp("mdFieldGid", "34559")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Номер версії")]
					[GProp("mdDataType", "int")]
					public const string _version_no = "version_no";
				}

				#region fields of EduGroupKind
				int m_gid_;
				string m_name_;
				int m_object_status_;
				int m_GroupLevel_;
				int m_parent_group_;
				string m_Description_;
				string m_comments_;
				int m_version_no_;
				#endregion fields of EduGroupKind

				public int gid
				{
					get { return m_gid_; }
					set { SetPropValue(value, ref m_gid_, S._gid); }
				}

				public string name
				{
					get { return m_name_; }
					set { SetPropValue(value, ref m_name_, S._name); }
				}

				public int object_status
				{
					get { return m_object_status_; }
					set { SetPropValue(value, ref m_object_status_, S._object_status); }
				}

				public int GroupLevel
				{
					get { return m_GroupLevel_; }
					set { SetPropValue(value, ref m_GroupLevel_, S._GroupLevel); }
				}

				public int parent_group
				{
					get { return m_parent_group_; }
					set { SetPropValue(value, ref m_parent_group_, S._parent_group); }
				}

				public string Description
				{
					get { return m_Description_; }
					set { SetPropValue(value, ref m_Description_, S._Description); }
				}

				public string comments
				{
					get { return m_comments_; }
					set { SetPropValue(value, ref m_comments_, S._comments); }
				}

				public int version_no
				{
					get { return m_version_no_; }
					set { SetPropValue(value, ref m_version_no_, S._version_no); }
				}

			}
		}

		#region fields of RegStart
		S.PublicInvitation.Collection_ m_PublicInvitations_;
		EntitiesCollection<S.EduGroupKind> m_EduGroupKinds_;
		#endregion fields of RegStart

		public S.PublicInvitation.Collection_ PublicInvitations
		{
			get
			{ 
				if (m_PublicInvitations_ == null)
				{
					lock(this)
					{
						if (m_PublicInvitations_ == null)
							m_PublicInvitations_ = new S.PublicInvitation.Collection_(GetHost<S.PublicInvitation>());
					}
				}
				return m_PublicInvitations_;
			}
		}

		public EntitiesCollection<S.EduGroupKind> EduGroupKinds
		{
			get
			{ 
				if (m_EduGroupKinds_ == null)
				{
					lock(this)
					{
						if (m_EduGroupKinds_ == null)
							m_EduGroupKinds_ = new EntitiesCollection<S.EduGroupKind>(GetHost<S.EduGroupKind>());
					}
				}
				return m_EduGroupKinds_;
			}
		}

		public Dictionary<IDBRequestsClient, bool> GetDBRequestsClients(string pCacheMode)
		{
			return null;
		}
		partial void AfterAddedToCache(string pCacheMode, IDataProxy pDataProxy);
		public void OnAddedToCache(string pCacheMode, IDataProxy pDataProxy, IDataCollector pCollector)
		{
			if (pCacheMode == "Domain")
			{
				m_PublicInvitations_ = new S.PublicInvitation.Collection_(GetHost<S.PublicInvitation>(), pDataProxy,true);
				pCollector.AddCollection(PublicInvitations);
				EduGroupKinds.LoadAll(pDataProxy, true);
			}
			AfterAddedToCache(pCacheMode, pDataProxy);
		}
	}
}

