﻿using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using appcore.DataProcessing;
using ADE.EntityModel;

namespace Entity_Schema.Education {

	public partial class MaterialsInfo: EntityBase<MaterialsInfo>
	{
		[GProp("AutoLoad", "1")]
		public static class S
		{
			[GProp("mdTable", "EduMaterials")]
			[GProp("mdTableUserName", "Навчальні матеріали")]
			[GProp("mdTableKind", "2")]
			[GProp("mdTableGid", "15933")]
			public const string _EduMaterials = "EduMaterials";

			[GProp("mdTable", "EduTopics")]
			[GProp("mdTableUserName", "Навчальні теми")]
			[GProp("mdTableKind", "2")]
			[GProp("mdTableGid", "15898")]
			[GProp("AutoLoad", "1")]
			public const string _EduTopics = "EduTopics";

			public partial class EduMaterials: RefEntityBase<EduMaterials,int>
			{
				[GProp("mdTable", "EduMaterials")]
				[GProp("mdTableUserName", "Навчальні матеріали")]
				[GProp("mdTableKind", "2")]
				[GProp("mdTableGid", "15933")]
				public static class S
				{
					[GProp("mdFieldGid", "34464")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Ідентифікатор")]
					[GProp("mdDataType", "int")]
					public const string _ID = "gid";

					[GProp("mdFieldGid", "34470")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Заголовок")]
					[GProp("mdDataType", "varchar(100)")]
					[GProp("DataLength", "100")]
					public const string _Caption = "Caption";

					[GProp("mdFieldGid", "34497")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Тема")]
					[GProp("mdDataType", "EduTopics_reference")]
					[GProp("mdReference", "EduTopics")]
					public const string _Topic = "Topic";

					[GProp("mdFieldGid", "34468")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Текст матеріалу")]
					[GProp("mdDataType", "varchar(MAX)")]
					[GProp("DataLength", "0")]
					public const string _MaterialsText = "MaterialsText";

					[GProp("mdFieldGid", "34496")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Тип матеріалу")]
					[GProp("mdDataType", "EduMaterialTypes")]
					public const string _MaterialType = "MaterialType";

					[GProp("mdFieldGid", "34539")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Дата, час реєстрації")]
					[GProp("mdDataType", "datetime")]
					public const string _reg_date = "reg_date";

					[GProp("mdFieldGid", "34065")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Адреса в мережі")]
					[GProp("mdDataType", "varchar(2000)")]
					[GProp("DataLength", "2000")]
					public const string _MaterialURL = "MaterialURL";

					[GProp("mdFieldGid", "34471")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Джерело")]
					[GProp("mdDataType", "varchar(255)")]
					[GProp("DataLength", "255")]
					public const string _Source = "Source";

					[GProp("mdFieldGid", "34540")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Автор реєстрації")]
					[GProp("mdDataType", "Persons_reference")]
					[GProp("mdReference", "Persons")]
					public const string _reg_author = "reg_author";

					[GProp("mdFieldGid", "34465")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Статус")]
					[GProp("mdDataType", "Object_statuses_reference")]
					[GProp("mdReference", "Object_statuses")]
					public const string _object_status = "object_status";

					[GProp("mdFieldGid", "34498")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Коментар")]
					[GProp("mdDataType", "varchar(MAX)")]
					[GProp("DataLength", "0")]
					public const string _comments = "comments";

					[GProp("mdFieldGid", "34477")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Номер версії")]
					[GProp("mdDataType", "int")]
					public const string _version_no = "version_no";
				}

				int m_gid_;
				public override int ID {
					get { return m_gid_; }
					set {
						if (m_gid_>0 && m_gid_ != value)
							throw new AccessViolationException("Changing persistent ID value is denied!");
						SetPropValue(value, ref m_gid_, S._ID); }
				}

				string m_Caption_;
				public string Caption {
					get { return m_Caption_; }
					set { SetPropValue(value, ref m_Caption_, S._Caption); }
				}

				int m_Topic_;
				public int Topic {
					get { return m_Topic_; }
					set { SetPropValue(value, ref m_Topic_, S._Topic); }
				}

				string m_MaterialsText_;
				public string MaterialsText {
					get { return m_MaterialsText_; }
					set { SetPropValue(value, ref m_MaterialsText_, S._MaterialsText); }
				}

				int m_MaterialType_;
				public int MaterialType {
					get { return m_MaterialType_; }
					set { SetPropValue(value, ref m_MaterialType_, S._MaterialType); }
				}

				DateTime m_reg_date_;
				public DateTime reg_date {
					get { return m_reg_date_; }
					set { SetPropValue(value, ref m_reg_date_, S._reg_date); }
				}

				string m_MaterialURL_;
				public string MaterialURL {
					get { return m_MaterialURL_; }
					set { SetPropValue(value, ref m_MaterialURL_, S._MaterialURL); }
				}

				string m_Source_;
				public string Source {
					get { return m_Source_; }
					set { SetPropValue(value, ref m_Source_, S._Source); }
				}

				int m_reg_author_;
				public int reg_author {
					get { return m_reg_author_; }
					set { SetPropValue(value, ref m_reg_author_, S._reg_author); }
				}

				int m_object_status_;
				public int object_status {
					get { return m_object_status_; }
					set { SetPropValue(value, ref m_object_status_, S._object_status); }
				}

				string m_comments_;
				public string comments {
					get { return m_comments_; }
					set { SetPropValue(value, ref m_comments_, S._comments); }
				}

				int m_version_no_;
				public int version_no {
					get { return m_version_no_; }
					set { SetPropValue(value, ref m_version_no_, S._version_no); }
				}

				public class Collection_ : RefEntitiesCollection<EduMaterials,int> 
				{

					public Collection_(IEntityHost<EduMaterials> pHost)
						:base(pHost)					{
					}
				}
			}

			public partial class EduTopics: RefEntityBase<EduTopics,int>
			{
				[GProp("mdTable", "EduTopics")]
				[GProp("mdTableUserName", "Навчальні теми")]
				[GProp("mdTableKind", "2")]
				[GProp("mdTableGid", "15898")]
				[GProp("AutoLoad", "1")]
				public static class S
				{
					[GProp("mdFieldGid", "34051")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Ідентифікатор")]
					[GProp("mdDataType", "int")]
					public const string _ID = "gid";

					[GProp("mdFieldGid", "34052")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Назва")]
					[GProp("mdDataType", "varchar(255)")]
					[GProp("DataLength", "255")]
					public const string _name = "name";

					[GProp("mdFieldGid", "34054")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Статус")]
					[GProp("mdDataType", "Object_statuses_reference")]
					[GProp("mdReference", "Object_statuses")]
					public const string _object_status = "object_status";

					[GProp("mdFieldGid", "34057")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Старша тема")]
					[GProp("mdDataType", "EduTopics_reference")]
					[GProp("mdReference", "EduTopics")]
					public const string _parent_topic = "parent_topic";

					[GProp("mdFieldGid", "34056")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Опис")]
					[GProp("mdDataType", "nvarchar(MAX)")]
					[GProp("DataLength", "0")]
					public const string _Description = "Description";

					[GProp("mdFieldGid", "34053")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Коментар")]
					[GProp("mdDataType", "varchar(255)")]
					[GProp("DataLength", "255")]
					public const string _comments = "comments";

					[GProp("mdFieldGid", "34055")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Номер версії")]
					[GProp("mdDataType", "int")]
					public const string _version_no = "version_no";
				}

				int m_gid_;
				public override int ID {
					get { return m_gid_; }
					set {
						if (m_gid_>0 && m_gid_ != value)
							throw new AccessViolationException("Changing persistent ID value is denied!");
						SetPropValue(value, ref m_gid_, S._ID); }
				}

				string m_name_;
				public string name {
					get { return m_name_; }
					set { SetPropValue(value, ref m_name_, S._name); }
				}

				int m_object_status_;
				public int object_status {
					get { return m_object_status_; }
					set { SetPropValue(value, ref m_object_status_, S._object_status); }
				}

				int m_parent_topic_;
				public int parent_topic {
					get { return m_parent_topic_; }
					set { SetPropValue(value, ref m_parent_topic_, S._parent_topic); }
				}

				string m_Description_;
				public string Description {
					get { return m_Description_; }
					set { SetPropValue(value, ref m_Description_, S._Description); }
				}

				string m_comments_;
				public string comments {
					get { return m_comments_; }
					set { SetPropValue(value, ref m_comments_, S._comments); }
				}

				int m_version_no_;
				public int version_no {
					get { return m_version_no_; }
					set { SetPropValue(value, ref m_version_no_, S._version_no); }
				}

				public class Collection_ : RefEntitiesCollection<EduTopics,int> 
				{

					public Collection_(IEntityHost<EduTopics> pHost)
						:base(pHost)					{
					}
				}
			}
		}

		S.EduMaterials.Collection_ m_EduMaterials_;
		public S.EduMaterials.Collection_ EduMaterials {
			get { 
				 if (m_EduMaterials_ == null) {
					 lock(this) {
					 if (m_EduMaterials_ == null)
						 m_EduMaterials_ = new S.EduMaterials.Collection_(GetHost<S.EduMaterials>());
					 }
				 }
				 return m_EduMaterials_;
			 }
		}

		S.EduTopics.Collection_ m_EduTopics_;
		public S.EduTopics.Collection_ EduTopics {
			get { 
				 if (m_EduTopics_ == null) {
					 lock(this) {
					 if (m_EduTopics_ == null)
						 m_EduTopics_ = new S.EduTopics.Collection_(GetHost<S.EduTopics>());
					 }
				 }
				 return m_EduTopics_;
			 }
		}
	}
}

