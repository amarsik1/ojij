﻿using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using appcore.DataProcessing;
using ADE.EntityModel;

namespace Entity_Schema.Education {

	public partial class LessonsInfo: EntityBase<LessonsInfo>
	{
		public static class S
		{
			[GProp("mdTable", "EduLessons")]
			[GProp("mdTableUserName", "Навчальні уроки")]
			[GProp("mdTableKind", "2")]
			[GProp("mdTableGid", "15901")]
			public const string _EduLessons = "EduLessons";

			[GProp("mdTable", "EduMaterialsList")]
			[GProp("mdTableUserName", "Набір навчальних матеріалів")]
			[GProp("mdTableKind", "2")]
			[GProp("mdTableGid", "15932")]
			public const string _EduMaterialsList = "EduMaterialsList";

			[GProp("mdTable", "EduTopics")]
			[GProp("mdTableUserName", "Навчальні теми")]
			[GProp("mdTableKind", "2")]
			[GProp("mdTableGid", "15898")]
			public const string _EduTopic = "EduTopic";

			public partial class EduLessons: RefEntityBase<EduLessons,int>
			{
				[GProp("mdTable", "EduLessons")]
				[GProp("mdTableUserName", "Навчальні уроки")]
				[GProp("mdTableKind", "2")]
				[GProp("mdTableGid", "15901")]
				public static class S
				{
					[GProp("mdFieldGid", "34074")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Ідентифікатор")]
					[GProp("mdDataType", "int")]
					public const string _ID = "gid";

					[GProp("mdFieldGid", "34081")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Статус")]
					[GProp("mdDataType", "Object_statuses_reference")]
					[GProp("mdReference", "Object_statuses")]
					public const string _object_status = "object_status";

					[GProp("mdFieldGid", "34494")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Навчальна тема")]
					[GProp("mdDataType", "EduTopics_reference")]
					[GProp("mdReference", "EduTopics")]
					public const string _EduTopic = "EduTopic";

					[GProp("mdFieldGid", "34075")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Назва уроку")]
					[GProp("mdDataType", "varchar(2000)")]
					[GProp("DataLength", "2000")]
					public const string _Name = "Name";

					[GProp("mdFieldGid", "34076")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Час уроку")]
					[GProp("mdDataType", "datetime")]
					public const string _LessonTime = "LessonTime";

					[GProp("mdFieldGid", "34077")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Навчальний клас")]
					[GProp("mdDataType", "EduGroup_reference")]
					[GProp("mdReference", "EduGroup")]
					public const string _EduGroup = "EduGroup";

					[GProp("mdFieldGid", "34078")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Опис")]
					[GProp("mdDataType", "nvarchar(MAX)")]
					[GProp("DataLength", "0")]
					public const string _Description = "Description";

					[GProp("mdFieldGid", "34082")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Основний викладач")]
					[GProp("mdDataType", "Persons_reference")]
					[GProp("mdReference", "Persons")]
					public const string _ResponsibleTeacher = "ResponsibleTeacher";

					[GProp("mdFieldGid", "34080")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Коментар")]
					[GProp("mdDataType", "varchar(MAX)")]
					[GProp("DataLength", "0")]
					public const string _comments = "comments";

					[GProp("mdFieldGid", "34079")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Номер версії")]
					[GProp("mdDataType", "int")]
					public const string _version_no = "version_no";
				}

				int m_gid_;
				public override int ID {
					get { return m_gid_; }
					set {
						if (m_gid_>0 && m_gid_ != value)
							throw new AccessViolationException("Changing persistent ID value is denied!");
						SetPropValue(value, ref m_gid_, S._ID); }
				}

				int m_object_status_;
				public int object_status {
					get { return m_object_status_; }
					set { SetPropValue(value, ref m_object_status_, S._object_status); }
				}

				int m_EduTopic_;
				public int EduTopic {
					get { return m_EduTopic_; }
					set { SetPropValue(value, ref m_EduTopic_, S._EduTopic); }
				}

				string m_Name_;
				public string Name {
					get { return m_Name_; }
					set { SetPropValue(value, ref m_Name_, S._Name); }
				}

				DateTime m_LessonTime_;
				public DateTime LessonTime {
					get { return m_LessonTime_; }
					set { SetPropValue(value, ref m_LessonTime_, S._LessonTime); }
				}

				int m_EduGroup_;
				public int EduGroup {
					get { return m_EduGroup_; }
					set { SetPropValue(value, ref m_EduGroup_, S._EduGroup); }
				}

				string m_Description_;
				public string Description {
					get { return m_Description_; }
					set { SetPropValue(value, ref m_Description_, S._Description); }
				}

				int m_ResponsibleTeacher_;
				public int ResponsibleTeacher {
					get { return m_ResponsibleTeacher_; }
					set { SetPropValue(value, ref m_ResponsibleTeacher_, S._ResponsibleTeacher); }
				}

				string m_comments_;
				public string comments {
					get { return m_comments_; }
					set { SetPropValue(value, ref m_comments_, S._comments); }
				}

				int m_version_no_;
				public int version_no {
					get { return m_version_no_; }
					set { SetPropValue(value, ref m_version_no_, S._version_no); }
				}

				public class Collection_ : RefEntitiesCollection<EduLessons,int> 
				{

					public Collection_(IEntityHost<EduLessons> pHost)
						:base(pHost)					{
					}
				}
			}

			public partial class EduMaterialsList: RefEntityBase<EduMaterialsList,int>
			{
				[GProp("mdTable", "EduMaterialsList")]
				[GProp("mdTableUserName", "Набір навчальних матеріалів")]
				[GProp("mdTableKind", "2")]
				[GProp("mdTableGid", "15932")]
				public static class S
				{
					[GProp("mdFieldGid", "34453")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Ідентифікатор")]
					[GProp("mdDataType", "int")]
					public const string _ID = "gid";

					[GProp("mdFieldGid", "34454")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Тема")]
					[GProp("mdDataType", "varchar(2000)")]
					[GProp("DataLength", "2000")]
					public const string _Topic = "Topic";

					[GProp("mdFieldGid", "34459")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Урок")]
					[GProp("mdDataType", "EduLessons_reference")]
					[GProp("mdReference", "EduLessons")]
					public const string _Lesson = "Lesson";

					[GProp("mdFieldGid", "34457")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Навчальна група")]
					[GProp("mdDataType", "EduGroup_reference")]
					[GProp("mdReference", "EduGroup")]
					public const string _EduGroup = "EduGroup";

					[GProp("mdFieldGid", "34460")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Тип підбірки")]
					[GProp("mdDataType", "EduTasksListKinds_reference")]
					[GProp("mdReference", "EduMaterialsListKinds")]
					public const string _ListKind = "ListKind";

					[GProp("mdFieldGid", "34456")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Час створення")]
					[GProp("mdDataType", "datetime")]
					public const string _CreatingTime = "CreatingTime";

					[GProp("mdFieldGid", "34455")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Статус")]
					[GProp("mdDataType", "Object_statuses_reference")]
					[GProp("mdReference", "Object_statuses")]
					public const string _object_status = "object_status";

					[GProp("mdFieldGid", "34458")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Автор")]
					[GProp("mdDataType", "Persons_reference")]
					[GProp("mdReference", "Persons")]
					public const string _Author = "Author";

					[GProp("mdFieldGid", "34499")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Коментар")]
					[GProp("mdDataType", "varchar(MAX)")]
					[GProp("DataLength", "0")]
					public const string _comments = "comments";
				}

				int m_gid_;
				public override int ID {
					get { return m_gid_; }
					set {
						if (m_gid_>0 && m_gid_ != value)
							throw new AccessViolationException("Changing persistent ID value is denied!");
						SetPropValue(value, ref m_gid_, S._ID); }
				}

				string m_Topic_;
				public string Topic {
					get { return m_Topic_; }
					set { SetPropValue(value, ref m_Topic_, S._Topic); }
				}

				int m_Lesson_;
				public int Lesson {
					get { return m_Lesson_; }
					set { SetPropValue(value, ref m_Lesson_, S._Lesson); }
				}

				int m_EduGroup_;
				public int EduGroup {
					get { return m_EduGroup_; }
					set { SetPropValue(value, ref m_EduGroup_, S._EduGroup); }
				}

				int m_ListKind_;
				public int ListKind {
					get { return m_ListKind_; }
					set { SetPropValue(value, ref m_ListKind_, S._ListKind); }
				}

				DateTime m_CreatingTime_;
				public DateTime CreatingTime {
					get { return m_CreatingTime_; }
					set { SetPropValue(value, ref m_CreatingTime_, S._CreatingTime); }
				}

				int m_object_status_;
				public int object_status {
					get { return m_object_status_; }
					set { SetPropValue(value, ref m_object_status_, S._object_status); }
				}

				int m_Author_;
				public int Author {
					get { return m_Author_; }
					set { SetPropValue(value, ref m_Author_, S._Author); }
				}

				string m_comments_;
				public string comments {
					get { return m_comments_; }
					set { SetPropValue(value, ref m_comments_, S._comments); }
				}

				public class Collection_ : RefEntitiesCollection<EduMaterialsList,int> 
				{

					public Collection_(IEntityHost<EduMaterialsList> pHost)
						:base(pHost)					{
					}
				}
			}

			public partial class EduTopic: RefEntityBase<EduTopic,int>
			{
				[GProp("mdTable", "EduTopics")]
				[GProp("mdTableUserName", "Навчальні теми")]
				[GProp("mdTableKind", "2")]
				[GProp("mdTableGid", "15898")]
				public static class S
				{
					[GProp("mdFieldGid", "34051")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Ідентифікатор")]
					[GProp("mdDataType", "int")]
					public const string _ID = "gid";

					[GProp("mdFieldGid", "34052")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Назва")]
					[GProp("mdDataType", "varchar(255)")]
					[GProp("DataLength", "255")]
					public const string _name = "name";

					[GProp("mdFieldGid", "34054")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Статус")]
					[GProp("mdDataType", "Object_statuses_reference")]
					[GProp("mdReference", "Object_statuses")]
					public const string _object_status = "object_status";

					[GProp("mdFieldGid", "34057")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Старша тема")]
					[GProp("mdDataType", "EduTopics_reference")]
					[GProp("mdReference", "EduTopics")]
					[GProp("Reference", "EduTopic")]
					public const string _parent_topic = "parent_topic";

					[GProp("mdFieldGid", "34056")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Опис")]
					[GProp("mdDataType", "nvarchar(MAX)")]
					[GProp("DataLength", "0")]
					public const string _Description = "Description";

					[GProp("mdFieldGid", "34053")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Коментар")]
					[GProp("mdDataType", "varchar(255)")]
					[GProp("DataLength", "255")]
					public const string _comments = "comments";

					[GProp("mdFieldGid", "34055")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Номер версії")]
					[GProp("mdDataType", "int")]
					public const string _version_no = "version_no";
				}

				int m_gid_;
				public override int ID {
					get { return m_gid_; }
					set {
						if (m_gid_>0 && m_gid_ != value)
							throw new AccessViolationException("Changing persistent ID value is denied!");
						SetPropValue(value, ref m_gid_, S._ID); }
				}

				string m_name_;
				public string name {
					get { return m_name_; }
					set { SetPropValue(value, ref m_name_, S._name); }
				}

				int m_object_status_;
				public int object_status {
					get { return m_object_status_; }
					set { SetPropValue(value, ref m_object_status_, S._object_status); }
				}

				int m_parent_topic_;
				public int parent_topic {
					get { return m_parent_topic_; }
					set { SetPropValue(value, ref m_parent_topic_, S._parent_topic); }
				}
				public EduTopic parent_topic_Ref()
				{
					return GetRefEntity<EduTopic,int>(m_parent_topic_);
				}

				string m_Description_;
				public string Description {
					get { return m_Description_; }
					set { SetPropValue(value, ref m_Description_, S._Description); }
				}

				string m_comments_;
				public string comments {
					get { return m_comments_; }
					set { SetPropValue(value, ref m_comments_, S._comments); }
				}

				int m_version_no_;
				public int version_no {
					get { return m_version_no_; }
					set { SetPropValue(value, ref m_version_no_, S._version_no); }
				}

				public class Collection_ : RefEntitiesCollection<EduTopic,int> 
				{

					public Collection_(IEntityHost<EduTopic> pHost)
						:base(pHost)					{
					}
				}
			}
		}

		S.EduLessons.Collection_ m_EduLessons_;
		public S.EduLessons.Collection_ EduLessons {
			get { 
				 if (m_EduLessons_ == null) {
					 lock(this) {
					 if (m_EduLessons_ == null)
						 m_EduLessons_ = new S.EduLessons.Collection_(GetHost<S.EduLessons>());
					 }
				 }
				 return m_EduLessons_;
			 }
		}

		S.EduMaterialsList.Collection_ m_EduMaterialsList_;
		public S.EduMaterialsList.Collection_ EduMaterialsList {
			get { 
				 if (m_EduMaterialsList_ == null) {
					 lock(this) {
					 if (m_EduMaterialsList_ == null)
						 m_EduMaterialsList_ = new S.EduMaterialsList.Collection_(GetHost<S.EduMaterialsList>());
					 }
				 }
				 return m_EduMaterialsList_;
			 }
		}

		S.EduTopic.Collection_ m_EduTopic_;
		public S.EduTopic.Collection_ EduTopic {
			get { 
				 if (m_EduTopic_ == null) {
					 lock(this) {
					 if (m_EduTopic_ == null)
						 m_EduTopic_ = new S.EduTopic.Collection_(GetHost<S.EduTopic>());
					 }
				 }
				 return m_EduTopic_;
			 }
		}
	}
}

