﻿using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using appcore.DataProcessing;
using ADE.EntityModel;

namespace Entity_Schema.RegInfo {

	public partial class UserEventsSet: EntityBase<UserEventsSet>
	{
		public UserEventsSet():base() {}
		public UserEventsSet(IEntityHost<UserEventsSet> pHost):base(pHost) {}

		public static class S
		{
			[GProp("HttpAction", "Register")]
			[GProp("ResultType", "1")]
			public const string _RegisterAccount = "RegisterAccount";

			[GProp("mdTable", "Web_Accounts")]
			[GProp("mdTableUserName", "Реєстраційні дані учасників")]
			[GProp("mdTableKind", "2")]
			[GProp("mdTableGid", "15218")]
			[GProp("MDSchemaType", "Mixed")]
			public const string _Web_Accounts = "Web_Accounts";

			[GProp("HttpAction", "RequestPasswordReset")]
			[GProp("ResultType", "1")]
			[GProp("mdTable", "Web_Requests")]
			public const string _RequestPasswordReset = "RequestPasswordReset";

			[GProp("HttpResultFor", "ConfirmEmail")]
			[GProp("ResultType", "1")]
			[GProp("mdTable", "Web_Requests")]
			public const string _ConfirmEmailResult = "ConfirmEmailResult";

			public const string _EMailMessage = "EMailMessage";

			public partial class RegisterAccount: EntityBase<RegisterAccount>
			{
				public RegisterAccount():base() {}
				public RegisterAccount(IEntityHost<RegisterAccount> pHost):base(pHost) {}

				[GProp("HttpAction", "Register")]
				[GProp("ResultType", "1")]
				public static class S
				{
					[GProp("mdFieldGid", "26011")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Прізвище")]
					[GProp("mdDataType", "varchar(100)")]
					[GProp("DataLength", "100")]
					public const string _Surname = "Surname";

					[GProp("mdFieldGid", "26010")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Ім'я")]
					[GProp("mdDataType", "varchar(100)")]
					[GProp("DataLength", "100")]
					public const string _FirstName = "FirstName";

					[GProp("mdFieldGid", "26009")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "По Батькові")]
					[GProp("mdDataType", "varchar(100)")]
					[GProp("DataLength", "100")]
					public const string _Middle_name = "Middle_name";

					[GProp("mdFieldGid", "23091")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Адреса електронної пошти")]
					[GProp("mdDataType", "varchar(255)")]
					[GProp("DataLength", "255")]
					public const string _Email = "Email";

					[GProp("mdFieldGid", "23092")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Телефони")]
					[GProp("mdDataType", "varchar(50)")]
					[GProp("DataLength", "50")]
					public const string _PhoneNo = "PhoneNo";

					public const string _GenderRB = "GenderRB";

					[GProp("mdFieldGid", "23112")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Пароль")]
					[GProp("mdDataType", "varchar(255)")]
					[GProp("DataLength", "255")]
					public const string _password = "password";

					public const string _InviteCode = "InviteCode";

					[GProp("mdFieldGid", "23093")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Коментар")]
					[GProp("mdDataType", "varchar(MAX)")]
					public const string _comments = "comments";

					[GProp("mdFieldGid", "26007")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Дата народження")]
					[GProp("mdDataType", "date")]
					public const string _BirthDate = "BirthDate";

					public const string _ChildSurName = "ChildSurName";

					public const string _ChildName = "ChildName";

					public const string _ChildEmail = "ChildEmail";
				}

				#region fields of RegisterAccount
				string m_Surname_;
				string m_FirstName_;
				string m_Middle_name_;
				string m_Email_;
				string m_PhoneNo_;
				string m_GenderRB_;
				string m_password_;
				string m_InviteCode_;
				string m_comments_;
				DateTime m_BirthDate_;
				string m_ChildSurName_;
				string m_ChildName_;
				string m_ChildEmail_;
				#endregion fields of RegisterAccount

				public string Surname
				{
					get { return m_Surname_; }
					set { SetPropValue(value, ref m_Surname_, S._Surname); }
				}

				public string FirstName
				{
					get { return m_FirstName_; }
					set { SetPropValue(value, ref m_FirstName_, S._FirstName); }
				}

				public string Middle_name
				{
					get { return m_Middle_name_; }
					set { SetPropValue(value, ref m_Middle_name_, S._Middle_name); }
				}

				public string Email
				{
					get { return m_Email_; }
					set { SetPropValue(value, ref m_Email_, S._Email); }
				}

				public string PhoneNo
				{
					get { return m_PhoneNo_; }
					set { SetPropValue(value, ref m_PhoneNo_, S._PhoneNo); }
				}

				public string GenderRB
				{
					get { return m_GenderRB_; }
					set { SetPropValue(value, ref m_GenderRB_, S._GenderRB); }
				}

				public string password
				{
					get { return m_password_; }
					set { SetPropValue(value, ref m_password_, S._password); }
				}

				public string InviteCode
				{
					get { return m_InviteCode_; }
					set { SetPropValue(value, ref m_InviteCode_, S._InviteCode); }
				}

				public string comments
				{
					get { return m_comments_; }
					set { SetPropValue(value, ref m_comments_, S._comments); }
				}

				public DateTime BirthDate
				{
					get { return m_BirthDate_; }
					set { SetPropValue(value, ref m_BirthDate_, S._BirthDate); }
				}

				public string ChildSurName
				{
					get { return m_ChildSurName_; }
					set { SetPropValue(value, ref m_ChildSurName_, S._ChildSurName); }
				}

				public string ChildName
				{
					get { return m_ChildName_; }
					set { SetPropValue(value, ref m_ChildName_, S._ChildName); }
				}

				public string ChildEmail
				{
					get { return m_ChildEmail_; }
					set { SetPropValue(value, ref m_ChildEmail_, S._ChildEmail); }
				}

			}

			public partial class Web_Account: EntityBase<Web_Account>
			{
				public Web_Account():base() {}
				public Web_Account(IEntityHost<Web_Account> pHost):base(pHost) {}

				[GProp("mdTable", "Web_Accounts")]
				[GProp("mdTableUserName", "Реєстраційні дані учасників")]
				[GProp("mdTableKind", "2")]
				[GProp("mdTableGid", "15218")]
				[GProp("MDSchemaType", "Mixed")]
				public static class S
				{
					[GProp("mdFieldGid", "23089")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Логін")]
					[GProp("mdDataType", "varchar(255)")]
					[GProp("DataLength", "255")]
					public const string _login = "login";

					[GProp("mdFieldGid", "23091")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Адреса електронної пошти")]
					[GProp("mdDataType", "varchar(255)")]
					[GProp("DataLength", "255")]
					public const string _e_mail_address = "e_mail_address";

					[GProp("mdFieldGid", "23092")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Телефони")]
					[GProp("mdDataType", "varchar(50)")]
					[GProp("DataLength", "50")]
					public const string _phones = "phones";

					[GProp("mdFieldGid", "23093")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Коментар")]
					[GProp("mdDataType", "varchar(MAX)")]
					public const string _comments = "comments";

					[GProp("mdFieldGid", "23096")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Додаткові дані")]
					[GProp("mdDataType", "varchar(MAX)")]
					public const string _AdditionalInfo = "AdditionalInfo";

					[GProp("mdFieldGid", "23112")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Пароль")]
					[GProp("mdDataType", "varchar(255)")]
					[GProp("DataLength", "255")]
					public const string _password_code = "password_code";

					[GProp("mdFieldGid", "26003")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Глобальний ідентифікатор")]
					[GProp("mdDataType", "GUIDs")]
					public const string _Guid = "Guid";

					[GProp("mdFieldGid", "26007")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Дата народження")]
					[GProp("mdDataType", "date")]
					public const string _BirthDate = "BirthDate";

					[GProp("mdFieldGid", "26008")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Стать")]
					[GProp("mdDataType", "Gender")]
					public const string _Gender = "Gender";

					[GProp("mdFieldGid", "26009")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "По Батькові")]
					[GProp("mdDataType", "varchar(100)")]
					[GProp("DataLength", "100")]
					public const string _Middle_name = "Middle_name";

					[GProp("mdFieldGid", "26010")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Ім'я")]
					[GProp("mdDataType", "varchar(100)")]
					[GProp("DataLength", "100")]
					public const string _FirstName = "FirstName";

					public const string _Surname = "Surname";

					public const string _ExtraData = "ExtraData";

					[GProp("mdFieldGid", "34618")]
					public const string _InviteCode = "InviteCode";

					[GProp("mdFieldGid", "23088")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Ідентифікатор")]
					[GProp("mdDataType", "int")]
					public const string _gid = "gid";

					[GProp("mdFieldGid", "23090")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Повне ім'я")]
					[GProp("mdDataType", "nvarchar(255)")]
					[GProp("DataLength", "255")]
					public const string _Full_Name = "Full_Name";

					[GProp("mdFieldGid", "23095")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Статус")]
					[GProp("mdDataType", "Object_statuses_reference")]
					[GProp("mdReference", "Object_statuses")]
					public const string _object_status = "object_status";

					[GProp("mdFieldGid", "31771")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Особа - влавник облікового запису")]
					[GProp("mdDataType", "Persons_reference")]
					[GProp("mdReference", "Persons")]
					public const string _OwnerPerson = "OwnerPerson";

					[GProp("mdFieldGid", "32034")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Електронна адреса підтверджена")]
					[GProp("mdDataType", "bool")]
					public const string _EMailConfirmed = "EMailConfirmed";

					[GProp("mdFieldGid", "33059")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Основний номер телефону")]
					[GProp("mdDataType", "varchar(16)")]
					[GProp("DataLength", "16")]
					public const string _MainPhone = "MainPhone";

					public const string _RequestCode = "RequestCode";
				}

				#region fields of Web_Account
				string m_login_;
				string m_e_mail_address_;
				string m_phones_;
				string m_comments_;
				string m_AdditionalInfo_;
				string m_password_code_;
				string m_Guid_;
				DateTime m_BirthDate_;
				int m_Gender_;
				string m_Middle_name_;
				string m_FirstName_;
				string m_Surname_;
				byte[] m_ExtraData_;
				string m_InviteCode_;
				int m_gid_;
				string m_Full_Name_;
				int m_object_status_;
				int m_OwnerPerson_;
				bool m_EMailConfirmed_;
				string m_MainPhone_;
				string m_RequestCode_;
				#endregion fields of Web_Account

				public string login
				{
					get { return m_login_; }
					set { SetPropValue(value, ref m_login_, S._login); }
				}

				public string e_mail_address
				{
					get { return m_e_mail_address_; }
					set { SetPropValue(value, ref m_e_mail_address_, S._e_mail_address); }
				}

				public string phones
				{
					get { return m_phones_; }
					set { SetPropValue(value, ref m_phones_, S._phones); }
				}

				public string comments
				{
					get { return m_comments_; }
					set { SetPropValue(value, ref m_comments_, S._comments); }
				}

				public string AdditionalInfo
				{
					get { return m_AdditionalInfo_; }
					set { SetPropValue(value, ref m_AdditionalInfo_, S._AdditionalInfo); }
				}

				public string password_code
				{
					get { return m_password_code_; }
					set { SetPropValue(value, ref m_password_code_, S._password_code); }
				}

				public string Guid
				{
					get { return m_Guid_; }
					set { SetPropValue(value, ref m_Guid_, S._Guid); }
				}

				public DateTime BirthDate
				{
					get { return m_BirthDate_; }
					set { SetPropValue(value, ref m_BirthDate_, S._BirthDate); }
				}

				public int Gender
				{
					get { return m_Gender_; }
					set { SetPropValue(value, ref m_Gender_, S._Gender); }
				}

				public string Middle_name
				{
					get { return m_Middle_name_; }
					set { SetPropValue(value, ref m_Middle_name_, S._Middle_name); }
				}

				public string FirstName
				{
					get { return m_FirstName_; }
					set { SetPropValue(value, ref m_FirstName_, S._FirstName); }
				}

				public string Surname
				{
					get { return m_Surname_; }
					set { SetPropValue(value, ref m_Surname_, S._Surname); }
				}

				public byte[] ExtraData
				{
					get { return m_ExtraData_; }
					set { SetPropValue(value, ref m_ExtraData_, S._ExtraData); }
				}

				public string InviteCode
				{
					get { return m_InviteCode_; }
					set { SetPropValue(value, ref m_InviteCode_, S._InviteCode); }
				}

				public int gid
				{
					get { return m_gid_; }
					set { SetPropValue(value, ref m_gid_, S._gid); }
				}

				public string Full_Name
				{
					get { return m_Full_Name_; }
					set { SetPropValue(value, ref m_Full_Name_, S._Full_Name); }
				}

				public int object_status
				{
					get { return m_object_status_; }
					set { SetPropValue(value, ref m_object_status_, S._object_status); }
				}

				public int OwnerPerson
				{
					get { return m_OwnerPerson_; }
					set { SetPropValue(value, ref m_OwnerPerson_, S._OwnerPerson); }
				}

				public bool EMailConfirmed
				{
					get { return m_EMailConfirmed_; }
					set { SetPropValue(value, ref m_EMailConfirmed_, S._EMailConfirmed); }
				}

				public string MainPhone
				{
					get { return m_MainPhone_; }
					set { SetPropValue(value, ref m_MainPhone_, S._MainPhone); }
				}

				public string RequestCode
				{
					get { return m_RequestCode_; }
					set { SetPropValue(value, ref m_RequestCode_, S._RequestCode); }
				}

			}

			public partial class RequestPasswordReset: EntityBase<RequestPasswordReset>
			{
				public RequestPasswordReset():base() {}
				public RequestPasswordReset(IEntityHost<RequestPasswordReset> pHost):base(pHost) {}

				[GProp("HttpAction", "RequestPasswordReset")]
				[GProp("ResultType", "1")]
				[GProp("mdTable", "Web_Requests")]
				public static class S
				{
					[GProp("mdFieldGid", "32025")]
					public const string _EmailAddress = "EmailAddress";

					[GProp("mdFieldGid", "32027")]
					public const string _WebAccountID = "WebAccountID";

					public const string _PhoneNo = "PhoneNo";
				}

				#region fields of RequestPasswordReset
				string m_EmailAddress_;
				int m_WebAccountID_;
				string m_PhoneNo_;
				#endregion fields of RequestPasswordReset

				public string EmailAddress
				{
					get { return m_EmailAddress_; }
					set { SetPropValue(value, ref m_EmailAddress_, S._EmailAddress); }
				}

				public int WebAccountID
				{
					get { return m_WebAccountID_; }
					set { SetPropValue(value, ref m_WebAccountID_, S._WebAccountID); }
				}

				public string PhoneNo
				{
					get { return m_PhoneNo_; }
					set { SetPropValue(value, ref m_PhoneNo_, S._PhoneNo); }
				}

			}

			public partial class ConfirmEmailResult: EntityBase<ConfirmEmailResult>
			{
				public ConfirmEmailResult():base() {}
				public ConfirmEmailResult(IEntityHost<ConfirmEmailResult> pHost):base(pHost) {}

				[GProp("HttpResultFor", "ConfirmEmail")]
				[GProp("ResultType", "1")]
				[GProp("mdTable", "Web_Requests")]
				public static class S
				{
					public const string _EmailAddress = "EmailAddress";

					[GProp("mdFieldGid", "32027")]
					public const string _WebAccountID = "WebAccountID";

					public const string _RequestCode = "RequestCode";

					[GProp("mdFieldGid", "32029")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Дата, час відповіді")]
					[GProp("mdDataType", "datetime")]
					public const string _ResponseTime = "ResponseTime";

					[GProp("mdFieldGid", "32030")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Код відповіді")]
					[GProp("mdDataType", "varchar(255)")]
					[GProp("DataLength", "255")]
					public const string _ResponseCode = "ResponseCode";

					[GProp("mdFieldGid", "32032")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Статус")]
					[GProp("mdDataType", "Object_statuses_reference")]
					[GProp("mdReference", "Object_statuses")]
					public const string _object_status = "object_status";
				}

				#region fields of ConfirmEmailResult
				string m_EmailAddress_;
				int m_WebAccountID_;
				string m_RequestCode_;
				DateTime m_ResponseTime_;
				string m_ResponseCode_;
				int m_object_status_;
				#endregion fields of ConfirmEmailResult

				public string EmailAddress
				{
					get { return m_EmailAddress_; }
					set { SetPropValue(value, ref m_EmailAddress_, S._EmailAddress); }
				}

				public int WebAccountID
				{
					get { return m_WebAccountID_; }
					set { SetPropValue(value, ref m_WebAccountID_, S._WebAccountID); }
				}

				public string RequestCode
				{
					get { return m_RequestCode_; }
					set { SetPropValue(value, ref m_RequestCode_, S._RequestCode); }
				}

				public DateTime ResponseTime
				{
					get { return m_ResponseTime_; }
					set { SetPropValue(value, ref m_ResponseTime_, S._ResponseTime); }
				}

				public string ResponseCode
				{
					get { return m_ResponseCode_; }
					set { SetPropValue(value, ref m_ResponseCode_, S._ResponseCode); }
				}

				public int object_status
				{
					get { return m_object_status_; }
					set { SetPropValue(value, ref m_object_status_, S._object_status); }
				}

			}

			public partial class EMailMessage: EntityBase<EMailMessage>
			{
				public EMailMessage():base() {}
				public EMailMessage(IEntityHost<EMailMessage> pHost):base(pHost) {}

				public static class S
				{
					public const string _ReceiverAddress = "ReceiverAddress";

					public const string _ReceiverName = "ReceiverName";

					public const string _Subject = "Subject";

					[GProp("IsHtml", "1")]
					public const string _Body = "Body";
				}

				#region fields of EMailMessage
				string m_ReceiverAddress_;
				string m_ReceiverName_;
				string m_Subject_;
				string m_Body_;
				#endregion fields of EMailMessage

				public string ReceiverAddress
				{
					get { return m_ReceiverAddress_; }
					set { SetPropValue(value, ref m_ReceiverAddress_, S._ReceiverAddress); }
				}

				public string ReceiverName
				{
					get { return m_ReceiverName_; }
					set { SetPropValue(value, ref m_ReceiverName_, S._ReceiverName); }
				}

				public string Subject
				{
					get { return m_Subject_; }
					set { SetPropValue(value, ref m_Subject_, S._Subject); }
				}

				public string Body
				{
					get { return m_Body_; }
					set { SetPropValue(value, ref m_Body_, S._Body); }
				}

			}
		}

		#region fields of UserEventsSet
		EntitiesCollection<S.RegisterAccount> m_RegisterAccount_;
		EntitiesCollection<S.Web_Account> m_Web_Accounts_;
		S.RequestPasswordReset m_RequestPasswordReset_;
		S.ConfirmEmailResult m_ConfirmEmailResult_;
		S.EMailMessage m_EMailMessage_;
		#endregion fields of UserEventsSet

		public EntitiesCollection<S.RegisterAccount> RegisterAccount
		{
			get
			{ 
				if (m_RegisterAccount_ == null)
				{
					lock(this)
					{
						if (m_RegisterAccount_ == null)
							m_RegisterAccount_ = new EntitiesCollection<S.RegisterAccount>(GetHost<S.RegisterAccount>());
					}
				}
				return m_RegisterAccount_;
			}
		}

		public EntitiesCollection<S.Web_Account> Web_Accounts
		{
			get
			{ 
				if (m_Web_Accounts_ == null)
				{
					lock(this)
					{
						if (m_Web_Accounts_ == null)
							m_Web_Accounts_ = new EntitiesCollection<S.Web_Account>(GetHost<S.Web_Account>());
					}
				}
				return m_Web_Accounts_;
			}
		}

		public S.RequestPasswordReset RequestPasswordReset
		{
			get
			{ 
				if (m_RequestPasswordReset_ == null)
				{
					lock(this)
					{
						if (m_RequestPasswordReset_ == null)
							m_RequestPasswordReset_ = new S.RequestPasswordReset(GetHost<S.RequestPasswordReset>());
					}
				}
				return m_RequestPasswordReset_;
			}
		}

		public S.ConfirmEmailResult ConfirmEmailResult
		{
			get
			{ 
				if (m_ConfirmEmailResult_ == null)
				{
					lock(this)
					{
						if (m_ConfirmEmailResult_ == null)
							m_ConfirmEmailResult_ = new S.ConfirmEmailResult(GetHost<S.ConfirmEmailResult>());
					}
				}
				return m_ConfirmEmailResult_;
			}
		}

		public S.EMailMessage EMailMessage
		{
			get
			{ 
				if (m_EMailMessage_ == null)
				{
					lock(this)
					{
						if (m_EMailMessage_ == null)
							m_EMailMessage_ = new S.EMailMessage(GetHost<S.EMailMessage>());
					}
				}
				return m_EMailMessage_;
			}
		}

	}
}

