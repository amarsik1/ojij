﻿using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using appcore.DataProcessing;
using ADE.EntityModel;

namespace Entity_Schema.Education {

	[DataCacheMode("Domain")]
	public partial class GroupsInfoSet: EntityBase<GroupsInfoSet>, ICachedEntity
	{
		public GroupsInfoSet():base() {}
		public GroupsInfoSet(IEntityHost<GroupsInfoSet> pHost):base(pHost) {}

		public static class S
		{
			[GProp("mdTable", "EduGroups")]
			[GProp("mdTableUserName", "Навчальні групи")]
			[GProp("mdTableKind", "2")]
			[GProp("mdTableGid", "15900")]
			[GProp("AutoLoad", "1")]
			[GProp("Filter", "Active")]
			[GProp("CacheMode", "Domain")]
			public const string _EduGroups = "EduGroups";

			[GProp("mdTable", "EduStudents")]
			[GProp("mdTableUserName", "Учні навчальних класів")]
			[GProp("mdTableKind", "2")]
			[GProp("mdTableGid", "15903")]
			[GProp("ExtDataProvider", "ARI")]
			[GProp("CacheMode", "Domain")]
			public const string _EduStudents = "EduStudents";

			[GProp("mdTable", "Persons")]
			[GProp("mdTableUserName", "Персони")]
			[GProp("mdTableKind", "2")]
			[GProp("mdTableGid", "15738")]
			[GProp("CacheMode", "Domain")]
			[GProp("ExtDataProvider", "DRI")]
			public const string _Persons = "Persons";

			public const string _PRequset = "PRequset";

			[GProp("mdTable", "GroupJoiningRequests")]
			[GProp("mdTableUserName", "Запити на приєднання до груп")]
			[GProp("mdTableKind", "9")]
			[GProp("mdTableGid", "15784")]
			public const string _GroupJoiningRequests = "GroupJoiningRequests";

			public partial class EduGroup: RefEntityBase<EduGroup,int>
			{
				public EduGroup():base() {}
				public EduGroup(IEntityHost<EduGroup> pHost):base(pHost) {}

				[GProp("mdTable", "EduGroups")]
				[GProp("mdTableUserName", "Навчальні групи")]
				[GProp("mdTableKind", "2")]
				[GProp("mdTableGid", "15900")]
				[GProp("AutoLoad", "1")]
				[GProp("Filter", "Active")]
				[GProp("CacheMode", "Domain")]
				public static class S
				{
					[GProp("mdFieldGid", "34067")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Ідентифікатор")]
					[GProp("mdDataType", "int")]
					public const string _ID = "gid";

					[GProp("mdFieldGid", "34069")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Назва")]
					[GProp("mdDataType", "varchar(255)")]
					[GProp("DataLength", "255")]
					public const string _name = "name";

					[GProp("mdFieldGid", "34072")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Клас навчання")]
					[GProp("mdDataType", "tinyint")]
					public const string _GroupLevel = "GroupLevel";

					[GProp("mdFieldGid", "34073")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Рік початку навчання")]
					[GProp("mdDataType", "smallint")]
					public const string _StartYear = "StartYear";

					[GProp("mdFieldGid", "34068")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Статус")]
					[GProp("mdDataType", "Object_statuses_reference")]
					[GProp("mdReference", "Object_statuses")]
					public const string _object_status = "object_status";

					[GProp("mdFieldGid", "34070")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Коментар")]
					[GProp("mdDataType", "varchar(MAX)")]
					[GProp("DataLength", "0")]
					public const string _comments = "comments";

					[GProp("mdFieldGid", "34071")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Номер версії")]
					[GProp("mdDataType", "int")]
					public const string _version_no = "version_no";

					[GProp("mdFieldGid", "34495")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Основний викладач")]
					[GProp("mdDataType", "Persons_reference")]
					[GProp("mdReference", "Persons")]
					[GProp("Reference", "Person")]
					public const string _ResponsibleTeacher = "ResponsibleTeacher";

					[GProp("mdTable", "EduGroupTeachers")]
					[GProp("mdTableUserName", "Вчителі навчальних груп")]
					[GProp("mdTableKind", "7")]
					[GProp("mdTableGid", "15928")]
					public const string _Teachers = "Teachers";

					[GProp("mdFieldGid", "34561")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Вид групи")]
					[GProp("mdDataType", "EduGroupKinds_reference")]
					[GProp("mdReference", "EduGroupKinds")]
					public const string _GroupKind = "GroupKind";

					[GProp("mdFieldGid", "34563")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Група контактиних осіб")]
					[GProp("mdDataType", "PersonGroups_reference")]
					[GProp("mdReference", "PersonsGroups")]
					public const string _PersonsGroup = "PersonsGroup";

					public partial class Teacher: EntityBase<Teacher>
					{
						public Teacher():base() {}
						public Teacher(IEntityHost<Teacher> pHost):base(pHost) {}

						[GProp("mdTable", "EduGroupTeachers")]
						[GProp("mdTableUserName", "Вчителі навчальних груп")]
						[GProp("mdTableKind", "7")]
						[GProp("mdTableGid", "15928")]
						public static class S
						{
							[GProp("mdFieldGid", "34408")]
							[GProp("mdFieldKind", "0")]
							[GProp("mdFieldUserName", "Особа вчителя")]
							[GProp("mdDataType", "Persons_reference")]
							[GProp("mdReference", "Persons")]
							[GProp("Reference", "Persons")]
							public const string _Person = "Person";

							[GProp("mdFieldGid", "34410")]
							[GProp("mdFieldKind", "0")]
							[GProp("mdFieldUserName", "Навчальний клас")]
							[GProp("mdDataType", "EduGroup_reference")]
							[GProp("mdReference", "EduGroup")]
							[GProp("Reference", "EduGroup")]
							public const string _Group = "Group";

							[GProp("mdFieldGid", "34412")]
							[GProp("mdFieldKind", "0")]
							[GProp("mdFieldUserName", "Коментар")]
							[GProp("mdDataType", "varchar(255)")]
							[GProp("DataLength", "255")]
							public const string _comments = "comments";
						}

						#region fields of Teacher
						int m_Person_;
						int m_Group_;
						string m_comments_;
						#endregion fields of Teacher

						public int Person
						{
							get { return m_Person_; }
							set { SetPropValue(value, ref m_Person_, S._Person); }
						}
						public Person Person_Ref()
						{
							return GetRefEntity<Person,int>(m_Person_);
						}

						public int Group
						{
							get { return m_Group_; }
							set { SetPropValue(value, ref m_Group_, S._Group); }
						}

						public string comments
						{
							get { return m_comments_; }
							set { SetPropValue(value, ref m_comments_, S._comments); }
						}

					}
				}

				#region fields of EduGroup
				int m_gid_;
				string m_name_;
				int m_GroupLevel_;
				int m_StartYear_;
				int m_object_status_;
				string m_comments_;
				int m_version_no_;
				int m_ResponsibleTeacher_;
				EntitiesCollection<S.Teacher> m_Teachers_;
				int m_GroupKind_;
				int m_PersonsGroup_;
				#endregion fields of EduGroup

				public override int ID
				{
					get { return m_gid_; }
					set
					{
						if (m_gid_>0 && m_gid_ != value)
							throw new AccessViolationException("Changing persistent ID value is denied!");
						SetPropValue(value, ref m_gid_, S._ID); }
				}

				public string name
				{
					get { return m_name_; }
					set { SetPropValue(value, ref m_name_, S._name); }
				}

				public int GroupLevel
				{
					get { return m_GroupLevel_; }
					set { SetPropValue(value, ref m_GroupLevel_, S._GroupLevel); }
				}

				public int StartYear
				{
					get { return m_StartYear_; }
					set { SetPropValue(value, ref m_StartYear_, S._StartYear); }
				}

				public int object_status
				{
					get { return m_object_status_; }
					set { SetPropValue(value, ref m_object_status_, S._object_status); }
				}

				public string comments
				{
					get { return m_comments_; }
					set { SetPropValue(value, ref m_comments_, S._comments); }
				}

				public int version_no
				{
					get { return m_version_no_; }
					set { SetPropValue(value, ref m_version_no_, S._version_no); }
				}

				public int ResponsibleTeacher
				{
					get { return m_ResponsibleTeacher_; }
					set { SetPropValue(value, ref m_ResponsibleTeacher_, S._ResponsibleTeacher); }
				}

				public EntitiesCollection<S.Teacher> Teachers
				{
					get
					{ 
						if (m_Teachers_ == null)
						{
							lock(this)
							{
								if (m_Teachers_ == null)
									m_Teachers_ = new EntitiesCollection<S.Teacher>(GetHost<S.Teacher>());
							}
						}
						return m_Teachers_;
					}
				}

				public int GroupKind
				{
					get { return m_GroupKind_; }
					set { SetPropValue(value, ref m_GroupKind_, S._GroupKind); }
				}

				public int PersonsGroup
				{
					get { return m_PersonsGroup_; }
					set { SetPropValue(value, ref m_PersonsGroup_, S._PersonsGroup); }
				}


				public class Collection_ : AutoReLoadCollection<EduGroup,int> 
				{
					public Collection_(IEntityHost<EduGroup> pHost, IDataProxy pProxy = null, bool pActiveItemsOnly = true)
						:base(pHost, pProxy, pActiveItemsOnly)
					{
					}
				}
			}

			public partial class EduStudent: EntityBase<EduStudent>
			{
				public EduStudent():base() {}
				public EduStudent(IEntityHost<EduStudent> pHost):base(pHost) {}

				[GProp("mdTable", "EduStudents")]
				[GProp("mdTableUserName", "Учні навчальних класів")]
				[GProp("mdTableKind", "2")]
				[GProp("mdTableGid", "15903")]
				[GProp("ExtDataProvider", "ARI")]
				[GProp("CacheMode", "Domain")]
				public static class S
				{
					[GProp("mdFieldGid", "34444")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Статус")]
					[GProp("mdDataType", "Object_statuses_reference")]
					[GProp("mdReference", "Object_statuses")]
					public const string _object_status = "object_status";

					[GProp("mdFieldGid", "34089")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Особа учня")]
					[GProp("mdDataType", "Persons_reference")]
					[GProp("mdReference", "Persons")]
					[GProp("Reference", "Person")]
					public const string _Person = "Person";

					[GProp("mdFieldGid", "34090")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Навчальна група")]
					[GProp("mdDataType", "EduGroup_reference")]
					[GProp("mdReference", "EduGroup")]
					[GProp("Reference", "EduGroup")]
					public const string _Group = "Group";

					[GProp("mdFieldGid", "34093")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Коментар")]
					[GProp("mdDataType", "varchar(255)")]
					[GProp("DataLength", "255")]
					public const string _comments = "comments";
				}

				#region fields of EduStudent
				int m_object_status_;
				int m_Person_;
				int m_Group_;
				string m_comments_;
				#endregion fields of EduStudent

				public int object_status
				{
					get { return m_object_status_; }
					set { SetPropValue(value, ref m_object_status_, S._object_status); }
				}

				public int Person
				{
					get { return m_Person_; }
					set { SetPropValue(value, ref m_Person_, S._Person); }
				}

				public int Group
				{
					get { return m_Group_; }
					set { SetPropValue(value, ref m_Group_, S._Group); }
				}

				public string comments
				{
					get { return m_comments_; }
					set { SetPropValue(value, ref m_comments_, S._comments); }
				}


				public class Collection_ : EntitiesCollection<EduStudent>
				{
					public MultiDataIndex<int> Group { get; private set; }
					public MultiDataIndex<int> Person { get; private set; }

					public Collection_(IEntityHost<EduStudent> pHost)
						:base(pHost)
					{
						Group = AddMultiDataIndex(x=>x.Group, S._Group);
						Person = AddMultiDataIndex(x=>x.Person, S._Person);
					}
				}
			}

			public partial class Person: RefEntityBase<Person,int>
			{
				public Person():base() {}
				public Person(IEntityHost<Person> pHost):base(pHost) {}

				[GProp("mdTable", "Persons")]
				[GProp("mdTableUserName", "Персони")]
				[GProp("mdTableKind", "2")]
				[GProp("mdTableGid", "15738")]
				[GProp("CacheMode", "Domain")]
				[GProp("ExtDataProvider", "DRI")]
				public static class S
				{
					[GProp("mdFieldGid", "30910")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Ідентифікатор")]
					[GProp("mdDataType", "int")]
					public const string _ID = "gid";

					[GProp("mdFieldGid", "30918")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Статус")]
					[GProp("mdDataType", "Object_statuses_reference")]
					[GProp("mdReference", "Object_statuses")]
					public const string _object_status = "object_status";

					[GProp("mdFieldGid", "30911")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Прізвище")]
					[GProp("mdDataType", "varchar(100)")]
					[GProp("DataLength", "100")]
					public const string _Surname = "Surname";

					[GProp("mdFieldGid", "30912")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Ім'я")]
					[GProp("mdDataType", "varchar(100)")]
					[GProp("DataLength", "100")]
					public const string _Name = "Name";

					[GProp("mdFieldGid", "30913")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "По Батькові")]
					[GProp("mdDataType", "varchar(100)")]
					[GProp("DataLength", "100")]
					public const string _Middle_name = "Middle_name";

					[GProp("mdFieldGid", "30914")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Cтать")]
					[GProp("mdDataType", "Gender")]
					public const string _gender = "gender";

					[GProp("mdFieldGid", "31023")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Прізвище, Ім'я, По-батькові")]
					[GProp("mdDataType", "varchar(255)")]
					[GProp("DataLength", "255")]
					public const string _Full_Name = "Full_Name";

					[GProp("mdFieldGid", "30915")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Телефони")]
					[GProp("mdDataType", "varchar(100)")]
					[GProp("DataLength", "100")]
					public const string _phones = "phones";

					[GProp("mdFieldGid", "30916")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Електронна пошта")]
					[GProp("mdDataType", "varchar(100)")]
					[GProp("DataLength", "100")]
					public const string _e_mail = "e_mail";

					[GProp("mdFieldGid", "30919")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Дата народження")]
					[GProp("mdDataType", "date")]
					public const string _birthday = "birthday";

					[GProp("mdFieldGid", "30917")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Номер версії")]
					[GProp("mdDataType", "int")]
					public const string _version_no = "version_no";
				}

				#region fields of Person
				int m_gid_;
				int m_object_status_;
				string m_Surname_;
				string m_Name_;
				string m_Middle_name_;
				int m_gender_;
				string m_Full_Name_;
				string m_phones_;
				string m_e_mail_;
				DateTime m_birthday_;
				int m_version_no_;
				#endregion fields of Person

				public override int ID
				{
					get { return m_gid_; }
					set
					{
						if (m_gid_>0 && m_gid_ != value)
							throw new AccessViolationException("Changing persistent ID value is denied!");
						SetPropValue(value, ref m_gid_, S._ID); }
				}

				public int object_status
				{
					get { return m_object_status_; }
					set { SetPropValue(value, ref m_object_status_, S._object_status); }
				}

				public string Surname
				{
					get { return m_Surname_; }
					set { SetPropValue(value, ref m_Surname_, S._Surname); }
				}

				public string Name
				{
					get { return m_Name_; }
					set { SetPropValue(value, ref m_Name_, S._Name); }
				}

				public string Middle_name
				{
					get { return m_Middle_name_; }
					set { SetPropValue(value, ref m_Middle_name_, S._Middle_name); }
				}

				public int gender
				{
					get { return m_gender_; }
					set { SetPropValue(value, ref m_gender_, S._gender); }
				}

				public string Full_Name
				{
					get { return m_Full_Name_; }
					set { SetPropValue(value, ref m_Full_Name_, S._Full_Name); }
				}

				public string phones
				{
					get { return m_phones_; }
					set { SetPropValue(value, ref m_phones_, S._phones); }
				}

				public string e_mail
				{
					get { return m_e_mail_; }
					set { SetPropValue(value, ref m_e_mail_, S._e_mail); }
				}

				public DateTime birthday
				{
					get { return m_birthday_; }
					set { SetPropValue(value, ref m_birthday_, S._birthday); }
				}

				public int version_no
				{
					get { return m_version_no_; }
					set { SetPropValue(value, ref m_version_no_, S._version_no); }
				}


				public class Collection_ : RefEntitiesCollection<Person,int> 
				{
					public Collection_(IEntityHost<Person> pHost)
						:base(pHost)
					{
					}
				}
			}

			public partial class PRequset: EntityBase<PRequset>
			{
				public PRequset():base() {}
				public PRequset(IEntityHost<PRequset> pHost):base(pHost) {}

				public static class S
				{
					[GProp("mdFieldGid", "30911")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Прізвище")]
					[GProp("mdDataType", "varchar(100)")]
					[GProp("DataLength", "100")]
					public const string _Surname = "Surname";

					public const string _Name = "Name";

					[GProp("mdFieldGid", "30916")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Електронна пошта")]
					[GProp("mdDataType", "varchar(100)")]
					[GProp("DataLength", "100")]
					public const string _e_mail = "e_mail";

					[GProp("mdFieldGid", "30915")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Телефони")]
					[GProp("mdDataType", "varchar(100)")]
					[GProp("DataLength", "100")]
					public const string _phone = "phone";
				}

				#region fields of PRequset
				string m_Surname_;
				string m_Name_;
				string m_e_mail_;
				string m_phone_;
				#endregion fields of PRequset

				public string Surname
				{
					get { return m_Surname_; }
					set { SetPropValue(value, ref m_Surname_, S._Surname); }
				}

				public string Name
				{
					get { return m_Name_; }
					set { SetPropValue(value, ref m_Name_, S._Name); }
				}

				public string e_mail
				{
					get { return m_e_mail_; }
					set { SetPropValue(value, ref m_e_mail_, S._e_mail); }
				}

				public string phone
				{
					get { return m_phone_; }
					set { SetPropValue(value, ref m_phone_, S._phone); }
				}

			}

			public partial class GroupJoiningRequest: EntityBase<GroupJoiningRequest>
			{
				public GroupJoiningRequest():base() {}
				public GroupJoiningRequest(IEntityHost<GroupJoiningRequest> pHost):base(pHost) {}

				[GProp("mdTable", "GroupJoiningRequests")]
				[GProp("mdTableUserName", "Запити на приєднання до груп")]
				[GProp("mdTableKind", "9")]
				[GProp("mdTableGid", "15784")]
				public static class S
				{
					[GProp("mdFieldGid", "31843")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "ID особи (телефон чи e-mail)")]
					[GProp("mdDataType", "varchar(255)")]
					[GProp("DataLength", "255")]
					public const string _JoiningPersonID = "JoiningPersonID";

					[GProp("mdFieldGid", "31774")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Реєстраційні дані потенційного учасника")]
					[GProp("mdDataType", "Web_Accounts_reference")]
					[GProp("mdReference", "Web_Accounts")]
					public const string _JoiningWebAccount = "JoiningWebAccount";

					[GProp("mdFieldGid", "31844")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Особа потенційного учасника")]
					[GProp("mdDataType", "Persons_reference")]
					[GProp("mdReference", "Persons")]
					public const string _JoiningPerson = "JoiningPerson";

					[GProp("mdFieldGid", "31773")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Група осіб")]
					[GProp("mdDataType", "PersonGroups_reference")]
					[GProp("mdReference", "PersonsGroups")]
					public const string _PersonsGroup = "PersonsGroup";

					[GProp("mdFieldGid", "31775")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Дата, час запиту")]
					[GProp("mdDataType", "datetime")]
					public const string _reg_date = "reg_date";

					[GProp("mdFieldGid", "31777")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Особа, яка прийняла рішення")]
					[GProp("mdDataType", "Persons_reference")]
					[GProp("mdReference", "Persons")]
					public const string _Approver = "Approver";

					[GProp("mdFieldGid", "31776")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Статус погодження")]
					[GProp("mdDataType", "ApproveStates")]
					public const string _State = "State";

					[GProp("mdFieldGid", "31778")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Дата, час рішення")]
					[GProp("mdDataType", "datetime")]
					public const string _DecisionTime = "DecisionTime";
				}

				#region fields of GroupJoiningRequest
				string m_JoiningPersonID_;
				int m_JoiningWebAccount_;
				int m_JoiningPerson_;
				int m_PersonsGroup_;
				DateTime m_reg_date_;
				int m_Approver_;
				int m_State_;
				DateTime m_DecisionTime_;
				#endregion fields of GroupJoiningRequest

				public string JoiningPersonID
				{
					get { return m_JoiningPersonID_; }
					set { SetPropValue(value, ref m_JoiningPersonID_, S._JoiningPersonID); }
				}

				public int JoiningWebAccount
				{
					get { return m_JoiningWebAccount_; }
					set { SetPropValue(value, ref m_JoiningWebAccount_, S._JoiningWebAccount); }
				}

				public int JoiningPerson
				{
					get { return m_JoiningPerson_; }
					set { SetPropValue(value, ref m_JoiningPerson_, S._JoiningPerson); }
				}

				public int PersonsGroup
				{
					get { return m_PersonsGroup_; }
					set { SetPropValue(value, ref m_PersonsGroup_, S._PersonsGroup); }
				}

				public DateTime reg_date
				{
					get { return m_reg_date_; }
					set { SetPropValue(value, ref m_reg_date_, S._reg_date); }
				}

				public int Approver
				{
					get { return m_Approver_; }
					set { SetPropValue(value, ref m_Approver_, S._Approver); }
				}

				public int State
				{
					get { return m_State_; }
					set { SetPropValue(value, ref m_State_, S._State); }
				}

				public DateTime DecisionTime
				{
					get { return m_DecisionTime_; }
					set { SetPropValue(value, ref m_DecisionTime_, S._DecisionTime); }
				}

			}
		}

		#region fields of GroupsInfoSet
		S.EduGroup.Collection_ m_EduGroups_;
		S.EduStudent.Collection_ m_EduStudents_;
		S.Person.Collection_ m_Persons_;
		S.PRequset m_PRequset_;
		EntitiesCollection<S.GroupJoiningRequest> m_GroupJoiningRequests_;
		#endregion fields of GroupsInfoSet

		public S.EduGroup.Collection_ EduGroups
		{
			get
			{ 
				if (m_EduGroups_ == null)
				{
					lock(this)
					{
						if (m_EduGroups_ == null)
							m_EduGroups_ = new S.EduGroup.Collection_(GetHost<S.EduGroup>());
					}
				}
				return m_EduGroups_;
			}
		}

		public S.EduStudent.Collection_ EduStudents
		{
			get
			{ 
				if (m_EduStudents_ == null)
				{
					lock(this)
					{
						if (m_EduStudents_ == null)
							m_EduStudents_ = new S.EduStudent.Collection_(GetHost<S.EduStudent>());
					}
				}
				return m_EduStudents_;
			}
		}

		public S.Person.Collection_ Persons
		{
			get
			{ 
				if (m_Persons_ == null)
				{
					lock(this)
					{
						if (m_Persons_ == null)
							m_Persons_ = new S.Person.Collection_(GetHost<S.Person>());
					}
				}
				return m_Persons_;
			}
		}

		public S.PRequset PRequset
		{
			get
			{ 
				if (m_PRequset_ == null)
				{
					lock(this)
					{
						if (m_PRequset_ == null)
							m_PRequset_ = new S.PRequset(GetHost<S.PRequset>());
					}
				}
				return m_PRequset_;
			}
		}

		public EntitiesCollection<S.GroupJoiningRequest> GroupJoiningRequests
		{
			get
			{ 
				if (m_GroupJoiningRequests_ == null)
				{
					lock(this)
					{
						if (m_GroupJoiningRequests_ == null)
							m_GroupJoiningRequests_ = new EntitiesCollection<S.GroupJoiningRequest>(GetHost<S.GroupJoiningRequest>());
					}
				}
				return m_GroupJoiningRequests_;
			}
		}

		public Dictionary<IDBRequestsClient, bool> GetDBRequestsClients(string pCacheMode)
		{
			return new Dictionary<IDBRequestsClient, bool> {{EduStudents.Group, true}, {EduStudents.Person, true}, {Persons.PrimaryKey, false}};
		}
		partial void AfterAddedToCache(string pCacheMode, IDataProxy pDataProxy);
		public void OnAddedToCache(string pCacheMode, IDataProxy pDataProxy, IDataCollector pCollector)
		{
			if (pCacheMode == "Domain")
			{
				m_EduGroups_ = new S.EduGroup.Collection_(GetHost<S.EduGroup>(), pDataProxy,true);
				pCollector.AddCollection(EduGroups);
				pCollector.AddCollection(Persons);
			}
			AfterAddedToCache(pCacheMode, pDataProxy);
		}
	}
}

