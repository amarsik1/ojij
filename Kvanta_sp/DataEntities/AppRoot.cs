﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ADE.EntityModel;

namespace Entity_Schema.Education
{
    [ViewTemplate("Cabinet")]
    public class AppRoot : EntityBase<AppRoot>
    {
        public GroupsInfoSet.S.EduGroup[] Groups { get; set; }
    }

    public class DataRequest : EntityBase<DataRequest>
    {

    }

    public class TaskListsRequest : EntityBase<TaskListsRequest>
    {
        public int GroupId { get; set; }
    }

    public class TaskList : EntityBase<TaskList>
    {

    }

    
}