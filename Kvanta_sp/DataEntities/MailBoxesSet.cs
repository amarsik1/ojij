﻿using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using appcore.DataProcessing;
using ADE.EntityModel;

namespace Entity_Schema.MailingInfo {

	[DataCacheMode("Domain")]
	public partial class MailBoxesSet: EntityBase<MailBoxesSet>, ICachedEntity
	{
		public MailBoxesSet():base() {}
		public MailBoxesSet(IEntityHost<MailBoxesSet> pHost):base(pHost) {}

		public static class S
		{
			[GProp("mdTable", "e_mail_boxes")]
			[GProp("mdTableUserName", "Електронні скриньки")]
			[GProp("mdTableKind", "2")]
			[GProp("mdTableGid", "2142")]
			[GProp("AutoLoad", "1")]
			[GProp("CacheMode", "Domain")]
			[GProp("Filter", "Active")]
			public const string _e_mail_boxes = "e_mail_boxes";

			[GProp("mdTable", "e_mail_templates")]
			[GProp("mdTableUserName", "Шаблони електронних листів")]
			[GProp("mdTableKind", "2")]
			[GProp("mdTableGid", "2143")]
			[GProp("AutoLoad", "1")]
			[GProp("CacheMode", "Domain")]
			[GProp("Filter", "Active")]
			public const string _e_mail_templates = "e_mail_templates";

			public partial class e_mail_boxe: RefEntityBase<e_mail_boxe,int>
			{
				public e_mail_boxe():base() {}
				public e_mail_boxe(IEntityHost<e_mail_boxe> pHost):base(pHost) {}

				[GProp("mdTable", "e_mail_boxes")]
				[GProp("mdTableUserName", "Електронні скриньки")]
				[GProp("mdTableKind", "2")]
				[GProp("mdTableGid", "2142")]
				[GProp("AutoLoad", "1")]
				[GProp("CacheMode", "Domain")]
				[GProp("Filter", "Active")]
				public static class S
				{
					[GProp("mdFieldGid", "8689")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Ідентифікатор")]
					[GProp("mdDataType", "smallint")]
					public const string _ID = "gid";

					[GProp("mdFieldGid", "8702")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Статус об'єкту")]
					[GProp("mdDataType", "Object_statuses_reference")]
					[GProp("mdReference", "Object_statuses")]
					public const string _object_status = "object_status";

					[GProp("mdFieldGid", "8688")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Електронна адреса")]
					[GProp("mdDataType", "varchar(255)")]
					[GProp("DataLength", "255")]
					public const string _e_mail = "e_mail";

					[GProp("mdFieldGid", "32310")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Назва відправника")]
					[GProp("mdDataType", "nvarchar(255)")]
					[GProp("DataLength", "255")]
					public const string _sender_name = "sender_name";

					[GProp("mdFieldGid", "8732")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Логін")]
					[GProp("mdDataType", "varchar(255)")]
					[GProp("DataLength", "255")]
					public const string _account_login = "account_login";

					[GProp("mdFieldGid", "8733")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Пароль")]
					[GProp("mdDataType", "varchar(255)")]
					[GProp("DataLength", "255")]
					public const string _account_password = "account_password";

					[GProp("mdFieldGid", "8687")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Порт")]
					[GProp("mdDataType", "int")]
					public const string _port = "port";

					[GProp("mdFieldGid", "8686")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "smtp сервер")]
					[GProp("mdDataType", "varchar(255)")]
					[GProp("DataLength", "255")]
					public const string _smtp_server = "smtp_server";

					[GProp("mdFieldGid", "8799")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Перевірка автентичності")]
					[GProp("mdDataType", "bit")]
					public const string _authentication = "authentication";

					[GProp("mdFieldGid", "8798")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "SSL-шифрування")]
					[GProp("mdDataType", "bit")]
					public const string _SSL = "SSL";
				}

				#region fields of e_mail_boxe
				int m_gid_;
				int m_object_status_;
				string m_e_mail_;
				string m_sender_name_;
				string m_account_login_;
				string m_account_password_;
				int m_port_;
				string m_smtp_server_;
				bool m_authentication_;
				bool m_SSL_;
				#endregion fields of e_mail_boxe

				public override int ID
				{
					get { return m_gid_; }
					set
					{
						if (m_gid_>0 && m_gid_ != value)
							throw new AccessViolationException("Changing persistent ID value is denied!");
						SetPropValue(value, ref m_gid_, S._ID); }
				}

				public int object_status
				{
					get { return m_object_status_; }
					set { SetPropValue(value, ref m_object_status_, S._object_status); }
				}

				public string e_mail
				{
					get { return m_e_mail_; }
					set { SetPropValue(value, ref m_e_mail_, S._e_mail); }
				}

				public string sender_name
				{
					get { return m_sender_name_; }
					set { SetPropValue(value, ref m_sender_name_, S._sender_name); }
				}

				public string account_login
				{
					get { return m_account_login_; }
					set { SetPropValue(value, ref m_account_login_, S._account_login); }
				}

				public string account_password
				{
					get { return m_account_password_; }
					set { SetPropValue(value, ref m_account_password_, S._account_password); }
				}

				public int port
				{
					get { return m_port_; }
					set { SetPropValue(value, ref m_port_, S._port); }
				}

				public string smtp_server
				{
					get { return m_smtp_server_; }
					set { SetPropValue(value, ref m_smtp_server_, S._smtp_server); }
				}

				public bool authentication
				{
					get { return m_authentication_; }
					set { SetPropValue(value, ref m_authentication_, S._authentication); }
				}

				public bool SSL
				{
					get { return m_SSL_; }
					set { SetPropValue(value, ref m_SSL_, S._SSL); }
				}


				public class Collection_ : AutoReLoadCollection<e_mail_boxe,int> 
				{
					public Collection_(IEntityHost<e_mail_boxe> pHost, IDataProxy pProxy = null, bool pActiveItemsOnly = true)
						:base(pHost, pProxy, pActiveItemsOnly)
					{
					}
				}
			}

			public partial class e_mail_template: RefEntityBase<e_mail_template,int>
			{
				public e_mail_template():base() {}
				public e_mail_template(IEntityHost<e_mail_template> pHost):base(pHost) {}

				[GProp("mdTable", "e_mail_templates")]
				[GProp("mdTableUserName", "Шаблони електронних листів")]
				[GProp("mdTableKind", "2")]
				[GProp("mdTableGid", "2143")]
				[GProp("AutoLoad", "1")]
				[GProp("CacheMode", "Domain")]
				[GProp("Filter", "Active")]
				public static class S
				{
					[GProp("mdFieldGid", "8691")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Ідентифікатор")]
					[GProp("mdDataType", "int")]
					public const string _ID = "gid";

					[GProp("mdFieldGid", "8704")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Статус")]
					[GProp("mdDataType", "Object_statuses_reference")]
					[GProp("mdReference", "Object_statuses")]
					public const string _object_status = "object_status";

					[GProp("mdFieldGid", "8690")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Назва")]
					[GProp("mdDataType", "varchar(255)")]
					[GProp("DataLength", "255")]
					public const string _name = "name";

					[GProp("mdFieldGid", "8734")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Шаблон теми листа")]
					[GProp("mdDataType", "varchar(255)")]
					[GProp("DataLength", "255")]
					public const string _subject_template = "subject_template";

					[GProp("mdFieldGid", "8737")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Шаблон тіла листа")]
					[GProp("mdDataType", "HTML_text")]
					[GProp("DataLength", "0")]
					public const string _Template = "Template";

					[GProp("mdFieldGid", "6695")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Тип електронного листа")]
					[GProp("mdDataType", "E_mail_letter_types")]
					public const string _E_mail_letter_type = "E_mail_letter_type";

					[GProp("mdFieldGid", "6700")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Основна поштова скринька")]
					[GProp("mdDataType", "e_mail_boxes_reference")]
					[GProp("mdReference", "e_mail_boxes")]
					public const string _base_mail_box = "base_mail_box";

					[GProp("mdFieldGid", "6693")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Налагодження")]
					[GProp("mdDataType", "varchar(MAX)")]
					[GProp("DataLength", "0")]
					public const string _Settings = "Settings";

					[GProp("mdFieldGid", "6694")]
					[GProp("mdFieldKind", "0")]
					[GProp("mdFieldUserName", "Коментар")]
					[GProp("mdDataType", "varchar(MAX)")]
					[GProp("DataLength", "0")]
					public const string _comments = "comments";
				}

				#region fields of e_mail_template
				int m_gid_;
				int m_object_status_;
				string m_name_;
				string m_subject_template_;
				string m_Template_;
				int m_E_mail_letter_type_;
				int m_base_mail_box_;
				string m_Settings_;
				string m_comments_;
				#endregion fields of e_mail_template

				public override int ID
				{
					get { return m_gid_; }
					set
					{
						if (m_gid_>0 && m_gid_ != value)
							throw new AccessViolationException("Changing persistent ID value is denied!");
						SetPropValue(value, ref m_gid_, S._ID); }
				}

				public int object_status
				{
					get { return m_object_status_; }
					set { SetPropValue(value, ref m_object_status_, S._object_status); }
				}

				public string name
				{
					get { return m_name_; }
					set { SetPropValue(value, ref m_name_, S._name); }
				}

				public string subject_template
				{
					get { return m_subject_template_; }
					set { SetPropValue(value, ref m_subject_template_, S._subject_template); }
				}

				public string Template
				{
					get { return m_Template_; }
					set { SetPropValue(value, ref m_Template_, S._Template); }
				}

				public int E_mail_letter_type
				{
					get { return m_E_mail_letter_type_; }
					set { SetPropValue(value, ref m_E_mail_letter_type_, S._E_mail_letter_type); }
				}

				public int base_mail_box
				{
					get { return m_base_mail_box_; }
					set { SetPropValue(value, ref m_base_mail_box_, S._base_mail_box); }
				}

				public string Settings
				{
					get { return m_Settings_; }
					set { SetPropValue(value, ref m_Settings_, S._Settings); }
				}

				public string comments
				{
					get { return m_comments_; }
					set { SetPropValue(value, ref m_comments_, S._comments); }
				}


				public class Collection_ : AutoReLoadCollection<e_mail_template,int> 
				{
					public Collection_(IEntityHost<e_mail_template> pHost, IDataProxy pProxy = null, bool pActiveItemsOnly = true)
						:base(pHost, pProxy, pActiveItemsOnly)
					{
					}
				}
			}
		}

		#region fields of MailBoxesSet
		S.e_mail_boxe.Collection_ m_e_mail_boxes_;
		S.e_mail_template.Collection_ m_e_mail_templates_;
		#endregion fields of MailBoxesSet

		public S.e_mail_boxe.Collection_ e_mail_boxes
		{
			get
			{ 
				if (m_e_mail_boxes_ == null)
				{
					lock(this)
					{
						if (m_e_mail_boxes_ == null)
							m_e_mail_boxes_ = new S.e_mail_boxe.Collection_(GetHost<S.e_mail_boxe>());
					}
				}
				return m_e_mail_boxes_;
			}
		}

		public S.e_mail_template.Collection_ e_mail_templates
		{
			get
			{ 
				if (m_e_mail_templates_ == null)
				{
					lock(this)
					{
						if (m_e_mail_templates_ == null)
							m_e_mail_templates_ = new S.e_mail_template.Collection_(GetHost<S.e_mail_template>());
					}
				}
				return m_e_mail_templates_;
			}
		}

		public Dictionary<IDBRequestsClient, bool> GetDBRequestsClients(string pCacheMode)
		{
			return null;
		}
		partial void AfterAddedToCache(string pCacheMode, IDataProxy pDataProxy);
		public void OnAddedToCache(string pCacheMode, IDataProxy pDataProxy, IDataCollector pCollector)
		{
			if (pCacheMode == "Domain")
			{
				m_e_mail_boxes_ = new S.e_mail_boxe.Collection_(GetHost<S.e_mail_boxe>(), pDataProxy,true);
				pCollector.AddCollection(e_mail_boxes);
				m_e_mail_templates_ = new S.e_mail_template.Collection_(GetHost<S.e_mail_template>(), pDataProxy,true);
				pCollector.AddCollection(e_mail_templates);
			}
			AfterAddedToCache(pCacheMode, pDataProxy);
		}
	}
}

