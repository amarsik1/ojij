﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace appcore
{
    public static class AppDefinition
    {
        public const short PRODUCT_ID = 121;
        public const string PRODUCT_VERSION = "1.8.15";
        public const string PRODUCT_NAME = "Kvanta";
        public const string PRODUCT_USERNAME = "КВАНТА";
        public const string PRODUCT_DESCRIPTION = "Система управління освітньою діяльністю";
        public const string DEVELOPER_COMPANY = "ТОВ \"АБСОЛЮТ СЕРВІС\"";
    }
}
