using System;
using AS_MetaData;

namespace MD_Schema
{
    /// <summary>
    /// Статична інформація про вирази в метаданих
    /// </summary>
    public static class _Expressions_SD_
    {
        public const string _BETWEEN_expName = "BETWEEN";        public const int _BETWEEN_expGid = 10;
        public static mdExpression BETWEEN
            {
            get { return Metadata.Expressions[_BETWEEN_expGid]; }
            }
        public const string _NOT_expName = "NOT";        public const int _NOT_expGid = 19;
        public static mdExpression NOT
            {
            get { return Metadata.Expressions[_NOT_expGid]; }
            }
        public const string _AND_expName = "AND";        public const int _AND_expGid = 20;
        public static mdExpression AND
            {
            get { return Metadata.Expressions[_AND_expGid]; }
            }
        public const string _OR_expName = "OR";        public const int _OR_expGid = 21;
        public static mdExpression OR
            {
            get { return Metadata.Expressions[_OR_expGid]; }
            }
        public const string _DATEADD_expName = "DATEADD";        public const int _DATEADD_expGid = 23;
        public static mdExpression DATEADD
            {
            get { return Metadata.Expressions[_DATEADD_expGid]; }
            }
        public const string _DATEDIFF_expName = "DATEDIFF";        public const int _DATEDIFF_expGid = 24;
        public static mdExpression DATEDIFF
            {
            get { return Metadata.Expressions[_DATEDIFF_expGid]; }
            }
        public const string _DATENAME_expName = "DATENAME";        public const int _DATENAME_expGid = 25;
        public static mdExpression DATENAME
            {
            get { return Metadata.Expressions[_DATENAME_expGid]; }
            }
        public const string _DATEPART_expName = "DATEPART";        public const int _DATEPART_expGid = 26;
        public static mdExpression DATEPART
            {
            get { return Metadata.Expressions[_DATEPART_expGid]; }
            }
        public const string _DAY_expName = "DAY";        public const int _DAY_expGid = 27;
        public static mdExpression DAY
            {
            get { return Metadata.Expressions[_DAY_expGid]; }
            }
        public const string _MONTH_expName = "MONTH";        public const int _MONTH_expGid = 28;
        public static mdExpression MONTH
            {
            get { return Metadata.Expressions[_MONTH_expGid]; }
            }
        public const string _YEAR_expName = "YEAR";        public const int _YEAR_expGid = 29;
        public static mdExpression YEAR
            {
            get { return Metadata.Expressions[_YEAR_expGid]; }
            }
        public const string _GETDATE_expName = "GETDATE";        public const int _GETDATE_expGid = 30;
        public static mdExpression GETDATE
            {
            get { return Metadata.Expressions[_GETDATE_expGid]; }
            }
        public const string _LIKE_expName = "LIKE";        public const int _LIKE_expGid = 31;
        public static mdExpression LIKE
            {
            get { return Metadata.Expressions[_LIKE_expGid]; }
            }
        public const string _LEFT_expName = "LEFT";        public const int _LEFT_expGid = 33;
        public static mdExpression LEFT
            {
            get { return Metadata.Expressions[_LEFT_expGid]; }
            }
        public const string _RIGHT_expName = "RIGHT";        public const int _RIGHT_expGid = 34;
        public static mdExpression RIGHT
            {
            get { return Metadata.Expressions[_RIGHT_expGid]; }
            }
        public const string _LOWER_expName = "LOWER";        public const int _LOWER_expGid = 35;
        public static mdExpression LOWER
            {
            get { return Metadata.Expressions[_LOWER_expGid]; }
            }
        public const string _LTRIM_expName = "LTRIM";        public const int _LTRIM_expGid = 36;
        public static mdExpression LTRIM
            {
            get { return Metadata.Expressions[_LTRIM_expGid]; }
            }
        public const string _RTRIM_expName = "RTRIM";        public const int _RTRIM_expGid = 37;
        public static mdExpression RTRIM
            {
            get { return Metadata.Expressions[_RTRIM_expGid]; }
            }
        public const string _UPPER_expName = "UPPER";        public const int _UPPER_expGid = 38;
        public static mdExpression UPPER
            {
            get { return Metadata.Expressions[_UPPER_expGid]; }
            }
        public const string _LEN_expName = "LEN";        public const int _LEN_expGid = 39;
        public static mdExpression LEN
            {
            get { return Metadata.Expressions[_LEN_expGid]; }
            }
        public const string _REPLACE_expName = "REPLACE";        public const int _REPLACE_expGid = 40;
        public static mdExpression REPLACE
            {
            get { return Metadata.Expressions[_REPLACE_expGid]; }
            }
        public const string _STUFF_expName = "STUFF";        public const int _STUFF_expGid = 41;
        public static mdExpression STUFF
            {
            get { return Metadata.Expressions[_STUFF_expGid]; }
            }
        public const string _SUBSTRING_expName = "SUBSTRING";        public const int _SUBSTRING_expGid = 42;
        public static mdExpression SUBSTRING
            {
            get { return Metadata.Expressions[_SUBSTRING_expGid]; }
            }
        public const string _ASCII_expName = "ASCII";        public const int _ASCII_expGid = 43;
        public static mdExpression ASCII
            {
            get { return Metadata.Expressions[_ASCII_expGid]; }
            }
        public const string _CHAR_expName = "CHAR";        public const int _CHAR_expGid = 44;
        public static mdExpression CHAR
            {
            get { return Metadata.Expressions[_CHAR_expGid]; }
            }
        public const string _UNICODE_expName = "UNICODE";        public const int _UNICODE_expGid = 45;
        public static mdExpression UNICODE
            {
            get { return Metadata.Expressions[_UNICODE_expGid]; }
            }
        public const string _NCHAR_expName = "NCHAR";        public const int _NCHAR_expGid = 46;
        public static mdExpression NCHAR
            {
            get { return Metadata.Expressions[_NCHAR_expGid]; }
            }
        public const string _CHARINDEX_expName = "CHARINDEX";        public const int _CHARINDEX_expGid = 47;
        public static mdExpression CHARINDEX
            {
            get { return Metadata.Expressions[_CHARINDEX_expGid]; }
            }
        public const string _PATINDEX_expName = "PATINDEX";        public const int _PATINDEX_expGid = 48;
        public static mdExpression PATINDEX
            {
            get { return Metadata.Expressions[_PATINDEX_expGid]; }
            }
        public const string _ABS_expName = "ABS";        public const int _ABS_expGid = 49;
        public static mdExpression ABS
            {
            get { return Metadata.Expressions[_ABS_expGid]; }
            }
        public const string _SIGN_expName = "SIGN";        public const int _SIGN_expGid = 50;
        public static mdExpression SIGN
            {
            get { return Metadata.Expressions[_SIGN_expGid]; }
            }
        public const string _CEILING_expName = "CEILING";        public const int _CEILING_expGid = 51;
        public static mdExpression CEILING
            {
            get { return Metadata.Expressions[_CEILING_expGid]; }
            }
        public const string _FLOOR_expName = "FLOOR";        public const int _FLOOR_expGid = 52;
        public static mdExpression FLOOR
            {
            get { return Metadata.Expressions[_FLOOR_expGid]; }
            }
        public const string _RAND_expName = "RAND";        public const int _RAND_expGid = 54;
        public static mdExpression RAND
            {
            get { return Metadata.Expressions[_RAND_expGid]; }
            }
        public const string _SQUARE_expName = "SQUARE";        public const int _SQUARE_expGid = 55;
        public static mdExpression SQUARE
            {
            get { return Metadata.Expressions[_SQUARE_expGid]; }
            }
        public const string _SQRT_expName = "SQRT";        public const int _SQRT_expGid = 56;
        public static mdExpression SQRT
            {
            get { return Metadata.Expressions[_SQRT_expGid]; }
            }
        public const string _EXP_expName = "EXP";        public const int _EXP_expGid = 57;
        public static mdExpression EXP
            {
            get { return Metadata.Expressions[_EXP_expGid]; }
            }
        public const string _LOG_expName = "LOG";        public const int _LOG_expGid = 58;
        public static mdExpression LOG
            {
            get { return Metadata.Expressions[_LOG_expGid]; }
            }
        public const string _LOG10_expName = "LOG10";        public const int _LOG10_expGid = 59;
        public static mdExpression LOG10
            {
            get { return Metadata.Expressions[_LOG10_expGid]; }
            }
        public const string _COS_expName = "COS";        public const int _COS_expGid = 60;
        public static mdExpression COS
            {
            get { return Metadata.Expressions[_COS_expGid]; }
            }
        public const string _SIN_expName = "SIN";        public const int _SIN_expGid = 61;
        public static mdExpression SIN
            {
            get { return Metadata.Expressions[_SIN_expGid]; }
            }
        public const string _CAST_expName = "CAST";        public const int _CAST_expGid = 62;
        public static mdExpression CAST
            {
            get { return Metadata.Expressions[_CAST_expGid]; }
            }
        public const string _ISNULL_expName = "ISNULL";        public const int _ISNULL_expGid = 63;
        public static mdExpression ISNULL
            {
            get { return Metadata.Expressions[_ISNULL_expGid]; }
            }
        public const string _CASE_expName = "CASE";        public const int _CASE_expGid = 66;
        public static mdExpression CASE
            {
            get { return Metadata.Expressions[_CASE_expGid]; }
            }
        public const string _IN_expName = "IN";        public const int _IN_expGid = 67;
        public static mdExpression IN
            {
            get { return Metadata.Expressions[_IN_expGid]; }
            }
        public const string _CONVERT_expName = "CONVERT";        public const int _CONVERT_expGid = 70;
        public static mdExpression CONVERT
            {
            get { return Metadata.Expressions[_CONVERT_expGid]; }
            }
        public const string _ROUND_expName = "ROUND";        public const int _ROUND_expGid = 73;
        public static mdExpression ROUND
            {
            get { return Metadata.Expressions[_ROUND_expGid]; }
            }
        public const string _EXISTS_expName = "EXISTS";        public const int _EXISTS_expGid = 74;
        public static mdExpression EXISTS
            {
            get { return Metadata.Expressions[_EXISTS_expGid]; }
            }
        public const string _RowsCount_expName = "RowsCount";        public const int _RowsCount_expGid = 89;
        public static mdExpression RowsCount
            {
            get { return Metadata.Expressions[_RowsCount_expGid]; }
            }
        public const string _documents_representation_expr_expName = "documents_representation_expr";        public const int _documents_representation_expr_expGid = 105;
        public static mdExpression documents_representation_expr
            {
            get { return Metadata.Expressions[_documents_representation_expr_expGid]; }
            }
        public const string _Safe_division_expName = "Safe_division";        public const int _Safe_division_expGid = 124;
        public static mdExpression Safe_division
            {
            get { return Metadata.Expressions[_Safe_division_expGid]; }
            }
        public const string _DateTimeInInterval_expName = "DateTimeInInterval";        public const int _DateTimeInInterval_expGid = 129;
        public static mdExpression DateTimeInInterval
            {
            get { return Metadata.Expressions[_DateTimeInInterval_expGid]; }
            }
        public const string _Period_And_Status_Filter_expName = "Period_And_Status_Filter";        public const int _Period_And_Status_Filter_expGid = 144;
        public static mdExpression Period_And_Status_Filter
            {
            get { return Metadata.Expressions[_Period_And_Status_Filter_expGid]; }
            }
        public const string _Storages_Representation_expr_expName = "Storages_Representation_expr";        public const int _Storages_Representation_expr_expGid = 146;
        public static mdExpression Storages_Representation_expr
            {
            get { return Metadata.Expressions[_Storages_Representation_expr_expGid]; }
            }
        public const string _Number_and_date_repr_expName = "Number_and_date_repr";        public const int _Number_and_date_repr_expGid = 157;
        public static mdExpression Number_and_date_repr
            {
            get { return Metadata.Expressions[_Number_and_date_repr_expGid]; }
            }
        public const string _DoubleEquals_expName = "DoubleEquals";        public const int _DoubleEquals_expGid = 164;
        public static mdExpression DoubleEquals
            {
            get { return Metadata.Expressions[_DoubleEquals_expGid]; }
            }
        public const string _Negative_Value_expName = "Negative_Value";        public const int _Negative_Value_expGid = 165;
        public static mdExpression Negative_Value
            {
            get { return Metadata.Expressions[_Negative_Value_expGid]; }
            }
        public const string _Percent_part_expName = "Percent_part";        public const int _Percent_part_expGid = 179;
        public static mdExpression Percent_part
            {
            get { return Metadata.Expressions[_Percent_part_expGid]; }
            }
        public const string _EqualsOrNulls_expName = "EqualsOrNulls";        public const int _EqualsOrNulls_expGid = 197;
        public static mdExpression EqualsOrNulls
            {
            get { return Metadata.Expressions[_EqualsOrNulls_expGid]; }
            }
        public const string _Sync_Log_View_filterExpr_expName = "Sync_Log_View_filterExpr";        public const int _Sync_Log_View_filterExpr_expGid = 208;
        public static mdExpression Sync_Log_View_filterExpr
            {
            get { return Metadata.Expressions[_Sync_Log_View_filterExpr_expGid]; }
            }
        public const string _LastIntInBinary_expName = "LastIntInBinary";        public const int _LastIntInBinary_expGid = 209;
        public static mdExpression LastIntInBinary
            {
            get { return Metadata.Expressions[_LastIntInBinary_expGid]; }
            }
        public const string _System_Log_View_filterExpr_expName = "System_Log_View_filterExpr";        public const int _System_Log_View_filterExpr_expGid = 213;
        public static mdExpression System_Log_View_filterExpr
            {
            get { return Metadata.Expressions[_System_Log_View_filterExpr_expGid]; }
            }
        public const string _DateToString_expName = "DateToString";        public const int _DateToString_expGid = 230;
        public static mdExpression DateToString
            {
            get { return Metadata.Expressions[_DateToString_expGid]; }
            }
        public const string _getLastUpdateObjectDateTime_expName = "getLastUpdateObjectDateTime";        public const int _getLastUpdateObjectDateTime_expGid = 319;
        public static mdExpression getLastUpdateObjectDateTime
            {
            get { return Metadata.Expressions[_getLastUpdateObjectDateTime_expGid]; }
            }
        public const string _Status_And_PeriodActivity_Filter_expName = "Status_And_PeriodActivity_Filter";        public const int _Status_And_PeriodActivity_Filter_expGid = 460;
        public static mdExpression Status_And_PeriodActivity_Filter
            {
            get { return Metadata.Expressions[_Status_And_PeriodActivity_Filter_expGid]; }
            }
        public const string _Active_Notifications_Filter_expName = "Active_Notifications_Filter";        public const int _Active_Notifications_Filter_expGid = 463;
        public static mdExpression Active_Notifications_Filter
            {
            get { return Metadata.Expressions[_Active_Notifications_Filter_expGid]; }
            }
        public const string _StringsPair_Join_expName = "StringsPair_Join";        public const int _StringsPair_Join_expGid = 464;
        public static mdExpression StringsPair_Join
            {
            get { return Metadata.Expressions[_StringsPair_Join_expGid]; }
            }
        public const string _Equality_And_StatusFilter_expName = "Equality_And_StatusFilter";        public const int _Equality_And_StatusFilter_expGid = 465;
        public static mdExpression Equality_And_StatusFilter
            {
            get { return Metadata.Expressions[_Equality_And_StatusFilter_expGid]; }
            }
        public const string _company_persons_representation_expName = "company_persons_representation";        public const int _company_persons_representation_expGid = 479;
        public static mdExpression company_persons_representation
            {
            get { return Metadata.Expressions[_company_persons_representation_expGid]; }
            }
        public const string _space_and_string_expName = "space_and_string";        public const int _space_and_string_expGid = 482;
        public static mdExpression space_and_string
            {
            get { return Metadata.Expressions[_space_and_string_expGid]; }
            }
        public const string _SmallMonthOfDate_expName = "SmallMonthOfDate";        public const int _SmallMonthOfDate_expGid = 489;
        public static mdExpression SmallMonthOfDate
            {
            get { return Metadata.Expressions[_SmallMonthOfDate_expGid]; }
            }
        public const string _getFullDirectoryPath_expName = "getFullDirectoryPath";        public const int _getFullDirectoryPath_expGid = 618;
        public static mdExpression getFullDirectoryPath
            {
            get { return Metadata.Expressions[_getFullDirectoryPath_expGid]; }
            }
        public const string _ValueWhenEquals_expName = "ValueWhenEquals";        public const int _ValueWhenEquals_expGid = 621;
        public static mdExpression ValueWhenEquals
            {
            get { return Metadata.Expressions[_ValueWhenEquals_expGid]; }
            }
        public const string _report_settings_is_for_db_type_expName = "report_settings_is_for_db_type";        public const int _report_settings_is_for_db_type_expGid = 637;
        public static mdExpression report_settings_is_for_db_type
            {
            get { return Metadata.Expressions[_report_settings_is_for_db_type_expGid]; }
            }
        public const string _is_report_group_available_expName = "is_report_group_available";        public const int _is_report_group_available_expGid = 638;
        public static mdExpression is_report_group_available
            {
            get { return Metadata.Expressions[_is_report_group_available_expGid]; }
            }
        public const string _is_report_settings_available_expName = "is_report_settings_available";        public const int _is_report_settings_available_expGid = 639;
        public static mdExpression is_report_settings_available
            {
            get { return Metadata.Expressions[_is_report_settings_available_expGid]; }
            }
        public const string _ReportAccountsStr_expName = "ReportAccountsStr";        public const int _ReportAccountsStr_expGid = 676;
        public static mdExpression ReportAccountsStr
            {
            get { return Metadata.Expressions[_ReportAccountsStr_expGid]; }
            }
        public const string _FavoriteReportGroupAccountsStr_expName = "FavoriteReportGroupAccountsStr";        public const int _FavoriteReportGroupAccountsStr_expGid = 677;
        public static mdExpression FavoriteReportGroupAccountsStr
            {
            get { return Metadata.Expressions[_FavoriteReportGroupAccountsStr_expGid]; }
            }
        public const string _is_report_available_for_db_expName = "is_report_available_for_db";        public const int _is_report_available_for_db_expGid = 678;
        public static mdExpression is_report_available_for_db
            {
            get { return Metadata.Expressions[_is_report_available_for_db_expGid]; }
            }
        public const string _is_report_group_available_for_db_expName = "is_report_group_available_for_db";        public const int _is_report_group_available_for_db_expGid = 679;
        public static mdExpression is_report_group_available_for_db
            {
            get { return Metadata.Expressions[_is_report_group_available_for_db_expGid]; }
            }
        public const string _is_report_hidden_for_user_expName = "is_report_hidden_for_user";        public const int _is_report_hidden_for_user_expGid = 680;
        public static mdExpression is_report_hidden_for_user
            {
            get { return Metadata.Expressions[_is_report_hidden_for_user_expGid]; }
            }
        public const string _IsNotificationByDataChangesRequestTheLastOne_expName = "IsNotificationByDataChangesRequestTheLastOne";        public const int _IsNotificationByDataChangesRequestTheLastOne_expGid = 694;
        public static mdExpression IsNotificationByDataChangesRequestTheLastOne
            {
            get { return Metadata.Expressions[_IsNotificationByDataChangesRequestTheLastOne_expGid]; }
            }
        public const string _SurNameAndInitials_expName = "SurNameAndInitials";        public const int _SurNameAndInitials_expGid = 716;
        public static mdExpression SurNameAndInitials
            {
            get { return Metadata.Expressions[_SurNameAndInitials_expGid]; }
            }
        public const string _has_active_working_measages_for_user_expName = "has_active_working_measages_for_user";        public const int _has_active_working_measages_for_user_expGid = 717;
        public static mdExpression has_active_working_measages_for_user
            {
            get { return Metadata.Expressions[_has_active_working_measages_for_user_expGid]; }
            }
        public const string _has_any_working_measages_for_user_expName = "has_any_working_measages_for_user";        public const int _has_any_working_measages_for_user_expGid = 718;
        public static mdExpression has_any_working_measages_for_user
            {
            get { return Metadata.Expressions[_has_any_working_measages_for_user_expGid]; }
            }
        public const string _getAllComments_expName = "getAllComments";        public const int _getAllComments_expGid = 724;
        public static mdExpression getAllComments
            {
            get { return Metadata.Expressions[_getAllComments_expGid]; }
            }
        public const string _Equals_And_StatusActive_expName = "Equals_And_StatusActive";        public const int _Equals_And_StatusActive_expGid = 752;
        public static mdExpression Equals_And_StatusActive
            {
            get { return Metadata.Expressions[_Equals_And_StatusActive_expGid]; }
            }
        public const string _getQnAGroupTop10Items_expName = "getQnAGroupTop10Items";        public const int _getQnAGroupTop10Items_expGid = 755;
        public static mdExpression getQnAGroupTop10Items
            {
            get { return Metadata.Expressions[_getQnAGroupTop10Items_expGid]; }
            }
        public const string _StatusAndBorder_expName = "StatusAndBorder";        public const int _StatusAndBorder_expGid = 756;
        public static mdExpression StatusAndBorder
            {
            get { return Metadata.Expressions[_StatusAndBorder_expGid]; }
            }
        public const string _Period_And_Status_Ext2_expName = "Period_And_Status_Ext2";        public const int _Period_And_Status_Ext2_expGid = 757;
        public static mdExpression Period_And_Status_Ext2
            {
            get { return Metadata.Expressions[_Period_And_Status_Ext2_expGid]; }
            }
        public const string _EqualityAndPeriod_expName = "EqualityAndPeriod";        public const int _EqualityAndPeriod_expGid = 758;
        public static mdExpression EqualityAndPeriod
            {
            get { return Metadata.Expressions[_EqualityAndPeriod_expGid]; }
            }
        public const string _IsActiveStatus_expName = "IsActiveStatus";        public const int _IsActiveStatus_expGid = 763;
        public static mdExpression IsActiveStatus
            {
            get { return Metadata.Expressions[_IsActiveStatus_expGid]; }
            }
        public const string _GetCompPerson_expName = "GetCompPerson";        public const int _GetCompPerson_expGid = 764;
        public static mdExpression GetCompPerson
            {
            get { return Metadata.Expressions[_GetCompPerson_expGid]; }
            }
        public const string _GetMultiAnswers_expName = "GetMultiAnswers";        public const int _GetMultiAnswers_expGid = 768;
        public static mdExpression GetMultiAnswers
            {
            get { return Metadata.Expressions[_GetMultiAnswers_expGid]; }
            }
        public const string _getActualCompanyPerson_expName = "getActualCompanyPerson";        public const int _getActualCompanyPerson_expGid = 786;
        public static mdExpression getActualCompanyPerson
            {
            get { return Metadata.Expressions[_getActualCompanyPerson_expGid]; }
            }
        public const string _GetWebAccountID_expName = "GetWebAccountID";        public const int _GetWebAccountID_expGid = 787;
        public static mdExpression GetWebAccountID
            {
            get { return Metadata.Expressions[_GetWebAccountID_expGid]; }
            }
        public const string _StrIndexOf_expName = "StrIndexOf";        public const int _StrIndexOf_expGid = 795;
        public static mdExpression StrIndexOf
            {
            get { return Metadata.Expressions[_StrIndexOf_expGid]; }
            }
        public const string _StrContains_expName = "StrContains";        public const int _StrContains_expGid = 796;
        public static mdExpression StrContains
            {
            get { return Metadata.Expressions[_StrContains_expGid]; }
            }

    }
    /// <summary>
    /// Статична інформація про типи даних в метаданих
    /// </summary>
    public static class _DataTypes_SD_
    {
        public const string _bool_dtName = "bool";        public const int _bool_dtGid = 1;
        public static mdDataType bool_dt
            {
            get { return Metadata.DataTypes[_bool_dtGid]; }
            }
        public static class bool_defaults_
            {
            public const System.Boolean _false = false;
            public static mdConstant _false_const = bool_dt.Constants[154];
            }
        public const string _float_dtName = "float";        public const int _float_dtGid = 2;
        public static mdDataType float_dt
            {
            get { return Metadata.DataTypes[_float_dtGid]; }
            }
        public static class float_defaults_
            {
            public const System.Double _0 = 0;
            public static mdConstant _0_const = float_dt.Constants[1];
            public const System.Double _1 = 1;
            public static mdConstant _1_const = float_dt.Constants[17];
            public const System.Double _999 = 999;
            public static mdConstant _999_const = float_dt.Constants[228];
            public const System.Double _20 = 20;
            public static mdConstant _20_const = float_dt.Constants[249];
            public const System.Double _m1 = -1;
            public static mdConstant _m1_const = float_dt.Constants[550];
            public const System.Double _4 = 4;
            public static mdConstant _4_const = float_dt.Constants[645];
            public const System.Double _100 = 100;
            public static mdConstant _100_const = float_dt.Constants[678];
            public const System.Double _3 = 3;
            public static mdConstant _3_const = float_dt.Constants[939];
            public const System.Double _2 = 2;
            public static mdConstant _2_const = float_dt.Constants[941];
            public const System.Double _30 = 30;
            public static mdConstant _30_const = float_dt.Constants[954];
            }
        public const string _bigint_dtName = "bigint";        public const int _bigint_dtGid = 4;
        public static mdDataType bigint_dt
            {
            get { return Metadata.DataTypes[_bigint_dtGid]; }
            }
        public const string _bit_dtName = "bit";        public const int _bit_dtGid = 5;
        public static mdDataType bit_dt
            {
            get { return Metadata.DataTypes[_bit_dtGid]; }
            }
        public static class bit_defaults_
            {
            public const System.Boolean _0 = false;
            public static mdConstant _0_const = bit_dt.Constants[178];
            public const System.Boolean _1 = true;
            public static mdConstant _1_const = bit_dt.Constants[376];
            }
        public const string _int_dtName = "int";        public const int _int_dtGid = 7;
        public static mdDataType int_dt
            {
            get { return Metadata.DataTypes[_int_dtGid]; }
            }
        public static class int_defaults_
            {
            public const System.Int32 _1 = 1;
            public static mdConstant _1_const = int_dt.Constants[155];
            public const System.Int32 _104 = 104;
            public static mdConstant _104_const = int_dt.Constants[182];
            public const System.Int32 _0 = 0;
            public static mdConstant _0_const = int_dt.Constants[208];
            public const System.Int32 _1024 = 1024;
            public static mdConstant _1024_const = int_dt.Constants[411];
            public const System.Int32 Twelve = 12;
            public static mdConstant Twelve_const = int_dt.Constants[1416];
            }
        public const string _smallint_dtName = "smallint";        public const int _smallint_dtGid = 10;
        public static mdDataType smallint_dt
            {
            get { return Metadata.DataTypes[_smallint_dtGid]; }
            }
        public const string _datetime_dtName = "datetime";        public const int _datetime_dtGid = 13;
        public static mdDataType datetime_dt
            {
            get { return Metadata.DataTypes[_datetime_dtGid]; }
            }
        public const string _varchar_50_dtName = "varchar(50)";        public const int _varchar_50_dtGid = 17;
        public static mdDataType varchar_50_dt
            {
            get { return Metadata.DataTypes[_varchar_50_dtGid]; }
            }
        public const string _varchar_MAX_dtName = "varchar(MAX)";        public const int _varchar_MAX_dtGid = 18;
        public static mdDataType varchar_MAX_dt
            {
            get { return Metadata.DataTypes[_varchar_MAX_dtGid]; }
            }
        public const string _nvarchar_50_dtName = "nvarchar(50)";        public const int _nvarchar_50_dtGid = 21;
        public static mdDataType nvarchar_50_dt
            {
            get { return Metadata.DataTypes[_nvarchar_50_dtGid]; }
            }
        public const string _nvarchar_MAX_dtName = "nvarchar(MAX)";        public const int _nvarchar_MAX_dtGid = 22;
        public static mdDataType nvarchar_MAX_dt
            {
            get { return Metadata.DataTypes[_nvarchar_MAX_dtGid]; }
            }
        public static class nvarchar_MAX_defaults_
            {
            public const System.String _d = ".";
            public static mdConstant _d_const = nvarchar_MAX_dt.Constants[28];
            public const System.String __m_ = " - ";
            public static mdConstant __m__const = nvarchar_MAX_dt.Constants[29];
            public const System.String Space = " ";
            public static mdConstant Space_const = nvarchar_MAX_dt.Constants[171];
            public const System.String __від_ = " від ";
            public static mdConstant __від__const = nvarchar_MAX_dt.Constants[181];
            public const System.String _в = "в";
            public static mdConstant _в_const = nvarchar_MAX_dt.Constants[212];
            public const System.String _МФО = "МФО";
            public static mdConstant _МФО_const = nvarchar_MAX_dt.Constants[213];
            public const System.String _ = "";
            public static mdConstant __const = nvarchar_MAX_dt.Constants[270];
            public const System.String _Центральна_база_даних = "Центральна база даних";
            public static mdConstant _Центральна_база_даних_const = nvarchar_MAX_dt.Constants[516];
            public const System.String Slash = "/";
            public static mdConstant Slash_const = nvarchar_MAX_dt.Constants[1442];
            public const System.String IsFree = "Вільно";
            public static mdConstant IsFree_const = nvarchar_MAX_dt.Constants[2507];
            public const System.String Saled = "Продано";
            public static mdConstant Saled_const = nvarchar_MAX_dt.Constants[2508];
            public const System.String Rented = "В оренді";
            public static mdConstant Rented_const = nvarchar_MAX_dt.Constants[2509];
            public const System.String SlashWithSpaces = " / ";
            public static mdConstant SlashWithSpaces_const = nvarchar_MAX_dt.Constants[5015];
            }
        public const string _varbinary_MAX_dtName = "varbinary(MAX)";        public const int _varbinary_MAX_dtGid = 26;
        public static mdDataType varbinary_MAX_dt
            {
            get { return Metadata.DataTypes[_varbinary_MAX_dtGid]; }
            }
        public const string _SQL_DateParts_dtName = "SQL_DateParts";        public const int _SQL_DateParts_dtGid = 31;
        public static mdDataType SQL_DateParts_dt
            {
            get { return Metadata.DataTypes[_SQL_DateParts_dtGid]; }
            }
        public static class SQL_DateParts_enum_
            {
            public const System.String _year = "year";
            public static mdConstant _year_const = SQL_DateParts_dt.Constants[2];
            public const System.String _quarter = "quarter";
            public static mdConstant _quarter_const = SQL_DateParts_dt.Constants[3];
            public const System.String _month = "month";
            public static mdConstant _month_const = SQL_DateParts_dt.Constants[4];
            public const System.String _dayofyear = "dayofyear";
            public static mdConstant _dayofyear_const = SQL_DateParts_dt.Constants[5];
            public const System.String _day = "day";
            public static mdConstant _day_const = SQL_DateParts_dt.Constants[6];
            public const System.String _week = "week";
            public static mdConstant _week_const = SQL_DateParts_dt.Constants[7];
            public const System.String _hour = "hour";
            public static mdConstant _hour_const = SQL_DateParts_dt.Constants[8];
            public const System.String _minute = "minute";
            public static mdConstant _minute_const = SQL_DateParts_dt.Constants[9];
            public const System.String _second = "second";
            public static mdConstant _second_const = SQL_DateParts_dt.Constants[10];
            public const System.String _millisecond = "millisecond";
            public static mdConstant _millisecond_const = SQL_DateParts_dt.Constants[11];
            public const System.String _weekday = "weekday";
            public static mdConstant _weekday_const = SQL_DateParts_dt.Constants[253];
            }
        public const string _Case_Type_Enum_dtName = "Case_Type_Enum";        public const int _Case_Type_Enum_dtGid = 32;
        public static mdDataType Case_Type_Enum_dt
            {
            get { return Metadata.DataTypes[_Case_Type_Enum_dtGid]; }
            }
        public static class Case_Type_Enum_enum_
            {
            public const System.Byte Case_Simple_With_Else = 1;
            public static mdConstant Case_Simple_With_Else_const = Case_Type_Enum_dt.Constants[12];
            public const System.Byte Case_Conditional_With_Else = 2;
            public static mdConstant Case_Conditional_With_Else_const = Case_Type_Enum_dt.Constants[13];
            public const System.Byte Case_Simple_Without_Else = 3;
            public static mdConstant Case_Simple_Without_Else_const = Case_Type_Enum_dt.Constants[14];
            public const System.Byte Case_Conditional_Without_Else = 4;
            public static mdConstant Case_Conditional_Without_Else_const = Case_Type_Enum_dt.Constants[15];
            }
        public const string _Object_statuses_reference_dtName = "Object_statuses_reference";        public const int _Object_statuses_reference_dtGid = 34;
        public static mdDataType Object_statuses_reference_dt
            {
            get { return Metadata.DataTypes[_Object_statuses_reference_dtGid]; }
            }
        public static class Object_statuses_reference_defaults_
            {
            public const System.Byte Draft = 1;
            public static mdConstant Draft_const = Object_statuses_reference_dt.Constants[179];
            public const System.Byte MaxActive = 127;
            public static mdConstant MaxActive_const = Object_statuses_reference_dt.Constants[188];
            public const System.Byte Active = 2;
            public static mdConstant Active_const = Object_statuses_reference_dt.Constants[189];
            public const System.Byte Accepted = 4;
            public static mdConstant Accepted_const = Object_statuses_reference_dt.Constants[369];
            public const System.Byte Blocked = 128;
            public static mdConstant Blocked_const = Object_statuses_reference_dt.Constants[2729];
            }
        public const string _Accounts_reference_dtName = "Accounts_reference";        public const int _Accounts_reference_dtGid = 35;
        public static mdDataType Accounts_reference_dt
            {
            get { return Metadata.DataTypes[_Accounts_reference_dtGid]; }
            }
        public const string _nvarchar_255_dtName = "nvarchar(255)";        public const int _nvarchar_255_dtGid = 37;
        public static mdDataType nvarchar_255_dt
            {
            get { return Metadata.DataTypes[_nvarchar_255_dtGid]; }
            }
        public const string _storages_reference_dtName = "storages_reference";        public const int _storages_reference_dtGid = 38;
        public static mdDataType storages_reference_dt
            {
            get { return Metadata.DataTypes[_storages_reference_dtGid]; }
            }
        public static class storages_reference_defaults_
            {
            public const System.Int32 CentralDB = 1;
            public static mdConstant CentralDB_const = storages_reference_dt.Constants[1303];
            }
        public const string _External_automation_systems_reference_dtName = "External_automation_systems_reference";        public const int _External_automation_systems_reference_dtGid = 39;
        public static mdDataType External_automation_systems_reference_dt
            {
            get { return Metadata.DataTypes[_External_automation_systems_reference_dtGid]; }
            }
        public const string _Operation_periods_reference_dtName = "Operation_periods_reference";        public const int _Operation_periods_reference_dtGid = 40;
        public static mdDataType Operation_periods_reference_dt
            {
            get { return Metadata.DataTypes[_Operation_periods_reference_dtGid]; }
            }
        public const string _operation_types_reference_dtName = "operation_types_reference";        public const int _operation_types_reference_dtGid = 41;
        public static mdDataType operation_types_reference_dt
            {
            get { return Metadata.DataTypes[_operation_types_reference_dtGid]; }
            }
        public static class operation_types_reference_defaults_
            {
            public const System.Int16 OrderOnDataChanging = 15561;
            public static mdConstant OrderOnDataChanging_const = operation_types_reference_dt.Constants[2556];
            public const System.Int16 FundingOrder = 15716;
            public static mdConstant FundingOrder_const = operation_types_reference_dt.Constants[2663];
            public const System.Int16 Service_Request = 15641;
            public static mdConstant Service_Request_const = operation_types_reference_dt.Constants[2690];
            public const System.Int16 Service_Responce = 15642;
            public static mdConstant Service_Responce_const = operation_types_reference_dt.Constants[2691];
            }
        public const string _Tables_reference_dtName = "Tables_reference";        public const int _Tables_reference_dtGid = 42;
        public static mdDataType Tables_reference_dt
            {
            get { return Metadata.DataTypes[_Tables_reference_dtGid]; }
            }
        public static class Tables_reference_defaults_
            {
            public const System.Int32 _650 = 650;
            public static mdConstant _650_const = Tables_reference_dt.Constants[1190];
            }
        public const string _GUIDs_dtName = "GUIDs";        public const int _GUIDs_dtGid = 44;
        public static mdDataType GUIDs_dt
            {
            get { return Metadata.DataTypes[_GUIDs_dtGid]; }
            }
        public const string _mdTables_reference_dtName = "mdTables_reference";        public const int _mdTables_reference_dtGid = 45;
        public static mdDataType mdTables_reference_dt
            {
            get { return Metadata.DataTypes[_mdTables_reference_dtGid]; }
            }
        public static class mdTables_reference_defaults_
            {
            public const System.Int32 _113 = 113;
            public static mdConstant _113_const = mdTables_reference_dt.Constants[552];
            public const System.Int32 _102 = 102;
            public static mdConstant _102_const = mdTables_reference_dt.Constants[632];
            public const System.Int32 _205 = 205;
            public static mdConstant _205_const = mdTables_reference_dt.Constants[634];
            public const System.Int32 _650 = 650;
            public static mdConstant _650_const = mdTables_reference_dt.Constants[635];
            public const System.Int32 _105 = 105;
            public static mdConstant _105_const = mdTables_reference_dt.Constants[917];
            public const System.Int32 _651 = 651;
            public static mdConstant _651_const = mdTables_reference_dt.Constants[959];
            public const System.Int32 _15178 = 15178;
            public static mdConstant _15178_const = mdTables_reference_dt.Constants[1271];
            public const System.Int32 Web_Accounts = 15218;
            public static mdConstant Web_Accounts_const = mdTables_reference_dt.Constants[1296];
            public const System.Int32 _1172 = 1172;
            public static mdConstant _1172_const = mdTables_reference_dt.Constants[2017];
            public const System.Int32 _188 = 188;
            public static mdConstant _188_const = mdTables_reference_dt.Constants[2020];
            public const System.Int32 _103 = 103;
            public static mdConstant _103_const = mdTables_reference_dt.Constants[2022];
            public const System.Int32 _141 = 141;
            public static mdConstant _141_const = mdTables_reference_dt.Constants[2026];
            public const System.Int32 Planer_Events_Journal = 7101;
            public static mdConstant Planer_Events_Journal_const = mdTables_reference_dt.Constants[2321];
            public const System.Int32 FinProjects = 15461;
            public static mdConstant FinProjects_const = mdTables_reference_dt.Constants[2391];
            public const System.Int32 DocumentationsOfProjects = 15627;
            public static mdConstant DocumentationsOfProjects_const = mdTables_reference_dt.Constants[2506];
            public const System.Int32 Budjet_Journal = 1607;
            public static mdConstant Budjet_Journal_const = mdTables_reference_dt.Constants[2543];
            public const System.Int32 _15738 = 15738;
            public static mdConstant _15738_const = mdTables_reference_dt.Constants[2628];
            public const System.Int32 _15774 = 15774;
            public static mdConstant _15774_const = mdTables_reference_dt.Constants[2698];
            public const System.Int32 _15773 = 15773;
            public static mdConstant _15773_const = mdTables_reference_dt.Constants[2699];
            public const System.Int32 _15775 = 15775;
            public static mdConstant _15775_const = mdTables_reference_dt.Constants[2700];
            public const System.Int32 _15771 = 15771;
            public static mdConstant _15771_const = mdTables_reference_dt.Constants[2701];
            public const System.Int32 _15772 = 15772;
            public static mdConstant _15772_const = mdTables_reference_dt.Constants[2702];
            public const System.Int32 _15770 = 15770;
            public static mdConstant _15770_const = mdTables_reference_dt.Constants[2703];
            public const System.Int32 insurance_companies = 15859;
            public static mdConstant insurance_companies_const = mdTables_reference_dt.Constants[2794];
            public const System.Int32 _15860 = 15860;
            public static mdConstant _15860_const = mdTables_reference_dt.Constants[2795];
            public const System.Int32 insurance_products = 15862;
            public static mdConstant insurance_products_const = mdTables_reference_dt.Constants[2800];
            public const System.Int32 _1776 = 1776;
            public static mdConstant _1776_const = mdTables_reference_dt.Constants[5003];
            public const System.Int32 _652 = 652;
            public static mdConstant _652_const = mdTables_reference_dt.Constants[5014];
            }
        public const string _DataBaseTypes_dtName = "DataBaseTypes";        public const int _DataBaseTypes_dtGid = 48;
        public static mdDataType DataBaseTypes_dt
            {
            get { return Metadata.DataTypes[_DataBaseTypes_dtGid]; }
            }
        public static class DataBaseTypes_enum_
            {
            public const System.Byte CDB = 0;
            public static mdConstant CDB_const = DataBaseTypes_dt.Constants[289];
            public const System.Byte _194 = 194;
            public static mdConstant _194_const = DataBaseTypes_dt.Constants[1288];
            }
        public const string _MDInterfaceTypes_dtName = "MDInterfaceTypes";        public const int _MDInterfaceTypes_dtGid = 49;
        public static mdDataType MDInterfaceTypes_dt
            {
            get { return Metadata.DataTypes[_MDInterfaceTypes_dtGid]; }
            }
        public const string _Varbinary_128_dtName = "Varbinary(128)";        public const int _Varbinary_128_dtGid = 50;
        public static mdDataType Varbinary_128_dt
            {
            get { return Metadata.DataTypes[_Varbinary_128_dtGid]; }
            }
        public const string _HTML_text_dtName = "HTML_text";        public const int _HTML_text_dtGid = 52;
        public static mdDataType HTML_text_dt
            {
            get { return Metadata.DataTypes[_HTML_text_dtGid]; }
            }
        public const string _File_Formats_dtName = "File_Formats";        public const int _File_Formats_dtGid = 54;
        public static mdDataType File_Formats_dt
            {
            get { return Metadata.DataTypes[_File_Formats_dtGid]; }
            }
        public static class File_Formats_enum_
            {
            public const System.Byte JPEG = 1;
            public static mdConstant JPEG_const = File_Formats_dt.Constants[2031];
            public const System.Byte PNG = 2;
            public static mdConstant PNG_const = File_Formats_dt.Constants[2032];
            public const System.Byte DOC = 3;
            public static mdConstant DOC_const = File_Formats_dt.Constants[2033];
            public const System.Byte DOCX = 4;
            public static mdConstant DOCX_const = File_Formats_dt.Constants[2034];
            public const System.Byte XLS = 5;
            public static mdConstant XLS_const = File_Formats_dt.Constants[2035];
            public const System.Byte XLSX = 6;
            public static mdConstant XLSX_const = File_Formats_dt.Constants[2036];
            public const System.Byte RTF = 7;
            public static mdConstant RTF_const = File_Formats_dt.Constants[2037];
            public const System.Byte PDF = 8;
            public static mdConstant PDF_const = File_Formats_dt.Constants[2038];
            public const System.Byte HTML = 9;
            public static mdConstant HTML_const = File_Formats_dt.Constants[2039];
            public const System.Byte CSS = 17;
            public static mdConstant CSS_const = File_Formats_dt.Constants[2694];
            public const System.Byte JS = 18;
            public static mdConstant JS_const = File_Formats_dt.Constants[2695];
            public const System.Byte TXT = 10;
            public static mdConstant TXT_const = File_Formats_dt.Constants[5023];
            public const System.Byte ZIP = 11;
            public static mdConstant ZIP_const = File_Formats_dt.Constants[5024];
            }
        public const string _Rich_Text_xml_dtName = "Rich_Text_xml";        public const int _Rich_Text_xml_dtGid = 55;
        public static mdDataType Rich_Text_xml_dt
            {
            get { return Metadata.DataTypes[_Rich_Text_xml_dtGid]; }
            }
        public const string _SmallMonth_dtName = "SmallMonth";        public const int _SmallMonth_dtGid = 59;
        public static mdDataType SmallMonth_dt
            {
            get { return Metadata.DataTypes[_SmallMonth_dtGid]; }
            }
        public const string _varchar_20_dtName = "varchar(20)";        public const int _varchar_20_dtGid = 101;
        public static mdDataType varchar_20_dt
            {
            get { return Metadata.DataTypes[_varchar_20_dtGid]; }
            }
        public const string _varchar_100_dtName = "varchar(100)";        public const int _varchar_100_dtGid = 102;
        public static mdDataType varchar_100_dt
            {
            get { return Metadata.DataTypes[_varchar_100_dtGid]; }
            }
        public const string _varchar_255_dtName = "varchar(255)";        public const int _varchar_255_dtGid = 103;
        public static mdDataType varchar_255_dt
            {
            get { return Metadata.DataTypes[_varchar_255_dtGid]; }
            }
        public const string _varchar_14_dtName = "varchar(14)";        public const int _varchar_14_dtGid = 104;
        public static mdDataType varchar_14_dt
            {
            get { return Metadata.DataTypes[_varchar_14_dtGid]; }
            }
        public const string _varchar_12_dtName = "varchar(12)";        public const int _varchar_12_dtGid = 105;
        public static mdDataType varchar_12_dt
            {
            get { return Metadata.DataTypes[_varchar_12_dtGid]; }
            }
        public const string _varchar_16_dtName = "varchar(16)";        public const int _varchar_16_dtGid = 106;
        public static mdDataType varchar_16_dt
            {
            get { return Metadata.DataTypes[_varchar_16_dtGid]; }
            }
        public const string _varchar_10_dtName = "varchar(10)";        public const int _varchar_10_dtGid = 107;
        public static mdDataType varchar_10_dt
            {
            get { return Metadata.DataTypes[_varchar_10_dtGid]; }
            }
        public const string _varchar_2000_dtName = "varchar(2000)";        public const int _varchar_2000_dtGid = 108;
        public static mdDataType varchar_2000_dt
            {
            get { return Metadata.DataTypes[_varchar_2000_dtGid]; }
            }
        public const string _e_mail_boxes_reference_dtName = "e_mail_boxes_reference";        public const int _e_mail_boxes_reference_dtGid = 110;
        public static mdDataType e_mail_boxes_reference_dt
            {
            get { return Metadata.DataTypes[_e_mail_boxes_reference_dtGid]; }
            }
        public const string _Persons_reference_dtName = "Persons_reference";        public const int _Persons_reference_dtGid = 117;
        public static mdDataType Persons_reference_dt
            {
            get { return Metadata.DataTypes[_Persons_reference_dtGid]; }
            }
        public const string _Varchar_5_dtName = "Varchar(5)";        public const int _Varchar_5_dtGid = 120;
        public static mdDataType Varchar_5_dt
            {
            get { return Metadata.DataTypes[_Varchar_5_dtGid]; }
            }
        public const string _FinancingObjects_reference_dtName = "FinancingObjects_reference";        public const int _FinancingObjects_reference_dtGid = 124;
        public static mdDataType FinancingObjects_reference_dt
            {
            get { return Metadata.DataTypes[_FinancingObjects_reference_dtGid]; }
            }
        public const string _FinancingObjectsGroups_reference_dtName = "FinancingObjectsGroups_reference";        public const int _FinancingObjectsGroups_reference_dtGid = 125;
        public static mdDataType FinancingObjectsGroups_reference_dt
            {
            get { return Metadata.DataTypes[_FinancingObjectsGroups_reference_dtGid]; }
            }
        public const string _SettingsVisibility_dtName = "SettingsVisibility";        public const int _SettingsVisibility_dtGid = 127;
        public static mdDataType SettingsVisibility_dt
            {
            get { return Metadata.DataTypes[_SettingsVisibility_dtGid]; }
            }
        public const string _MarketingProjects_reference_dtName = "MarketingProjects_reference";        public const int _MarketingProjects_reference_dtGid = 130;
        public static mdDataType MarketingProjects_reference_dt
            {
            get { return Metadata.DataTypes[_MarketingProjects_reference_dtGid]; }
            }
        public const string _QuestionnaireTemplates_reference_dtName = "QuestionnaireTemplates_reference";        public const int _QuestionnaireTemplates_reference_dtGid = 131;
        public static mdDataType QuestionnaireTemplates_reference_dt
            {
            get { return Metadata.DataTypes[_QuestionnaireTemplates_reference_dtGid]; }
            }
        public const string _QuestionGroups_reference_dtName = "QuestionGroups_reference";        public const int _QuestionGroups_reference_dtGid = 132;
        public static mdDataType QuestionGroups_reference_dt
            {
            get { return Metadata.DataTypes[_QuestionGroups_reference_dtGid]; }
            }
        public const string _QuestionKinds_reference_dtName = "QuestionKinds_reference";        public const int _QuestionKinds_reference_dtGid = 133;
        public static mdDataType QuestionKinds_reference_dt
            {
            get { return Metadata.DataTypes[_QuestionKinds_reference_dtGid]; }
            }
        public const string _QuestionDataTypes_dtName = "QuestionDataTypes";        public const int _QuestionDataTypes_dtGid = 134;
        public static mdDataType QuestionDataTypes_dt
            {
            get { return Metadata.DataTypes[_QuestionDataTypes_dtGid]; }
            }
        public static class QuestionDataTypes_enum_
            {
            public const System.Byte Boolean = 1;
            public static mdConstant Boolean_const = QuestionDataTypes_dt.Constants[2679];
            public const System.Byte Integer = 2;
            public static mdConstant Integer_const = QuestionDataTypes_dt.Constants[2680];
            public const System.Byte FractionalNumber = 3;
            public static mdConstant FractionalNumber_const = QuestionDataTypes_dt.Constants[2681];
            public const System.Byte DateTime = 6;
            public static mdConstant DateTime_const = QuestionDataTypes_dt.Constants[2682];
            public const System.Byte AlternativeSelection = 11;
            public static mdConstant AlternativeSelection_const = QuestionDataTypes_dt.Constants[2683];
            public const System.Byte MultipleAlternativeSelection = 12;
            public static mdConstant MultipleAlternativeSelection_const = QuestionDataTypes_dt.Constants[2684];
            public const System.Byte AlternativesMatrix = 14;
            public static mdConstant AlternativesMatrix_const = QuestionDataTypes_dt.Constants[2685];
            public const System.Byte GPS_Position = 21;
            public static mdConstant GPS_Position_const = QuestionDataTypes_dt.Constants[2687];
            public const System.Byte Text = 5;
            public static mdConstant Text_const = QuestionDataTypes_dt.Constants[2689];
            public const System.Byte MultiMatrix = 15;
            public static mdConstant MultiMatrix_const = QuestionDataTypes_dt.Constants[2709];
            public const System.Byte AlternativesRank = 13;
            public static mdConstant AlternativesRank_const = QuestionDataTypes_dt.Constants[2711];
            public const System.Byte FotoAttach = 23;
            public static mdConstant FotoAttach_const = QuestionDataTypes_dt.Constants[2750];
            public const System.Byte SMSVerifyPhoneNumber = 25;
            public static mdConstant SMSVerifyPhoneNumber_const = QuestionDataTypes_dt.Constants[2753];
            public const System.Byte AuthCode = 26;
            public static mdConstant AuthCode_const = QuestionDataTypes_dt.Constants[2802];
            }
        public const string _EMail_Journal_reference_dtName = "EMail_Journal_reference";        public const int _EMail_Journal_reference_dtGid = 135;
        public static mdDataType EMail_Journal_reference_dt
            {
            get { return Metadata.DataTypes[_EMail_Journal_reference_dtGid]; }
            }
        public const string _QnAlternativesKinds_reference_dtName = "QnAlternativesKinds_reference";        public const int _QnAlternativesKinds_reference_dtGid = 136;
        public static mdDataType QnAlternativesKinds_reference_dt
            {
            get { return Metadata.DataTypes[_QnAlternativesKinds_reference_dtGid]; }
            }
        public const string _QnAlternativesGroups_reference_dtName = "QnAlternativesGroups_reference";        public const int _QnAlternativesGroups_reference_dtGid = 137;
        public static mdDataType QnAlternativesGroups_reference_dt
            {
            get { return Metadata.DataTypes[_QnAlternativesGroups_reference_dtGid]; }
            }
        public const string _QnAlternativesItems_reference_dtName = "QnAlternativesItems_reference";        public const int _QnAlternativesItems_reference_dtGid = 138;
        public static mdDataType QnAlternativesItems_reference_dt
            {
            get { return Metadata.DataTypes[_QnAlternativesItems_reference_dtGid]; }
            }
        public const string _QnT_Items_reference_dtName = "QnT_Items_reference";        public const int _QnT_Items_reference_dtGid = 139;
        public static mdDataType QnT_Items_reference_dt
            {
            get { return Metadata.DataTypes[_QnT_Items_reference_dtGid]; }
            }
        public const string _PersonGroups_reference_dtName = "PersonGroups_reference";        public const int _PersonGroups_reference_dtGid = 141;
        public static mdDataType PersonGroups_reference_dt
            {
            get { return Metadata.DataTypes[_PersonGroups_reference_dtGid]; }
            }
        public const string _QnT_Conditions_reference_dtName = "QnT_Conditions_reference";        public const int _QnT_Conditions_reference_dtGid = 143;
        public static mdDataType QnT_Conditions_reference_dt
            {
            get { return Metadata.DataTypes[_QnT_Conditions_reference_dtGid]; }
            }
        public const string _QntParamsValues_dtName = "QntParamsValues";        public const int _QntParamsValues_dtGid = 144;
        public static mdDataType QntParamsValues_dt
            {
            get { return Metadata.DataTypes[_QntParamsValues_dtGid]; }
            }
        public const string _QuestionKindGroups_reference_dtName = "QuestionKindGroups_reference";        public const int _QuestionKindGroups_reference_dtGid = 146;
        public static mdDataType QuestionKindGroups_reference_dt
            {
            get { return Metadata.DataTypes[_QuestionKindGroups_reference_dtGid]; }
            }
        public const string _Web_Requests_reference_dtName = "Web_Requests_reference";        public const int _Web_Requests_reference_dtGid = 147;
        public static mdDataType Web_Requests_reference_dt
            {
            get { return Metadata.DataTypes[_Web_Requests_reference_dtGid]; }
            }
        public const string _WebRequestTypes_dtName = "WebRequestTypes";        public const int _WebRequestTypes_dtGid = 148;
        public static mdDataType WebRequestTypes_dt
            {
            get { return Metadata.DataTypes[_WebRequestTypes_dtGid]; }
            }
        public static class WebRequestTypes_enum_
            {
            public const System.Byte EMailConfirmation = 1;
            public static mdConstant EMailConfirmation_const = WebRequestTypes_dt.Constants[2705];
            public const System.Byte PasswordChange = 2;
            public static mdConstant PasswordChange_const = WebRequestTypes_dt.Constants[2706];
            public const System.Byte RegisterInvitation = 14;
            public static mdConstant RegisterInvitation_const = WebRequestTypes_dt.Constants[2817];
            }
        public const string _EvalResultTypes_dtName = "EvalResultTypes";        public const int _EvalResultTypes_dtGid = 151;
        public static mdDataType EvalResultTypes_dt
            {
            get { return Metadata.DataTypes[_EvalResultTypes_dtGid]; }
            }
        public static class EvalResultTypes_enum_
            {
            public const System.Byte Bool = 1;
            public static mdConstant Bool_const = EvalResultTypes_dt.Constants[2712];
            public const System.Byte Int = 2;
            public static mdConstant Int_const = EvalResultTypes_dt.Constants[2714];
            public const System.Byte String = 7;
            public static mdConstant String_const = EvalResultTypes_dt.Constants[2716];
            public const System.Byte IntArray = 8;
            public static mdConstant IntArray_const = EvalResultTypes_dt.Constants[2718];
            }
        public const string _Account_Settings_Types_reference_dtName = "Account_Settings_Types_reference";        public const int _Account_Settings_Types_reference_dtGid = 163;
        public static mdDataType Account_Settings_Types_reference_dt
            {
            get { return Metadata.DataTypes[_Account_Settings_Types_reference_dtGid]; }
            }
        public const string _int_natural_dtName = "int_natural";        public const int _int_natural_dtGid = 164;
        public static mdDataType int_natural_dt
            {
            get { return Metadata.DataTypes[_int_natural_dtGid]; }
            }
        public const string _WebBrowsers_reference_dtName = "WebBrowsers_reference";        public const int _WebBrowsers_reference_dtGid = 169;
        public static mdDataType WebBrowsers_reference_dt
            {
            get { return Metadata.DataTypes[_WebBrowsers_reference_dtGid]; }
            }
        public const string _ContractForms_dtName = "ContractForms";        public const int _ContractForms_dtGid = 173;
        public static mdDataType ContractForms_dt
            {
            get { return Metadata.DataTypes[_ContractForms_dtGid]; }
            }
        public static class ContractForms_enum_
            {
            public const System.Byte GeneralLegal = 0;
            public static mdConstant GeneralLegal_const = ContractForms_dt.Constants[2737];
            public const System.Byte Addition = 1;
            public static mdConstant Addition_const = ContractForms_dt.Constants[2738];
            public const System.Byte Spoken = 2;
            public static mdConstant Spoken_const = ContractForms_dt.Constants[2739];
            public const System.Byte Intentions = 3;
            public static mdConstant Intentions_const = ContractForms_dt.Constants[2740];
            }
        public const string _Numeric_9s3NN_dtName = "Numeric(9,3)NN";        public const int _Numeric_9s3NN_dtGid = 181;
        public static mdDataType Numeric_9s3NN_dt
            {
            get { return Metadata.DataTypes[_Numeric_9s3NN_dtGid]; }
            }
        public const string _Numeric7d2NN_dtName = "Numeric7.2NN";        public const int _Numeric7d2NN_dtGid = 182;
        public static mdDataType Numeric7d2NN_dt
            {
            get { return Metadata.DataTypes[_Numeric7d2NN_dtGid]; }
            }
        public const string _varchar_10_000_dtName = "varchar(10 000)";        public const int _varchar_10_000_dtGid = 185;
        public static mdDataType varchar_10_000_dt
            {
            get { return Metadata.DataTypes[_varchar_10_000_dtGid]; }
            }
        public const string _ContactsGroups_reference_dtName = "ContactsGroups_reference";        public const int _ContactsGroups_reference_dtGid = 213;
        public static mdDataType ContactsGroups_reference_dt
            {
            get { return Metadata.DataTypes[_ContactsGroups_reference_dtGid]; }
            }
        public const string _GModules_reference_dtName = "GModules_reference";        public const int _GModules_reference_dtGid = 214;
        public static mdDataType GModules_reference_dt
            {
            get { return Metadata.DataTypes[_GModules_reference_dtGid]; }
            }
        public const string _GMItemTypes_dtName = "GMItemTypes";        public const int _GMItemTypes_dtGid = 216;
        public static mdDataType GMItemTypes_dt
            {
            get { return Metadata.DataTypes[_GMItemTypes_dtGid]; }
            }
        public static class GMItemTypes_enum_
            {
            public const System.Byte GSchema = 1;
            public static mdConstant GSchema_const = GMItemTypes_dt.Constants[2806];
            public const System.Byte GActionsMap = 2;
            public static mdConstant GActionsMap_const = GMItemTypes_dt.Constants[2807];
            public const System.Byte HTMLTemplate = 3;
            public static mdConstant HTMLTemplate_const = GMItemTypes_dt.Constants[2808];
            public const System.Byte XMLTemplate = 4;
            public static mdConstant XMLTemplate_const = GMItemTypes_dt.Constants[2809];
            public const System.Byte Other = 99;
            public static mdConstant Other_const = GMItemTypes_dt.Constants[2810];
            }
        public const string _PersonsEventTypes_dtName = "PersonsEventTypes";        public const int _PersonsEventTypes_dtGid = 218;
        public static mdDataType PersonsEventTypes_dt
            {
            get { return Metadata.DataTypes[_PersonsEventTypes_dtGid]; }
            }
        public static class PersonsEventTypes_enum_
            {
            public const System.Byte Call = 1;
            public static mdConstant Call_const = PersonsEventTypes_dt.Constants[2812];
            public const System.Byte Meeting = 2;
            public static mdConstant Meeting_const = PersonsEventTypes_dt.Constants[2813];
            public const System.Byte Task = 3;
            public static mdConstant Task_const = PersonsEventTypes_dt.Constants[2814];
            public const System.Byte Other = 99;
            public static mdConstant Other_const = PersonsEventTypes_dt.Constants[2815];
            }
        public const string _EduTopics_reference_dtName = "EduTopics_reference";        public const int _EduTopics_reference_dtGid = 222;
        public static mdDataType EduTopics_reference_dt
            {
            get { return Metadata.DataTypes[_EduTopics_reference_dtGid]; }
            }
        public const string _EduGroup_reference_dtName = "EduGroup_reference";        public const int _EduGroup_reference_dtGid = 224;
        public static mdDataType EduGroup_reference_dt
            {
            get { return Metadata.DataTypes[_EduGroup_reference_dtGid]; }
            }
        public const string _EduLessons_reference_dtName = "EduLessons_reference";        public const int _EduLessons_reference_dtGid = 225;
        public static mdDataType EduLessons_reference_dt
            {
            get { return Metadata.DataTypes[_EduLessons_reference_dtGid]; }
            }
        public const string _Money_and_Salary_Documents_dtName = "Money_and_Salary_Documents";        public const int _Money_and_Salary_Documents_dtGid = 243;
        public static mdDataType Money_and_Salary_Documents_dt
            {
            get { return Metadata.DataTypes[_Money_and_Salary_Documents_dtGid]; }
            }
        public const string _NotificationStates_dtName = "NotificationStates";        public const int _NotificationStates_dtGid = 244;
        public static mdDataType NotificationStates_dt
            {
            get { return Metadata.DataTypes[_NotificationStates_dtGid]; }
            }
        public static class NotificationStates_enum_
            {
            public const System.Byte postponed = 1;
            public static mdConstant postponed_const = NotificationStates_dt.Constants[1129];
            public const System.Byte Active = 2;
            public static mdConstant Active_const = NotificationStates_dt.Constants[1130];
            public const System.Byte Accepted = 8;
            public static mdConstant Accepted_const = NotificationStates_dt.Constants[1131];
            public const System.Byte Canceled = 9;
            public static mdConstant Canceled_const = NotificationStates_dt.Constants[1132];
            public const System.Byte Read = 3;
            public static mdConstant Read_const = NotificationStates_dt.Constants[2545];
            }
        public const string _Types_of_UserNotifications_reference_dtName = "Types_of_UserNotifications_reference";        public const int _Types_of_UserNotifications_reference_dtGid = 247;
        public static mdDataType Types_of_UserNotifications_reference_dt
            {
            get { return Metadata.DataTypes[_Types_of_UserNotifications_reference_dtGid]; }
            }
        public const string _varchar_24_dtName = "varchar(24)";        public const int _varchar_24_dtGid = 248;
        public static mdDataType varchar_24_dt
            {
            get { return Metadata.DataTypes[_varchar_24_dtGid]; }
            }
        public const string _ExternalFiles_reference_dtName = "ExternalFiles_reference";        public const int _ExternalFiles_reference_dtGid = 283;
        public static mdDataType ExternalFiles_reference_dt
            {
            get { return Metadata.DataTypes[_ExternalFiles_reference_dtGid]; }
            }
        public const string _External_File_Directories_reference_dtName = "External_File_Directories_reference";        public const int _External_File_Directories_reference_dtGid = 284;
        public static mdDataType External_File_Directories_reference_dt
            {
            get { return Metadata.DataTypes[_External_File_Directories_reference_dtGid]; }
            }
        public const string _varchar_2_dtName = "varchar(2)";        public const int _varchar_2_dtGid = 287;
        public static mdDataType varchar_2_dt
            {
            get { return Metadata.DataTypes[_varchar_2_dtGid]; }
            }
        public const string _numeric_3d0NN_dtName = "numeric(3.0)NN";        public const int _numeric_3d0NN_dtGid = 288;
        public static mdDataType numeric_3d0NN_dt
            {
            get { return Metadata.DataTypes[_numeric_3d0NN_dtGid]; }
            }
        public const string _languages_reference_dtName = "languages_reference";        public const int _languages_reference_dtGid = 289;
        public static mdDataType languages_reference_dt
            {
            get { return Metadata.DataTypes[_languages_reference_dtGid]; }
            }
        public static class languages_reference_defaults_
            {
            public const System.Int16 UA = 1;
            public static mdConstant UA_const = languages_reference_dt.Constants[1272];
            }
        public const string _Char_2_dtName = "Char(2)";        public const int _Char_2_dtGid = 290;
        public static mdDataType Char_2_dt
            {
            get { return Metadata.DataTypes[_Char_2_dtGid]; }
            }
        public const string _mdFields_reference_dtName = "mdFields_reference";        public const int _mdFields_reference_dtGid = 291;
        public static mdDataType mdFields_reference_dt
            {
            get { return Metadata.DataTypes[_mdFields_reference_dtGid]; }
            }
        public const string _Web_Catalog_reference_dtName = "Web_Catalog_reference";        public const int _Web_Catalog_reference_dtGid = 292;
        public static mdDataType Web_Catalog_reference_dt
            {
            get { return Metadata.DataTypes[_Web_Catalog_reference_dtGid]; }
            }
        public const string _Goods_Catalog_Objects_dtName = "Goods_Catalog_Objects";        public const int _Goods_Catalog_Objects_dtGid = 293;
        public static mdDataType Goods_Catalog_Objects_dt
            {
            get { return Metadata.DataTypes[_Goods_Catalog_Objects_dtGid]; }
            }
        public const string _mdRolesReference_dtName = "mdRolesReference";        public const int _mdRolesReference_dtGid = 297;
        public static mdDataType mdRolesReference_dt
            {
            get { return Metadata.DataTypes[_mdRolesReference_dtGid]; }
            }
        public const string _SettlementsRecorders_dtName = "SettlementsRecorders";        public const int _SettlementsRecorders_dtGid = 298;
        public static mdDataType SettlementsRecorders_dt
            {
            get { return Metadata.DataTypes[_SettlementsRecorders_dtGid]; }
            }
        public const string _Web_Accounts_reference_dtName = "Web_Accounts_reference";        public const int _Web_Accounts_reference_dtGid = 304;
        public static mdDataType Web_Accounts_reference_dt
            {
            get { return Metadata.DataTypes[_Web_Accounts_reference_dtGid]; }
            }
        public const string _User_Notifications_reference_dtName = "User_Notifications_reference";        public const int _User_Notifications_reference_dtGid = 306;
        public static mdDataType User_Notifications_reference_dt
            {
            get { return Metadata.DataTypes[_User_Notifications_reference_dtGid]; }
            }
        public const string _varchar_30_dtName = "varchar(30)";        public const int _varchar_30_dtGid = 339;
        public static mdDataType varchar_30_dt
            {
            get { return Metadata.DataTypes[_varchar_30_dtGid]; }
            }
        public const string _PrintingFormTemplates_reference_dtName = "PrintingFormTemplates_reference";        public const int _PrintingFormTemplates_reference_dtGid = 348;
        public static mdDataType PrintingFormTemplates_reference_dt
            {
            get { return Metadata.DataTypes[_PrintingFormTemplates_reference_dtGid]; }
            }
        public const string _PrintingFormKinds_dtName = "PrintingFormKinds";        public const int _PrintingFormKinds_dtGid = 349;
        public static mdDataType PrintingFormKinds_dt
            {
            get { return Metadata.DataTypes[_PrintingFormKinds_dtGid]; }
            }
        public static class PrintingFormKinds_enum_
            {
            public const System.Byte Other = 199;
            public static mdConstant Other_const = PrintingFormKinds_dt.Constants[1465];
            public const System.Byte MainForm = 198;
            public static mdConstant MainForm_const = PrintingFormKinds_dt.Constants[2479];
            }
        public const string _Web_showcase_reference_dtName = "Web_showcase_reference";        public const int _Web_showcase_reference_dtGid = 352;
        public static mdDataType Web_showcase_reference_dt
            {
            get { return Metadata.DataTypes[_Web_showcase_reference_dtGid]; }
            }
        public const string _VerticalAlignment_dtName = "VerticalAlignment";        public const int _VerticalAlignment_dtGid = 353;
        public static mdDataType VerticalAlignment_dt
            {
            get { return Metadata.DataTypes[_VerticalAlignment_dtGid]; }
            }
        public const string _HorizontalAlignment_dtName = "HorizontalAlignment";        public const int _HorizontalAlignment_dtGid = 354;
        public static mdDataType HorizontalAlignment_dt
            {
            get { return Metadata.DataTypes[_HorizontalAlignment_dtGid]; }
            }
        public const string _EduTaskSolution_reference_dtName = "EduTaskSolution_reference";        public const int _EduTaskSolution_reference_dtGid = 358;
        public static mdDataType EduTaskSolution_reference_dt
            {
            get { return Metadata.DataTypes[_EduTaskSolution_reference_dtGid]; }
            }
        public const string _EduTeacher_reference_dtName = "EduTeacher_reference";        public const int _EduTeacher_reference_dtGid = 359;
        public static mdDataType EduTeacher_reference_dt
            {
            get { return Metadata.DataTypes[_EduTeacher_reference_dtGid]; }
            }
        public const string _EduTasksListKinds_reference_dtName = "EduTasksListKinds_reference";        public const int _EduTasksListKinds_reference_dtGid = 362;
        public static mdDataType EduTasksListKinds_reference_dt
            {
            get { return Metadata.DataTypes[_EduTasksListKinds_reference_dtGid]; }
            }
        public const string _EduMaterialsList_reference_dtName = "EduMaterialsList_reference";        public const int _EduMaterialsList_reference_dtGid = 365;
        public static mdDataType EduMaterialsList_reference_dt
            {
            get { return Metadata.DataTypes[_EduMaterialsList_reference_dtGid]; }
            }
        public const string _EduMaterials_reference_dtName = "EduMaterials_reference";        public const int _EduMaterials_reference_dtGid = 367;
        public static mdDataType EduMaterials_reference_dt
            {
            get { return Metadata.DataTypes[_EduMaterials_reference_dtGid]; }
            }
        public const string _EduMaterialTypes_dtName = "EduMaterialTypes";        public const int _EduMaterialTypes_dtGid = 370;
        public static mdDataType EduMaterialTypes_dt
            {
            get { return Metadata.DataTypes[_EduMaterialTypes_dtGid]; }
            }
        public static class EduMaterialTypes_enum_
            {
            public const System.Byte Theory = 1;
            public static mdConstant Theory_const = EduMaterialTypes_dt.Constants[2832];
            public const System.Byte Task = 2;
            public static mdConstant Task_const = EduMaterialTypes_dt.Constants[2833];
            public const System.Byte InterestingFacts = 3;
            public static mdConstant InterestingFacts_const = EduMaterialTypes_dt.Constants[2834];
            public const System.Byte Others = 9;
            public static mdConstant Others_const = EduMaterialTypes_dt.Constants[2835];
            }
        public const string _EduGroupKinds_reference_dtName = "EduGroupKinds_reference";        public const int _EduGroupKinds_reference_dtGid = 376;
        public static mdDataType EduGroupKinds_reference_dt
            {
            get { return Metadata.DataTypes[_EduGroupKinds_reference_dtGid]; }
            }
        public const string _GoodsShowcaseTypes_dtName = "GoodsShowcaseTypes";        public const int _GoodsShowcaseTypes_dtGid = 403;
        public static mdDataType GoodsShowcaseTypes_dt
            {
            get { return Metadata.DataTypes[_GoodsShowcaseTypes_dtGid]; }
            }
        public const string _Reports_Settings_reference_dtName = "Reports_Settings_reference";        public const int _Reports_Settings_reference_dtGid = 504;
        public static mdDataType Reports_Settings_reference_dt
            {
            get { return Metadata.DataTypes[_Reports_Settings_reference_dtGid]; }
            }
        public const string _Favorite_Report_Groups_reference_dtName = "Favorite_Report_Groups_reference";        public const int _Favorite_Report_Groups_reference_dtGid = 505;
        public static mdDataType Favorite_Report_Groups_reference_dt
            {
            get { return Metadata.DataTypes[_Favorite_Report_Groups_reference_dtGid]; }
            }
        public static class Favorite_Report_Groups_reference_defaults_
            {
            public const System.Int32 root_group = 1;
            public static mdConstant root_group_const = Favorite_Report_Groups_reference_dt.Constants[2371];
            }
        public const string _RolesEnum_dtName = "RolesEnum";        public const int _RolesEnum_dtGid = 506;
        public static mdDataType RolesEnum_dt
            {
            get { return Metadata.DataTypes[_RolesEnum_dtGid]; }
            }
        public const string _Gender_dtName = "Gender";        public const int _Gender_dtGid = 520;
        public static mdDataType Gender_dt
            {
            get { return Metadata.DataTypes[_Gender_dtGid]; }
            }
        public static class Gender_enum_
            {
            public const System.Byte Man = 1;
            public static mdConstant Man_const = Gender_dt.Constants[2246];
            public const System.Byte Woman = 2;
            public static mdConstant Woman_const = Gender_dt.Constants[2247];
            }
        public const string _Persons_Message_Types_dtName = "Persons_Message_Types";        public const int _Persons_Message_Types_dtGid = 528;
        public static mdDataType Persons_Message_Types_dt
            {
            get { return Metadata.DataTypes[_Persons_Message_Types_dtGid]; }
            }
        public static class Persons_Message_Types_enum_
            {
            public const System.Byte General = 0;
            public static mdConstant General_const = Persons_Message_Types_dt.Constants[2268];
            public const System.Byte ByKPI = 1;
            public static mdConstant ByKPI_const = Persons_Message_Types_dt.Constants[2269];
            public const System.Byte Private = 2;
            public static mdConstant Private_const = Persons_Message_Types_dt.Constants[2620];
            }
        public const string _InternalJournals_referense_dtName = "InternalJournals_referense";        public const int _InternalJournals_referense_dtGid = 529;
        public static mdDataType InternalJournals_referense_dt
            {
            get { return Metadata.DataTypes[_InternalJournals_referense_dtGid]; }
            }
        public const string _SocNetDefs_reference_dtName = "SocNetDefs_reference";        public const int _SocNetDefs_reference_dtGid = 532;
        public static mdDataType SocNetDefs_reference_dt
            {
            get { return Metadata.DataTypes[_SocNetDefs_reference_dtGid]; }
            }
        public const string _SocNetProfiles_reference_dtName = "SocNetProfiles_reference";        public const int _SocNetProfiles_reference_dtGid = 533;
        public static mdDataType SocNetProfiles_reference_dt
            {
            get { return Metadata.DataTypes[_SocNetProfiles_reference_dtGid]; }
            }
        public const string _UserSessions_reference_dtName = "UserSessions_reference";        public const int _UserSessions_reference_dtGid = 534;
        public static mdDataType UserSessions_reference_dt
            {
            get { return Metadata.DataTypes[_UserSessions_reference_dtGid]; }
            }
        public const string _nvarchar_1000_dtName = "nvarchar(1000)";        public const int _nvarchar_1000_dtGid = 535;
        public static mdDataType nvarchar_1000_dt
            {
            get { return Metadata.DataTypes[_nvarchar_1000_dtGid]; }
            }
        public const string _Attachable_Objects_dtName = "Attachable_Objects";        public const int _Attachable_Objects_dtGid = 544;
        public static mdDataType Attachable_Objects_dt
            {
            get { return Metadata.DataTypes[_Attachable_Objects_dtGid]; }
            }
        public const string _FinProjects_reference_dtName = "FinProjects_reference";        public const int _FinProjects_reference_dtGid = 545;
        public static mdDataType FinProjects_reference_dt
            {
            get { return Metadata.DataTypes[_FinProjects_reference_dtGid]; }
            }
        public const string _nvarchar_2000_dtName = "nvarchar(2000)";        public const int _nvarchar_2000_dtGid = 547;
        public static mdDataType nvarchar_2000_dt
            {
            get { return Metadata.DataTypes[_nvarchar_2000_dtGid]; }
            }
        public const string _statuses_table_dtName = "statuses_table";        public const int _statuses_table_dtGid = 548;
        public static mdDataType statuses_table_dt
            {
            get { return Metadata.DataTypes[_statuses_table_dtGid]; }
            }
        public const string _FinProjects_Group_reference_dtName = "FinProjects_Group_reference";        public const int _FinProjects_Group_reference_dtGid = 560;
        public static mdDataType FinProjects_Group_reference_dt
            {
            get { return Metadata.DataTypes[_FinProjects_Group_reference_dtGid]; }
            }
        public const string _PhysicalPersonsData_reference_dtName = "PhysicalPersonsData_reference";        public const int _PhysicalPersonsData_reference_dtGid = 568;
        public static mdDataType PhysicalPersonsData_reference_dt
            {
            get { return Metadata.DataTypes[_PhysicalPersonsData_reference_dtGid]; }
            }
        public const string _ProjectAssets_reference_dtName = "ProjectAssets_reference";        public const int _ProjectAssets_reference_dtGid = 569;
        public static mdDataType ProjectAssets_reference_dt
            {
            get { return Metadata.DataTypes[_ProjectAssets_reference_dtGid]; }
            }
        public const string _ProjectAssetsTypes_dtName = "ProjectAssetsTypes";        public const int _ProjectAssetsTypes_dtGid = 570;
        public static mdDataType ProjectAssetsTypes_dt
            {
            get { return Metadata.DataTypes[_ProjectAssetsTypes_dtGid]; }
            }
        public static class ProjectAssetsTypes_enum_
            {
            public const System.Byte Flat = 1;
            public static mdConstant Flat_const = ProjectAssetsTypes_dt.Constants[2453];
            public const System.Byte NonResidentalApartment = 4;
            public static mdConstant NonResidentalApartment_const = ProjectAssetsTypes_dt.Constants[2454];
            public const System.Byte CarPlace = 6;
            public static mdConstant CarPlace_const = ProjectAssetsTypes_dt.Constants[2455];
            public const System.Byte Others = 99;
            public static mdConstant Others_const = ProjectAssetsTypes_dt.Constants[2456];
            }
        public const string _Numeric_12s3NN_dtName = "Numeric(12,3)NN";        public const int _Numeric_12s3NN_dtGid = 571;
        public static mdDataType Numeric_12s3NN_dt
            {
            get { return Metadata.DataTypes[_Numeric_12s3NN_dtGid]; }
            }
        public const string _DocumentationKinds_reference_dtName = "DocumentationKinds_reference";        public const int _DocumentationKinds_reference_dtGid = 579;
        public static mdDataType DocumentationKinds_reference_dt
            {
            get { return Metadata.DataTypes[_DocumentationKinds_reference_dtGid]; }
            }
        public const string _DocumentationsOfProjects_reference_dtName = "DocumentationsOfProjects_reference";        public const int _DocumentationsOfProjects_reference_dtGid = 580;
        public static mdDataType DocumentationsOfProjects_reference_dt
            {
            get { return Metadata.DataTypes[_DocumentationsOfProjects_reference_dtGid]; }
            }
        public const string _PhoneNumbers_reference_dtName = "PhoneNumbers_reference";        public const int _PhoneNumbers_reference_dtGid = 590;
        public static mdDataType PhoneNumbers_reference_dt
            {
            get { return Metadata.DataTypes[_PhoneNumbers_reference_dtGid]; }
            }
        public const string _PhoneOperators_reference_dtName = "PhoneOperators_reference";        public const int _PhoneOperators_reference_dtGid = 591;
        public static mdDataType PhoneOperators_reference_dt
            {
            get { return Metadata.DataTypes[_PhoneOperators_reference_dtGid]; }
            }
        public const string _PhoneOwners_reference_dtName = "PhoneOwners_reference";        public const int _PhoneOwners_reference_dtGid = 592;
        public static mdDataType PhoneOwners_reference_dt
            {
            get { return Metadata.DataTypes[_PhoneOwners_reference_dtGid]; }
            }
        public const string _MobileDevices_reference_dtName = "MobileDevices_reference";        public const int _MobileDevices_reference_dtGid = 593;
        public static mdDataType MobileDevices_reference_dt
            {
            get { return Metadata.DataTypes[_MobileDevices_reference_dtGid]; }
            }
        public const string _DocumentationTypes_dtName = "DocumentationTypes";        public const int _DocumentationTypes_dtGid = 594;
        public static mdDataType DocumentationTypes_dt
            {
            get { return Metadata.DataTypes[_DocumentationTypes_dtGid]; }
            }
        public static class DocumentationTypes_enum_
            {
            public const System.Byte Contract = 1;
            public static mdConstant Contract_const = DocumentationTypes_dt.Constants[2551];
            public const System.Byte MontageSchema = 21;
            public static mdConstant MontageSchema_const = DocumentationTypes_dt.Constants[2552];
            public const System.Byte ElementDrawing = 24;
            public static mdConstant ElementDrawing_const = DocumentationTypes_dt.Constants[2553];
            public const System.Byte Others = 99;
            public static mdConstant Others_const = DocumentationTypes_dt.Constants[2555];
            }
        public const string _Payments_Shedule_reference_dtName = "Payments_Shedule_reference";        public const int _Payments_Shedule_reference_dtGid = 595;
        public static mdDataType Payments_Shedule_reference_dt
            {
            get { return Metadata.DataTypes[_Payments_Shedule_reference_dtGid]; }
            }
        public const string _RegularPayments_reference_dtName = "RegularPayments_reference";        public const int _RegularPayments_reference_dtGid = 597;
        public static mdDataType RegularPayments_reference_dt
            {
            get { return Metadata.DataTypes[_RegularPayments_reference_dtGid]; }
            }
        public const string _ApproveStates_dtName = "ApproveStates";        public const int _ApproveStates_dtGid = 598;
        public static mdDataType ApproveStates_dt
            {
            get { return Metadata.DataTypes[_ApproveStates_dtGid]; }
            }
        public static class ApproveStates_enum_
            {
            public const System.Byte NotSpecified = 0;
            public static mdConstant NotSpecified_const = ApproveStates_dt.Constants[2565];
            public const System.Byte Approved = 1;
            public static mdConstant Approved_const = ApproveStates_dt.Constants[2567];
            public const System.Byte Rejected = 2;
            public static mdConstant Rejected_const = ApproveStates_dt.Constants[2569];
            public const System.Byte ApprovingExpected = 3;
            public static mdConstant ApprovingExpected_const = ApproveStates_dt.Constants[2651];
            }
        public const string _UserProfiles_dtName = "UserProfiles";        public const int _UserProfiles_dtGid = 716;
        public static mdDataType UserProfiles_dt
            {
            get { return Metadata.DataTypes[_UserProfiles_dtGid]; }
            }
        public const string _UserConnectionTypes_dtName = "UserConnectionTypes";        public const int _UserConnectionTypes_dtGid = 717;
        public static mdDataType UserConnectionTypes_dt
            {
            get { return Metadata.DataTypes[_UserConnectionTypes_dtGid]; }
            }
        public static class UserConnectionTypes_enum_
            {
            public const System.Byte DesktopConsole = 1;
            public static mdConstant DesktopConsole_const = UserConnectionTypes_dt.Constants[2607];
            public const System.Byte ExternalDesktop = 2;
            public static mdConstant ExternalDesktop_const = UserConnectionTypes_dt.Constants[2608];
            public const System.Byte ExternalServer = 3;
            public static mdConstant ExternalServer_const = UserConnectionTypes_dt.Constants[2609];
            public const System.Byte LocalServer = 4;
            public static mdConstant LocalServer_const = UserConnectionTypes_dt.Constants[2611];
            public const System.Byte API_Client = 11;
            public static mdConstant API_Client_const = UserConnectionTypes_dt.Constants[2613];
            public const System.Byte WebServer = 21;
            public static mdConstant WebServer_const = UserConnectionTypes_dt.Constants[2615];
            public const System.Byte MobileApp = 31;
            public static mdConstant MobileApp_const = UserConnectionTypes_dt.Constants[2617];
            }
        public const string _AnyReference_dtName = "AnyReference";        public const int _AnyReference_dtGid = 1000;
        public static mdDataType AnyReference_dt
            {
            get { return Metadata.DataTypes[_AnyReference_dtGid]; }
            }
        public static class AnyReference_defaults_
            {
            }
        public const string _company_persons_reference_dtName = "company_persons_reference";        public const int _company_persons_reference_dtGid = 1001;
        public static mdDataType company_persons_reference_dt
            {
            get { return Metadata.DataTypes[_company_persons_reference_dtGid]; }
            }
        public const string _contragents_reference_dtName = "contragents_reference";        public const int _contragents_reference_dtGid = 1003;
        public static mdDataType contragents_reference_dt
            {
            get { return Metadata.DataTypes[_contragents_reference_dtGid]; }
            }
        public const string _documents_reference_dtName = "documents_reference";        public const int _documents_reference_dtGid = 1006;
        public static mdDataType documents_reference_dt
            {
            get { return Metadata.DataTypes[_documents_reference_dtGid]; }
            }
        public const string _Contract_reference_dtName = "Contract_reference";        public const int _Contract_reference_dtGid = 1058;
        public static mdDataType Contract_reference_dt
            {
            get { return Metadata.DataTypes[_Contract_reference_dtGid]; }
            }
        public const string _date_dtName = "date";        public const int _date_dtGid = 1059;
        public static mdDataType date_dt
            {
            get { return Metadata.DataTypes[_date_dtGid]; }
            }
        public const string _time_dtName = "time";        public const int _time_dtGid = 1060;
        public static mdDataType time_dt
            {
            get { return Metadata.DataTypes[_time_dtGid]; }
            }
        public const string _money_accounts_reference_dtName = "money_accounts_reference";        public const int _money_accounts_reference_dtGid = 1081;
        public static mdDataType money_accounts_reference_dt
            {
            get { return Metadata.DataTypes[_money_accounts_reference_dtGid]; }
            }
        public const string _companies_reference_dtName = "companies_reference";        public const int _companies_reference_dtGid = 1082;
        public static mdDataType companies_reference_dt
            {
            get { return Metadata.DataTypes[_companies_reference_dtGid]; }
            }
        public const string _Numeric_12s3_dtName = "Numeric(12,3)";        public const int _Numeric_12s3_dtGid = 1224;
        public static mdDataType Numeric_12s3_dt
            {
            get { return Metadata.DataTypes[_Numeric_12s3_dtGid]; }
            }
        public const string _SQL_datatypes_dtName = "SQL_datatypes";        public const int _SQL_datatypes_dtGid = 1225;
        public static mdDataType SQL_datatypes_dt
            {
            get { return Metadata.DataTypes[_SQL_datatypes_dtGid]; }
            }
        public static class SQL_datatypes_enum_
            {
            public const System.String _varchar = "varchar";
            public static mdConstant _varchar_const = SQL_datatypes_dt.Constants[176];
            public const System.String _Int = "Int";
            public static mdConstant _Int_const = SQL_datatypes_dt.Constants[410];
            }
        public const string _contragent_persons_reference_dtName = "contragent_persons_reference";        public const int _contragent_persons_reference_dtGid = 1226;
        public static mdDataType contragent_persons_reference_dt
            {
            get { return Metadata.DataTypes[_contragent_persons_reference_dtGid]; }
            }
        public const string _days_of_week_dtName = "days_of_week";        public const int _days_of_week_dtGid = 1232;
        public static mdDataType days_of_week_dt
            {
            get { return Metadata.DataTypes[_days_of_week_dtGid]; }
            }
        public static class days_of_week_enum_
            {
            public const System.Byte _1 = 1;
            public static mdConstant _1_const = days_of_week_dt.Constants[254];
            public const System.Byte _2 = 2;
            public static mdConstant _2_const = days_of_week_dt.Constants[255];
            public const System.Byte _3 = 3;
            public static mdConstant _3_const = days_of_week_dt.Constants[256];
            public const System.Byte _4 = 4;
            public static mdConstant _4_const = days_of_week_dt.Constants[257];
            public const System.Byte _5 = 5;
            public static mdConstant _5_const = days_of_week_dt.Constants[258];
            public const System.Byte _6 = 6;
            public static mdConstant _6_const = days_of_week_dt.Constants[259];
            public const System.Byte _7 = 7;
            public static mdConstant _7_const = days_of_week_dt.Constants[260];
            }
        public const string _SystemEventTypes_reference_dtName = "SystemEventTypes_reference";        public const int _SystemEventTypes_reference_dtGid = 1235;
        public static mdDataType SystemEventTypes_reference_dt
            {
            get { return Metadata.DataTypes[_SystemEventTypes_reference_dtGid]; }
            }
        public static class SystemEventTypes_reference_defaults_
            {
            public const System.Int16 _15 = 15;
            public static mdConstant _15_const = SystemEventTypes_reference_dt.Constants[405];
            public const System.Int16 _16 = 16;
            public static mdConstant _16_const = SystemEventTypes_reference_dt.Constants[406];
            public const System.Int16 _17 = 17;
            public static mdConstant _17_const = SystemEventTypes_reference_dt.Constants[407];
            }
        public const string _Int_non_negative_dtName = "Int_non_negative";        public const int _Int_non_negative_dtGid = 1246;
        public static mdDataType Int_non_negative_dt
            {
            get { return Metadata.DataTypes[_Int_non_negative_dtGid]; }
            }
        public const string _Numeric14d2NN_dtName = "Numeric14.2NN";        public const int _Numeric14d2NN_dtGid = 1247;
        public static mdDataType Numeric14d2NN_dt
            {
            get { return Metadata.DataTypes[_Numeric14d2NN_dtGid]; }
            }
        public const string _Numeric12d2_dtName = "Numeric12.2";        public const int _Numeric12d2_dtGid = 1252;
        public static mdDataType Numeric12d2_dt
            {
            get { return Metadata.DataTypes[_Numeric12d2_dtGid]; }
            }
        public const string _types_of_money_account_dtName = "types_of_money_account";        public const int _types_of_money_account_dtGid = 1262;
        public static mdDataType types_of_money_account_dt
            {
            get { return Metadata.DataTypes[_types_of_money_account_dtGid]; }
            }
        public static class types_of_money_account_enum_
            {
            public const System.Byte Cash = 1;
            public static mdConstant Cash_const = types_of_money_account_dt.Constants[308];
            public const System.Byte ClearingAccount = 2;
            public static mdConstant ClearingAccount_const = types_of_money_account_dt.Constants[309];
            }
        public const string _Numeric12d2NN_dtName = "Numeric12.2NN";        public const int _Numeric12d2NN_dtGid = 1279;
        public static mdDataType Numeric12d2NN_dt
            {
            get { return Metadata.DataTypes[_Numeric12d2NN_dtGid]; }
            }
        public const string _contragent_groups_reference_dtName = "contragent_groups_reference";        public const int _contragent_groups_reference_dtGid = 1293;
        public static mdDataType contragent_groups_reference_dt
            {
            get { return Metadata.DataTypes[_contragent_groups_reference_dtGid]; }
            }
        public const string _e_mail_templates_reference_dtName = "e_mail_templates_reference";        public const int _e_mail_templates_reference_dtGid = 1323;
        public static mdDataType e_mail_templates_reference_dt
            {
            get { return Metadata.DataTypes[_e_mail_templates_reference_dtGid]; }
            }
        public const string _e_mail_packages_reference_dtName = "e_mail_packages_reference";        public const int _e_mail_packages_reference_dtGid = 1324;
        public static mdDataType e_mail_packages_reference_dt
            {
            get { return Metadata.DataTypes[_e_mail_packages_reference_dtGid]; }
            }
        public const string _E_mail_letter_types_dtName = "E_mail_letter_types";        public const int _E_mail_letter_types_dtGid = 1325;
        public static mdDataType E_mail_letter_types_dt
            {
            get { return Metadata.DataTypes[_E_mail_letter_types_dtGid]; }
            }
        public static class E_mail_letter_types_enum_
            {
            public const System.Byte _99 = 99;
            public static mdConstant _99_const = E_mail_letter_types_dt.Constants[490];
            public const System.Byte RegisterWebUser = 21;
            public static mdConstant RegisterWebUser_const = E_mail_letter_types_dt.Constants[2289];
            public const System.Byte UpdateWebUser = 22;
            public static mdConstant UpdateWebUser_const = E_mail_letter_types_dt.Constants[2290];
            public const System.Byte PasswordRemind = 24;
            public static mdConstant PasswordRemind_const = E_mail_letter_types_dt.Constants[2291];
            public const System.Byte EventSubscription = 31;
            public static mdConstant EventSubscription_const = E_mail_letter_types_dt.Constants[2292];
            public const System.Byte QuestionnaireInvitation = 29;
            public static mdConstant QuestionnaireInvitation_const = E_mail_letter_types_dt.Constants[2723];
            }
        public const string _SentResultTypes_dtName = "SentResultTypes";        public const int _SentResultTypes_dtGid = 1328;
        public static mdDataType SentResultTypes_dt
            {
            get { return Metadata.DataTypes[_SentResultTypes_dtGid]; }
            }
        public static class SentResultTypes_enum_
            {
            public const System.Int32 _0 = 0;
            public static mdConstant _0_const = SentResultTypes_dt.Constants[132];
            public const System.Int32 _1 = 1;
            public static mdConstant _1_const = SentResultTypes_dt.Constants[137];
            public const System.Int32 _2 = 2;
            public static mdConstant _2_const = SentResultTypes_dt.Constants[504];
            }
        public const string _Char_6_dtName = "Char(6)";        public const int _Char_6_dtGid = 1339;
        public static mdDataType Char_6_dt
            {
            get { return Metadata.DataTypes[_Char_6_dtGid]; }
            }
        public const string _Enterprise_Departments_reference_dtName = "Enterprise_Departments_reference";        public const int _Enterprise_Departments_reference_dtGid = 2028;
        public static mdDataType Enterprise_Departments_reference_dt
            {
            get { return Metadata.DataTypes[_Enterprise_Departments_reference_dtGid]; }
            }
        public const string _Resource_Flow_Items_reference_dtName = "Resource_Flow_Items_reference";        public const int _Resource_Flow_Items_reference_dtGid = 2029;
        public static mdDataType Resource_Flow_Items_reference_dt
            {
            get { return Metadata.DataTypes[_Resource_Flow_Items_reference_dtGid]; }
            }
        public const string _Resource_Flow_Item_Groups_reference_dtName = "Resource_Flow_Item_Groups_reference";        public const int _Resource_Flow_Item_Groups_reference_dtGid = 2030;
        public static mdDataType Resource_Flow_Item_Groups_reference_dt
            {
            get { return Metadata.DataTypes[_Resource_Flow_Item_Groups_reference_dtGid]; }
            }
        public const string _Numeric7d2_dtName = "Numeric7.2";        public const int _Numeric7d2_dtGid = 2037;
        public static mdDataType Numeric7d2_dt
            {
            get { return Metadata.DataTypes[_Numeric7d2_dtGid]; }
            }
        public const string _contragents_money_accounts_reference_dtName = "contragents_money_accounts_reference";        public const int _contragents_money_accounts_reference_dtGid = 2043;
        public static mdDataType contragents_money_accounts_reference_dt
            {
            get { return Metadata.DataTypes[_contragents_money_accounts_reference_dtGid]; }
            }
        public const string _Banks_reference_dtName = "Banks_reference";        public const int _Banks_reference_dtGid = 2044;
        public static mdDataType Banks_reference_dt
            {
            get { return Metadata.DataTypes[_Banks_reference_dtGid]; }
            }
        public const string _types_of_contract_reference_dtName = "types_of_contract_reference";        public const int _types_of_contract_reference_dtGid = 2055;
        public static mdDataType types_of_contract_reference_dt
            {
            get { return Metadata.DataTypes[_types_of_contract_reference_dtGid]; }
            }
        public const string _BalanseAccounts_reference_dtName = "BalanseAccounts_reference";        public const int _BalanseAccounts_reference_dtGid = 2064;
        public static mdDataType BalanseAccounts_reference_dt
            {
            get { return Metadata.DataTypes[_BalanseAccounts_reference_dtGid]; }
            }
        public const string _RegisterTypes_dtName = "RegisterTypes";        public const int _RegisterTypes_dtGid = 2069;
        public static mdDataType RegisterTypes_dt
            {
            get { return Metadata.DataTypes[_RegisterTypes_dtGid]; }
            }
        public static class RegisterTypes_enum_
            {
            public const System.Byte MoneyRest = 5;
            public static mdConstant MoneyRest_const = RegisterTypes_dt.Constants[613];
            public const System.Byte SettlementsWithContragents = 7;
            public static mdConstant SettlementsWithContragents_const = RegisterTypes_dt.Constants[614];
            public const System.Byte OtherEnterpriseResources = 11;
            public static mdConstant OtherEnterpriseResources_const = RegisterTypes_dt.Constants[616];
            }
        public const string _balance_subconto_reference_dtName = "balance_subconto_reference";        public const int _balance_subconto_reference_dtGid = 2077;
        public static mdDataType balance_subconto_reference_dt
            {
            get { return Metadata.DataTypes[_balance_subconto_reference_dtGid]; }
            }
        public const string _Budjet_Journal_reference_dtName = "Budjet_Journal_reference";        public const int _Budjet_Journal_reference_dtGid = 2080;
        public static mdDataType Budjet_Journal_reference_dt
            {
            get { return Metadata.DataTypes[_Budjet_Journal_reference_dtGid]; }
            }
        public const string _Fin_Documents_dtName = "Fin_Documents";        public const int _Fin_Documents_dtGid = 2081;
        public static mdDataType Fin_Documents_dt
            {
            get { return Metadata.DataTypes[_Fin_Documents_dtGid]; }
            }
        public const string _Periodicity_types_dtName = "Periodicity_types";        public const int _Periodicity_types_dtGid = 2096;
        public static mdDataType Periodicity_types_dt
            {
            get { return Metadata.DataTypes[_Periodicity_types_dtGid]; }
            }
        public static class Periodicity_types_enum_
            {
            public const System.Byte _1 = 1;
            public static mdConstant _1_const = Periodicity_types_dt.Constants[927];
            public const System.Byte _2 = 2;
            public static mdConstant _2_const = Periodicity_types_dt.Constants[928];
            public const System.Byte _4 = 4;
            public static mdConstant _4_const = Periodicity_types_dt.Constants[929];
            public const System.Byte _8 = 8;
            public static mdConstant _8_const = Periodicity_types_dt.Constants[930];
            public const System.Byte _16 = 16;
            public static mdConstant _16_const = Periodicity_types_dt.Constants[2562];
            }
        public const string _Numeric14d2_dtName = "Numeric14.2";        public const int _Numeric14d2_dtGid = 2100;
        public static mdDataType Numeric14d2_dt
            {
            get { return Metadata.DataTypes[_Numeric14d2_dtGid]; }
            }
        public const string _SourceDocument_Statuses_dtName = "SourceDocument_Statuses";        public const int _SourceDocument_Statuses_dtGid = 2103;
        public static mdDataType SourceDocument_Statuses_dt
            {
            get { return Metadata.DataTypes[_SourceDocument_Statuses_dtGid]; }
            }
        public static class SourceDocument_Statuses_enum_
            {
            public const System.Byte _1 = 1;
            public static mdConstant _1_const = SourceDocument_Statuses_dt.Constants[945];
            public const System.Byte _16 = 16;
            public static mdConstant _16_const = SourceDocument_Statuses_dt.Constants[946];
            public const System.Byte _4 = 4;
            public static mdConstant _4_const = SourceDocument_Statuses_dt.Constants[947];
            public const System.Byte _8 = 8;
            public static mdConstant _8_const = SourceDocument_Statuses_dt.Constants[948];
            }
        public const string _CRM_Events_Types_reference_dtName = "CRM_Events_Types_reference";        public const int _CRM_Events_Types_reference_dtGid = 4008;
        public static mdDataType CRM_Events_Types_reference_dt
            {
            get { return Metadata.DataTypes[_CRM_Events_Types_reference_dtGid]; }
            }
        public const string _PrintingFormsTemplates_Sets_reference_dtName = "PrintingFormsTemplates_Sets_reference";        public const int _PrintingFormsTemplates_Sets_reference_dtGid = 4010;
        public static mdDataType PrintingFormsTemplates_Sets_reference_dt
            {
            get { return Metadata.DataTypes[_PrintingFormsTemplates_Sets_reference_dtGid]; }
            }
        public const string _types_of_attachments_reference_dtName = "types_of_attachments_reference";        public const int _types_of_attachments_reference_dtGid = 5139;
        public static mdDataType types_of_attachments_reference_dt
            {
            get { return Metadata.DataTypes[_types_of_attachments_reference_dtGid]; }
            }
        public const string _Planer_Events_Journal_reference_dtName = "Planer_Events_Journal_reference";        public const int _Planer_Events_Journal_reference_dtGid = 7328;
        public static mdDataType Planer_Events_Journal_reference_dt
            {
            get { return Metadata.DataTypes[_Planer_Events_Journal_reference_dtGid]; }
            }
        public const string _Priority_Levels_dtName = "Priority_Levels";        public const int _Priority_Levels_dtGid = 7342;
        public static mdDataType Priority_Levels_dt
            {
            get { return Metadata.DataTypes[_Priority_Levels_dtGid]; }
            }
        public static class Priority_Levels_enum_
            {
            public const System.Byte _1 = 1;
            public static mdConstant _1_const = Priority_Levels_dt.Constants[15150];
            public const System.Byte _4 = 4;
            public static mdConstant _4_const = Priority_Levels_dt.Constants[15151];
            public const System.Byte _10 = 10;
            public static mdConstant _10_const = Priority_Levels_dt.Constants[15152];
            public const System.Byte _20 = 20;
            public static mdConstant _20_const = Priority_Levels_dt.Constants[15153];
            public const System.Byte _40 = 40;
            public static mdConstant _40_const = Priority_Levels_dt.Constants[15154];
            }

    }

}
