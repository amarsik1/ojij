using System;
using AS_MetaData;

namespace MD_Schema
{

    /// <summary>
    /// Схема таблиці 'Закріплення пристроїв за відповідальними особами'
    /// </summary>
    public static class DevicesResponsiblePersons_TS_   
    {
        public const int __tGid = 15742;
        public const string __tName = "DevicesResponsiblePersons";
        static mdTable m_mdTable;
        static DevicesResponsiblePersons_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _MobileDevice_fGid = 30996;
       
        public static mdTableField MobileDevice
        {
            get { return __MD.Fields[_MobileDevice_fGid];}
        }
        public const string _MobileDevice_fName = "MobileDevice";        public const int _ResponsiblePerson_fGid = 30997;
       
        public static mdTableField ResponsiblePerson
        {
            get { return __MD.Fields[_ResponsiblePerson_fGid];}
        }
        public const string _ResponsiblePerson_fName = "ResponsiblePerson";        public const int _PhoneDeviceID_fGid = 30999;
       
        public static mdTableField PhoneDeviceID
        {
            get { return __MD.Fields[_PhoneDeviceID_fGid];}
        }
        public const string _PhoneDeviceID_fName = "PhoneDeviceID";        public const int _SerialNo_fGid = 31000;
       
        public static mdTableField SerialNo
        {
            get { return __MD.Fields[_SerialNo_fGid];}
        }
        public const string _SerialNo_fName = "SerialNo";       
        public static mdTableField MobileDevice_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_MobileDevice_fGid]);}
        }
        public const string _MobileDevice_view__fName = "MobileDevice_view_";       
        public static mdTableField ResponsiblePerson_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_ResponsiblePerson_fGid]);}
        }
        public const string _ResponsiblePerson_view__fName = "ResponsiblePerson_view_";       
        public static mdTableField MobileDevice_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_MobileDevice_fGid]);}
        }
        public const string _MobileDevice_status__fName = "MobileDevice_status_";       
        public static mdTableField ResponsiblePerson_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_ResponsiblePerson_fGid]);}
        }
        public const string _ResponsiblePerson_status__fName = "ResponsiblePerson_status_";
    }
    /// <summary>
    /// Схема таблиці 'Діти особи'
    /// </summary>
    public static class PersonsChildren_TS_   
    {
        public const int __tGid = 15918;
        public const string __tName = "PersonsChildren";
        static mdTable m_mdTable;
        static PersonsChildren_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _ParentPerson_fGid = 34344;
       
        public static mdTableField ParentPerson
        {
            get { return __MD.Fields[_ParentPerson_fGid];}
        }
        public const string _ParentPerson_fName = "ParentPerson";        public const int _ChildPerson_fGid = 34345;
       
        public static mdTableField ChildPerson
        {
            get { return __MD.Fields[_ChildPerson_fGid];}
        }
        public const string _ChildPerson_fName = "ChildPerson";       
        public static mdTableField ParentPerson_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_ParentPerson_fGid]);}
        }
        public const string _ParentPerson_view__fName = "ParentPerson_view_";       
        public static mdTableField ChildPerson_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_ChildPerson_fGid]);}
        }
        public const string _ChildPerson_view__fName = "ChildPerson_view_";       
        public static mdTableField ParentPerson_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_ParentPerson_fGid]);}
        }
        public const string _ParentPerson_status__fName = "ParentPerson_status_";       
        public static mdTableField ChildPerson_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_ChildPerson_fGid]);}
        }
        public const string _ChildPerson_status__fName = "ChildPerson_status_";
    }
    /// <summary>
    /// Схема таблиці 'Відповідність номерів телефонів'
    /// </summary>
    public static class PhoneOwnersMap_TS_   
    {
        public const int __tGid = 15678;
        public const string __tName = "PhoneOwnersMap";
        static mdTable m_mdTable;
        static PhoneOwnersMap_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _PhoneNumber_fGid = 29606;
       
        public static mdTableField PhoneNumber
        {
            get { return __MD.Fields[_PhoneNumber_fGid];}
        }
        public const string _PhoneNumber_fName = "PhoneNumber";        public const int _OwnerRef_fGid = 29607;
       
        public static mdTableField OwnerRef
        {
            get { return __MD.Fields[_OwnerRef_fGid];}
        }
        public const string _OwnerRef_fName = "OwnerRef";        public const int _OwnerRef_tid_fGid = 29608;
       
        public static mdTableField OwnerRef_tid
        {
            get { return __MD.Fields[_OwnerRef_tid_fGid];}
        }
        public const string _OwnerRef_tid_fName = "OwnerRef_tid";        public const int _OwnerName_fGid = 29609;
       
        public static mdTableField OwnerName
        {
            get { return __MD.Fields[_OwnerName_fGid];}
        }
        public const string _OwnerName_fName = "OwnerName";        public const int _comments_fGid = 29610;
       
        public static mdTableField comments
        {
            get { return __MD.Fields[_comments_fGid];}
        }
        public const string _comments_fName = "comments";       
        public static mdTableField PhoneNumber_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_PhoneNumber_fGid]);}
        }
        public const string _PhoneNumber_view__fName = "PhoneNumber_view_";       
        public static mdTableField PhoneNumber_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_PhoneNumber_fGid]);}
        }
        public const string _PhoneNumber_status__fName = "PhoneNumber_status_";       
        public static mdTableField OwnerRef_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_OwnerRef_fGid]);}
        }
        public const string _OwnerRef_view__fName = "OwnerRef_view_";        public const int _OwnerRef_view_companies__fGid = 99000317;
       
        public static mdTableField OwnerRef_view_companies_
        {
            get { return __MD.Fields[_OwnerRef_view_companies__fGid];}
        }
        public const string _OwnerRef_view_companies__fName = "OwnerRef_view_companies_";        public const int _OwnerRef_view_company_persons__fGid = 99000318;
       
        public static mdTableField OwnerRef_view_company_persons_
        {
            get { return __MD.Fields[_OwnerRef_view_company_persons__fGid];}
        }
        public const string _OwnerRef_view_company_persons__fName = "OwnerRef_view_company_persons_";        public const int _OwnerRef_view_contragents__fGid = 99000319;
       
        public static mdTableField OwnerRef_view_contragents_
        {
            get { return __MD.Fields[_OwnerRef_view_contragents__fGid];}
        }
        public const string _OwnerRef_view_contragents__fName = "OwnerRef_view_contragents_";        public const int _OwnerRef_view_contragent_persons__fGid = 99000320;
       
        public static mdTableField OwnerRef_view_contragent_persons_
        {
            get { return __MD.Fields[_OwnerRef_view_contragent_persons__fGid];}
        }
        public const string _OwnerRef_view_contragent_persons__fName = "OwnerRef_view_contragent_persons_";        public const int _OwnerRef_view_Enterprise_Departments__fGid = 99000321;
       
        public static mdTableField OwnerRef_view_Enterprise_Departments_
        {
            get { return __MD.Fields[_OwnerRef_view_Enterprise_Departments__fGid];}
        }
        public const string _OwnerRef_view_Enterprise_Departments__fName = "OwnerRef_view_Enterprise_Departments_";        public const int _OwnerRef_view_Persons__fGid = 99000322;
       
        public static mdTableField OwnerRef_view_Persons_
        {
            get { return __MD.Fields[_OwnerRef_view_Persons__fGid];}
        }
        public const string _OwnerRef_view_Persons__fName = "OwnerRef_view_Persons_";
    }
    /// <summary>
    /// Схема таблиці 'CRM - події'
    /// </summary>
    public static class CRM_Events_TS_   
    {
        public const int __tGid = 5016;
        public const string __tName = "CRM_Events";
        static mdTable m_mdTable;
        static CRM_Events_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _Operation_fGid = 22234;
       
        public static mdTableField Operation
        {
            get { return __MD.Fields[_Operation_fGid];}
        }
        public const string _Operation_fName = "Operation";       
        public static mdTableField Operation_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Operation_fGid]);}
        }
        public const string _Operation_view__fName = "Operation_view_";       
        public static mdTableField Operation_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Operation_fGid]);}
        }
        public const string _Operation_status__fName = "Operation_status_";
    }
    /// <summary>
    /// Схема таблиці 'Типи CRM-подій'
    /// </summary>
    public static class CRM_Events_Types_TS_   
    {
        public const int __tGid = 5017;
        public const string __tName = "CRM_Events_Types";
        static mdTable m_mdTable;
        static CRM_Events_Types_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _gid_fGid = 20293;
       
        public static mdTableField gid
        {
            get { return __MD.Fields[_gid_fGid];}
        }
        public const string _gid_fName = "gid";        public const int _name_fGid = 20294;
       
        public static mdTableField name
        {
            get { return __MD.Fields[_name_fGid];}
        }
        public const string _name_fName = "name";
    }
    /// <summary>
    /// Схема таблиці 'CRM - Задачі'
    /// </summary>
    public static class CRM_Tasks_TS_   
    {
        public const int __tGid = 15156;
        public const string __tName = "CRM_Tasks";
        static mdTable m_mdTable;
        static CRM_Tasks_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _Operation_fGid = 22222;
       
        public static mdTableField Operation
        {
            get { return __MD.Fields[_Operation_fGid];}
        }
        public const string _Operation_fName = "Operation";        public const int _Author_fGid = 22223;
       
        public static mdTableField Author
        {
            get { return __MD.Fields[_Author_fGid];}
        }
        public const string _Author_fName = "Author";        public const int _reg_date_fGid = 22224;
       
        public static mdTableField reg_date
        {
            get { return __MD.Fields[_reg_date_fGid];}
        }
        public const string _reg_date_fName = "reg_date";        public const int _contragent_fGid = 22225;
       
        public static mdTableField contragent
        {
            get { return __MD.Fields[_contragent_fGid];}
        }
        public const string _contragent_fName = "contragent";        public const int _contragent_contact_person_fGid = 22226;
       
        public static mdTableField contragent_contact_person
        {
            get { return __MD.Fields[_contragent_contact_person_fGid];}
        }
        public const string _contragent_contact_person_fName = "contragent_contact_person";        public const int _responsible_person_fGid = 22227;
       
        public static mdTableField responsible_person
        {
            get { return __MD.Fields[_responsible_person_fGid];}
        }
        public const string _responsible_person_fName = "responsible_person";        public const int _Type_of_task_fGid = 22228;
       
        public static mdTableField Type_of_task
        {
            get { return __MD.Fields[_Type_of_task_fGid];}
        }
        public const string _Type_of_task_fName = "Type_of_task";        public const int _phones_fGid = 22230;
       
        public static mdTableField phones
        {
            get { return __MD.Fields[_phones_fGid];}
        }
        public const string _phones_fName = "phones";        public const int _Priority_Level_fGid = 22232;
       
        public static mdTableField Priority_Level
        {
            get { return __MD.Fields[_Priority_Level_fGid];}
        }
        public const string _Priority_Level_fName = "Priority_Level";        public const int _object_status_fGid = 22256;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";       
        public static mdTableField Type_of_task_view
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Type_of_task_fGid]);}
        }
        public const string _Type_of_task_view_fName = "Type_of_task_view";       
        public static mdTableField responsible_person_view
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_responsible_person_fGid]);}
        }
        public const string _responsible_person_view_fName = "responsible_person_view";       
        public static mdTableField contragent_name
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_contragent_fGid]);}
        }
        public const string _contragent_name_fName = "contragent_name";        public const int _contragent_contact_person_view_fGid = 22274;
       
        public static mdTableField contragent_contact_person_view
        {
            get { return __MD.Fields[_contragent_contact_person_view_fGid];}
        }
        public const string _contragent_contact_person_view_fName = "contragent_contact_person_view";        public const int _planed_datetime_fGid = 22279;
       
        public static mdTableField planed_datetime
        {
            get { return __MD.Fields[_planed_datetime_fGid];}
        }
        public const string _planed_datetime_fName = "planed_datetime";        public const int _Event_Title_fGid = 22281;
       
        public static mdTableField Event_Title
        {
            get { return __MD.Fields[_Event_Title_fGid];}
        }
        public const string _Event_Title_fName = "Event_Title";        public const int _Event_Body_fGid = 22314;
       
        public static mdTableField Event_Body
        {
            get { return __MD.Fields[_Event_Body_fGid];}
        }
        public const string _Event_Body_fName = "Event_Body";       
        public static mdTableField Operation_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Operation_fGid]);}
        }
        public const string _Operation_view__fName = "Operation_view_";       
        public static mdTableField Author_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Author_fGid]);}
        }
        public const string _Author_view__fName = "Author_view_";       
        public static mdTableField contragent_contact_person_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_contragent_contact_person_fGid]);}
        }
        public const string _contragent_contact_person_view__fName = "contragent_contact_person_view_";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";       
        public static mdTableField Author_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Author_fGid]);}
        }
        public const string _Author_status__fName = "Author_status_";       
        public static mdTableField contragent_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_contragent_fGid]);}
        }
        public const string _contragent_status__fName = "contragent_status_";       
        public static mdTableField contragent_contact_person_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_contragent_contact_person_fGid]);}
        }
        public const string _contragent_contact_person_status__fName = "contragent_contact_person_status_";       
        public static mdTableField responsible_person_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_responsible_person_fGid]);}
        }
        public const string _responsible_person_status__fName = "responsible_person_status_";
    }
    /// <summary>
    /// Схема таблиці 'CRM - події та активні задачі'
    /// </summary>
    public static class CRM_Union_TS_   
    {
        public const int __tGid = 15157;
        public const string __tName = "CRM_Union";
        static mdTable m_mdTable;
        static CRM_Union_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _active_status_fGid = 22299;
       
        public static mdTableField active_status
        {
            get { return __MD.Fields[_active_status_fGid];}
        }
        public const string _active_status_fName = "active_status";
    }
    /// <summary>
    /// Схема таблиці 'Додаткові дані об'єктів для Web-каталогів'
    /// </summary>
    public static class objects_web_info_TS_   
    {
        public const int __tGid = 15193;
        public const string __tName = "objects_web_info";
        static mdTable m_mdTable;
        static objects_web_info_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _DataObject_fGid = 22741;
       
        public static mdTableField DataObject
        {
            get { return __MD.Fields[_DataObject_fGid];}
        }
        public const string _DataObject_fName = "DataObject";        public const int _DataObject_tid_fGid = 22742;
       
        public static mdTableField DataObject_tid
        {
            get { return __MD.Fields[_DataObject_tid_fGid];}
        }
        public const string _DataObject_tid_fName = "DataObject_tid";        public const int _Short_Description_fGid = 22743;
       
        public static mdTableField Short_Description
        {
            get { return __MD.Fields[_Short_Description_fGid];}
        }
        public const string _Short_Description_fName = "Short_Description";        public const int _Full_Description_fGid = 22745;
       
        public static mdTableField Full_Description
        {
            get { return __MD.Fields[_Full_Description_fGid];}
        }
        public const string _Full_Description_fName = "Full_Description";        public const int _Web_Catalog_fGid = 22746;
       
        public static mdTableField Web_Catalog
        {
            get { return __MD.Fields[_Web_Catalog_fGid];}
        }
        public const string _Web_Catalog_fName = "Web_Catalog";        public const int _url_fGid = 22747;
       
        public static mdTableField url
        {
            get { return __MD.Fields[_url_fGid];}
        }
        public const string _url_fName = "url";        public const int _page_title_fGid = 22765;
       
        public static mdTableField page_title
        {
            get { return __MD.Fields[_page_title_fGid];}
        }
        public const string _page_title_fName = "page_title";        public const int _keywords_fGid = 22766;
       
        public static mdTableField keywords
        {
            get { return __MD.Fields[_keywords_fGid];}
        }
        public const string _keywords_fName = "keywords";        public const int _Catalog_URL_fGid = 24204;
       
        public static mdTableField Catalog_URL
        {
            get { return __MD.Fields[_Catalog_URL_fGid];}
        }
        public const string _Catalog_URL_fName = "Catalog_URL";        public const int _HTML_Body_Code_fGid = 24878;
       
        public static mdTableField HTML_Body_Code
        {
            get { return __MD.Fields[_HTML_Body_Code_fGid];}
        }
        public const string _HTML_Body_Code_fName = "HTML_Body_Code";       
        public static mdTableField Web_Catalog_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Web_Catalog_fGid]);}
        }
        public const string _Web_Catalog_view__fName = "Web_Catalog_view_";       
        public static mdTableField Web_Catalog_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Web_Catalog_fGid]);}
        }
        public const string _Web_Catalog_status__fName = "Web_Catalog_status_";
    }
    /// <summary>
    /// Схема таблиці 'Профілі користувачів у соціальних мережах'
    /// </summary>
    public static class SocNetProfiles_TS_ 
    {
        public static mdTable __MD
        {
            get 
            {
                return SocNetProfiles_CTS_.__MD ;
            }
        }
        public const int _lower_email_fGid = 26005;
       
        public static mdTableField lower_email
        {
            get { return __MD.Fields[_lower_email_fGid];}
        }
        public const string _lower_email_fName = "lower_email";
    }
    /// <summary>
    /// Схема таблиці 'Web - Каталоги'
    /// </summary>
    public static class Web_Catalogs_TS_   
    {
        public const int __tGid = 15192;
        public const string __tName = "Web_Catalogs";
        static mdTable m_mdTable;
        static Web_Catalogs_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _name_fGid = 22731;
       
        public static mdTableField name
        {
            get { return __MD.Fields[_name_fGid];}
        }
        public const string _name_fName = "name";        public const int _gid_fGid = 22732;
       
        public static mdTableField gid
        {
            get { return __MD.Fields[_gid_fGid];}
        }
        public const string _gid_fName = "gid";        public const int _object_status_fGid = 22733;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _version_no_fGid = 22734;
       
        public static mdTableField version_no
        {
            get { return __MD.Fields[_version_no_fGid];}
        }
        public const string _version_no_fName = "version_no";        public const int _comments_fGid = 22735;
       
        public static mdTableField comments
        {
            get { return __MD.Fields[_comments_fGid];}
        }
        public const string _comments_fName = "comments";        public const int _parent_Catalog_fGid = 22736;
       
        public static mdTableField parent_Catalog
        {
            get { return __MD.Fields[_parent_Catalog_fGid];}
        }
        public const string _parent_Catalog_fName = "parent_Catalog";        public const int _Full_URL_Address_fGid = 22737;
       
        public static mdTableField Full_URL_Address
        {
            get { return __MD.Fields[_Full_URL_Address_fGid];}
        }
        public const string _Full_URL_Address_fName = "Full_URL_Address";        public const int _Main_showcase_fGid = 24396;
       
        public static mdTableField Main_showcase
        {
            get { return __MD.Fields[_Main_showcase_fGid];}
        }
        public const string _Main_showcase_fName = "Main_showcase";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";       
        public static mdTableField parent_Catalog_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_parent_Catalog_fGid]);}
        }
        public const string _parent_Catalog_view__fName = "parent_Catalog_view_";       
        public static mdTableField Main_showcase_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Main_showcase_fGid]);}
        }
        public const string _Main_showcase_view__fName = "Main_showcase_view_";       
        public static mdTableField parent_Catalog_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_parent_Catalog_fGid]);}
        }
        public const string _parent_Catalog_status__fName = "parent_Catalog_status_";       
        public static mdTableField Main_showcase_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Main_showcase_fGid]);}
        }
        public const string _Main_showcase_status__fName = "Main_showcase_status_";
    }
    /// <summary>
    /// Схема таблиці 'Web - Запити'
    /// </summary>
    public static class Web_Requests_TS_   
    {
        public const int __tGid = 15799;
        public const string __tName = "Web_Requests";
        static mdTable m_mdTable;
        static Web_Requests_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _gid_fGid = 32023;
       
        public static mdTableField gid
        {
            get { return __MD.Fields[_gid_fGid];}
        }
        public const string _gid_fName = "gid";        public const int _RequestCode_fGid = 32025;
       
        public static mdTableField RequestCode
        {
            get { return __MD.Fields[_RequestCode_fGid];}
        }
        public const string _RequestCode_fName = "RequestCode";        public const int _reg_date_fGid = 32026;
       
        public static mdTableField reg_date
        {
            get { return __MD.Fields[_reg_date_fGid];}
        }
        public const string _reg_date_fName = "reg_date";        public const int _WebAccount_fGid = 32027;
       
        public static mdTableField WebAccount
        {
            get { return __MD.Fields[_WebAccount_fGid];}
        }
        public const string _WebAccount_fName = "WebAccount";        public const int _RequestType_fGid = 32028;
       
        public static mdTableField RequestType
        {
            get { return __MD.Fields[_RequestType_fGid];}
        }
        public const string _RequestType_fName = "RequestType";        public const int _ResponseTime_fGid = 32029;
       
        public static mdTableField ResponseTime
        {
            get { return __MD.Fields[_ResponseTime_fGid];}
        }
        public const string _ResponseTime_fName = "ResponseTime";        public const int _ResponseCode_fGid = 32030;
       
        public static mdTableField ResponseCode
        {
            get { return __MD.Fields[_ResponseCode_fGid];}
        }
        public const string _ResponseCode_fName = "ResponseCode";        public const int _version_no_fGid = 32031;
       
        public static mdTableField version_no
        {
            get { return __MD.Fields[_version_no_fGid];}
        }
        public const string _version_no_fName = "version_no";        public const int _object_status_fGid = 32032;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _AccountLogin_fGid = 32245;
       
        public static mdTableField AccountLogin
        {
            get { return __MD.Fields[_AccountLogin_fGid];}
        }
        public const string _AccountLogin_fName = "AccountLogin";       
        public static mdTableField WebAccount_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_WebAccount_fGid]);}
        }
        public const string _WebAccount_view__fName = "WebAccount_view_";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";       
        public static mdTableField WebAccount_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_WebAccount_fGid]);}
        }
        public const string _WebAccount_status__fName = "WebAccount_status_";
    }
    /// <summary>
    /// Схема таблиці 'Web - Запити'
    /// </summary>
    public static class Web_Requests_V_TS_   
    {
        public const int __tGid = 15800;
        public const string __tName = "Web_Requests_V";
        static mdTable m_mdTable;
        static Web_Requests_V_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _gid_fGid = 32035;
       
        public static mdTableField gid
        {
            get { return __MD.Fields[_gid_fGid];}
        }
        public const string _gid_fName = "gid";        public const int _WebAccount_fGid = 32036;
       
        public static mdTableField WebAccount
        {
            get { return __MD.Fields[_WebAccount_fGid];}
        }
        public const string _WebAccount_fName = "WebAccount";        public const int _RequestsCount_fGid = 32037;
       
        public static mdTableField RequestsCount
        {
            get { return __MD.Fields[_RequestsCount_fGid];}
        }
        public const string _RequestsCount_fName = "RequestsCount";        public const int _LastDateTime_fGid = 32038;
       
        public static mdTableField LastDateTime
        {
            get { return __MD.Fields[_LastDateTime_fGid];}
        }
        public const string _LastDateTime_fName = "LastDateTime";       
        public static mdTableField WebAccount_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_WebAccount_fGid]);}
        }
        public const string _WebAccount_view__fName = "WebAccount_view_";
    }
    /// <summary>
    /// Схема таблиці 'Web-вітрини'
    /// </summary>
    public static class Web_showcase_TS_   
    {
        public const int __tGid = 15312;
        public const string __tName = "Web_showcase";
        static mdTable m_mdTable;
        static Web_showcase_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _gid_fGid = 24382;
       
        public static mdTableField gid
        {
            get { return __MD.Fields[_gid_fGid];}
        }
        public const string _gid_fName = "gid";        public const int _name_fGid = 24383;
       
        public static mdTableField name
        {
            get { return __MD.Fields[_name_fGid];}
        }
        public const string _name_fName = "name";        public const int _comments_fGid = 24384;
       
        public static mdTableField comments
        {
            get { return __MD.Fields[_comments_fGid];}
        }
        public const string _comments_fName = "comments";        public const int _object_status_fGid = 24385;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _version_no_fGid = 24386;
       
        public static mdTableField version_no
        {
            get { return __MD.Fields[_version_no_fGid];}
        }
        public const string _version_no_fName = "version_no";        public const int _Owner_object_fGid = 24387;
       
        public static mdTableField Owner_object
        {
            get { return __MD.Fields[_Owner_object_fGid];}
        }
        public const string _Owner_object_fName = "Owner_object";        public const int _Owner_object_tid_fGid = 24388;
       
        public static mdTableField Owner_object_tid
        {
            get { return __MD.Fields[_Owner_object_tid_fGid];}
        }
        public const string _Owner_object_tid_fName = "Owner_object_tid";        public const int _VerticalAlignment_fGid = 24389;
       
        public static mdTableField VerticalAlignment
        {
            get { return __MD.Fields[_VerticalAlignment_fGid];}
        }
        public const string _VerticalAlignment_fName = "VerticalAlignment";        public const int _HorizontalAlignment_fGid = 24390;
       
        public static mdTableField HorizontalAlignment
        {
            get { return __MD.Fields[_HorizontalAlignment_fGid];}
        }
        public const string _HorizontalAlignment_fName = "HorizontalAlignment";        public const int _Caption_fGid = 24391;
       
        public static mdTableField Caption
        {
            get { return __MD.Fields[_Caption_fGid];}
        }
        public const string _Caption_fName = "Caption";        public const int _ShowcaseType_fGid = 24491;
       
        public static mdTableField ShowcaseType
        {
            get { return __MD.Fields[_ShowcaseType_fGid];}
        }
        public const string _ShowcaseType_fName = "ShowcaseType";        public const int _HTML_Body_Code_fGid = 24877;
       
        public static mdTableField HTML_Body_Code
        {
            get { return __MD.Fields[_HTML_Body_Code_fGid];}
        }
        public const string _HTML_Body_Code_fName = "HTML_Body_Code";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";
    }
    /// <summary>
    /// Схема таблиці 'Web Оглядачі'
    /// </summary>
    public static class WebBrowsers_TS_   
    {
        public const int __tGid = 15807;
        public const string __tName = "WebBrowsers";
        static mdTable m_mdTable;
        static WebBrowsers_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _gid_fGid = 32248;
       
        public static mdTableField gid
        {
            get { return __MD.Fields[_gid_fGid];}
        }
        public const string _gid_fName = "gid";        public const int _Name_fGid = 32249;
       
        public static mdTableField Name
        {
            get { return __MD.Fields[_Name_fGid];}
        }
        public const string _Name_fName = "Name";        public const int _Version_fGid = 32250;
       
        public static mdTableField Version
        {
            get { return __MD.Fields[_Version_fGid];}
        }
        public const string _Version_fName = "Version";        public const int _FullName_fGid = 32251;
       
        public static mdTableField FullName
        {
            get { return __MD.Fields[_FullName_fGid];}
        }
        public const string _FullName_fName = "FullName";        public const int _DeviceManufacturer_fGid = 32252;
       
        public static mdTableField DeviceManufacturer
        {
            get { return __MD.Fields[_DeviceManufacturer_fGid];}
        }
        public const string _DeviceManufacturer_fName = "DeviceManufacturer";        public const int _DeviceModel_fGid = 32253;
       
        public static mdTableField DeviceModel
        {
            get { return __MD.Fields[_DeviceModel_fGid];}
        }
        public const string _DeviceModel_fName = "DeviceModel";        public const int _BrowserId_fGid = 32254;
       
        public static mdTableField BrowserId
        {
            get { return __MD.Fields[_BrowserId_fGid];}
        }
        public const string _BrowserId_fName = "BrowserId";        public const int _Platform_fGid = 32255;
       
        public static mdTableField Platform
        {
            get { return __MD.Fields[_Platform_fGid];}
        }
        public const string _Platform_fName = "Platform";        public const int _Capabilities_fGid = 32256;
       
        public static mdTableField Capabilities
        {
            get { return __MD.Fields[_Capabilities_fGid];}
        }
        public const string _Capabilities_fName = "Capabilities";        public const int _Comments_fGid = 32257;
       
        public static mdTableField Comments
        {
            get { return __MD.Fields[_Comments_fGid];}
        }
        public const string _Comments_fName = "Comments";        public const int _version_no_fGid = 33015;
       
        public static mdTableField version_no
        {
            get { return __MD.Fields[_version_no_fGid];}
        }
        public const string _version_no_fName = "version_no";
    }
    /// <summary>
    /// Схема таблиці 'Web-сеанси'
    /// </summary>
    public static class WebSessions_TS_   
    {
        public const int __tGid = 15806;
        public const string __tName = "WebSessions";
        static mdTable m_mdTable;
        static WebSessions_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _UserSession_fGid = 32246;
       
        public static mdTableField UserSession
        {
            get { return __MD.Fields[_UserSession_fGid];}
        }
        public const string _UserSession_fName = "UserSession";       
        public static mdTableField reg_date
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_UserSession_fGid]);}
        }
        public const string _reg_date_fName = "reg_date";        public const int _WebAccount_fGid = 32258;
       
        public static mdTableField WebAccount
        {
            get { return __MD.Fields[_WebAccount_fGid];}
        }
        public const string _WebAccount_fName = "WebAccount";        public const int _WebBrowser_fGid = 32259;
       
        public static mdTableField WebBrowser
        {
            get { return __MD.Fields[_WebBrowser_fGid];}
        }
        public const string _WebBrowser_fName = "WebBrowser";        public const int _IpAddress_fGid = 32260;
       
        public static mdTableField IpAddress
        {
            get { return __MD.Fields[_IpAddress_fGid];}
        }
        public const string _IpAddress_fName = "IpAddress";        public const int _HostName_fGid = 32261;
       
        public static mdTableField HostName
        {
            get { return __MD.Fields[_HostName_fGid];}
        }
        public const string _HostName_fName = "HostName";        public const int _UserLanguages_fGid = 32262;
       
        public static mdTableField UserLanguages
        {
            get { return __MD.Fields[_UserLanguages_fGid];}
        }
        public const string _UserLanguages_fName = "UserLanguages";        public const int _RequestsCount_fGid = 32263;
       
        public static mdTableField RequestsCount
        {
            get { return __MD.Fields[_RequestsCount_fGid];}
        }
        public const string _RequestsCount_fName = "RequestsCount";        public const int _LastRequestTime_fGid = 32264;
       
        public static mdTableField LastRequestTime
        {
            get { return __MD.Fields[_LastRequestTime_fGid];}
        }
        public const string _LastRequestTime_fName = "LastRequestTime";        public const int _WebRequest_fGid = 32303;
       
        public static mdTableField WebRequest
        {
            get { return __MD.Fields[_WebRequest_fGid];}
        }
        public const string _WebRequest_fName = "WebRequest";        public const int _Token_fGid = 34461;
       
        public static mdTableField Token
        {
            get { return __MD.Fields[_Token_fGid];}
        }
        public const string _Token_fName = "Token";       
        public static mdTableField WebAccount_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_WebAccount_fGid]);}
        }
        public const string _WebAccount_view__fName = "WebAccount_view_";       
        public static mdTableField WebBrowser_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_WebBrowser_fGid]);}
        }
        public const string _WebBrowser_view__fName = "WebBrowser_view_";       
        public static mdTableField WebRequest_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_WebRequest_fGid]);}
        }
        public const string _WebRequest_view__fName = "WebRequest_view_";       
        public static mdTableField UserSession_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_UserSession_fGid]);}
        }
        public const string _UserSession_status__fName = "UserSession_status_";       
        public static mdTableField WebAccount_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_WebAccount_fGid]);}
        }
        public const string _WebAccount_status__fName = "WebAccount_status_";       
        public static mdTableField WebRequest_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_WebRequest_fGid]);}
        }
        public const string _WebRequest_status__fName = "WebRequest_status_";
    }
    /// <summary>
    /// Схема таблиці 'Вкладені елементи документації'
    /// </summary>
    public static class IncludedDocItems_TS_   
    {
        public const int __tGid = 15683;
        public const string __tName = "IncludedDocItems";
        static mdTable m_mdTable;
        static IncludedDocItems_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _MSchemaDoc_fGid = 29643;
       
        public static mdTableField MSchemaDoc
        {
            get { return __MD.Fields[_MSchemaDoc_fGid];}
        }
        public const string _MSchemaDoc_fName = "MSchemaDoc";        public const int _ItemDoc_fGid = 29644;
       
        public static mdTableField ItemDoc
        {
            get { return __MD.Fields[_ItemDoc_fGid];}
        }
        public const string _ItemDoc_fName = "ItemDoc";        public const int _RowSubKey_fGid = 29645;
       
        public static mdTableField RowSubKey
        {
            get { return __MD.Fields[_RowSubKey_fGid];}
        }
        public const string _RowSubKey_fName = "RowSubKey";       
        public static mdTableField MSchemaDoc_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_MSchemaDoc_fGid]);}
        }
        public const string _MSchemaDoc_view__fName = "MSchemaDoc_view_";       
        public static mdTableField ItemDoc_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_ItemDoc_fGid]);}
        }
        public const string _ItemDoc_view__fName = "ItemDoc_view_";       
        public static mdTableField MSchemaDoc_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_MSchemaDoc_fGid]);}
        }
        public const string _MSchemaDoc_status__fName = "MSchemaDoc_status_";       
        public static mdTableField ItemDoc_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_ItemDoc_fGid]);}
        }
        public const string _ItemDoc_status__fName = "ItemDoc_status_";
    }
    /// <summary>
    /// Схема таблиці 'Специфікація монтажних схем'
    /// </summary>
    public static class MontageSchemaSP_TS_   
    {
        public const int __tGid = 15680;
        public const string __tName = "MontageSchemaSP";
        static mdTable m_mdTable;
        static MontageSchemaSP_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _MSchemaDoc_fGid = 29628;
       
        public static mdTableField MSchemaDoc
        {
            get { return __MD.Fields[_MSchemaDoc_fGid];}
        }
        public const string _MSchemaDoc_fName = "MSchemaDoc";        public const int _AxisCount_fGid = 29629;
       
        public static mdTableField AxisCount
        {
            get { return __MD.Fields[_AxisCount_fGid];}
        }
        public const string _AxisCount_fName = "AxisCount";        public const int _CountInProject_fGid = 29630;
       
        public static mdTableField CountInProject
        {
            get { return __MD.Fields[_CountInProject_fGid];}
        }
        public const string _CountInProject_fName = "CountInProject";        public const int _FloorsStr_fGid = 29631;
       
        public static mdTableField FloorsStr
        {
            get { return __MD.Fields[_FloorsStr_fGid];}
        }
        public const string _FloorsStr_fName = "FloorsStr";       
        public static mdTableField MSchemaDoc_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_MSchemaDoc_fGid]);}
        }
        public const string _MSchemaDoc_view__fName = "MSchemaDoc_view_";       
        public static mdTableField MSchemaDoc_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_MSchemaDoc_fGid]);}
        }
        public const string _MSchemaDoc_status__fName = "MSchemaDoc_status_";
    }
    /// <summary>
    /// Схема таблиці 'Т.Ч. специфікації монтажної схеми'
    /// </summary>
    public static class MontageSchemaSP_tp_TS_   
    {
        public const int __tGid = 15681;
        public const string __tName = "MontageSchemaSP_tp";
        static mdTable m_mdTable;
        static MontageSchemaSP_tp_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _MSchemaDoc_fGid = 29632;
       
        public static mdTableField MSchemaDoc
        {
            get { return __MD.Fields[_MSchemaDoc_fGid];}
        }
        public const string _MSchemaDoc_fName = "MSchemaDoc";        public const int _ItemName_fGid = 29633;
       
        public static mdTableField ItemName
        {
            get { return __MD.Fields[_ItemName_fGid];}
        }
        public const string _ItemName_fName = "ItemName";        public const int _ordinal_fGid = 29634;
       
        public static mdTableField ordinal
        {
            get { return __MD.Fields[_ordinal_fGid];}
        }
        public const string _ordinal_fName = "ordinal";        public const int _RowSubKey_fGid = 29635;
       
        public static mdTableField RowSubKey
        {
            get { return __MD.Fields[_RowSubKey_fGid];}
        }
        public const string _RowSubKey_fName = "RowSubKey";        public const int _CountOnSchema_fGid = 29636;
       
        public static mdTableField CountOnSchema
        {
            get { return __MD.Fields[_CountOnSchema_fGid];}
        }
        public const string _CountOnSchema_fName = "CountOnSchema";        public const int _ItemDoc_fGid = 29646;
       
        public static mdTableField ItemDoc
        {
            get { return __MD.Fields[_ItemDoc_fGid];}
        }
        public const string _ItemDoc_fName = "ItemDoc";       
        public static mdTableField MSchemaDoc_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_MSchemaDoc_fGid]);}
        }
        public const string _MSchemaDoc_view__fName = "MSchemaDoc_view_";       
        public static mdTableField ItemDoc_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_ItemDoc_fGid]);}
        }
        public const string _ItemDoc_view__fName = "ItemDoc_view_";       
        public static mdTableField MSchemaDoc_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_MSchemaDoc_fGid]);}
        }
        public const string _MSchemaDoc_status__fName = "MSchemaDoc_status_";       
        public static mdTableField ItemDoc_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_ItemDoc_fGid]);}
        }
        public const string _ItemDoc_status__fName = "ItemDoc_status_";
    }
    /// <summary>
    /// Схема таблиці 'Специфікація елементів монтажної схеми по осях'
    /// </summary>
    public static class MontageSchemaSP_tpByAxis_TS_   
    {
        public const int __tGid = 15682;
        public const string __tName = "MontageSchemaSP_tpByAxis";
        static mdTable m_mdTable;
        static MontageSchemaSP_tpByAxis_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _MSchemaDoc_fGid = 29637;
       
        public static mdTableField MSchemaDoc
        {
            get { return __MD.Fields[_MSchemaDoc_fGid];}
        }
        public const string _MSchemaDoc_fName = "MSchemaDoc";        public const int _RowSubKey_fGid = 29638;
       
        public static mdTableField RowSubKey
        {
            get { return __MD.Fields[_RowSubKey_fGid];}
        }
        public const string _RowSubKey_fName = "RowSubKey";        public const int _AxisNo_fGid = 29639;
       
        public static mdTableField AxisNo
        {
            get { return __MD.Fields[_AxisNo_fGid];}
        }
        public const string _AxisNo_fName = "AxisNo";        public const int _ItemsCount_fGid = 29640;
       
        public static mdTableField ItemsCount
        {
            get { return __MD.Fields[_ItemsCount_fGid];}
        }
        public const string _ItemsCount_fName = "ItemsCount";        public const int _ItemName_fGid = 29641;
       
        public static mdTableField ItemName
        {
            get { return __MD.Fields[_ItemName_fGid];}
        }
        public const string _ItemName_fName = "ItemName";        public const int _ordinal_fGid = 29642;
       
        public static mdTableField ordinal
        {
            get { return __MD.Fields[_ordinal_fGid];}
        }
        public const string _ordinal_fName = "ordinal";       
        public static mdTableField MSchemaDoc_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_MSchemaDoc_fGid]);}
        }
        public const string _MSchemaDoc_view__fName = "MSchemaDoc_view_";       
        public static mdTableField MSchemaDoc_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_MSchemaDoc_fGid]);}
        }
        public const string _MSchemaDoc_status__fName = "MSchemaDoc_status_";
    }
    /// <summary>
    /// Схема таблиці 'Електронні скриньки'
    /// </summary>
    public static class e_mail_boxes_TS_   
    {
        public const int __tGid = 2142;
        public const string __tName = "e_mail_boxes";
        static mdTable m_mdTable;
        static e_mail_boxes_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _smtp_server_fGid = 8686;
       
        public static mdTableField smtp_server
        {
            get { return __MD.Fields[_smtp_server_fGid];}
        }
        public const string _smtp_server_fName = "smtp_server";        public const int _port_fGid = 8687;
       
        public static mdTableField port
        {
            get { return __MD.Fields[_port_fGid];}
        }
        public const string _port_fName = "port";        public const int _e_mail_fGid = 8688;
       
        public static mdTableField e_mail
        {
            get { return __MD.Fields[_e_mail_fGid];}
        }
        public const string _e_mail_fName = "e_mail";        public const int _gid_fGid = 8689;
       
        public static mdTableField gid
        {
            get { return __MD.Fields[_gid_fGid];}
        }
        public const string _gid_fName = "gid";        public const int _object_status_fGid = 8702;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _version_no_fGid = 8703;
       
        public static mdTableField version_no
        {
            get { return __MD.Fields[_version_no_fGid];}
        }
        public const string _version_no_fName = "version_no";        public const int _account_login_fGid = 8732;
       
        public static mdTableField account_login
        {
            get { return __MD.Fields[_account_login_fGid];}
        }
        public const string _account_login_fName = "account_login";        public const int _account_password_fGid = 8733;
       
        public static mdTableField account_password
        {
            get { return __MD.Fields[_account_password_fGid];}
        }
        public const string _account_password_fName = "account_password";        public const int _SSL_fGid = 8798;
       
        public static mdTableField SSL
        {
            get { return __MD.Fields[_SSL_fGid];}
        }
        public const string _SSL_fName = "SSL";        public const int _authentication_fGid = 8799;
       
        public static mdTableField authentication
        {
            get { return __MD.Fields[_authentication_fGid];}
        }
        public const string _authentication_fName = "authentication";        public const int _sender_name_fGid = 32310;
       
        public static mdTableField sender_name
        {
            get { return __MD.Fields[_sender_name_fGid];}
        }
        public const string _sender_name_fName = "sender_name";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";
    }
    /// <summary>
    /// Схема таблиці 'Пакети електронних листів (т.ч.)'
    /// </summary>
    public static class e_mail_package_tp_TS_   
    {
        public const int __tGid = 2145;
        public const string __tName = "e_mail_package_tp";
        static mdTable m_mdTable;
        static e_mail_package_tp_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _SentResult_fGid = 6732;
       
        public static mdTableField SentResult
        {
            get { return __MD.Fields[_SentResult_fGid];}
        }
        public const string _SentResult_fName = "SentResult";        public const int _error_text_fGid = 6733;
       
        public static mdTableField error_text
        {
            get { return __MD.Fields[_error_text_fGid];}
        }
        public const string _error_text_fName = "error_text";        public const int _e_mail_message_fGid = 8695;
       
        public static mdTableField e_mail_message
        {
            get { return __MD.Fields[_e_mail_message_fGid];}
        }
        public const string _e_mail_message_fName = "e_mail_message";        public const int _ordinal_fGid = 8745;
       
        public static mdTableField ordinal
        {
            get { return __MD.Fields[_ordinal_fGid];}
        }
        public const string _ordinal_fName = "ordinal";        public const int _WebAccount_fGid = 32232;
       
        public static mdTableField WebAccount
        {
            get { return __MD.Fields[_WebAccount_fGid];}
        }
        public const string _WebAccount_fName = "WebAccount";        public const int _WebRequest_fGid = 32234;
       
        public static mdTableField WebRequest
        {
            get { return __MD.Fields[_WebRequest_fGid];}
        }
        public const string _WebRequest_fName = "WebRequest";        public const int _e_mail_address_fGid = 32235;
       
        public static mdTableField e_mail_address
        {
            get { return __MD.Fields[_e_mail_address_fGid];}
        }
        public const string _e_mail_address_fName = "e_mail_address";       
        public static mdTableField RequestCode
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_WebRequest_fGid]);}
        }
        public const string _RequestCode_fName = "RequestCode";       
        public static mdTableField Full_Name
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_WebAccount_fGid]);}
        }
        public const string _Full_Name_fName = "Full_Name";        public const int _Gender_fGid = 32242;
       
        public static mdTableField Gender
        {
            get { return __MD.Fields[_Gender_fGid];}
        }
        public const string _Gender_fName = "Gender";        public const int _pack_datetime_fGid = 32266;
       
        public static mdTableField pack_datetime
        {
            get { return __MD.Fields[_pack_datetime_fGid];}
        }
        public const string _pack_datetime_fName = "pack_datetime";        public const int _SentTime_fGid = 32267;
       
        public static mdTableField SentTime
        {
            get { return __MD.Fields[_SentTime_fGid];}
        }
        public const string _SentTime_fName = "SentTime";        public const int _MailTime_fGid = 32268;
       
        public static mdTableField MailTime
        {
            get { return __MD.Fields[_MailTime_fGid];}
        }
        public const string _MailTime_fName = "MailTime";        public const int _Surname_fGid = 32307;
       
        public static mdTableField Surname
        {
            get { return __MD.Fields[_Surname_fGid];}
        }
        public const string _Surname_fName = "Surname";        public const int _FirstName_fGid = 32308;
       
        public static mdTableField FirstName
        {
            get { return __MD.Fields[_FirstName_fGid];}
        }
        public const string _FirstName_fName = "FirstName";        public const int _Middle_name_fGid = 32309;
       
        public static mdTableField Middle_name
        {
            get { return __MD.Fields[_Middle_name_fGid];}
        }
        public const string _Middle_name_fName = "Middle_name";        public const int _IsAuthorized_fGid = 32317;
       
        public static mdTableField IsAuthorized
        {
            get { return __MD.Fields[_IsAuthorized_fGid];}
        }
        public const string _IsAuthorized_fName = "IsAuthorized";        public const int _request_status_fGid = 32319;
       
        public static mdTableField request_status
        {
            get { return __MD.Fields[_request_status_fGid];}
        }
        public const string _request_status_fName = "request_status";        public const int _IsRejected_fGid = 32320;
       
        public static mdTableField IsRejected
        {
            get { return __MD.Fields[_IsRejected_fGid];}
        }
        public const string _IsRejected_fName = "IsRejected";        public const int _IsError_fGid = 32339;
       
        public static mdTableField IsError
        {
            get { return __MD.Fields[_IsError_fGid];}
        }
        public const string _IsError_fName = "IsError";       
        public static mdTableField e_mail_message_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_e_mail_message_fGid]);}
        }
        public const string _e_mail_message_view__fName = "e_mail_message_view_";       
        public static mdTableField request_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_request_status_fGid]);}
        }
        public const string _request_status_view__fName = "request_status_view_";       
        public static mdTableField e_mail_message_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_e_mail_message_fGid]);}
        }
        public const string _e_mail_message_status__fName = "e_mail_message_status_";       
        public static mdTableField WebAccount_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_WebAccount_fGid]);}
        }
        public const string _WebAccount_status__fName = "WebAccount_status_";
    }
    /// <summary>
    /// Схема таблиці 'Пакети електронних листів'
    /// </summary>
    public static class e_mail_packages_TS_   
    {
        public const int __tGid = 2144;
        public const string __tName = "e_mail_packages";
        static mdTable m_mdTable;
        static e_mail_packages_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _reg_date_fGid = 8692;
       
        public static mdTableField reg_date
        {
            get { return __MD.Fields[_reg_date_fGid];}
        }
        public const string _reg_date_fName = "reg_date";        public const int _e_mail_template_fGid = 8693;
       
        public static mdTableField e_mail_template
        {
            get { return __MD.Fields[_e_mail_template_fGid];}
        }
        public const string _e_mail_template_fName = "e_mail_template";        public const int _gid_fGid = 8694;
       
        public static mdTableField gid
        {
            get { return __MD.Fields[_gid_fGid];}
        }
        public const string _gid_fName = "gid";        public const int _doc_number_fGid = 8697;
       
        public static mdTableField doc_number
        {
            get { return __MD.Fields[_doc_number_fGid];}
        }
        public const string _doc_number_fName = "doc_number";        public const int _object_status_fGid = 8706;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _version_no_fGid = 8707;
       
        public static mdTableField version_no
        {
            get { return __MD.Fields[_version_no_fGid];}
        }
        public const string _version_no_fName = "version_no";        public const int _previous_db_version_no_fGid = 8746;
       
        public static mdTableField previous_db_version_no
        {
            get { return __MD.Fields[_previous_db_version_no_fGid];}
        }
        public const string _previous_db_version_no_fName = "previous_db_version_no";        public const int _comments_fGid = 8801;
       
        public static mdTableField comments
        {
            get { return __MD.Fields[_comments_fGid];}
        }
        public const string _comments_fName = "comments";        public const int _e_mail_box_fGid = 29567;
       
        public static mdTableField e_mail_box
        {
            get { return __MD.Fields[_e_mail_box_fGid];}
        }
        public const string _e_mail_box_fName = "e_mail_box";        public const int _RecipientsCount_fGid = 32269;
       
        public static mdTableField RecipientsCount
        {
            get { return __MD.Fields[_RecipientsCount_fGid];}
        }
        public const string _RecipientsCount_fName = "RecipientsCount";        public const int _AuthorizedCount_fGid = 32321;
       
        public static mdTableField AuthorizedCount
        {
            get { return __MD.Fields[_AuthorizedCount_fGid];}
        }
        public const string _AuthorizedCount_fName = "AuthorizedCount";        public const int _RejectsCount_fGid = 32322;
       
        public static mdTableField RejectsCount
        {
            get { return __MD.Fields[_RejectsCount_fGid];}
        }
        public const string _RejectsCount_fName = "RejectsCount";        public const int _ErrorsCount_fGid = 32340;
       
        public static mdTableField ErrorsCount
        {
            get { return __MD.Fields[_ErrorsCount_fGid];}
        }
        public const string _ErrorsCount_fName = "ErrorsCount";       
        public static mdTableField e_mail_template_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_e_mail_template_fGid]);}
        }
        public const string _e_mail_template_view__fName = "e_mail_template_view_";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";       
        public static mdTableField e_mail_box_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_e_mail_box_fGid]);}
        }
        public const string _e_mail_box_view__fName = "e_mail_box_view_";       
        public static mdTableField e_mail_template_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_e_mail_template_fGid]);}
        }
        public const string _e_mail_template_status__fName = "e_mail_template_status_";       
        public static mdTableField e_mail_box_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_e_mail_box_fGid]);}
        }
        public const string _e_mail_box_status__fName = "e_mail_box_status_";
    }
    /// <summary>
    /// Схема таблиці 'Шаблони електронних листів'
    /// </summary>
    public static class e_mail_templates_TS_   
    {
        public const int __tGid = 2143;
        public const string __tName = "e_mail_templates";
        static mdTable m_mdTable;
        static e_mail_templates_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _Settings_fGid = 6693;
       
        public static mdTableField Settings
        {
            get { return __MD.Fields[_Settings_fGid];}
        }
        public const string _Settings_fName = "Settings";        public const int _comments_fGid = 6694;
       
        public static mdTableField comments
        {
            get { return __MD.Fields[_comments_fGid];}
        }
        public const string _comments_fName = "comments";        public const int _E_mail_letter_type_fGid = 6695;
       
        public static mdTableField E_mail_letter_type
        {
            get { return __MD.Fields[_E_mail_letter_type_fGid];}
        }
        public const string _E_mail_letter_type_fName = "E_mail_letter_type";        public const int _base_mail_box_fGid = 6700;
       
        public static mdTableField base_mail_box
        {
            get { return __MD.Fields[_base_mail_box_fGid];}
        }
        public const string _base_mail_box_fName = "base_mail_box";        public const int _name_fGid = 8690;
       
        public static mdTableField name
        {
            get { return __MD.Fields[_name_fGid];}
        }
        public const string _name_fName = "name";        public const int _gid_fGid = 8691;
       
        public static mdTableField gid
        {
            get { return __MD.Fields[_gid_fGid];}
        }
        public const string _gid_fName = "gid";        public const int _object_status_fGid = 8704;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _version_no_fGid = 8705;
       
        public static mdTableField version_no
        {
            get { return __MD.Fields[_version_no_fGid];}
        }
        public const string _version_no_fName = "version_no";        public const int _subject_template_fGid = 8734;
       
        public static mdTableField subject_template
        {
            get { return __MD.Fields[_subject_template_fGid];}
        }
        public const string _subject_template_fName = "subject_template";        public const int _Template_fGid = 8737;
       
        public static mdTableField Template
        {
            get { return __MD.Fields[_Template_fGid];}
        }
        public const string _Template_fName = "Template";       
        public static mdTableField base_mail_box_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_base_mail_box_fGid]);}
        }
        public const string _base_mail_box_view__fName = "base_mail_box_view_";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";       
        public static mdTableField base_mail_box_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_base_mail_box_fGid]);}
        }
        public const string _base_mail_box_status__fName = "base_mail_box_status_";
    }
    /// <summary>
    /// Схема таблиці 'Електронні листи-документи'
    /// </summary>
    public static class EMail_Document_TS_   
    {
        public const int __tGid = 350;
        public const string __tName = "EMail_Document";
        static mdTable m_mdTable;
        static EMail_Document_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _eMail_fGid = 2572;
       
        public static mdTableField eMail
        {
            get { return __MD.Fields[_eMail_fGid];}
        }
        public const string _eMail_fName = "eMail";        public const int _document_tid_fGid = 2573;
       
        public static mdTableField document_tid
        {
            get { return __MD.Fields[_document_tid_fGid];}
        }
        public const string _document_tid_fName = "document_tid";        public const int _document_fGid = 2579;
       
        public static mdTableField document
        {
            get { return __MD.Fields[_document_fGid];}
        }
        public const string _document_fName = "document";       
        public static mdTableField eMail_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_eMail_fGid]);}
        }
        public const string _eMail_view__fName = "eMail_view_";       
        public static mdTableField eMail_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_eMail_fGid]);}
        }
        public const string _eMail_status__fName = "eMail_status_";
    }
    /// <summary>
    /// Схема таблиці 'Електронні листи'
    /// </summary>
    public static class EMail_Journal_TS_   
    {
        public const int __tGid = 251;
        public const string __tName = "EMail_Journal";
        static mdTable m_mdTable;
        static EMail_Journal_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _gid_fGid = 1641;
       
        public static mdTableField gid
        {
            get { return __MD.Fields[_gid_fGid];}
        }
        public const string _gid_fName = "gid";        public const int _Caption_fGid = 2563;
       
        public static mdTableField Caption
        {
            get { return __MD.Fields[_Caption_fGid];}
        }
        public const string _Caption_fName = "Caption";        public const int _Body_fGid = 2564;
       
        public static mdTableField Body
        {
            get { return __MD.Fields[_Body_fGid];}
        }
        public const string _Body_fName = "Body";        public const int _sender_fGid = 2565;
       
        public static mdTableField sender
        {
            get { return __MD.Fields[_sender_fGid];}
        }
        public const string _sender_fName = "sender";        public const int _recepient_fGid = 2568;
       
        public static mdTableField recepient
        {
            get { return __MD.Fields[_recepient_fGid];}
        }
        public const string _recepient_fName = "recepient";        public const int _operation_type_fGid = 2571;
       
        public static mdTableField operation_type
        {
            get { return __MD.Fields[_operation_type_fGid];}
        }
        public const string _operation_type_fName = "operation_type";        public const int _reg_date_fGid = 2581;
       
        public static mdTableField reg_date
        {
            get { return __MD.Fields[_reg_date_fGid];}
        }
        public const string _reg_date_fName = "reg_date";        public const int _doc_number_fGid = 2582;
       
        public static mdTableField doc_number
        {
            get { return __MD.Fields[_doc_number_fGid];}
        }
        public const string _doc_number_fName = "doc_number";        public const int _comments_fGid = 2584;
       
        public static mdTableField comments
        {
            get { return __MD.Fields[_comments_fGid];}
        }
        public const string _comments_fName = "comments";        public const int _object_status_fGid = 15148;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _version_no_fGid = 23251;
       
        public static mdTableField version_no
        {
            get { return __MD.Fields[_version_no_fGid];}
        }
        public const string _version_no_fName = "version_no";        public const int _Author_fGid = 33922;
       
        public static mdTableField Author
        {
            get { return __MD.Fields[_Author_fGid];}
        }
        public const string _Author_fName = "Author";        public const int _RecipientPerson_fGid = 33923;
       
        public static mdTableField RecipientPerson
        {
            get { return __MD.Fields[_RecipientPerson_fGid];}
        }
        public const string _RecipientPerson_fName = "RecipientPerson";       
        public static mdTableField sender_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_sender_fGid]);}
        }
        public const string _sender_view__fName = "sender_view_";       
        public static mdTableField operation_type_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_operation_type_fGid]);}
        }
        public const string _operation_type_view__fName = "operation_type_view_";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";       
        public static mdTableField Author_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Author_fGid]);}
        }
        public const string _Author_view__fName = "Author_view_";       
        public static mdTableField RecipientPerson_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_RecipientPerson_fGid]);}
        }
        public const string _RecipientPerson_view__fName = "RecipientPerson_view_";       
        public static mdTableField sender_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_sender_fGid]);}
        }
        public const string _sender_status__fName = "sender_status_";       
        public static mdTableField Author_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Author_fGid]);}
        }
        public const string _Author_status__fName = "Author_status_";       
        public static mdTableField RecipientPerson_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_RecipientPerson_fGid]);}
        }
        public const string _RecipientPerson_status__fName = "RecipientPerson_status_";
    }
    /// <summary>
    /// Схема таблиці 'Електронні листи - повідомлення'
    /// </summary>
    public static class EMail_Messages_TS_   
    {
        public const int __tGid = 15889;
        public const string __tName = "EMail_Messages";
        static mdTable m_mdTable;
        static EMail_Messages_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _eMail_fGid = 33919;
       
        public static mdTableField eMail
        {
            get { return __MD.Fields[_eMail_fGid];}
        }
        public const string _eMail_fName = "eMail";        public const int _EMailTemplate_fGid = 33920;
       
        public static mdTableField EMailTemplate
        {
            get { return __MD.Fields[_EMailTemplate_fGid];}
        }
        public const string _EMailTemplate_fName = "EMailTemplate";        public const int _MessageKind_fGid = 33921;
       
        public static mdTableField MessageKind
        {
            get { return __MD.Fields[_MessageKind_fGid];}
        }
        public const string _MessageKind_fName = "MessageKind";       
        public static mdTableField eMail_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_eMail_fGid]);}
        }
        public const string _eMail_view__fName = "eMail_view_";       
        public static mdTableField EMailTemplate_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_EMailTemplate_fGid]);}
        }
        public const string _EMailTemplate_view__fName = "EMailTemplate_view_";       
        public static mdTableField eMail_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_eMail_fGid]);}
        }
        public const string _eMail_status__fName = "eMail_status_";       
        public static mdTableField EMailTemplate_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_EMailTemplate_fGid]);}
        }
        public const string _EMailTemplate_status__fName = "EMailTemplate_status_";
    }
    /// <summary>
    /// Схема таблиці 'Договори з контрагентами'
    /// </summary>
    public static class contract_TS_   
    {
        public const int __tGid = 188;
        public const string __tName = "contract";
        static mdTable m_mdTable;
        static contract_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _gid_fGid = 1239;
       
        public static mdTableField gid
        {
            get { return __MD.Fields[_gid_fGid];}
        }
        public const string _gid_fName = "gid";        public const int _number_fGid = 1241;
       
        public static mdTableField number
        {
            get { return __MD.Fields[_number_fGid];}
        }
        public const string _number_fName = "number";        public const int _date_began_agreement_fGid = 1242;
       
        public static mdTableField date_began_agreement
        {
            get { return __MD.Fields[_date_began_agreement_fGid];}
        }
        public const string _date_began_agreement_fName = "date_began_agreement";        public const int _date_end_agreement_fGid = 1243;
       
        public static mdTableField date_end_agreement
        {
            get { return __MD.Fields[_date_end_agreement_fGid];}
        }
        public const string _date_end_agreement_fName = "date_end_agreement";        public const int _object_status_fGid = 1732;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _company_fGid = 1843;
       
        public static mdTableField company
        {
            get { return __MD.Fields[_company_fGid];}
        }
        public const string _company_fName = "company";        public const int _contragent_fGid = 1846;
       
        public static mdTableField contragent
        {
            get { return __MD.Fields[_contragent_fGid];}
        }
        public const string _contragent_fName = "contragent";        public const int _comments_fGid = 1847;
       
        public static mdTableField comments
        {
            get { return __MD.Fields[_comments_fGid];}
        }
        public const string _comments_fName = "comments";        public const int _version_no_fGid = 2954;
       
        public static mdTableField version_no
        {
            get { return __MD.Fields[_version_no_fGid];}
        }
        public const string _version_no_fName = "version_no";        public const int _date_agreement_fGid = 3133;
       
        public static mdTableField date_agreement
        {
            get { return __MD.Fields[_date_agreement_fGid];}
        }
        public const string _date_agreement_fName = "date_agreement";        public const int _subject_contract_fGid = 4982;
       
        public static mdTableField subject_contract
        {
            get { return __MD.Fields[_subject_contract_fGid];}
        }
        public const string _subject_contract_fName = "subject_contract";        public const int _number_contract_contragent_fGid = 4983;
       
        public static mdTableField number_contract_contragent
        {
            get { return __MD.Fields[_number_contract_contragent_fGid];}
        }
        public const string _number_contract_contragent_fName = "number_contract_contragent";        public const int _amount_fGid = 4984;
       
        public static mdTableField amount
        {
            get { return __MD.Fields[_amount_fGid];}
        }
        public const string _amount_fName = "amount";        public const int _master_contract_fGid = 4985;
       
        public static mdTableField master_contract
        {
            get { return __MD.Fields[_master_contract_fGid];}
        }
        public const string _master_contract_fName = "master_contract";        public const int _author_fGid = 4986;
       
        public static mdTableField author
        {
            get { return __MD.Fields[_author_fGid];}
        }
        public const string _author_fName = "author";       
        public static mdTableField contragent_name
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_contragent_fGid]);}
        }
        public const string _contragent_name_fName = "contragent_name";        public const int _representation_fGid = 4989;
       
        public static mdTableField representation
        {
            get { return __MD.Fields[_representation_fGid];}
        }
        public const string _representation_fName = "representation";        public const int _Company_representative_fGid = 5307;
       
        public static mdTableField Company_representative
        {
            get { return __MD.Fields[_Company_representative_fGid];}
        }
        public const string _Company_representative_fName = "Company_representative";        public const int _Contragent_representative_fGid = 5308;
       
        public static mdTableField Contragent_representative
        {
            get { return __MD.Fields[_Contragent_representative_fGid];}
        }
        public const string _Contragent_representative_fName = "Contragent_representative";        public const int _type_of_contract_fGid = 6334;
       
        public static mdTableField type_of_contract
        {
            get { return __MD.Fields[_type_of_contract_fGid];}
        }
        public const string _type_of_contract_fName = "type_of_contract";        public const int _Resource_Flow_Item_fGid = 9946;
       
        public static mdTableField Resource_Flow_Item
        {
            get { return __MD.Fields[_Resource_Flow_Item_fGid];}
        }
        public const string _Resource_Flow_Item_fName = "Resource_Flow_Item";        public const int _actual_fGid = 16651;
       
        public static mdTableField actual
        {
            get { return __MD.Fields[_actual_fGid];}
        }
        public const string _actual_fName = "actual";        public const int _asid_fGid = 20901;
       
        public static mdTableField asid
        {
            get { return __MD.Fields[_asid_fGid];}
        }
        public const string _asid_fName = "asid";        public const int _Department_fGid = 23387;
       
        public static mdTableField Department
        {
            get { return __MD.Fields[_Department_fGid];}
        }
        public const string _Department_fName = "Department";        public const int _Status_fGid = 26560;
       
        public static mdTableField Status
        {
            get { return __MD.Fields[_Status_fGid];}
        }
        public const string _Status_fName = "Status";        public const int _UpdateDateTime_fGid = 26566;
       
        public static mdTableField UpdateDateTime
        {
            get { return __MD.Fields[_UpdateDateTime_fGid];}
        }
        public const string _UpdateDateTime_fName = "UpdateDateTime";        public const int _IsAddition_fGid = 27533;
       
        public static mdTableField IsAddition
        {
            get { return __MD.Fields[_IsAddition_fGid];}
        }
        public const string _IsAddition_fName = "IsAddition";        public const int _FinProject_fGid = 30777;
       
        public static mdTableField FinProject
        {
            get { return __MD.Fields[_FinProject_fGid];}
        }
        public const string _FinProject_fName = "FinProject";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";       
        public static mdTableField company_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_company_fGid]);}
        }
        public const string _company_view__fName = "company_view_";       
        public static mdTableField master_contract_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_master_contract_fGid]);}
        }
        public const string _master_contract_view__fName = "master_contract_view_";       
        public static mdTableField author_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_author_fGid]);}
        }
        public const string _author_view__fName = "author_view_";       
        public static mdTableField Company_representative_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Company_representative_fGid]);}
        }
        public const string _Company_representative_view__fName = "Company_representative_view_";       
        public static mdTableField Contragent_representative_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Contragent_representative_fGid]);}
        }
        public const string _Contragent_representative_view__fName = "Contragent_representative_view_";       
        public static mdTableField type_of_contract_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_type_of_contract_fGid]);}
        }
        public const string _type_of_contract_view__fName = "type_of_contract_view_";       
        public static mdTableField Resource_Flow_Item_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Resource_Flow_Item_fGid]);}
        }
        public const string _Resource_Flow_Item_view__fName = "Resource_Flow_Item_view_";       
        public static mdTableField Department_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Department_fGid]);}
        }
        public const string _Department_view__fName = "Department_view_";       
        public static mdTableField FinProject_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_FinProject_fGid]);}
        }
        public const string _FinProject_view__fName = "FinProject_view_";       
        public static mdTableField company_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_company_fGid]);}
        }
        public const string _company_status__fName = "company_status_";       
        public static mdTableField contragent_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_contragent_fGid]);}
        }
        public const string _contragent_status__fName = "contragent_status_";       
        public static mdTableField master_contract_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_master_contract_fGid]);}
        }
        public const string _master_contract_status__fName = "master_contract_status_";       
        public static mdTableField author_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_author_fGid]);}
        }
        public const string _author_status__fName = "author_status_";       
        public static mdTableField Company_representative_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Company_representative_fGid]);}
        }
        public const string _Company_representative_status__fName = "Company_representative_status_";       
        public static mdTableField Contragent_representative_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Contragent_representative_fGid]);}
        }
        public const string _Contragent_representative_status__fName = "Contragent_representative_status_";       
        public static mdTableField type_of_contract_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_type_of_contract_fGid]);}
        }
        public const string _type_of_contract_status__fName = "type_of_contract_status_";       
        public static mdTableField Resource_Flow_Item_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Resource_Flow_Item_fGid]);}
        }
        public const string _Resource_Flow_Item_status__fName = "Resource_Flow_Item_status_";       
        public static mdTableField Department_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Department_fGid]);}
        }
        public const string _Department_status__fName = "Department_status_";       
        public static mdTableField FinProject_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_FinProject_fGid]);}
        }
        public const string _FinProject_status__fName = "FinProject_status_";
    }
    /// <summary>
    /// Схема таблиці 'Групи контрагентів'
    /// </summary>
    public static class contragent_groups_TS_   
    {
        public const int __tGid = 1172;
        public const string __tName = "contragent_groups";
        static mdTable m_mdTable;
        static contragent_groups_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _name_fGid = 4768;
       
        public static mdTableField name
        {
            get { return __MD.Fields[_name_fGid];}
        }
        public const string _name_fName = "name";        public const int _comments_fGid = 4769;
       
        public static mdTableField comments
        {
            get { return __MD.Fields[_comments_fGid];}
        }
        public const string _comments_fName = "comments";        public const int _object_status_fGid = 4770;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _version_no_fGid = 4771;
       
        public static mdTableField version_no
        {
            get { return __MD.Fields[_version_no_fGid];}
        }
        public const string _version_no_fName = "version_no";        public const int _gid_fGid = 4772;
       
        public static mdTableField gid
        {
            get { return __MD.Fields[_gid_fGid];}
        }
        public const string _gid_fName = "gid";        public const int _parent_group_fGid = 4774;
       
        public static mdTableField parent_group
        {
            get { return __MD.Fields[_parent_group_fGid];}
        }
        public const string _parent_group_fName = "parent_group";        public const int _asid_fGid = 20892;
       
        public static mdTableField asid
        {
            get { return __MD.Fields[_asid_fGid];}
        }
        public const string _asid_fName = "asid";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";       
        public static mdTableField parent_group_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_parent_group_fGid]);}
        }
        public const string _parent_group_view__fName = "parent_group_view_";       
        public static mdTableField parent_group_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_parent_group_fGid]);}
        }
        public const string _parent_group_status__fName = "parent_group_status_";
    }
    /// <summary>
    /// Схема таблиці 'Співробітники контрагентів'
    /// </summary>
    public static class contragent_persons_TS_   
    {
        public const int __tGid = 106;
        public const string __tName = "contragent_persons";
        static mdTable m_mdTable;
        static contragent_persons_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _Name_fGid = 113;
       
        public static mdTableField Name
        {
            get { return __MD.Fields[_Name_fGid];}
        }
        public const string _Name_fName = "Name";        public const int _contragent_fGid = 114;
       
        public static mdTableField contragent
        {
            get { return __MD.Fields[_contragent_fGid];}
        }
        public const string _contragent_fName = "contragent";        public const int _phones_fGid = 116;
       
        public static mdTableField phones
        {
            get { return __MD.Fields[_phones_fGid];}
        }
        public const string _phones_fName = "phones";        public const int _comments_fGid = 117;
       
        public static mdTableField comments
        {
            get { return __MD.Fields[_comments_fGid];}
        }
        public const string _comments_fName = "comments";        public const int _e_mail_fGid = 119;
       
        public static mdTableField e_mail
        {
            get { return __MD.Fields[_e_mail_fGid];}
        }
        public const string _e_mail_fName = "e_mail";        public const int _gid_fGid = 162;
       
        public static mdTableField gid
        {
            get { return __MD.Fields[_gid_fGid];}
        }
        public const string _gid_fName = "gid";        public const int _object_status_fGid = 237;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _edrpou_code_fGid = 23490;
       
        public static mdTableField edrpou_code
        {
            get { return __MD.Fields[_edrpou_code_fGid];}
        }
        public const string _edrpou_code_fName = "edrpou_code";        public const int _Middle_name_fGid = 28488;
       
        public static mdTableField Middle_name
        {
            get { return __MD.Fields[_Middle_name_fGid];}
        }
        public const string _Middle_name_fName = "Middle_name";        public const int _Surname_fGid = 28489;
       
        public static mdTableField Surname
        {
            get { return __MD.Fields[_Surname_fGid];}
        }
        public const string _Surname_fName = "Surname";        public const int _Full_Name_fGid = 28491;
       
        public static mdTableField Full_Name
        {
            get { return __MD.Fields[_Full_Name_fGid];}
        }
        public const string _Full_Name_fName = "Full_Name";        public const int _Person_fGid = 31759;
       
        public static mdTableField Person
        {
            get { return __MD.Fields[_Person_fGid];}
        }
        public const string _Person_fName = "Person";       
        public static mdTableField contragent_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_contragent_fGid]);}
        }
        public const string _contragent_view__fName = "contragent_view_";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";       
        public static mdTableField Person_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Person_fGid]);}
        }
        public const string _Person_view__fName = "Person_view_";       
        public static mdTableField contragent_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_contragent_fGid]);}
        }
        public const string _contragent_status__fName = "contragent_status_";       
        public static mdTableField Person_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Person_fGid]);}
        }
        public const string _Person_status__fName = "Person_status_";
    }
    /// <summary>
    /// Схема таблиці 'Контрагенти'
    /// </summary>
    public static class contragents_TS_   
    {
        public const int __tGid = 105;
        public const string __tName = "contragents";
        static mdTable m_mdTable;
        static contragents_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _gid_fGid = 558;
       
        public static mdTableField gid
        {
            get { return __MD.Fields[_gid_fGid];}
        }
        public const string _gid_fName = "gid";        public const int _name_fGid = 561;
       
        public static mdTableField name
        {
            get { return __MD.Fields[_name_fGid];}
        }
        public const string _name_fName = "name";        public const int _legal_name_fGid = 562;
       
        public static mdTableField legal_name
        {
            get { return __MD.Fields[_legal_name_fGid];}
        }
        public const string _legal_name_fName = "legal_name";        public const int _legal_address_fGid = 563;
       
        public static mdTableField legal_address
        {
            get { return __MD.Fields[_legal_address_fGid];}
        }
        public const string _legal_address_fName = "legal_address";        public const int _edrpou_code_fGid = 564;
       
        public static mdTableField edrpou_code
        {
            get { return __MD.Fields[_edrpou_code_fGid];}
        }
        public const string _edrpou_code_fName = "edrpou_code";        public const int _ipn_code_fGid = 565;
       
        public static mdTableField ipn_code
        {
            get { return __MD.Fields[_ipn_code_fGid];}
        }
        public const string _ipn_code_fName = "ipn_code";        public const int _comments_fGid = 570;
       
        public static mdTableField comments
        {
            get { return __MD.Fields[_comments_fGid];}
        }
        public const string _comments_fName = "comments";        public const int _object_status_fGid = 1298;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _version_no_fGid = 3434;
       
        public static mdTableField version_no
        {
            get { return __MD.Fields[_version_no_fGid];}
        }
        public const string _version_no_fName = "version_no";        public const int _storage_gid_fGid = 4892;
       
        public static mdTableField storage_gid
        {
            get { return __MD.Fields[_storage_gid_fGid];}
        }
        public const string _storage_gid_fName = "storage_gid";        public const int _fact_address_fGid = 5533;
       
        public static mdTableField fact_address
        {
            get { return __MD.Fields[_fact_address_fGid];}
        }
        public const string _fact_address_fName = "fact_address";        public const int _e_mail_fGid = 5546;
       
        public static mdTableField e_mail
        {
            get { return __MD.Fields[_e_mail_fGid];}
        }
        public const string _e_mail_fName = "e_mail";        public const int _phones_fGid = 5564;
       
        public static mdTableField phones
        {
            get { return __MD.Fields[_phones_fGid];}
        }
        public const string _phones_fName = "phones";        public const int _contragent_group_fGid = 5831;
       
        public static mdTableField contragent_group
        {
            get { return __MD.Fields[_contragent_group_fGid];}
        }
        public const string _contragent_group_fName = "contragent_group";        public const int _main_money_account_fGid = 5892;
       
        public static mdTableField main_money_account
        {
            get { return __MD.Fields[_main_money_account_fGid];}
        }
        public const string _main_money_account_fName = "main_money_account";        public const int _asid_fGid = 9740;
       
        public static mdTableField asid
        {
            get { return __MD.Fields[_asid_fGid];}
        }
        public const string _asid_fName = "asid";        public const int _contragent_as_company_fGid = 24128;
       
        public static mdTableField contragent_as_company
        {
            get { return __MD.Fields[_contragent_as_company_fGid];}
        }
        public const string _contragent_as_company_fName = "contragent_as_company";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";       
        public static mdTableField storage_gid_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_storage_gid_fGid]);}
        }
        public const string _storage_gid_view__fName = "storage_gid_view_";       
        public static mdTableField contragent_group_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_contragent_group_fGid]);}
        }
        public const string _contragent_group_view__fName = "contragent_group_view_";       
        public static mdTableField main_money_account_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_main_money_account_fGid]);}
        }
        public const string _main_money_account_view__fName = "main_money_account_view_";       
        public static mdTableField storage_gid_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_storage_gid_fGid]);}
        }
        public const string _storage_gid_status__fName = "storage_gid_status_";       
        public static mdTableField contragent_group_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_contragent_group_fGid]);}
        }
        public const string _contragent_group_status__fName = "contragent_group_status_";       
        public static mdTableField main_money_account_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_main_money_account_fGid]);}
        }
        public const string _main_money_account_status__fName = "main_money_account_status_";
    }
    /// <summary>
    /// Схема таблиці 'Банківські рахунки контрагентів'
    /// </summary>
    public static class contragents_money_accounts_TS_   
    {
        public const int __tGid = 1176;
        public const string __tName = "contragents_money_accounts";
        static mdTable m_mdTable;
        static contragents_money_accounts_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _name_fGid = 5884;
       
        public static mdTableField name
        {
            get { return __MD.Fields[_name_fGid];}
        }
        public const string _name_fName = "name";        public const int _contragent_fGid = 5886;
       
        public static mdTableField contragent
        {
            get { return __MD.Fields[_contragent_fGid];}
        }
        public const string _contragent_fName = "contragent";        public const int _account_number_fGid = 5887;
       
        public static mdTableField account_number
        {
            get { return __MD.Fields[_account_number_fGid];}
        }
        public const string _account_number_fName = "account_number";       
        public static mdTableField MFO
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Bank_fGid]);}
        }
        public const string _MFO_fName = "MFO";        public const int _name_bank_fGid = 5889;
       
        public static mdTableField name_bank
        {
            get { return __MD.Fields[_name_bank_fGid];}
        }
        public const string _name_bank_fName = "name_bank";        public const int _adress_bank_fGid = 5890;
       
        public static mdTableField adress_bank
        {
            get { return __MD.Fields[_adress_bank_fGid];}
        }
        public const string _adress_bank_fName = "adress_bank";        public const int _gid_fGid = 5891;
       
        public static mdTableField gid
        {
            get { return __MD.Fields[_gid_fGid];}
        }
        public const string _gid_fName = "gid";        public const int _contragents_money_account_fGid = 5893;
       
        public static mdTableField contragents_money_account
        {
            get { return __MD.Fields[_contragents_money_account_fGid];}
        }
        public const string _contragents_money_account_fName = "contragents_money_account";        public const int _is_main_bank_account_fGid = 5894;
       
        public static mdTableField is_main_bank_account
        {
            get { return __MD.Fields[_is_main_bank_account_fGid];}
        }
        public const string _is_main_bank_account_fName = "is_main_bank_account";        public const int _Bank_account_view_fGid = 5895;
       
        public static mdTableField Bank_account_view
        {
            get { return __MD.Fields[_Bank_account_view_fGid];}
        }
        public const string _Bank_account_view_fName = "Bank_account_view";        public const int _asid_fGid = 20111;
       
        public static mdTableField asid
        {
            get { return __MD.Fields[_asid_fGid];}
        }
        public const string _asid_fName = "asid";        public const int _Bank_fGid = 20713;
       
        public static mdTableField Bank
        {
            get { return __MD.Fields[_Bank_fGid];}
        }
        public const string _Bank_fName = "Bank";       
        public static mdTableField contragent_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_contragent_fGid]);}
        }
        public const string _contragent_view__fName = "contragent_view_";       
        public static mdTableField contragent_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_contragent_fGid]);}
        }
        public const string _contragent_status__fName = "contragent_status_";       
        public static mdTableField Bank_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Bank_fGid]);}
        }
        public const string _Bank_status__fName = "Bank_status_";
    }
    /// <summary>
    /// Схема таблиці 'Платіжний календар'
    /// </summary>
    public static class Payments_Shedule_TS_   
    {
        public const int __tGid = 15695;
        public const string __tName = "Payments_Shedule";
        static mdTable m_mdTable;
        static Payments_Shedule_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _contragent_fGid = 29941;
       
        public static mdTableField contragent
        {
            get { return __MD.Fields[_contragent_fGid];}
        }
        public const string _contragent_fName = "contragent";        public const int _gid_fGid = 29942;
       
        public static mdTableField gid
        {
            get { return __MD.Fields[_gid_fGid];}
        }
        public const string _gid_fName = "gid";        public const int _Contract_fGid = 29943;
       
        public static mdTableField Contract
        {
            get { return __MD.Fields[_Contract_fGid];}
        }
        public const string _Contract_fName = "Contract";        public const int _object_status_fGid = 29945;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _Payment_Amount_fGid = 29946;
       
        public static mdTableField Payment_Amount
        {
            get { return __MD.Fields[_Payment_Amount_fGid];}
        }
        public const string _Payment_Amount_fName = "Payment_Amount";        public const int _purpose_of_payment_fGid = 29947;
       
        public static mdTableField purpose_of_payment
        {
            get { return __MD.Fields[_purpose_of_payment_fGid];}
        }
        public const string _purpose_of_payment_fName = "purpose_of_payment";        public const int _PlannedDate_fGid = 29948;
       
        public static mdTableField PlannedDate
        {
            get { return __MD.Fields[_PlannedDate_fGid];}
        }
        public const string _PlannedDate_fName = "PlannedDate";        public const int _company_fGid = 29959;
       
        public static mdTableField company
        {
            get { return __MD.Fields[_company_fGid];}
        }
        public const string _company_fName = "company";        public const int _money_account_fGid = 29960;
       
        public static mdTableField money_account
        {
            get { return __MD.Fields[_money_account_fGid];}
        }
        public const string _money_account_fName = "money_account";        public const int _IsOpen_fGid = 29961;
       
        public static mdTableField IsOpen
        {
            get { return __MD.Fields[_IsOpen_fGid];}
        }
        public const string _IsOpen_fName = "IsOpen";        public const int _FactAmount_fGid = 29967;
       
        public static mdTableField FactAmount
        {
            get { return __MD.Fields[_FactAmount_fGid];}
        }
        public const string _FactAmount_fName = "FactAmount";        public const int _Priority_Level_fGid = 29968;
       
        public static mdTableField Priority_Level
        {
            get { return __MD.Fields[_Priority_Level_fGid];}
        }
        public const string _Priority_Level_fName = "Priority_Level";        public const int _PriorityNo_fGid = 29969;
       
        public static mdTableField PriorityNo
        {
            get { return __MD.Fields[_PriorityNo_fGid];}
        }
        public const string _PriorityNo_fName = "PriorityNo";        public const int _version_no_fGid = 29982;
       
        public static mdTableField version_no
        {
            get { return __MD.Fields[_version_no_fGid];}
        }
        public const string _version_no_fName = "version_no";        public const int _RegularPayment_fGid = 29994;
       
        public static mdTableField RegularPayment
        {
            get { return __MD.Fields[_RegularPayment_fGid];}
        }
        public const string _RegularPayment_fName = "RegularPayment";        public const int _contract_number_fGid = 30070;
       
        public static mdTableField contract_number
        {
            get { return __MD.Fields[_contract_number_fGid];}
        }
        public const string _contract_number_fName = "contract_number";        public const int _date_agreement_fGid = 30105;
       
        public static mdTableField date_agreement
        {
            get { return __MD.Fields[_date_agreement_fGid];}
        }
        public const string _date_agreement_fName = "date_agreement";        public const int _Company_representative_Contract_fGid = 30107;
       
        public static mdTableField Company_representative_Contract
        {
            get { return __MD.Fields[_Company_representative_Contract_fGid];}
        }
        public const string _Company_representative_Contract_fName = "Company_representative_Contract";        public const int _PayAmountStr_fGid = 30265;
       
        public static mdTableField PayAmountStr
        {
            get { return __MD.Fields[_PayAmountStr_fGid];}
        }
        public const string _PayAmountStr_fName = "PayAmountStr";        public const int _PayDateStr_fGid = 30266;
       
        public static mdTableField PayDateStr
        {
            get { return __MD.Fields[_PayDateStr_fGid];}
        }
        public const string _PayDateStr_fName = "PayDateStr";        public const int _representation_fGid = 30267;
       
        public static mdTableField representation
        {
            get { return __MD.Fields[_representation_fGid];}
        }
        public const string _representation_fName = "representation";        public const int _Base_Document_BJ_fGid = 30450;
       
        public static mdTableField Base_Document_BJ
        {
            get { return __MD.Fields[_Base_Document_BJ_fGid];}
        }
        public const string _Base_Document_BJ_fName = "Base_Document_BJ";        public const int _BDocNumber_fGid = 33967;
       
        public static mdTableField BDocNumber
        {
            get { return __MD.Fields[_BDocNumber_fGid];}
        }
        public const string _BDocNumber_fName = "BDocNumber";        public const int _BDocDate_fGid = 33968;
       
        public static mdTableField BDocDate
        {
            get { return __MD.Fields[_BDocDate_fGid];}
        }
        public const string _BDocDate_fName = "BDocDate";       
        public static mdTableField contragent_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_contragent_fGid]);}
        }
        public const string _contragent_view__fName = "contragent_view_";       
        public static mdTableField Contract_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Contract_fGid]);}
        }
        public const string _Contract_view__fName = "Contract_view_";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";       
        public static mdTableField company_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_company_fGid]);}
        }
        public const string _company_view__fName = "company_view_";       
        public static mdTableField money_account_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_money_account_fGid]);}
        }
        public const string _money_account_view__fName = "money_account_view_";       
        public static mdTableField RegularPayment_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_RegularPayment_fGid]);}
        }
        public const string _RegularPayment_view__fName = "RegularPayment_view_";       
        public static mdTableField Base_Document_BJ_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Base_Document_BJ_fGid]);}
        }
        public const string _Base_Document_BJ_view__fName = "Base_Document_BJ_view_";       
        public static mdTableField contragent_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_contragent_fGid]);}
        }
        public const string _contragent_status__fName = "contragent_status_";       
        public static mdTableField Contract_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Contract_fGid]);}
        }
        public const string _Contract_status__fName = "Contract_status_";       
        public static mdTableField company_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_company_fGid]);}
        }
        public const string _company_status__fName = "company_status_";       
        public static mdTableField money_account_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_money_account_fGid]);}
        }
        public const string _money_account_status__fName = "money_account_status_";       
        public static mdTableField RegularPayment_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_RegularPayment_fGid]);}
        }
        public const string _RegularPayment_status__fName = "RegularPayment_status_";       
        public static mdTableField Base_Document_BJ_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Base_Document_BJ_fGid]);}
        }
        public const string _Base_Document_BJ_status__fName = "Base_Document_BJ_status_";
    }
    /// <summary>
    /// Схема таблиці 'Регулярні платежі'
    /// </summary>
    public static class RegularPayments_TS_   
    {
        public const int __tGid = 15697;
        public const string __tName = "RegularPayments";
        static mdTable m_mdTable;
        static RegularPayments_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _gid_fGid = 29983;
       
        public static mdTableField gid
        {
            get { return __MD.Fields[_gid_fGid];}
        }
        public const string _gid_fName = "gid";        public const int _contragent_fGid = 29984;
       
        public static mdTableField contragent
        {
            get { return __MD.Fields[_contragent_fGid];}
        }
        public const string _contragent_fName = "contragent";        public const int _object_status_fGid = 29985;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _Contract_fGid = 29986;
       
        public static mdTableField Contract
        {
            get { return __MD.Fields[_Contract_fGid];}
        }
        public const string _Contract_fName = "Contract";        public const int _Payment_Amount_fGid = 29987;
       
        public static mdTableField Payment_Amount
        {
            get { return __MD.Fields[_Payment_Amount_fGid];}
        }
        public const string _Payment_Amount_fName = "Payment_Amount";        public const int _purpose_of_payment_fGid = 29988;
       
        public static mdTableField purpose_of_payment
        {
            get { return __MD.Fields[_purpose_of_payment_fGid];}
        }
        public const string _purpose_of_payment_fName = "purpose_of_payment";        public const int _money_account_fGid = 29989;
       
        public static mdTableField money_account
        {
            get { return __MD.Fields[_money_account_fGid];}
        }
        public const string _money_account_fName = "money_account";        public const int _company_fGid = 29990;
       
        public static mdTableField company
        {
            get { return __MD.Fields[_company_fGid];}
        }
        public const string _company_fName = "company";        public const int _version_no_fGid = 29991;
       
        public static mdTableField version_no
        {
            get { return __MD.Fields[_version_no_fGid];}
        }
        public const string _version_no_fName = "version_no";        public const int _StartDate_fGid = 29992;
       
        public static mdTableField StartDate
        {
            get { return __MD.Fields[_StartDate_fGid];}
        }
        public const string _StartDate_fName = "StartDate";        public const int _EndDate_fGid = 29993;
       
        public static mdTableField EndDate
        {
            get { return __MD.Fields[_EndDate_fGid];}
        }
        public const string _EndDate_fName = "EndDate";        public const int _Periodicity_fGid = 30031;
       
        public static mdTableField Periodicity
        {
            get { return __MD.Fields[_Periodicity_fGid];}
        }
        public const string _Periodicity_fName = "Periodicity";       
        public static mdTableField contragent_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_contragent_fGid]);}
        }
        public const string _contragent_view__fName = "contragent_view_";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";       
        public static mdTableField Contract_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Contract_fGid]);}
        }
        public const string _Contract_view__fName = "Contract_view_";       
        public static mdTableField money_account_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_money_account_fGid]);}
        }
        public const string _money_account_view__fName = "money_account_view_";       
        public static mdTableField company_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_company_fGid]);}
        }
        public const string _company_view__fName = "company_view_";       
        public static mdTableField contragent_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_contragent_fGid]);}
        }
        public const string _contragent_status__fName = "contragent_status_";       
        public static mdTableField Contract_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Contract_fGid]);}
        }
        public const string _Contract_status__fName = "Contract_status_";       
        public static mdTableField money_account_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_money_account_fGid]);}
        }
        public const string _money_account_status__fName = "money_account_status_";       
        public static mdTableField company_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_company_fGid]);}
        }
        public const string _company_status__fName = "company_status_";
    }
    /// <summary>
    /// Схема таблиці 'Взаєморозрахунки з контрагентами'
    /// </summary>
    public static class settlements_TS_   
    {
        public const int __tGid = 1177;
        public const string __tName = "settlements";
        static mdTable m_mdTable;
        static settlements_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _recorder_fGid = 5896;
       
        public static mdTableField recorder
        {
            get { return __MD.Fields[_recorder_fGid];}
        }
        public const string _recorder_fName = "recorder";        public const int _contragent_fGid = 5900;
       
        public static mdTableField contragent
        {
            get { return __MD.Fields[_contragent_fGid];}
        }
        public const string _contragent_fName = "contragent";        public const int _base_document_fGid = 5902;
       
        public static mdTableField base_document
        {
            get { return __MD.Fields[_base_document_fGid];}
        }
        public const string _base_document_fName = "base_document";        public const int _amount_fGid = 5903;
       
        public static mdTableField amount
        {
            get { return __MD.Fields[_amount_fGid];}
        }
        public const string _amount_fName = "amount";        public const int _company_fGid = 5904;
       
        public static mdTableField company
        {
            get { return __MD.Fields[_company_fGid];}
        }
        public const string _company_fName = "company";        public const int _reg_date_fGid = 5908;
       
        public static mdTableField reg_date
        {
            get { return __MD.Fields[_reg_date_fGid];}
        }
        public const string _reg_date_fName = "reg_date";        public const int _action_number_fGid = 5909;
       
        public static mdTableField action_number
        {
            get { return __MD.Fields[_action_number_fGid];}
        }
        public const string _action_number_fName = "action_number";        public const int _is_outgo_fGid = 5910;
       
        public static mdTableField is_outgo
        {
            get { return __MD.Fields[_is_outgo_fGid];}
        }
        public const string _is_outgo_fName = "is_outgo";        public const int _Resource_Flow_Item_fGid = 9085;
       
        public static mdTableField Resource_Flow_Item
        {
            get { return __MD.Fields[_Resource_Flow_Item_fGid];}
        }
        public const string _Resource_Flow_Item_fName = "Resource_Flow_Item";        public const int _BalanseAccount_fGid = 9086;
       
        public static mdTableField BalanseAccount
        {
            get { return __MD.Fields[_BalanseAccount_fGid];}
        }
        public const string _BalanseAccount_fName = "BalanseAccount";        public const int _recorder_tid_fGid = 22862;
       
        public static mdTableField recorder_tid
        {
            get { return __MD.Fields[_recorder_tid_fGid];}
        }
        public const string _recorder_tid_fName = "recorder_tid";       
        public static mdTableField contragent_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_contragent_fGid]);}
        }
        public const string _contragent_view__fName = "contragent_view_";       
        public static mdTableField base_document_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_base_document_fGid]);}
        }
        public const string _base_document_view__fName = "base_document_view_";       
        public static mdTableField company_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_company_fGid]);}
        }
        public const string _company_view__fName = "company_view_";       
        public static mdTableField Resource_Flow_Item_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Resource_Flow_Item_fGid]);}
        }
        public const string _Resource_Flow_Item_view__fName = "Resource_Flow_Item_view_";       
        public static mdTableField BalanseAccount_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_BalanseAccount_fGid]);}
        }
        public const string _BalanseAccount_view__fName = "BalanseAccount_view_";       
        public static mdTableField recorder_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_recorder_fGid]);}
        }
        public const string _recorder_view__fName = "recorder_view_";        public const int _recorder_view_documents__fGid = 99000078;
       
        public static mdTableField recorder_view_documents_
        {
            get { return __MD.Fields[_recorder_view_documents__fGid];}
        }
        public const string _recorder_view_documents__fName = "recorder_view_documents_";       
        public static mdTableField contragent_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_contragent_fGid]);}
        }
        public const string _contragent_status__fName = "contragent_status_";       
        public static mdTableField base_document_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_base_document_fGid]);}
        }
        public const string _base_document_status__fName = "base_document_status_";       
        public static mdTableField company_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_company_fGid]);}
        }
        public const string _company_status__fName = "company_status_";       
        public static mdTableField Resource_Flow_Item_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Resource_Flow_Item_fGid]);}
        }
        public const string _Resource_Flow_Item_status__fName = "Resource_Flow_Item_status_";       
        public static mdTableField BalanseAccount_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_BalanseAccount_fGid]);}
        }
        public const string _BalanseAccount_status__fName = "BalanseAccount_status_";
    }
    /// <summary>
    /// Схема таблиці 'Взаєморозрахунки з контрагентами'
    /// </summary>
    public static class settlements_dynamic_totals_TS_   
    {
        public const int __tGid = 1179;
        public const string __tName = "settlements_dynamic_totals";
        static mdTable m_mdTable;
        static settlements_dynamic_totals_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _contragent_fGid = 5913;
       
        public static mdTableField contragent
        {
            get { return __MD.Fields[_contragent_fGid];}
        }
        public const string _contragent_fName = "contragent";        public const int _base_document_fGid = 5915;
       
        public static mdTableField base_document
        {
            get { return __MD.Fields[_base_document_fGid];}
        }
        public const string _base_document_fName = "base_document";        public const int _amount_in_fGid = 5919;
       
        public static mdTableField amount_in
        {
            get { return __MD.Fields[_amount_in_fGid];}
        }
        public const string _amount_in_fName = "amount_in";        public const int _amount_out_fGid = 5920;
       
        public static mdTableField amount_out
        {
            get { return __MD.Fields[_amount_out_fGid];}
        }
        public const string _amount_out_fName = "amount_out";        public const int _amount_rest_fGid = 5921;
       
        public static mdTableField amount_rest
        {
            get { return __MD.Fields[_amount_rest_fGid];}
        }
        public const string _amount_rest_fName = "amount_rest";        public const int _amount_initial_rest_fGid = 5922;
       
        public static mdTableField amount_initial_rest
        {
            get { return __MD.Fields[_amount_initial_rest_fGid];}
        }
        public const string _amount_initial_rest_fName = "amount_initial_rest";        public const int _company_fGid = 5924;
       
        public static mdTableField company
        {
            get { return __MD.Fields[_company_fGid];}
        }
        public const string _company_fName = "company";        public const int _recorder_fGid = 6042;
       
        public static mdTableField recorder
        {
            get { return __MD.Fields[_recorder_fGid];}
        }
        public const string _recorder_fName = "recorder";        public const int _reg_date_fGid = 6043;
       
        public static mdTableField reg_date
        {
            get { return __MD.Fields[_reg_date_fGid];}
        }
        public const string _reg_date_fName = "reg_date";        public const int _BalanseAccount_fGid = 9088;
       
        public static mdTableField BalanseAccount
        {
            get { return __MD.Fields[_BalanseAccount_fGid];}
        }
        public const string _BalanseAccount_fName = "BalanseAccount";        public const int _amount_initial_rest_credit_fGid = 9282;
       
        public static mdTableField amount_initial_rest_credit
        {
            get { return __MD.Fields[_amount_initial_rest_credit_fGid];}
        }
        public const string _amount_initial_rest_credit_fName = "amount_initial_rest_credit";        public const int _amount_initial_rest_debit_fGid = 9283;
       
        public static mdTableField amount_initial_rest_debit
        {
            get { return __MD.Fields[_amount_initial_rest_debit_fGid];}
        }
        public const string _amount_initial_rest_debit_fName = "amount_initial_rest_debit";        public const int _amount_rest_debit_fGid = 9288;
       
        public static mdTableField amount_rest_debit
        {
            get { return __MD.Fields[_amount_rest_debit_fGid];}
        }
        public const string _amount_rest_debit_fName = "amount_rest_debit";        public const int _amount_rest_credit_fGid = 9289;
       
        public static mdTableField amount_rest_credit
        {
            get { return __MD.Fields[_amount_rest_credit_fGid];}
        }
        public const string _amount_rest_credit_fName = "amount_rest_credit";        public const int _company_tid_fGid = 9323;
       
        public static mdTableField company_tid
        {
            get { return __MD.Fields[_company_tid_fGid];}
        }
        public const string _company_tid_fName = "company_tid";        public const int _sign_initial_rest_fGid = 9419;
       
        public static mdTableField sign_initial_rest
        {
            get { return __MD.Fields[_sign_initial_rest_fGid];}
        }
        public const string _sign_initial_rest_fName = "sign_initial_rest";        public const int _SIGN_rest_fGid = 9420;
       
        public static mdTableField SIGN_rest
        {
            get { return __MD.Fields[_SIGN_rest_fGid];}
        }
        public const string _SIGN_rest_fName = "SIGN_rest";        public const int _recorder_tid_fGid = 9431;
       
        public static mdTableField recorder_tid
        {
            get { return __MD.Fields[_recorder_tid_fGid];}
        }
        public const string _recorder_tid_fName = "recorder_tid";        public const int _MonthOfPeriod_fGid = 29468;
       
        public static mdTableField MonthOfPeriod
        {
            get { return __MD.Fields[_MonthOfPeriod_fGid];}
        }
        public const string _MonthOfPeriod_fName = "MonthOfPeriod";        public const int _BaseDocBJ_RegDate_fGid = 32914;
       
        public static mdTableField BaseDocBJ_RegDate
        {
            get { return __MD.Fields[_BaseDocBJ_RegDate_fGid];}
        }
        public const string _BaseDocBJ_RegDate_fName = "BaseDocBJ_RegDate";        public const int _contragent_tid_fGid = 32931;
       
        public static mdTableField contragent_tid
        {
            get { return __MD.Fields[_contragent_tid_fGid];}
        }
        public const string _contragent_tid_fName = "contragent_tid";        public const int _Resource_Flow_Item_fGid = 32933;
       
        public static mdTableField Resource_Flow_Item
        {
            get { return __MD.Fields[_Resource_Flow_Item_fGid];}
        }
        public const string _Resource_Flow_Item_fName = "Resource_Flow_Item";       
        public static mdTableField contragent_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_contragent_fGid]);}
        }
        public const string _contragent_view__fName = "contragent_view_";       
        public static mdTableField base_document_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_base_document_fGid]);}
        }
        public const string _base_document_view__fName = "base_document_view_";       
        public static mdTableField company_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_company_fGid]);}
        }
        public const string _company_view__fName = "company_view_";       
        public static mdTableField BalanseAccount_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_BalanseAccount_fGid]);}
        }
        public const string _BalanseAccount_view__fName = "BalanseAccount_view_";       
        public static mdTableField Resource_Flow_Item_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Resource_Flow_Item_fGid]);}
        }
        public const string _Resource_Flow_Item_view__fName = "Resource_Flow_Item_view_";       
        public static mdTableField recorder_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_recorder_fGid]);}
        }
        public const string _recorder_view__fName = "recorder_view_";        public const int _recorder_view_documents__fGid = 99000090;
       
        public static mdTableField recorder_view_documents_
        {
            get { return __MD.Fields[_recorder_view_documents__fGid];}
        }
        public const string _recorder_view_documents__fName = "recorder_view_documents_";
    }
    /// <summary>
    /// Схема таблиці 'Взаєморозрахунки з контрагентами'
    /// </summary>
    public static class settlements_totals_TS_   
    {
        public const int __tGid = 1178;
        public const string __tName = "settlements_totals";
        static mdTable m_mdTable;
        static settlements_totals_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _operation_period_fGid = 5911;
       
        public static mdTableField operation_period
        {
            get { return __MD.Fields[_operation_period_fGid];}
        }
        public const string _operation_period_fName = "operation_period";        public const int _contragent_fGid = 5912;
       
        public static mdTableField contragent
        {
            get { return __MD.Fields[_contragent_fGid];}
        }
        public const string _contragent_fName = "contragent";        public const int _base_document_fGid = 5914;
       
        public static mdTableField base_document
        {
            get { return __MD.Fields[_base_document_fGid];}
        }
        public const string _base_document_fName = "base_document";        public const int _amount_in_fGid = 5916;
       
        public static mdTableField amount_in
        {
            get { return __MD.Fields[_amount_in_fGid];}
        }
        public const string _amount_in_fName = "amount_in";        public const int _amount_out_fGid = 5917;
       
        public static mdTableField amount_out
        {
            get { return __MD.Fields[_amount_out_fGid];}
        }
        public const string _amount_out_fName = "amount_out";        public const int _amount_rest_fGid = 5918;
       
        public static mdTableField amount_rest
        {
            get { return __MD.Fields[_amount_rest_fGid];}
        }
        public const string _amount_rest_fName = "amount_rest";        public const int _company_fGid = 5923;
       
        public static mdTableField company
        {
            get { return __MD.Fields[_company_fGid];}
        }
        public const string _company_fName = "company";        public const int _BalanseAccount_fGid = 9087;
       
        public static mdTableField BalanseAccount
        {
            get { return __MD.Fields[_BalanseAccount_fGid];}
        }
        public const string _BalanseAccount_fName = "BalanseAccount";       
        public static mdTableField operation_period_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_operation_period_fGid]);}
        }
        public const string _operation_period_view__fName = "operation_period_view_";       
        public static mdTableField contragent_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_contragent_fGid]);}
        }
        public const string _contragent_view__fName = "contragent_view_";       
        public static mdTableField base_document_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_base_document_fGid]);}
        }
        public const string _base_document_view__fName = "base_document_view_";       
        public static mdTableField company_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_company_fGid]);}
        }
        public const string _company_view__fName = "company_view_";       
        public static mdTableField BalanseAccount_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_BalanseAccount_fGid]);}
        }
        public const string _BalanseAccount_view__fName = "BalanseAccount_view_";       
        public static mdTableField operation_period_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_operation_period_fGid]);}
        }
        public const string _operation_period_status__fName = "operation_period_status_";       
        public static mdTableField contragent_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_contragent_fGid]);}
        }
        public const string _contragent_status__fName = "contragent_status_";       
        public static mdTableField base_document_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_base_document_fGid]);}
        }
        public const string _base_document_status__fName = "base_document_status_";       
        public static mdTableField company_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_company_fGid]);}
        }
        public const string _company_status__fName = "company_status_";       
        public static mdTableField BalanseAccount_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_BalanseAccount_fGid]);}
        }
        public const string _BalanseAccount_status__fName = "BalanseAccount_status_";
    }
    /// <summary>
    /// Схема таблиці 'Види договорів'
    /// </summary>
    public static class types_of_contract_TS_   
    {
        public const int __tGid = 1212;
        public const string __tName = "types_of_contract";
        static mdTable m_mdTable;
        static types_of_contract_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _name_fGid = 6325;
       
        public static mdTableField name
        {
            get { return __MD.Fields[_name_fGid];}
        }
        public const string _name_fName = "name";        public const int _comment_fGid = 6326;
       
        public static mdTableField comment
        {
            get { return __MD.Fields[_comment_fGid];}
        }
        public const string _comment_fName = "comment";        public const int _gid_fGid = 6327;
       
        public static mdTableField gid
        {
            get { return __MD.Fields[_gid_fGid];}
        }
        public const string _gid_fName = "gid";        public const int _object_status_fGid = 6333;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _version_no_fGid = 23252;
       
        public static mdTableField version_no
        {
            get { return __MD.Fields[_version_no_fGid];}
        }
        public const string _version_no_fName = "version_no";        public const int _IsAddition_fGid = 27531;
       
        public static mdTableField IsAddition
        {
            get { return __MD.Fields[_IsAddition_fGid];}
        }
        public const string _IsAddition_fName = "IsAddition";        public const int _IsForBuilding_fGid = 28970;
       
        public static mdTableField IsForBuilding
        {
            get { return __MD.Fields[_IsForBuilding_fGid];}
        }
        public const string _IsForBuilding_fName = "IsForBuilding";        public const int _ContractForm_fGid = 32429;
       
        public static mdTableField ContractForm
        {
            get { return __MD.Fields[_ContractForm_fGid];}
        }
        public const string _ContractForm_fName = "ContractForm";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";
    }
    /// <summary>
    /// Схема таблиці 'Маркетингові проекти'
    /// </summary>
    public static class MarketingProjects_TS_   
    {
        public const int __tGid = 15768;
        public const string __tName = "MarketingProjects";
        static mdTable m_mdTable;
        static MarketingProjects_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _gid_fGid = 31630;
       
        public static mdTableField gid
        {
            get { return __MD.Fields[_gid_fGid];}
        }
        public const string _gid_fName = "gid";        public const int _name_fGid = 31631;
       
        public static mdTableField name
        {
            get { return __MD.Fields[_name_fGid];}
        }
        public const string _name_fName = "name";        public const int _object_status_fGid = 31632;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _Description_fGid = 31633;
       
        public static mdTableField Description
        {
            get { return __MD.Fields[_Description_fGid];}
        }
        public const string _Description_fName = "Description";        public const int _comments_fGid = 31634;
       
        public static mdTableField comments
        {
            get { return __MD.Fields[_comments_fGid];}
        }
        public const string _comments_fName = "comments";        public const int _version_no_fGid = 31635;
       
        public static mdTableField version_no
        {
            get { return __MD.Fields[_version_no_fGid];}
        }
        public const string _version_no_fName = "version_no";        public const int _Customer_fGid = 31636;
       
        public static mdTableField Customer
        {
            get { return __MD.Fields[_Customer_fGid];}
        }
        public const string _Customer_fName = "Customer";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";       
        public static mdTableField Customer_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Customer_fGid]);}
        }
        public const string _Customer_view__fName = "Customer_view_";       
        public static mdTableField Customer_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Customer_fGid]);}
        }
        public const string _Customer_status__fName = "Customer_status_";
    }
    /// <summary>
    /// Схема таблиці 'Респонденти маркетингових проектів'
    /// </summary>
    public static class MProjRespondents_TS_   
    {
        public const int __tGid = 15828;
        public const string __tName = "MProjRespondents";
        static mdTable m_mdTable;
        static MProjRespondents_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _Respondent_fGid = 33067;
       
        public static mdTableField Respondent
        {
            get { return __MD.Fields[_Respondent_fGid];}
        }
        public const string _Respondent_fName = "Respondent";        public const int _Project_fGid = 33068;
       
        public static mdTableField Project
        {
            get { return __MD.Fields[_Project_fGid];}
        }
        public const string _Project_fName = "Project";        public const int _Invitation_fGid = 33069;
       
        public static mdTableField Invitation
        {
            get { return __MD.Fields[_Invitation_fGid];}
        }
        public const string _Invitation_fName = "Invitation";       
        public static mdTableField Respondent_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Respondent_fGid]);}
        }
        public const string _Respondent_view__fName = "Respondent_view_";       
        public static mdTableField Project_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Project_fGid]);}
        }
        public const string _Project_view__fName = "Project_view_";       
        public static mdTableField Invitation_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Invitation_fGid]);}
        }
        public const string _Invitation_view__fName = "Invitation_view_";       
        public static mdTableField Respondent_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Respondent_fGid]);}
        }
        public const string _Respondent_status__fName = "Respondent_status_";       
        public static mdTableField Project_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Project_fGid]);}
        }
        public const string _Project_status__fName = "Project_status_";       
        public static mdTableField Invitation_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Invitation_fGid]);}
        }
        public const string _Invitation_status__fName = "Invitation_status_";
    }
    /// <summary>
    /// Схема таблиці 'Елементи груп альтернатив'
    /// </summary>
    public static class QnA_GroupsItems_TS_   
    {
        public const int __tGid = 15776;
        public const string __tName = "QnA_GroupsItems";
        static mdTable m_mdTable;
        static QnA_GroupsItems_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _AlternativesGroup_fGid = 31674;
       
        public static mdTableField AlternativesGroup
        {
            get { return __MD.Fields[_AlternativesGroup_fGid];}
        }
        public const string _AlternativesGroup_fName = "AlternativesGroup";        public const int _Item_fGid = 31675;
       
        public static mdTableField Item
        {
            get { return __MD.Fields[_Item_fGid];}
        }
        public const string _Item_fName = "Item";        public const int _ordinal_fGid = 31676;
       
        public static mdTableField ordinal
        {
            get { return __MD.Fields[_ordinal_fGid];}
        }
        public const string _ordinal_fName = "ordinal";        public const int _IntValue_fGid = 31756;
       
        public static mdTableField IntValue
        {
            get { return __MD.Fields[_IntValue_fGid];}
        }
        public const string _IntValue_fName = "IntValue";        public const int _Item_IntValue_fGid = 31922;
       
        public static mdTableField Item_IntValue
        {
            get { return __MD.Fields[_Item_IntValue_fGid];}
        }
        public const string _Item_IntValue_fName = "Item_IntValue";       
        public static mdTableField Item_name
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Item_fGid]);}
        }
        public const string _Item_name_fName = "Item_name";        public const int _ImageFile_fGid = 31924;
       
        public static mdTableField ImageFile
        {
            get { return __MD.Fields[_ImageFile_fGid];}
        }
        public const string _ImageFile_fName = "ImageFile";        public const int _ImageFileName_fGid = 31926;
       
        public static mdTableField ImageFileName
        {
            get { return __MD.Fields[_ImageFileName_fGid];}
        }
        public const string _ImageFileName_fName = "ImageFileName";        public const int _asid_fGid = 31962;
       
        public static mdTableField asid
        {
            get { return __MD.Fields[_asid_fGid];}
        }
        public const string _asid_fName = "asid";        public const int _comments_fGid = 33677;
       
        public static mdTableField comments
        {
            get { return __MD.Fields[_comments_fGid];}
        }
        public const string _comments_fName = "comments";       
        public static mdTableField AlternativesGroup_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_AlternativesGroup_fGid]);}
        }
        public const string _AlternativesGroup_view__fName = "AlternativesGroup_view_";       
        public static mdTableField ImageFile_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_ImageFile_fGid]);}
        }
        public const string _ImageFile_view__fName = "ImageFile_view_";       
        public static mdTableField AlternativesGroup_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_AlternativesGroup_fGid]);}
        }
        public const string _AlternativesGroup_status__fName = "AlternativesGroup_status_";       
        public static mdTableField Item_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Item_fGid]);}
        }
        public const string _Item_status__fName = "Item_status_";       
        public static mdTableField ImageFile_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_ImageFile_fGid]);}
        }
        public const string _ImageFile_status__fName = "ImageFile_status_";
    }
    /// <summary>
    /// Схема таблиці 'Групи альтернатив в опитуваннях'
    /// </summary>
    public static class QnAlternativesGroups_TS_   
    {
        public const int __tGid = 15774;
        public const string __tName = "QnAlternativesGroups";
        static mdTable m_mdTable;
        static QnAlternativesGroups_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _gid_fGid = 31661;
       
        public static mdTableField gid
        {
            get { return __MD.Fields[_gid_fGid];}
        }
        public const string _gid_fName = "gid";        public const int _name_fGid = 31662;
       
        public static mdTableField name
        {
            get { return __MD.Fields[_name_fGid];}
        }
        public const string _name_fName = "name";        public const int _object_status_fGid = 31663;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _comments_fGid = 31664;
       
        public static mdTableField comments
        {
            get { return __MD.Fields[_comments_fGid];}
        }
        public const string _comments_fName = "comments";        public const int _version_no_fGid = 31665;
       
        public static mdTableField version_no
        {
            get { return __MD.Fields[_version_no_fGid];}
        }
        public const string _version_no_fName = "version_no";        public const int _AlternativesKind_fGid = 31666;
       
        public static mdTableField AlternativesKind
        {
            get { return __MD.Fields[_AlternativesKind_fGid];}
        }
        public const string _AlternativesKind_fName = "AlternativesKind";        public const int _parent_group_fGid = 31716;
       
        public static mdTableField parent_group
        {
            get { return __MD.Fields[_parent_group_fGid];}
        }
        public const string _parent_group_fName = "parent_group";        public const int _top10ItemsStr_fGid = 31742;
       
        public static mdTableField top10ItemsStr
        {
            get { return __MD.Fields[_top10ItemsStr_fGid];}
        }
        public const string _top10ItemsStr_fName = "top10ItemsStr";        public const int _Is4ServiceUse_fGid = 31894;
       
        public static mdTableField Is4ServiceUse
        {
            get { return __MD.Fields[_Is4ServiceUse_fGid];}
        }
        public const string _Is4ServiceUse_fName = "Is4ServiceUse";        public const int _asid_fGid = 31958;
       
        public static mdTableField asid
        {
            get { return __MD.Fields[_asid_fGid];}
        }
        public const string _asid_fName = "asid";        public const int _UseCommentsInUI_fGid = 33646;
       
        public static mdTableField UseCommentsInUI
        {
            get { return __MD.Fields[_UseCommentsInUI_fGid];}
        }
        public const string _UseCommentsInUI_fName = "UseCommentsInUI";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";       
        public static mdTableField AlternativesKind_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_AlternativesKind_fGid]);}
        }
        public const string _AlternativesKind_view__fName = "AlternativesKind_view_";       
        public static mdTableField parent_group_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_parent_group_fGid]);}
        }
        public const string _parent_group_view__fName = "parent_group_view_";       
        public static mdTableField AlternativesKind_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_AlternativesKind_fGid]);}
        }
        public const string _AlternativesKind_status__fName = "AlternativesKind_status_";       
        public static mdTableField parent_group_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_parent_group_fGid]);}
        }
        public const string _parent_group_status__fName = "parent_group_status_";
    }
    /// <summary>
    /// Схема таблиці 'Елементи альтернатив для опитувань'
    /// </summary>
    public static class QnAlternativesItems_TS_   
    {
        public const int __tGid = 15775;
        public const string __tName = "QnAlternativesItems";
        static mdTable m_mdTable;
        static QnAlternativesItems_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _gid_fGid = 31667;
       
        public static mdTableField gid
        {
            get { return __MD.Fields[_gid_fGid];}
        }
        public const string _gid_fName = "gid";        public const int _name_fGid = 31668;
       
        public static mdTableField name
        {
            get { return __MD.Fields[_name_fGid];}
        }
        public const string _name_fName = "name";        public const int _object_status_fGid = 31669;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _comments_fGid = 31670;
       
        public static mdTableField comments
        {
            get { return __MD.Fields[_comments_fGid];}
        }
        public const string _comments_fName = "comments";        public const int _version_no_fGid = 31671;
       
        public static mdTableField version_no
        {
            get { return __MD.Fields[_version_no_fGid];}
        }
        public const string _version_no_fName = "version_no";        public const int _AlternativeKind_fGid = 31672;
       
        public static mdTableField AlternativeKind
        {
            get { return __MD.Fields[_AlternativeKind_fGid];}
        }
        public const string _AlternativeKind_fName = "AlternativeKind";        public const int _ordinal_fGid = 31673;
       
        public static mdTableField ordinal
        {
            get { return __MD.Fields[_ordinal_fGid];}
        }
        public const string _ordinal_fName = "ordinal";        public const int _IncludedToCurGroup_fGid = 31701;
       
        public static mdTableField IncludedToCurGroup
        {
            get { return __MD.Fields[_IncludedToCurGroup_fGid];}
        }
        public const string _IncludedToCurGroup_fName = "IncludedToCurGroup";        public const int _ImageFile_fGid = 31708;
       
        public static mdTableField ImageFile
        {
            get { return __MD.Fields[_ImageFile_fGid];}
        }
        public const string _ImageFile_fName = "ImageFile";        public const int _IntValue_fGid = 31743;
       
        public static mdTableField IntValue
        {
            get { return __MD.Fields[_IntValue_fGid];}
        }
        public const string _IntValue_fName = "IntValue";        public const int _SmallerImage_fGid = 31755;
       
        public static mdTableField SmallerImage
        {
            get { return __MD.Fields[_SmallerImage_fGid];}
        }
        public const string _SmallerImage_fName = "SmallerImage";        public const int _asid_fGid = 31960;
       
        public static mdTableField asid
        {
            get { return __MD.Fields[_asid_fGid];}
        }
        public const string _asid_fName = "asid";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";       
        public static mdTableField AlternativeKind_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_AlternativeKind_fGid]);}
        }
        public const string _AlternativeKind_view__fName = "AlternativeKind_view_";       
        public static mdTableField ImageFile_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_ImageFile_fGid]);}
        }
        public const string _ImageFile_view__fName = "ImageFile_view_";       
        public static mdTableField SmallerImage_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_SmallerImage_fGid]);}
        }
        public const string _SmallerImage_view__fName = "SmallerImage_view_";       
        public static mdTableField AlternativeKind_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_AlternativeKind_fGid]);}
        }
        public const string _AlternativeKind_status__fName = "AlternativeKind_status_";       
        public static mdTableField ImageFile_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_ImageFile_fGid]);}
        }
        public const string _ImageFile_status__fName = "ImageFile_status_";       
        public static mdTableField SmallerImage_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_SmallerImage_fGid]);}
        }
        public const string _SmallerImage_status__fName = "SmallerImage_status_";
    }
    /// <summary>
    /// Схема таблиці 'Види альтернатив для опитувань'
    /// </summary>
    public static class QnAlternativesKinds_TS_   
    {
        public const int __tGid = 15773;
        public const string __tName = "QnAlternativesKinds";
        static mdTable m_mdTable;
        static QnAlternativesKinds_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _gid_fGid = 31655;
       
        public static mdTableField gid
        {
            get { return __MD.Fields[_gid_fGid];}
        }
        public const string _gid_fName = "gid";        public const int _name_fGid = 31656;
       
        public static mdTableField name
        {
            get { return __MD.Fields[_name_fGid];}
        }
        public const string _name_fName = "name";        public const int _object_status_fGid = 31657;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _comments_fGid = 31658;
       
        public static mdTableField comments
        {
            get { return __MD.Fields[_comments_fGid];}
        }
        public const string _comments_fName = "comments";        public const int _version_no_fGid = 31659;
       
        public static mdTableField version_no
        {
            get { return __MD.Fields[_version_no_fGid];}
        }
        public const string _version_no_fName = "version_no";        public const int _parent_kind_fGid = 31660;
       
        public static mdTableField parent_kind
        {
            get { return __MD.Fields[_parent_kind_fGid];}
        }
        public const string _parent_kind_fName = "parent_kind";        public const int _MainGroup_fGid = 31699;
       
        public static mdTableField MainGroup
        {
            get { return __MD.Fields[_MainGroup_fGid];}
        }
        public const string _MainGroup_fName = "MainGroup";        public const int _asid_fGid = 31959;
       
        public static mdTableField asid
        {
            get { return __MD.Fields[_asid_fGid];}
        }
        public const string _asid_fName = "asid";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";       
        public static mdTableField parent_kind_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_parent_kind_fGid]);}
        }
        public const string _parent_kind_view__fName = "parent_kind_view_";       
        public static mdTableField MainGroup_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_MainGroup_fGid]);}
        }
        public const string _MainGroup_view__fName = "MainGroup_view_";       
        public static mdTableField parent_kind_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_parent_kind_fGid]);}
        }
        public const string _parent_kind_status__fName = "parent_kind_status_";       
        public static mdTableField MainGroup_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_MainGroup_fGid]);}
        }
        public const string _MainGroup_status__fName = "MainGroup_status_";
    }
    /// <summary>
    /// Схема таблиці 'Додаткові параметри відповідей'
    /// </summary>
    public static class QnAnswerParams_tp_TS_   
    {
        public const int __tGid = 15796;
        public const string __tName = "QnAnswerParams_tp";
        static mdTable m_mdTable;
        static QnAnswerParams_tp_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _Operation_fGid = 31998;
       
        public static mdTableField Operation
        {
            get { return __MD.Fields[_Operation_fGid];}
        }
        public const string _Operation_fName = "Operation";        public const int _AnswerKey_fGid = 31999;
       
        public static mdTableField AnswerKey
        {
            get { return __MD.Fields[_AnswerKey_fGid];}
        }
        public const string _AnswerKey_fName = "AnswerKey";        public const int _QuestionItem_fGid = 32000;
       
        public static mdTableField QuestionItem
        {
            get { return __MD.Fields[_QuestionItem_fGid];}
        }
        public const string _QuestionItem_fName = "QuestionItem";        public const int _IntAnswer_fGid = 32001;
       
        public static mdTableField IntAnswer
        {
            get { return __MD.Fields[_IntAnswer_fGid];}
        }
        public const string _IntAnswer_fName = "IntAnswer";        public const int _TextAnswer_fGid = 32002;
       
        public static mdTableField TextAnswer
        {
            get { return __MD.Fields[_TextAnswer_fGid];}
        }
        public const string _TextAnswer_fName = "TextAnswer";        public const int _AlternativeItem_fGid = 32003;
       
        public static mdTableField AlternativeItem
        {
            get { return __MD.Fields[_AlternativeItem_fGid];}
        }
        public const string _AlternativeItem_fName = "AlternativeItem";       
        public static mdTableField Operation_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Operation_fGid]);}
        }
        public const string _Operation_view__fName = "Operation_view_";       
        public static mdTableField QuestionItem_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_QuestionItem_fGid]);}
        }
        public const string _QuestionItem_view__fName = "QuestionItem_view_";       
        public static mdTableField AlternativeItem_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_AlternativeItem_fGid]);}
        }
        public const string _AlternativeItem_view__fName = "AlternativeItem_view_";       
        public static mdTableField Operation_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Operation_fGid]);}
        }
        public const string _Operation_status__fName = "Operation_status_";       
        public static mdTableField QuestionItem_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_QuestionItem_fGid]);}
        }
        public const string _QuestionItem_status__fName = "QuestionItem_status_";       
        public static mdTableField AlternativeItem_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_AlternativeItem_fGid]);}
        }
        public const string _AlternativeItem_status__fName = "AlternativeItem_status_";
    }
    /// <summary>
    /// Схема таблиці 'Відповіді за опитуваннями (т.ч.)'
    /// </summary>
    public static class QnAnswers_tp_TS_   
    {
        public const int __tGid = 15779;
        public const string __tName = "QnAnswers_tp";
        static mdTable m_mdTable;
        static QnAnswers_tp_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _Operation_fGid = 31718;
       
        public static mdTableField Operation
        {
            get { return __MD.Fields[_Operation_fGid];}
        }
        public const string _Operation_fName = "Operation";        public const int _QuestionItem_fGid = 31719;
       
        public static mdTableField QuestionItem
        {
            get { return __MD.Fields[_QuestionItem_fGid];}
        }
        public const string _QuestionItem_fName = "QuestionItem";        public const int _TextAnswer_fGid = 31720;
       
        public static mdTableField TextAnswer
        {
            get { return __MD.Fields[_TextAnswer_fGid];}
        }
        public const string _TextAnswer_fName = "TextAnswer";        public const int _SelectedAlternative_fGid = 31721;
       
        public static mdTableField SelectedAlternative
        {
            get { return __MD.Fields[_SelectedAlternative_fGid];}
        }
        public const string _SelectedAlternative_fName = "SelectedAlternative";        public const int _ordinal_fGid = 31722;
       
        public static mdTableField ordinal
        {
            get { return __MD.Fields[_ordinal_fGid];}
        }
        public const string _ordinal_fName = "ordinal";        public const int _AnswerDateTime_fGid = 31723;
       
        public static mdTableField AnswerDateTime
        {
            get { return __MD.Fields[_AnswerDateTime_fGid];}
        }
        public const string _AnswerDateTime_fName = "AnswerDateTime";        public const int _IntAnswer_fGid = 31724;
       
        public static mdTableField IntAnswer
        {
            get { return __MD.Fields[_IntAnswer_fGid];}
        }
        public const string _IntAnswer_fName = "IntAnswer";        public const int _QuestionGroup_fGid = 31880;
       
        public static mdTableField QuestionGroup
        {
            get { return __MD.Fields[_QuestionGroup_fGid];}
        }
        public const string _QuestionGroup_fName = "QuestionGroup";        public const int _QuestionDataType_fGid = 31904;
       
        public static mdTableField QuestionDataType
        {
            get { return __MD.Fields[_QuestionDataType_fGid];}
        }
        public const string _QuestionDataType_fName = "QuestionDataType";        public const int _AnswerKey_fGid = 31997;
       
        public static mdTableField AnswerKey
        {
            get { return __MD.Fields[_AnswerKey_fGid];}
        }
        public const string _AnswerKey_fName = "AnswerKey";        public const int _object_status_fGid = 32183;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _QnI_ordinal_fGid = 32280;
       
        public static mdTableField QnI_ordinal
        {
            get { return __MD.Fields[_QnI_ordinal_fGid];}
        }
        public const string _QnI_ordinal_fName = "QnI_ordinal";        public const int _MultiAnswersStr_fGid = 32331;
       
        public static mdTableField MultiAnswersStr
        {
            get { return __MD.Fields[_MultiAnswersStr_fGid];}
        }
        public const string _MultiAnswersStr_fName = "MultiAnswersStr";       
        public static mdTableField Operation_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Operation_fGid]);}
        }
        public const string _Operation_view__fName = "Operation_view_";       
        public static mdTableField QuestionItem_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_QuestionItem_fGid]);}
        }
        public const string _QuestionItem_view__fName = "QuestionItem_view_";       
        public static mdTableField SelectedAlternative_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_SelectedAlternative_fGid]);}
        }
        public const string _SelectedAlternative_view__fName = "SelectedAlternative_view_";       
        public static mdTableField QuestionGroup_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_QuestionGroup_fGid]);}
        }
        public const string _QuestionGroup_view__fName = "QuestionGroup_view_";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";       
        public static mdTableField QuestionItem_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_QuestionItem_fGid]);}
        }
        public const string _QuestionItem_status__fName = "QuestionItem_status_";       
        public static mdTableField SelectedAlternative_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_SelectedAlternative_fGid]);}
        }
        public const string _SelectedAlternative_status__fName = "SelectedAlternative_status_";       
        public static mdTableField QuestionGroup_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_QuestionGroup_fGid]);}
        }
        public const string _QuestionGroup_status__fName = "QuestionGroup_status_";
    }
    /// <summary>
    /// Схема таблиці 'Статистика відповідей в опитуваннях'
    /// </summary>
    public static class QnAnswers_V_TS_   
    {
        public const int __tGid = 15802;
        public const string __tName = "QnAnswers_V";
        static mdTable m_mdTable;
        static QnAnswers_V_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _QuestionItem_fGid = 32051;
       
        public static mdTableField QuestionItem
        {
            get { return __MD.Fields[_QuestionItem_fGid];}
        }
        public const string _QuestionItem_fName = "QuestionItem";        public const int _AnswerDateTime_fGid = 32243;
       
        public static mdTableField AnswerDateTime
        {
            get { return __MD.Fields[_AnswerDateTime_fGid];}
        }
        public const string _AnswerDateTime_fName = "AnswerDateTime";        public const int _QuestionGroup_fGid = 32244;
       
        public static mdTableField QuestionGroup
        {
            get { return __MD.Fields[_QuestionGroup_fGid];}
        }
        public const string _QuestionGroup_fName = "QuestionGroup";        public const int _SelectedAlternative_fGid = 32271;
       
        public static mdTableField SelectedAlternative
        {
            get { return __MD.Fields[_SelectedAlternative_fGid];}
        }
        public const string _SelectedAlternative_fName = "SelectedAlternative";        public const int _OpCount_fGid = 32272;
       
        public static mdTableField OpCount
        {
            get { return __MD.Fields[_OpCount_fGid];}
        }
        public const string _OpCount_fName = "OpCount";        public const int _Operation_fGid = 32273;
       
        public static mdTableField Operation
        {
            get { return __MD.Fields[_Operation_fGid];}
        }
        public const string _Operation_fName = "Operation";        public const int _AnswerDate_fGid = 32274;
       
        public static mdTableField AnswerDate
        {
            get { return __MD.Fields[_AnswerDate_fGid];}
        }
        public const string _AnswerDate_fName = "AnswerDate";        public const int _WeekDay_fGid = 32275;
       
        public static mdTableField WeekDay
        {
            get { return __MD.Fields[_WeekDay_fGid];}
        }
        public const string _WeekDay_fName = "WeekDay";        public const int _dateop_fGid = 32276;
       
        public static mdTableField dateop
        {
            get { return __MD.Fields[_dateop_fGid];}
        }
        public const string _dateop_fName = "dateop";        public const int _WAuthorsCount_fGid = 32277;
       
        public static mdTableField WAuthorsCount
        {
            get { return __MD.Fields[_WAuthorsCount_fGid];}
        }
        public const string _WAuthorsCount_fName = "WAuthorsCount";        public const int _IntAnswer_fGid = 32278;
       
        public static mdTableField IntAnswer
        {
            get { return __MD.Fields[_IntAnswer_fGid];}
        }
        public const string _IntAnswer_fName = "IntAnswer";        public const int _TextAnswer_fGid = 32279;
       
        public static mdTableField TextAnswer
        {
            get { return __MD.Fields[_TextAnswer_fGid];}
        }
        public const string _TextAnswer_fName = "TextAnswer";        public const int _QnI_ordinal_fGid = 32281;
       
        public static mdTableField QnI_ordinal
        {
            get { return __MD.Fields[_QnI_ordinal_fGid];}
        }
        public const string _QnI_ordinal_fName = "QnI_ordinal";        public const int _reg_date_fGid = 32282;
       
        public static mdTableField reg_date
        {
            get { return __MD.Fields[_reg_date_fGid];}
        }
        public const string _reg_date_fName = "reg_date";        public const int _object_status_fGid = 32283;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _ip_start_fGid = 32284;
       
        public static mdTableField ip_start
        {
            get { return __MD.Fields[_ip_start_fGid];}
        }
        public const string _ip_start_fName = "ip_start";        public const int _QnTemplate_fGid = 32285;
       
        public static mdTableField QnTemplate
        {
            get { return __MD.Fields[_QnTemplate_fGid];}
        }
        public const string _QnTemplate_fName = "QnTemplate";        public const int _MultiAnswersStr_fGid = 32332;
       
        public static mdTableField MultiAnswersStr
        {
            get { return __MD.Fields[_MultiAnswersStr_fGid];}
        }
        public const string _MultiAnswersStr_fName = "MultiAnswersStr";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";       
        public static mdTableField QnTemplate_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_QnTemplate_fGid]);}
        }
        public const string _QnTemplate_view__fName = "QnTemplate_view_";       
        public static mdTableField QuestionItem_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_QuestionItem_fGid]);}
        }
        public const string _QuestionItem_view__fName = "QuestionItem_view_";       
        public static mdTableField QuestionGroup_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_QuestionGroup_fGid]);}
        }
        public const string _QuestionGroup_view__fName = "QuestionGroup_view_";       
        public static mdTableField SelectedAlternative_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_SelectedAlternative_fGid]);}
        }
        public const string _SelectedAlternative_view__fName = "SelectedAlternative_view_";       
        public static mdTableField Operation_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_OpCount_fGid]);}
        }
        public const string _Operation_view__fName = "Operation_view_";
    }
    /// <summary>
    /// Схема таблиці 'Вкладені файли до відповідей анкети'
    /// </summary>
    public static class QnAttachedFiles_tp_TS_   
    {
        public const int __tGid = 15819;
        public const string __tName = "QnAttachedFiles_tp";
        static mdTable m_mdTable;
        static QnAttachedFiles_tp_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _Operation_fGid = 32569;
       
        public static mdTableField Operation
        {
            get { return __MD.Fields[_Operation_fGid];}
        }
        public const string _Operation_fName = "Operation";        public const int _ordinal_fGid = 32570;
       
        public static mdTableField ordinal
        {
            get { return __MD.Fields[_ordinal_fGid];}
        }
        public const string _ordinal_fName = "ordinal";        public const int _QuestionItem_fGid = 32571;
       
        public static mdTableField QuestionItem
        {
            get { return __MD.Fields[_QuestionItem_fGid];}
        }
        public const string _QuestionItem_fName = "QuestionItem";        public const int _AttachedFile_fGid = 32572;
       
        public static mdTableField AttachedFile
        {
            get { return __MD.Fields[_AttachedFile_fGid];}
        }
        public const string _AttachedFile_fName = "AttachedFile";        public const int _AnswerKey_fGid = 32573;
       
        public static mdTableField AnswerKey
        {
            get { return __MD.Fields[_AnswerKey_fGid];}
        }
        public const string _AnswerKey_fName = "AnswerKey";       
        public static mdTableField Operation_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Operation_fGid]);}
        }
        public const string _Operation_view__fName = "Operation_view_";       
        public static mdTableField QuestionItem_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_QuestionItem_fGid]);}
        }
        public const string _QuestionItem_view__fName = "QuestionItem_view_";       
        public static mdTableField AttachedFile_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_AttachedFile_fGid]);}
        }
        public const string _AttachedFile_view__fName = "AttachedFile_view_";       
        public static mdTableField Operation_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Operation_fGid]);}
        }
        public const string _Operation_status__fName = "Operation_status_";       
        public static mdTableField QuestionItem_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_QuestionItem_fGid]);}
        }
        public const string _QuestionItem_status__fName = "QuestionItem_status_";       
        public static mdTableField AttachedFile_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_AttachedFile_fGid]);}
        }
        public const string _AttachedFile_status__fName = "AttachedFile_status_";
    }
    /// <summary>
    /// Схема таблиці 'Відповіді дати і часу за опитуваннями (т.ч.)'
    /// </summary>
    public static class QnDateTimeAnswers_tp_TS_   
    {
        public const int __tGid = 15782;
        public const string __tName = "QnDateTimeAnswers_tp";
        static mdTable m_mdTable;
        static QnDateTimeAnswers_tp_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _Operation_fGid = 31735;
       
        public static mdTableField Operation
        {
            get { return __MD.Fields[_Operation_fGid];}
        }
        public const string _Operation_fName = "Operation";        public const int _ordinal_fGid = 31736;
       
        public static mdTableField ordinal
        {
            get { return __MD.Fields[_ordinal_fGid];}
        }
        public const string _ordinal_fName = "ordinal";        public const int _QuestionItem_fGid = 31737;
       
        public static mdTableField QuestionItem
        {
            get { return __MD.Fields[_QuestionItem_fGid];}
        }
        public const string _QuestionItem_fName = "QuestionItem";        public const int _DatetTimeAnswer_fGid = 31738;
       
        public static mdTableField DatetTimeAnswer
        {
            get { return __MD.Fields[_DatetTimeAnswer_fGid];}
        }
        public const string _DatetTimeAnswer_fName = "DatetTimeAnswer";        public const int _AnswerKey_fGid = 32005;
       
        public static mdTableField AnswerKey
        {
            get { return __MD.Fields[_AnswerKey_fGid];}
        }
        public const string _AnswerKey_fName = "AnswerKey";       
        public static mdTableField Operation_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Operation_fGid]);}
        }
        public const string _Operation_view__fName = "Operation_view_";       
        public static mdTableField QuestionItem_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_QuestionItem_fGid]);}
        }
        public const string _QuestionItem_view__fName = "QuestionItem_view_";       
        public static mdTableField Operation_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Operation_fGid]);}
        }
        public const string _Operation_status__fName = "Operation_status_";       
        public static mdTableField QuestionItem_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_QuestionItem_fGid]);}
        }
        public const string _QuestionItem_status__fName = "QuestionItem_status_";
    }
    /// <summary>
    /// Схема таблиці 'Багатозначні відкриті відповіді за опитуваннями (т.ч.)'
    /// </summary>
    public static class QnMOAnswers_tp_TS_   
    {
        public const int __tGid = 15781;
        public const string __tName = "QnMOAnswers_tp";
        static mdTable m_mdTable;
        static QnMOAnswers_tp_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _Operation_fGid = 31729;
       
        public static mdTableField Operation
        {
            get { return __MD.Fields[_Operation_fGid];}
        }
        public const string _Operation_fName = "Operation";        public const int _QuestionItem_fGid = 31730;
       
        public static mdTableField QuestionItem
        {
            get { return __MD.Fields[_QuestionItem_fGid];}
        }
        public const string _QuestionItem_fName = "QuestionItem";        public const int _ordinal_fGid = 31731;
       
        public static mdTableField ordinal
        {
            get { return __MD.Fields[_ordinal_fGid];}
        }
        public const string _ordinal_fName = "ordinal";        public const int _TextAnswer_fGid = 31732;
       
        public static mdTableField TextAnswer
        {
            get { return __MD.Fields[_TextAnswer_fGid];}
        }
        public const string _TextAnswer_fName = "TextAnswer";        public const int _IntAnswer_fGid = 31734;
       
        public static mdTableField IntAnswer
        {
            get { return __MD.Fields[_IntAnswer_fGid];}
        }
        public const string _IntAnswer_fName = "IntAnswer";        public const int _RowSubKey_fGid = 31757;
       
        public static mdTableField RowSubKey
        {
            get { return __MD.Fields[_RowSubKey_fGid];}
        }
        public const string _RowSubKey_fName = "RowSubKey";        public const int _AnswerKey_fGid = 32006;
       
        public static mdTableField AnswerKey
        {
            get { return __MD.Fields[_AnswerKey_fGid];}
        }
        public const string _AnswerKey_fName = "AnswerKey";       
        public static mdTableField Operation_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Operation_fGid]);}
        }
        public const string _Operation_view__fName = "Operation_view_";       
        public static mdTableField QuestionItem_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_QuestionItem_fGid]);}
        }
        public const string _QuestionItem_view__fName = "QuestionItem_view_";       
        public static mdTableField Operation_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Operation_fGid]);}
        }
        public const string _Operation_status__fName = "Operation_status_";       
        public static mdTableField QuestionItem_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_QuestionItem_fGid]);}
        }
        public const string _QuestionItem_status__fName = "QuestionItem_status_";
    }
    /// <summary>
    /// Схема таблиці 'Багатозначні відповіді за опитуваннями (т.ч.)'
    /// </summary>
    public static class QnMultiAnswers_tp_TS_   
    {
        public const int __tGid = 15780;
        public const string __tName = "QnMultiAnswers_tp";
        static mdTable m_mdTable;
        static QnMultiAnswers_tp_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _Operation_fGid = 31725;
       
        public static mdTableField Operation
        {
            get { return __MD.Fields[_Operation_fGid];}
        }
        public const string _Operation_fName = "Operation";        public const int _ordinal_fGid = 31726;
       
        public static mdTableField ordinal
        {
            get { return __MD.Fields[_ordinal_fGid];}
        }
        public const string _ordinal_fName = "ordinal";        public const int _SelectedAlternative_fGid = 31727;
       
        public static mdTableField SelectedAlternative
        {
            get { return __MD.Fields[_SelectedAlternative_fGid];}
        }
        public const string _SelectedAlternative_fName = "SelectedAlternative";        public const int _IntAnswer_fGid = 31728;
       
        public static mdTableField IntAnswer
        {
            get { return __MD.Fields[_IntAnswer_fGid];}
        }
        public const string _IntAnswer_fName = "IntAnswer";        public const int _QuestionItem_fGid = 31733;
       
        public static mdTableField QuestionItem
        {
            get { return __MD.Fields[_QuestionItem_fGid];}
        }
        public const string _QuestionItem_fName = "QuestionItem";        public const int _AnswerKey_fGid = 32004;
       
        public static mdTableField AnswerKey
        {
            get { return __MD.Fields[_AnswerKey_fGid];}
        }
        public const string _AnswerKey_fName = "AnswerKey";        public const int _RowSubKey_fGid = 32065;
       
        public static mdTableField RowSubKey
        {
            get { return __MD.Fields[_RowSubKey_fGid];}
        }
        public const string _RowSubKey_fName = "RowSubKey";        public const int _TextAnswer_fGid = 32066;
       
        public static mdTableField TextAnswer
        {
            get { return __MD.Fields[_TextAnswer_fGid];}
        }
        public const string _TextAnswer_fName = "TextAnswer";        public const int _SelectedAlternative2_fGid = 32067;
       
        public static mdTableField SelectedAlternative2
        {
            get { return __MD.Fields[_SelectedAlternative2_fGid];}
        }
        public const string _SelectedAlternative2_fName = "SelectedAlternative2";       
        public static mdTableField Operation_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Operation_fGid]);}
        }
        public const string _Operation_view__fName = "Operation_view_";       
        public static mdTableField SelectedAlternative_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_SelectedAlternative_fGid]);}
        }
        public const string _SelectedAlternative_view__fName = "SelectedAlternative_view_";       
        public static mdTableField QuestionItem_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_QuestionItem_fGid]);}
        }
        public const string _QuestionItem_view__fName = "QuestionItem_view_";       
        public static mdTableField SelectedAlternative2_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_SelectedAlternative2_fGid]);}
        }
        public const string _SelectedAlternative2_view__fName = "SelectedAlternative2_view_";       
        public static mdTableField Operation_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Operation_fGid]);}
        }
        public const string _Operation_status__fName = "Operation_status_";       
        public static mdTableField SelectedAlternative_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_SelectedAlternative_fGid]);}
        }
        public const string _SelectedAlternative_status__fName = "SelectedAlternative_status_";       
        public static mdTableField QuestionItem_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_QuestionItem_fGid]);}
        }
        public const string _QuestionItem_status__fName = "QuestionItem_status_";       
        public static mdTableField SelectedAlternative2_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_SelectedAlternative2_fGid]);}
        }
        public const string _SelectedAlternative2_status__fName = "SelectedAlternative2_status_";
    }
    /// <summary>
    /// Схема таблиці 'Статистика багатозначних відповідей в опитуваннях'
    /// </summary>
    public static class QnMultiAnswers_V_TS_   
    {
        public const int __tGid = 15808;
        public const string __tName = "QnMultiAnswers_V";
        static mdTable m_mdTable;
        static QnMultiAnswers_V_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _Operation_fGid = 32286;
       
        public static mdTableField Operation
        {
            get { return __MD.Fields[_Operation_fGid];}
        }
        public const string _Operation_fName = "Operation";        public const int _QnTemplate_fGid = 32287;
       
        public static mdTableField QnTemplate
        {
            get { return __MD.Fields[_QnTemplate_fGid];}
        }
        public const string _QnTemplate_fName = "QnTemplate";        public const int _object_status_fGid = 32288;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _reg_date_fGid = 32289;
       
        public static mdTableField reg_date
        {
            get { return __MD.Fields[_reg_date_fGid];}
        }
        public const string _reg_date_fName = "reg_date";        public const int _QuestionItem_fGid = 32290;
       
        public static mdTableField QuestionItem
        {
            get { return __MD.Fields[_QuestionItem_fGid];}
        }
        public const string _QuestionItem_fName = "QuestionItem";        public const int _IntAnswer_fGid = 32291;
       
        public static mdTableField IntAnswer
        {
            get { return __MD.Fields[_IntAnswer_fGid];}
        }
        public const string _IntAnswer_fName = "IntAnswer";        public const int _QuestionGroup_fGid = 32292;
       
        public static mdTableField QuestionGroup
        {
            get { return __MD.Fields[_QuestionGroup_fGid];}
        }
        public const string _QuestionGroup_fName = "QuestionGroup";        public const int _AnswerDateTime_fGid = 32293;
       
        public static mdTableField AnswerDateTime
        {
            get { return __MD.Fields[_AnswerDateTime_fGid];}
        }
        public const string _AnswerDateTime_fName = "AnswerDateTime";        public const int _AnswerKey_fGid = 32294;
       
        public static mdTableField AnswerKey
        {
            get { return __MD.Fields[_AnswerKey_fGid];}
        }
        public const string _AnswerKey_fName = "AnswerKey";        public const int _ip_start_fGid = 32295;
       
        public static mdTableField ip_start
        {
            get { return __MD.Fields[_ip_start_fGid];}
        }
        public const string _ip_start_fName = "ip_start";        public const int _QnI_ordinal_fGid = 32296;
       
        public static mdTableField QnI_ordinal
        {
            get { return __MD.Fields[_QnI_ordinal_fGid];}
        }
        public const string _QnI_ordinal_fName = "QnI_ordinal";        public const int _SelectedAlternative_fGid = 32297;
       
        public static mdTableField SelectedAlternative
        {
            get { return __MD.Fields[_SelectedAlternative_fGid];}
        }
        public const string _SelectedAlternative_fName = "SelectedAlternative";        public const int _SelectedAlternative2_fGid = 32298;
       
        public static mdTableField SelectedAlternative2
        {
            get { return __MD.Fields[_SelectedAlternative2_fGid];}
        }
        public const string _SelectedAlternative2_fName = "SelectedAlternative2";        public const int _WAuthorsCount_fGid = 32299;
       
        public static mdTableField WAuthorsCount
        {
            get { return __MD.Fields[_WAuthorsCount_fGid];}
        }
        public const string _WAuthorsCount_fName = "WAuthorsCount";        public const int _OpCount_fGid = 32300;
       
        public static mdTableField OpCount
        {
            get { return __MD.Fields[_OpCount_fGid];}
        }
        public const string _OpCount_fName = "OpCount";        public const int _AnswerDate_fGid = 32301;
       
        public static mdTableField AnswerDate
        {
            get { return __MD.Fields[_AnswerDate_fGid];}
        }
        public const string _AnswerDate_fName = "AnswerDate";        public const int _WeekDay_fGid = 32302;
       
        public static mdTableField WeekDay
        {
            get { return __MD.Fields[_WeekDay_fGid];}
        }
        public const string _WeekDay_fName = "WeekDay";       
        public static mdTableField QnTemplate_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_QnTemplate_fGid]);}
        }
        public const string _QnTemplate_view__fName = "QnTemplate_view_";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";       
        public static mdTableField QuestionGroup_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_QuestionGroup_fGid]);}
        }
        public const string _QuestionGroup_view__fName = "QuestionGroup_view_";       
        public static mdTableField Operation_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Operation_fGid]);}
        }
        public const string _Operation_view__fName = "Operation_view_";       
        public static mdTableField QuestionItem_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_QuestionItem_fGid]);}
        }
        public const string _QuestionItem_view__fName = "QuestionItem_view_";       
        public static mdTableField SelectedAlternative_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_SelectedAlternative_fGid]);}
        }
        public const string _SelectedAlternative_view__fName = "SelectedAlternative_view_";       
        public static mdTableField SelectedAlternative2_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_SelectedAlternative2_fGid]);}
        }
        public const string _SelectedAlternative2_view__fName = "SelectedAlternative2_view_";
    }
    /// <summary>
    /// Схема таблиці 'Джерела альтернатив службових груп'
    /// </summary>
    public static class QnT_AltGroupSources_TS_   
    {
        public const int __tGid = 15792;
        public const string __tName = "QnT_AltGroupSources";
        static mdTable m_mdTable;
        static QnT_AltGroupSources_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _QuestionnaireTemplate_fGid = 31895;
       
        public static mdTableField QuestionnaireTemplate
        {
            get { return __MD.Fields[_QuestionnaireTemplate_fGid];}
        }
        public const string _QuestionnaireTemplate_fName = "QuestionnaireTemplate";        public const int _SrvAlternativesGroup_fGid = 31896;
       
        public static mdTableField SrvAlternativesGroup
        {
            get { return __MD.Fields[_SrvAlternativesGroup_fGid];}
        }
        public const string _SrvAlternativesGroup_fName = "SrvAlternativesGroup";        public const int _SrcQuestion_fGid = 31897;
       
        public static mdTableField SrcQuestion
        {
            get { return __MD.Fields[_SrcQuestion_fGid];}
        }
        public const string _SrcQuestion_fName = "SrcQuestion";       
        public static mdTableField QuestionnaireTemplate_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_QuestionnaireTemplate_fGid]);}
        }
        public const string _QuestionnaireTemplate_view__fName = "QuestionnaireTemplate_view_";       
        public static mdTableField SrvAlternativesGroup_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_SrvAlternativesGroup_fGid]);}
        }
        public const string _SrvAlternativesGroup_view__fName = "SrvAlternativesGroup_view_";       
        public static mdTableField SrcQuestion_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_SrcQuestion_fGid]);}
        }
        public const string _SrcQuestion_view__fName = "SrcQuestion_view_";       
        public static mdTableField QuestionnaireTemplate_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_QuestionnaireTemplate_fGid]);}
        }
        public const string _QuestionnaireTemplate_status__fName = "QuestionnaireTemplate_status_";       
        public static mdTableField SrvAlternativesGroup_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_SrvAlternativesGroup_fGid]);}
        }
        public const string _SrvAlternativesGroup_status__fName = "SrvAlternativesGroup_status_";       
        public static mdTableField SrcQuestion_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_SrcQuestion_fGid]);}
        }
        public const string _SrcQuestion_status__fName = "SrcQuestion_status_";
    }
    /// <summary>
    /// Схема таблиці 'Умови в шаблонах опитувань'
    /// </summary>
    public static class QnT_Conditions_TS_   
    {
        public const int __tGid = 15790;
        public const string __tName = "QnT_Conditions";
        static mdTable m_mdTable;
        static QnT_Conditions_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _QuestionnaireTemplate_fGid = 31883;
       
        public static mdTableField QuestionnaireTemplate
        {
            get { return __MD.Fields[_QuestionnaireTemplate_fGid];}
        }
        public const string _QuestionnaireTemplate_fName = "QuestionnaireTemplate";        public const int _ordinal_fGid = 31884;
       
        public static mdTableField ordinal
        {
            get { return __MD.Fields[_ordinal_fGid];}
        }
        public const string _ordinal_fName = "ordinal";        public const int _name_fGid = 31885;
       
        public static mdTableField name
        {
            get { return __MD.Fields[_name_fGid];}
        }
        public const string _name_fName = "name";        public const int _gid_fGid = 31886;
       
        public static mdTableField gid
        {
            get { return __MD.Fields[_gid_fGid];}
        }
        public const string _gid_fName = "gid";        public const int _expression_fGid = 31887;
       
        public static mdTableField expression
        {
            get { return __MD.Fields[_expression_fGid];}
        }
        public const string _expression_fName = "expression";        public const int _ParamsCount_fGid = 31888;
       
        public static mdTableField ParamsCount
        {
            get { return __MD.Fields[_ParamsCount_fGid];}
        }
        public const string _ParamsCount_fName = "ParamsCount";        public const int _comments_fGid = 31889;
       
        public static mdTableField comments
        {
            get { return __MD.Fields[_comments_fGid];}
        }
        public const string _comments_fName = "comments";        public const int _ResultType_fGid = 32179;
       
        public static mdTableField ResultType
        {
            get { return __MD.Fields[_ResultType_fGid];}
        }
        public const string _ResultType_fName = "ResultType";       
        public static mdTableField QuestionnaireTemplate_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_QuestionnaireTemplate_fGid]);}
        }
        public const string _QuestionnaireTemplate_view__fName = "QuestionnaireTemplate_view_";       
        public static mdTableField QuestionnaireTemplate_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_QuestionnaireTemplate_fGid]);}
        }
        public const string _QuestionnaireTemplate_status__fName = "QuestionnaireTemplate_status_";
    }
    /// <summary>
    /// Схема таблиці 'Параметри виразів у анкетах'
    /// </summary>
    public static class QnT_ConditionsParams_TS_   
    {
        public const int __tGid = 15791;
        public const string __tName = "QnT_ConditionsParams";
        static mdTable m_mdTable;
        static QnT_ConditionsParams_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _QuestionnaireTemplate_fGid = 31890;
       
        public static mdTableField QuestionnaireTemplate
        {
            get { return __MD.Fields[_QuestionnaireTemplate_fGid];}
        }
        public const string _QuestionnaireTemplate_fName = "QuestionnaireTemplate";        public const int _QnT_Condition_fGid = 31891;
       
        public static mdTableField QnT_Condition
        {
            get { return __MD.Fields[_QnT_Condition_fGid];}
        }
        public const string _QnT_Condition_fName = "QnT_Condition";        public const int _Ordinal_fGid = 31892;
       
        public static mdTableField Ordinal
        {
            get { return __MD.Fields[_Ordinal_fGid];}
        }
        public const string _Ordinal_fName = "Ordinal";        public const int _ParamValue_fGid = 31893;
       
        public static mdTableField ParamValue
        {
            get { return __MD.Fields[_ParamValue_fGid];}
        }
        public const string _ParamValue_fName = "ParamValue";        public const int _ParamValue_tid_fGid = 31905;
       
        public static mdTableField ParamValue_tid
        {
            get { return __MD.Fields[_ParamValue_tid_fGid];}
        }
        public const string _ParamValue_tid_fName = "ParamValue_tid";        public const int _ParamAlias_fGid = 32190;
       
        public static mdTableField ParamAlias
        {
            get { return __MD.Fields[_ParamAlias_fGid];}
        }
        public const string _ParamAlias_fName = "ParamAlias";       
        public static mdTableField QuestionnaireTemplate_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_QuestionnaireTemplate_fGid]);}
        }
        public const string _QuestionnaireTemplate_view__fName = "QuestionnaireTemplate_view_";       
        public static mdTableField QnT_Condition_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_QnT_Condition_fGid]);}
        }
        public const string _QnT_Condition_view__fName = "QnT_Condition_view_";       
        public static mdTableField QuestionnaireTemplate_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_QuestionnaireTemplate_fGid]);}
        }
        public const string _QuestionnaireTemplate_status__fName = "QuestionnaireTemplate_status_";       
        public static mdTableField QnT_Condition_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_QnT_Condition_fGid]);}
        }
        public const string _QnT_Condition_status__fName = "QnT_Condition_status_";       
        public static mdTableField ParamValue_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_ParamValue_fGid]);}
        }
        public const string _ParamValue_view__fName = "ParamValue_view_";        public const int _ParamValue_view_QnT_Items__fGid = 99000439;
       
        public static mdTableField ParamValue_view_QnT_Items_
        {
            get { return __MD.Fields[_ParamValue_view_QnT_Items__fGid];}
        }
        public const string _ParamValue_view_QnT_Items__fName = "ParamValue_view_QnT_Items_";        public const int _ParamValue_view_QnAlternativesGroups__fGid = 99000440;
       
        public static mdTableField ParamValue_view_QnAlternativesGroups_
        {
            get { return __MD.Fields[_ParamValue_view_QnAlternativesGroups__fGid];}
        }
        public const string _ParamValue_view_QnAlternativesGroups__fName = "ParamValue_view_QnAlternativesGroups_";        public const int _ParamValue_view_QnAlternativesItems__fGid = 99000441;
       
        public static mdTableField ParamValue_view_QnAlternativesItems_
        {
            get { return __MD.Fields[_ParamValue_view_QnAlternativesItems__fGid];}
        }
        public const string _ParamValue_view_QnAlternativesItems__fName = "ParamValue_view_QnAlternativesItems_";
    }
    /// <summary>
    /// Схема таблиці 'Питання в шаблонах опитувань'
    /// </summary>
    public static class QnT_Items_TS_   
    {
        public const int __tGid = 15771;
        public const string __tName = "QnT_Items";
        static mdTable m_mdTable;
        static QnT_Items_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _gid_fGid = 31648;
       
        public static mdTableField gid
        {
            get { return __MD.Fields[_gid_fGid];}
        }
        public const string _gid_fName = "gid";        public const int _QuestionnaireTemplate_fGid = 31649;
       
        public static mdTableField QuestionnaireTemplate
        {
            get { return __MD.Fields[_QuestionnaireTemplate_fGid];}
        }
        public const string _QuestionnaireTemplate_fName = "QuestionnaireTemplate";        public const int _QuestionGroup_fGid = 31650;
       
        public static mdTableField QuestionGroup
        {
            get { return __MD.Fields[_QuestionGroup_fGid];}
        }
        public const string _QuestionGroup_fName = "QuestionGroup";        public const int _QuestionKind_fGid = 31677;
       
        public static mdTableField QuestionKind
        {
            get { return __MD.Fields[_QuestionKind_fGid];}
        }
        public const string _QuestionKind_fName = "QuestionKind";        public const int _AlternativesGroup_fGid = 31678;
       
        public static mdTableField AlternativesGroup
        {
            get { return __MD.Fields[_AlternativesGroup_fGid];}
        }
        public const string _AlternativesGroup_fName = "AlternativesGroup";        public const int _QuestionText_fGid = 31679;
       
        public static mdTableField QuestionText
        {
            get { return __MD.Fields[_QuestionText_fGid];}
        }
        public const string _QuestionText_fName = "QuestionText";        public const int _AnswerRequiered_fGid = 31680;
       
        public static mdTableField AnswerRequiered
        {
            get { return __MD.Fields[_AnswerRequiered_fGid];}
        }
        public const string _AnswerRequiered_fName = "AnswerRequiered";        public const int _ordinal_fGid = 31681;
       
        public static mdTableField ordinal
        {
            get { return __MD.Fields[_ordinal_fGid];}
        }
        public const string _ordinal_fName = "ordinal";        public const int _ItemCode_fGid = 31686;
       
        public static mdTableField ItemCode
        {
            get { return __MD.Fields[_ItemCode_fGid];}
        }
        public const string _ItemCode_fName = "ItemCode";        public const int _InterviewersComment_fGid = 31695;
       
        public static mdTableField InterviewersComment
        {
            get { return __MD.Fields[_InterviewersComment_fGid];}
        }
        public const string _InterviewersComment_fName = "InterviewersComment";        public const int _RespondentsComment_fGid = 31696;
       
        public static mdTableField RespondentsComment
        {
            get { return __MD.Fields[_RespondentsComment_fGid];}
        }
        public const string _RespondentsComment_fName = "RespondentsComment";        public const int _AvailabilityCondition_fGid = 31739;
       
        public static mdTableField AvailabilityCondition
        {
            get { return __MD.Fields[_AvailabilityCondition_fGid];}
        }
        public const string _AvailabilityCondition_fName = "AvailabilityCondition";        public const int _comments_fGid = 31740;
       
        public static mdTableField comments
        {
            get { return __MD.Fields[_comments_fGid];}
        }
        public const string _comments_fName = "comments";        public const int _IsOpenQuestion_fGid = 31753;
       
        public static mdTableField IsOpenQuestion
        {
            get { return __MD.Fields[_IsOpenQuestion_fGid];}
        }
        public const string _IsOpenQuestion_fName = "IsOpenQuestion";        public const int _QuestionDataType_fGid = 31882;
       
        public static mdTableField QuestionDataType
        {
            get { return __MD.Fields[_QuestionDataType_fGid];}
        }
        public const string _QuestionDataType_fName = "QuestionDataType";        public const int _QParams_fGid = 31927;
       
        public static mdTableField QParams
        {
            get { return __MD.Fields[_QParams_fGid];}
        }
        public const string _QParams_fName = "QParams";        public const int _asid_fGid = 31961;
       
        public static mdTableField asid
        {
            get { return __MD.Fields[_asid_fGid];}
        }
        public const string _asid_fName = "asid";        public const int _AlternativesGroup2_fGid = 32064;
       
        public static mdTableField AlternativesGroup2
        {
            get { return __MD.Fields[_AlternativesGroup2_fGid];}
        }
        public const string _AlternativesGroup2_fName = "AlternativesGroup2";        public const int _AvConditionType_fGid = 32180;
       
        public static mdTableField AvConditionType
        {
            get { return __MD.Fields[_AvConditionType_fGid];}
        }
        public const string _AvConditionType_fName = "AvConditionType";        public const int _AnswersCount_fGid = 32182;
       
        public static mdTableField AnswersCount
        {
            get { return __MD.Fields[_AnswersCount_fGid];}
        }
        public const string _AnswersCount_fName = "AnswersCount";       
        public static mdTableField QuestionnaireTemplate_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_QuestionnaireTemplate_fGid]);}
        }
        public const string _QuestionnaireTemplate_view__fName = "QuestionnaireTemplate_view_";       
        public static mdTableField QuestionGroup_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_QuestionGroup_fGid]);}
        }
        public const string _QuestionGroup_view__fName = "QuestionGroup_view_";       
        public static mdTableField QuestionKind_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_QuestionKind_fGid]);}
        }
        public const string _QuestionKind_view__fName = "QuestionKind_view_";       
        public static mdTableField AlternativesGroup_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_AlternativesGroup_fGid]);}
        }
        public const string _AlternativesGroup_view__fName = "AlternativesGroup_view_";       
        public static mdTableField AvailabilityCondition_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_AvailabilityCondition_fGid]);}
        }
        public const string _AvailabilityCondition_view__fName = "AvailabilityCondition_view_";       
        public static mdTableField AlternativesGroup2_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_AlternativesGroup2_fGid]);}
        }
        public const string _AlternativesGroup2_view__fName = "AlternativesGroup2_view_";       
        public static mdTableField QuestionnaireTemplate_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_QuestionnaireTemplate_fGid]);}
        }
        public const string _QuestionnaireTemplate_status__fName = "QuestionnaireTemplate_status_";       
        public static mdTableField QuestionGroup_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_QuestionGroup_fGid]);}
        }
        public const string _QuestionGroup_status__fName = "QuestionGroup_status_";       
        public static mdTableField QuestionKind_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_QuestionKind_fGid]);}
        }
        public const string _QuestionKind_status__fName = "QuestionKind_status_";       
        public static mdTableField AlternativesGroup_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_AlternativesGroup_fGid]);}
        }
        public const string _AlternativesGroup_status__fName = "AlternativesGroup_status_";       
        public static mdTableField AvailabilityCondition_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_AvailabilityCondition_fGid]);}
        }
        public const string _AvailabilityCondition_status__fName = "AvailabilityCondition_status_";       
        public static mdTableField AlternativesGroup2_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_AlternativesGroup2_fGid]);}
        }
        public const string _AlternativesGroup2_status__fName = "AlternativesGroup2_status_";
    }
    /// <summary>
    /// Схема таблиці 'Умовні переходи в питаннях'
    /// </summary>
    public static class QnT_PassConditions_TS_   
    {
        public const int __tGid = 15793;
        public const string __tName = "QnT_PassConditions";
        static mdTable m_mdTable;
        static QnT_PassConditions_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _QuestionnaireTemplate_fGid = 31898;
       
        public static mdTableField QuestionnaireTemplate
        {
            get { return __MD.Fields[_QuestionnaireTemplate_fGid];}
        }
        public const string _QuestionnaireTemplate_fName = "QuestionnaireTemplate";        public const int _BaseQuestion_fGid = 31899;
       
        public static mdTableField BaseQuestion
        {
            get { return __MD.Fields[_BaseQuestion_fGid];}
        }
        public const string _BaseQuestion_fName = "BaseQuestion";        public const int _RowSubKey_fGid = 31900;
       
        public static mdTableField RowSubKey
        {
            get { return __MD.Fields[_RowSubKey_fGid];}
        }
        public const string _RowSubKey_fName = "RowSubKey";        public const int _ordinal_fGid = 31901;
       
        public static mdTableField ordinal
        {
            get { return __MD.Fields[_ordinal_fGid];}
        }
        public const string _ordinal_fName = "ordinal";        public const int _NextQuestion_fGid = 31902;
       
        public static mdTableField NextQuestion
        {
            get { return __MD.Fields[_NextQuestion_fGid];}
        }
        public const string _NextQuestion_fName = "NextQuestion";        public const int _QnT_Condition_fGid = 31903;
       
        public static mdTableField QnT_Condition
        {
            get { return __MD.Fields[_QnT_Condition_fGid];}
        }
        public const string _QnT_Condition_fName = "QnT_Condition";        public const int _NextQGroup_fGid = 32142;
       
        public static mdTableField NextQGroup
        {
            get { return __MD.Fields[_NextQGroup_fGid];}
        }
        public const string _NextQGroup_fName = "NextQGroup";       
        public static mdTableField QuestionnaireTemplate_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_QuestionnaireTemplate_fGid]);}
        }
        public const string _QuestionnaireTemplate_view__fName = "QuestionnaireTemplate_view_";       
        public static mdTableField BaseQuestion_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_BaseQuestion_fGid]);}
        }
        public const string _BaseQuestion_view__fName = "BaseQuestion_view_";       
        public static mdTableField NextQuestion_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_NextQuestion_fGid]);}
        }
        public const string _NextQuestion_view__fName = "NextQuestion_view_";       
        public static mdTableField QnT_Condition_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_QnT_Condition_fGid]);}
        }
        public const string _QnT_Condition_view__fName = "QnT_Condition_view_";       
        public static mdTableField NextQGroup_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_NextQGroup_fGid]);}
        }
        public const string _NextQGroup_view__fName = "NextQGroup_view_";       
        public static mdTableField QuestionnaireTemplate_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_QuestionnaireTemplate_fGid]);}
        }
        public const string _QuestionnaireTemplate_status__fName = "QuestionnaireTemplate_status_";       
        public static mdTableField BaseQuestion_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_BaseQuestion_fGid]);}
        }
        public const string _BaseQuestion_status__fName = "BaseQuestion_status_";       
        public static mdTableField NextQuestion_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_NextQuestion_fGid]);}
        }
        public const string _NextQuestion_status__fName = "NextQuestion_status_";       
        public static mdTableField QnT_Condition_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_QnT_Condition_fGid]);}
        }
        public const string _QnT_Condition_status__fName = "QnT_Condition_status_";       
        public static mdTableField NextQGroup_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_NextQGroup_fGid]);}
        }
        public const string _NextQGroup_status__fName = "NextQGroup_status_";
    }
    /// <summary>
    /// Схема таблиці 'Групи питань в опитуванні'
    /// </summary>
    public static class QuestionGroups_TS_   
    {
        public const int __tGid = 15770;
        public const string __tName = "QuestionGroups";
        static mdTable m_mdTable;
        static QuestionGroups_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _gid_fGid = 31643;
       
        public static mdTableField gid
        {
            get { return __MD.Fields[_gid_fGid];}
        }
        public const string _gid_fName = "gid";        public const int _name_fGid = 31644;
       
        public static mdTableField name
        {
            get { return __MD.Fields[_name_fGid];}
        }
        public const string _name_fName = "name";        public const int _comments_fGid = 31645;
       
        public static mdTableField comments
        {
            get { return __MD.Fields[_comments_fGid];}
        }
        public const string _comments_fName = "comments";        public const int _QuestionnaireTemplate_fGid = 31646;
       
        public static mdTableField QuestionnaireTemplate
        {
            get { return __MD.Fields[_QuestionnaireTemplate_fGid];}
        }
        public const string _QuestionnaireTemplate_fName = "QuestionnaireTemplate";        public const int _parent_group_fGid = 31647;
       
        public static mdTableField parent_group
        {
            get { return __MD.Fields[_parent_group_fGid];}
        }
        public const string _parent_group_fName = "parent_group";        public const int _ordinal_fGid = 31741;
       
        public static mdTableField ordinal
        {
            get { return __MD.Fields[_ordinal_fGid];}
        }
        public const string _ordinal_fName = "ordinal";        public const int _ViewStyle_fGid = 31748;
       
        public static mdTableField ViewStyle
        {
            get { return __MD.Fields[_ViewStyle_fGid];}
        }
        public const string _ViewStyle_fName = "ViewStyle";        public const int _ViewTemplate_fGid = 31749;
       
        public static mdTableField ViewTemplate
        {
            get { return __MD.Fields[_ViewTemplate_fGid];}
        }
        public const string _ViewTemplate_fName = "ViewTemplate";        public const int _InterviewersComment_fGid = 31750;
       
        public static mdTableField InterviewersComment
        {
            get { return __MD.Fields[_InterviewersComment_fGid];}
        }
        public const string _InterviewersComment_fName = "InterviewersComment";        public const int _RespondentsComment_fGid = 31751;
       
        public static mdTableField RespondentsComment
        {
            get { return __MD.Fields[_RespondentsComment_fGid];}
        }
        public const string _RespondentsComment_fName = "RespondentsComment";        public const int _GroupCaption_fGid = 31752;
       
        public static mdTableField GroupCaption
        {
            get { return __MD.Fields[_GroupCaption_fGid];}
        }
        public const string _GroupCaption_fName = "GroupCaption";        public const int _AlternativesGroup_fGid = 31754;
       
        public static mdTableField AlternativesGroup
        {
            get { return __MD.Fields[_AlternativesGroup_fGid];}
        }
        public const string _AlternativesGroup_fName = "AlternativesGroup";        public const int _asid_fGid = 31964;
       
        public static mdTableField asid
        {
            get { return __MD.Fields[_asid_fGid];}
        }
        public const string _asid_fName = "asid";        public const int _QItemsCount_fGid = 32191;
       
        public static mdTableField QItemsCount
        {
            get { return __MD.Fields[_QItemsCount_fGid];}
        }
        public const string _QItemsCount_fName = "QItemsCount";        public const int _ChildGroupsCount_fGid = 32192;
       
        public static mdTableField ChildGroupsCount
        {
            get { return __MD.Fields[_ChildGroupsCount_fGid];}
        }
        public const string _ChildGroupsCount_fName = "ChildGroupsCount";       
        public static mdTableField QuestionnaireTemplate_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_QuestionnaireTemplate_fGid]);}
        }
        public const string _QuestionnaireTemplate_view__fName = "QuestionnaireTemplate_view_";       
        public static mdTableField parent_group_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_parent_group_fGid]);}
        }
        public const string _parent_group_view__fName = "parent_group_view_";       
        public static mdTableField AlternativesGroup_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_AlternativesGroup_fGid]);}
        }
        public const string _AlternativesGroup_view__fName = "AlternativesGroup_view_";       
        public static mdTableField QuestionnaireTemplate_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_QuestionnaireTemplate_fGid]);}
        }
        public const string _QuestionnaireTemplate_status__fName = "QuestionnaireTemplate_status_";       
        public static mdTableField parent_group_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_parent_group_fGid]);}
        }
        public const string _parent_group_status__fName = "parent_group_status_";       
        public static mdTableField AlternativesGroup_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_AlternativesGroup_fGid]);}
        }
        public const string _AlternativesGroup_status__fName = "AlternativesGroup_status_";
    }
    /// <summary>
    /// Схема таблиці 'Групи видів питань'
    /// </summary>
    public static class QuestionKindGroups_TS_   
    {
        public const int __tGid = 15797;
        public const string __tName = "QuestionKindGroups";
        static mdTable m_mdTable;
        static QuestionKindGroups_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _gid_fGid = 32007;
       
        public static mdTableField gid
        {
            get { return __MD.Fields[_gid_fGid];}
        }
        public const string _gid_fName = "gid";        public const int _name_fGid = 32008;
       
        public static mdTableField name
        {
            get { return __MD.Fields[_name_fGid];}
        }
        public const string _name_fName = "name";        public const int _object_status_fGid = 32009;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _comments_fGid = 32010;
       
        public static mdTableField comments
        {
            get { return __MD.Fields[_comments_fGid];}
        }
        public const string _comments_fName = "comments";        public const int _version_no_fGid = 32011;
       
        public static mdTableField version_no
        {
            get { return __MD.Fields[_version_no_fGid];}
        }
        public const string _version_no_fName = "version_no";        public const int _parent_group_fGid = 32012;
       
        public static mdTableField parent_group
        {
            get { return __MD.Fields[_parent_group_fGid];}
        }
        public const string _parent_group_fName = "parent_group";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";       
        public static mdTableField parent_group_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_parent_group_fGid]);}
        }
        public const string _parent_group_view__fName = "parent_group_view_";       
        public static mdTableField parent_group_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_parent_group_fGid]);}
        }
        public const string _parent_group_status__fName = "parent_group_status_";
    }
    /// <summary>
    /// Схема таблиці 'Види питань'
    /// </summary>
    public static class QuestionKinds_TS_   
    {
        public const int __tGid = 15772;
        public const string __tName = "QuestionKinds";
        static mdTable m_mdTable;
        static QuestionKinds_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _gid_fGid = 31651;
       
        public static mdTableField gid
        {
            get { return __MD.Fields[_gid_fGid];}
        }
        public const string _gid_fName = "gid";        public const int _name_fGid = 31652;
       
        public static mdTableField name
        {
            get { return __MD.Fields[_name_fGid];}
        }
        public const string _name_fName = "name";        public const int _DataType_fGid = 31653;
       
        public static mdTableField DataType
        {
            get { return __MD.Fields[_DataType_fGid];}
        }
        public const string _DataType_fName = "DataType";        public const int _IsOpenQuestion_fGid = 31687;
       
        public static mdTableField IsOpenQuestion
        {
            get { return __MD.Fields[_IsOpenQuestion_fGid];}
        }
        public const string _IsOpenQuestion_fName = "IsOpenQuestion";        public const int _Code_fGid = 31688;
       
        public static mdTableField Code
        {
            get { return __MD.Fields[_Code_fGid];}
        }
        public const string _Code_fName = "Code";        public const int _comments_fGid = 31689;
       
        public static mdTableField comments
        {
            get { return __MD.Fields[_comments_fGid];}
        }
        public const string _comments_fName = "comments";        public const int _RespondentsComment_fGid = 31690;
       
        public static mdTableField RespondentsComment
        {
            get { return __MD.Fields[_RespondentsComment_fGid];}
        }
        public const string _RespondentsComment_fName = "RespondentsComment";        public const int _InterviewersComment_fGid = 31691;
       
        public static mdTableField InterviewersComment
        {
            get { return __MD.Fields[_InterviewersComment_fGid];}
        }
        public const string _InterviewersComment_fName = "InterviewersComment";        public const int _ViewTemplate_fGid = 31692;
       
        public static mdTableField ViewTemplate
        {
            get { return __MD.Fields[_ViewTemplate_fGid];}
        }
        public const string _ViewTemplate_fName = "ViewTemplate";        public const int _AlternativesGroup_fGid = 31693;
       
        public static mdTableField AlternativesGroup
        {
            get { return __MD.Fields[_AlternativesGroup_fGid];}
        }
        public const string _AlternativesGroup_fName = "AlternativesGroup";        public const int _AnswerRequiered_fGid = 31694;
       
        public static mdTableField AnswerRequiered
        {
            get { return __MD.Fields[_AnswerRequiered_fGid];}
        }
        public const string _AnswerRequiered_fName = "AnswerRequiered";        public const int _object_status_fGid = 31698;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _version_no_fGid = 31700;
       
        public static mdTableField version_no
        {
            get { return __MD.Fields[_version_no_fGid];}
        }
        public const string _version_no_fName = "version_no";        public const int _asid_fGid = 31963;
       
        public static mdTableField asid
        {
            get { return __MD.Fields[_asid_fGid];}
        }
        public const string _asid_fName = "asid";        public const int _KindGroup_fGid = 32013;
       
        public static mdTableField KindGroup
        {
            get { return __MD.Fields[_KindGroup_fGid];}
        }
        public const string _KindGroup_fName = "KindGroup";       
        public static mdTableField AlternativesGroup_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_AlternativesGroup_fGid]);}
        }
        public const string _AlternativesGroup_view__fName = "AlternativesGroup_view_";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";       
        public static mdTableField KindGroup_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_KindGroup_fGid]);}
        }
        public const string _KindGroup_view__fName = "KindGroup_view_";       
        public static mdTableField AlternativesGroup_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_AlternativesGroup_fGid]);}
        }
        public const string _AlternativesGroup_status__fName = "AlternativesGroup_status_";       
        public static mdTableField KindGroup_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_KindGroup_fGid]);}
        }
        public const string _KindGroup_status__fName = "KindGroup_status_";
    }
    /// <summary>
    /// Схема таблиці 'Заповнені анкети'
    /// </summary>
    public static class QuestionnairesFilled_TS_   
    {
        public const int __tGid = 15778;
        public const string __tName = "QuestionnairesFilled";
        static mdTable m_mdTable;
        static QuestionnairesFilled_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _reg_date_fGid = 31710;
       
        public static mdTableField reg_date
        {
            get { return __MD.Fields[_reg_date_fGid];}
        }
        public const string _reg_date_fName = "reg_date";        public const int _doc_number_fGid = 31711;
       
        public static mdTableField doc_number
        {
            get { return __MD.Fields[_doc_number_fGid];}
        }
        public const string _doc_number_fName = "doc_number";        public const int _short_representation_fGid = 31712;
       
        public static mdTableField short_representation
        {
            get { return __MD.Fields[_short_representation_fGid];}
        }
        public const string _short_representation_fName = "short_representation";        public const int _Project_fGid = 31713;
       
        public static mdTableField Project
        {
            get { return __MD.Fields[_Project_fGid];}
        }
        public const string _Project_fName = "Project";        public const int _QnTemplate_fGid = 31714;
       
        public static mdTableField QnTemplate
        {
            get { return __MD.Fields[_QnTemplate_fGid];}
        }
        public const string _QnTemplate_fName = "QnTemplate";        public const int _Author_fGid = 31715;
       
        public static mdTableField Author
        {
            get { return __MD.Fields[_Author_fGid];}
        }
        public const string _Author_fName = "Author";        public const int _Operation_fGid = 31717;
       
        public static mdTableField Operation
        {
            get { return __MD.Fields[_Operation_fGid];}
        }
        public const string _Operation_fName = "Operation";        public const int _Interviewer_fGid = 31881;
       
        public static mdTableField Interviewer
        {
            get { return __MD.Fields[_Interviewer_fGid];}
        }
        public const string _Interviewer_fName = "Interviewer";        public const int _object_status_fGid = 32085;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _WebRequest_fGid = 32237;
       
        public static mdTableField WebRequest
        {
            get { return __MD.Fields[_WebRequest_fGid];}
        }
        public const string _WebRequest_fName = "WebRequest";        public const int _ip_start_fGid = 32238;
       
        public static mdTableField ip_start
        {
            get { return __MD.Fields[_ip_start_fGid];}
        }
        public const string _ip_start_fName = "ip_start";        public const int _ip_end_fGid = 32239;
       
        public static mdTableField ip_end
        {
            get { return __MD.Fields[_ip_end_fGid];}
        }
        public const string _ip_end_fName = "ip_end";        public const int _time_start_fGid = 32240;
       
        public static mdTableField time_start
        {
            get { return __MD.Fields[_time_start_fGid];}
        }
        public const string _time_start_fName = "time_start";        public const int _UserSession_fGid = 32265;
       
        public static mdTableField UserSession
        {
            get { return __MD.Fields[_UserSession_fGid];}
        }
        public const string _UserSession_fName = "UserSession";        public const int _MailTime_fGid = 32270;
       
        public static mdTableField MailTime
        {
            get { return __MD.Fields[_MailTime_fGid];}
        }
        public const string _MailTime_fName = "MailTime";        public const int _WebBrowser_fGid = 32304;
       
        public static mdTableField WebBrowser
        {
            get { return __MD.Fields[_WebBrowser_fGid];}
        }
        public const string _WebBrowser_fName = "WebBrowser";        public const int _WebAuthor_fGid = 32324;
       
        public static mdTableField WebAuthor
        {
            get { return __MD.Fields[_WebAuthor_fGid];}
        }
        public const string _WebAuthor_fName = "WebAuthor";       
        public static mdTableField RequestCode
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_WebRequest_fGid]);}
        }
        public const string _RequestCode_fName = "RequestCode";        public const int _KeyQuestion_fGid = 32978;
       
        public static mdTableField KeyQuestion
        {
            get { return __MD.Fields[_KeyQuestion_fGid];}
        }
        public const string _KeyQuestion_fName = "KeyQuestion";        public const int _KeyIntAnswer_fGid = 32979;
       
        public static mdTableField KeyIntAnswer
        {
            get { return __MD.Fields[_KeyIntAnswer_fGid];}
        }
        public const string _KeyIntAnswer_fName = "KeyIntAnswer";        public const int _KeyAlternative_fGid = 32980;
       
        public static mdTableField KeyAlternative
        {
            get { return __MD.Fields[_KeyAlternative_fGid];}
        }
        public const string _KeyAlternative_fName = "KeyAlternative";       
        public static mdTableField Project_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Project_fGid]);}
        }
        public const string _Project_view__fName = "Project_view_";       
        public static mdTableField QnTemplate_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_QnTemplate_fGid]);}
        }
        public const string _QnTemplate_view__fName = "QnTemplate_view_";       
        public static mdTableField Author_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Author_fGid]);}
        }
        public const string _Author_view__fName = "Author_view_";       
        public static mdTableField Operation_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Operation_fGid]);}
        }
        public const string _Operation_view__fName = "Operation_view_";       
        public static mdTableField Interviewer_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Interviewer_fGid]);}
        }
        public const string _Interviewer_view__fName = "Interviewer_view_";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";       
        public static mdTableField UserSession_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_UserSession_fGid]);}
        }
        public const string _UserSession_view__fName = "UserSession_view_";       
        public static mdTableField WebBrowser_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_WebBrowser_fGid]);}
        }
        public const string _WebBrowser_view__fName = "WebBrowser_view_";       
        public static mdTableField WebAuthor_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_WebAuthor_fGid]);}
        }
        public const string _WebAuthor_view__fName = "WebAuthor_view_";       
        public static mdTableField KeyQuestion_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_KeyQuestion_fGid]);}
        }
        public const string _KeyQuestion_view__fName = "KeyQuestion_view_";       
        public static mdTableField KeyAlternative_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_KeyAlternative_fGid]);}
        }
        public const string _KeyAlternative_view__fName = "KeyAlternative_view_";       
        public static mdTableField Project_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Project_fGid]);}
        }
        public const string _Project_status__fName = "Project_status_";       
        public static mdTableField QnTemplate_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_QnTemplate_fGid]);}
        }
        public const string _QnTemplate_status__fName = "QnTemplate_status_";       
        public static mdTableField Author_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Author_fGid]);}
        }
        public const string _Author_status__fName = "Author_status_";       
        public static mdTableField Interviewer_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Interviewer_fGid]);}
        }
        public const string _Interviewer_status__fName = "Interviewer_status_";       
        public static mdTableField WebRequest_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_WebRequest_fGid]);}
        }
        public const string _WebRequest_status__fName = "WebRequest_status_";       
        public static mdTableField UserSession_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_UserSession_fGid]);}
        }
        public const string _UserSession_status__fName = "UserSession_status_";       
        public static mdTableField WebAuthor_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_WebAuthor_fGid]);}
        }
        public const string _WebAuthor_status__fName = "WebAuthor_status_";       
        public static mdTableField KeyQuestion_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_KeyQuestion_fGid]);}
        }
        public const string _KeyQuestion_status__fName = "KeyQuestion_status_";       
        public static mdTableField KeyAlternative_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_KeyAlternative_fGid]);}
        }
        public const string _KeyAlternative_status__fName = "KeyAlternative_status_";
    }
    /// <summary>
    /// Схема таблиці 'Шаблони опитувань'
    /// </summary>
    public static class QuestionnaireTemplates_TS_   
    {
        public const int __tGid = 15769;
        public const string __tName = "QuestionnaireTemplates";
        static mdTable m_mdTable;
        static QuestionnaireTemplates_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _gid_fGid = 31637;
       
        public static mdTableField gid
        {
            get { return __MD.Fields[_gid_fGid];}
        }
        public const string _gid_fName = "gid";        public const int _name_fGid = 31638;
       
        public static mdTableField name
        {
            get { return __MD.Fields[_name_fGid];}
        }
        public const string _name_fName = "name";        public const int _object_status_fGid = 31639;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _version_no_fGid = 31640;
       
        public static mdTableField version_no
        {
            get { return __MD.Fields[_version_no_fGid];}
        }
        public const string _version_no_fName = "version_no";        public const int _comments_fGid = 31641;
       
        public static mdTableField comments
        {
            get { return __MD.Fields[_comments_fGid];}
        }
        public const string _comments_fName = "comments";        public const int _Project_fGid = 31642;
       
        public static mdTableField Project
        {
            get { return __MD.Fields[_Project_fGid];}
        }
        public const string _Project_fName = "Project";        public const int _Description_fGid = 31697;
       
        public static mdTableField Description
        {
            get { return __MD.Fields[_Description_fGid];}
        }
        public const string _Description_fName = "Description";        public const int _MainImage_fGid = 32052;
       
        public static mdTableField MainImage
        {
            get { return __MD.Fields[_MainImage_fGid];}
        }
        public const string _MainImage_fName = "MainImage";        public const int _StartDate_fGid = 32053;
       
        public static mdTableField StartDate
        {
            get { return __MD.Fields[_StartDate_fGid];}
        }
        public const string _StartDate_fName = "StartDate";        public const int _EndDate_fGid = 32054;
       
        public static mdTableField EndDate
        {
            get { return __MD.Fields[_EndDate_fGid];}
        }
        public const string _EndDate_fName = "EndDate";        public const int _Start_text_fGid = 32068;
       
        public static mdTableField Start_text
        {
            get { return __MD.Fields[_Start_text_fGid];}
        }
        public const string _Start_text_fName = "Start_text";        public const int _End_text_fGid = 32069;
       
        public static mdTableField End_text
        {
            get { return __MD.Fields[_End_text_fGid];}
        }
        public const string _End_text_fName = "End_text";        public const int _refusal_text_fGid = 32070;
       
        public static mdTableField refusal_text
        {
            get { return __MD.Fields[_refusal_text_fGid];}
        }
        public const string _refusal_text_fName = "refusal_text";        public const int _QuestionnairesFilledCount_fGid = 32084;
       
        public static mdTableField QuestionnairesFilledCount
        {
            get { return __MD.Fields[_QuestionnairesFilledCount_fGid];}
        }
        public const string _QuestionnairesFilledCount_fName = "QuestionnairesFilledCount";        public const int _ParticipantsGroup_fGid = 32312;
       
        public static mdTableField ParticipantsGroup
        {
            get { return __MD.Fields[_ParticipantsGroup_fGid];}
        }
        public const string _ParticipantsGroup_fName = "ParticipantsGroup";        public const int _HeaderHTML_fGid = 32428;
       
        public static mdTableField HeaderHTML
        {
            get { return __MD.Fields[_HeaderHTML_fGid];}
        }
        public const string _HeaderHTML_fName = "HeaderHTML";        public const int _URLName_fGid = 32949;
       
        public static mdTableField URLName
        {
            get { return __MD.Fields[_URLName_fGid];}
        }
        public const string _URLName_fName = "URLName";        public const int _QOptions_fGid = 32968;
       
        public static mdTableField QOptions
        {
            get { return __MD.Fields[_QOptions_fGid];}
        }
        public const string _QOptions_fName = "QOptions";        public const int _KeyQuestion_fGid = 32977;
       
        public static mdTableField KeyQuestion
        {
            get { return __MD.Fields[_KeyQuestion_fGid];}
        }
        public const string _KeyQuestion_fName = "KeyQuestion";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";       
        public static mdTableField Project_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Project_fGid]);}
        }
        public const string _Project_view__fName = "Project_view_";       
        public static mdTableField MainImage_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_MainImage_fGid]);}
        }
        public const string _MainImage_view__fName = "MainImage_view_";       
        public static mdTableField ParticipantsGroup_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_ParticipantsGroup_fGid]);}
        }
        public const string _ParticipantsGroup_view__fName = "ParticipantsGroup_view_";       
        public static mdTableField KeyQuestion_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_KeyQuestion_fGid]);}
        }
        public const string _KeyQuestion_view__fName = "KeyQuestion_view_";       
        public static mdTableField Project_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Project_fGid]);}
        }
        public const string _Project_status__fName = "Project_status_";       
        public static mdTableField MainImage_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_MainImage_fGid]);}
        }
        public const string _MainImage_status__fName = "MainImage_status_";       
        public static mdTableField ParticipantsGroup_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_ParticipantsGroup_fGid]);}
        }
        public const string _ParticipantsGroup_status__fName = "ParticipantsGroup_status_";       
        public static mdTableField KeyQuestion_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_KeyQuestion_fGid]);}
        }
        public const string _KeyQuestion_status__fName = "KeyQuestion_status_";
    }
    /// <summary>
    /// Схема таблиці 'Параметри респондентів'
    /// </summary>
    public static class RespondentsParameters_TS_   
    {
        public const int __tGid = 15827;
        public const string __tName = "RespondentsParameters";
        static mdTable m_mdTable;
        static RespondentsParameters_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _Project_fGid = 33060;
       
        public static mdTableField Project
        {
            get { return __MD.Fields[_Project_fGid];}
        }
        public const string _Project_fName = "Project";        public const int _Respondent_fGid = 33061;
       
        public static mdTableField Respondent
        {
            get { return __MD.Fields[_Respondent_fGid];}
        }
        public const string _Respondent_fName = "Respondent";        public const int _ParamName_fGid = 33062;
       
        public static mdTableField ParamName
        {
            get { return __MD.Fields[_ParamName_fGid];}
        }
        public const string _ParamName_fName = "ParamName";        public const int _ParamValue_fGid = 33063;
       
        public static mdTableField ParamValue
        {
            get { return __MD.Fields[_ParamValue_fGid];}
        }
        public const string _ParamValue_fName = "ParamValue";        public const int _version_no_fGid = 33066;
       
        public static mdTableField version_no
        {
            get { return __MD.Fields[_version_no_fGid];}
        }
        public const string _version_no_fName = "version_no";       
        public static mdTableField Project_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Project_fGid]);}
        }
        public const string _Project_view__fName = "Project_view_";       
        public static mdTableField Respondent_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Respondent_fGid]);}
        }
        public const string _Respondent_view__fName = "Respondent_view_";       
        public static mdTableField Project_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Project_fGid]);}
        }
        public const string _Project_status__fName = "Project_status_";       
        public static mdTableField Respondent_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Respondent_fGid]);}
        }
        public const string _Respondent_status__fName = "Respondent_status_";
    }
    /// <summary>
    /// Схема таблиці 'Перевірка рішення'
    /// </summary>
    public static class EduChecking_TS_   
    {
        public const int __tGid = 15923;
        public const string __tName = "EduChecking";
        static mdTable m_mdTable;
        static EduChecking_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _gid_fGid = 34383;
       
        public static mdTableField gid
        {
            get { return __MD.Fields[_gid_fGid];}
        }
        public const string _gid_fName = "gid";        public const int _Solution_fGid = 34397;
       
        public static mdTableField Solution
        {
            get { return __MD.Fields[_Solution_fGid];}
        }
        public const string _Solution_fName = "Solution";        public const int _Mark_fGid = 34399;
       
        public static mdTableField Mark
        {
            get { return __MD.Fields[_Mark_fGid];}
        }
        public const string _Mark_fName = "Mark";        public const int _StartDate_fGid = 34400;
       
        public static mdTableField StartDate
        {
            get { return __MD.Fields[_StartDate_fGid];}
        }
        public const string _StartDate_fName = "StartDate";        public const int _reg_date_fGid = 34402;
       
        public static mdTableField reg_date
        {
            get { return __MD.Fields[_reg_date_fGid];}
        }
        public const string _reg_date_fName = "reg_date";        public const int _Teacher_fGid = 34413;
       
        public static mdTableField Teacher
        {
            get { return __MD.Fields[_Teacher_fGid];}
        }
        public const string _Teacher_fName = "Teacher";        public const int _object_status_fGid = 34450;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _version_no_fGid = 34451;
       
        public static mdTableField version_no
        {
            get { return __MD.Fields[_version_no_fGid];}
        }
        public const string _version_no_fName = "version_no";        public const int _comments_fGid = 34452;
       
        public static mdTableField comments
        {
            get { return __MD.Fields[_comments_fGid];}
        }
        public const string _comments_fName = "comments";       
        public static mdTableField Solution_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Solution_fGid]);}
        }
        public const string _Solution_view__fName = "Solution_view_";       
        public static mdTableField Teacher_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Teacher_fGid]);}
        }
        public const string _Teacher_view__fName = "Teacher_view_";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";       
        public static mdTableField Solution_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Solution_fGid]);}
        }
        public const string _Solution_status__fName = "Solution_status_";       
        public static mdTableField Teacher_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Teacher_fGid]);}
        }
        public const string _Teacher_status__fName = "Teacher_status_";
    }
    /// <summary>
    /// Схема таблиці 'Види навчальних груп'
    /// </summary>
    public static class EduGroupKinds_TS_   
    {
        public const int __tGid = 15941;
        public const string __tName = "EduGroupKinds";
        static mdTable m_mdTable;
        static EduGroupKinds_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _gid_fGid = 34555;
       
        public static mdTableField gid
        {
            get { return __MD.Fields[_gid_fGid];}
        }
        public const string _gid_fName = "gid";        public const int _name_fGid = 34556;
       
        public static mdTableField name
        {
            get { return __MD.Fields[_name_fGid];}
        }
        public const string _name_fName = "name";        public const int _object_status_fGid = 34557;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _comments_fGid = 34558;
       
        public static mdTableField comments
        {
            get { return __MD.Fields[_comments_fGid];}
        }
        public const string _comments_fName = "comments";        public const int _version_no_fGid = 34559;
       
        public static mdTableField version_no
        {
            get { return __MD.Fields[_version_no_fGid];}
        }
        public const string _version_no_fName = "version_no";        public const int _GroupLevel_fGid = 34562;
       
        public static mdTableField GroupLevel
        {
            get { return __MD.Fields[_GroupLevel_fGid];}
        }
        public const string _GroupLevel_fName = "GroupLevel";        public const int _parent_group_fGid = 34616;
       
        public static mdTableField parent_group
        {
            get { return __MD.Fields[_parent_group_fGid];}
        }
        public const string _parent_group_fName = "parent_group";        public const int _Description_fGid = 34617;
       
        public static mdTableField Description
        {
            get { return __MD.Fields[_Description_fGid];}
        }
        public const string _Description_fName = "Description";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";       
        public static mdTableField parent_group_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_parent_group_fGid]);}
        }
        public const string _parent_group_view__fName = "parent_group_view_";       
        public static mdTableField parent_group_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_parent_group_fGid]);}
        }
        public const string _parent_group_status__fName = "parent_group_status_";
    }
    /// <summary>
    /// Схема таблиці 'Навчальні групи'
    /// </summary>
    public static class EduGroups_TS_   
    {
        public const int __tGid = 15900;
        public const string __tName = "EduGroups";
        static mdTable m_mdTable;
        static EduGroups_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _gid_fGid = 34067;
       
        public static mdTableField gid
        {
            get { return __MD.Fields[_gid_fGid];}
        }
        public const string _gid_fName = "gid";        public const int _object_status_fGid = 34068;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _name_fGid = 34069;
       
        public static mdTableField name
        {
            get { return __MD.Fields[_name_fGid];}
        }
        public const string _name_fName = "name";        public const int _comments_fGid = 34070;
       
        public static mdTableField comments
        {
            get { return __MD.Fields[_comments_fGid];}
        }
        public const string _comments_fName = "comments";        public const int _version_no_fGid = 34071;
       
        public static mdTableField version_no
        {
            get { return __MD.Fields[_version_no_fGid];}
        }
        public const string _version_no_fName = "version_no";        public const int _GroupLevel_fGid = 34072;
       
        public static mdTableField GroupLevel
        {
            get { return __MD.Fields[_GroupLevel_fGid];}
        }
        public const string _GroupLevel_fName = "GroupLevel";        public const int _StartYear_fGid = 34073;
       
        public static mdTableField StartYear
        {
            get { return __MD.Fields[_StartYear_fGid];}
        }
        public const string _StartYear_fName = "StartYear";        public const int _ResponsibleTeacher_fGid = 34495;
       
        public static mdTableField ResponsibleTeacher
        {
            get { return __MD.Fields[_ResponsibleTeacher_fGid];}
        }
        public const string _ResponsibleTeacher_fName = "ResponsibleTeacher";        public const int _GroupKind_fGid = 34561;
       
        public static mdTableField GroupKind
        {
            get { return __MD.Fields[_GroupKind_fGid];}
        }
        public const string _GroupKind_fName = "GroupKind";        public const int _PersonsGroup_fGid = 34563;
       
        public static mdTableField PersonsGroup
        {
            get { return __MD.Fields[_PersonsGroup_fGid];}
        }
        public const string _PersonsGroup_fName = "PersonsGroup";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";       
        public static mdTableField ResponsibleTeacher_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_ResponsibleTeacher_fGid]);}
        }
        public const string _ResponsibleTeacher_view__fName = "ResponsibleTeacher_view_";       
        public static mdTableField GroupKind_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_GroupKind_fGid]);}
        }
        public const string _GroupKind_view__fName = "GroupKind_view_";       
        public static mdTableField PersonsGroup_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_PersonsGroup_fGid]);}
        }
        public const string _PersonsGroup_view__fName = "PersonsGroup_view_";       
        public static mdTableField ResponsibleTeacher_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_ResponsibleTeacher_fGid]);}
        }
        public const string _ResponsibleTeacher_status__fName = "ResponsibleTeacher_status_";       
        public static mdTableField GroupKind_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_GroupKind_fGid]);}
        }
        public const string _GroupKind_status__fName = "GroupKind_status_";       
        public static mdTableField PersonsGroup_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_PersonsGroup_fGid]);}
        }
        public const string _PersonsGroup_status__fName = "PersonsGroup_status_";
    }
    /// <summary>
    /// Схема таблиці 'Вчителі навчальних груп'
    /// </summary>
    public static class EduGroupTeachers_TS_   
    {
        public const int __tGid = 15928;
        public const string __tName = "EduGroupTeachers";
        static mdTable m_mdTable;
        static EduGroupTeachers_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _Person_fGid = 34408;
       
        public static mdTableField Person
        {
            get { return __MD.Fields[_Person_fGid];}
        }
        public const string _Person_fName = "Person";        public const int _EduGroup_fGid = 34410;
       
        public static mdTableField EduGroup
        {
            get { return __MD.Fields[_EduGroup_fGid];}
        }
        public const string _EduGroup_fName = "EduGroup";        public const int _comments_fGid = 34412;
       
        public static mdTableField comments
        {
            get { return __MD.Fields[_comments_fGid];}
        }
        public const string _comments_fName = "comments";       
        public static mdTableField Person_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Person_fGid]);}
        }
        public const string _Person_view__fName = "Person_view_";       
        public static mdTableField EduGroup_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_EduGroup_fGid]);}
        }
        public const string _EduGroup_view__fName = "EduGroup_view_";       
        public static mdTableField EduGroup_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_EduGroup_fGid]);}
        }
        public const string _EduGroup_status__fName = "EduGroup_status_";       
        public static mdTableField Person_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Person_fGid]);}
        }
        public const string _Person_status__fName = "Person_status_";
    }
    /// <summary>
    /// Схема таблиці 'Навчальні уроки'
    /// </summary>
    public static class EduLessons_TS_   
    {
        public const int __tGid = 15901;
        public const string __tName = "EduLessons";
        static mdTable m_mdTable;
        static EduLessons_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _gid_fGid = 34074;
       
        public static mdTableField gid
        {
            get { return __MD.Fields[_gid_fGid];}
        }
        public const string _gid_fName = "gid";        public const int _Name_fGid = 34075;
       
        public static mdTableField Name
        {
            get { return __MD.Fields[_Name_fGid];}
        }
        public const string _Name_fName = "Name";        public const int _LessonTime_fGid = 34076;
       
        public static mdTableField LessonTime
        {
            get { return __MD.Fields[_LessonTime_fGid];}
        }
        public const string _LessonTime_fName = "LessonTime";        public const int _EduGroup_fGid = 34077;
       
        public static mdTableField EduGroup
        {
            get { return __MD.Fields[_EduGroup_fGid];}
        }
        public const string _EduGroup_fName = "EduGroup";        public const int _Description_fGid = 34078;
       
        public static mdTableField Description
        {
            get { return __MD.Fields[_Description_fGid];}
        }
        public const string _Description_fName = "Description";        public const int _version_no_fGid = 34079;
       
        public static mdTableField version_no
        {
            get { return __MD.Fields[_version_no_fGid];}
        }
        public const string _version_no_fName = "version_no";        public const int _comments_fGid = 34080;
       
        public static mdTableField comments
        {
            get { return __MD.Fields[_comments_fGid];}
        }
        public const string _comments_fName = "comments";        public const int _object_status_fGid = 34081;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _ResponsibleTeacher_fGid = 34082;
       
        public static mdTableField ResponsibleTeacher
        {
            get { return __MD.Fields[_ResponsibleTeacher_fGid];}
        }
        public const string _ResponsibleTeacher_fName = "ResponsibleTeacher";        public const int _EduTopic_fGid = 34494;
       
        public static mdTableField EduTopic
        {
            get { return __MD.Fields[_EduTopic_fGid];}
        }
        public const string _EduTopic_fName = "EduTopic";       
        public static mdTableField EduGroup_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_EduGroup_fGid]);}
        }
        public const string _EduGroup_view__fName = "EduGroup_view_";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";       
        public static mdTableField ResponsibleTeacher_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_ResponsibleTeacher_fGid]);}
        }
        public const string _ResponsibleTeacher_view__fName = "ResponsibleTeacher_view_";       
        public static mdTableField EduTopic_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_EduTopic_fGid]);}
        }
        public const string _EduTopic_view__fName = "EduTopic_view_";       
        public static mdTableField EduGroup_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_EduGroup_fGid]);}
        }
        public const string _EduGroup_status__fName = "EduGroup_status_";       
        public static mdTableField ResponsibleTeacher_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_ResponsibleTeacher_fGid]);}
        }
        public const string _ResponsibleTeacher_status__fName = "ResponsibleTeacher_status_";       
        public static mdTableField EduTopic_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_EduTopic_fGid]);}
        }
        public const string _EduTopic_status__fName = "EduTopic_status_";
    }
    /// <summary>
    /// Схема таблиці 'Навчальні матеріали'
    /// </summary>
    public static class EduMaterials_TS_   
    {
        public const int __tGid = 15933;
        public const string __tName = "EduMaterials";
        static mdTable m_mdTable;
        static EduMaterials_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _MaterialURL_fGid = 34065;
       
        public static mdTableField MaterialURL
        {
            get { return __MD.Fields[_MaterialURL_fGid];}
        }
        public const string _MaterialURL_fName = "MaterialURL";        public const int _gid_fGid = 34464;
       
        public static mdTableField gid
        {
            get { return __MD.Fields[_gid_fGid];}
        }
        public const string _gid_fName = "gid";        public const int _object_status_fGid = 34465;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _MaterialsText_fGid = 34468;
       
        public static mdTableField MaterialsText
        {
            get { return __MD.Fields[_MaterialsText_fGid];}
        }
        public const string _MaterialsText_fName = "MaterialsText";        public const int _Caption_fGid = 34470;
       
        public static mdTableField Caption
        {
            get { return __MD.Fields[_Caption_fGid];}
        }
        public const string _Caption_fName = "Caption";        public const int _Source_fGid = 34471;
       
        public static mdTableField Source
        {
            get { return __MD.Fields[_Source_fGid];}
        }
        public const string _Source_fName = "Source";        public const int _version_no_fGid = 34477;
       
        public static mdTableField version_no
        {
            get { return __MD.Fields[_version_no_fGid];}
        }
        public const string _version_no_fName = "version_no";        public const int _MaterialType_fGid = 34496;
       
        public static mdTableField MaterialType
        {
            get { return __MD.Fields[_MaterialType_fGid];}
        }
        public const string _MaterialType_fName = "MaterialType";        public const int _Topic_fGid = 34497;
       
        public static mdTableField Topic
        {
            get { return __MD.Fields[_Topic_fGid];}
        }
        public const string _Topic_fName = "Topic";        public const int _comments_fGid = 34498;
       
        public static mdTableField comments
        {
            get { return __MD.Fields[_comments_fGid];}
        }
        public const string _comments_fName = "comments";        public const int _reg_date_fGid = 34539;
       
        public static mdTableField reg_date
        {
            get { return __MD.Fields[_reg_date_fGid];}
        }
        public const string _reg_date_fName = "reg_date";        public const int _reg_author_fGid = 34540;
       
        public static mdTableField reg_author
        {
            get { return __MD.Fields[_reg_author_fGid];}
        }
        public const string _reg_author_fName = "reg_author";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";       
        public static mdTableField Topic_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Topic_fGid]);}
        }
        public const string _Topic_view__fName = "Topic_view_";       
        public static mdTableField reg_author_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_reg_author_fGid]);}
        }
        public const string _reg_author_view__fName = "reg_author_view_";       
        public static mdTableField Topic_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Topic_fGid]);}
        }
        public const string _Topic_status__fName = "Topic_status_";       
        public static mdTableField reg_author_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_reg_author_fGid]);}
        }
        public const string _reg_author_status__fName = "reg_author_status_";
    }
    /// <summary>
    /// Схема таблиці 'Набір навчальних матеріалів'
    /// </summary>
    public static class EduMaterialsList_TS_   
    {
        public const int __tGid = 15932;
        public const string __tName = "EduMaterialsList";
        static mdTable m_mdTable;
        static EduMaterialsList_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _gid_fGid = 34453;
       
        public static mdTableField gid
        {
            get { return __MD.Fields[_gid_fGid];}
        }
        public const string _gid_fName = "gid";        public const int _Topic_fGid = 34454;
       
        public static mdTableField Topic
        {
            get { return __MD.Fields[_Topic_fGid];}
        }
        public const string _Topic_fName = "Topic";        public const int _object_status_fGid = 34455;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _CreatingTime_fGid = 34456;
       
        public static mdTableField CreatingTime
        {
            get { return __MD.Fields[_CreatingTime_fGid];}
        }
        public const string _CreatingTime_fName = "CreatingTime";        public const int _EduGroup_fGid = 34457;
       
        public static mdTableField EduGroup
        {
            get { return __MD.Fields[_EduGroup_fGid];}
        }
        public const string _EduGroup_fName = "EduGroup";        public const int _Author_fGid = 34458;
       
        public static mdTableField Author
        {
            get { return __MD.Fields[_Author_fGid];}
        }
        public const string _Author_fName = "Author";        public const int _Lesson_fGid = 34459;
       
        public static mdTableField Lesson
        {
            get { return __MD.Fields[_Lesson_fGid];}
        }
        public const string _Lesson_fName = "Lesson";        public const int _ListKind_fGid = 34460;
       
        public static mdTableField ListKind
        {
            get { return __MD.Fields[_ListKind_fGid];}
        }
        public const string _ListKind_fName = "ListKind";        public const int _comments_fGid = 34499;
       
        public static mdTableField comments
        {
            get { return __MD.Fields[_comments_fGid];}
        }
        public const string _comments_fName = "comments";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";       
        public static mdTableField EduGroup_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_EduGroup_fGid]);}
        }
        public const string _EduGroup_view__fName = "EduGroup_view_";       
        public static mdTableField Author_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Author_fGid]);}
        }
        public const string _Author_view__fName = "Author_view_";       
        public static mdTableField Lesson_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Lesson_fGid]);}
        }
        public const string _Lesson_view__fName = "Lesson_view_";       
        public static mdTableField ListKind_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_ListKind_fGid]);}
        }
        public const string _ListKind_view__fName = "ListKind_view_";       
        public static mdTableField EduGroup_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_EduGroup_fGid]);}
        }
        public const string _EduGroup_status__fName = "EduGroup_status_";       
        public static mdTableField Author_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Author_fGid]);}
        }
        public const string _Author_status__fName = "Author_status_";       
        public static mdTableField Lesson_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Lesson_fGid]);}
        }
        public const string _Lesson_status__fName = "Lesson_status_";       
        public static mdTableField ListKind_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_ListKind_fGid]);}
        }
        public const string _ListKind_status__fName = "ListKind_status_";
    }
    /// <summary>
    /// Схема таблиці 'Додаткові матеріали в наборі завдань'
    /// </summary>
    public static class EduMaterialsList_tp_TS_   
    {
        public const int __tGid = 15934;
        public const string __tName = "EduMaterialsList_tp";
        static mdTable m_mdTable;
        static EduMaterialsList_tp_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _MaxMark_fGid = 34346;
       
        public static mdTableField MaxMark
        {
            get { return __MD.Fields[_MaxMark_fGid];}
        }
        public const string _MaxMark_fName = "MaxMark";        public const int _MaterialItem_fGid = 34472;
       
        public static mdTableField MaterialItem
        {
            get { return __MD.Fields[_MaterialItem_fGid];}
        }
        public const string _MaterialItem_fName = "MaterialItem";        public const int _MaterialsList_fGid = 34474;
       
        public static mdTableField MaterialsList
        {
            get { return __MD.Fields[_MaterialsList_fGid];}
        }
        public const string _MaterialsList_fName = "MaterialsList";        public const int _Ordinal_fGid = 34476;
       
        public static mdTableField Ordinal
        {
            get { return __MD.Fields[_Ordinal_fGid];}
        }
        public const string _Ordinal_fName = "Ordinal";        public const int _comments_fGid = 34478;
       
        public static mdTableField comments
        {
            get { return __MD.Fields[_comments_fGid];}
        }
        public const string _comments_fName = "comments";       
        public static mdTableField MaterialItem_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_MaterialItem_fGid]);}
        }
        public const string _MaterialItem_view__fName = "MaterialItem_view_";       
        public static mdTableField MaterialsList_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_MaterialsList_fGid]);}
        }
        public const string _MaterialsList_view__fName = "MaterialsList_view_";       
        public static mdTableField MaterialItem_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_MaterialItem_fGid]);}
        }
        public const string _MaterialItem_status__fName = "MaterialItem_status_";       
        public static mdTableField MaterialsList_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_MaterialsList_fGid]);}
        }
        public const string _MaterialsList_status__fName = "MaterialsList_status_";
    }
    /// <summary>
    /// Схема таблиці 'Види наборів навчальних матеріалів'
    /// </summary>
    public static class EduMaterialsListKinds_TS_   
    {
        public const int __tGid = 15919;
        public const string __tName = "EduMaterialsListKinds";
        static mdTable m_mdTable;
        static EduMaterialsListKinds_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _Name_fGid = 34430;
       
        public static mdTableField Name
        {
            get { return __MD.Fields[_Name_fGid];}
        }
        public const string _Name_fName = "Name";        public const int _gid_fGid = 34432;
       
        public static mdTableField gid
        {
            get { return __MD.Fields[_gid_fGid];}
        }
        public const string _gid_fName = "gid";        public const int _MaxMark_fGid = 34434;
       
        public static mdTableField MaxMark
        {
            get { return __MD.Fields[_MaxMark_fGid];}
        }
        public const string _MaxMark_fName = "MaxMark";        public const int _version_no_fGid = 34447;
       
        public static mdTableField version_no
        {
            get { return __MD.Fields[_version_no_fGid];}
        }
        public const string _version_no_fName = "version_no";        public const int _comments_fGid = 34448;
       
        public static mdTableField comments
        {
            get { return __MD.Fields[_comments_fGid];}
        }
        public const string _comments_fName = "comments";        public const int _object_status_fGid = 34449;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";
    }
    /// <summary>
    /// Схема таблиці 'Учні навчальних класів'
    /// </summary>
    public static class EduStudents_TS_   
    {
        public const int __tGid = 15903;
        public const string __tName = "EduStudents";
        static mdTable m_mdTable;
        static EduStudents_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _Person_fGid = 34089;
       
        public static mdTableField Person
        {
            get { return __MD.Fields[_Person_fGid];}
        }
        public const string _Person_fName = "Person";        public const int _EduGroup_fGid = 34090;
       
        public static mdTableField EduGroup
        {
            get { return __MD.Fields[_EduGroup_fGid];}
        }
        public const string _EduGroup_fName = "EduGroup";       
        public static mdTableField Full_Name
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Person_fGid]);}
        }
        public const string _Full_Name_fName = "Full_Name";        public const int _comments_fGid = 34093;
       
        public static mdTableField comments
        {
            get { return __MD.Fields[_comments_fGid];}
        }
        public const string _comments_fName = "comments";        public const int _group_status_fGid = 34444;
       
        public static mdTableField group_status
        {
            get { return __MD.Fields[_group_status_fGid];}
        }
        public const string _group_status_fName = "group_status";       
        public static mdTableField EduGroup_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_EduGroup_fGid]);}
        }
        public const string _EduGroup_view__fName = "EduGroup_view_";       
        public static mdTableField group_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_group_status_fGid]);}
        }
        public const string _group_status_view__fName = "group_status_view_";       
        public static mdTableField Person_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Person_fGid]);}
        }
        public const string _Person_status__fName = "Person_status_";
    }
    /// <summary>
    /// Схема таблиці 'Рішення завдання'
    /// </summary>
    public static class EduTaskSolution_TS_   
    {
        public const int __tGid = 15927;
        public const string __tName = "EduTaskSolution";
        static mdTable m_mdTable;
        static EduTaskSolution_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _gid_fGid = 34387;
       
        public static mdTableField gid
        {
            get { return __MD.Fields[_gid_fGid];}
        }
        public const string _gid_fName = "gid";        public const int _Author_fGid = 34389;
       
        public static mdTableField Author
        {
            get { return __MD.Fields[_Author_fGid];}
        }
        public const string _Author_fName = "Author";        public const int _Mark_fGid = 34393;
       
        public static mdTableField Mark
        {
            get { return __MD.Fields[_Mark_fGid];}
        }
        public const string _Mark_fName = "Mark";        public const int _reg_date_fGid = 34395;
       
        public static mdTableField reg_date
        {
            get { return __MD.Fields[_reg_date_fGid];}
        }
        public const string _reg_date_fName = "reg_date";        public const int _Solution_fGid = 34435;
       
        public static mdTableField Solution
        {
            get { return __MD.Fields[_Solution_fGid];}
        }
        public const string _Solution_fName = "Solution";        public const int _version_no_fGid = 34446;
       
        public static mdTableField version_no
        {
            get { return __MD.Fields[_version_no_fGid];}
        }
        public const string _version_no_fName = "version_no";        public const int _object_status_fGid = 34480;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _Answer_fGid = 34483;
       
        public static mdTableField Answer
        {
            get { return __MD.Fields[_Answer_fGid];}
        }
        public const string _Answer_fName = "Answer";        public const int _Group_fGid = 34486;
       
        public static mdTableField Group
        {
            get { return __MD.Fields[_Group_fGid];}
        }
        public const string _Group_fName = "Group";       
        public static mdTableField Author_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Author_fGid]);}
        }
        public const string _Author_view__fName = "Author_view_";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";       
        public static mdTableField Group_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Group_fGid]);}
        }
        public const string _Group_view__fName = "Group_view_";       
        public static mdTableField Author_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Author_fGid]);}
        }
        public const string _Author_status__fName = "Author_status_";       
        public static mdTableField Group_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Group_fGid]);}
        }
        public const string _Group_status__fName = "Group_status_";
    }
    /// <summary>
    /// Схема таблиці 'Навчальні теми'
    /// </summary>
    public static class EduTopics_TS_   
    {
        public const int __tGid = 15898;
        public const string __tName = "EduTopics";
        static mdTable m_mdTable;
        static EduTopics_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _gid_fGid = 34051;
       
        public static mdTableField gid
        {
            get { return __MD.Fields[_gid_fGid];}
        }
        public const string _gid_fName = "gid";        public const int _name_fGid = 34052;
       
        public static mdTableField name
        {
            get { return __MD.Fields[_name_fGid];}
        }
        public const string _name_fName = "name";        public const int _comments_fGid = 34053;
       
        public static mdTableField comments
        {
            get { return __MD.Fields[_comments_fGid];}
        }
        public const string _comments_fName = "comments";        public const int _object_status_fGid = 34054;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _version_no_fGid = 34055;
       
        public static mdTableField version_no
        {
            get { return __MD.Fields[_version_no_fGid];}
        }
        public const string _version_no_fName = "version_no";        public const int _Description_fGid = 34056;
       
        public static mdTableField Description
        {
            get { return __MD.Fields[_Description_fGid];}
        }
        public const string _Description_fName = "Description";        public const int _parent_topic_fGid = 34057;
       
        public static mdTableField parent_topic
        {
            get { return __MD.Fields[_parent_topic_fGid];}
        }
        public const string _parent_topic_fName = "parent_topic";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";       
        public static mdTableField parent_topic_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_parent_topic_fGid]);}
        }
        public const string _parent_topic_view__fName = "parent_topic_view_";       
        public static mdTableField parent_topic_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_parent_topic_fGid]);}
        }
        public const string _parent_topic_status__fName = "parent_topic_status_";
    }
    /// <summary>
    /// Схема таблиці 'Балансова відомість'
    /// </summary>
    public static class BalanceView_TS_   
    {
        public const int __tGid = 15822;
        public const string __tName = "BalanceView";
        static mdTable m_mdTable;
        static BalanceView_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _BalanseAccount_fGid = 32917;
       
        public static mdTableField BalanseAccount
        {
            get { return __MD.Fields[_BalanseAccount_fGid];}
        }
        public const string _BalanseAccount_fName = "BalanseAccount";        public const int _subkonto_fGid = 32918;
       
        public static mdTableField subkonto
        {
            get { return __MD.Fields[_subkonto_fGid];}
        }
        public const string _subkonto_fName = "subkonto";        public const int _recorder_fGid = 32919;
       
        public static mdTableField recorder
        {
            get { return __MD.Fields[_recorder_fGid];}
        }
        public const string _recorder_fName = "recorder";        public const int _reg_date_fGid = 32920;
       
        public static mdTableField reg_date
        {
            get { return __MD.Fields[_reg_date_fGid];}
        }
        public const string _reg_date_fName = "reg_date";        public const int _amount_initial_rest_fGid = 32921;
       
        public static mdTableField amount_initial_rest
        {
            get { return __MD.Fields[_amount_initial_rest_fGid];}
        }
        public const string _amount_initial_rest_fName = "amount_initial_rest";        public const int _amount_initial_rest_debit_fGid = 32922;
       
        public static mdTableField amount_initial_rest_debit
        {
            get { return __MD.Fields[_amount_initial_rest_debit_fGid];}
        }
        public const string _amount_initial_rest_debit_fName = "amount_initial_rest_debit";        public const int _amount_initial_rest_credit_fGid = 32923;
       
        public static mdTableField amount_initial_rest_credit
        {
            get { return __MD.Fields[_amount_initial_rest_credit_fGid];}
        }
        public const string _amount_initial_rest_credit_fName = "amount_initial_rest_credit";        public const int _amount_in_fGid = 32924;
       
        public static mdTableField amount_in
        {
            get { return __MD.Fields[_amount_in_fGid];}
        }
        public const string _amount_in_fName = "amount_in";        public const int _amount_out_fGid = 32925;
       
        public static mdTableField amount_out
        {
            get { return __MD.Fields[_amount_out_fGid];}
        }
        public const string _amount_out_fName = "amount_out";        public const int _amount_rest_fGid = 32926;
       
        public static mdTableField amount_rest
        {
            get { return __MD.Fields[_amount_rest_fGid];}
        }
        public const string _amount_rest_fName = "amount_rest";        public const int _amount_rest_debit_fGid = 32927;
       
        public static mdTableField amount_rest_debit
        {
            get { return __MD.Fields[_amount_rest_debit_fGid];}
        }
        public const string _amount_rest_debit_fName = "amount_rest_debit";        public const int _amount_rest_credit_fGid = 32928;
       
        public static mdTableField amount_rest_credit
        {
            get { return __MD.Fields[_amount_rest_credit_fGid];}
        }
        public const string _amount_rest_credit_fName = "amount_rest_credit";        public const int _Code_fGid = 32929;
       
        public static mdTableField Code
        {
            get { return __MD.Fields[_Code_fGid];}
        }
        public const string _Code_fName = "Code";        public const int _MonthOfDate_fGid = 32930;
       
        public static mdTableField MonthOfDate
        {
            get { return __MD.Fields[_MonthOfDate_fGid];}
        }
        public const string _MonthOfDate_fName = "MonthOfDate";        public const int _subkonto_tid_fGid = 32932;
       
        public static mdTableField subkonto_tid
        {
            get { return __MD.Fields[_subkonto_tid_fGid];}
        }
        public const string _subkonto_tid_fName = "subkonto_tid";
    }
    /// <summary>
    /// Схема таблиці 'Рахунки обліку'
    /// </summary>
    public static class BalanseAccounts_TS_   
    {
        public const int __tGid = 1585;
        public const string __tName = "BalanseAccounts";
        static mdTable m_mdTable;
        static BalanseAccounts_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
       
        public static mdDefaultDataRow _dr_NA
        {
            get { return __MD.DefaultDataRows[343];}
        }       
        public const int _dr_NA_Gid = 10;
       
        public static mdDefaultDataRow _dr_Materials
        {
            get { return __MD.DefaultDataRows[344];}
        }       
        public const int _dr_Materials_Gid = 20;
       
        public static mdDefaultDataRow _dr_NA_Investments
        {
            get { return __MD.DefaultDataRows[346];}
        }       
        public const int _dr_NA_Investments_Gid = 15;
       
        public static mdDefaultDataRow _dr_Cash
        {
            get { return __MD.DefaultDataRows[350];}
        }       
        public const int _dr_Cash_Gid = 30;
       
        public static mdDefaultDataRow _dr_NonCash_Money
        {
            get { return __MD.DefaultDataRows[351];}
        }       
        public const int _dr_NonCash_Money_Gid = 31;
       
        public static mdDefaultDataRow _dr_Settlements_with_suppliers
        {
            get { return __MD.DefaultDataRows[354];}
        }       
        public const int _dr_Settlements_with_suppliers_Gid = 63;
       
        public static mdDefaultDataRow _dr_Salary_Settlements
        {
            get { return __MD.DefaultDataRows[355];}
        }       
        public const int _dr_Salary_Settlements_Gid = 66;
       
        public static mdDefaultDataRow _dr_VAT_duty
        {
            get { return __MD.DefaultDataRows[356];}
        }       
        public const int _dr_VAT_duty_Gid = 64;
       
        public static mdDefaultDataRow _dr_Internal_Settlements
        {
            get { return __MD.DefaultDataRows[358];}
        }       
        public const int _dr_Internal_Settlements_Gid = 68;
       
        public static mdDefaultDataRow _dr_Credits
        {
            get { return __MD.DefaultDataRows[359];}
        }       
        public const int _dr_Credits_Gid = 60;
       
        public static mdDefaultDataRow _dr_AdministrativeCosts
        {
            get { return __MD.DefaultDataRows[361];}
        }       
        public const int _dr_AdministrativeCosts_Gid = 92;
       
        public static mdDefaultDataRow _dr_GeneralProductionCosts
        {
            get { return __MD.DefaultDataRows[364];}
        }       
        public const int _dr_GeneralProductionCosts_Gid = 91;
       
        public static mdDefaultDataRow _dr_SalaryCosts
        {
            get { return __MD.DefaultDataRows[365];}
        }       
        public const int _dr_SalaryCosts_Gid = 93;
       
        public static mdDefaultDataRow _dr_Initial_Rests
        {
            get { return __MD.DefaultDataRows[383];}
        }       
        public const int _dr_Initial_Rests_Gid = 1;
       
        public static mdDefaultDataRow _dr_OtherCosts
        {
            get { return __MD.DefaultDataRows[386];}
        }       
        public const int _dr_OtherCosts_Gid = 94;
       
        public static mdDefaultDataRow _dr_TargetedAssistance
        {
            get { return __MD.DefaultDataRows[1026];}
        }       
        public const int _dr_TargetedAssistance_Gid = 48;
        public const int _gid_fGid = 9075;
       
        public static mdTableField gid
        {
            get { return __MD.Fields[_gid_fGid];}
        }
        public const string _gid_fName = "gid";        public const int _name_fGid = 9076;
       
        public static mdTableField name
        {
            get { return __MD.Fields[_name_fGid];}
        }
        public const string _name_fName = "name";        public const int _comments_fGid = 9077;
       
        public static mdTableField comments
        {
            get { return __MD.Fields[_comments_fGid];}
        }
        public const string _comments_fName = "comments";        public const int _object_status_fGid = 9092;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _version_no_fGid = 9093;
       
        public static mdTableField version_no
        {
            get { return __MD.Fields[_version_no_fGid];}
        }
        public const string _version_no_fName = "version_no";        public const int _Code_fGid = 9139;
       
        public static mdTableField Code
        {
            get { return __MD.Fields[_Code_fGid];}
        }
        public const string _Code_fName = "Code";        public const int _RegisterType_fGid = 9147;
       
        public static mdTableField RegisterType
        {
            get { return __MD.Fields[_RegisterType_fGid];}
        }
        public const string _RegisterType_fName = "RegisterType";        public const int _representation_fGid = 9397;
       
        public static mdTableField representation
        {
            get { return __MD.Fields[_representation_fGid];}
        }
        public const string _representation_fName = "representation";        public const int _parent_BA_fGid = 28699;
       
        public static mdTableField parent_BA
        {
            get { return __MD.Fields[_parent_BA_fGid];}
        }
        public const string _parent_BA_fName = "parent_BA";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";       
        public static mdTableField parent_BA_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_parent_BA_fGid]);}
        }
        public const string _parent_BA_view__fName = "parent_BA_view_";       
        public static mdTableField parent_BA_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_parent_BA_fGid]);}
        }
        public const string _parent_BA_status__fName = "parent_BA_status_";
    }
    /// <summary>
    /// Схема таблиці 'Банківські установи'
    /// </summary>
    public static class Banks_TS_   
    {
        public const int __tGid = 1566;
        public const string __tName = "Banks";
        static mdTable m_mdTable;
        static Banks_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _object_status_fGid = 5942;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _version_no_fGid = 5943;
       
        public static mdTableField version_no
        {
            get { return __MD.Fields[_version_no_fGid];}
        }
        public const string _version_no_fName = "version_no";        public const int _gid_fGid = 8817;
       
        public static mdTableField gid
        {
            get { return __MD.Fields[_gid_fGid];}
        }
        public const string _gid_fName = "gid";        public const int _Name_fGid = 8818;
       
        public static mdTableField Name
        {
            get { return __MD.Fields[_Name_fGid];}
        }
        public const string _Name_fName = "Name";        public const int _MFO_fGid = 8819;
       
        public static mdTableField MFO
        {
            get { return __MD.Fields[_MFO_fGid];}
        }
        public const string _MFO_fName = "MFO";        public const int _Address_fGid = 8820;
       
        public static mdTableField Address
        {
            get { return __MD.Fields[_Address_fGid];}
        }
        public const string _Address_fName = "Address";        public const int _comments_fGid = 8821;
       
        public static mdTableField comments
        {
            get { return __MD.Fields[_comments_fGid];}
        }
        public const string _comments_fName = "comments";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";
    }
    /// <summary>
    /// Схема таблиці 'Перелік членів комісії за бюджетними документами'
    /// </summary>
    public static class BJ_commission_members_tp_TS_   
    {
        public const int __tGid = 15727;
        public const string __tName = "BJ_commission_members_tp";
        static mdTable m_mdTable;
        static BJ_commission_members_tp_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _document_fGid = 30826;
       
        public static mdTableField document
        {
            get { return __MD.Fields[_document_fGid];}
        }
        public const string _document_fName = "document";        public const int _ordinal_fGid = 30827;
       
        public static mdTableField ordinal
        {
            get { return __MD.Fields[_ordinal_fGid];}
        }
        public const string _ordinal_fName = "ordinal";        public const int _Person_fGid = 30828;
       
        public static mdTableField Person
        {
            get { return __MD.Fields[_Person_fGid];}
        }
        public const string _Person_fName = "Person";        public const int _ApproveState_fGid = 30829;
       
        public static mdTableField ApproveState
        {
            get { return __MD.Fields[_ApproveState_fGid];}
        }
        public const string _ApproveState_fName = "ApproveState";        public const int _Comments_fGid = 30830;
       
        public static mdTableField Comments
        {
            get { return __MD.Fields[_Comments_fGid];}
        }
        public const string _Comments_fName = "Comments";       
        public static mdTableField document_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_document_fGid]);}
        }
        public const string _document_view__fName = "document_view_";       
        public static mdTableField Person_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Person_fGid]);}
        }
        public const string _Person_view__fName = "Person_view_";       
        public static mdTableField document_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_document_fGid]);}
        }
        public const string _document_status__fName = "document_status_";       
        public static mdTableField Person_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Person_fGid]);}
        }
        public const string _Person_status__fName = "Person_status_";
    }
    /// <summary>
    /// Схема таблиці 'Грошові рахунки організацій'
    /// </summary>
    public static class money_accounts_TS_   
    {
        public const int __tGid = 205;
        public const string __tName = "money_accounts";
        static mdTable m_mdTable;
        static money_accounts_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _gid_fGid = 1824;
       
        public static mdTableField gid
        {
            get { return __MD.Fields[_gid_fGid];}
        }
        public const string _gid_fName = "gid";        public const int _name_fGid = 1825;
       
        public static mdTableField name
        {
            get { return __MD.Fields[_name_fGid];}
        }
        public const string _name_fName = "name";        public const int _non_cash_fGid = 1826;
       
        public static mdTableField non_cash
        {
            get { return __MD.Fields[_non_cash_fGid];}
        }
        public const string _non_cash_fName = "non_cash";        public const int _Company_fGid = 1827;
       
        public static mdTableField Company
        {
            get { return __MD.Fields[_Company_fGid];}
        }
        public const string _Company_fName = "Company";        public const int _account_fGid = 1828;
       
        public static mdTableField account
        {
            get { return __MD.Fields[_account_fGid];}
        }
        public const string _account_fName = "account";       
        public static mdTableField MFO
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Bank_fGid]);}
        }
        public const string _MFO_fName = "MFO";        public const int _name_bank_fGid = 1830;
       
        public static mdTableField name_bank
        {
            get { return __MD.Fields[_name_bank_fGid];}
        }
        public const string _name_bank_fName = "name_bank";        public const int _adress_bank_fGid = 1831;
       
        public static mdTableField adress_bank
        {
            get { return __MD.Fields[_adress_bank_fGid];}
        }
        public const string _adress_bank_fName = "adress_bank";        public const int _object_status_fGid = 1853;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _money_account_type_fGid = 2717;
       
        public static mdTableField money_account_type
        {
            get { return __MD.Fields[_money_account_type_fGid];}
        }
        public const string _money_account_type_fName = "money_account_type";        public const int _version_no_fGid = 3009;
       
        public static mdTableField version_no
        {
            get { return __MD.Fields[_version_no_fGid];}
        }
        public const string _version_no_fName = "version_no";        public const int _is_main_bank_account_fGid = 3812;
       
        public static mdTableField is_main_bank_account
        {
            get { return __MD.Fields[_is_main_bank_account_fGid];}
        }
        public const string _is_main_bank_account_fName = "is_main_bank_account";        public const int _main_bank_account_fGid = 3813;
       
        public static mdTableField main_bank_account
        {
            get { return __MD.Fields[_main_bank_account_fGid];}
        }
        public const string _main_bank_account_fName = "main_bank_account";        public const int _Bank_account_view_fGid = 3922;
       
        public static mdTableField Bank_account_view
        {
            get { return __MD.Fields[_Bank_account_view_fGid];}
        }
        public const string _Bank_account_view_fName = "Bank_account_view";        public const int _Bank_fGid = 5929;
       
        public static mdTableField Bank
        {
            get { return __MD.Fields[_Bank_fGid];}
        }
        public const string _Bank_fName = "Bank";        public const int _BalanseAccount_fGid = 9146;
       
        public static mdTableField BalanseAccount
        {
            get { return __MD.Fields[_BalanseAccount_fGid];}
        }
        public const string _BalanseAccount_fName = "BalanseAccount";        public const int _asid_fGid = 20910;
       
        public static mdTableField asid
        {
            get { return __MD.Fields[_asid_fGid];}
        }
        public const string _asid_fName = "asid";        public const int _amount_rest_fGid = 23487;
       
        public static mdTableField amount_rest
        {
            get { return __MD.Fields[_amount_rest_fGid];}
        }
        public const string _amount_rest_fName = "amount_rest";        public const int _edrpou_code_fGid = 27611;
       
        public static mdTableField edrpou_code
        {
            get { return __MD.Fields[_edrpou_code_fGid];}
        }
        public const string _edrpou_code_fName = "edrpou_code";        public const int _FinProject_fGid = 29218;
       
        public static mdTableField FinProject
        {
            get { return __MD.Fields[_FinProject_fGid];}
        }
        public const string _FinProject_fName = "FinProject";       
        public static mdTableField Company_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Company_fGid]);}
        }
        public const string _Company_view__fName = "Company_view_";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";       
        public static mdTableField main_bank_account_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_main_bank_account_fGid]);}
        }
        public const string _main_bank_account_view__fName = "main_bank_account_view_";       
        public static mdTableField BalanseAccount_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_BalanseAccount_fGid]);}
        }
        public const string _BalanseAccount_view__fName = "BalanseAccount_view_";       
        public static mdTableField FinProject_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_FinProject_fGid]);}
        }
        public const string _FinProject_view__fName = "FinProject_view_";       
        public static mdTableField Company_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Company_fGid]);}
        }
        public const string _Company_status__fName = "Company_status_";       
        public static mdTableField main_bank_account_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_main_bank_account_fGid]);}
        }
        public const string _main_bank_account_status__fName = "main_bank_account_status_";       
        public static mdTableField Bank_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Bank_fGid]);}
        }
        public const string _Bank_status__fName = "Bank_status_";       
        public static mdTableField BalanseAccount_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_BalanseAccount_fGid]);}
        }
        public const string _BalanseAccount_status__fName = "BalanseAccount_status_";       
        public static mdTableField FinProject_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_FinProject_fGid]);}
        }
        public const string _FinProject_status__fName = "FinProject_status_";
    }
    /// <summary>
    /// Схема таблиці 'Залишки грошей'
    /// </summary>
    public static class money_rests_TS_   
    {
        public const int __tGid = 1186;
        public const string __tName = "money_rests";
        static mdTable m_mdTable;
        static money_rests_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _reg_date_fGid = 5983;
       
        public static mdTableField reg_date
        {
            get { return __MD.Fields[_reg_date_fGid];}
        }
        public const string _reg_date_fName = "reg_date";        public const int _recorder_fGid = 5984;
       
        public static mdTableField recorder
        {
            get { return __MD.Fields[_recorder_fGid];}
        }
        public const string _recorder_fName = "recorder";        public const int _action_number_fGid = 5985;
       
        public static mdTableField action_number
        {
            get { return __MD.Fields[_action_number_fGid];}
        }
        public const string _action_number_fName = "action_number";        public const int _is_outgo_fGid = 5986;
       
        public static mdTableField is_outgo
        {
            get { return __MD.Fields[_is_outgo_fGid];}
        }
        public const string _is_outgo_fName = "is_outgo";        public const int _money_account_fGid = 5988;
       
        public static mdTableField money_account
        {
            get { return __MD.Fields[_money_account_fGid];}
        }
        public const string _money_account_fName = "money_account";        public const int _amount_fGid = 5991;
       
        public static mdTableField amount
        {
            get { return __MD.Fields[_amount_fGid];}
        }
        public const string _amount_fName = "amount";        public const int _recorder_tid_fGid = 6021;
       
        public static mdTableField recorder_tid
        {
            get { return __MD.Fields[_recorder_tid_fGid];}
        }
        public const string _recorder_tid_fName = "recorder_tid";        public const int _Resource_Flow_Item_fGid = 9097;
       
        public static mdTableField Resource_Flow_Item
        {
            get { return __MD.Fields[_Resource_Flow_Item_fGid];}
        }
        public const string _Resource_Flow_Item_fName = "Resource_Flow_Item";        public const int _payment_comment_fGid = 27095;
       
        public static mdTableField payment_comment
        {
            get { return __MD.Fields[_payment_comment_fGid];}
        }
        public const string _payment_comment_fName = "payment_comment";        public const int _contragent_kf_fGid = 27097;
       
        public static mdTableField contragent_kf
        {
            get { return __MD.Fields[_contragent_kf_fGid];}
        }
        public const string _contragent_kf_fName = "contragent_kf";        public const int _FinProject_fGid = 27100;
       
        public static mdTableField FinProject
        {
            get { return __MD.Fields[_FinProject_fGid];}
        }
        public const string _FinProject_fName = "FinProject";        public const int _SheduledPayment_fGid = 29963;
       
        public static mdTableField SheduledPayment
        {
            get { return __MD.Fields[_SheduledPayment_fGid];}
        }
        public const string _SheduledPayment_fName = "SheduledPayment";        public const int _BaseDoc_fGid = 31002;
       
        public static mdTableField BaseDoc
        {
            get { return __MD.Fields[_BaseDoc_fGid];}
        }
        public const string _BaseDoc_fName = "BaseDoc";       
        public static mdTableField money_account_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_money_account_fGid]);}
        }
        public const string _money_account_view__fName = "money_account_view_";       
        public static mdTableField Resource_Flow_Item_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Resource_Flow_Item_fGid]);}
        }
        public const string _Resource_Flow_Item_view__fName = "Resource_Flow_Item_view_";       
        public static mdTableField contragent_kf_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_contragent_kf_fGid]);}
        }
        public const string _contragent_kf_view__fName = "contragent_kf_view_";       
        public static mdTableField FinProject_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_FinProject_fGid]);}
        }
        public const string _FinProject_view__fName = "FinProject_view_";       
        public static mdTableField SheduledPayment_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_SheduledPayment_fGid]);}
        }
        public const string _SheduledPayment_view__fName = "SheduledPayment_view_";       
        public static mdTableField BaseDoc_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_BaseDoc_fGid]);}
        }
        public const string _BaseDoc_view__fName = "BaseDoc_view_";       
        public static mdTableField recorder_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_recorder_fGid]);}
        }
        public const string _recorder_view__fName = "recorder_view_";        public const int _recorder_view_documents__fGid = 99000092;
       
        public static mdTableField recorder_view_documents_
        {
            get { return __MD.Fields[_recorder_view_documents__fGid];}
        }
        public const string _recorder_view_documents__fName = "recorder_view_documents_";       
        public static mdTableField money_account_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_money_account_fGid]);}
        }
        public const string _money_account_status__fName = "money_account_status_";       
        public static mdTableField Resource_Flow_Item_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Resource_Flow_Item_fGid]);}
        }
        public const string _Resource_Flow_Item_status__fName = "Resource_Flow_Item_status_";       
        public static mdTableField contragent_kf_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_contragent_kf_fGid]);}
        }
        public const string _contragent_kf_status__fName = "contragent_kf_status_";       
        public static mdTableField FinProject_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_FinProject_fGid]);}
        }
        public const string _FinProject_status__fName = "FinProject_status_";       
        public static mdTableField SheduledPayment_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_SheduledPayment_fGid]);}
        }
        public const string _SheduledPayment_status__fName = "SheduledPayment_status_";       
        public static mdTableField BaseDoc_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_BaseDoc_fGid]);}
        }
        public const string _BaseDoc_status__fName = "BaseDoc_status_";
    }
    /// <summary>
    /// Схема таблиці 'Залишки та обіг грошових коштів'
    /// </summary>
    public static class money_rests_dynamic_totals_TS_   
    {
        public const int __tGid = 1188;
        public const string __tName = "money_rests_dynamic_totals";
        static mdTable m_mdTable;
        static money_rests_dynamic_totals_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _money_account_fGid = 5990;
       
        public static mdTableField money_account
        {
            get { return __MD.Fields[_money_account_fGid];}
        }
        public const string _money_account_fName = "money_account";        public const int _amount_in_fGid = 5995;
       
        public static mdTableField amount_in
        {
            get { return __MD.Fields[_amount_in_fGid];}
        }
        public const string _amount_in_fName = "amount_in";        public const int _amount_out_fGid = 5996;
       
        public static mdTableField amount_out
        {
            get { return __MD.Fields[_amount_out_fGid];}
        }
        public const string _amount_out_fName = "amount_out";        public const int _amount_rest_fGid = 5997;
       
        public static mdTableField amount_rest
        {
            get { return __MD.Fields[_amount_rest_fGid];}
        }
        public const string _amount_rest_fName = "amount_rest";        public const int _amount_initial_rest_fGid = 5998;
       
        public static mdTableField amount_initial_rest
        {
            get { return __MD.Fields[_amount_initial_rest_fGid];}
        }
        public const string _amount_initial_rest_fName = "amount_initial_rest";        public const int _recorder_fGid = 6045;
       
        public static mdTableField recorder
        {
            get { return __MD.Fields[_recorder_fGid];}
        }
        public const string _recorder_fName = "recorder";        public const int _recorder_tid_fGid = 6046;
       
        public static mdTableField recorder_tid
        {
            get { return __MD.Fields[_recorder_tid_fGid];}
        }
        public const string _recorder_tid_fName = "recorder_tid";        public const int _reg_date_fGid = 6047;
       
        public static mdTableField reg_date
        {
            get { return __MD.Fields[_reg_date_fGid];}
        }
        public const string _reg_date_fName = "reg_date";        public const int _op_day_fGid = 6048;
       
        public static mdTableField op_day
        {
            get { return __MD.Fields[_op_day_fGid];}
        }
        public const string _op_day_fName = "op_day";        public const int _amount_rest_debit_fGid = 9292;
       
        public static mdTableField amount_rest_debit
        {
            get { return __MD.Fields[_amount_rest_debit_fGid];}
        }
        public const string _amount_rest_debit_fName = "amount_rest_debit";        public const int _amount_rest_credit_fGid = 9293;
       
        public static mdTableField amount_rest_credit
        {
            get { return __MD.Fields[_amount_rest_credit_fGid];}
        }
        public const string _amount_rest_credit_fName = "amount_rest_credit";        public const int _amount_initial_rest_credit_fGid = 9294;
       
        public static mdTableField amount_initial_rest_credit
        {
            get { return __MD.Fields[_amount_initial_rest_credit_fGid];}
        }
        public const string _amount_initial_rest_credit_fName = "amount_initial_rest_credit";        public const int _amount_initial_rest_debit_fGid = 9295;
       
        public static mdTableField amount_initial_rest_debit
        {
            get { return __MD.Fields[_amount_initial_rest_debit_fGid];}
        }
        public const string _amount_initial_rest_debit_fName = "amount_initial_rest_debit";        public const int _BalanseAccount_fGid = 9296;
       
        public static mdTableField BalanseAccount
        {
            get { return __MD.Fields[_BalanseAccount_fGid];}
        }
        public const string _BalanseAccount_fName = "BalanseAccount";        public const int _company_money_account_tid_fGid = 9326;
       
        public static mdTableField company_money_account_tid
        {
            get { return __MD.Fields[_company_money_account_tid_fGid];}
        }
        public const string _company_money_account_tid_fName = "company_money_account_tid";        public const int _SIGN_rest_fGid = 9423;
       
        public static mdTableField SIGN_rest
        {
            get { return __MD.Fields[_SIGN_rest_fGid];}
        }
        public const string _SIGN_rest_fName = "SIGN_rest";        public const int _SIGN_InitialRest_fGid = 9424;
       
        public static mdTableField SIGN_InitialRest
        {
            get { return __MD.Fields[_SIGN_InitialRest_fGid];}
        }
        public const string _SIGN_InitialRest_fName = "SIGN_InitialRest";        public const int _company_fGid = 20815;
       
        public static mdTableField company
        {
            get { return __MD.Fields[_company_fGid];}
        }
        public const string _company_fName = "company";        public const int _Resource_Flow_Item_fGid = 26878;
       
        public static mdTableField Resource_Flow_Item
        {
            get { return __MD.Fields[_Resource_Flow_Item_fGid];}
        }
        public const string _Resource_Flow_Item_fName = "Resource_Flow_Item";        public const int _payment_comment_fGid = 27096;
       
        public static mdTableField payment_comment
        {
            get { return __MD.Fields[_payment_comment_fGid];}
        }
        public const string _payment_comment_fName = "payment_comment";        public const int _contragent_kf_fGid = 27098;
       
        public static mdTableField contragent_kf
        {
            get { return __MD.Fields[_contragent_kf_fGid];}
        }
        public const string _contragent_kf_fName = "contragent_kf";        public const int _FinProject_fGid = 27101;
       
        public static mdTableField FinProject
        {
            get { return __MD.Fields[_FinProject_fGid];}
        }
        public const string _FinProject_fName = "FinProject";        public const int _CorCompany_fGid = 27278;
       
        public static mdTableField CorCompany
        {
            get { return __MD.Fields[_CorCompany_fGid];}
        }
        public const string _CorCompany_fName = "CorCompany";        public const int _IsInternalPayment_fGid = 27704;
       
        public static mdTableField IsInternalPayment
        {
            get { return __MD.Fields[_IsInternalPayment_fGid];}
        }
        public const string _IsInternalPayment_fName = "IsInternalPayment";        public const int _SheduledPayment_fGid = 29965;
       
        public static mdTableField SheduledPayment
        {
            get { return __MD.Fields[_SheduledPayment_fGid];}
        }
        public const string _SheduledPayment_fName = "SheduledPayment";        public const int _BaseDoc_fGid = 31003;
       
        public static mdTableField BaseDoc
        {
            get { return __MD.Fields[_BaseDoc_fGid];}
        }
        public const string _BaseDoc_fName = "BaseDoc";        public const int _MA_Project_fGid = 31872;
       
        public static mdTableField MA_Project
        {
            get { return __MD.Fields[_MA_Project_fGid];}
        }
        public const string _MA_Project_fName = "MA_Project";        public const int _base_doc_number_fGid = 31877;
       
        public static mdTableField base_doc_number
        {
            get { return __MD.Fields[_base_doc_number_fGid];}
        }
        public const string _base_doc_number_fName = "base_doc_number";       
        public static mdTableField money_account_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_money_account_fGid]);}
        }
        public const string _money_account_view__fName = "money_account_view_";       
        public static mdTableField BalanseAccount_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_BalanseAccount_fGid]);}
        }
        public const string _BalanseAccount_view__fName = "BalanseAccount_view_";       
        public static mdTableField company_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_company_fGid]);}
        }
        public const string _company_view__fName = "company_view_";       
        public static mdTableField Resource_Flow_Item_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Resource_Flow_Item_fGid]);}
        }
        public const string _Resource_Flow_Item_view__fName = "Resource_Flow_Item_view_";       
        public static mdTableField contragent_kf_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_contragent_kf_fGid]);}
        }
        public const string _contragent_kf_view__fName = "contragent_kf_view_";       
        public static mdTableField FinProject_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_FinProject_fGid]);}
        }
        public const string _FinProject_view__fName = "FinProject_view_";       
        public static mdTableField CorCompany_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_CorCompany_fGid]);}
        }
        public const string _CorCompany_view__fName = "CorCompany_view_";       
        public static mdTableField SheduledPayment_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_SheduledPayment_fGid]);}
        }
        public const string _SheduledPayment_view__fName = "SheduledPayment_view_";       
        public static mdTableField BaseDoc_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_BaseDoc_fGid]);}
        }
        public const string _BaseDoc_view__fName = "BaseDoc_view_";       
        public static mdTableField MA_Project_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_MA_Project_fGid]);}
        }
        public const string _MA_Project_view__fName = "MA_Project_view_";       
        public static mdTableField recorder_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_recorder_fGid]);}
        }
        public const string _recorder_view__fName = "recorder_view_";        public const int _recorder_view_documents__fGid = 99000102;
       
        public static mdTableField recorder_view_documents_
        {
            get { return __MD.Fields[_recorder_view_documents__fGid];}
        }
        public const string _recorder_view_documents__fName = "recorder_view_documents_";
    }
    /// <summary>
    /// Схема таблиці 'Залишки грошей. Підсумки'
    /// </summary>
    public static class money_rests_totals_TS_   
    {
        public const int __tGid = 1187;
        public const string __tName = "money_rests_totals";
        static mdTable m_mdTable;
        static money_rests_totals_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _operation_period_fGid = 5987;
       
        public static mdTableField operation_period
        {
            get { return __MD.Fields[_operation_period_fGid];}
        }
        public const string _operation_period_fName = "operation_period";        public const int _money_account_fGid = 5989;
       
        public static mdTableField money_account
        {
            get { return __MD.Fields[_money_account_fGid];}
        }
        public const string _money_account_fName = "money_account";        public const int _amount_in_fGid = 5992;
       
        public static mdTableField amount_in
        {
            get { return __MD.Fields[_amount_in_fGid];}
        }
        public const string _amount_in_fName = "amount_in";        public const int _amount_out_fGid = 5993;
       
        public static mdTableField amount_out
        {
            get { return __MD.Fields[_amount_out_fGid];}
        }
        public const string _amount_out_fName = "amount_out";        public const int _amount_rest_fGid = 5994;
       
        public static mdTableField amount_rest
        {
            get { return __MD.Fields[_amount_rest_fGid];}
        }
        public const string _amount_rest_fName = "amount_rest";       
        public static mdTableField operation_period_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_operation_period_fGid]);}
        }
        public const string _operation_period_view__fName = "operation_period_view_";       
        public static mdTableField money_account_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_money_account_fGid]);}
        }
        public const string _money_account_view__fName = "money_account_view_";       
        public static mdTableField operation_period_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_operation_period_fGid]);}
        }
        public const string _operation_period_status__fName = "operation_period_status_";       
        public static mdTableField money_account_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_money_account_fGid]);}
        }
        public const string _money_account_status__fName = "money_account_status_";
    }
    /// <summary>
    /// Схема таблиці 'Стан оформлення первинних документів'
    /// </summary>
    public static class ObjectSource_Statuses_TS_   
    {
        public const int __tGid = 15479;
        public const string __tName = "ObjectSource_Statuses";
        static mdTable m_mdTable;
        static ObjectSource_Statuses_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _Responsible_Person_fGid = 26555;
       
        public static mdTableField Responsible_Person
        {
            get { return __MD.Fields[_Responsible_Person_fGid];}
        }
        public const string _Responsible_Person_fName = "Responsible_Person";        public const int _Status_fGid = 26556;
       
        public static mdTableField Status
        {
            get { return __MD.Fields[_Status_fGid];}
        }
        public const string _Status_fName = "Status";        public const int _version_no_fGid = 26557;
       
        public static mdTableField version_no
        {
            get { return __MD.Fields[_version_no_fGid];}
        }
        public const string _version_no_fName = "version_no";        public const int _document_fGid = 26558;
       
        public static mdTableField document
        {
            get { return __MD.Fields[_document_fGid];}
        }
        public const string _document_fName = "document";        public const int _document_tid_fGid = 26561;
       
        public static mdTableField document_tid
        {
            get { return __MD.Fields[_document_tid_fGid];}
        }
        public const string _document_tid_fName = "document_tid";       
        public static mdTableField Responsible_Person_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Responsible_Person_fGid]);}
        }
        public const string _Responsible_Person_view__fName = "Responsible_Person_view_";       
        public static mdTableField Responsible_Person_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Responsible_Person_fGid]);}
        }
        public const string _Responsible_Person_status__fName = "Responsible_Person_status_";       
        public static mdTableField document_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_document_fGid]);}
        }
        public const string _document_view__fName = "document_view_";        public const int _document_view_contract__fGid = 99000233;
       
        public static mdTableField document_view_contract_
        {
            get { return __MD.Fields[_document_view_contract__fGid];}
        }
        public const string _document_view_contract__fName = "document_view_contract_";        public const int _document_view_DocumentationsOfProjects__fGid = 99000234;
       
        public static mdTableField document_view_DocumentationsOfProjects_
        {
            get { return __MD.Fields[_document_view_DocumentationsOfProjects__fGid];}
        }
        public const string _document_view_DocumentationsOfProjects__fName = "document_view_DocumentationsOfProjects_";
    }
    /// <summary>
    /// Схема таблиці 'Групи статей доходів та витрат'
    /// </summary>
    public static class Resource_Flow_Item_Groups_TS_   
    {
        public const int __tGid = 652;
        public const string __tName = "Resource_Flow_Item_Groups";
        static mdTable m_mdTable;
        static Resource_Flow_Item_Groups_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _gid_fGid = 5325;
       
        public static mdTableField gid
        {
            get { return __MD.Fields[_gid_fGid];}
        }
        public const string _gid_fName = "gid";        public const int _Name_fGid = 5326;
       
        public static mdTableField Name
        {
            get { return __MD.Fields[_Name_fGid];}
        }
        public const string _Name_fName = "Name";        public const int _Description_fGid = 5327;
       
        public static mdTableField Description
        {
            get { return __MD.Fields[_Description_fGid];}
        }
        public const string _Description_fName = "Description";        public const int _object_status_fGid = 5335;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _version_no_fGid = 5759;
       
        public static mdTableField version_no
        {
            get { return __MD.Fields[_version_no_fGid];}
        }
        public const string _version_no_fName = "version_no";        public const int _parent_group_fGid = 9659;
       
        public static mdTableField parent_group
        {
            get { return __MD.Fields[_parent_group_fGid];}
        }
        public const string _parent_group_fName = "parent_group";        public const int _asid_fGid = 20272;
       
        public static mdTableField asid
        {
            get { return __MD.Fields[_asid_fGid];}
        }
        public const string _asid_fName = "asid";        public const int _GroupCode_fGid = 30459;
       
        public static mdTableField GroupCode
        {
            get { return __MD.Fields[_GroupCode_fGid];}
        }
        public const string _GroupCode_fName = "GroupCode";        public const int _SettlementsAccount_fGid = 32902;
       
        public static mdTableField SettlementsAccount
        {
            get { return __MD.Fields[_SettlementsAccount_fGid];}
        }
        public const string _SettlementsAccount_fName = "SettlementsAccount";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";       
        public static mdTableField parent_group_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_parent_group_fGid]);}
        }
        public const string _parent_group_view__fName = "parent_group_view_";       
        public static mdTableField SettlementsAccount_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_SettlementsAccount_fGid]);}
        }
        public const string _SettlementsAccount_view__fName = "SettlementsAccount_view_";       
        public static mdTableField parent_group_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_parent_group_fGid]);}
        }
        public const string _parent_group_status__fName = "parent_group_status_";       
        public static mdTableField SettlementsAccount_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_SettlementsAccount_fGid]);}
        }
        public const string _SettlementsAccount_status__fName = "SettlementsAccount_status_";
    }
    /// <summary>
    /// Схема таблиці 'Статті доходів та витрат'
    /// </summary>
    public static class Resource_Flow_Items_TS_   
    {
        public const int __tGid = 651;
        public const string __tName = "Resource_Flow_Items";
        static mdTable m_mdTable;
        static Resource_Flow_Items_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _gid_fGid = 5322;
       
        public static mdTableField gid
        {
            get { return __MD.Fields[_gid_fGid];}
        }
        public const string _gid_fName = "gid";        public const int _Name_fGid = 5323;
       
        public static mdTableField Name
        {
            get { return __MD.Fields[_Name_fGid];}
        }
        public const string _Name_fName = "Name";        public const int _Description_fGid = 5324;
       
        public static mdTableField Description
        {
            get { return __MD.Fields[_Description_fGid];}
        }
        public const string _Description_fName = "Description";        public const int _Item_Group_fGid = 5328;
       
        public static mdTableField Item_Group
        {
            get { return __MD.Fields[_Item_Group_fGid];}
        }
        public const string _Item_Group_fName = "Item_Group";        public const int _object_status_fGid = 5334;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _version_no_fGid = 5758;
       
        public static mdTableField version_no
        {
            get { return __MD.Fields[_version_no_fGid];}
        }
        public const string _version_no_fName = "version_no";        public const int _BalanseAccount_fGid = 9078;
       
        public static mdTableField BalanseAccount
        {
            get { return __MD.Fields[_BalanseAccount_fGid];}
        }
        public const string _BalanseAccount_fName = "BalanseAccount";        public const int _asid_fGid = 10559;
       
        public static mdTableField asid
        {
            get { return __MD.Fields[_asid_fGid];}
        }
        public const string _asid_fName = "asid";        public const int _ordering_fGid = 10620;
       
        public static mdTableField ordering
        {
            get { return __MD.Fields[_ordering_fGid];}
        }
        public const string _ordering_fName = "ordering";        public const int _ItemCode_fGid = 30453;
       
        public static mdTableField ItemCode
        {
            get { return __MD.Fields[_ItemCode_fGid];}
        }
        public const string _ItemCode_fName = "ItemCode";        public const int _SettlementsAccount_fGid = 32903;
       
        public static mdTableField SettlementsAccount
        {
            get { return __MD.Fields[_SettlementsAccount_fGid];}
        }
        public const string _SettlementsAccount_fName = "SettlementsAccount";       
        public static mdTableField Item_Group_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Item_Group_fGid]);}
        }
        public const string _Item_Group_view__fName = "Item_Group_view_";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";       
        public static mdTableField BalanseAccount_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_BalanseAccount_fGid]);}
        }
        public const string _BalanseAccount_view__fName = "BalanseAccount_view_";       
        public static mdTableField SettlementsAccount_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_SettlementsAccount_fGid]);}
        }
        public const string _SettlementsAccount_view__fName = "SettlementsAccount_view_";       
        public static mdTableField Item_Group_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Item_Group_fGid]);}
        }
        public const string _Item_Group_status__fName = "Item_Group_status_";       
        public static mdTableField BalanseAccount_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_BalanseAccount_fGid]);}
        }
        public const string _BalanseAccount_status__fName = "BalanseAccount_status_";       
        public static mdTableField SettlementsAccount_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_SettlementsAccount_fGid]);}
        }
        public const string _SettlementsAccount_status__fName = "SettlementsAccount_status_";
    }
    /// <summary>
    /// Схема таблиці 'Вкладені файли до електрронних листів'
    /// </summary>
    public static class EMail_Attachments_TS_   
    {
        public const int __tGid = 15330;
        public const string __tName = "EMail_Attachments";
        static mdTable m_mdTable;
        static EMail_Attachments_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _eMail_fGid = 24602;
       
        public static mdTableField eMail
        {
            get { return __MD.Fields[_eMail_fGid];}
        }
        public const string _eMail_fName = "eMail";        public const int _AttachmentFile_fGid = 24603;
       
        public static mdTableField AttachmentFile
        {
            get { return __MD.Fields[_AttachmentFile_fGid];}
        }
        public const string _AttachmentFile_fName = "AttachmentFile";       
        public static mdTableField File_name
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_AttachmentFile_fGid]);}
        }
        public const string _File_name_fName = "File_name";        public const int _File_datetime_fGid = 24605;
       
        public static mdTableField File_datetime
        {
            get { return __MD.Fields[_File_datetime_fGid];}
        }
        public const string _File_datetime_fName = "File_datetime";        public const int _FileFormat_fGid = 24606;
       
        public static mdTableField FileFormat
        {
            get { return __MD.Fields[_FileFormat_fGid];}
        }
        public const string _FileFormat_fName = "FileFormat";        public const int _ordinal_fGid = 24607;
       
        public static mdTableField ordinal
        {
            get { return __MD.Fields[_ordinal_fGid];}
        }
        public const string _ordinal_fName = "ordinal";       
        public static mdTableField eMail_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_eMail_fGid]);}
        }
        public const string _eMail_view__fName = "eMail_view_";       
        public static mdTableField eMail_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_eMail_fGid]);}
        }
        public const string _eMail_status__fName = "eMail_status_";       
        public static mdTableField AttachmentFile_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_AttachmentFile_fGid]);}
        }
        public const string _AttachmentFile_status__fName = "AttachmentFile_status_";
    }
    /// <summary>
    /// Схема таблиці 'Вкладені файли до планера'
    /// </summary>
    public static class Planer_Attachments_TS_   
    {
        public const int __tGid = 7102;
        public const string __tName = "Planer_Attachments";
        static mdTable m_mdTable;
        static Planer_Attachments_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _AttachmentFile_fGid = 24340;
       
        public static mdTableField AttachmentFile
        {
            get { return __MD.Fields[_AttachmentFile_fGid];}
        }
        public const string _AttachmentFile_fName = "AttachmentFile";        public const int _ordinal_fGid = 24665;
       
        public static mdTableField ordinal
        {
            get { return __MD.Fields[_ordinal_fGid];}
        }
        public const string _ordinal_fName = "ordinal";        public const int _FileFormat_fGid = 24667;
       
        public static mdTableField FileFormat
        {
            get { return __MD.Fields[_FileFormat_fGid];}
        }
        public const string _FileFormat_fName = "FileFormat";        public const int _FilePath_fGid = 30631;
       
        public static mdTableField FilePath
        {
            get { return __MD.Fields[_FilePath_fGid];}
        }
        public const string _FilePath_fName = "FilePath";        public const int _Planer_Operation_fGid = 32640;
       
        public static mdTableField Planer_Operation
        {
            get { return __MD.Fields[_Planer_Operation_fGid];}
        }
        public const string _Planer_Operation_fName = "Planer_Operation";       
        public static mdTableField File_name
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_AttachmentFile_fGid]);}
        }
        public const string _File_name_fName = "File_name";        public const int _File_datetime_fGid = 32642;
       
        public static mdTableField File_datetime
        {
            get { return __MD.Fields[_File_datetime_fGid];}
        }
        public const string _File_datetime_fName = "File_datetime";        public const int _Type_of_Attachment_fGid = 32645;
       
        public static mdTableField Type_of_Attachment
        {
            get { return __MD.Fields[_Type_of_Attachment_fGid];}
        }
        public const string _Type_of_Attachment_fName = "Type_of_Attachment";       
        public static mdTableField Planer_Operation_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Planer_Operation_fGid]);}
        }
        public const string _Planer_Operation_view__fName = "Planer_Operation_view_";       
        public static mdTableField Type_of_Attachment_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Type_of_Attachment_fGid]);}
        }
        public const string _Type_of_Attachment_view__fName = "Type_of_Attachment_view_";       
        public static mdTableField AttachmentFile_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_AttachmentFile_fGid]);}
        }
        public const string _AttachmentFile_status__fName = "AttachmentFile_status_";       
        public static mdTableField Planer_Operation_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Planer_Operation_fGid]);}
        }
        public const string _Planer_Operation_status__fName = "Planer_Operation_status_";       
        public static mdTableField Type_of_Attachment_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Type_of_Attachment_fGid]);}
        }
        public const string _Type_of_Attachment_status__fName = "Type_of_Attachment_status_";
    }
    /// <summary>
    /// Схема таблиці 'Відповіді на службові звернення'
    /// </summary>
    public static class Service_Responce_TS_ 
    {
        public static mdTable __MD
        {
            get 
            {
                return Service_Responce_CTS_.__MD ;
            }
        }
        public const int _Project_fGid = 31791;
       
        public static mdTableField Project
        {
            get { return __MD.Fields[_Project_fGid];}
        }
        public const string _Project_fName = "Project";
    }
    /// <summary>
    /// Схема таблиці 'Журнал бюджетних операцій'
    /// </summary>
    public static class Budjet_Journal_TS_   
    {
        public const int __tGid = 1607;
        public const string __tName = "Budjet_Journal";
        static mdTable m_mdTable;
        static Budjet_Journal_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _operation_type_fGid = 9356;
       
        public static mdTableField operation_type
        {
            get { return __MD.Fields[_operation_type_fGid];}
        }
        public const string _operation_type_fName = "operation_type";        public const int _reg_date_fGid = 9357;
       
        public static mdTableField reg_date
        {
            get { return __MD.Fields[_reg_date_fGid];}
        }
        public const string _reg_date_fName = "reg_date";        public const int _comments_fGid = 9358;
       
        public static mdTableField comments
        {
            get { return __MD.Fields[_comments_fGid];}
        }
        public const string _comments_fName = "comments";        public const int _doc_number_fGid = 9359;
       
        public static mdTableField doc_number
        {
            get { return __MD.Fields[_doc_number_fGid];}
        }
        public const string _doc_number_fName = "doc_number";        public const int _object_status_fGid = 9360;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _author_fGid = 9361;
       
        public static mdTableField author
        {
            get { return __MD.Fields[_author_fGid];}
        }
        public const string _author_fName = "author";        public const int _short_representation_fGid = 9362;
       
        public static mdTableField short_representation
        {
            get { return __MD.Fields[_short_representation_fGid];}
        }
        public const string _short_representation_fName = "short_representation";        public const int _gid_fGid = 9363;
       
        public static mdTableField gid
        {
            get { return __MD.Fields[_gid_fGid];}
        }
        public const string _gid_fName = "gid";        public const int _Total_Amount_fGid = 9387;
       
        public static mdTableField Total_Amount
        {
            get { return __MD.Fields[_Total_Amount_fGid];}
        }
        public const string _Total_Amount_fName = "Total_Amount";        public const int _version_no_fGid = 9406;
       
        public static mdTableField version_no
        {
            get { return __MD.Fields[_version_no_fGid];}
        }
        public const string _version_no_fName = "version_no";        public const int _Company_fGid = 26407;
       
        public static mdTableField Company
        {
            get { return __MD.Fields[_Company_fGid];}
        }
        public const string _Company_fName = "Company";        public const int _Money_account_fGid = 27913;
       
        public static mdTableField Money_account
        {
            get { return __MD.Fields[_Money_account_fGid];}
        }
        public const string _Money_account_fName = "Money_account";        public const int _Project_fGid = 31789;
       
        public static mdTableField Project
        {
            get { return __MD.Fields[_Project_fGid];}
        }
        public const string _Project_fName = "Project";        public const int _CommentsCount_fGid = 31829;
       
        public static mdTableField CommentsCount
        {
            get { return __MD.Fields[_CommentsCount_fGid];}
        }
        public const string _CommentsCount_fName = "CommentsCount";       
        public static mdTableField operation_type_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_operation_type_fGid]);}
        }
        public const string _operation_type_view__fName = "operation_type_view_";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";       
        public static mdTableField author_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_author_fGid]);}
        }
        public const string _author_view__fName = "author_view_";       
        public static mdTableField Company_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Company_fGid]);}
        }
        public const string _Company_view__fName = "Company_view_";       
        public static mdTableField Money_account_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Money_account_fGid]);}
        }
        public const string _Money_account_view__fName = "Money_account_view_";       
        public static mdTableField Project_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Project_fGid]);}
        }
        public const string _Project_view__fName = "Project_view_";       
        public static mdTableField author_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_author_fGid]);}
        }
        public const string _author_status__fName = "author_status_";       
        public static mdTableField Company_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Company_fGid]);}
        }
        public const string _Company_status__fName = "Company_status_";       
        public static mdTableField Money_account_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Money_account_fGid]);}
        }
        public const string _Money_account_status__fName = "Money_account_status_";       
        public static mdTableField Project_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Project_fGid]);}
        }
        public const string _Project_status__fName = "Project_status_";
    }
    /// <summary>
    /// Схема таблиці 'Обороти ресурсів підприємства'
    /// </summary>
    public static class ResourcesFlow_TS_   
    {
        public const int __tGid = 1218;
        public const string __tName = "ResourcesFlow";
        static mdTable m_mdTable;
        static ResourcesFlow_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _reg_date_fGid = 6429;
       
        public static mdTableField reg_date
        {
            get { return __MD.Fields[_reg_date_fGid];}
        }
        public const string _reg_date_fName = "reg_date";        public const int _recorder_fGid = 6430;
       
        public static mdTableField recorder
        {
            get { return __MD.Fields[_recorder_fGid];}
        }
        public const string _recorder_fName = "recorder";        public const int _action_number_fGid = 6431;
       
        public static mdTableField action_number
        {
            get { return __MD.Fields[_action_number_fGid];}
        }
        public const string _action_number_fName = "action_number";        public const int _is_outgo_fGid = 6432;
       
        public static mdTableField is_outgo
        {
            get { return __MD.Fields[_is_outgo_fGid];}
        }
        public const string _is_outgo_fName = "is_outgo";        public const int _Department_fGid = 6434;
       
        public static mdTableField Department
        {
            get { return __MD.Fields[_Department_fGid];}
        }
        public const string _Department_fName = "Department";        public const int _Resource_Flow_Item_fGid = 6435;
       
        public static mdTableField Resource_Flow_Item
        {
            get { return __MD.Fields[_Resource_Flow_Item_fGid];}
        }
        public const string _Resource_Flow_Item_fName = "Resource_Flow_Item";        public const int _Ammount_fGid = 6440;
       
        public static mdTableField Ammount
        {
            get { return __MD.Fields[_Ammount_fGid];}
        }
        public const string _Ammount_fName = "Ammount";        public const int _BalanseAccount_fGid = 9099;
       
        public static mdTableField BalanseAccount
        {
            get { return __MD.Fields[_BalanseAccount_fGid];}
        }
        public const string _BalanseAccount_fName = "BalanseAccount";        public const int _recorder_tid_fGid = 9396;
       
        public static mdTableField recorder_tid
        {
            get { return __MD.Fields[_recorder_tid_fGid];}
        }
        public const string _recorder_tid_fName = "recorder_tid";        public const int _ordering_RFI_fGid = 10912;
       
        public static mdTableField ordering_RFI
        {
            get { return __MD.Fields[_ordering_RFI_fGid];}
        }
        public const string _ordering_RFI_fName = "ordering_RFI";        public const int _Company_fGid = 20224;
       
        public static mdTableField Company
        {
            get { return __MD.Fields[_Company_fGid];}
        }
        public const string _Company_fName = "Company";        public const int _Month_of_Period_fGid = 27639;
       
        public static mdTableField Month_of_Period
        {
            get { return __MD.Fields[_Month_of_Period_fGid];}
        }
        public const string _Month_of_Period_fName = "Month_of_Period";        public const int _FinProject_fGid = 32910;
       
        public static mdTableField FinProject
        {
            get { return __MD.Fields[_FinProject_fGid];}
        }
        public const string _FinProject_fName = "FinProject";       
        public static mdTableField Department_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Department_fGid]);}
        }
        public const string _Department_view__fName = "Department_view_";       
        public static mdTableField Resource_Flow_Item_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Resource_Flow_Item_fGid]);}
        }
        public const string _Resource_Flow_Item_view__fName = "Resource_Flow_Item_view_";       
        public static mdTableField BalanseAccount_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_BalanseAccount_fGid]);}
        }
        public const string _BalanseAccount_view__fName = "BalanseAccount_view_";       
        public static mdTableField Company_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Company_fGid]);}
        }
        public const string _Company_view__fName = "Company_view_";       
        public static mdTableField FinProject_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_FinProject_fGid]);}
        }
        public const string _FinProject_view__fName = "FinProject_view_";       
        public static mdTableField recorder_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_recorder_fGid]);}
        }
        public const string _recorder_view__fName = "recorder_view_";        public const int _recorder_view_documents__fGid = 99000104;
       
        public static mdTableField recorder_view_documents_
        {
            get { return __MD.Fields[_recorder_view_documents__fGid];}
        }
        public const string _recorder_view_documents__fName = "recorder_view_documents_";        public const int _recorder_view_Budjet_Journal__fGid = 99000105;
       
        public static mdTableField recorder_view_Budjet_Journal_
        {
            get { return __MD.Fields[_recorder_view_Budjet_Journal__fGid];}
        }
        public const string _recorder_view_Budjet_Journal__fName = "recorder_view_Budjet_Journal_";       
        public static mdTableField Department_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Department_fGid]);}
        }
        public const string _Department_status__fName = "Department_status_";       
        public static mdTableField Resource_Flow_Item_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Resource_Flow_Item_fGid]);}
        }
        public const string _Resource_Flow_Item_status__fName = "Resource_Flow_Item_status_";       
        public static mdTableField BalanseAccount_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_BalanseAccount_fGid]);}
        }
        public const string _BalanseAccount_status__fName = "BalanseAccount_status_";       
        public static mdTableField Company_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Company_fGid]);}
        }
        public const string _Company_status__fName = "Company_status_";       
        public static mdTableField FinProject_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_FinProject_fGid]);}
        }
        public const string _FinProject_status__fName = "FinProject_status_";
    }
    /// <summary>
    /// Схема таблиці 'Обороти ресурсів підприємства'
    /// </summary>
    public static class ResourcesFlow_dynamic_totals_TS_   
    {
        public const int __tGid = 1220;
        public const string __tName = "ResourcesFlow_dynamic_totals";
        static mdTable m_mdTable;
        static ResourcesFlow_dynamic_totals_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _Resource_Flow_Item_fGid = 6437;
       
        public static mdTableField Resource_Flow_Item
        {
            get { return __MD.Fields[_Resource_Flow_Item_fGid];}
        }
        public const string _Resource_Flow_Item_fName = "Resource_Flow_Item";        public const int _Department_fGid = 6439;
       
        public static mdTableField Department
        {
            get { return __MD.Fields[_Department_fGid];}
        }
        public const string _Department_fName = "Department";        public const int _Ammount_in_fGid = 6445;
       
        public static mdTableField Ammount_in
        {
            get { return __MD.Fields[_Ammount_in_fGid];}
        }
        public const string _Ammount_in_fName = "Ammount_in";        public const int _Ammount_out_fGid = 6446;
       
        public static mdTableField Ammount_out
        {
            get { return __MD.Fields[_Ammount_out_fGid];}
        }
        public const string _Ammount_out_fName = "Ammount_out";        public const int _Ammount_rest_fGid = 6447;
       
        public static mdTableField Ammount_rest
        {
            get { return __MD.Fields[_Ammount_rest_fGid];}
        }
        public const string _Ammount_rest_fName = "Ammount_rest";        public const int _Ammount_initial_rest_fGid = 6448;
       
        public static mdTableField Ammount_initial_rest
        {
            get { return __MD.Fields[_Ammount_initial_rest_fGid];}
        }
        public const string _Ammount_initial_rest_fName = "Ammount_initial_rest";        public const int _BalanseAccount_fGid = 9101;
       
        public static mdTableField BalanseAccount
        {
            get { return __MD.Fields[_BalanseAccount_fGid];}
        }
        public const string _BalanseAccount_fName = "BalanseAccount";        public const int _reg_date_fGid = 9302;
       
        public static mdTableField reg_date
        {
            get { return __MD.Fields[_reg_date_fGid];}
        }
        public const string _reg_date_fName = "reg_date";        public const int _Ammount_rest_credit_fGid = 9303;
       
        public static mdTableField Ammount_rest_credit
        {
            get { return __MD.Fields[_Ammount_rest_credit_fGid];}
        }
        public const string _Ammount_rest_credit_fName = "Ammount_rest_credit";        public const int _Ammount_rest_debit_fGid = 9304;
       
        public static mdTableField Ammount_rest_debit
        {
            get { return __MD.Fields[_Ammount_rest_debit_fGid];}
        }
        public const string _Ammount_rest_debit_fName = "Ammount_rest_debit";        public const int _Ammount_initial_rest_debit_fGid = 9305;
       
        public static mdTableField Ammount_initial_rest_debit
        {
            get { return __MD.Fields[_Ammount_initial_rest_debit_fGid];}
        }
        public const string _Ammount_initial_rest_debit_fName = "Ammount_initial_rest_debit";        public const int _Ammount_initial_rest_credit_fGid = 9306;
       
        public static mdTableField Ammount_initial_rest_credit
        {
            get { return __MD.Fields[_Ammount_initial_rest_credit_fGid];}
        }
        public const string _Ammount_initial_rest_credit_fName = "Ammount_initial_rest_credit";        public const int _EnterpriseDepartment_tid_fGid = 9327;
       
        public static mdTableField EnterpriseDepartment_tid
        {
            get { return __MD.Fields[_EnterpriseDepartment_tid_fGid];}
        }
        public const string _EnterpriseDepartment_tid_fName = "EnterpriseDepartment_tid";        public const int _SIGN_InitialRest_fGid = 9421;
       
        public static mdTableField SIGN_InitialRest
        {
            get { return __MD.Fields[_SIGN_InitialRest_fGid];}
        }
        public const string _SIGN_InitialRest_fName = "SIGN_InitialRest";        public const int _SIGN_Rest_fGid = 9422;
       
        public static mdTableField SIGN_Rest
        {
            get { return __MD.Fields[_SIGN_Rest_fGid];}
        }
        public const string _SIGN_Rest_fName = "SIGN_Rest";        public const int _recorder_fGid = 9433;
       
        public static mdTableField recorder
        {
            get { return __MD.Fields[_recorder_fGid];}
        }
        public const string _recorder_fName = "recorder";        public const int _recorder_tid_fGid = 9434;
       
        public static mdTableField recorder_tid
        {
            get { return __MD.Fields[_recorder_tid_fGid];}
        }
        public const string _recorder_tid_fName = "recorder_tid";        public const int _FinProject_fGid = 32912;
       
        public static mdTableField FinProject
        {
            get { return __MD.Fields[_FinProject_fGid];}
        }
        public const string _FinProject_fName = "FinProject";        public const int _FP_tid_fGid = 32913;
       
        public static mdTableField FP_tid
        {
            get { return __MD.Fields[_FP_tid_fGid];}
        }
        public const string _FP_tid_fName = "FP_tid";       
        public static mdTableField Resource_Flow_Item_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Resource_Flow_Item_fGid]);}
        }
        public const string _Resource_Flow_Item_view__fName = "Resource_Flow_Item_view_";       
        public static mdTableField Department_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Department_fGid]);}
        }
        public const string _Department_view__fName = "Department_view_";       
        public static mdTableField BalanseAccount_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_BalanseAccount_fGid]);}
        }
        public const string _BalanseAccount_view__fName = "BalanseAccount_view_";       
        public static mdTableField FinProject_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_FinProject_fGid]);}
        }
        public const string _FinProject_view__fName = "FinProject_view_";       
        public static mdTableField recorder_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_recorder_fGid]);}
        }
        public const string _recorder_view__fName = "recorder_view_";        public const int _recorder_view_documents__fGid = 99000118;
       
        public static mdTableField recorder_view_documents_
        {
            get { return __MD.Fields[_recorder_view_documents__fGid];}
        }
        public const string _recorder_view_documents__fName = "recorder_view_documents_";        public const int _recorder_view_Budjet_Journal__fGid = 99000119;
       
        public static mdTableField recorder_view_Budjet_Journal_
        {
            get { return __MD.Fields[_recorder_view_Budjet_Journal__fGid];}
        }
        public const string _recorder_view_Budjet_Journal__fName = "recorder_view_Budjet_Journal_";
    }
    /// <summary>
    /// Схема таблиці 'Обороти ресурсів підприємства'
    /// </summary>
    public static class ResourcesFlow_totals_TS_   
    {
        public const int __tGid = 1219;
        public const string __tName = "ResourcesFlow_totals";
        static mdTable m_mdTable;
        static ResourcesFlow_totals_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _operation_period_fGid = 6433;
       
        public static mdTableField operation_period
        {
            get { return __MD.Fields[_operation_period_fGid];}
        }
        public const string _operation_period_fName = "operation_period";        public const int _Resource_Flow_Item_fGid = 6436;
       
        public static mdTableField Resource_Flow_Item
        {
            get { return __MD.Fields[_Resource_Flow_Item_fGid];}
        }
        public const string _Resource_Flow_Item_fName = "Resource_Flow_Item";        public const int _Department_fGid = 6438;
       
        public static mdTableField Department
        {
            get { return __MD.Fields[_Department_fGid];}
        }
        public const string _Department_fName = "Department";        public const int _Ammount_in_fGid = 6442;
       
        public static mdTableField Ammount_in
        {
            get { return __MD.Fields[_Ammount_in_fGid];}
        }
        public const string _Ammount_in_fName = "Ammount_in";        public const int _Ammount_out_fGid = 6443;
       
        public static mdTableField Ammount_out
        {
            get { return __MD.Fields[_Ammount_out_fGid];}
        }
        public const string _Ammount_out_fName = "Ammount_out";        public const int _Ammount_rest_fGid = 6444;
       
        public static mdTableField Ammount_rest
        {
            get { return __MD.Fields[_Ammount_rest_fGid];}
        }
        public const string _Ammount_rest_fName = "Ammount_rest";        public const int _BalanseAccount_fGid = 9100;
       
        public static mdTableField BalanseAccount
        {
            get { return __MD.Fields[_BalanseAccount_fGid];}
        }
        public const string _BalanseAccount_fName = "BalanseAccount";        public const int _Company_fGid = 20225;
       
        public static mdTableField Company
        {
            get { return __MD.Fields[_Company_fGid];}
        }
        public const string _Company_fName = "Company";        public const int _FinProject_fGid = 32911;
       
        public static mdTableField FinProject
        {
            get { return __MD.Fields[_FinProject_fGid];}
        }
        public const string _FinProject_fName = "FinProject";       
        public static mdTableField operation_period_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_operation_period_fGid]);}
        }
        public const string _operation_period_view__fName = "operation_period_view_";       
        public static mdTableField Resource_Flow_Item_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Resource_Flow_Item_fGid]);}
        }
        public const string _Resource_Flow_Item_view__fName = "Resource_Flow_Item_view_";       
        public static mdTableField Department_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Department_fGid]);}
        }
        public const string _Department_view__fName = "Department_view_";       
        public static mdTableField BalanseAccount_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_BalanseAccount_fGid]);}
        }
        public const string _BalanseAccount_view__fName = "BalanseAccount_view_";       
        public static mdTableField Company_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Company_fGid]);}
        }
        public const string _Company_view__fName = "Company_view_";       
        public static mdTableField FinProject_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_FinProject_fGid]);}
        }
        public const string _FinProject_view__fName = "FinProject_view_";       
        public static mdTableField operation_period_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_operation_period_fGid]);}
        }
        public const string _operation_period_status__fName = "operation_period_status_";       
        public static mdTableField Resource_Flow_Item_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Resource_Flow_Item_fGid]);}
        }
        public const string _Resource_Flow_Item_status__fName = "Resource_Flow_Item_status_";       
        public static mdTableField Department_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Department_fGid]);}
        }
        public const string _Department_status__fName = "Department_status_";       
        public static mdTableField BalanseAccount_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_BalanseAccount_fGid]);}
        }
        public const string _BalanseAccount_status__fName = "BalanseAccount_status_";       
        public static mdTableField Company_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Company_fGid]);}
        }
        public const string _Company_status__fName = "Company_status_";       
        public static mdTableField FinProject_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_FinProject_fGid]);}
        }
        public const string _FinProject_status__fName = "FinProject_status_";
    }
    /// <summary>
    /// Схема таблиці 'Журнал операцій з контрагентами'
    /// </summary>
    public static class documents_TS_   
    {
        public const int __tGid = 113;
        public const string __tName = "documents";
        static mdTable m_mdTable;
        static documents_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _gid_fGid = 632;
       
        public static mdTableField gid
        {
            get { return __MD.Fields[_gid_fGid];}
        }
        public const string _gid_fName = "gid";        public const int _operation_type_fGid = 637;
       
        public static mdTableField operation_type
        {
            get { return __MD.Fields[_operation_type_fGid];}
        }
        public const string _operation_type_fName = "operation_type";        public const int _reg_date_fGid = 640;
       
        public static mdTableField reg_date
        {
            get { return __MD.Fields[_reg_date_fGid];}
        }
        public const string _reg_date_fName = "reg_date";        public const int _comments_fGid = 642;
       
        public static mdTableField comments
        {
            get { return __MD.Fields[_comments_fGid];}
        }
        public const string _comments_fName = "comments";        public const int _doc_number_fGid = 648;
       
        public static mdTableField doc_number
        {
            get { return __MD.Fields[_doc_number_fGid];}
        }
        public const string _doc_number_fName = "doc_number";        public const int _object_status_fGid = 1351;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _author_fGid = 1998;
       
        public static mdTableField author
        {
            get { return __MD.Fields[_author_fGid];}
        }
        public const string _author_fName = "author";        public const int _company_fGid = 2025;
       
        public static mdTableField company
        {
            get { return __MD.Fields[_company_fGid];}
        }
        public const string _company_fName = "company";        public const int _version_no_fGid = 2821;
       
        public static mdTableField version_no
        {
            get { return __MD.Fields[_version_no_fGid];}
        }
        public const string _version_no_fName = "version_no";        public const int _representation_fGid = 5337;
       
        public static mdTableField representation
        {
            get { return __MD.Fields[_representation_fGid];}
        }
        public const string _representation_fName = "representation";       
        public static mdTableField operation_type_name
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_operation_type_fGid]);}
        }
        public const string _operation_type_name_fName = "operation_type_name";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";       
        public static mdTableField author_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_author_fGid]);}
        }
        public const string _author_view__fName = "author_view_";       
        public static mdTableField company_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_company_fGid]);}
        }
        public const string _company_view__fName = "company_view_";       
        public static mdTableField author_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_author_fGid]);}
        }
        public const string _author_status__fName = "author_status_";       
        public static mdTableField company_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_company_fGid]);}
        }
        public const string _company_status__fName = "company_status_";
    }
    /// <summary>
    /// Схема таблиці 'Поточні ціни активів'
    /// </summary>
    public static class ActiveProjAssetsPrices_TS_   
    {
        public const int __tGid = 15612;
        public const string __tName = "ActiveProjAssetsPrices";
        static mdTable m_mdTable;
        static ActiveProjAssetsPrices_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _ProjectAsset_fGid = 28662;
       
        public static mdTableField ProjectAsset
        {
            get { return __MD.Fields[_ProjectAsset_fGid];}
        }
        public const string _ProjectAsset_fName = "ProjectAsset";        public const int _reg_date_fGid = 28663;
       
        public static mdTableField reg_date
        {
            get { return __MD.Fields[_reg_date_fGid];}
        }
        public const string _reg_date_fName = "reg_date";        public const int _AssetsCost_fGid = 28664;
       
        public static mdTableField AssetsCost
        {
            get { return __MD.Fields[_AssetsCost_fGid];}
        }
        public const string _AssetsCost_fName = "AssetsCost";        public const int _recorder_fGid = 28665;
       
        public static mdTableField recorder
        {
            get { return __MD.Fields[_recorder_fGid];}
        }
        public const string _recorder_fName = "recorder";        public const int _max_date_fGid = 28666;
       
        public static mdTableField max_date
        {
            get { return __MD.Fields[_max_date_fGid];}
        }
        public const string _max_date_fName = "max_date";       
        public static mdTableField ProjectAsset_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_ProjectAsset_fGid]);}
        }
        public const string _ProjectAsset_view__fName = "ProjectAsset_view_";       
        public static mdTableField recorder_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_recorder_fGid]);}
        }
        public const string _recorder_view__fName = "recorder_view_";
    }
    /// <summary>
    /// Схема таблиці 'Акт виконання робіт'
    /// </summary>
    public static class ActOfWorkExecution_TS_   
    {
        public const int __tGid = 15764;
        public const string __tName = "ActOfWorkExecution";
        static mdTable m_mdTable;
        static ActOfWorkExecution_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _document_fGid = 31548;
       
        public static mdTableField document
        {
            get { return __MD.Fields[_document_fGid];}
        }
        public const string _document_fName = "document";        public const int _contragent_fGid = 31549;
       
        public static mdTableField contragent
        {
            get { return __MD.Fields[_contragent_fGid];}
        }
        public const string _contragent_fName = "contragent";        public const int _Contract_fGid = 31550;
       
        public static mdTableField Contract
        {
            get { return __MD.Fields[_Contract_fGid];}
        }
        public const string _Contract_fName = "Contract";        public const int _reg_date_fGid = 31551;
       
        public static mdTableField reg_date
        {
            get { return __MD.Fields[_reg_date_fGid];}
        }
        public const string _reg_date_fName = "reg_date";        public const int _doc_number_fGid = 31552;
       
        public static mdTableField doc_number
        {
            get { return __MD.Fields[_doc_number_fGid];}
        }
        public const string _doc_number_fName = "doc_number";        public const int _company_fGid = 31553;
       
        public static mdTableField company
        {
            get { return __MD.Fields[_company_fGid];}
        }
        public const string _company_fName = "company";        public const int _Resource_Flow_Item_fGid = 31554;
       
        public static mdTableField Resource_Flow_Item
        {
            get { return __MD.Fields[_Resource_Flow_Item_fGid];}
        }
        public const string _Resource_Flow_Item_fName = "Resource_Flow_Item";        public const int _FinProject_fGid = 31555;
       
        public static mdTableField FinProject
        {
            get { return __MD.Fields[_FinProject_fGid];}
        }
        public const string _FinProject_fName = "FinProject";        public const int _FinancingObject_fGid = 31556;
       
        public static mdTableField FinancingObject
        {
            get { return __MD.Fields[_FinancingObject_fGid];}
        }
        public const string _FinancingObject_fName = "FinancingObject";        public const int _Amount_fGid = 31557;
       
        public static mdTableField Amount
        {
            get { return __MD.Fields[_Amount_fGid];}
        }
        public const string _Amount_fName = "Amount";        public const int _base_document_fGid = 31558;
       
        public static mdTableField base_document
        {
            get { return __MD.Fields[_base_document_fGid];}
        }
        public const string _base_document_fName = "base_document";        public const int _Description_fGid = 31559;
       
        public static mdTableField Description
        {
            get { return __MD.Fields[_Description_fGid];}
        }
        public const string _Description_fName = "Description";        public const int _ExpenseAccount_fGid = 32915;
       
        public static mdTableField ExpenseAccount
        {
            get { return __MD.Fields[_ExpenseAccount_fGid];}
        }
        public const string _ExpenseAccount_fName = "ExpenseAccount";        public const int _SettlementsAccount_fGid = 32916;
       
        public static mdTableField SettlementsAccount
        {
            get { return __MD.Fields[_SettlementsAccount_fGid];}
        }
        public const string _SettlementsAccount_fName = "SettlementsAccount";       
        public static mdTableField document_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_document_fGid]);}
        }
        public const string _document_view__fName = "document_view_";       
        public static mdTableField contragent_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_contragent_fGid]);}
        }
        public const string _contragent_view__fName = "contragent_view_";       
        public static mdTableField Contract_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Contract_fGid]);}
        }
        public const string _Contract_view__fName = "Contract_view_";       
        public static mdTableField company_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_company_fGid]);}
        }
        public const string _company_view__fName = "company_view_";       
        public static mdTableField Resource_Flow_Item_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Resource_Flow_Item_fGid]);}
        }
        public const string _Resource_Flow_Item_view__fName = "Resource_Flow_Item_view_";       
        public static mdTableField FinProject_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_FinProject_fGid]);}
        }
        public const string _FinProject_view__fName = "FinProject_view_";       
        public static mdTableField FinancingObject_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_FinancingObject_fGid]);}
        }
        public const string _FinancingObject_view__fName = "FinancingObject_view_";       
        public static mdTableField base_document_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_base_document_fGid]);}
        }
        public const string _base_document_view__fName = "base_document_view_";       
        public static mdTableField ExpenseAccount_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_ExpenseAccount_fGid]);}
        }
        public const string _ExpenseAccount_view__fName = "ExpenseAccount_view_";       
        public static mdTableField SettlementsAccount_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_SettlementsAccount_fGid]);}
        }
        public const string _SettlementsAccount_view__fName = "SettlementsAccount_view_";       
        public static mdTableField document_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_document_fGid]);}
        }
        public const string _document_status__fName = "document_status_";       
        public static mdTableField contragent_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_contragent_fGid]);}
        }
        public const string _contragent_status__fName = "contragent_status_";       
        public static mdTableField Contract_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Contract_fGid]);}
        }
        public const string _Contract_status__fName = "Contract_status_";       
        public static mdTableField company_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_company_fGid]);}
        }
        public const string _company_status__fName = "company_status_";       
        public static mdTableField Resource_Flow_Item_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Resource_Flow_Item_fGid]);}
        }
        public const string _Resource_Flow_Item_status__fName = "Resource_Flow_Item_status_";       
        public static mdTableField FinProject_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_FinProject_fGid]);}
        }
        public const string _FinProject_status__fName = "FinProject_status_";       
        public static mdTableField FinancingObject_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_FinancingObject_fGid]);}
        }
        public const string _FinancingObject_status__fName = "FinancingObject_status_";       
        public static mdTableField base_document_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_base_document_fGid]);}
        }
        public const string _base_document_status__fName = "base_document_status_";       
        public static mdTableField ExpenseAccount_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_ExpenseAccount_fGid]);}
        }
        public const string _ExpenseAccount_status__fName = "ExpenseAccount_status_";       
        public static mdTableField SettlementsAccount_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_SettlementsAccount_fGid]);}
        }
        public const string _SettlementsAccount_status__fName = "SettlementsAccount_status_";
    }
    /// <summary>
    /// Схема таблиці 'Аналіз бюджетів'
    /// </summary>
    public static class Budjet_Report_TS_   
    {
        public const int __tGid = 15492;
        public const string __tName = "Budjet_Report";
        static mdTable m_mdTable;
        static Budjet_Report_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _Resource_Flow_Item_fGid = 26677;
       
        public static mdTableField Resource_Flow_Item
        {
            get { return __MD.Fields[_Resource_Flow_Item_fGid];}
        }
        public const string _Resource_Flow_Item_fName = "Resource_Flow_Item";        public const int _document_fGid = 26680;
       
        public static mdTableField document
        {
            get { return __MD.Fields[_document_fGid];}
        }
        public const string _document_fName = "document";        public const int _FinProject_fGid = 26681;
       
        public static mdTableField FinProject
        {
            get { return __MD.Fields[_FinProject_fGid];}
        }
        public const string _FinProject_fName = "FinProject";        public const int _amount_planed_fGid = 26714;
       
        public static mdTableField amount_planed
        {
            get { return __MD.Fields[_amount_planed_fGid];}
        }
        public const string _amount_planed_fName = "amount_planed";        public const int _company_fGid = 26715;
       
        public static mdTableField company
        {
            get { return __MD.Fields[_company_fGid];}
        }
        public const string _company_fName = "company";        public const int _comment_fGid = 26716;
       
        public static mdTableField comment
        {
            get { return __MD.Fields[_comment_fGid];}
        }
        public const string _comment_fName = "comment";        public const int _amount_executed_fGid = 26717;
       
        public static mdTableField amount_executed
        {
            get { return __MD.Fields[_amount_executed_fGid];}
        }
        public const string _amount_executed_fName = "amount_executed";        public const int _reg_date_fGid = 26727;
       
        public static mdTableField reg_date
        {
            get { return __MD.Fields[_reg_date_fGid];}
        }
        public const string _reg_date_fName = "reg_date";        public const int _amount_in_fGid = 26734;
       
        public static mdTableField amount_in
        {
            get { return __MD.Fields[_amount_in_fGid];}
        }
        public const string _amount_in_fName = "amount_in";        public const int _amount_out_fGid = 26735;
       
        public static mdTableField amount_out
        {
            get { return __MD.Fields[_amount_out_fGid];}
        }
        public const string _amount_out_fName = "amount_out";        public const int _object_status_fGid = 26736;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _RestOfIncoms_fGid = 26738;
       
        public static mdTableField RestOfIncoms
        {
            get { return __MD.Fields[_RestOfIncoms_fGid];}
        }
        public const string _RestOfIncoms_fName = "RestOfIncoms";        public const int _PercentOfIncoms_fGid = 26739;
       
        public static mdTableField PercentOfIncoms
        {
            get { return __MD.Fields[_PercentOfIncoms_fGid];}
        }
        public const string _PercentOfIncoms_fName = "PercentOfIncoms";        public const int _RestOfPayments_fGid = 26740;
       
        public static mdTableField RestOfPayments
        {
            get { return __MD.Fields[_RestOfPayments_fGid];}
        }
        public const string _RestOfPayments_fName = "RestOfPayments";        public const int _PercentOfPayments_fGid = 26741;
       
        public static mdTableField PercentOfPayments
        {
            get { return __MD.Fields[_PercentOfPayments_fGid];}
        }
        public const string _PercentOfPayments_fName = "PercentOfPayments";        public const int _RestOfExecution_fGid = 26806;
       
        public static mdTableField RestOfExecution
        {
            get { return __MD.Fields[_RestOfExecution_fGid];}
        }
        public const string _RestOfExecution_fName = "RestOfExecution";        public const int _PercentOfExecution_fGid = 26807;
       
        public static mdTableField PercentOfExecution
        {
            get { return __MD.Fields[_PercentOfExecution_fGid];}
        }
        public const string _PercentOfExecution_fName = "PercentOfExecution";
    }
    /// <summary>
    /// Схема таблиці 'Т.Ч. Бюджетних операцій'
    /// </summary>
    public static class Budjet_tp_TS_   
    {
        public const int __tGid = 15463;
        public const string __tName = "Budjet_tp";
        static mdTable m_mdTable;
        static Budjet_tp_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _document_fGid = 26402;
       
        public static mdTableField document
        {
            get { return __MD.Fields[_document_fGid];}
        }
        public const string _document_fName = "document";        public const int _Resource_Flow_Item_fGid = 26403;
       
        public static mdTableField Resource_Flow_Item
        {
            get { return __MD.Fields[_Resource_Flow_Item_fGid];}
        }
        public const string _Resource_Flow_Item_fName = "Resource_Flow_Item";        public const int _ordinal_fGid = 26404;
       
        public static mdTableField ordinal
        {
            get { return __MD.Fields[_ordinal_fGid];}
        }
        public const string _ordinal_fName = "ordinal";        public const int _amount_fGid = 26405;
       
        public static mdTableField amount
        {
            get { return __MD.Fields[_amount_fGid];}
        }
        public const string _amount_fName = "amount";        public const int _comment_fGid = 26406;
       
        public static mdTableField comment
        {
            get { return __MD.Fields[_comment_fGid];}
        }
        public const string _comment_fName = "comment";        public const int _company_fGid = 26679;
       
        public static mdTableField company
        {
            get { return __MD.Fields[_company_fGid];}
        }
        public const string _company_fName = "company";        public const int _contragent_fGid = 27104;
       
        public static mdTableField contragent
        {
            get { return __MD.Fields[_contragent_fGid];}
        }
        public const string _contragent_fName = "contragent";        public const int _RowSubKey_fGid = 31849;
       
        public static mdTableField RowSubKey
        {
            get { return __MD.Fields[_RowSubKey_fGid];}
        }
        public const string _RowSubKey_fName = "RowSubKey";       
        public static mdTableField document_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_document_fGid]);}
        }
        public const string _document_view__fName = "document_view_";       
        public static mdTableField Resource_Flow_Item_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Resource_Flow_Item_fGid]);}
        }
        public const string _Resource_Flow_Item_view__fName = "Resource_Flow_Item_view_";       
        public static mdTableField company_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_company_fGid]);}
        }
        public const string _company_view__fName = "company_view_";       
        public static mdTableField contragent_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_contragent_fGid]);}
        }
        public const string _contragent_view__fName = "contragent_view_";       
        public static mdTableField document_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_document_fGid]);}
        }
        public const string _document_status__fName = "document_status_";       
        public static mdTableField Resource_Flow_Item_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Resource_Flow_Item_fGid]);}
        }
        public const string _Resource_Flow_Item_status__fName = "Resource_Flow_Item_status_";       
        public static mdTableField company_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_company_fGid]);}
        }
        public const string _company_status__fName = "company_status_";       
        public static mdTableField contragent_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_contragent_fGid]);}
        }
        public const string _contragent_status__fName = "contragent_status_";
    }
    /// <summary>
    /// Схема таблиці 'Відомість виконання проекту'
    /// </summary>
    public static class BudjetExecution_TS_   
    {
        public const int __tGid = 15466;
        public const string __tName = "BudjetExecution";
        static mdTable m_mdTable;
        static BudjetExecution_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _FinProject_fGid = 26421;
       
        public static mdTableField FinProject
        {
            get { return __MD.Fields[_FinProject_fGid];}
        }
        public const string _FinProject_fName = "FinProject";        public const int _document_fGid = 26422;
       
        public static mdTableField document
        {
            get { return __MD.Fields[_document_fGid];}
        }
        public const string _document_fName = "document";        public const int _reg_date_fGid = 26423;
       
        public static mdTableField reg_date
        {
            get { return __MD.Fields[_reg_date_fGid];}
        }
        public const string _reg_date_fName = "reg_date";        public const int _doc_number_fGid = 26424;
       
        public static mdTableField doc_number
        {
            get { return __MD.Fields[_doc_number_fGid];}
        }
        public const string _doc_number_fName = "doc_number";        public const int _company_fGid = 26425;
       
        public static mdTableField company
        {
            get { return __MD.Fields[_company_fGid];}
        }
        public const string _company_fName = "company";        public const int _comments_fGid = 26426;
       
        public static mdTableField comments
        {
            get { return __MD.Fields[_comments_fGid];}
        }
        public const string _comments_fName = "comments";        public const int _object_status_fGid = 26723;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _amount_total_fGid = 26831;
       
        public static mdTableField amount_total
        {
            get { return __MD.Fields[_amount_total_fGid];}
        }
        public const string _amount_total_fName = "amount_total";        public const int _order_company_fGid = 27523;
       
        public static mdTableField order_company
        {
            get { return __MD.Fields[_order_company_fGid];}
        }
        public const string _order_company_fName = "order_company";       
        public static mdTableField FinProject_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_FinProject_fGid]);}
        }
        public const string _FinProject_view__fName = "FinProject_view_";       
        public static mdTableField document_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_document_fGid]);}
        }
        public const string _document_view__fName = "document_view_";       
        public static mdTableField company_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_company_fGid]);}
        }
        public const string _company_view__fName = "company_view_";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";       
        public static mdTableField order_company_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_order_company_fGid]);}
        }
        public const string _order_company_view__fName = "order_company_view_";       
        public static mdTableField FinProject_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_FinProject_fGid]);}
        }
        public const string _FinProject_status__fName = "FinProject_status_";       
        public static mdTableField company_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_company_fGid]);}
        }
        public const string _company_status__fName = "company_status_";       
        public static mdTableField order_company_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_order_company_fGid]);}
        }
        public const string _order_company_status__fName = "order_company_status_";
    }
    /// <summary>
    /// Схема таблиці 'BudjetExecution_V'
    /// </summary>
    public static class BudjetExecution_V_TS_   
    {
        public const int __tGid = 15495;
        public const string __tName = "BudjetExecution_V";
        static mdTable m_mdTable;
        static BudjetExecution_V_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _document_fGid = 26718;
       
        public static mdTableField document
        {
            get { return __MD.Fields[_document_fGid];}
        }
        public const string _document_fName = "document";        public const int _FinProject_fGid = 26719;
       
        public static mdTableField FinProject
        {
            get { return __MD.Fields[_FinProject_fGid];}
        }
        public const string _FinProject_fName = "FinProject";        public const int _Resource_Flow_Item_fGid = 26720;
       
        public static mdTableField Resource_Flow_Item
        {
            get { return __MD.Fields[_Resource_Flow_Item_fGid];}
        }
        public const string _Resource_Flow_Item_fName = "Resource_Flow_Item";        public const int _company_fGid = 26721;
       
        public static mdTableField company
        {
            get { return __MD.Fields[_company_fGid];}
        }
        public const string _company_fName = "company";        public const int _reg_date_fGid = 26722;
       
        public static mdTableField reg_date
        {
            get { return __MD.Fields[_reg_date_fGid];}
        }
        public const string _reg_date_fName = "reg_date";        public const int _object_status_fGid = 26724;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _Exec_Amount_fGid = 26725;
       
        public static mdTableField Exec_Amount
        {
            get { return __MD.Fields[_Exec_Amount_fGid];}
        }
        public const string _Exec_Amount_fName = "Exec_Amount";        public const int _comments_fGid = 26726;
       
        public static mdTableField comments
        {
            get { return __MD.Fields[_comments_fGid];}
        }
        public const string _comments_fName = "comments";       
        public static mdTableField Resource_Flow_Item_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Resource_Flow_Item_fGid]);}
        }
        public const string _Resource_Flow_Item_view__fName = "Resource_Flow_Item_view_";       
        public static mdTableField company_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_company_fGid]);}
        }
        public const string _company_view__fName = "company_view_";       
        public static mdTableField document_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_document_fGid]);}
        }
        public const string _document_view__fName = "document_view_";       
        public static mdTableField FinProject_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_FinProject_fGid]);}
        }
        public const string _FinProject_view__fName = "FinProject_view_";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";
    }
    /// <summary>
    /// Схема таблиці 'Бюджетний план видатків'
    /// </summary>
    public static class BudjetPlan_TS_   
    {
        public const int __tGid = 15462;
        public const string __tName = "BudjetPlan";
        static mdTable m_mdTable;
        static BudjetPlan_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _document_fGid = 26392;
       
        public static mdTableField document
        {
            get { return __MD.Fields[_document_fGid];}
        }
        public const string _document_fName = "document";        public const int _reg_date_fGid = 26393;
       
        public static mdTableField reg_date
        {
            get { return __MD.Fields[_reg_date_fGid];}
        }
        public const string _reg_date_fName = "reg_date";        public const int _doc_number_fGid = 26394;
       
        public static mdTableField doc_number
        {
            get { return __MD.Fields[_doc_number_fGid];}
        }
        public const string _doc_number_fName = "doc_number";        public const int _object_status_fGid = 26395;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _company_fGid = 26397;
       
        public static mdTableField company
        {
            get { return __MD.Fields[_company_fGid];}
        }
        public const string _company_fName = "company";        public const int _FinProject_fGid = 26398;
       
        public static mdTableField FinProject
        {
            get { return __MD.Fields[_FinProject_fGid];}
        }
        public const string _FinProject_fName = "FinProject";        public const int _comments_fGid = 26399;
       
        public static mdTableField comments
        {
            get { return __MD.Fields[_comments_fGid];}
        }
        public const string _comments_fName = "comments";       
        public static mdTableField Project_Name
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_FinProject_fGid]);}
        }
        public const string _Project_Name_fName = "Project_Name";        public const int _Representation_fGid = 26401;
       
        public static mdTableField Representation
        {
            get { return __MD.Fields[_Representation_fGid];}
        }
        public const string _Representation_fName = "Representation";        public const int _amount_total_fGid = 26830;
       
        public static mdTableField amount_total
        {
            get { return __MD.Fields[_amount_total_fGid];}
        }
        public const string _amount_total_fName = "amount_total";        public const int _order_company_fGid = 27521;
       
        public static mdTableField order_company
        {
            get { return __MD.Fields[_order_company_fGid];}
        }
        public const string _order_company_fName = "order_company";       
        public static mdTableField document_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_document_fGid]);}
        }
        public const string _document_view__fName = "document_view_";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";       
        public static mdTableField company_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_company_fGid]);}
        }
        public const string _company_view__fName = "company_view_";       
        public static mdTableField order_company_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_order_company_fGid]);}
        }
        public const string _order_company_view__fName = "order_company_view_";       
        public static mdTableField company_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_company_fGid]);}
        }
        public const string _company_status__fName = "company_status_";       
        public static mdTableField FinProject_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_FinProject_fGid]);}
        }
        public const string _FinProject_status__fName = "FinProject_status_";       
        public static mdTableField order_company_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_order_company_fGid]);}
        }
        public const string _order_company_status__fName = "order_company_status_";
    }
    /// <summary>
    /// Схема таблиці 'Бюджетний план надходжень'
    /// </summary>
    public static class BudjetPlan_income_TS_   
    {
        public const int __tGid = 15718;
        public const string __tName = "BudjetPlan_income";
        static mdTable m_mdTable;
        static BudjetPlan_income_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _order_company_fGid = 30474;
       
        public static mdTableField order_company
        {
            get { return __MD.Fields[_order_company_fGid];}
        }
        public const string _order_company_fName = "order_company";        public const int _document_fGid = 30475;
       
        public static mdTableField document
        {
            get { return __MD.Fields[_document_fGid];}
        }
        public const string _document_fName = "document";        public const int _FinProject_fGid = 30476;
       
        public static mdTableField FinProject
        {
            get { return __MD.Fields[_FinProject_fGid];}
        }
        public const string _FinProject_fName = "FinProject";        public const int _reg_date_fGid = 30477;
       
        public static mdTableField reg_date
        {
            get { return __MD.Fields[_reg_date_fGid];}
        }
        public const string _reg_date_fName = "reg_date";        public const int _doc_number_fGid = 30478;
       
        public static mdTableField doc_number
        {
            get { return __MD.Fields[_doc_number_fGid];}
        }
        public const string _doc_number_fName = "doc_number";        public const int _object_status_fGid = 30479;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _company_fGid = 30480;
       
        public static mdTableField company
        {
            get { return __MD.Fields[_company_fGid];}
        }
        public const string _company_fName = "company";        public const int _comments_fGid = 30481;
       
        public static mdTableField comments
        {
            get { return __MD.Fields[_comments_fGid];}
        }
        public const string _comments_fName = "comments";       
        public static mdTableField Project_Name
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_FinProject_fGid]);}
        }
        public const string _Project_Name_fName = "Project_Name";        public const int _amount_total_fGid = 30483;
       
        public static mdTableField amount_total
        {
            get { return __MD.Fields[_amount_total_fGid];}
        }
        public const string _amount_total_fName = "amount_total";        public const int _Representation_fGid = 30484;
       
        public static mdTableField Representation
        {
            get { return __MD.Fields[_Representation_fGid];}
        }
        public const string _Representation_fName = "Representation";       
        public static mdTableField order_company_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_order_company_fGid]);}
        }
        public const string _order_company_view__fName = "order_company_view_";       
        public static mdTableField document_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_document_fGid]);}
        }
        public const string _document_view__fName = "document_view_";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";       
        public static mdTableField company_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_company_fGid]);}
        }
        public const string _company_view__fName = "company_view_";       
        public static mdTableField order_company_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_order_company_fGid]);}
        }
        public const string _order_company_status__fName = "order_company_status_";       
        public static mdTableField FinProject_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_FinProject_fGid]);}
        }
        public const string _FinProject_status__fName = "FinProject_status_";       
        public static mdTableField company_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_company_fGid]);}
        }
        public const string _company_status__fName = "company_status_";
    }
    /// <summary>
    /// Схема таблиці 'Об'єкти фінансування'
    /// </summary>
    public static class FinancingObjects_TS_   
    {
        public const int __tGid = 15762;
        public const string __tName = "FinancingObjects";
        static mdTable m_mdTable;
        static FinancingObjects_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _gid_fGid = 31499;
       
        public static mdTableField gid
        {
            get { return __MD.Fields[_gid_fGid];}
        }
        public const string _gid_fName = "gid";        public const int _name_fGid = 31500;
       
        public static mdTableField name
        {
            get { return __MD.Fields[_name_fGid];}
        }
        public const string _name_fName = "name";        public const int _full_name_fGid = 31501;
       
        public static mdTableField full_name
        {
            get { return __MD.Fields[_full_name_fGid];}
        }
        public const string _full_name_fName = "full_name";        public const int _Description_fGid = 31502;
       
        public static mdTableField Description
        {
            get { return __MD.Fields[_Description_fGid];}
        }
        public const string _Description_fName = "Description";        public const int _responsible_person_fGid = 31503;
       
        public static mdTableField responsible_person
        {
            get { return __MD.Fields[_responsible_person_fGid];}
        }
        public const string _responsible_person_fName = "responsible_person";        public const int _comments_fGid = 31504;
       
        public static mdTableField comments
        {
            get { return __MD.Fields[_comments_fGid];}
        }
        public const string _comments_fName = "comments";        public const int _object_status_fGid = 31505;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _version_no_fGid = 31506;
       
        public static mdTableField version_no
        {
            get { return __MD.Fields[_version_no_fGid];}
        }
        public const string _version_no_fName = "version_no";        public const int _GroupOfObjects_fGid = 31512;
       
        public static mdTableField GroupOfObjects
        {
            get { return __MD.Fields[_GroupOfObjects_fGid];}
        }
        public const string _GroupOfObjects_fName = "GroupOfObjects";       
        public static mdTableField responsible_person_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_responsible_person_fGid]);}
        }
        public const string _responsible_person_view__fName = "responsible_person_view_";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";       
        public static mdTableField GroupOfObjects_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_GroupOfObjects_fGid]);}
        }
        public const string _GroupOfObjects_view__fName = "GroupOfObjects_view_";       
        public static mdTableField responsible_person_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_responsible_person_fGid]);}
        }
        public const string _responsible_person_status__fName = "responsible_person_status_";       
        public static mdTableField GroupOfObjects_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_GroupOfObjects_fGid]);}
        }
        public const string _GroupOfObjects_status__fName = "GroupOfObjects_status_";
    }
    /// <summary>
    /// Схема таблиці 'Групи об'єктів фінансування'
    /// </summary>
    public static class FinancingObjectsGroups_TS_   
    {
        public const int __tGid = 15763;
        public const string __tName = "FinancingObjectsGroups";
        static mdTable m_mdTable;
        static FinancingObjectsGroups_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _gid_fGid = 31507;
       
        public static mdTableField gid
        {
            get { return __MD.Fields[_gid_fGid];}
        }
        public const string _gid_fName = "gid";        public const int _name_fGid = 31508;
       
        public static mdTableField name
        {
            get { return __MD.Fields[_name_fGid];}
        }
        public const string _name_fName = "name";        public const int _object_status_fGid = 31509;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _comments_fGid = 31510;
       
        public static mdTableField comments
        {
            get { return __MD.Fields[_comments_fGid];}
        }
        public const string _comments_fName = "comments";        public const int _version_no_fGid = 31511;
       
        public static mdTableField version_no
        {
            get { return __MD.Fields[_version_no_fGid];}
        }
        public const string _version_no_fName = "version_no";        public const int _parent_group_fGid = 31513;
       
        public static mdTableField parent_group
        {
            get { return __MD.Fields[_parent_group_fGid];}
        }
        public const string _parent_group_fName = "parent_group";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";       
        public static mdTableField parent_group_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_parent_group_fGid]);}
        }
        public const string _parent_group_view__fName = "parent_group_view_";       
        public static mdTableField parent_group_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_parent_group_fGid]);}
        }
        public const string _parent_group_status__fName = "parent_group_status_";
    }
    /// <summary>
    /// Схема таблиці 'Залишки бюджетів'
    /// </summary>
    public static class FinBudjetRests_TS_   
    {
        public const int __tGid = 15498;
        public const string __tName = "FinBudjetRests";
        static mdTable m_mdTable;
        static FinBudjetRests_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _recorder_fGid = 26835;
       
        public static mdTableField recorder
        {
            get { return __MD.Fields[_recorder_fGid];}
        }
        public const string _recorder_fName = "recorder";        public const int _FinProject_fGid = 26836;
       
        public static mdTableField FinProject
        {
            get { return __MD.Fields[_FinProject_fGid];}
        }
        public const string _FinProject_fName = "FinProject";        public const int _Company_fGid = 26837;
       
        public static mdTableField Company
        {
            get { return __MD.Fields[_Company_fGid];}
        }
        public const string _Company_fName = "Company";        public const int _Resource_Flow_Item_fGid = 26838;
       
        public static mdTableField Resource_Flow_Item
        {
            get { return __MD.Fields[_Resource_Flow_Item_fGid];}
        }
        public const string _Resource_Flow_Item_fName = "Resource_Flow_Item";        public const int _Budjet_amount_fGid = 26839;
       
        public static mdTableField Budjet_amount
        {
            get { return __MD.Fields[_Budjet_amount_fGid];}
        }
        public const string _Budjet_amount_fName = "Budjet_amount";        public const int _Money_amount_fGid = 26840;
       
        public static mdTableField Money_amount
        {
            get { return __MD.Fields[_Money_amount_fGid];}
        }
        public const string _Money_amount_fName = "Money_amount";        public const int _is_outgo_fGid = 26862;
       
        public static mdTableField is_outgo
        {
            get { return __MD.Fields[_is_outgo_fGid];}
        }
        public const string _is_outgo_fName = "is_outgo";        public const int _reg_date_fGid = 26863;
       
        public static mdTableField reg_date
        {
            get { return __MD.Fields[_reg_date_fGid];}
        }
        public const string _reg_date_fName = "reg_date";        public const int _action_number_fGid = 26864;
       
        public static mdTableField action_number
        {
            get { return __MD.Fields[_action_number_fGid];}
        }
        public const string _action_number_fName = "action_number";        public const int _CorCompany_fGid = 26953;
       
        public static mdTableField CorCompany
        {
            get { return __MD.Fields[_CorCompany_fGid];}
        }
        public const string _CorCompany_fName = "CorCompany";        public const int _Contragent_fGid = 27108;
       
        public static mdTableField Contragent
        {
            get { return __MD.Fields[_Contragent_fGid];}
        }
        public const string _Contragent_fName = "Contragent";        public const int _comment_fGid = 27110;
       
        public static mdTableField comment
        {
            get { return __MD.Fields[_comment_fGid];}
        }
        public const string _comment_fName = "comment";        public const int _Quantity_fGid = 31357;
       
        public static mdTableField Quantity
        {
            get { return __MD.Fields[_Quantity_fGid];}
        }
        public const string _Quantity_fName = "Quantity";       
        public static mdTableField recorder_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_recorder_fGid]);}
        }
        public const string _recorder_view__fName = "recorder_view_";       
        public static mdTableField FinProject_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_FinProject_fGid]);}
        }
        public const string _FinProject_view__fName = "FinProject_view_";       
        public static mdTableField Company_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Company_fGid]);}
        }
        public const string _Company_view__fName = "Company_view_";       
        public static mdTableField Resource_Flow_Item_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Resource_Flow_Item_fGid]);}
        }
        public const string _Resource_Flow_Item_view__fName = "Resource_Flow_Item_view_";       
        public static mdTableField CorCompany_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_CorCompany_fGid]);}
        }
        public const string _CorCompany_view__fName = "CorCompany_view_";       
        public static mdTableField Contragent_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Contragent_fGid]);}
        }
        public const string _Contragent_view__fName = "Contragent_view_";       
        public static mdTableField recorder_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_recorder_fGid]);}
        }
        public const string _recorder_status__fName = "recorder_status_";       
        public static mdTableField FinProject_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_FinProject_fGid]);}
        }
        public const string _FinProject_status__fName = "FinProject_status_";       
        public static mdTableField Company_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Company_fGid]);}
        }
        public const string _Company_status__fName = "Company_status_";       
        public static mdTableField Resource_Flow_Item_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Resource_Flow_Item_fGid]);}
        }
        public const string _Resource_Flow_Item_status__fName = "Resource_Flow_Item_status_";       
        public static mdTableField CorCompany_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_CorCompany_fGid]);}
        }
        public const string _CorCompany_status__fName = "CorCompany_status_";       
        public static mdTableField Contragent_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Contragent_fGid]);}
        }
        public const string _Contragent_status__fName = "Contragent_status_";
    }
    /// <summary>
    /// Схема таблиці 'Підсумки фінансових показників'
    /// </summary>
    public static class FinBudjetRests_dynamic_totals_TS_   
    {
        public const int __tGid = 15500;
        public const string __tName = "FinBudjetRests_dynamic_totals";
        static mdTable m_mdTable;
        static FinBudjetRests_dynamic_totals_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _FinProject_fGid = 26851;
       
        public static mdTableField FinProject
        {
            get { return __MD.Fields[_FinProject_fGid];}
        }
        public const string _FinProject_fName = "FinProject";        public const int _Company_fGid = 26852;
       
        public static mdTableField Company
        {
            get { return __MD.Fields[_Company_fGid];}
        }
        public const string _Company_fName = "Company";        public const int _Resource_Flow_Item_fGid = 26853;
       
        public static mdTableField Resource_Flow_Item
        {
            get { return __MD.Fields[_Resource_Flow_Item_fGid];}
        }
        public const string _Resource_Flow_Item_fName = "Resource_Flow_Item";        public const int _Budjet_amount_in_fGid = 26854;
       
        public static mdTableField Budjet_amount_in
        {
            get { return __MD.Fields[_Budjet_amount_in_fGid];}
        }
        public const string _Budjet_amount_in_fName = "Budjet_amount_in";        public const int _Budjet_amount_out_fGid = 26855;
       
        public static mdTableField Budjet_amount_out
        {
            get { return __MD.Fields[_Budjet_amount_out_fGid];}
        }
        public const string _Budjet_amount_out_fName = "Budjet_amount_out";        public const int _Budjet_amount_rest_fGid = 26856;
       
        public static mdTableField Budjet_amount_rest
        {
            get { return __MD.Fields[_Budjet_amount_rest_fGid];}
        }
        public const string _Budjet_amount_rest_fName = "Budjet_amount_rest";        public const int _Budjet_amount_initial_rest_fGid = 26857;
       
        public static mdTableField Budjet_amount_initial_rest
        {
            get { return __MD.Fields[_Budjet_amount_initial_rest_fGid];}
        }
        public const string _Budjet_amount_initial_rest_fName = "Budjet_amount_initial_rest";        public const int _Money_amount_in_fGid = 26858;
       
        public static mdTableField Money_amount_in
        {
            get { return __MD.Fields[_Money_amount_in_fGid];}
        }
        public const string _Money_amount_in_fName = "Money_amount_in";        public const int _Money_amount_out_fGid = 26859;
       
        public static mdTableField Money_amount_out
        {
            get { return __MD.Fields[_Money_amount_out_fGid];}
        }
        public const string _Money_amount_out_fName = "Money_amount_out";        public const int _Budjet_amount_tin_fGid = 26865;
       
        public static mdTableField Budjet_amount_tin
        {
            get { return __MD.Fields[_Budjet_amount_tin_fGid];}
        }
        public const string _Budjet_amount_tin_fName = "Budjet_amount_tin";        public const int _Budjet_amount_tout_fGid = 26866;
       
        public static mdTableField Budjet_amount_tout
        {
            get { return __MD.Fields[_Budjet_amount_tout_fGid];}
        }
        public const string _Budjet_amount_tout_fName = "Budjet_amount_tout";        public const int _Money_amount_tin_fGid = 26867;
       
        public static mdTableField Money_amount_tin
        {
            get { return __MD.Fields[_Money_amount_tin_fGid];}
        }
        public const string _Money_amount_tin_fName = "Money_amount_tin";        public const int _Money_amount_tout_fGid = 26868;
       
        public static mdTableField Money_amount_tout
        {
            get { return __MD.Fields[_Money_amount_tout_fGid];}
        }
        public const string _Money_amount_tout_fName = "Money_amount_tout";        public const int _RestOfIncoms_fGid = 26869;
       
        public static mdTableField RestOfIncoms
        {
            get { return __MD.Fields[_RestOfIncoms_fGid];}
        }
        public const string _RestOfIncoms_fName = "RestOfIncoms";        public const int _PercentOfIncoms_fGid = 26870;
       
        public static mdTableField PercentOfIncoms
        {
            get { return __MD.Fields[_PercentOfIncoms_fGid];}
        }
        public const string _PercentOfIncoms_fName = "PercentOfIncoms";        public const int _RestOfPayments_fGid = 26871;
       
        public static mdTableField RestOfPayments
        {
            get { return __MD.Fields[_RestOfPayments_fGid];}
        }
        public const string _RestOfPayments_fName = "RestOfPayments";        public const int _PercentOfPayments_fGid = 26872;
       
        public static mdTableField PercentOfPayments
        {
            get { return __MD.Fields[_PercentOfPayments_fGid];}
        }
        public const string _PercentOfPayments_fName = "PercentOfPayments";        public const int _PercentOfExecution_fGid = 26873;
       
        public static mdTableField PercentOfExecution
        {
            get { return __MD.Fields[_PercentOfExecution_fGid];}
        }
        public const string _PercentOfExecution_fName = "PercentOfExecution";        public const int _recorder_fGid = 26874;
       
        public static mdTableField recorder
        {
            get { return __MD.Fields[_recorder_fGid];}
        }
        public const string _recorder_fName = "recorder";        public const int _reg_date_fGid = 26875;
       
        public static mdTableField reg_date
        {
            get { return __MD.Fields[_reg_date_fGid];}
        }
        public const string _reg_date_fName = "reg_date";        public const int _Item_Group_fGid = 26952;
       
        public static mdTableField Item_Group
        {
            get { return __MD.Fields[_Item_Group_fGid];}
        }
        public const string _Item_Group_fName = "Item_Group";        public const int _CorCompany_fGid = 26955;
       
        public static mdTableField CorCompany
        {
            get { return __MD.Fields[_CorCompany_fGid];}
        }
        public const string _CorCompany_fName = "CorCompany";        public const int _Contragent_fGid = 27109;
       
        public static mdTableField Contragent
        {
            get { return __MD.Fields[_Contragent_fGid];}
        }
        public const string _Contragent_fName = "Contragent";        public const int _comment_fGid = 27111;
       
        public static mdTableField comment
        {
            get { return __MD.Fields[_comment_fGid];}
        }
        public const string _comment_fName = "comment";        public const int _company_as_contragent_fGid = 27525;
       
        public static mdTableField company_as_contragent
        {
            get { return __MD.Fields[_company_as_contragent_fGid];}
        }
        public const string _company_as_contragent_fName = "company_as_contragent";        public const int _ComObjArea_fGid = 29735;
       
        public static mdTableField ComObjArea
        {
            get { return __MD.Fields[_ComObjArea_fGid];}
        }
        public const string _ComObjArea_fName = "ComObjArea";        public const int _Money_tout_m2_fGid = 29736;
       
        public static mdTableField Money_tout_m2
        {
            get { return __MD.Fields[_Money_tout_m2_fGid];}
        }
        public const string _Money_tout_m2_fName = "Money_tout_m2";        public const int _Quantity_in_fGid = 31361;
       
        public static mdTableField Quantity_in
        {
            get { return __MD.Fields[_Quantity_in_fGid];}
        }
        public const string _Quantity_in_fName = "Quantity_in";        public const int _Quantity_out_fGid = 31362;
       
        public static mdTableField Quantity_out
        {
            get { return __MD.Fields[_Quantity_out_fGid];}
        }
        public const string _Quantity_out_fName = "Quantity_out";        public const int _Quantity_rest_fGid = 31364;
       
        public static mdTableField Quantity_rest
        {
            get { return __MD.Fields[_Quantity_rest_fGid];}
        }
        public const string _Quantity_rest_fName = "Quantity_rest";        public const int _Quantity_initial_rest_fGid = 31365;
       
        public static mdTableField Quantity_initial_rest
        {
            get { return __MD.Fields[_Quantity_initial_rest_fGid];}
        }
        public const string _Quantity_initial_rest_fName = "Quantity_initial_rest";        public const int _operation_type_fGid = 31912;
       
        public static mdTableField operation_type
        {
            get { return __MD.Fields[_operation_type_fGid];}
        }
        public const string _operation_type_fName = "operation_type";        public const int _doc_number_fGid = 31913;
       
        public static mdTableField doc_number
        {
            get { return __MD.Fields[_doc_number_fGid];}
        }
        public const string _doc_number_fName = "doc_number";       
        public static mdTableField FinProject_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_FinProject_fGid]);}
        }
        public const string _FinProject_view__fName = "FinProject_view_";       
        public static mdTableField Company_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Company_fGid]);}
        }
        public const string _Company_view__fName = "Company_view_";       
        public static mdTableField Resource_Flow_Item_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Resource_Flow_Item_fGid]);}
        }
        public const string _Resource_Flow_Item_view__fName = "Resource_Flow_Item_view_";       
        public static mdTableField recorder_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_recorder_fGid]);}
        }
        public const string _recorder_view__fName = "recorder_view_";       
        public static mdTableField Item_Group_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Item_Group_fGid]);}
        }
        public const string _Item_Group_view__fName = "Item_Group_view_";       
        public static mdTableField CorCompany_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_CorCompany_fGid]);}
        }
        public const string _CorCompany_view__fName = "CorCompany_view_";       
        public static mdTableField Contragent_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Contragent_fGid]);}
        }
        public const string _Contragent_view__fName = "Contragent_view_";       
        public static mdTableField company_as_contragent_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_company_as_contragent_fGid]);}
        }
        public const string _company_as_contragent_view__fName = "company_as_contragent_view_";       
        public static mdTableField operation_type_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_operation_type_fGid]);}
        }
        public const string _operation_type_view__fName = "operation_type_view_";
    }
    /// <summary>
    /// Схема таблиці 'Залишки бюджетів'
    /// </summary>
    public static class FinBudjetRests_totals_TS_   
    {
        public const int __tGid = 15499;
        public const string __tName = "FinBudjetRests_totals";
        static mdTable m_mdTable;
        static FinBudjetRests_totals_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _FinProject_fGid = 26841;
       
        public static mdTableField FinProject
        {
            get { return __MD.Fields[_FinProject_fGid];}
        }
        public const string _FinProject_fName = "FinProject";        public const int _Company_fGid = 26842;
       
        public static mdTableField Company
        {
            get { return __MD.Fields[_Company_fGid];}
        }
        public const string _Company_fName = "Company";        public const int _Resource_Flow_Item_fGid = 26843;
       
        public static mdTableField Resource_Flow_Item
        {
            get { return __MD.Fields[_Resource_Flow_Item_fGid];}
        }
        public const string _Resource_Flow_Item_fName = "Resource_Flow_Item";        public const int _Budjet_amount_in_fGid = 26844;
       
        public static mdTableField Budjet_amount_in
        {
            get { return __MD.Fields[_Budjet_amount_in_fGid];}
        }
        public const string _Budjet_amount_in_fName = "Budjet_amount_in";        public const int _Budjet_amount_out_fGid = 26845;
       
        public static mdTableField Budjet_amount_out
        {
            get { return __MD.Fields[_Budjet_amount_out_fGid];}
        }
        public const string _Budjet_amount_out_fName = "Budjet_amount_out";        public const int _Budjet_amount_rest_fGid = 26846;
       
        public static mdTableField Budjet_amount_rest
        {
            get { return __MD.Fields[_Budjet_amount_rest_fGid];}
        }
        public const string _Budjet_amount_rest_fName = "Budjet_amount_rest";        public const int _Money_amount_in_fGid = 26847;
       
        public static mdTableField Money_amount_in
        {
            get { return __MD.Fields[_Money_amount_in_fGid];}
        }
        public const string _Money_amount_in_fName = "Money_amount_in";        public const int _Money_amount_out_fGid = 26848;
       
        public static mdTableField Money_amount_out
        {
            get { return __MD.Fields[_Money_amount_out_fGid];}
        }
        public const string _Money_amount_out_fName = "Money_amount_out";        public const int _operation_period_fGid = 26850;
       
        public static mdTableField operation_period
        {
            get { return __MD.Fields[_operation_period_fGid];}
        }
        public const string _operation_period_fName = "operation_period";        public const int _CorCompany_fGid = 26954;
       
        public static mdTableField CorCompany
        {
            get { return __MD.Fields[_CorCompany_fGid];}
        }
        public const string _CorCompany_fName = "CorCompany";        public const int _Quantity_in_fGid = 31359;
       
        public static mdTableField Quantity_in
        {
            get { return __MD.Fields[_Quantity_in_fGid];}
        }
        public const string _Quantity_in_fName = "Quantity_in";        public const int _Quantity_out_fGid = 31360;
       
        public static mdTableField Quantity_out
        {
            get { return __MD.Fields[_Quantity_out_fGid];}
        }
        public const string _Quantity_out_fName = "Quantity_out";        public const int _Quantity_rest_fGid = 31363;
       
        public static mdTableField Quantity_rest
        {
            get { return __MD.Fields[_Quantity_rest_fGid];}
        }
        public const string _Quantity_rest_fName = "Quantity_rest";       
        public static mdTableField FinProject_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_FinProject_fGid]);}
        }
        public const string _FinProject_view__fName = "FinProject_view_";       
        public static mdTableField Company_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Company_fGid]);}
        }
        public const string _Company_view__fName = "Company_view_";       
        public static mdTableField Resource_Flow_Item_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Resource_Flow_Item_fGid]);}
        }
        public const string _Resource_Flow_Item_view__fName = "Resource_Flow_Item_view_";       
        public static mdTableField operation_period_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_operation_period_fGid]);}
        }
        public const string _operation_period_view__fName = "operation_period_view_";       
        public static mdTableField CorCompany_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_CorCompany_fGid]);}
        }
        public const string _CorCompany_view__fName = "CorCompany_view_";       
        public static mdTableField FinProject_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_FinProject_fGid]);}
        }
        public const string _FinProject_status__fName = "FinProject_status_";       
        public static mdTableField Company_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Company_fGid]);}
        }
        public const string _Company_status__fName = "Company_status_";       
        public static mdTableField Resource_Flow_Item_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Resource_Flow_Item_fGid]);}
        }
        public const string _Resource_Flow_Item_status__fName = "Resource_Flow_Item_status_";       
        public static mdTableField operation_period_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_operation_period_fGid]);}
        }
        public const string _operation_period_status__fName = "operation_period_status_";       
        public static mdTableField CorCompany_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_CorCompany_fGid]);}
        }
        public const string _CorCompany_status__fName = "CorCompany_status_";
    }
    /// <summary>
    /// Схема таблиці 'Проекти'
    /// </summary>
    public static class FinProjects_TS_   
    {
        public const int __tGid = 15461;
        public const string __tName = "FinProjects";
        static mdTable m_mdTable;
        static FinProjects_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _gid_fGid = 26386;
       
        public static mdTableField gid
        {
            get { return __MD.Fields[_gid_fGid];}
        }
        public const string _gid_fName = "gid";        public const int _name_fGid = 26387;
       
        public static mdTableField name
        {
            get { return __MD.Fields[_name_fGid];}
        }
        public const string _name_fName = "name";        public const int _version_no_fGid = 26388;
       
        public static mdTableField version_no
        {
            get { return __MD.Fields[_version_no_fGid];}
        }
        public const string _version_no_fName = "version_no";        public const int _object_status_fGid = 26389;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _Description_fGid = 26390;
       
        public static mdTableField Description
        {
            get { return __MD.Fields[_Description_fGid];}
        }
        public const string _Description_fName = "Description";        public const int _comments_fGid = 26391;
       
        public static mdTableField comments
        {
            get { return __MD.Fields[_comments_fGid];}
        }
        public const string _comments_fName = "comments";        public const int _asid_fGid = 27328;
       
        public static mdTableField asid
        {
            get { return __MD.Fields[_asid_fGid];}
        }
        public const string _asid_fName = "asid";        public const int _Project_Group_fGid = 27373;
       
        public static mdTableField Project_Group
        {
            get { return __MD.Fields[_Project_Group_fGid];}
        }
        public const string _Project_Group_fName = "Project_Group";        public const int _ComObjArea_fGid = 27705;
       
        public static mdTableField ComObjArea
        {
            get { return __MD.Fields[_ComObjArea_fGid];}
        }
        public const string _ComObjArea_fName = "ComObjArea";        public const int _ObjArea_fGid = 28691;
       
        public static mdTableField ObjArea
        {
            get { return __MD.Fields[_ObjArea_fGid];}
        }
        public const string _ObjArea_fName = "ObjArea";        public const int _FlatsArea_fGid = 28692;
       
        public static mdTableField FlatsArea
        {
            get { return __MD.Fields[_FlatsArea_fGid];}
        }
        public const string _FlatsArea_fName = "FlatsArea";        public const int _ProjCost_fGid = 28693;
       
        public static mdTableField ProjCost
        {
            get { return __MD.Fields[_ProjCost_fGid];}
        }
        public const string _ProjCost_fName = "ProjCost";        public const int _CurAssetsCost_fGid = 28694;
       
        public static mdTableField CurAssetsCost
        {
            get { return __MD.Fields[_CurAssetsCost_fGid];}
        }
        public const string _CurAssetsCost_fName = "CurAssetsCost";        public const int _Budjet_amount_tin_fGid = 28729;
       
        public static mdTableField Budjet_amount_tin
        {
            get { return __MD.Fields[_Budjet_amount_tin_fGid];}
        }
        public const string _Budjet_amount_tin_fName = "Budjet_amount_tin";        public const int _Money_amount_tout_fGid = 28730;
       
        public static mdTableField Money_amount_tout
        {
            get { return __MD.Fields[_Money_amount_tout_fGid];}
        }
        public const string _Money_amount_tout_fName = "Money_amount_tout";        public const int _Budjet_amount_tout_fGid = 28731;
       
        public static mdTableField Budjet_amount_tout
        {
            get { return __MD.Fields[_Budjet_amount_tout_fGid];}
        }
        public const string _Budjet_amount_tout_fName = "Budjet_amount_tout";        public const int _IncomeByAssets_fGid = 28732;
       
        public static mdTableField IncomeByAssets
        {
            get { return __MD.Fields[_IncomeByAssets_fGid];}
        }
        public const string _IncomeByAssets_fName = "IncomeByAssets";        public const int _SaleAmount_fGid = 28733;
       
        public static mdTableField SaleAmount
        {
            get { return __MD.Fields[_SaleAmount_fGid];}
        }
        public const string _SaleAmount_fName = "SaleAmount";        public const int _responsible_person_fGid = 28935;
       
        public static mdTableField responsible_person
        {
            get { return __MD.Fields[_responsible_person_fGid];}
        }
        public const string _responsible_person_fName = "responsible_person";        public const int _ParentProject_fGid = 31760;
       
        public static mdTableField ParentProject
        {
            get { return __MD.Fields[_ParentProject_fGid];}
        }
        public const string _ParentProject_fName = "ParentProject";        public const int _CurPersonIsMember_fGid = 31786;
       
        public static mdTableField CurPersonIsMember
        {
            get { return __MD.Fields[_CurPersonIsMember_fGid];}
        }
        public const string _CurPersonIsMember_fName = "CurPersonIsMember";        public const int _RequestedMembership_fGid = 31787;
       
        public static mdTableField RequestedMembership
        {
            get { return __MD.Fields[_RequestedMembership_fGid];}
        }
        public const string _RequestedMembership_fName = "RequestedMembership";        public const int _MembersGroup_fGid = 31842;
       
        public static mdTableField MembersGroup
        {
            get { return __MD.Fields[_MembersGroup_fGid];}
        }
        public const string _MembersGroup_fName = "MembersGroup";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";       
        public static mdTableField Project_Group_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Project_Group_fGid]);}
        }
        public const string _Project_Group_view__fName = "Project_Group_view_";       
        public static mdTableField responsible_person_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_responsible_person_fGid]);}
        }
        public const string _responsible_person_view__fName = "responsible_person_view_";       
        public static mdTableField ParentProject_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_ParentProject_fGid]);}
        }
        public const string _ParentProject_view__fName = "ParentProject_view_";       
        public static mdTableField MembersGroup_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_MembersGroup_fGid]);}
        }
        public const string _MembersGroup_view__fName = "MembersGroup_view_";       
        public static mdTableField Project_Group_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Project_Group_fGid]);}
        }
        public const string _Project_Group_status__fName = "Project_Group_status_";       
        public static mdTableField responsible_person_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_responsible_person_fGid]);}
        }
        public const string _responsible_person_status__fName = "responsible_person_status_";       
        public static mdTableField ParentProject_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_ParentProject_fGid]);}
        }
        public const string _ParentProject_status__fName = "ParentProject_status_";       
        public static mdTableField MembersGroup_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_MembersGroup_fGid]);}
        }
        public const string _MembersGroup_status__fName = "MembersGroup_status_";
    }
    /// <summary>
    /// Схема таблиці 'Групи фінансових проектів'
    /// </summary>
    public static class FinProjects_Groups_TS_   
    {
        public const int __tGid = 15533;
        public const string __tName = "FinProjects_Groups";
        static mdTable m_mdTable;
        static FinProjects_Groups_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _gid_fGid = 27367;
       
        public static mdTableField gid
        {
            get { return __MD.Fields[_gid_fGid];}
        }
        public const string _gid_fName = "gid";        public const int _name_fGid = 27368;
       
        public static mdTableField name
        {
            get { return __MD.Fields[_name_fGid];}
        }
        public const string _name_fName = "name";        public const int _object_status_fGid = 27369;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _comments_fGid = 27370;
       
        public static mdTableField comments
        {
            get { return __MD.Fields[_comments_fGid];}
        }
        public const string _comments_fName = "comments";        public const int _version_no_fGid = 27371;
       
        public static mdTableField version_no
        {
            get { return __MD.Fields[_version_no_fGid];}
        }
        public const string _version_no_fName = "version_no";        public const int _parent_group_fGid = 27372;
       
        public static mdTableField parent_group
        {
            get { return __MD.Fields[_parent_group_fGid];}
        }
        public const string _parent_group_fName = "parent_group";        public const int _RowsCount_fGid = 30885;
       
        public static mdTableField RowsCount
        {
            get { return __MD.Fields[_RowsCount_fGid];}
        }
        public const string _RowsCount_fName = "RowsCount";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";       
        public static mdTableField parent_group_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_parent_group_fGid]);}
        }
        public const string _parent_group_view__fName = "parent_group_view_";       
        public static mdTableField parent_group_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_parent_group_fGid]);}
        }
        public const string _parent_group_status__fName = "parent_group_status_";
    }
    /// <summary>
    /// Схема таблиці 'Заявки на фінансування'
    /// </summary>
    public static class FundingOrder_TS_   
    {
        public const int __tGid = 15716;
        public const string __tName = "FundingOrder";
        static mdTable m_mdTable;
        static FundingOrder_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _document_fGid = 30439;
       
        public static mdTableField document
        {
            get { return __MD.Fields[_document_fGid];}
        }
        public const string _document_fName = "document";        public const int _reg_date_fGid = 30440;
       
        public static mdTableField reg_date
        {
            get { return __MD.Fields[_reg_date_fGid];}
        }
        public const string _reg_date_fName = "reg_date";        public const int _doc_number_fGid = 30441;
       
        public static mdTableField doc_number
        {
            get { return __MD.Fields[_doc_number_fGid];}
        }
        public const string _doc_number_fName = "doc_number";        public const int _company_fGid = 30442;
       
        public static mdTableField company
        {
            get { return __MD.Fields[_company_fGid];}
        }
        public const string _company_fName = "company";        public const int _FinProject_fGid = 30443;
       
        public static mdTableField FinProject
        {
            get { return __MD.Fields[_FinProject_fGid];}
        }
        public const string _FinProject_fName = "FinProject";        public const int _contragent_fGid = 30444;
       
        public static mdTableField contragent
        {
            get { return __MD.Fields[_contragent_fGid];}
        }
        public const string _contragent_fName = "contragent";        public const int _Amount_fGid = 30445;
       
        public static mdTableField Amount
        {
            get { return __MD.Fields[_Amount_fGid];}
        }
        public const string _Amount_fName = "Amount";        public const int _PaymentTerm_fGid = 30446;
       
        public static mdTableField PaymentTerm
        {
            get { return __MD.Fields[_PaymentTerm_fGid];}
        }
        public const string _PaymentTerm_fName = "PaymentTerm";        public const int _comments_fGid = 30447;
       
        public static mdTableField comments
        {
            get { return __MD.Fields[_comments_fGid];}
        }
        public const string _comments_fName = "comments";        public const int _object_status_fGid = 30448;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _Resource_Flow_Item_fGid = 30449;
       
        public static mdTableField Resource_Flow_Item
        {
            get { return __MD.Fields[_Resource_Flow_Item_fGid];}
        }
        public const string _Resource_Flow_Item_fName = "Resource_Flow_Item";        public const int _Contract_fGid = 30485;
       
        public static mdTableField Contract
        {
            get { return __MD.Fields[_Contract_fGid];}
        }
        public const string _Contract_fName = "Contract";        public const int _purpose_of_payment_fGid = 30664;
       
        public static mdTableField purpose_of_payment
        {
            get { return __MD.Fields[_purpose_of_payment_fGid];}
        }
        public const string _purpose_of_payment_fName = "purpose_of_payment";        public const int _base_document_fGid = 30825;
       
        public static mdTableField base_document
        {
            get { return __MD.Fields[_base_document_fGid];}
        }
        public const string _base_document_fName = "base_document";        public const int _amount_out_fGid = 30857;
       
        public static mdTableField amount_out
        {
            get { return __MD.Fields[_amount_out_fGid];}
        }
        public const string _amount_out_fName = "amount_out";        public const int _IsApproved_fGid = 31367;
       
        public static mdTableField IsApproved
        {
            get { return __MD.Fields[_IsApproved_fGid];}
        }
        public const string _IsApproved_fName = "IsApproved";        public const int _IsRejected_fGid = 31368;
       
        public static mdTableField IsRejected
        {
            get { return __MD.Fields[_IsRejected_fGid];}
        }
        public const string _IsRejected_fName = "IsRejected";        public const int _IsApproveExpected_fGid = 31396;
       
        public static mdTableField IsApproveExpected
        {
            get { return __MD.Fields[_IsApproveExpected_fGid];}
        }
        public const string _IsApproveExpected_fName = "IsApproveExpected";        public const int _FinancingObject_fGid = 31514;
       
        public static mdTableField FinancingObject
        {
            get { return __MD.Fields[_FinancingObject_fGid];}
        }
        public const string _FinancingObject_fName = "FinancingObject";        public const int _author_fGid = 31792;
       
        public static mdTableField author
        {
            get { return __MD.Fields[_author_fGid];}
        }
        public const string _author_fName = "author";       
        public static mdTableField document_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_document_fGid]);}
        }
        public const string _document_view__fName = "document_view_";       
        public static mdTableField company_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_company_fGid]);}
        }
        public const string _company_view__fName = "company_view_";       
        public static mdTableField FinProject_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_FinProject_fGid]);}
        }
        public const string _FinProject_view__fName = "FinProject_view_";       
        public static mdTableField contragent_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_contragent_fGid]);}
        }
        public const string _contragent_view__fName = "contragent_view_";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";       
        public static mdTableField Resource_Flow_Item_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Resource_Flow_Item_fGid]);}
        }
        public const string _Resource_Flow_Item_view__fName = "Resource_Flow_Item_view_";       
        public static mdTableField Contract_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Contract_fGid]);}
        }
        public const string _Contract_view__fName = "Contract_view_";       
        public static mdTableField base_document_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_base_document_fGid]);}
        }
        public const string _base_document_view__fName = "base_document_view_";       
        public static mdTableField FinancingObject_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_FinancingObject_fGid]);}
        }
        public const string _FinancingObject_view__fName = "FinancingObject_view_";       
        public static mdTableField author_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_author_fGid]);}
        }
        public const string _author_view__fName = "author_view_";       
        public static mdTableField company_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_company_fGid]);}
        }
        public const string _company_status__fName = "company_status_";       
        public static mdTableField FinProject_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_FinProject_fGid]);}
        }
        public const string _FinProject_status__fName = "FinProject_status_";       
        public static mdTableField contragent_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_contragent_fGid]);}
        }
        public const string _contragent_status__fName = "contragent_status_";       
        public static mdTableField Resource_Flow_Item_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Resource_Flow_Item_fGid]);}
        }
        public const string _Resource_Flow_Item_status__fName = "Resource_Flow_Item_status_";       
        public static mdTableField Contract_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Contract_fGid]);}
        }
        public const string _Contract_status__fName = "Contract_status_";       
        public static mdTableField base_document_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_base_document_fGid]);}
        }
        public const string _base_document_status__fName = "base_document_status_";       
        public static mdTableField FinancingObject_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_FinancingObject_fGid]);}
        }
        public const string _FinancingObject_status__fName = "FinancingObject_status_";       
        public static mdTableField author_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_author_fGid]);}
        }
        public const string _author_status__fName = "author_status_";
    }
    /// <summary>
    /// Схема таблиці 'Відомість руху коштів'
    /// </summary>
    public static class MoneyMovementList_TS_   
    {
        public const int __tGid = 15464;
        public const string __tName = "MoneyMovementList";
        static mdTable m_mdTable;
        static MoneyMovementList_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _document_fGid = 26410;
       
        public static mdTableField document
        {
            get { return __MD.Fields[_document_fGid];}
        }
        public const string _document_fName = "document";        public const int _company_fGid = 26411;
       
        public static mdTableField company
        {
            get { return __MD.Fields[_company_fGid];}
        }
        public const string _company_fName = "company";        public const int _reg_date_fGid = 26412;
       
        public static mdTableField reg_date
        {
            get { return __MD.Fields[_reg_date_fGid];}
        }
        public const string _reg_date_fName = "reg_date";        public const int _doc_number_fGid = 26413;
       
        public static mdTableField doc_number
        {
            get { return __MD.Fields[_doc_number_fGid];}
        }
        public const string _doc_number_fName = "doc_number";        public const int _comments_fGid = 26832;
       
        public static mdTableField comments
        {
            get { return __MD.Fields[_comments_fGid];}
        }
        public const string _comments_fName = "comments";        public const int _amount_in_fGid = 26833;
       
        public static mdTableField amount_in
        {
            get { return __MD.Fields[_amount_in_fGid];}
        }
        public const string _amount_in_fName = "amount_in";        public const int _amount_out_fGid = 26834;
       
        public static mdTableField amount_out
        {
            get { return __MD.Fields[_amount_out_fGid];}
        }
        public const string _amount_out_fName = "amount_out";        public const int _amount_inrest_fGid = 26876;
       
        public static mdTableField amount_inrest
        {
            get { return __MD.Fields[_amount_inrest_fGid];}
        }
        public const string _amount_inrest_fName = "amount_inrest";        public const int _amount_rest_fGid = 26877;
       
        public static mdTableField amount_rest
        {
            get { return __MD.Fields[_amount_rest_fGid];}
        }
        public const string _amount_rest_fName = "amount_rest";        public const int _Money_account_fGid = 27099;
       
        public static mdTableField Money_account
        {
            get { return __MD.Fields[_Money_account_fGid];}
        }
        public const string _Money_account_fName = "Money_account";        public const int _Total_Amount_fGid = 27138;
       
        public static mdTableField Total_Amount
        {
            get { return __MD.Fields[_Total_Amount_fGid];}
        }
        public const string _Total_Amount_fName = "Total_Amount";       
        public static mdTableField document_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_document_fGid]);}
        }
        public const string _document_view__fName = "document_view_";       
        public static mdTableField company_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_company_fGid]);}
        }
        public const string _company_view__fName = "company_view_";       
        public static mdTableField Money_account_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Money_account_fGid]);}
        }
        public const string _Money_account_view__fName = "Money_account_view_";       
        public static mdTableField document_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_document_fGid]);}
        }
        public const string _document_status__fName = "document_status_";       
        public static mdTableField company_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_company_fGid]);}
        }
        public const string _company_status__fName = "company_status_";       
        public static mdTableField Money_account_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Money_account_fGid]);}
        }
        public const string _Money_account_status__fName = "Money_account_status_";
    }
    /// <summary>
    /// Схема таблиці 'Т.Ч. Відомостей платежів'
    /// </summary>
    public static class MoneyMovementList_tp_TS_   
    {
        public const int __tGid = 15465;
        public const string __tName = "MoneyMovementList_tp";
        static mdTable m_mdTable;
        static MoneyMovementList_tp_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _document_fGid = 26414;
       
        public static mdTableField document
        {
            get { return __MD.Fields[_document_fGid];}
        }
        public const string _document_fName = "document";        public const int _Resource_Flow_Item_fGid = 26415;
       
        public static mdTableField Resource_Flow_Item
        {
            get { return __MD.Fields[_Resource_Flow_Item_fGid];}
        }
        public const string _Resource_Flow_Item_fName = "Resource_Flow_Item";        public const int _ordinal_fGid = 26416;
       
        public static mdTableField ordinal
        {
            get { return __MD.Fields[_ordinal_fGid];}
        }
        public const string _ordinal_fName = "ordinal";        public const int _amount_in_fGid = 26417;
       
        public static mdTableField amount_in
        {
            get { return __MD.Fields[_amount_in_fGid];}
        }
        public const string _amount_in_fName = "amount_in";        public const int _comment_fGid = 26418;
       
        public static mdTableField comment
        {
            get { return __MD.Fields[_comment_fGid];}
        }
        public const string _comment_fName = "comment";        public const int _amount_out_fGid = 26419;
       
        public static mdTableField amount_out
        {
            get { return __MD.Fields[_amount_out_fGid];}
        }
        public const string _amount_out_fName = "amount_out";        public const int _FinProject_fGid = 26420;
       
        public static mdTableField FinProject
        {
            get { return __MD.Fields[_FinProject_fGid];}
        }
        public const string _FinProject_fName = "FinProject";        public const int _Company_fGid = 26731;
       
        public static mdTableField Company
        {
            get { return __MD.Fields[_Company_fGid];}
        }
        public const string _Company_fName = "Company";        public const int _reg_date_fGid = 26732;
       
        public static mdTableField reg_date
        {
            get { return __MD.Fields[_reg_date_fGid];}
        }
        public const string _reg_date_fName = "reg_date";        public const int _object_status_fGid = 26733;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _contragent_fGid = 27102;
       
        public static mdTableField contragent
        {
            get { return __MD.Fields[_contragent_fGid];}
        }
        public const string _contragent_fName = "contragent";        public const int _Payment_Doc_Number_fGid = 27155;
       
        public static mdTableField Payment_Doc_Number
        {
            get { return __MD.Fields[_Payment_Doc_Number_fGid];}
        }
        public const string _Payment_Doc_Number_fName = "Payment_Doc_Number";        public const int _BaseDocument_fGid = 28537;
       
        public static mdTableField BaseDocument
        {
            get { return __MD.Fields[_BaseDocument_fGid];}
        }
        public const string _BaseDocument_fName = "BaseDocument";        public const int _ProjectAsset_fGid = 28725;
       
        public static mdTableField ProjectAsset
        {
            get { return __MD.Fields[_ProjectAsset_fGid];}
        }
        public const string _ProjectAsset_fName = "ProjectAsset";        public const int _ProjectAsset_rented_fGid = 28726;
       
        public static mdTableField ProjectAsset_rented
        {
            get { return __MD.Fields[_ProjectAsset_rented_fGid];}
        }
        public const string _ProjectAsset_rented_fName = "ProjectAsset_rented";        public const int _RowSubKey_fGid = 29216;
       
        public static mdTableField RowSubKey
        {
            get { return __MD.Fields[_RowSubKey_fGid];}
        }
        public const string _RowSubKey_fName = "RowSubKey";        public const int _flow_amount_fGid = 29624;
       
        public static mdTableField flow_amount
        {
            get { return __MD.Fields[_flow_amount_fGid];}
        }
        public const string _flow_amount_fName = "flow_amount";        public const int _SheduledPayment_fGid = 30331;
       
        public static mdTableField SheduledPayment
        {
            get { return __MD.Fields[_SheduledPayment_fGid];}
        }
        public const string _SheduledPayment_fName = "SheduledPayment";        public const int _SheduledPaymentAmount_fGid = 30332;
       
        public static mdTableField SheduledPaymentAmount
        {
            get { return __MD.Fields[_SheduledPaymentAmount_fGid];}
        }
        public const string _SheduledPaymentAmount_fName = "SheduledPaymentAmount";       
        public static mdTableField document_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_document_fGid]);}
        }
        public const string _document_view__fName = "document_view_";       
        public static mdTableField Resource_Flow_Item_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Resource_Flow_Item_fGid]);}
        }
        public const string _Resource_Flow_Item_view__fName = "Resource_Flow_Item_view_";       
        public static mdTableField FinProject_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_FinProject_fGid]);}
        }
        public const string _FinProject_view__fName = "FinProject_view_";       
        public static mdTableField Company_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Company_fGid]);}
        }
        public const string _Company_view__fName = "Company_view_";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";       
        public static mdTableField contragent_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_contragent_fGid]);}
        }
        public const string _contragent_view__fName = "contragent_view_";       
        public static mdTableField BaseDocument_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_BaseDocument_fGid]);}
        }
        public const string _BaseDocument_view__fName = "BaseDocument_view_";       
        public static mdTableField ProjectAsset_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_ProjectAsset_fGid]);}
        }
        public const string _ProjectAsset_view__fName = "ProjectAsset_view_";       
        public static mdTableField SheduledPayment_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_SheduledPayment_fGid]);}
        }
        public const string _SheduledPayment_view__fName = "SheduledPayment_view_";       
        public static mdTableField Resource_Flow_Item_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Resource_Flow_Item_fGid]);}
        }
        public const string _Resource_Flow_Item_status__fName = "Resource_Flow_Item_status_";       
        public static mdTableField FinProject_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_FinProject_fGid]);}
        }
        public const string _FinProject_status__fName = "FinProject_status_";       
        public static mdTableField Company_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Company_fGid]);}
        }
        public const string _Company_status__fName = "Company_status_";       
        public static mdTableField contragent_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_contragent_fGid]);}
        }
        public const string _contragent_status__fName = "contragent_status_";       
        public static mdTableField BaseDocument_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_BaseDocument_fGid]);}
        }
        public const string _BaseDocument_status__fName = "BaseDocument_status_";       
        public static mdTableField ProjectAsset_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_ProjectAsset_fGid]);}
        }
        public const string _ProjectAsset_status__fName = "ProjectAsset_status_";       
        public static mdTableField SheduledPayment_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_SheduledPayment_fGid]);}
        }
        public const string _SheduledPayment_status__fName = "SheduledPayment_status_";
    }
    /// <summary>
    /// Схема таблиці 'Ціни активів проектів'
    /// </summary>
    public static class ProjAssetsPrices_TS_   
    {
        public const int __tGid = 15608;
        public const string __tName = "ProjAssetsPrices";
        static mdTable m_mdTable;
        static ProjAssetsPrices_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _ProjectAsset_fGid = 28644;
       
        public static mdTableField ProjectAsset
        {
            get { return __MD.Fields[_ProjectAsset_fGid];}
        }
        public const string _ProjectAsset_fName = "ProjectAsset";        public const int _recorder_fGid = 28656;
       
        public static mdTableField recorder
        {
            get { return __MD.Fields[_recorder_fGid];}
        }
        public const string _recorder_fName = "recorder";        public const int _reg_date_fGid = 28657;
       
        public static mdTableField reg_date
        {
            get { return __MD.Fields[_reg_date_fGid];}
        }
        public const string _reg_date_fName = "reg_date";        public const int _AssetsCost_fGid = 28658;
       
        public static mdTableField AssetsCost
        {
            get { return __MD.Fields[_AssetsCost_fGid];}
        }
        public const string _AssetsCost_fName = "AssetsCost";       
        public static mdTableField ProjectAsset_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_ProjectAsset_fGid]);}
        }
        public const string _ProjectAsset_view__fName = "ProjectAsset_view_";       
        public static mdTableField recorder_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_recorder_fGid]);}
        }
        public const string _recorder_view__fName = "recorder_view_";       
        public static mdTableField ProjectAsset_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_ProjectAsset_fGid]);}
        }
        public const string _ProjectAsset_status__fName = "ProjectAsset_status_";       
        public static mdTableField recorder_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_recorder_fGid]);}
        }
        public const string _recorder_status__fName = "recorder_status_";
    }
    /// <summary>
    /// Схема таблиці 'Встановлення цін активів'
    /// </summary>
    public static class ProjAssetsPricesSet_TS_   
    {
        public const int __tGid = 15609;
        public const string __tName = "ProjAssetsPricesSet";
        static mdTable m_mdTable;
        static ProjAssetsPricesSet_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _document_fGid = 28645;
       
        public static mdTableField document
        {
            get { return __MD.Fields[_document_fGid];}
        }
        public const string _document_fName = "document";        public const int _FinProject_fGid = 28646;
       
        public static mdTableField FinProject
        {
            get { return __MD.Fields[_FinProject_fGid];}
        }
        public const string _FinProject_fName = "FinProject";        public const int _reg_date_fGid = 28647;
       
        public static mdTableField reg_date
        {
            get { return __MD.Fields[_reg_date_fGid];}
        }
        public const string _reg_date_fName = "reg_date";        public const int _doc_number_fGid = 28648;
       
        public static mdTableField doc_number
        {
            get { return __MD.Fields[_doc_number_fGid];}
        }
        public const string _doc_number_fName = "doc_number";        public const int _company_fGid = 28649;
       
        public static mdTableField company
        {
            get { return __MD.Fields[_company_fGid];}
        }
        public const string _company_fName = "company";        public const int _comments_fGid = 28650;
       
        public static mdTableField comments
        {
            get { return __MD.Fields[_comments_fGid];}
        }
        public const string _comments_fName = "comments";       
        public static mdTableField document_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_document_fGid]);}
        }
        public const string _document_view__fName = "document_view_";       
        public static mdTableField FinProject_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_FinProject_fGid]);}
        }
        public const string _FinProject_view__fName = "FinProject_view_";       
        public static mdTableField company_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_company_fGid]);}
        }
        public const string _company_view__fName = "company_view_";       
        public static mdTableField document_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_document_fGid]);}
        }
        public const string _document_status__fName = "document_status_";       
        public static mdTableField FinProject_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_FinProject_fGid]);}
        }
        public const string _FinProject_status__fName = "FinProject_status_";       
        public static mdTableField company_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_company_fGid]);}
        }
        public const string _company_status__fName = "company_status_";
    }
    /// <summary>
    /// Схема таблиці 'Т.Ч. Встановлення цін активів'
    /// </summary>
    public static class ProjAssetsPricesSet_tp_TS_   
    {
        public const int __tGid = 15610;
        public const string __tName = "ProjAssetsPricesSet_tp";
        static mdTable m_mdTable;
        static ProjAssetsPricesSet_tp_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _document_fGid = 28651;
       
        public static mdTableField document
        {
            get { return __MD.Fields[_document_fGid];}
        }
        public const string _document_fName = "document";        public const int _ProjectAsset_fGid = 28652;
       
        public static mdTableField ProjectAsset
        {
            get { return __MD.Fields[_ProjectAsset_fGid];}
        }
        public const string _ProjectAsset_fName = "ProjectAsset";        public const int _ordinal_fGid = 28653;
       
        public static mdTableField ordinal
        {
            get { return __MD.Fields[_ordinal_fGid];}
        }
        public const string _ordinal_fName = "ordinal";        public const int _AssetsCost_fGid = 28654;
       
        public static mdTableField AssetsCost
        {
            get { return __MD.Fields[_AssetsCost_fGid];}
        }
        public const string _AssetsCost_fName = "AssetsCost";        public const int _AssetsPrice_m2_fGid = 28655;
       
        public static mdTableField AssetsPrice_m2
        {
            get { return __MD.Fields[_AssetsPrice_m2_fGid];}
        }
        public const string _AssetsPrice_m2_fName = "AssetsPrice_m2";        public const int _ObjArea_fGid = 28671;
       
        public static mdTableField ObjArea
        {
            get { return __MD.Fields[_ObjArea_fGid];}
        }
        public const string _ObjArea_fName = "ObjArea";       
        public static mdTableField document_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_document_fGid]);}
        }
        public const string _document_view__fName = "document_view_";       
        public static mdTableField ProjectAsset_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_ProjectAsset_fGid]);}
        }
        public const string _ProjectAsset_view__fName = "ProjectAsset_view_";       
        public static mdTableField document_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_document_fGid]);}
        }
        public const string _document_status__fName = "document_status_";       
        public static mdTableField ProjectAsset_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_ProjectAsset_fGid]);}
        }
        public const string _ProjectAsset_status__fName = "ProjectAsset_status_";
    }
    /// <summary>
    /// Схема таблиці 'Активи проектів'
    /// </summary>
    public static class ProjectAssets_TS_   
    {
        public const int __tGid = 15569;
        public const string __tName = "ProjectAssets";
        static mdTable m_mdTable;
        static ProjectAssets_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _gid_fGid = 27905;
       
        public static mdTableField gid
        {
            get { return __MD.Fields[_gid_fGid];}
        }
        public const string _gid_fName = "gid";        public const int _name_fGid = 27906;
       
        public static mdTableField name
        {
            get { return __MD.Fields[_name_fGid];}
        }
        public const string _name_fName = "name";        public const int _object_status_fGid = 27907;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _version_no_fGid = 27908;
       
        public static mdTableField version_no
        {
            get { return __MD.Fields[_version_no_fGid];}
        }
        public const string _version_no_fName = "version_no";        public const int _comments_fGid = 27909;
       
        public static mdTableField comments
        {
            get { return __MD.Fields[_comments_fGid];}
        }
        public const string _comments_fName = "comments";        public const int _FinProject_fGid = 27910;
       
        public static mdTableField FinProject
        {
            get { return __MD.Fields[_FinProject_fGid];}
        }
        public const string _FinProject_fName = "FinProject";        public const int _AssetsType_fGid = 27911;
       
        public static mdTableField AssetsType
        {
            get { return __MD.Fields[_AssetsType_fGid];}
        }
        public const string _AssetsType_fName = "AssetsType";        public const int _ObjArea_fGid = 27912;
       
        public static mdTableField ObjArea
        {
            get { return __MD.Fields[_ObjArea_fGid];}
        }
        public const string _ObjArea_fName = "ObjArea";        public const int _Floor_Number_fGid = 28516;
       
        public static mdTableField Floor_Number
        {
            get { return __MD.Fields[_Floor_Number_fGid];}
        }
        public const string _Floor_Number_fName = "Floor_Number";        public const int _RoomsCount_fGid = 28517;
       
        public static mdTableField RoomsCount
        {
            get { return __MD.Fields[_RoomsCount_fGid];}
        }
        public const string _RoomsCount_fName = "RoomsCount";        public const int _IsSaled_fGid = 28557;
       
        public static mdTableField IsSaled
        {
            get { return __MD.Fields[_IsSaled_fGid];}
        }
        public const string _IsSaled_fName = "IsSaled";        public const int _IsRented_fGid = 28560;
       
        public static mdTableField IsRented
        {
            get { return __MD.Fields[_IsRented_fGid];}
        }
        public const string _IsRented_fName = "IsRented";        public const int _ProjCost_fGid = 28642;
       
        public static mdTableField ProjCost
        {
            get { return __MD.Fields[_ProjCost_fGid];}
        }
        public const string _ProjCost_fName = "ProjCost";        public const int _AssetNumber_fGid = 28643;
       
        public static mdTableField AssetNumber
        {
            get { return __MD.Fields[_AssetNumber_fGid];}
        }
        public const string _AssetNumber_fName = "AssetNumber";        public const int _CurAssetsCost_fGid = 28667;
       
        public static mdTableField CurAssetsCost
        {
            get { return __MD.Fields[_CurAssetsCost_fGid];}
        }
        public const string _CurAssetsCost_fName = "CurAssetsCost";        public const int _price_date_fGid = 28669;
       
        public static mdTableField price_date
        {
            get { return __MD.Fields[_price_date_fGid];}
        }
        public const string _price_date_fName = "price_date";        public const int _price_recorder_fGid = 28670;
       
        public static mdTableField price_recorder
        {
            get { return __MD.Fields[_price_recorder_fGid];}
        }
        public const string _price_recorder_fName = "price_recorder";        public const int _ProjPrice_m2_fGid = 28672;
       
        public static mdTableField ProjPrice_m2
        {
            get { return __MD.Fields[_ProjPrice_m2_fGid];}
        }
        public const string _ProjPrice_m2_fName = "ProjPrice_m2";        public const int _CurAssetsPrice_m2_fGid = 28673;
       
        public static mdTableField CurAssetsPrice_m2
        {
            get { return __MD.Fields[_CurAssetsPrice_m2_fGid];}
        }
        public const string _CurAssetsPrice_m2_fName = "CurAssetsPrice_m2";        public const int _MoneyIncome_fGid = 28727;
       
        public static mdTableField MoneyIncome
        {
            get { return __MD.Fields[_MoneyIncome_fGid];}
        }
        public const string _MoneyIncome_fName = "MoneyIncome";        public const int _SaleAmount_fGid = 28728;
       
        public static mdTableField SaleAmount
        {
            get { return __MD.Fields[_SaleAmount_fGid];}
        }
        public const string _SaleAmount_fName = "SaleAmount";        public const int _SaleInComeAmount_fGid = 29022;
       
        public static mdTableField SaleInComeAmount
        {
            get { return __MD.Fields[_SaleInComeAmount_fGid];}
        }
        public const string _SaleInComeAmount_fName = "SaleInComeAmount";        public const int _DebtAmount_fGid = 29023;
       
        public static mdTableField DebtAmount
        {
            get { return __MD.Fields[_DebtAmount_fGid];}
        }
        public const string _DebtAmount_fName = "DebtAmount";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";       
        public static mdTableField FinProject_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_FinProject_fGid]);}
        }
        public const string _FinProject_view__fName = "FinProject_view_";       
        public static mdTableField price_recorder_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_price_recorder_fGid]);}
        }
        public const string _price_recorder_view__fName = "price_recorder_view_";       
        public static mdTableField FinProject_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_FinProject_fGid]);}
        }
        public const string _FinProject_status__fName = "FinProject_status_";       
        public static mdTableField price_recorder_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_price_recorder_fGid]);}
        }
        public const string _price_recorder_status__fName = "price_recorder_status_";
    }
    /// <summary>
    /// Схема таблиці 'Реєстр комунального майна'
    /// </summary>
    public static class ProjectAssets_V_TS_   
    {
        public const int __tGid = 15613;
        public const string __tName = "ProjectAssets_V";
        static mdTable m_mdTable;
        static ProjectAssets_V_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _FinProject_fGid = 28678;
       
        public static mdTableField FinProject
        {
            get { return __MD.Fields[_FinProject_fGid];}
        }
        public const string _FinProject_fName = "FinProject";        public const int _AssetsType_fGid = 28679;
       
        public static mdTableField AssetsType
        {
            get { return __MD.Fields[_AssetsType_fGid];}
        }
        public const string _AssetsType_fName = "AssetsType";        public const int _ProjectAsset_fGid = 28680;
       
        public static mdTableField ProjectAsset
        {
            get { return __MD.Fields[_ProjectAsset_fGid];}
        }
        public const string _ProjectAsset_fName = "ProjectAsset";        public const int _AssetNumber_fGid = 28681;
       
        public static mdTableField AssetNumber
        {
            get { return __MD.Fields[_AssetNumber_fGid];}
        }
        public const string _AssetNumber_fName = "AssetNumber";        public const int _Floor_Number_fGid = 28682;
       
        public static mdTableField Floor_Number
        {
            get { return __MD.Fields[_Floor_Number_fGid];}
        }
        public const string _Floor_Number_fName = "Floor_Number";        public const int _ObjArea_fGid = 28683;
       
        public static mdTableField ObjArea
        {
            get { return __MD.Fields[_ObjArea_fGid];}
        }
        public const string _ObjArea_fName = "ObjArea";        public const int _FlatsArea_fGid = 28684;
       
        public static mdTableField FlatsArea
        {
            get { return __MD.Fields[_FlatsArea_fGid];}
        }
        public const string _FlatsArea_fName = "FlatsArea";        public const int _object_status_fGid = 28685;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _ProjCost_fGid = 28686;
       
        public static mdTableField ProjCost
        {
            get { return __MD.Fields[_ProjCost_fGid];}
        }
        public const string _ProjCost_fName = "ProjCost";        public const int _CurAssetsCost_fGid = 28687;
       
        public static mdTableField CurAssetsCost
        {
            get { return __MD.Fields[_CurAssetsCost_fGid];}
        }
        public const string _CurAssetsCost_fName = "CurAssetsCost";        public const int _ProjPrice_m2_fGid = 28688;
       
        public static mdTableField ProjPrice_m2
        {
            get { return __MD.Fields[_ProjPrice_m2_fGid];}
        }
        public const string _ProjPrice_m2_fName = "ProjPrice_m2";        public const int _CurAssetsPrice_m2_fGid = 28689;
       
        public static mdTableField CurAssetsPrice_m2
        {
            get { return __MD.Fields[_CurAssetsPrice_m2_fGid];}
        }
        public const string _CurAssetsPrice_m2_fName = "CurAssetsPrice_m2";        public const int _IsSaled_fGid = 28690;
       
        public static mdTableField IsSaled
        {
            get { return __MD.Fields[_IsSaled_fGid];}
        }
        public const string _IsSaled_fName = "IsSaled";        public const int _MoneyIncome_fGid = 29019;
       
        public static mdTableField MoneyIncome
        {
            get { return __MD.Fields[_MoneyIncome_fGid];}
        }
        public const string _MoneyIncome_fName = "MoneyIncome";        public const int _SaleAmount_fGid = 29020;
       
        public static mdTableField SaleAmount
        {
            get { return __MD.Fields[_SaleAmount_fGid];}
        }
        public const string _SaleAmount_fName = "SaleAmount";        public const int _DebtAmount_fGid = 29024;
       
        public static mdTableField DebtAmount
        {
            get { return __MD.Fields[_DebtAmount_fGid];}
        }
        public const string _DebtAmount_fName = "DebtAmount";        public const int _ResultCost_fGid = 29025;
       
        public static mdTableField ResultCost
        {
            get { return __MD.Fields[_ResultCost_fGid];}
        }
        public const string _ResultCost_fName = "ResultCost";        public const int _IsRented_fGid = 29026;
       
        public static mdTableField IsRented
        {
            get { return __MD.Fields[_IsRented_fGid];}
        }
        public const string _IsRented_fName = "IsRented";        public const int _AssetState_fGid = 29027;
       
        public static mdTableField AssetState
        {
            get { return __MD.Fields[_AssetState_fGid];}
        }
        public const string _AssetState_fName = "AssetState";       
        public static mdTableField FinProject_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_FinProject_fGid]);}
        }
        public const string _FinProject_view__fName = "FinProject_view_";        public const int _name_fGid = 99000282;
       
        public static mdTableField name
        {
            get { return __MD.Fields[_name_fGid];}
        }
        public const string _name_fName = "name";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";
    }
    /// <summary>
    /// Схема таблиці 'Договори оренди майна'
    /// </summary>
    public static class ProjectAssetsRent_TS_   
    {
        public const int __tGid = 15602;
        public const string __tName = "ProjectAssetsRent";
        static mdTable m_mdTable;
        static ProjectAssetsRent_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _document_fGid = 28528;
       
        public static mdTableField document
        {
            get { return __MD.Fields[_document_fGid];}
        }
        public const string _document_fName = "document";        public const int _FinProject_fGid = 28529;
       
        public static mdTableField FinProject
        {
            get { return __MD.Fields[_FinProject_fGid];}
        }
        public const string _FinProject_fName = "FinProject";        public const int _ProjectAsset_fGid = 28530;
       
        public static mdTableField ProjectAsset
        {
            get { return __MD.Fields[_ProjectAsset_fGid];}
        }
        public const string _ProjectAsset_fName = "ProjectAsset";        public const int _contragent_fGid = 28531;
       
        public static mdTableField contragent
        {
            get { return __MD.Fields[_contragent_fGid];}
        }
        public const string _contragent_fName = "contragent";        public const int _MonthlyAmount_fGid = 28532;
       
        public static mdTableField MonthlyAmount
        {
            get { return __MD.Fields[_MonthlyAmount_fGid];}
        }
        public const string _MonthlyAmount_fName = "MonthlyAmount";        public const int _reg_date_fGid = 28533;
       
        public static mdTableField reg_date
        {
            get { return __MD.Fields[_reg_date_fGid];}
        }
        public const string _reg_date_fName = "reg_date";        public const int _doc_number_fGid = 28534;
       
        public static mdTableField doc_number
        {
            get { return __MD.Fields[_doc_number_fGid];}
        }
        public const string _doc_number_fName = "doc_number";        public const int _company_fGid = 28535;
       
        public static mdTableField company
        {
            get { return __MD.Fields[_company_fGid];}
        }
        public const string _company_fName = "company";        public const int _comments_fGid = 28536;
       
        public static mdTableField comments
        {
            get { return __MD.Fields[_comments_fGid];}
        }
        public const string _comments_fName = "comments";        public const int _StartDate_fGid = 28553;
       
        public static mdTableField StartDate
        {
            get { return __MD.Fields[_StartDate_fGid];}
        }
        public const string _StartDate_fName = "StartDate";        public const int _EndDate_fGid = 28554;
       
        public static mdTableField EndDate
        {
            get { return __MD.Fields[_EndDate_fGid];}
        }
        public const string _EndDate_fName = "EndDate";        public const int _InComeAmount_fGid = 28556;
       
        public static mdTableField InComeAmount
        {
            get { return __MD.Fields[_InComeAmount_fGid];}
        }
        public const string _InComeAmount_fName = "InComeAmount";        public const int _object_status_fGid = 28559;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";       
        public static mdTableField document_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_document_fGid]);}
        }
        public const string _document_view__fName = "document_view_";       
        public static mdTableField FinProject_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_FinProject_fGid]);}
        }
        public const string _FinProject_view__fName = "FinProject_view_";       
        public static mdTableField ProjectAsset_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_ProjectAsset_fGid]);}
        }
        public const string _ProjectAsset_view__fName = "ProjectAsset_view_";       
        public static mdTableField contragent_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_contragent_fGid]);}
        }
        public const string _contragent_view__fName = "contragent_view_";       
        public static mdTableField company_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_company_fGid]);}
        }
        public const string _company_view__fName = "company_view_";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";       
        public static mdTableField FinProject_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_FinProject_fGid]);}
        }
        public const string _FinProject_status__fName = "FinProject_status_";       
        public static mdTableField ProjectAsset_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_ProjectAsset_fGid]);}
        }
        public const string _ProjectAsset_status__fName = "ProjectAsset_status_";       
        public static mdTableField contragent_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_contragent_fGid]);}
        }
        public const string _contragent_status__fName = "contragent_status_";       
        public static mdTableField company_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_company_fGid]);}
        }
        public const string _company_status__fName = "company_status_";
    }
    /// <summary>
    /// Схема таблиці 'Договори продажу майна'
    /// </summary>
    public static class ProjectAssetsSale_TS_   
    {
        public const int __tGid = 15601;
        public const string __tName = "ProjectAssetsSale";
        static mdTable m_mdTable;
        static ProjectAssetsSale_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _document_fGid = 28518;
       
        public static mdTableField document
        {
            get { return __MD.Fields[_document_fGid];}
        }
        public const string _document_fName = "document";        public const int _FinProject_fGid = 28519;
       
        public static mdTableField FinProject
        {
            get { return __MD.Fields[_FinProject_fGid];}
        }
        public const string _FinProject_fName = "FinProject";        public const int _reg_date_fGid = 28520;
       
        public static mdTableField reg_date
        {
            get { return __MD.Fields[_reg_date_fGid];}
        }
        public const string _reg_date_fName = "reg_date";        public const int _doc_number_fGid = 28521;
       
        public static mdTableField doc_number
        {
            get { return __MD.Fields[_doc_number_fGid];}
        }
        public const string _doc_number_fName = "doc_number";        public const int _company_fGid = 28522;
       
        public static mdTableField company
        {
            get { return __MD.Fields[_company_fGid];}
        }
        public const string _company_fName = "company";        public const int _comments_fGid = 28523;
       
        public static mdTableField comments
        {
            get { return __MD.Fields[_comments_fGid];}
        }
        public const string _comments_fName = "comments";        public const int _ProjectAsset_fGid = 28524;
       
        public static mdTableField ProjectAsset
        {
            get { return __MD.Fields[_ProjectAsset_fGid];}
        }
        public const string _ProjectAsset_fName = "ProjectAsset";        public const int _contragent_fGid = 28525;
       
        public static mdTableField contragent
        {
            get { return __MD.Fields[_contragent_fGid];}
        }
        public const string _contragent_fName = "contragent";        public const int _Amount_fGid = 28526;
       
        public static mdTableField Amount
        {
            get { return __MD.Fields[_Amount_fGid];}
        }
        public const string _Amount_fName = "Amount";        public const int _PaymentTerm_fGid = 28527;
       
        public static mdTableField PaymentTerm
        {
            get { return __MD.Fields[_PaymentTerm_fGid];}
        }
        public const string _PaymentTerm_fName = "PaymentTerm";        public const int _IncomeAmount_fGid = 28555;
       
        public static mdTableField IncomeAmount
        {
            get { return __MD.Fields[_IncomeAmount_fGid];}
        }
        public const string _IncomeAmount_fName = "IncomeAmount";        public const int _object_status_fGid = 28558;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _DebtAmount_fGid = 29021;
       
        public static mdTableField DebtAmount
        {
            get { return __MD.Fields[_DebtAmount_fGid];}
        }
        public const string _DebtAmount_fName = "DebtAmount";        public const int _ObjArea_fGid = 29623;
       
        public static mdTableField ObjArea
        {
            get { return __MD.Fields[_ObjArea_fGid];}
        }
        public const string _ObjArea_fName = "ObjArea";       
        public static mdTableField document_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_document_fGid]);}
        }
        public const string _document_view__fName = "document_view_";       
        public static mdTableField FinProject_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_FinProject_fGid]);}
        }
        public const string _FinProject_view__fName = "FinProject_view_";       
        public static mdTableField company_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_company_fGid]);}
        }
        public const string _company_view__fName = "company_view_";       
        public static mdTableField ProjectAsset_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_ProjectAsset_fGid]);}
        }
        public const string _ProjectAsset_view__fName = "ProjectAsset_view_";       
        public static mdTableField contragent_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_contragent_fGid]);}
        }
        public const string _contragent_view__fName = "contragent_view_";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";       
        public static mdTableField FinProject_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_FinProject_fGid]);}
        }
        public const string _FinProject_status__fName = "FinProject_status_";       
        public static mdTableField company_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_company_fGid]);}
        }
        public const string _company_status__fName = "company_status_";       
        public static mdTableField ProjectAsset_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_ProjectAsset_fGid]);}
        }
        public const string _ProjectAsset_status__fName = "ProjectAsset_status_";       
        public static mdTableField contragent_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_contragent_fGid]);}
        }
        public const string _contragent_status__fName = "contragent_status_";
    }
    /// <summary>
    /// Схема таблиці 'Перерозподіл платежів'
    /// </summary>
    public static class Reallocation_money_TS_   
    {
        public const int __tGid = 15575;
        public const string __tName = "Reallocation_money";
        static mdTable m_mdTable;
        static Reallocation_money_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _document_fGid = 27955;
       
        public static mdTableField document
        {
            get { return __MD.Fields[_document_fGid];}
        }
        public const string _document_fName = "document";        public const int _doc_number_fGid = 27956;
       
        public static mdTableField doc_number
        {
            get { return __MD.Fields[_doc_number_fGid];}
        }
        public const string _doc_number_fName = "doc_number";        public const int _reg_date_fGid = 27957;
       
        public static mdTableField reg_date
        {
            get { return __MD.Fields[_reg_date_fGid];}
        }
        public const string _reg_date_fName = "reg_date";        public const int _company_fGid = 27958;
       
        public static mdTableField company
        {
            get { return __MD.Fields[_company_fGid];}
        }
        public const string _company_fName = "company";        public const int _comments_fGid = 27959;
       
        public static mdTableField comments
        {
            get { return __MD.Fields[_comments_fGid];}
        }
        public const string _comments_fName = "comments";        public const int _FinProject_fGid = 27967;
       
        public static mdTableField FinProject
        {
            get { return __MD.Fields[_FinProject_fGid];}
        }
        public const string _FinProject_fName = "FinProject";        public const int _Resource_Flow_Item_fGid = 27968;
       
        public static mdTableField Resource_Flow_Item
        {
            get { return __MD.Fields[_Resource_Flow_Item_fGid];}
        }
        public const string _Resource_Flow_Item_fName = "Resource_Flow_Item";       
        public static mdTableField document_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_document_fGid]);}
        }
        public const string _document_view__fName = "document_view_";       
        public static mdTableField company_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_company_fGid]);}
        }
        public const string _company_view__fName = "company_view_";       
        public static mdTableField FinProject_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_FinProject_fGid]);}
        }
        public const string _FinProject_view__fName = "FinProject_view_";       
        public static mdTableField Resource_Flow_Item_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Resource_Flow_Item_fGid]);}
        }
        public const string _Resource_Flow_Item_view__fName = "Resource_Flow_Item_view_";       
        public static mdTableField document_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_document_fGid]);}
        }
        public const string _document_status__fName = "document_status_";       
        public static mdTableField company_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_company_fGid]);}
        }
        public const string _company_status__fName = "company_status_";       
        public static mdTableField FinProject_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_FinProject_fGid]);}
        }
        public const string _FinProject_status__fName = "FinProject_status_";       
        public static mdTableField Resource_Flow_Item_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Resource_Flow_Item_fGid]);}
        }
        public const string _Resource_Flow_Item_status__fName = "Resource_Flow_Item_status_";
    }
    /// <summary>
    /// Схема таблиці 'Перерозподіл платежів т.ч.'
    /// </summary>
    public static class Reallocation_money_tp_TS_   
    {
        public const int __tGid = 15576;
        public const string __tName = "Reallocation_money_tp";
        static mdTable m_mdTable;
        static Reallocation_money_tp_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _document_fGid = 27960;
       
        public static mdTableField document
        {
            get { return __MD.Fields[_document_fGid];}
        }
        public const string _document_fName = "document";        public const int _FinProject_fGid = 27961;
       
        public static mdTableField FinProject
        {
            get { return __MD.Fields[_FinProject_fGid];}
        }
        public const string _FinProject_fName = "FinProject";        public const int _ordinal_fGid = 27962;
       
        public static mdTableField ordinal
        {
            get { return __MD.Fields[_ordinal_fGid];}
        }
        public const string _ordinal_fName = "ordinal";        public const int _Resource_Flow_Item_fGid = 27963;
       
        public static mdTableField Resource_Flow_Item
        {
            get { return __MD.Fields[_Resource_Flow_Item_fGid];}
        }
        public const string _Resource_Flow_Item_fName = "Resource_Flow_Item";        public const int _amount_in_fGid = 27964;
       
        public static mdTableField amount_in
        {
            get { return __MD.Fields[_amount_in_fGid];}
        }
        public const string _amount_in_fName = "amount_in";        public const int _amount_out_fGid = 27965;
       
        public static mdTableField amount_out
        {
            get { return __MD.Fields[_amount_out_fGid];}
        }
        public const string _amount_out_fName = "amount_out";        public const int _comment_fGid = 27966;
       
        public static mdTableField comment
        {
            get { return __MD.Fields[_comment_fGid];}
        }
        public const string _comment_fName = "comment";       
        public static mdTableField document_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_document_fGid]);}
        }
        public const string _document_view__fName = "document_view_";       
        public static mdTableField FinProject_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_FinProject_fGid]);}
        }
        public const string _FinProject_view__fName = "FinProject_view_";       
        public static mdTableField Resource_Flow_Item_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Resource_Flow_Item_fGid]);}
        }
        public const string _Resource_Flow_Item_view__fName = "Resource_Flow_Item_view_";       
        public static mdTableField document_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_document_fGid]);}
        }
        public const string _document_status__fName = "document_status_";       
        public static mdTableField FinProject_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_FinProject_fGid]);}
        }
        public const string _FinProject_status__fName = "FinProject_status_";       
        public static mdTableField Resource_Flow_Item_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Resource_Flow_Item_fGid]);}
        }
        public const string _Resource_Flow_Item_status__fName = "Resource_Flow_Item_status_";
    }
    /// <summary>
    /// Схема таблиці 'Налаштування користувачів'
    /// </summary>
    public static class Account_Settings_TS_ 
    {
        public static mdTable __MD
        {
            get 
            {
                return Account_Settings_CTS_.__MD ;
            }
        }
        public const int _version_no_fGid = 5746;
       
        public static mdTableField version_no
        {
            get { return __MD.Fields[_version_no_fGid];}
        }
        public const string _version_no_fName = "version_no";
    }
    /// <summary>
    /// Схема таблиці 'Типи налаштувань користувачів'
    /// </summary>
    public static class Account_Settings_Types_TS_ 
    {
        public static mdTable __MD
        {
            get 
            {
                return Account_Settings_Types_CTS_.__MD ;
            }
        }
        public const int _object_status_fGid = 5266;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _version_no_fGid = 5747;
       
        public static mdTableField version_no
        {
            get { return __MD.Fields[_version_no_fGid];}
        }
        public const string _version_no_fName = "version_no";
    }
    /// <summary>
    /// Схема таблиці 'Облікові записи користувачів'
    /// </summary>
    public static class Accounts_TS_ 
    {
        public static mdTable __MD
        {
            get 
            {
                return Accounts_CTS_.__MD ;
            }
        }
        public const int _EMailBox_fGid = 1637;
       
        public static mdTableField EMailBox
        {
            get { return __MD.Fields[_EMailBox_fGid];}
        }
        public const string _EMailBox_fName = "EMailBox";        public const int _person_fGid = 4896;
       
        public static mdTableField person
        {
            get { return __MD.Fields[_person_fGid];}
        }
        public const string _person_fName = "person";        public const int _main_company_fGid = 26829;
       
        public static mdTableField main_company
        {
            get { return __MD.Fields[_main_company_fGid];}
        }
        public const string _main_company_fName = "main_company";        public const int _CompanyPerson_fGid = 31781;
       
        public static mdTableField CompanyPerson
        {
            get { return __MD.Fields[_CompanyPerson_fGid];}
        }
        public const string _CompanyPerson_fName = "CompanyPerson";
    }
    /// <summary>
    /// Схема таблиці 'Константи'
    /// </summary>
    public static class Constants_TS_ 
    {
        public static mdTable __MD
        {
            get 
            {
                return Constants_CTS_.__MD ;
            }
        }
        public const int _VAT_Rate_value_fGid = 4118;
       
        public static mdTableField VAT_Rate_value
        {
            get { return __MD.Fields[_VAT_Rate_value_fGid];}
        }
        public const string _VAT_Rate_value_fName = "VAT_Rate_value";        public const int _main_company_fGid = 5445;
       
        public static mdTableField main_company
        {
            get { return __MD.Fields[_main_company_fGid];}
        }
        public const string _main_company_fName = "main_company";        public const int _base_mail_box_fGid = 15288;
       
        public static mdTableField base_mail_box
        {
            get { return __MD.Fields[_base_mail_box_fGid];}
        }
        public const string _base_mail_box_fName = "base_mail_box";
    }
    /// <summary>
    /// Схема таблиці 'Директорії зовнішніх файлів'
    /// </summary>
    public static class External_File_Directories_TS_ 
    {
        public static mdTable __MD
        {
            get 
            {
                return External_File_Directories_CTS_.__MD ;
            }
        }

        /// <summary>
        /// Запис: 'ContractsFiles' з таблиці 'Директорії зовнішніх файлів'
        /// </summary>       
        public static mdDefaultDataRow _dr_ContractsAttachments
        {
            get { return __MD.DefaultDataRows[875];}
        }       
        public const int _dr_ContractsAttachments_Gid = 41;

        /// <summary>
        /// Запис: 'ProjectsDocumentationsFiles' з таблиці 'Директорії зовнішніх файлів'
        /// </summary>       
        public static mdDefaultDataRow _dr_ProjectsDocumentations
        {
            get { return __MD.DefaultDataRows[919];}
        }       
        public const int _dr_ProjectsDocumentations_Gid = 45;

        /// <summary>
        /// Запис: 'FinProjectsFiles' з таблиці 'Директорії зовнішніх файлів'
        /// </summary>       
        public static mdDefaultDataRow _dr_FinProjAttachments
        {
            get { return __MD.DefaultDataRows[921];}
        }       
        public const int _dr_FinProjAttachments_Gid = 47;

        /// <summary>
        /// Запис: 'Файли оренди активів' з таблиці 'Директорії зовнішніх файлів'
        /// </summary>       
        public static mdDefaultDataRow _dr_AssetsRentAttachments
        {
            get { return __MD.DefaultDataRows[958];}
        }       
        public const int _dr_AssetsRentAttachments_Gid = 53;

        /// <summary>
        /// Запис: 'Файли продажу активів' з таблиці 'Директорії зовнішніх файлів'
        /// </summary>       
        public static mdDefaultDataRow _dr_AssetsSalesAttachments
        {
            get { return __MD.DefaultDataRows[959];}
        }       
        public const int _dr_AssetsSalesAttachments_Gid = 54;

        /// <summary>
        /// Запис: 'Файли заявок на фінансування' з таблиці 'Директорії зовнішніх файлів'
        /// </summary>       
        public static mdDefaultDataRow _dr_FinanceOrderAttachments
        {
            get { return __MD.DefaultDataRows[960];}
        }       
        public const int _dr_FinanceOrderAttachments_Gid = 55;

        /// <summary>
        /// Запис: 'BudjetPlanOutFiles' з таблиці 'Директорії зовнішніх файлів'
        /// </summary>       
        public static mdDefaultDataRow _dr_BudjetPlanOutcomeAttachments
        {
            get { return __MD.DefaultDataRows[978];}
        }       
        public const int _dr_BudjetPlanOutcomeAttachments_Gid = 56;

        /// <summary>
        /// Запис: 'BudjetPlanInFiles' з таблиці 'Директорії зовнішніх файлів'
        /// </summary>       
        public static mdDefaultDataRow _dr_BudjetPlanIncomeAttachments
        {
            get { return __MD.DefaultDataRows[979];}
        }       
        public const int _dr_BudjetPlanIncomeAttachments_Gid = 57;

        /// <summary>
        /// Запис: 'BudjetExecutionFiles' з таблиці 'Директорії зовнішніх файлів'
        /// </summary>       
        public static mdDefaultDataRow _dr_BudjetExecutionAttachments
        {
            get { return __MD.DefaultDataRows[980];}
        }       
        public const int _dr_BudjetExecutionAttachments_Gid = 58;

        /// <summary>
        /// Запис: 'ActOfWorkExecutionFiles' з таблиці 'Директорії зовнішніх файлів'
        /// </summary>       
        public static mdDefaultDataRow _dr_ActOfWorkExecutionAttachments
        {
            get { return __MD.DefaultDataRows[981];}
        }       
        public const int _dr_ActOfWorkExecutionAttachments_Gid = 59;

        /// <summary>
        /// Запис: 'QuestionnaireTemplatesFiles' з таблиці 'Директорії зовнішніх файлів'
        /// </summary>       
        public static mdDefaultDataRow _dr_QuestionnaireTemplates
        {
            get { return __MD.DefaultDataRows[983];}
        }       
        public const int _dr_QuestionnaireTemplates_Gid = 80;

        /// <summary>
        /// Запис: 'AlternativesImages' з таблиці 'Директорії зовнішніх файлів'
        /// </summary>       
        public static mdDefaultDataRow _dr_AlternativesImages
        {
            get { return __MD.DefaultDataRows[1006];}
        }       
        public const int _dr_AlternativesImages_Gid = 81;

        /// <summary>
        /// Запис: 'ProposalsFiles' з таблиці 'Директорії зовнішніх файлів'
        /// </summary>       
        public static mdDefaultDataRow _dr_ProposalsFiles
        {
            get { return __MD.DefaultDataRows[1007];}
        }       
        public const int _dr_ProposalsFiles_Gid = 65;

        /// <summary>
        /// Запис: 'HTMLContent' з таблиці 'Директорії зовнішніх файлів'
        /// </summary>       
        public static mdDefaultDataRow _dr_HTMLContent
        {
            get { return __MD.DefaultDataRows[1012];}
        }       
        public const int _dr_HTMLContent_Gid = 83;

        /// <summary>
        /// Запис: 'css' з таблиці 'Директорії зовнішніх файлів'
        /// </summary>       
        public static mdDefaultDataRow _dr_HTML_css
        {
            get { return __MD.DefaultDataRows[1017];}
        }       
        public const int _dr_HTML_css_Gid = 84;

        /// <summary>
        /// Запис: 'js' з таблиці 'Директорії зовнішніх файлів'
        /// </summary>       
        public static mdDefaultDataRow _dr_HTML_js
        {
            get { return __MD.DefaultDataRows[1018];}
        }       
        public const int _dr_HTML_js_Gid = 85;

        /// <summary>
        /// Запис: 'PhotoAnswers' з таблиці 'Директорії зовнішніх файлів'
        /// </summary>       
        public static mdDefaultDataRow _dr_QnPhotoAnswers
        {
            get { return __MD.DefaultDataRows[1025];}
        }       
        public const int _dr_QnPhotoAnswers_Gid = 7;

        /// <summary>
        /// Запис: 'InsuranceProposalsFiles' з таблиці 'Директорії зовнішніх файлів'
        /// </summary>       
        public static mdDefaultDataRow _dr_InsuranceProposalsFiles
        {
            get { return __MD.DefaultDataRows[1078];}
        }       
        public const int _dr_InsuranceProposalsFiles_Gid = 66;

        /// <summary>
        /// Запис: 'InsuranceContractsFiles' з таблиці 'Директорії зовнішніх файлів'
        /// </summary>       
        public static mdDefaultDataRow _dr_InsuranceContractsFiles
        {
            get { return __MD.DefaultDataRows[1079];}
        }       
        public const int _dr_InsuranceContractsFiles_Gid = 67;

        /// <summary>
        /// Запис: 'WebAccountsFiles' з таблиці 'Директорії зовнішніх файлів'
        /// </summary>       
        public static mdDefaultDataRow _dr_WebAccountsFiles
        {
            get { return __MD.DefaultDataRows[1080];}
        }       
        public const int _dr_WebAccountsFiles_Gid = 74;

        /// <summary>
        /// Запис: 'BusinessAgentsFiles' з таблиці 'Директорії зовнішніх файлів'
        /// </summary>       
        public static mdDefaultDataRow _dr_BusinessAgentsFiles
        {
            get { return __MD.DefaultDataRows[1081];}
        }       
        public const int _dr_BusinessAgentsFiles_Gid = 75;

        /// <summary>
        /// Запис: 'PaymentNotificationsFiles' з таблиці 'Директорії зовнішніх файлів'
        /// </summary>       
        public static mdDefaultDataRow _dr_PaymentNotificationsFiles
        {
            get { return __MD.DefaultDataRows[1082];}
        }       
        public const int _dr_PaymentNotificationsFiles_Gid = 68;

    }
    /// <summary>
    /// Схема таблиці 'Зовнішні файли'
    /// </summary>
    public static class External_Files_TS_ 
    {
        public static mdTable __MD
        {
            get 
            {
                return External_Files_CTS_.__MD ;
            }
        }
        public const int _type_of_content_fGid = 24666;
       
        public static mdTableField type_of_content
        {
            get { return __MD.Fields[_type_of_content_fGid];}
        }
        public const string _type_of_content_fName = "type_of_content";        public const int _SmallerAnalog_fGid = 24767;
       
        public static mdTableField SmallerAnalog
        {
            get { return __MD.Fields[_SmallerAnalog_fGid];}
        }
        public const string _SmallerAnalog_fName = "SmallerAnalog";        public const int _Alt_Text_fGid = 24768;
       
        public static mdTableField Alt_Text
        {
            get { return __MD.Fields[_Alt_Text_fGid];}
        }
        public const string _Alt_Text_fName = "Alt_Text";        public const int _Annotation_fGid = 24769;
       
        public static mdTableField Annotation
        {
            get { return __MD.Fields[_Annotation_fGid];}
        }
        public const string _Annotation_fName = "Annotation";
    }
    /// <summary>
    /// Схема таблиці 'Перелік можливих статусів для таблиць'
    /// </summary>
    public static class Object_statuses_mapping_TS_ 
    {
        public static mdTable __MD
        {
            get 
            {
                return Object_statuses_mapping_CTS_.__MD ;
            }
        }
       
        public static mdDefaultDataRow _dr_SentForTesting
        {
            get { return __MD.DefaultDataRows[939];}
        }

    }
    /// <summary>
    /// Схема таблиці 'Операційні періоди'
    /// </summary>
    public static class Operation_periods_TS_ 
    {
        public static mdTable __MD
        {
            get 
            {
                return Operation_periods_CTS_.__MD ;
            }
        }
        public const int _object_status_fGid = 5415;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _version_no_fGid = 5748;
       
        public static mdTableField version_no
        {
            get { return __MD.Fields[_version_no_fGid];}
        }
        public const string _version_no_fName = "version_no";
    }
    /// <summary>
    /// Схема таблиці 'Групи осіб'
    /// </summary>
    public static class PersonsGroups_TS_ 
    {
        public static mdTable __MD
        {
            get 
            {
                return PersonsGroups_CTS_.__MD ;
            }
        }
        public const int _MembersCount_fGid = 31873;
       
        public static mdTableField MembersCount
        {
            get { return __MD.Fields[_MembersCount_fGid];}
        }
        public const string _MembersCount_fName = "MembersCount";
    }
    /// <summary>
    /// Схема таблиці 'Набори шаблонів друкованих форм'
    /// </summary>
    public static class PrintingFormsTemplates_Sets_TS_ 
    {
        public static mdTable __MD
        {
            get 
            {
                return PrintingFormsTemplates_Sets_CTS_.__MD ;
            }
        }
        public const int _SalesInvoiceTemplate_Number_fGid = 20344;
       
        public static mdTableField SalesInvoiceTemplate_Number
        {
            get { return __MD.Fields[_SalesInvoiceTemplate_Number_fGid];}
        }
        public const string _SalesInvoiceTemplate_Number_fName = "SalesInvoiceTemplate_Number";        public const int _UseTransportInvoices_fGid = 20345;
       
        public static mdTableField UseTransportInvoices
        {
            get { return __MD.Fields[_UseTransportInvoices_fGid];}
        }
        public const string _UseTransportInvoices_fName = "UseTransportInvoices";        public const int _TransportInvoiceTemplate_Number_fGid = 20346;
       
        public static mdTableField TransportInvoiceTemplate_Number
        {
            get { return __MD.Fields[_TransportInvoiceTemplate_Number_fGid];}
        }
        public const string _TransportInvoiceTemplate_Number_fName = "TransportInvoiceTemplate_Number";
    }
    /// <summary>
    /// Схема таблиці 'Ідентифікатори записів в зовнішніх системах'
    /// </summary>
    public static class Record_asids_TS_ 
    {
        public static mdTable __MD
        {
            get 
            {
                return Record_asids_CTS_.__MD ;
            }
        }
        public const int _version_no_fGid = 1404;
       
        public static mdTableField version_no
        {
            get { return __MD.Fields[_version_no_fGid];}
        }
        public const string _version_no_fName = "version_no";
    }
    /// <summary>
    /// Схема таблиці 'Зовнішні коди об'єктів'
    /// </summary>
    public static class Record_asids_view_TS_   
    {
        public const int __tGid = 689;
        public const string __tName = "Record_asids_view";
        static mdTable m_mdTable;
        static Record_asids_view_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _record_fGid = 5770;
       
        public static mdTableField record
        {
            get { return __MD.Fields[_record_fGid];}
        }
        public const string _record_fName = "record";        public const int _record_tid_fGid = 5771;
       
        public static mdTableField record_tid
        {
            get { return __MD.Fields[_record_tid_fGid];}
        }
        public const string _record_tid_fName = "record_tid";        public const int _asid_fGid = 5772;
       
        public static mdTableField asid
        {
            get { return __MD.Fields[_asid_fGid];}
        }
        public const string _asid_fName = "asid";        public const int _ext_as_gid_fGid = 5773;
       
        public static mdTableField ext_as_gid
        {
            get { return __MD.Fields[_ext_as_gid_fGid];}
        }
        public const string _ext_as_gid_fName = "ext_as_gid";
    }
    /// <summary>
    /// Схема таблиці 'Налаштування звітів'
    /// </summary>
    public static class Reports_Settings_TS_ 
    {
        public static mdTable __MD
        {
            get 
            {
                return Reports_Settings_CTS_.__MD ;
            }
        }
        public const int _Code_fGid = 34584;
       
        public static mdTableField Code
        {
            get { return __MD.Fields[_Code_fGid];}
        }
        public const string _Code_fName = "Code";
    }
    /// <summary>
    /// Схема таблиці 'Бази даних'
    /// </summary>
    public static class Storages_TS_ 
    {
        public static mdTable __MD
        {
            get 
            {
                return Storages_CTS_.__MD ;
            }
        }
        public const int _EMailBox_fGid = 1636;
       
        public static mdTableField EMailBox
        {
            get { return __MD.Fields[_EMailBox_fGid];}
        }
        public const string _EMailBox_fName = "EMailBox";        public const int _Representation_fGid = 5061;
       
        public static mdTableField Representation
        {
            get { return __MD.Fields[_Representation_fGid];}
        }
        public const string _Representation_fName = "Representation";        public const int _constant1_fGid = 25269;
       
        public static mdTableField constant1
        {
            get { return __MD.Fields[_constant1_fGid];}
        }
        public const string _constant1_fName = "constant1";
    }
    /// <summary>
    /// Схема таблиці 'Типи системних подій'
    /// </summary>
    public static class SystemEventTypes_TS_ 
    {
        public static mdTable __MD
        {
            get 
            {
                return SystemEventTypes_CTS_.__MD ;
            }
        }

        /// <summary>
        /// Запис: 'Затверджено документ на КПК' з таблиці 'Типи системних подій'
        /// </summary>       
        public static mdDefaultDataRow _dr_Approved_On_PPC
        {
            get { return __MD.DefaultDataRows[174];}
        }       
        public const int _dr_Approved_On_PPC_Gid = 103;

        /// <summary>
        /// Запис: 'Успішна відправка e-mail' з таблиці 'Типи системних подій'
        /// </summary>       
        public static mdDefaultDataRow _dr_Successful_Email_Sent
        {
            get { return __MD.DefaultDataRows[542];}
        }       
        public const int _dr_Successful_Email_Sent_Gid = 200;

        /// <summary>
        /// Запис: 'Помилка під час відправки e-mail' з таблиці 'Типи системних подій'
        /// </summary>       
        public static mdDefaultDataRow _dr_Email_Sent_Failed
        {
            get { return __MD.DefaultDataRows[543];}
        }       
        public const int _dr_Email_Sent_Failed_Gid = 201;

        /// <summary>
        /// Запис: 'Завантаження з КПК' з таблиці 'Типи системних подій'
        /// </summary>       
        public static mdDefaultDataRow _dr_Imported_From_PPC
        {
            get { return __MD.DefaultDataRows[549];}
        }       
        public const int _dr_Imported_From_PPC_Gid = 19;
        public const int _gid_fGid = 71;
       
        public static mdTableField gid
        {
            get { return __MD.Fields[_gid_fGid];}
        }
        public const string _gid_fName = "gid";        public const int _object_status_fGid = 5277;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _version_no_fGid = 9934;
       
        public static mdTableField version_no
        {
            get { return __MD.Fields[_version_no_fGid];}
        }
        public const string _version_no_fName = "version_no";
    }
    /// <summary>
    /// Схема таблиці 'Системні параметри'
    /// </summary>
    public static class SystemParameters_TS_ 
    {
        public static mdTable __MD
        {
            get 
            {
                return SystemParameters_CTS_.__MD ;
            }
        }
        public const int _storage_gid_fGid = 27150;
       
        public static mdTableField storage_gid
        {
            get { return __MD.Fields[_storage_gid_fGid];}
        }
        public const string _storage_gid_fName = "storage_gid";
    }
    /// <summary>
    /// Схема таблиці 'Реєстраційні дані учасників'
    /// </summary>
    public static class Web_Accounts_TS_ 
    {
        public static mdTable __MD
        {
            get 
            {
                return Web_Accounts_CTS_.__MD ;
            }
        }
        public const int _phones_fGid = 23092;
       
        public static mdTableField phones
        {
            get { return __MD.Fields[_phones_fGid];}
        }
        public const string _phones_fName = "phones";        public const int _comments_fGid = 23093;
       
        public static mdTableField comments
        {
            get { return __MD.Fields[_comments_fGid];}
        }
        public const string _comments_fName = "comments";        public const int _version_no_fGid = 23094;
       
        public static mdTableField version_no
        {
            get { return __MD.Fields[_version_no_fGid];}
        }
        public const string _version_no_fName = "version_no";        public const int _AdditionalInfo_fGid = 23096;
       
        public static mdTableField AdditionalInfo
        {
            get { return __MD.Fields[_AdditionalInfo_fGid];}
        }
        public const string _AdditionalInfo_fName = "AdditionalInfo";        public const int _password_code_fGid = 23112;
       
        public static mdTableField password_code
        {
            get { return __MD.Fields[_password_code_fGid];}
        }
        public const string _password_code_fName = "password_code";        public const int _Guid_fGid = 26003;
       
        public static mdTableField Guid
        {
            get { return __MD.Fields[_Guid_fGid];}
        }
        public const string _Guid_fName = "Guid";        public const int _lower_email_fGid = 26006;
       
        public static mdTableField lower_email
        {
            get { return __MD.Fields[_lower_email_fGid];}
        }
        public const string _lower_email_fName = "lower_email";        public const int _BirthDate_fGid = 26007;
       
        public static mdTableField BirthDate
        {
            get { return __MD.Fields[_BirthDate_fGid];}
        }
        public const string _BirthDate_fName = "BirthDate";        public const int _Gender_fGid = 26008;
       
        public static mdTableField Gender
        {
            get { return __MD.Fields[_Gender_fGid];}
        }
        public const string _Gender_fName = "Gender";        public const int _Middle_name_fGid = 26009;
       
        public static mdTableField Middle_name
        {
            get { return __MD.Fields[_Middle_name_fGid];}
        }
        public const string _Middle_name_fName = "Middle_name";        public const int _FirstName_fGid = 26010;
       
        public static mdTableField FirstName
        {
            get { return __MD.Fields[_FirstName_fGid];}
        }
        public const string _FirstName_fName = "FirstName";        public const int _Surname_fGid = 26011;
       
        public static mdTableField Surname
        {
            get { return __MD.Fields[_Surname_fGid];}
        }
        public const string _Surname_fName = "Surname";        public const int _UserAvaUrl_fGid = 26022;
       
        public static mdTableField UserAvaUrl
        {
            get { return __MD.Fields[_UserAvaUrl_fGid];}
        }
        public const string _UserAvaUrl_fName = "UserAvaUrl";        public const int _IsEmailingSubscriber_fGid = 26027;
       
        public static mdTableField IsEmailingSubscriber
        {
            get { return __MD.Fields[_IsEmailingSubscriber_fGid];}
        }
        public const string _IsEmailingSubscriber_fName = "IsEmailingSubscriber";        public const int _ExtraData_fGid = 31770;
       
        public static mdTableField ExtraData
        {
            get { return __MD.Fields[_ExtraData_fGid];}
        }
        public const string _ExtraData_fName = "ExtraData";        public const int _RegistrationDevice_fGid = 31772;
       
        public static mdTableField RegistrationDevice
        {
            get { return __MD.Fields[_RegistrationDevice_fGid];}
        }
        public const string _RegistrationDevice_fName = "RegistrationDevice";        public const int _EMailConfirmTime_fGid = 32033;
       
        public static mdTableField EMailConfirmTime
        {
            get { return __MD.Fields[_EMailConfirmTime_fGid];}
        }
        public const string _EMailConfirmTime_fName = "EMailConfirmTime";        public const int _EMailConfirmed_fGid = 32034;
       
        public static mdTableField EMailConfirmed
        {
            get { return __MD.Fields[_EMailConfirmed_fGid];}
        }
        public const string _EMailConfirmed_fName = "EMailConfirmed";        public const int _MemberingGroupsCount_fGid = 32086;
       
        public static mdTableField MemberingGroupsCount
        {
            get { return __MD.Fields[_MemberingGroupsCount_fGid];}
        }
        public const string _MemberingGroupsCount_fName = "MemberingGroupsCount";        public const int _JoiningGroupsRequestsCount_fGid = 32088;
       
        public static mdTableField JoiningGroupsRequestsCount
        {
            get { return __MD.Fields[_JoiningGroupsRequestsCount_fGid];}
        }
        public const string _JoiningGroupsRequestsCount_fName = "JoiningGroupsRequestsCount";        public const int _IsAuthorized_fGid = 32316;
       
        public static mdTableField IsAuthorized
        {
            get { return __MD.Fields[_IsAuthorized_fGid];}
        }
        public const string _IsAuthorized_fName = "IsAuthorized";        public const int _MainPhone_fGid = 33059;
       
        public static mdTableField MainPhone
        {
            get { return __MD.Fields[_MainPhone_fGid];}
        }
        public const string _MainPhone_fName = "MainPhone";
    }
    /// <summary>
    /// Схема таблиці 'Організації'
    /// </summary>
    public static class companies_TS_   
    {
        public const int __tGid = 102;
        public const string __tName = "companies";
        static mdTable m_mdTable;
        static companies_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _gid_fGid = 511;
       
        public static mdTableField gid
        {
            get { return __MD.Fields[_gid_fGid];}
        }
        public const string _gid_fName = "gid";        public const int _name_fGid = 515;
       
        public static mdTableField name
        {
            get { return __MD.Fields[_name_fGid];}
        }
        public const string _name_fName = "name";        public const int _comments_fGid = 524;
       
        public static mdTableField comments
        {
            get { return __MD.Fields[_comments_fGid];}
        }
        public const string _comments_fName = "comments";        public const int _object_status_fGid = 1264;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _contragent_fGid = 1822;
       
        public static mdTableField contragent
        {
            get { return __MD.Fields[_contragent_fGid];}
        }
        public const string _contragent_fName = "contragent";        public const int _version_no_fGid = 2810;
       
        public static mdTableField version_no
        {
            get { return __MD.Fields[_version_no_fGid];}
        }
        public const string _version_no_fName = "version_no";        public const int _main_bank_account_fGid = 3811;
       
        public static mdTableField main_bank_account
        {
            get { return __MD.Fields[_main_bank_account_fGid];}
        }
        public const string _main_bank_account_fName = "main_bank_account";        public const int _phones_fGid = 3870;
       
        public static mdTableField phones
        {
            get { return __MD.Fields[_phones_fGid];}
        }
        public const string _phones_fName = "phones";        public const int _edrpou_code_fGid = 5448;
       
        public static mdTableField edrpou_code
        {
            get { return __MD.Fields[_edrpou_code_fGid];}
        }
        public const string _edrpou_code_fName = "edrpou_code";        public const int _Director_fGid = 10820;
       
        public static mdTableField Director
        {
            get { return __MD.Fields[_Director_fGid];}
        }
        public const string _Director_fName = "Director";        public const int _Chief_Accountant_fGid = 10821;
       
        public static mdTableField Chief_Accountant
        {
            get { return __MD.Fields[_Chief_Accountant_fGid];}
        }
        public const string _Chief_Accountant_fName = "Chief_Accountant";        public const int _e_mail_fGid = 15243;
       
        public static mdTableField e_mail
        {
            get { return __MD.Fields[_e_mail_fGid];}
        }
        public const string _e_mail_fName = "e_mail";        public const int _asid_fGid = 20890;
       
        public static mdTableField asid
        {
            get { return __MD.Fields[_asid_fGid];}
        }
        public const string _asid_fName = "asid";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";       
        public static mdTableField contragent_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_contragent_fGid]);}
        }
        public const string _contragent_view__fName = "contragent_view_";       
        public static mdTableField main_bank_account_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_main_bank_account_fGid]);}
        }
        public const string _main_bank_account_view__fName = "main_bank_account_view_";       
        public static mdTableField Director_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Director_fGid]);}
        }
        public const string _Director_view__fName = "Director_view_";       
        public static mdTableField Chief_Accountant_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Chief_Accountant_fGid]);}
        }
        public const string _Chief_Accountant_view__fName = "Chief_Accountant_view_";       
        public static mdTableField contragent_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_contragent_fGid]);}
        }
        public const string _contragent_status__fName = "contragent_status_";       
        public static mdTableField main_bank_account_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_main_bank_account_fGid]);}
        }
        public const string _main_bank_account_status__fName = "main_bank_account_status_";       
        public static mdTableField Director_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Director_fGid]);}
        }
        public const string _Director_status__fName = "Director_status_";       
        public static mdTableField Chief_Accountant_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Chief_Accountant_fGid]);}
        }
        public const string _Chief_Accountant_status__fName = "Chief_Accountant_status_";
    }
    /// <summary>
    /// Схема таблиці 'Співробітники'
    /// </summary>
    public static class company_persons_TS_ 
    {
        public static mdTable __MD
        {
            get 
            {
                return company_persons_CTS_.__MD ;
            }
        }
        public const int _ipn_fGid = 527;
       
        public static mdTableField ipn
        {
            get { return __MD.Fields[_ipn_fGid];}
        }
        public const string _ipn_fName = "ipn";        public const int _phones_fGid = 538;
       
        public static mdTableField phones
        {
            get { return __MD.Fields[_phones_fGid];}
        }
        public const string _phones_fName = "phones";        public const int _comments_fGid = 540;
       
        public static mdTableField comments
        {
            get { return __MD.Fields[_comments_fGid];}
        }
        public const string _comments_fName = "comments";        public const int _birthday_fGid = 4197;
       
        public static mdTableField birthday
        {
            get { return __MD.Fields[_birthday_fGid];}
        }
        public const string _birthday_fName = "birthday";        public const int _asid_fGid = 20903;
       
        public static mdTableField asid
        {
            get { return __MD.Fields[_asid_fGid];}
        }
        public const string _asid_fName = "asid";        public const int _gender_fGid = 24308;
       
        public static mdTableField gender
        {
            get { return __MD.Fields[_gender_fGid];}
        }
        public const string _gender_fName = "gender";
    }
    /// <summary>
    /// Схема таблиці 'Групи контактів'
    /// </summary>
    public static class ContactsGroups_TS_   
    {
        public const int __tGid = 15866;
        public const string __tName = "ContactsGroups";
        static mdTable m_mdTable;
        static ContactsGroups_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _gid_fGid = 33607;
       
        public static mdTableField gid
        {
            get { return __MD.Fields[_gid_fGid];}
        }
        public const string _gid_fName = "gid";        public const int _name_fGid = 33608;
       
        public static mdTableField name
        {
            get { return __MD.Fields[_name_fGid];}
        }
        public const string _name_fName = "name";        public const int _Description_fGid = 33609;
       
        public static mdTableField Description
        {
            get { return __MD.Fields[_Description_fGid];}
        }
        public const string _Description_fName = "Description";        public const int _object_status_fGid = 33610;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _Author_fGid = 33611;
       
        public static mdTableField Author
        {
            get { return __MD.Fields[_Author_fGid];}
        }
        public const string _Author_fName = "Author";        public const int _comments_fGid = 33612;
       
        public static mdTableField comments
        {
            get { return __MD.Fields[_comments_fGid];}
        }
        public const string _comments_fName = "comments";        public const int _version_no_fGid = 33613;
       
        public static mdTableField version_no
        {
            get { return __MD.Fields[_version_no_fGid];}
        }
        public const string _version_no_fName = "version_no";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";       
        public static mdTableField Author_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Author_fGid]);}
        }
        public const string _Author_view__fName = "Author_view_";       
        public static mdTableField Author_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Author_fGid]);}
        }
        public const string _Author_status__fName = "Author_status_";
    }
    /// <summary>
    /// Схема таблиці 'Склад груп контактів'
    /// </summary>
    public static class ContactsGroups_tp_TS_   
    {
        public const int __tGid = 15867;
        public const string __tName = "ContactsGroups_tp";
        static mdTable m_mdTable;
        static ContactsGroups_tp_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _PersonsGroup_fGid = 33614;
       
        public static mdTableField PersonsGroup
        {
            get { return __MD.Fields[_PersonsGroup_fGid];}
        }
        public const string _PersonsGroup_fName = "PersonsGroup";        public const int _Person_fGid = 33615;
       
        public static mdTableField Person
        {
            get { return __MD.Fields[_Person_fGid];}
        }
        public const string _Person_fName = "Person";        public const int _ordinal_fGid = 33616;
       
        public static mdTableField ordinal
        {
            get { return __MD.Fields[_ordinal_fGid];}
        }
        public const string _ordinal_fName = "ordinal";        public const int _GroupAuthor_fGid = 33618;
       
        public static mdTableField GroupAuthor
        {
            get { return __MD.Fields[_GroupAuthor_fGid];}
        }
        public const string _GroupAuthor_fName = "GroupAuthor";        public const int _Comment_fGid = 33925;
       
        public static mdTableField Comment
        {
            get { return __MD.Fields[_Comment_fGid];}
        }
        public const string _Comment_fName = "Comment";       
        public static mdTableField PersonsGroup_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_PersonsGroup_fGid]);}
        }
        public const string _PersonsGroup_view__fName = "PersonsGroup_view_";       
        public static mdTableField Person_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Person_fGid]);}
        }
        public const string _Person_view__fName = "Person_view_";       
        public static mdTableField GroupAuthor_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_GroupAuthor_fGid]);}
        }
        public const string _GroupAuthor_view__fName = "GroupAuthor_view_";       
        public static mdTableField PersonsGroup_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_PersonsGroup_fGid]);}
        }
        public const string _PersonsGroup_status__fName = "PersonsGroup_status_";       
        public static mdTableField Person_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Person_fGid]);}
        }
        public const string _Person_status__fName = "Person_status_";       
        public static mdTableField GroupAuthor_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_GroupAuthor_fGid]);}
        }
        public const string _GroupAuthor_status__fName = "GroupAuthor_status_";
    }
    /// <summary>
    /// Схема таблиці 'Види документації'
    /// </summary>
    public static class DocumentationKinds_TS_   
    {
        public const int __tGid = 15626;
        public const string __tName = "DocumentationKinds";
        static mdTable m_mdTable;
        static DocumentationKinds_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }

        /// <summary>
        /// Запис: 'Елементи монтажних схем' з таблиці 'Види документації'
        /// </summary>       
        public static mdDefaultDataRow _dr_MontageSchemaItems
        {
            get { return __MD.DefaultDataRows[945];}
        }       
        public const int _dr_MontageSchemaItems_Gid = 12;
        public const int _gid_fGid = 28936;
       
        public static mdTableField gid
        {
            get { return __MD.Fields[_gid_fGid];}
        }
        public const string _gid_fName = "gid";        public const int _name_fGid = 28937;
       
        public static mdTableField name
        {
            get { return __MD.Fields[_name_fGid];}
        }
        public const string _name_fName = "name";        public const int _object_status_fGid = 28938;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _version_no_fGid = 28939;
       
        public static mdTableField version_no
        {
            get { return __MD.Fields[_version_no_fGid];}
        }
        public const string _version_no_fName = "version_no";        public const int _comments_fGid = 28940;
       
        public static mdTableField comments
        {
            get { return __MD.Fields[_comments_fGid];}
        }
        public const string _comments_fName = "comments";        public const int _parent_kind_fGid = 28941;
       
        public static mdTableField parent_kind
        {
            get { return __MD.Fields[_parent_kind_fGid];}
        }
        public const string _parent_kind_fName = "parent_kind";        public const int _Code_fGid = 28942;
       
        public static mdTableField Code
        {
            get { return __MD.Fields[_Code_fGid];}
        }
        public const string _Code_fName = "Code";        public const int _Can_contain_subkinds_fGid = 28943;
       
        public static mdTableField Can_contain_subkinds
        {
            get { return __MD.Fields[_Can_contain_subkinds_fGid];}
        }
        public const string _Can_contain_subkinds_fName = "Can_contain_subkinds";        public const int _Can_contain_items_fGid = 28944;
       
        public static mdTableField Can_contain_items
        {
            get { return __MD.Fields[_Can_contain_items_fGid];}
        }
        public const string _Can_contain_items_fName = "Can_contain_items";        public const int _DocType_fGid = 29627;
       
        public static mdTableField DocType
        {
            get { return __MD.Fields[_DocType_fGid];}
        }
        public const string _DocType_fName = "DocType";        public const int _Developer_fGid = 29733;
       
        public static mdTableField Developer
        {
            get { return __MD.Fields[_Developer_fGid];}
        }
        public const string _Developer_fName = "Developer";        public const int _FinProject_fGid = 29734;
       
        public static mdTableField FinProject
        {
            get { return __MD.Fields[_FinProject_fGid];}
        }
        public const string _FinProject_fName = "FinProject";        public const int _RFI_fGid = 29737;
       
        public static mdTableField RFI
        {
            get { return __MD.Fields[_RFI_fGid];}
        }
        public const string _RFI_fName = "RFI";        public const int _RowsCount_fGid = 30886;
       
        public static mdTableField RowsCount
        {
            get { return __MD.Fields[_RowsCount_fGid];}
        }
        public const string _RowsCount_fName = "RowsCount";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";       
        public static mdTableField parent_kind_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_parent_kind_fGid]);}
        }
        public const string _parent_kind_view__fName = "parent_kind_view_";       
        public static mdTableField Developer_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Developer_fGid]);}
        }
        public const string _Developer_view__fName = "Developer_view_";       
        public static mdTableField FinProject_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_FinProject_fGid]);}
        }
        public const string _FinProject_view__fName = "FinProject_view_";       
        public static mdTableField RFI_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_RFI_fGid]);}
        }
        public const string _RFI_view__fName = "RFI_view_";       
        public static mdTableField parent_kind_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_parent_kind_fGid]);}
        }
        public const string _parent_kind_status__fName = "parent_kind_status_";       
        public static mdTableField Developer_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Developer_fGid]);}
        }
        public const string _Developer_status__fName = "Developer_status_";       
        public static mdTableField FinProject_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_FinProject_fGid]);}
        }
        public const string _FinProject_status__fName = "FinProject_status_";       
        public static mdTableField RFI_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_RFI_fGid]);}
        }
        public const string _RFI_status__fName = "RFI_status_";
    }
    /// <summary>
    /// Схема таблиці 'Документація проектів'
    /// </summary>
    public static class DocumentationsOfProjects_TS_   
    {
        public const int __tGid = 15627;
        public const string __tName = "DocumentationsOfProjects";
        static mdTable m_mdTable;
        static DocumentationsOfProjects_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _gid_fGid = 28945;
       
        public static mdTableField gid
        {
            get { return __MD.Fields[_gid_fGid];}
        }
        public const string _gid_fName = "gid";        public const int _name_fGid = 28946;
       
        public static mdTableField name
        {
            get { return __MD.Fields[_name_fGid];}
        }
        public const string _name_fName = "name";        public const int _Code_fGid = 28947;
       
        public static mdTableField Code
        {
            get { return __MD.Fields[_Code_fGid];}
        }
        public const string _Code_fName = "Code";        public const int _DocumentationKind_fGid = 28948;
       
        public static mdTableField DocumentationKind
        {
            get { return __MD.Fields[_DocumentationKind_fGid];}
        }
        public const string _DocumentationKind_fName = "DocumentationKind";        public const int _comments_fGid = 28949;
       
        public static mdTableField comments
        {
            get { return __MD.Fields[_comments_fGid];}
        }
        public const string _comments_fName = "comments";        public const int _object_status_fGid = 28950;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _version_no_fGid = 28951;
       
        public static mdTableField version_no
        {
            get { return __MD.Fields[_version_no_fGid];}
        }
        public const string _version_no_fName = "version_no";        public const int _Developer_fGid = 28954;
       
        public static mdTableField Developer
        {
            get { return __MD.Fields[_Developer_fGid];}
        }
        public const string _Developer_fName = "Developer";        public const int _responsible_person_fGid = 28955;
       
        public static mdTableField responsible_person
        {
            get { return __MD.Fields[_responsible_person_fGid];}
        }
        public const string _responsible_person_fName = "responsible_person";        public const int _Attached_File_fGid = 28956;
       
        public static mdTableField Attached_File
        {
            get { return __MD.Fields[_Attached_File_fGid];}
        }
        public const string _Attached_File_fName = "Attached_File";        public const int _StartDate_fGid = 28957;
       
        public static mdTableField StartDate
        {
            get { return __MD.Fields[_StartDate_fGid];}
        }
        public const string _StartDate_fName = "StartDate";        public const int _EndDate_fGid = 28958;
       
        public static mdTableField EndDate
        {
            get { return __MD.Fields[_EndDate_fGid];}
        }
        public const string _EndDate_fName = "EndDate";        public const int _Status_fGid = 28959;
       
        public static mdTableField Status
        {
            get { return __MD.Fields[_Status_fGid];}
        }
        public const string _Status_fName = "Status";        public const int _FinProject_fGid = 29625;
       
        public static mdTableField FinProject
        {
            get { return __MD.Fields[_FinProject_fGid];}
        }
        public const string _FinProject_fName = "FinProject";        public const int _RFI_fGid = 29626;
       
        public static mdTableField RFI
        {
            get { return __MD.Fields[_RFI_fGid];}
        }
        public const string _RFI_fName = "RFI";       
        public static mdTableField DocumentationKind_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_DocumentationKind_fGid]);}
        }
        public const string _DocumentationKind_view__fName = "DocumentationKind_view_";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";       
        public static mdTableField Developer_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Developer_fGid]);}
        }
        public const string _Developer_view__fName = "Developer_view_";       
        public static mdTableField responsible_person_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_responsible_person_fGid]);}
        }
        public const string _responsible_person_view__fName = "responsible_person_view_";       
        public static mdTableField Attached_File_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Attached_File_fGid]);}
        }
        public const string _Attached_File_view__fName = "Attached_File_view_";       
        public static mdTableField FinProject_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_FinProject_fGid]);}
        }
        public const string _FinProject_view__fName = "FinProject_view_";       
        public static mdTableField RFI_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_RFI_fGid]);}
        }
        public const string _RFI_view__fName = "RFI_view_";       
        public static mdTableField DocumentationKind_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_DocumentationKind_fGid]);}
        }
        public const string _DocumentationKind_status__fName = "DocumentationKind_status_";       
        public static mdTableField Developer_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Developer_fGid]);}
        }
        public const string _Developer_status__fName = "Developer_status_";       
        public static mdTableField responsible_person_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_responsible_person_fGid]);}
        }
        public const string _responsible_person_status__fName = "responsible_person_status_";       
        public static mdTableField Attached_File_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Attached_File_fGid]);}
        }
        public const string _Attached_File_status__fName = "Attached_File_status_";       
        public static mdTableField FinProject_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_FinProject_fGid]);}
        }
        public const string _FinProject_status__fName = "FinProject_status_";       
        public static mdTableField RFI_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_RFI_fGid]);}
        }
        public const string _RFI_status__fName = "RFI_status_";
    }
    /// <summary>
    /// Схема таблиці 'Підрозділи підприємства'
    /// </summary>
    public static class Enterprise_Departments_TS_   
    {
        public const int __tGid = 650;
        public const string __tName = "Enterprise_Departments";
        static mdTable m_mdTable;
        static Enterprise_Departments_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _gid_fGid = 5313;
       
        public static mdTableField gid
        {
            get { return __MD.Fields[_gid_fGid];}
        }
        public const string _gid_fName = "gid";        public const int _Name_fGid = 5314;
       
        public static mdTableField Name
        {
            get { return __MD.Fields[_Name_fGid];}
        }
        public const string _Name_fName = "Name";        public const int _Full_Name_fGid = 5315;
       
        public static mdTableField Full_Name
        {
            get { return __MD.Fields[_Full_Name_fGid];}
        }
        public const string _Full_Name_fName = "Full_Name";        public const int _company_fGid = 5316;
       
        public static mdTableField company
        {
            get { return __MD.Fields[_company_fGid];}
        }
        public const string _company_fName = "company";        public const int _object_status_fGid = 5333;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _version_no_fGid = 5757;
       
        public static mdTableField version_no
        {
            get { return __MD.Fields[_version_no_fGid];}
        }
        public const string _version_no_fName = "version_no";        public const int _leader_fGid = 22513;
       
        public static mdTableField leader
        {
            get { return __MD.Fields[_leader_fGid];}
        }
        public const string _leader_fName = "leader";        public const int _asid_fGid = 22595;
       
        public static mdTableField asid
        {
            get { return __MD.Fields[_asid_fGid];}
        }
        public const string _asid_fName = "asid";        public const int _comments_fGid = 30269;
       
        public static mdTableField comments
        {
            get { return __MD.Fields[_comments_fGid];}
        }
        public const string _comments_fName = "comments";       
        public static mdTableField company_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_company_fGid]);}
        }
        public const string _company_view__fName = "company_view_";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";       
        public static mdTableField leader_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_leader_fGid]);}
        }
        public const string _leader_view__fName = "leader_view_";       
        public static mdTableField company_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_company_fGid]);}
        }
        public const string _company_status__fName = "company_status_";       
        public static mdTableField leader_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_leader_fGid]);}
        }
        public const string _leader_status__fName = "leader_status_";
    }
    /// <summary>
    /// Схема таблиці 'Запити на приєднання до груп'
    /// </summary>
    public static class GroupJoiningRequests_TS_   
    {
        public const int __tGid = 15784;
        public const string __tName = "GroupJoiningRequests";
        static mdTable m_mdTable;
        static GroupJoiningRequests_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _PersonsGroup_fGid = 31773;
       
        public static mdTableField PersonsGroup
        {
            get { return __MD.Fields[_PersonsGroup_fGid];}
        }
        public const string _PersonsGroup_fName = "PersonsGroup";        public const int _JoiningWebAccount_fGid = 31774;
       
        public static mdTableField JoiningWebAccount
        {
            get { return __MD.Fields[_JoiningWebAccount_fGid];}
        }
        public const string _JoiningWebAccount_fName = "JoiningWebAccount";        public const int _reg_date_fGid = 31775;
       
        public static mdTableField reg_date
        {
            get { return __MD.Fields[_reg_date_fGid];}
        }
        public const string _reg_date_fName = "reg_date";        public const int _State_fGid = 31776;
       
        public static mdTableField State
        {
            get { return __MD.Fields[_State_fGid];}
        }
        public const string _State_fName = "State";        public const int _Approver_fGid = 31777;
       
        public static mdTableField Approver
        {
            get { return __MD.Fields[_Approver_fGid];}
        }
        public const string _Approver_fName = "Approver";        public const int _DecisionTime_fGid = 31778;
       
        public static mdTableField DecisionTime
        {
            get { return __MD.Fields[_DecisionTime_fGid];}
        }
        public const string _DecisionTime_fName = "DecisionTime";        public const int _JoiningPersonID_fGid = 31843;
       
        public static mdTableField JoiningPersonID
        {
            get { return __MD.Fields[_JoiningPersonID_fGid];}
        }
        public const string _JoiningPersonID_fName = "JoiningPersonID";        public const int _JoiningPerson_fGid = 31844;
       
        public static mdTableField JoiningPerson
        {
            get { return __MD.Fields[_JoiningPerson_fGid];}
        }
        public const string _JoiningPerson_fName = "JoiningPerson";       
        public static mdTableField PersonsGroup_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_PersonsGroup_fGid]);}
        }
        public const string _PersonsGroup_view__fName = "PersonsGroup_view_";       
        public static mdTableField JoiningWebAccount_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_JoiningWebAccount_fGid]);}
        }
        public const string _JoiningWebAccount_view__fName = "JoiningWebAccount_view_";       
        public static mdTableField Approver_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Approver_fGid]);}
        }
        public const string _Approver_view__fName = "Approver_view_";       
        public static mdTableField JoiningPerson_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_JoiningPerson_fGid]);}
        }
        public const string _JoiningPerson_view__fName = "JoiningPerson_view_";       
        public static mdTableField PersonsGroup_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_PersonsGroup_fGid]);}
        }
        public const string _PersonsGroup_status__fName = "PersonsGroup_status_";       
        public static mdTableField JoiningWebAccount_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_JoiningWebAccount_fGid]);}
        }
        public const string _JoiningWebAccount_status__fName = "JoiningWebAccount_status_";       
        public static mdTableField Approver_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Approver_fGid]);}
        }
        public const string _Approver_status__fName = "Approver_status_";       
        public static mdTableField JoiningPerson_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_JoiningPerson_fGid]);}
        }
        public const string _JoiningPerson_status__fName = "JoiningPerson_status_";
    }
    /// <summary>
    /// Схема таблиці 'Склад груп осіб'
    /// </summary>
    public static class MembersOfGroups_TS_   
    {
        public const int __tGid = 15783;
        public const string __tName = "MembersOfGroups";
        static mdTable m_mdTable;
        static MembersOfGroups_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _PersonsGroup_fGid = 31761;
       
        public static mdTableField PersonsGroup
        {
            get { return __MD.Fields[_PersonsGroup_fGid];}
        }
        public const string _PersonsGroup_fName = "PersonsGroup";        public const int _Person_fGid = 31762;
       
        public static mdTableField Person
        {
            get { return __MD.Fields[_Person_fGid];}
        }
        public const string _Person_fName = "Person";        public const int _Role_fGid = 31763;
       
        public static mdTableField Role
        {
            get { return __MD.Fields[_Role_fGid];}
        }
        public const string _Role_fName = "Role";        public const int _Group_status_fGid = 32087;
       
        public static mdTableField Group_status
        {
            get { return __MD.Fields[_Group_status_fGid];}
        }
        public const string _Group_status_fName = "Group_status";        public const int _Invitation_fGid = 32311;
       
        public static mdTableField Invitation
        {
            get { return __MD.Fields[_Invitation_fGid];}
        }
        public const string _Invitation_fName = "Invitation";       
        public static mdTableField PersonsGroup_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_PersonsGroup_fGid]);}
        }
        public const string _PersonsGroup_view__fName = "PersonsGroup_view_";       
        public static mdTableField Person_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Person_fGid]);}
        }
        public const string _Person_view__fName = "Person_view_";       
        public static mdTableField Group_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Group_status_fGid]);}
        }
        public const string _Group_status_view__fName = "Group_status_view_";       
        public static mdTableField Invitation_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Invitation_fGid]);}
        }
        public const string _Invitation_view__fName = "Invitation_view_";       
        public static mdTableField Person_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Person_fGid]);}
        }
        public const string _Person_status__fName = "Person_status_";       
        public static mdTableField Invitation_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Invitation_fGid]);}
        }
        public const string _Invitation_status__fName = "Invitation_status_";
    }
    /// <summary>
    /// Схема таблиці 'Вкладені файли об'єктів'
    /// </summary>
    public static class Object_Attachments_TS_ 
    {
        public static mdTable __MD
        {
            get 
            {
                return Object_Attachments_CTS_.__MD ;
            }
        }
        public const int _SmallerAnalog_fGid = 30662;
       
        public static mdTableField SmallerAnalog
        {
            get { return __MD.Fields[_SmallerAnalog_fGid];}
        }
        public const string _SmallerAnalog_fName = "SmallerAnalog";        public const int _FileFormat_fGid = 31863;
       
        public static mdTableField FileFormat
        {
            get { return __MD.Fields[_FileFormat_fGid];}
        }
        public const string _FileFormat_fName = "FileFormat";        public const int _FileVersion_fGid = 31921;
       
        public static mdTableField FileVersion
        {
            get { return __MD.Fields[_FileVersion_fGid];}
        }
        public const string _FileVersion_fName = "FileVersion";        public const int _FileDirectory_fGid = 32050;
       
        public static mdTableField FileDirectory
        {
            get { return __MD.Fields[_FileDirectory_fGid];}
        }
        public const string _FileDirectory_fName = "FileDirectory";
    }
    /// <summary>
    /// Схема таблиці 'Об'єднання профілів користувачів та контактів'
    /// </summary>
    public static class PersonProfilesTP_TS_   
    {
        public const int __tGid = 15888;
        public const string __tName = "PersonProfilesTP";
        static mdTable m_mdTable;
        static PersonProfilesTP_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _OwnerPerson_fGid = 33914;
       
        public static mdTableField OwnerPerson
        {
            get { return __MD.Fields[_OwnerPerson_fGid];}
        }
        public const string _OwnerPerson_fName = "OwnerPerson";        public const int _ItemKey_fGid = 33915;
       
        public static mdTableField ItemKey
        {
            get { return __MD.Fields[_ItemKey_fGid];}
        }
        public const string _ItemKey_fName = "ItemKey";        public const int _JoinedPerson_fGid = 33916;
       
        public static mdTableField JoinedPerson
        {
            get { return __MD.Fields[_JoinedPerson_fGid];}
        }
        public const string _JoinedPerson_fName = "JoinedPerson";        public const int _SocProfile_fGid = 33917;
       
        public static mdTableField SocProfile
        {
            get { return __MD.Fields[_SocProfile_fGid];}
        }
        public const string _SocProfile_fName = "SocProfile";        public const int _ApproveState_fGid = 33918;
       
        public static mdTableField ApproveState
        {
            get { return __MD.Fields[_ApproveState_fGid];}
        }
        public const string _ApproveState_fName = "ApproveState";        public const int _SocNet_fGid = 33929;
       
        public static mdTableField SocNet
        {
            get { return __MD.Fields[_SocNet_fGid];}
        }
        public const string _SocNet_fName = "SocNet";        public const int _Uid_fGid = 33930;
       
        public static mdTableField Uid
        {
            get { return __MD.Fields[_Uid_fGid];}
        }
        public const string _Uid_fName = "Uid";        public const int _IsContactToCurrent_fGid = 33950;
       
        public static mdTableField IsContactToCurrent
        {
            get { return __MD.Fields[_IsContactToCurrent_fGid];}
        }
        public const string _IsContactToCurrent_fName = "IsContactToCurrent";        public const int _IsJContactToCurrent_fGid = 33952;
       
        public static mdTableField IsJContactToCurrent
        {
            get { return __MD.Fields[_IsJContactToCurrent_fGid];}
        }
        public const string _IsJContactToCurrent_fName = "IsJContactToCurrent";       
        public static mdTableField OwnerPerson_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_OwnerPerson_fGid]);}
        }
        public const string _OwnerPerson_view__fName = "OwnerPerson_view_";       
        public static mdTableField JoinedPerson_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_JoinedPerson_fGid]);}
        }
        public const string _JoinedPerson_view__fName = "JoinedPerson_view_";       
        public static mdTableField SocProfile_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_SocProfile_fGid]);}
        }
        public const string _SocProfile_view__fName = "SocProfile_view_";       
        public static mdTableField SocNet_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_SocNet_fGid]);}
        }
        public const string _SocNet_view__fName = "SocNet_view_";       
        public static mdTableField OwnerPerson_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_OwnerPerson_fGid]);}
        }
        public const string _OwnerPerson_status__fName = "OwnerPerson_status_";       
        public static mdTableField JoinedPerson_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_JoinedPerson_fGid]);}
        }
        public const string _JoinedPerson_status__fName = "JoinedPerson_status_";       
        public static mdTableField SocProfile_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_SocProfile_fGid]);}
        }
        public const string _SocProfile_status__fName = "SocProfile_status_";       
        public static mdTableField SocNet_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_SocNet_fGid]);}
        }
        public const string _SocNet_status__fName = "SocNet_status_";
    }
    /// <summary>
    /// Схема таблиці 'Персони'
    /// </summary>
    public static class Persons_TS_ 
    {
        public static mdTable __MD
        {
            get 
            {
                return Persons_CTS_.__MD ;
            }
        }
        public const int _asid_fGid = 31120;
       
        public static mdTableField asid
        {
            get { return __MD.Fields[_asid_fGid];}
        }
        public const string _asid_fName = "asid";        public const int _SurNameAndInitials_fGid = 31347;
       
        public static mdTableField SurNameAndInitials
        {
            get { return __MD.Fields[_SurNameAndInitials_fGid];}
        }
        public const string _SurNameAndInitials_fName = "SurNameAndInitials";        public const int _CompPersonID_fGid = 32196;
       
        public static mdTableField CompPersonID
        {
            get { return __MD.Fields[_CompPersonID_fGid];}
        }
        public const string _CompPersonID_fName = "CompPersonID";        public const int _MainPhone_fGid = 33110;
       
        public static mdTableField MainPhone
        {
            get { return __MD.Fields[_MainPhone_fGid];}
        }
        public const string _MainPhone_fName = "MainPhone";        public const int _AuthorPerson_fGid = 33468;
       
        public static mdTableField AuthorPerson
        {
            get { return __MD.Fields[_AuthorPerson_fGid];}
        }
        public const string _AuthorPerson_fName = "AuthorPerson";        public const int _IsContactToCurrent_fGid = 33619;
       
        public static mdTableField IsContactToCurrent
        {
            get { return __MD.Fields[_IsContactToCurrent_fGid];}
        }
        public const string _IsContactToCurrent_fName = "IsContactToCurrent";        public const int _comments_fGid = 33620;
       
        public static mdTableField comments
        {
            get { return __MD.Fields[_comments_fGid];}
        }
        public const string _comments_fName = "comments";        public const int _WebAccountID_fGid = 33928;
       
        public static mdTableField WebAccountID
        {
            get { return __MD.Fields[_WebAccountID_fGid];}
        }
        public const string _WebAccountID_fName = "WebAccountID";
    }
    /// <summary>
    /// Схема таблиці 'Повідомлення користувачів'
    /// </summary>
    public static class PersonsMessages_TS_ 
    {
        public static mdTable __MD
        {
            get 
            {
                return PersonsMessages_CTS_.__MD ;
            }
        }
        public const int _version_no_fGid = 30928;
       
        public static mdTableField version_no
        {
            get { return __MD.Fields[_version_no_fGid];}
        }
        public const string _version_no_fName = "version_no";
    }
    /// <summary>
    /// Схема таблиці 'Персональний календар'
    /// </summary>
    public static class PersonsSchedule_TS_   
    {
        public const int __tGid = 15874;
        public const string __tName = "PersonsSchedule";
        static mdTable m_mdTable;
        static PersonsSchedule_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _gid_fGid = 33712;
       
        public static mdTableField gid
        {
            get { return __MD.Fields[_gid_fGid];}
        }
        public const string _gid_fName = "gid";        public const int _Event_Title_fGid = 33713;
       
        public static mdTableField Event_Title
        {
            get { return __MD.Fields[_Event_Title_fGid];}
        }
        public const string _Event_Title_fName = "Event_Title";        public const int _OwnerPerson_fGid = 33714;
       
        public static mdTableField OwnerPerson
        {
            get { return __MD.Fields[_OwnerPerson_fGid];}
        }
        public const string _OwnerPerson_fName = "OwnerPerson";        public const int _version_no_fGid = 33715;
       
        public static mdTableField version_no
        {
            get { return __MD.Fields[_version_no_fGid];}
        }
        public const string _version_no_fName = "version_no";        public const int _reg_date_fGid = 33716;
       
        public static mdTableField reg_date
        {
            get { return __MD.Fields[_reg_date_fGid];}
        }
        public const string _reg_date_fName = "reg_date";        public const int _object_status_fGid = 33717;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _Description_fGid = 33718;
       
        public static mdTableField Description
        {
            get { return __MD.Fields[_Description_fGid];}
        }
        public const string _Description_fName = "Description";        public const int _plan_date_fGid = 33719;
       
        public static mdTableField plan_date
        {
            get { return __MD.Fields[_plan_date_fGid];}
        }
        public const string _plan_date_fName = "plan_date";        public const int _fact_date_fGid = 33720;
       
        public static mdTableField fact_date
        {
            get { return __MD.Fields[_fact_date_fGid];}
        }
        public const string _fact_date_fName = "fact_date";        public const int _PartnerPerson_fGid = 33721;
       
        public static mdTableField PartnerPerson
        {
            get { return __MD.Fields[_PartnerPerson_fGid];}
        }
        public const string _PartnerPerson_fName = "PartnerPerson";        public const int _DataObject_fGid = 33722;
       
        public static mdTableField DataObject
        {
            get { return __MD.Fields[_DataObject_fGid];}
        }
        public const string _DataObject_fName = "DataObject";        public const int _DataObject_tid_fGid = 33723;
       
        public static mdTableField DataObject_tid
        {
            get { return __MD.Fields[_DataObject_tid_fGid];}
        }
        public const string _DataObject_tid_fName = "DataObject_tid";        public const int _EventType_fGid = 33724;
       
        public static mdTableField EventType
        {
            get { return __MD.Fields[_EventType_fGid];}
        }
        public const string _EventType_fName = "EventType";       
        public static mdTableField OwnerPerson_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_OwnerPerson_fGid]);}
        }
        public const string _OwnerPerson_view__fName = "OwnerPerson_view_";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";       
        public static mdTableField PartnerPerson_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_PartnerPerson_fGid]);}
        }
        public const string _PartnerPerson_view__fName = "PartnerPerson_view_";       
        public static mdTableField OwnerPerson_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_OwnerPerson_fGid]);}
        }
        public const string _OwnerPerson_status__fName = "OwnerPerson_status_";       
        public static mdTableField PartnerPerson_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_PartnerPerson_fGid]);}
        }
        public const string _PartnerPerson_status__fName = "PartnerPerson_status_";
    }
    /// <summary>
    /// Схема таблиці 'Персональні дані фізичних осіб'
    /// </summary>
    public static class PhysicalPersonsData_TS_   
    {
        public const int __tGid = 15568;
        public const string __tName = "PhysicalPersonsData";
        static mdTable m_mdTable;
        static PhysicalPersonsData_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _gid_fGid = 27878;
       
        public static mdTableField gid
        {
            get { return __MD.Fields[_gid_fGid];}
        }
        public const string _gid_fName = "gid";        public const int _Name_fGid = 27879;
       
        public static mdTableField Name
        {
            get { return __MD.Fields[_Name_fGid];}
        }
        public const string _Name_fName = "Name";        public const int _Middle_name_fGid = 27880;
       
        public static mdTableField Middle_name
        {
            get { return __MD.Fields[_Middle_name_fGid];}
        }
        public const string _Middle_name_fName = "Middle_name";        public const int _Surname_fGid = 27881;
       
        public static mdTableField Surname
        {
            get { return __MD.Fields[_Surname_fGid];}
        }
        public const string _Surname_fName = "Surname";        public const int _gender_fGid = 27882;
       
        public static mdTableField gender
        {
            get { return __MD.Fields[_gender_fGid];}
        }
        public const string _gender_fName = "gender";        public const int _ipn_fGid = 27883;
       
        public static mdTableField ipn
        {
            get { return __MD.Fields[_ipn_fGid];}
        }
        public const string _ipn_fName = "ipn";        public const int _passport_series_fGid = 27884;
       
        public static mdTableField passport_series
        {
            get { return __MD.Fields[_passport_series_fGid];}
        }
        public const string _passport_series_fName = "passport_series";        public const int _passport_number_fGid = 27885;
       
        public static mdTableField passport_number
        {
            get { return __MD.Fields[_passport_number_fGid];}
        }
        public const string _passport_number_fName = "passport_number";        public const int _passport_issuance_org_fGid = 27886;
       
        public static mdTableField passport_issuance_org
        {
            get { return __MD.Fields[_passport_issuance_org_fGid];}
        }
        public const string _passport_issuance_org_fName = "passport_issuance_org";        public const int _passport_issuance_date_fGid = 27887;
       
        public static mdTableField passport_issuance_date
        {
            get { return __MD.Fields[_passport_issuance_date_fGid];}
        }
        public const string _passport_issuance_date_fName = "passport_issuance_date";        public const int _birthday_fGid = 27888;
       
        public static mdTableField birthday
        {
            get { return __MD.Fields[_birthday_fGid];}
        }
        public const string _birthday_fName = "birthday";        public const int _registration_address_fGid = 27889;
       
        public static mdTableField registration_address
        {
            get { return __MD.Fields[_registration_address_fGid];}
        }
        public const string _registration_address_fName = "registration_address";        public const int _home_address_fGid = 27890;
       
        public static mdTableField home_address
        {
            get { return __MD.Fields[_home_address_fGid];}
        }
        public const string _home_address_fName = "home_address";        public const int _phones_fGid = 27891;
       
        public static mdTableField phones
        {
            get { return __MD.Fields[_phones_fGid];}
        }
        public const string _phones_fName = "phones";        public const int _object_status_fGid = 27892;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _version_no_fGid = 27893;
       
        public static mdTableField version_no
        {
            get { return __MD.Fields[_version_no_fGid];}
        }
        public const string _version_no_fName = "version_no";        public const int _comments_fGid = 27894;
       
        public static mdTableField comments
        {
            get { return __MD.Fields[_comments_fGid];}
        }
        public const string _comments_fName = "comments";        public const int _Full_Name_fGid = 27895;
       
        public static mdTableField Full_Name
        {
            get { return __MD.Fields[_Full_Name_fGid];}
        }
        public const string _Full_Name_fName = "Full_Name";        public const int _e_mail_fGid = 27896;
       
        public static mdTableField e_mail
        {
            get { return __MD.Fields[_e_mail_fGid];}
        }
        public const string _e_mail_fName = "e_mail";        public const int _OwnerPerson_fGid = 33711;
       
        public static mdTableField OwnerPerson
        {
            get { return __MD.Fields[_OwnerPerson_fGid];}
        }
        public const string _OwnerPerson_fName = "OwnerPerson";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";       
        public static mdTableField OwnerPerson_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_OwnerPerson_fGid]);}
        }
        public const string _OwnerPerson_view__fName = "OwnerPerson_view_";       
        public static mdTableField OwnerPerson_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_OwnerPerson_fGid]);}
        }
        public const string _OwnerPerson_status__fName = "OwnerPerson_status_";
    }
    /// <summary>
    /// Схема таблиці 'Журнал планера подій'
    /// </summary>
    public static class Planer_Events_Journal_TS_ 
    {
        public static mdTable __MD
        {
            get 
            {
                return Planer_Events_Journal_CTS_.__MD ;
            }
        }
        public const int _WorkComments_fGid = 29346;
       
        public static mdTableField WorkComments
        {
            get { return __MD.Fields[_WorkComments_fGid];}
        }
        public const string _WorkComments_fName = "WorkComments";        public const int _asid_fGid = 31119;
       
        public static mdTableField asid
        {
            get { return __MD.Fields[_asid_fGid];}
        }
        public const string _asid_fName = "asid";        public const int _AttachedFilesCount_fGid = 31702;
       
        public static mdTableField AttachedFilesCount
        {
            get { return __MD.Fields[_AttachedFilesCount_fGid];}
        }
        public const string _AttachedFilesCount_fName = "AttachedFilesCount";        public const int _Project_fGid = 31788;
       
        public static mdTableField Project
        {
            get { return __MD.Fields[_Project_fGid];}
        }
        public const string _Project_fName = "Project";        public const int _WebAuthor_fGid = 32048;
       
        public static mdTableField WebAuthor
        {
            get { return __MD.Fields[_WebAuthor_fGid];}
        }
        public const string _WebAuthor_fName = "WebAuthor";
    }
    /// <summary>
    /// Схема таблиці 'Пропозиції учасників'
    /// </summary>
    public static class Proposals_TS_   
    {
        public const int __tGid = 15787;
        public const string __tName = "Proposals";
        static mdTable m_mdTable;
        static Proposals_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _Operation_fGid = 31851;
       
        public static mdTableField Operation
        {
            get { return __MD.Fields[_Operation_fGid];}
        }
        public const string _Operation_fName = "Operation";        public const int _reg_date_fGid = 31852;
       
        public static mdTableField reg_date
        {
            get { return __MD.Fields[_reg_date_fGid];}
        }
        public const string _reg_date_fName = "reg_date";        public const int _doc_number_fGid = 31853;
       
        public static mdTableField doc_number
        {
            get { return __MD.Fields[_doc_number_fGid];}
        }
        public const string _doc_number_fName = "doc_number";        public const int _Title_fGid = 31854;
       
        public static mdTableField Title
        {
            get { return __MD.Fields[_Title_fGid];}
        }
        public const string _Title_fName = "Title";        public const int _Author_fGid = 31855;
       
        public static mdTableField Author
        {
            get { return __MD.Fields[_Author_fGid];}
        }
        public const string _Author_fName = "Author";        public const int _Project_fGid = 31856;
       
        public static mdTableField Project
        {
            get { return __MD.Fields[_Project_fGid];}
        }
        public const string _Project_fName = "Project";        public const int _PersonGroup_fGid = 31857;
       
        public static mdTableField PersonGroup
        {
            get { return __MD.Fields[_PersonGroup_fGid];}
        }
        public const string _PersonGroup_fName = "PersonGroup";        public const int _ApprovesCount_fGid = 31874;
       
        public static mdTableField ApprovesCount
        {
            get { return __MD.Fields[_ApprovesCount_fGid];}
        }
        public const string _ApprovesCount_fName = "ApprovesCount";        public const int _RejectsCount_fGid = 31875;
       
        public static mdTableField RejectsCount
        {
            get { return __MD.Fields[_RejectsCount_fGid];}
        }
        public const string _RejectsCount_fName = "RejectsCount";        public const int _CommentsCount_fGid = 31876;
       
        public static mdTableField CommentsCount
        {
            get { return __MD.Fields[_CommentsCount_fGid];}
        }
        public const string _CommentsCount_fName = "CommentsCount";       
        public static mdTableField Operation_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Operation_fGid]);}
        }
        public const string _Operation_view__fName = "Operation_view_";       
        public static mdTableField Author_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Author_fGid]);}
        }
        public const string _Author_view__fName = "Author_view_";       
        public static mdTableField Project_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_Project_fGid]);}
        }
        public const string _Project_view__fName = "Project_view_";       
        public static mdTableField PersonGroup_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_PersonGroup_fGid]);}
        }
        public const string _PersonGroup_view__fName = "PersonGroup_view_";       
        public static mdTableField Operation_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Operation_fGid]);}
        }
        public const string _Operation_status__fName = "Operation_status_";       
        public static mdTableField Author_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Author_fGid]);}
        }
        public const string _Author_status__fName = "Author_status_";       
        public static mdTableField Project_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_Project_fGid]);}
        }
        public const string _Project_status__fName = "Project_status_";       
        public static mdTableField PersonGroup_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_PersonGroup_fGid]);}
        }
        public const string _PersonGroup_status__fName = "PersonGroup_status_";
    }
    /// <summary>
    /// Схема таблиці 'Публічні запрошення'
    /// </summary>
    public static class PublicInvitations_TS_   
    {
        public const int __tGid = 15944;
        public const string __tName = "PublicInvitations";
        static mdTable m_mdTable;
        static PublicInvitations_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _gid_fGid = 34603;
       
        public static mdTableField gid
        {
            get { return __MD.Fields[_gid_fGid];}
        }
        public const string _gid_fName = "gid";        public const int _comments_fGid = 34604;
       
        public static mdTableField comments
        {
            get { return __MD.Fields[_comments_fGid];}
        }
        public const string _comments_fName = "comments";        public const int _version_no_fGid = 34605;
       
        public static mdTableField version_no
        {
            get { return __MD.Fields[_version_no_fGid];}
        }
        public const string _version_no_fName = "version_no";        public const int _object_status_fGid = 34606;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _PersonsGroup_fGid = 34607;
       
        public static mdTableField PersonsGroup
        {
            get { return __MD.Fields[_PersonsGroup_fGid];}
        }
        public const string _PersonsGroup_fName = "PersonsGroup";        public const int _start_date_fGid = 34608;
       
        public static mdTableField start_date
        {
            get { return __MD.Fields[_start_date_fGid];}
        }
        public const string _start_date_fName = "start_date";        public const int _end_date_fGid = 34609;
       
        public static mdTableField end_date
        {
            get { return __MD.Fields[_end_date_fGid];}
        }
        public const string _end_date_fName = "end_date";        public const int _EduGroupKind_fGid = 34610;
       
        public static mdTableField EduGroupKind
        {
            get { return __MD.Fields[_EduGroupKind_fGid];}
        }
        public const string _EduGroupKind_fName = "EduGroupKind";        public const int _UsersRole_fGid = 34611;
       
        public static mdTableField UsersRole
        {
            get { return __MD.Fields[_UsersRole_fGid];}
        }
        public const string _UsersRole_fName = "UsersRole";        public const int _name_fGid = 34612;
       
        public static mdTableField name
        {
            get { return __MD.Fields[_name_fGid];}
        }
        public const string _name_fName = "name";        public const int _RegistrationAdmin_fGid = 34615;
       
        public static mdTableField RegistrationAdmin
        {
            get { return __MD.Fields[_RegistrationAdmin_fGid];}
        }
        public const string _RegistrationAdmin_fName = "RegistrationAdmin";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";       
        public static mdTableField PersonsGroup_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_PersonsGroup_fGid]);}
        }
        public const string _PersonsGroup_view__fName = "PersonsGroup_view_";       
        public static mdTableField EduGroupKind_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_EduGroupKind_fGid]);}
        }
        public const string _EduGroupKind_view__fName = "EduGroupKind_view_";       
        public static mdTableField RegistrationAdmin_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_RegistrationAdmin_fGid]);}
        }
        public const string _RegistrationAdmin_view__fName = "RegistrationAdmin_view_";       
        public static mdTableField PersonsGroup_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_PersonsGroup_fGid]);}
        }
        public const string _PersonsGroup_status__fName = "PersonsGroup_status_";       
        public static mdTableField EduGroupKind_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_EduGroupKind_fGid]);}
        }
        public const string _EduGroupKind_status__fName = "EduGroupKind_status_";       
        public static mdTableField RegistrationAdmin_status_
        {
            get { return __MD.GetStatusField(__MD.Fields[_RegistrationAdmin_fGid]);}
        }
        public const string _RegistrationAdmin_status__fName = "RegistrationAdmin_status_";
    }
    /// <summary>
    /// Схема таблиці 'Службові звернення'
    /// </summary>
    public static class Service_Request_TS_ 
    {
        public static mdTable __MD
        {
            get 
            {
                return Service_Request_CTS_.__MD ;
            }
        }
        public const int _ExistsResponce_fGid = 30239;
       
        public static mdTableField ExistsResponce
        {
            get { return __MD.Fields[_ExistsResponce_fGid];}
        }
        public const string _ExistsResponce_fName = "ExistsResponce";        public const int _Project_fGid = 31790;
       
        public static mdTableField Project
        {
            get { return __MD.Fields[_Project_fGid];}
        }
        public const string _Project_fName = "Project";
    }
    /// <summary>
    /// Схема таблиці 'Типи вкладених файлів'
    /// </summary>
    public static class Types_of_attachments_TS_   
    {
        public const int __tGid = 5183;
        public const string __tName = "Types_of_attachments";
        static mdTable m_mdTable;
        static Types_of_attachments_TS_()
        {
            Metadata.OnMetadataReset += reset;
        }
        public static mdTable __MD
        {
            get 
            {
                if (m_mdTable == null)
                {
                    m_mdTable = Metadata.Tables[__tGid];
                }
                return m_mdTable;
            }
        }
        public static bool __Exists
        {
            get 
            {
                return (m_mdTable != null || Metadata.Tables.ContainsKey(__tGid));
            }
        }

        public static void reset()
        {
            m_mdTable = null;
        }
        public const int _gid_fGid = 21011;
       
        public static mdTableField gid
        {
            get { return __MD.Fields[_gid_fGid];}
        }
        public const string _gid_fName = "gid";        public const int _name_fGid = 21012;
       
        public static mdTableField name
        {
            get { return __MD.Fields[_name_fGid];}
        }
        public const string _name_fName = "name";        public const int _comments_fGid = 21013;
       
        public static mdTableField comments
        {
            get { return __MD.Fields[_comments_fGid];}
        }
        public const string _comments_fName = "comments";        public const int _object_status_fGid = 21014;
       
        public static mdTableField object_status
        {
            get { return __MD.Fields[_object_status_fGid];}
        }
        public const string _object_status_fName = "object_status";        public const int _version_no_fGid = 21015;
       
        public static mdTableField version_no
        {
            get { return __MD.Fields[_version_no_fGid];}
        }
        public const string _version_no_fName = "version_no";        public const int _FileFormat_fGid = 21016;
       
        public static mdTableField FileFormat
        {
            get { return __MD.Fields[_FileFormat_fGid];}
        }
        public const string _FileFormat_fName = "FileFormat";       
        public static mdTableField object_status_view_
        {
            get { return __MD.GetRepresentationField(__MD.Fields[_object_status_fGid]);}
        }
        public const string _object_status_view__fName = "object_status_view_";
    }
    /// <summary>
    /// Схема таблиці 'Типи сповіщень користувачам'
    /// </summary>
    public static class Types_of_UserNotifications_TS_ 
    {
        public static mdTable __MD
        {
            get 
            {
                return Types_of_UserNotifications_CTS_.__MD ;
            }
        }
        public const int _version_no_fGid = 23242;
       
        public static mdTableField version_no
        {
            get { return __MD.Fields[_version_no_fGid];}
        }
        public const string _version_no_fName = "version_no";
    }
    /// <summary>
    /// Схема таблиці 'Сповіщення користувачам'
    /// </summary>
    public static class User_Notifications_TS_ 
    {
        public static mdTable __MD
        {
            get 
            {
                return User_Notifications_CTS_.__MD ;
            }
        }
        public const int _version_no_fGid = 23253;
       
        public static mdTableField version_no
        {
            get { return __MD.Fields[_version_no_fGid];}
        }
        public const string _version_no_fName = "version_no";
    }    /// <summary>
    /// Схема параметрів сеансу в системі
    /// </summary>
    public static class _SP_Shema_
    {
        public const string _Date_of_report_spName = "Date_of_report";        public const int _Date_of_report_spGid = 100;
        public static mdSessionParameter Date_of_report
            {
            get { return Metadata.SessionParameters[_Date_of_report_spGid]; }
            }

        public const string _Current_storage_spName = "Current_storage";        public const int _Current_storage_spGid = 101;
        public static mdSessionParameter Current_storage
            {
            get { return Metadata.SessionParameters[_Current_storage_spGid]; }
            }

        public const string _app_version_spName = "app_version";        public const int _app_version_spGid = 102;
        public static mdSessionParameter app_version
            {
            get { return Metadata.SessionParameters[_app_version_spGid]; }
            }

        public const string _Account_Company_Person_spName = "Account_Company_Person";        public const int _Account_Company_Person_spGid = 103;
        public static mdSessionParameter Account_Company_Person
            {
            get { return Metadata.SessionParameters[_Account_Company_Person_spGid]; }
            }

        public const string _Working_date_spName = "Working_date";        public const int _Working_date_spGid = 105;
        public static mdSessionParameter Working_date
            {
            get { return Metadata.SessionParameters[_Working_date_spGid]; }
            }

        public const string _Date_of_begin_spName = "Date_of_begin";        public const int _Date_of_begin_spGid = 106;
        public static mdSessionParameter Date_of_begin
            {
            get { return Metadata.SessionParameters[_Date_of_begin_spGid]; }
            }

        public const string _Date_of_end_spName = "Date_of_end";        public const int _Date_of_end_spGid = 107;
        public static mdSessionParameter Date_of_end
            {
            get { return Metadata.SessionParameters[_Date_of_end_spGid]; }
            }

        public const string _Current_database_type_spName = "Current_database_type";        public const int _Current_database_type_spGid = 118;
        public static mdSessionParameter Current_database_type
            {
            get { return Metadata.SessionParameters[_Current_database_type_spGid]; }
            }

        public const string _IntArray_spName = "IntArray";        public const int _IntArray_spGid = 124;
        public static mdSessionParameter IntArray
            {
            get { return Metadata.SessionParameters[_IntArray_spGid]; }
            }

        public const string _Current_external_automation_system_spName = "Current_external_automation_system";        public const int _Current_external_automation_system_spGid = 126;
        public static mdSessionParameter Current_external_automation_system
            {
            get { return Metadata.SessionParameters[_Current_external_automation_system_spGid]; }
            }

        public const string _main_company_spName = "main_company";        public const int _main_company_spGid = 127;
        public static mdSessionParameter main_company
            {
            get { return Metadata.SessionParameters[_main_company_spGid]; }
            }

        public const string _Main_External_automation_system_spName = "Main_External_automation_system";        public const int _Main_External_automation_system_spGid = 130;
        public static mdSessionParameter Main_External_automation_system
            {
            get { return Metadata.SessionParameters[_Main_External_automation_system_spGid]; }
            }

        public const string _Term1_spName = "Term1";        public const int _Term1_spGid = 136;
        public static mdSessionParameter Term1
            {
            get { return Metadata.SessionParameters[_Term1_spGid]; }
            }

        public const string _LocalDate_spName = "LocalDate";        public const int _LocalDate_spGid = 138;
        public static mdSessionParameter LocalDate
            {
            get { return Metadata.SessionParameters[_LocalDate_spGid]; }
            }

        public const string _Current_EMailBox_spName = "Current_EMailBox";        public const int _Current_EMailBox_spGid = 143;
        public static mdSessionParameter Current_EMailBox
            {
            get { return Metadata.SessionParameters[_Current_EMailBox_spGid]; }
            }

        public const string _Base_Mail_Box_spName = "Base_Mail_Box";        public const int _Base_Mail_Box_spGid = 345;
        public static mdSessionParameter Base_Mail_Box
            {
            get { return Metadata.SessionParameters[_Base_Mail_Box_spGid]; }
            }

        public const string _Storage_Id_spName = "Storage_Id";        public const int _Storage_Id_spGid = 355;
        public static mdSessionParameter Storage_Id
            {
            get { return Metadata.SessionParameters[_Storage_Id_spGid]; }
            }

        public const string _DBType_filter_spName = "DBType_filter";        public const int _DBType_filter_spGid = 404;
        public static mdSessionParameter DBType_filter
            {
            get { return Metadata.SessionParameters[_DBType_filter_spGid]; }
            }

        public const string _account_filter_spName = "account_filter";        public const int _account_filter_spGid = 405;
        public static mdSessionParameter account_filter
            {
            get { return Metadata.SessionParameters[_account_filter_spGid]; }
            }

        public const string _QnA_Group_spName = "QnA_Group";        public const int _QnA_Group_spGid = 432;
        public static mdSessionParameter QnA_Group
            {
            get { return Metadata.SessionParameters[_QnA_Group_spGid]; }
            }

        public const string _Account_Person_spName = "Account_Person";        public const int _Account_Person_spGid = 434;
        public static mdSessionParameter Account_Person
            {
            get { return Metadata.SessionParameters[_Account_Person_spGid]; }
            }

        public const string _WebAccount_spName = "WebAccount";        public const int _WebAccount_spGid = 435;
        public static mdSessionParameter WebAccount
            {
            get { return Metadata.SessionParameters[_WebAccount_spGid]; }
            }

        public const string _CurPerson_spName = "CurPerson";        public const int _CurPerson_spGid = 436;
        public static mdSessionParameter CurPerson
            {
            get { return Metadata.SessionParameters[_CurPerson_spGid]; }
            }

        public const string _CurRequestType_spName = "CurRequestType";        public const int _CurRequestType_spGid = 438;
        public static mdSessionParameter CurRequestType
            {
            get { return Metadata.SessionParameters[_CurRequestType_spGid]; }
            }

        public const string _CurUserEMail_spName = "CurUserEMail";        public const int _CurUserEMail_spGid = 439;
        public static mdSessionParameter CurUserEMail
            {
            get { return Metadata.SessionParameters[_CurUserEMail_spGid]; }
            }

        public const string _CurUserEMailConfirmed_spName = "CurUserEMailConfirmed";        public const int _CurUserEMailConfirmed_spGid = 440;
        public static mdSessionParameter CurUserEMailConfirmed
            {
            get { return Metadata.SessionParameters[_CurUserEMailConfirmed_spGid]; }
            }


    }
    /// <summary>
    /// Схема ролей користувачів в системі
    /// </summary>
    public static class _User_Roles_
    {
        public const string _DataBase_Owner_rName = "DataBase_Owner";        public const int _DataBase_Owner_rGid = 0;
        public static mdRole DataBase_Owner
            {
            get { return Metadata.Roles[_DataBase_Owner_rGid]; }
            }

        public const string _Server_Admin_rName = "Server_Admin";        public const int _Server_Admin_rGid = 1;
        public static mdRole Server_Admin
            {
            get { return Metadata.Roles[_Server_Admin_rGid]; }
            }

        public const string _Full_Reader_rName = "Full_Reader";        public const int _Full_Reader_rGid = 2;
        public static mdRole Full_Reader
            {
            get { return Metadata.Roles[_Full_Reader_rGid]; }
            }

        public const string _CoreUser_rName = "CoreUser";        public const int _CoreUser_rGid = 89;
        public static mdRole CoreUser
            {
            get { return Metadata.Roles[_CoreUser_rGid]; }
            }

        public const string _Guest_rName = "Guest";        public const int _Guest_rGid = 91;
        public static mdRole Guest
            {
            get { return Metadata.Roles[_Guest_rGid]; }
            }

        public const string _FinManager_rName = "FinManager";        public const int _FinManager_rGid = 609;
        public static mdRole FinManager
            {
            get { return Metadata.Roles[_FinManager_rGid]; }
            }

        public const string _BudjetManager_rName = "BudjetManager";        public const int _BudjetManager_rGid = 610;
        public static mdRole BudjetManager
            {
            get { return Metadata.Roles[_BudjetManager_rGid]; }
            }

        public const string _edit_fin_directories_rName = "edit_fin_directories";        public const int _edit_fin_directories_rGid = 611;
        public static mdRole edit_fin_directories
            {
            get { return Metadata.Roles[_edit_fin_directories_rGid]; }
            }

        public const string _Operator_MoneyAccount_rName = "Operator_MoneyAccount";        public const int _Operator_MoneyAccount_rGid = 737;
        public static mdRole Operator_MoneyAccount
            {
            get { return Metadata.Roles[_Operator_MoneyAccount_rGid]; }
            }

        public const string _AssetsOperator_rName = "AssetsOperator";        public const int _AssetsOperator_rGid = 738;
        public static mdRole AssetsOperator
            {
            get { return Metadata.Roles[_AssetsOperator_rGid]; }
            }

        public const string _EditProjDocumentation_rName = "EditProjDocumentation";        public const int _EditProjDocumentation_rGid = 739;
        public static mdRole EditProjDocumentation
            {
            get { return Metadata.Roles[_EditProjDocumentation_rGid]; }
            }

        public const string _PaymentsApprover_rName = "PaymentsApprover";        public const int _PaymentsApprover_rGid = 750;
        public static mdRole PaymentsApprover
            {
            get { return Metadata.Roles[_PaymentsApprover_rGid]; }
            }

        public const string _ProjectMember_rName = "ProjectMember";        public const int _ProjectMember_rGid = 753;
        public static mdRole ProjectMember
            {
            get { return Metadata.Roles[_ProjectMember_rGid]; }
            }

        public const string _ProjectViewer_rName = "ProjectViewer";        public const int _ProjectViewer_rGid = 755;
        public static mdRole ProjectViewer
            {
            get { return Metadata.Roles[_ProjectViewer_rGid]; }
            }

        public const string _WebDataView_rName = "WebDataView";        public const int _WebDataView_rGid = 761;
        public static mdRole WebDataView
            {
            get { return Metadata.Roles[_WebDataView_rGid]; }
            }

        public const string _BusinessAgent_rName = "BusinessAgent";        public const int _BusinessAgent_rGid = 762;
        public static mdRole BusinessAgent
            {
            get { return Metadata.Roles[_BusinessAgent_rGid]; }
            }


    }
    /// <summary>
    /// Статична інформація про проект
    /// </summary>
    public static class _ProjectInfo_
    {
        public const int ProjectID = 215;
        public const string ProjectName = @"Кванта";
        public const string ProjectVersion = "1.7.200";
        public const string ProjectDescription = @"";

    }

}