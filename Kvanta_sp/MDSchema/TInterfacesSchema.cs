using System;
using System.Linq;
using AS_MetaData;
using asb_ipc;

namespace MD_ViewsSchema
{    
    public class IProjAssetsDocs_TIS_ 
    {
        public const string __iName = "IProjAssetsDocs";
        public const int __iGid = 121;
        private static mdTableInterface s_TableInterface_;
        public static mdTableInterface __TableInterface
        {
            get 
            {
                if (s_TableInterface_ == null)
                    s_TableInterface_ = Metadata.TableInterfaces[121];
                return s_TableInterface_;
            }
        }
        
        public static IProjAssetsDocs_TIS_[] ReadFromDB(IDataManager pDM, mdEvaluationParameters pFilter, mdTable pTable)
        {
            if (pDM == null || pTable == null)
                return null;
            ValueTableSchema lSchema = pTable.GetInterfaceImplementationSchema(__TableInterface);
            if (lSchema == null)
                throw new Exception(string.Format("Таблиця {0} не реалізує інтерфейс IProjAssetsDocs.", pTable));

            return pDM.ReadValueTable(lSchema, pFilter).Select(r => new IProjAssetsDocs_TIS_(r, true)).ToArray();
        }

        private ValueTableRow m_ValuesRow_;
        public ValueTableRow _ValuesRow
        {
            get 
            {
                return m_ValuesRow_;
            }
        }
        private mdTable m_MDTable_;
        public mdTable __MDTable
        {
            get 
            {
                return m_MDTable_;
            }
        }
		public IProjAssetsDocs_TIS_(mdTable pTable)
        {
            m_ValuesRow_ = new ValueTableRow(pTable.GetInterfaceImplementationSchema(__TableInterface));
            m_MDTable_ = pTable;
        }
        private IProjAssetsDocs_TIS_(ValueTableRow pRow_, bool pIsChecked)
        {
            if (pIsChecked)
            {
                m_ValuesRow_ = pRow_;
                m_MDTable_ = pRow_.Schema.BaseTable;
            }
            else
            {
                m_MDTable_ = pRow_.Schema.BaseTable;
                var lSchema = m_MDTable_.GetInterfaceImplementationSchema(__TableInterface);
                if (lSchema == null)
                    throw new Exception(string.Format("Таблиця {0} не реалізує інтерфейс IProjAssetsDocs.", m_MDTable_.Name));
                if (lSchema == pRow_.Schema)
                    m_ValuesRow_ = pRow_;
                else
                    m_ValuesRow_ = new ValueTableRow(lSchema, lSchema.Select(f => pRow_[f]).ToArray());
            }
        }
        public IProjAssetsDocs_TIS_(ValueTableRow pRow_)
            :this(pRow_, false)
        {
        }

        public static mdInterfaceField reg_date_field 
        {
            get 
            {
                return __TableInterface.Fields[260];
            }
        }
        public mdTableField reg_date_field_ 
        {
            get 
            {
                return __MDTable.GetFieldOfInterfaceImplementation(__TableInterface.Fields[260]);
            }
        }
        public System.DateTime reg_date
        {
            get 
            {
                return m_ValuesRow_.GetDateTime(__MDTable.GetFieldOfInterfaceImplementation(__TableInterface.Fields[260]));
            }
        }
        public static mdInterfaceField doc_number_field 
        {
            get 
            {
                return __TableInterface.Fields[261];
            }
        }
        public mdTableField doc_number_field_ 
        {
            get 
            {
                return __MDTable.GetFieldOfInterfaceImplementation(__TableInterface.Fields[261]);
            }
        }
        public System.String doc_number
        {
            get 
            {
                return m_ValuesRow_.GetString(__MDTable.GetFieldOfInterfaceImplementation(__TableInterface.Fields[261]));
            }
        }
        public static mdInterfaceField document_field 
        {
            get 
            {
                return __TableInterface.Fields[262];
            }
        }
        public mdTableField document_field_ 
        {
            get 
            {
                return __MDTable.GetFieldOfInterfaceImplementation(__TableInterface.Fields[262]);
            }
        }
        public System.Int32 document
        {
            get 
            {
                return m_ValuesRow_.GetInt(__MDTable.GetFieldOfInterfaceImplementation(__TableInterface.Fields[262]));
            }
        }
        public static mdInterfaceField FinProject_field 
        {
            get 
            {
                return __TableInterface.Fields[263];
            }
        }
        public mdTableField FinProject_field_ 
        {
            get 
            {
                return __MDTable.GetFieldOfInterfaceImplementation(__TableInterface.Fields[263]);
            }
        }
        public System.Int32 FinProject
        {
            get 
            {
                return m_ValuesRow_.GetInt(__MDTable.GetFieldOfInterfaceImplementation(__TableInterface.Fields[263]));
            }
        }
        public static mdInterfaceField ProjectAsset_field 
        {
            get 
            {
                return __TableInterface.Fields[264];
            }
        }
        public mdTableField ProjectAsset_field_ 
        {
            get 
            {
                return __MDTable.GetFieldOfInterfaceImplementation(__TableInterface.Fields[264]);
            }
        }
        public System.Int32 ProjectAsset
        {
            get 
            {
                return m_ValuesRow_.GetInt(__MDTable.GetFieldOfInterfaceImplementation(__TableInterface.Fields[264]));
            }
        }
        public static mdInterfaceField contragent_field 
        {
            get 
            {
                return __TableInterface.Fields[265];
            }
        }
        public mdTableField contragent_field_ 
        {
            get 
            {
                return __MDTable.GetFieldOfInterfaceImplementation(__TableInterface.Fields[265]);
            }
        }
        public System.Int32 contragent
        {
            get 
            {
                return m_ValuesRow_.GetInt(__MDTable.GetFieldOfInterfaceImplementation(__TableInterface.Fields[265]));
            }
        }
        public static mdInterfaceField Amount_field 
        {
            get 
            {
                return __TableInterface.Fields[266];
            }
        }
        public mdTableField Amount_field_ 
        {
            get 
            {
                return __MDTable.GetFieldOfInterfaceImplementation(__TableInterface.Fields[266]);
            }
        }
        public System.Decimal Amount
        {
            get 
            {
                return m_ValuesRow_.GetDecimal(__MDTable.GetFieldOfInterfaceImplementation(__TableInterface.Fields[266]));
            }
        }
        public static mdInterfaceField comments_field 
        {
            get 
            {
                return __TableInterface.Fields[267];
            }
        }
        public mdTableField comments_field_ 
        {
            get 
            {
                return __MDTable.GetFieldOfInterfaceImplementation(__TableInterface.Fields[267]);
            }
        }
        public System.String comments
        {
            get 
            {
                return m_ValuesRow_.GetString(__MDTable.GetFieldOfInterfaceImplementation(__TableInterface.Fields[267]));
            }
        }
        public static mdInterfaceField object_status_field 
        {
            get 
            {
                return __TableInterface.Fields[268];
            }
        }
        public mdTableField object_status_field_ 
        {
            get 
            {
                return __MDTable.GetFieldOfInterfaceImplementation(__TableInterface.Fields[268]);
            }
        }
        public System.Byte object_status
        {
            get 
            {
                return m_ValuesRow_.GetByte(__MDTable.GetFieldOfInterfaceImplementation(__TableInterface.Fields[268]));
            }
        }
    }
}
