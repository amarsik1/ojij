using System;
using System.Linq;
using AS_MetaData;
using asb_ipc;
using MD_Schema;

namespace MD_ViewsSchema
{
    /// <summary>
    /// Схема представлення 'Проекти'
    /// </summary>
    public class FinProjects_V_VS_ 
    {
        public const string __vName = "FinProjects_V";
        public const int __vGid = 2161;
        public static mdTable __MDTable
        {
            get 
            {
                return Metadata.Tables[15461] ;
            }
        }
        public static mdTableView __MDView
        {
            get 
            {
                return Metadata.TableViews[2161] ;
            }
        }
        public static FinProjects_V_VS_[] ReadFromDB(IDataManager pDM, mdEvaluationParameters pFilter)
        {
            return pDM.ReadTableView(__MDView, pFilter).Select(r => new FinProjects_V_VS_(r)).ToArray();
        }

        private ValueTableRow m_ValuesRow_;
        public ValueTableRow _ValuesRow
        {
            get 
            {
                return m_ValuesRow_;
            }
        }
        public FinProjects_V_VS_(ValueTableRow pRow_)
        {
            m_ValuesRow_ = pRow_;   
        }
        public FinProjects_V_VS_()
        {
            m_ValuesRow_ = new ValueTableRow(__MDView.Schema);   
        }
        public System.Int32 gid
        {
            get 
            {
                return m_ValuesRow_.GetInt(__MDTable.Fields[26386]);
            }
            set 
            {
                m_ValuesRow_[__MDTable.Fields[26386]] = value;
            }
        }
        public System.Byte object_status
        {
            get 
            {
                return m_ValuesRow_.GetByte(__MDTable.Fields[26389]);
            }
            set 
            {
                m_ValuesRow_[__MDTable.Fields[26389]] = value;
            }
        }
        public string object_status_FV_
        {
            get 
            {
                return m_ValuesRow_.GetFormatedString(__MDTable.Fields[26389]);
            }
     }
        public System.String comments
        {
            get 
            {
                return m_ValuesRow_.GetString(__MDTable.Fields[26391]);
            }
            set 
            {
                m_ValuesRow_[__MDTable.Fields[26391]] = value;
            }
        }
        public System.Int32 Project_Group
        {
            get 
            {
                return m_ValuesRow_.GetInt(__MDTable.Fields[27373]);
            }
            set 
            {
                m_ValuesRow_[__MDTable.Fields[27373]] = value;
            }
        }
        public string Project_Group_FV_
        {
            get 
            {
                return m_ValuesRow_.GetFormatedString(__MDTable.Fields[27373]);
            }
     }
        public System.String name
        {
            get 
            {
                return m_ValuesRow_.GetString(__MDTable.Fields[26387]);
            }
            set 
            {
                m_ValuesRow_[__MDTable.Fields[26387]] = value;
            }
        }
        public System.String Description
        {
            get 
            {
                return m_ValuesRow_.GetString(__MDTable.Fields[26390]);
            }
            set 
            {
                m_ValuesRow_[__MDTable.Fields[26390]] = value;
            }
        }
        public System.Int32 responsible_person
        {
            get 
            {
                return m_ValuesRow_.GetInt(__MDTable.Fields[28935]);
            }
            set 
            {
                m_ValuesRow_[__MDTable.Fields[28935]] = value;
            }
        }
        public string responsible_person_FV_
        {
            get 
            {
                return m_ValuesRow_.GetFormatedString(__MDTable.Fields[28935]);
            }
     }
    }
}
