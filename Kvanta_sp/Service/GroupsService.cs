﻿using ADE.Core;
using ADE.EntityModel;
using Entity_Schema.Education;
using Entity_Schema.RegInfo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Entity_Schema.Education.GroupsInfoSet.S;

namespace K7P_sp.Service
{
    public class GroupsService : ServiceBase
    {
        public GroupsService(IExecutionContext pContext)
            : base(pContext)
        {

        }

        public EduGroup[] GetAll()
        {
            var lRes = new AppRoot();
            var lGroupsInfo = Context.EntitiesCache.GetEntity<GroupsInfoSet>();
            return lGroupsInfo.EduGroups.ToArray();
        }

        [OutFormat("html", "EduGroupsList")]
        public EduGroup[] List()
        {
            return GetAll();
        }

        [OutFormat("html", "EduGroupEdit")]
        public EduGroup EditGroup(int id)
        {
            if (id > 0)
            {
                var lGroupsInfo = Context.EntitiesCache.GetEntity<GroupsInfoSet>();
                return lGroupsInfo.EduGroups.GetItem(id);
            }
            var lRes = new EduGroup() { ID = id };
            return lRes;
        }

        public OperationResult SaveEduGroup(EduGroup pGroup)
        {
            if (pGroup == null)
                return new OperationResult(EnumOperationStatus.ClientError, "No data");
            if (pGroup.ID > 0)
                return UpdateGroup(pGroup);
            else
                return CreateGroup(pGroup);
        }

        public Tuple<int, string>[] GetAllIdsAndNames()
        {
            var lRes = new AppRoot();
            var lGroupsInfo = Context.EntitiesCache.GetEntity<GroupsInfoSet>();
            return lGroupsInfo.EduGroups.Select(x => Tuple.Create(x.ID, x.name)).ToArray();
        }

        public EduGroup[] GetGroupsByPerson(int id)
        {
            var lGroupsInfo = Context.EntitiesCache.GetEntity<GroupsInfoSet>();
            var lStudentItems = lGroupsInfo.EduStudents.Person.GetItemsByKey(id);
            if (lStudentItems == null)
                return new EduGroup[0];
            var lEG = lGroupsInfo.EduGroups;
            return lStudentItems.Select(hv => lEG.GetItem(hv.Group)).ToArray();
        }

        public GroupsInfoSet GetStudentsByGroup(int id)
        {
            //int groupID = group.ID;
            var lGroupsInfo = Context.EntitiesCache.GetEntity<GroupsInfoSet>();
            var lSt = lGroupsInfo.EduStudents;
            var lItems = lSt.Group.GetItemsByKey(id);

            var lRes = new GroupsInfoSet();
            if (lItems != null)
            foreach (var hv in lItems)
                lRes.EduStudents.Add(hv.Clone());
            lRes.EduGroups.Add(lGroupsInfo.EduGroups.GetItem(id).Clone());
            return lRes;
        }

        public OperationResult AddStudentToGroup(EduStudent student)
        {
            if (student == null)
                return new OperationResult(EnumOperationStatus.ClientError, "No Data!");
            if (student.Group == 0)
                return new OperationResult(EnumOperationStatus.ClientError, "No Group!");

            var lProxy = Context.EntitiesCache.DataProxy;
            lProxy.InsertEntities(new[] { student });

            var lStudents = Context.EntitiesCache.GetEntity<GroupsInfoSet>().EduStudents;
            if (lStudents.Count != 0)
                lStudents.Add(student);

            return new OperationResult();
        }
        /*
        public OperationResult MoveStudentToGroup(Tuple<int,int,int> studentAndGroup)
        {
            var studentRes = GetStudent(studentAndGroup.Item1);
            if (!studentRes.IsOK) {
                return studentRes;
            }
            //TODO: check group
            studentRes.Data.Group = studentAndGroup.Item2;
            //TODO: why update entities is void, but not bool????
            DataManager.UpdateEntities(new[] { studentRes.Data });
            return new OperationResult(EnumOperationStatus.OK);
        }
        */
        private OperationResult<EduStudent[]> GetStudentByGroup(int groupId)
        {
            var studsDs = Context.EntitiesCache.GetEntity<GroupsInfoSet>().EduStudents;
            studsDs.EnsureLoaded(DataManager, true);
            var result = studsDs.Group.GetItemsByKey(groupId);
            if (result == null)
            {
                return new OperationResult<EduStudent[]>(EnumOperationStatus.NotFound);
            }
            return new OperationResult<EduStudent[]>(result);
        }

        public OperationResult<RegStart.S.EduGroupKind[]> GetGroupKinds()
        {
            var lStartModel = Context.EntitiesCache.GetEntity<RegStart>();
            var lGroups = lStartModel?.EduGroupKinds;
            if (lGroups == null)
                return new OperationResult<RegStart.S.EduGroupKind[]>(EnumOperationStatus.NotFound, "No Data!");
            return new OperationResult<RegStart.S.EduGroupKind[]>(lGroups.ToArray());
        }

        public OperationResult RemoveStudentFromGroup(EduStudent student)
        {
            if (student == null)
                return new OperationResult(EnumOperationStatus.ClientError, "No Data!");

            var lProxy = Context.EntitiesCache.DataProxy;
            var lStudents = Context.EntitiesCache.GetEntity<GroupsInfoSet>().EduStudents;

            if (lStudents.Count == 0)
                lStudents.LoadAll(lProxy, true);
            if (!lStudents.Remove(student))
                return new OperationResult(EnumOperationStatus.NotFound, $"Student with ID {student.Person} not found");

            lProxy.DeleteEntities(new[] { student });
            return new OperationResult();
        }

        public OperationResult<EduStudent> MoveStudent(Tuple<int,int,int> pInfo) // personId, oldGroupId, newGroupId
        {
            var lGroupsInfo = Context.EntitiesCache.GetEntity<GroupsInfoSet>();
            EduGroup oldGroup = lGroupsInfo.EduGroups.GetItem(pInfo.Item1);

            OperationResult result = RemoveStudentFromGroup(lGroupsInfo.EduStudents.First(x => x.Group == pInfo.Item2 && x.Person == pInfo.Item1));
            if (!result.IsOK)
                return new OperationResult<EduStudent>(result.StatusCode, result.Message);

            EduStudent newStudent = new EduStudent();
            newStudent.Group = pInfo.Item3;
            newStudent.Person = pInfo.Item1;
            result = AddStudentToGroup(newStudent);
            if (!result.IsOK)
                return new OperationResult<EduStudent>(result.StatusCode, result.Message);
            return new OperationResult<EduStudent>(newStudent);

        }

        public OperationResult CreateGroup(EduGroup group)
        {
            if (group == null)
                return new OperationResult(EnumOperationStatus.ClientError, "No Data!");
            var lProxy = Context.EntitiesCache.DataProxy;
            lProxy.InsertEntities(new[] { group });
            var lGroups = Context.EntitiesCache.GetEntity<GroupsInfoSet>().EduGroups;
            if (lGroups.Count != 0)
                lGroups.Add(group);
            return new OperationResult();
        }

        public OperationResult UpdateGroup(EduGroup group)
        {
            if (group == null)
                return new OperationResult(EnumOperationStatus.ClientError, "No Data!");

            var lGroupsInfo = Context.EntitiesCache.GetEntity<GroupsInfoSet>();
            var lExisting = lGroupsInfo.EduGroups.GetItem(group.ID);
            if (lExisting == null)
                return new OperationResult(EnumOperationStatus.NotFound, $"Group with ID {group.ID} not exists!");

            var lProxy = Context.EntitiesCache.DataProxy;
            lProxy.UpdateEntities(new[] { group });
            group.CopyTo(lExisting);

            return new OperationResult(EnumOperationStatus.OK);
        }

        public OperationResult DeleteGroup(EduGroup group)
        {
            if (group == null)
                return new OperationResult(EnumOperationStatus.ClientError, "No Data!");
            var lProxy = Context.EntitiesCache.DataProxy;
            var lGroups = Context.EntitiesCache.GetEntity<GroupsInfoSet>().EduGroups;

            if (lGroups.Count == 0)
                lGroups.LoadAll(lProxy, true);
            if (lGroups.Remove(group))
                return new OperationResult(EnumOperationStatus.NotFound, $"Group with ID {group.ID} not found");

            lProxy.DeleteEntities(new[] { group });
            return new OperationResult();
        }
    }
}
