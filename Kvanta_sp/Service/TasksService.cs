﻿using ADE.Core;
using ADE.EntityModel;
using Entity_Schema.Education;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Entity_Schema.Education.MaterialsInfo.S;

namespace K7P_sp.Service
{
    class TasksService : ServiceBase
    {
        public TasksService(IExecutionContext pContext)
            : base(pContext)
        {

        }

        public OperationResult CreateMaterial(EduMaterials material)
        {
            if (material == null)
                return new OperationResult(EnumOperationStatus.ClientError, "No data!");

            if (material.MaterialsText == null || material.MaterialsText == "") 
                return new OperationResult(EnumOperationStatus.ClientError, "No text!");

            var lProxy = Context.EntitiesCache.DataProxy;
            lProxy.InsertEntities(new[] { material });

            var lMaterials = Context.EntitiesCache.GetEntity<MaterialsInfo>().EduMaterials;
            if (lMaterials.Count != 0)
                lMaterials.Add(material.Clone());

            return new OperationResult(EnumOperationStatus.OK);
        }

        public OperationResult UpdateMaterial(EduMaterials material)
        {
            if (material == null)
                return new OperationResult(EnumOperationStatus.ClientError, "No data!");
            if (material.MaterialsText == null || material.MaterialsText == "")
                return new OperationResult(EnumOperationStatus.ClientError, "No text!");

            var lMaterials = Context.EntitiesCache.GetEntity<MaterialsInfo>().EduMaterials;
            if (lMaterials.Count == 0)
                lMaterials.LoadAll(Context.EntitiesCache.DataProxy, true);

            var lExisting = lMaterials.GetItem(material.ID);
            if (lExisting == null)
                return new OperationResult(EnumOperationStatus.NotFound, $"No material with ID {material.ID}");

            Context.EntitiesCache.DataProxy.UpdateEntities(new[] { material });
            lExisting = material;

            return new OperationResult(EnumOperationStatus.OK);
        }

        public OperationResult DeleteMaterial(EduMaterials material)
        {
            if (material == null)
                return new OperationResult(EnumOperationStatus.ClientError, "No Data!");

            var lMaterials = Context.EntitiesCache.GetEntity<MaterialsInfo>().EduMaterials;

            if (lMaterials.Remove(material))
                return new OperationResult(EnumOperationStatus.NotFound, $"Marial with ID {material.ID} not found");

            var lProxy = Context.EntitiesCache.DataProxy;

            lProxy.DeleteEntities(new[] { material });

            return new OperationResult();
        }
    }
}
