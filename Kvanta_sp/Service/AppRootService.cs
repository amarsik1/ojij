﻿using ADE.EntityModel;
using Entity_Schema.Education;
using Entity_Schema.RegInfo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Entity_Schema.Education.GroupsInfoSet.S;

namespace K7P_sp.Service
{
    public class AppRootService : ServiceBase
    {
        public AppRootService(IExecutionContext pContext)
            : base(pContext)
        {

        }
        [OutFormat("html")]
        public AppRoot Cabinet(DataRequest pRequest)
        {
            var lRes = new AppRoot();
            var lGroupsInfo = Context.EntitiesCache.GetEntity<GroupsInfoSet>();
            lRes.Groups = lGroupsInfo.EduGroups.ToArray();
            return lRes;
        }

        public RegStart StartInfo(DataRequest pRequest)
        {
            return Context.EntitiesCache.GetEntity<RegStart>();
        }

    }
}
