﻿using ADE.Core;
using ADE.Core.Model;
using ADE.EntityModel;
using Entity_Schema.Education;
using Entity_Schema.RegInfo;
using K7_sp;
using System.Collections.Generic;
using System.Linq;
using static Entity_Schema.RegInfo.UserEventsSet.S;

namespace K7P_sp.Service
{
    public class AccountService : ServiceBase
    {
        public AccountService(IExecutionContext pContext)
            : base(pContext)
        {

        }

        public OperationResult<Web_Account> Register(RegisterAccount rgForm)
        {
            if (rgForm == null)
                return new OperationResult<Web_Account>(EnumOperationStatus.ClientError, "No Data");
            if (string.IsNullOrEmpty(rgForm.InviteCode))
                return new OperationResult<Web_Account>(EnumOperationStatus.ClientError, "No Invitation Code");
            Dictionary<string, string> lExtraData = new Dictionary<string, string>();
            if (!string.IsNullOrEmpty(rgForm.ChildSurName))
                lExtraData[RegisterAccount.S._ChildSurName] = rgForm.ChildSurName;
            if (!string.IsNullOrEmpty(rgForm.ChildName))
                lExtraData[RegisterAccount.S._ChildName] = rgForm.ChildName;
            if (!string.IsNullOrEmpty(rgForm.ChildEmail))
                lExtraData[RegisterAccount.S._ChildEmail] = rgForm.ChildEmail;
            int lGender = 0;
            Web_Account lWA = new Web_Account()
            {
                Surname = rgForm.Surname,
                FirstName = rgForm.FirstName,
                Middle_name = rgForm.Middle_name,
                e_mail_address = rgForm.Email,
                phones = rgForm.PhoneNo,
                login = rgForm.Email,
                password_code = rgForm.password,
                BirthDate = rgForm.BirthDate,
                Gender = lGender,
                InviteCode = rgForm.InviteCode,
                comments = rgForm.comments,
                object_status = AS_MetaData.Metadata.MinActiveStatus,
                AdditionalInfo = string.Join(";\n",
                    lExtraData.Where(x => !string.IsNullOrEmpty(x.Value))
                    .Select(k => $"{k.Key} = {k.Value}"))
            };

            var lRes = Context.EntitiesCache.DataProxy.ExecCommand<Web_Account, Web_Account>((int)CustomCommands.RegisterWebUser, lWA);
            if (lRes.IsOK)
            {
                var lData = lRes.Data;
                if (lData != null && lData.Length > 0)
                {
                    var lResWA = lData[0];
                    var lMailService = Context.GetSendingChannel("Mail");
                    lMailService?.SendData(lResWA.e_mail_address, lResWA.Full_Name, "OnRegisteredUserLetter", lResWA);

                    return new OperationResult<Web_Account>(new Web_Account
                    {
                        gid = lResWA.gid,
                        Guid = lResWA.Guid,
                        e_mail_address = lResWA.e_mail_address
                    });
                }
            }
            return new OperationResult<Web_Account>(lRes.StatusCode, lRes.Message);
        }

        public OperationResult<ConfirmEmailResult> ConfirmEmail(ConfirmEmailResult pData)
        {
            if (pData == null)
                return new OperationResult<ConfirmEmailResult>(EnumOperationStatus.ClientError, "No Data");
            if (string.IsNullOrEmpty(pData.EmailAddress))
                return new OperationResult<ConfirmEmailResult>(EnumOperationStatus.ClientError, "No Email Address");
            pData.ResponseCode = pData.EmailAddress;
            var lRes = Context.EntitiesCache.DataProxy.ExecCommand<ConfirmEmailResult, ConfirmEmailResult>((int)CustomCommands.ConfirmEmailAddress, pData);
            if (lRes.IsOK)
            {
                var lData = lRes.Data;
                if (lData != null && lData.Length > 0)
                    return new OperationResult<ConfirmEmailResult>(lData[0]);
            }
            return new OperationResult<ConfirmEmailResult>(lRes.StatusCode, lRes.Message);
        }

        public OperationResult<byte> CheckRequestCode(string code)
        {
            var lCodeData = Context.EntitiesCache.DataProxy.ReadEntities<ConfirmEmailResult>(r => r.RequestCode == code);
            if (lCodeData == null || lCodeData.Length == 0)
                return new OperationResult<byte>(EnumOperationStatus.NotFound);
            else
                return new OperationResult<byte>((byte)lCodeData[0].object_status);
        }

        public OperationResult<UserInfo> Info()
        {
            return new OperationResult<UserInfo>(Context.User);

        }
    }
}
