﻿using System.Configuration;
using System.Web.Http;
using Newtonsoft.Json;
using appcore;
using GApi.Lib;
using System;
using Unity;
using Owin;
using Unity.Lifetime;
using System.Linq;
using K7_Web.Providers;
using K7_Web.Models;
using appcore.Log;
using ADE.Core.Interfaces;
using System.Collections.Generic;

namespace K7_Web.AtlasAPI
{
    public class KvantaApplication : AppRootBase, IDisposable
    {
        public IUnityContainer Container { get; private set; }

        private KvantaAppContext appContext;
        private BaseApplicationOAuthProvider authProvider;
        private IMessageManager logger;

        public KvantaApplication()
        {
            logger = new DummyLogger();
            //logger = new SQLCEMessageLogger()

            K7P_sp.ModulesLoader.LoadModules();

            var lCoreURL = ConfigurationManager.AppSettings["CoreURL"];
            appContext = new KvantaAppContext(ConfigurationManager.ConnectionStrings["Atlas.Cn"].ConnectionString, logger, AppDefinition.PRODUCT_NAME, 
                AppDefinition.PRODUCT_ID, new Dictionary<string, string> { { "CoreURL", lCoreURL } });
            authProvider = new BaseApplicationOAuthProvider(appContext, new GApi.Lib.Auth.ClaimsParserBase(), Consts.AuthAppClientID);

            Container = new UnityContainer();
            Container.RegisterInstance(logger);
            Container.RegisterInstance<IMessageManagerBase>(logger);
            Container.RegisterInstance<AtlasContextCore>(appContext, new ContainerControlledLifetimeManager());
            Container.RegisterInstance(authProvider, new ContainerControlledLifetimeManager());

            var customCommands = new int[] {
                (int)K7_sp.CustomCommands.RegisterWebUser,
                (int)K7_sp.CustomCommands.ConfirmPasswordReset,
                (int)K7_sp.CustomCommands.RequestPasswordReset,
                (int)K7_sp.CustomCommands.AddContactPerson,
                (int)K7_sp.CustomCommands.AddContactPerson,
                (int)K7_sp.CustomCommands.AddExtContactPerson,
                (int)K7_sp.CustomCommands.SetInvitation,
                (int)K7_sp.CustomCommands.GetInvitationInfo,
                (int)K7_sp.CustomCommands.AcceptInvitation,
                (int)K7_sp.CustomCommands.AcceptInvitation,
                (int)K7_sp.CustomCommands.ChangePassword
            };


            base.Init(Container, lCoreURL, 
                ConfigurationManager.AppSettings["RequireHTTPS"] == "true", appContext, authProvider, customCommands);


        }

        public override void Configure(IAppBuilder app)
        {
            base.Configure(app);
        }

        public override void Register(HttpConfiguration config)
        {
            //RouteConfig.RegisterRoutes(System.Web.Routing.RouteTable.Routes);
            base.Register(config);

            config.Routes.MapHttpRoute(
                 "WithActionApi",
                 "api/{controller}/{action}"
             );

            //config.Routes.MapHttpRoute(
            //    "DefaultApi",
            //    "api/{controller}/{id}",
            //    new { action = "DefaultAction", id = System.Web.Http.RouteParameter.Optional }
            //);

           
            //config.Routes.MapHttpRoute(
            //    name: "GApi",
            //    //routeTemplate: "api/{controller}/{action}/{id}",
            //    routeTemplate: "gapi/{service}/{action}",
            //    defaults: new { controller = "ApplicationService", service = RouteParameter.Optional, action = RouteParameter.Optional }
            //);
        }

        public void Dispose()
        {
            if (Container != null)
            {
                Container.Dispose();
            }
        }


        //public static KvantaAppContext CurrentAtlasContext
        //{
        //    get { return s_AtlasAppContext; }
        //}
        //private static KvantaAppContext s_AtlasAppContext;
        //private static Dictionary<string, string> s_RootGraphPages = new Dictionary<string, string>();
        //private static List<string> s_RootPages = new List<string>();
        //public static bool ContainsRootPage(string pName)
        //{
        //    return s_RootPages.Contains(pName);
        //}
        //public static bool ContainsRootGraphPage(string pName)
        //{
        //    return s_RootGraphPages.ContainsKey(pName);
        //}
        //public static string GetRootGraphPageModule(string pName)
        //{
        //    string lModule;
        //    s_RootGraphPages.TryGetValue(pName, out lModule);
        //    return lModule;
        //}


        ////public static void LogException(Exception ex)
        ////{
        ////    Logger.LogException(ex);
        ////}

        //public static void CheckConnection()
        //{
        //    var lAtlasContext = s_AtlasAppContext;
        //    if (!lAtlasContext.CheckConnection()) return;
        //    RootCommandsController.Reset();
        //    onReconnect();
        //}

        //private static GApi.Lib.Models.MailBox getNewFromVTR(ValueTableRow pItemVTR)
        //{
        //    if (pItemVTR == null || !AS_MetaData.Metadata.IsActiveStatus(pItemVTR.GetByte(e_mail_boxes_TS_.object_status))) return null;
        //    var lServAddr = pItemVTR.GetString(e_mail_boxes_TS_.smtp_server);
        //    var lKind = GApi.Lib.Models.MailBox.Kinds.Other;
        //    if (!string.IsNullOrEmpty(lServAddr))
        //    {
        //        if (lServAddr.StartsWith("@SendGrid#"))
        //            lKind = GApi.Lib.Models.MailBox.Kinds.SendGrid;
        //        else if (lServAddr.StartsWith("@SparkPost#"))
        //            lKind = GApi.Lib.Models.MailBox.Kinds.Sparkpost;
        //        else
        //            lKind = GApi.Lib.Models.MailBox.Kinds.Smtp;
        //    }
        //    var lBox = new GApi.Lib.Models.MailBox(
        //        pID: pItemVTR.GetInt(e_mail_boxes_TS_.gid),
        //        pSenderEMail: pItemVTR.GetString(e_mail_boxes_TS_.e_mail),
        //        pSenderName: pItemVTR.GetString(e_mail_boxes_TS_.sender_name),
        //        pAccountLogin: pItemVTR.GetString(e_mail_boxes_TS_.account_login),
        //        pAccountPW: pItemVTR.GetString(e_mail_boxes_TS_.account_password),
        //        pServerAddress: pItemVTR.GetString(e_mail_boxes_TS_.smtp_server),
        //        pServerPort: pItemVTR.GetInt(e_mail_boxes_TS_.port),
        //        pAuthenticationCheck: pItemVTR.GetBool(e_mail_boxes_TS_.authentication),
        //        pUseSSL: pItemVTR.GetBool(e_mail_boxes_TS_.SSL),
        //        pBoxKind: lKind
        //    );
        //    return lBox;
        //}

        //public override void OnStart()
        //{
        //    GlobalConfiguration.Configure(WebApiConfig.Register);
        //    //RouteConfig.RegisterRoutes(RouteTable.Routes);
        //    var formatter = GlobalConfiguration.Configuration.Formatters.JsonFormatter;
        //    formatter.SerializerSettings = new JsonSerializerSettings
        //    {
        //        Formatting = Formatting.Indented,
        //        TypeNameHandling = TypeNameHandling.Objects,
        //        ContractResolver = new CamelCasePropertyNamesContractResolver()
        //    };
        //    GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
        //    GlobalConfiguration.Configuration.Formatters.Remove(GlobalConfiguration.Configuration.Formatters.XmlFormatter);

        //    AppCore.init(null, "7K_WEB_UI", 119, AppComponentTypes.WinService, BinaryIoInitializer.Instance);
        //    var lSettings = ConfigurationManager.ConnectionStrings["Atlas.Cn"].ConnectionString;
        //    if (string.IsNullOrEmpty(lSettings)) return;
        //    var lAtlasContext = new KvantaAppContext(lSettings);
        //    s_AtlasAppContext = lAtlasContext;
        //    TheOnlyProjectID = AppCore.GetIntValue(ConfigurationManager.AppSettings["TheOnlyProjectID"]);
        //    TheCompanyName = AppCore.GetStringValue(ConfigurationManager.AppSettings["TheCompanyName"]);
        //    CoreURL = AppCore.GetStringValue(ConfigurationManager.AppSettings["CoreURL"]);
        //    RequireHTTPS = AppCore.GetBooleanValue(ConfigurationManager.AppSettings["RequireHTTPS"]);

        //    WebGraphController.SetCoreURL(CoreURL);
        //    MailController.Init(_SP_Shema_.Base_Mail_Box, e_mail_boxes_TS_.__MD);

        //    GSessionContextBase.SetAtlasCommandsMap(new Dictionary<string, int> {
        //        { "RegisterWebUser", (int)K7_sp.CustomCommands.RegisterWebUser },
        //        { "RequestEmailConfirm", (int)K7_sp.CustomCommands.RequestEmailConfirm },
        //        { "ConfirmPasswordReset", (int)K7_sp.CustomCommands.ConfirmPasswordReset },
        //        { "RequestPasswordReset", (int)K7_sp.CustomCommands.RequestPasswordReset },
        //        { "AddContactPerson", (int)K7_sp.CustomCommands.AddContactPerson },
        //        { "AddExtContactPerson", (int)K7_sp.CustomCommands.AddExtContactPerson },
        //        { "SetInvitation", (int)K7_sp.CustomCommands.SetInvitation },
        //        { "GetInvitationInfo", (int)K7_sp.CustomCommands.GetInvitationInfo },
        //        { "AcceptInvitation", (int)K7_sp.CustomCommands.AcceptInvitation },
        //        { "ChangePassword", (int)K7_sp.CustomCommands.ChangePassword }
        //    });
        //    GSessionContextBase.SetRootSessionAtlasCommands(
        //        (int)K7_sp.CustomCommands.RegisterWebUser,
        //        (int)K7_sp.CustomCommands.RequestPasswordReset,
        //        (int)K7_sp.CustomCommands.ConfirmPasswordReset,
        //        (int)K7_sp.CustomCommands.GetInvitationInfo);

        //    var lRootPages = AppCore.GetStringValue(ConfigurationManager.AppSettings["RootPages"]);
        //    s_RootPages.Clear();
        //    if (!string.IsNullOrEmpty(lRootPages))
        //    {
        //        var lStrParts = lRootPages.Split(',');
        //        foreach (var p in lStrParts)
        //            s_RootPages.Add(p.Trim());
        //    }
        //    lRootPages = AppCore.GetStringValue(ConfigurationManager.AppSettings["RootGraphPages"]);
        //    s_RootGraphPages.Clear();
        //    if (!string.IsNullOrEmpty(lRootPages))
        //    {
        //        var lStrParts = lRootPages.Split(';');
        //        foreach (var lPart in lStrParts)
        //        {
        //            var lModule = "User";
        //            var lSubParts = lPart.Split(':');
        //            if (lSubParts.Length > 1)
        //                lModule = lSubParts[1].Trim();
        //            var lPages = lSubParts[0].Split(',');
        //            foreach (var p in lPages)
        //                s_RootGraphPages[p.Trim()] = lModule;
        //        }
        //    }

        //    ExternalFileItem.Init(Object_Attachments_TS_.FileFormat);
        //    GApi.Lib.Models.MailBox.Init(new ValueTableSchema(
        //        e_mail_boxes_TS_.__MD,
        //        e_mail_boxes_TS_.gid,
        //        e_mail_boxes_TS_.object_status,
        //        e_mail_boxes_TS_.e_mail,
        //        e_mail_boxes_TS_.sender_name,
        //        e_mail_boxes_TS_.account_login,
        //        e_mail_boxes_TS_.account_password,
        //        e_mail_boxes_TS_.smtp_server,
        //        e_mail_boxes_TS_.port,
        //        e_mail_boxes_TS_.SSL,
        //        e_mail_boxes_TS_.authentication),
        //        getNewFromVTR);
        //    base.OnStart();
        //    GSessionContextBase.FinishInit();
        //}

        ////protected void Application_BeginRequest()
        ////{
        ////    if (!RequireHTTPS || Context.Request.IsSecureConnection) return;
        ////    // This is an insecure connection, so redirect to the secure version
        ////    var uri = new UriBuilder(Context.Request.Url) { Scheme = "https" };
        ////    if (uri.Port > 32000 && uri.Host.Equals("localhost"))
        ////    {
        ////        // Development box - set uri.Port to 44300 by default
        ////        uri.Port = 44356;
        ////    }
        ////    else
        ////    {
        ////        uri.Port = 443;
        ////    }
        ////    Response.Redirect(uri.ToString());
        ////}

        ////protected void Application_End()
        ////{
        ////    TelegramBot.Api.DeleteWebhookAsync().Wait();
        ////    //ViberProvider.Instance.SetWebHook("").Wait();
        ////}
    }
}