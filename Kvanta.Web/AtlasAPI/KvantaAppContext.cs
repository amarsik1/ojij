﻿using asb_ipc;
using MD_Schema;
using K7_sp;
using appcore;
using System.Collections.Generic;

namespace K7_Web.AtlasAPI
{
    public class KvantaAppContext : AtlasContextCore
    {
        public KvantaAppContext(string settingsString, IMessageManager messageManager, string projName, short projId, Dictionary<string, string> appParams = null) 
            : base(settingsString, messageManager, projName, projId, appParams)
        {
            MailService = new KvantaMailingService(this);
        }

        //protected override bool initSchemas(ref ValueTableSchema pRequestsVTS, ref ValueTableSchema pWebBrowsersSchema, ref ValueTableSchema pWebSessionsSchema)
        //{
        //    pWebBrowsersSchema = new ValueTableSchema(WebBrowsers_TS_.__MD, WebBrowsers_TS_.__MD.GetPhysicalFields());
        //    pWebSessionsSchema = new ValueTableSchema(WebSessions_TS_.__MD, WebSessions_TS_.__MD.GetPhysicalFields());
        //    pRequestsVTS = new ValueTableSchema(Web_Requests_TS_.__MD,
        //        Web_Requests_TS_.reg_date,
        //        Web_Requests_TS_.object_status,
        //        Web_Requests_TS_.RequestCode,
        //        Web_Requests_TS_.WebAccount,
        //        Web_Requests_TS_.WebAccount_view_,
        //        Web_Requests_TS_.AccountLogin,
        //        Web_Requests_TS_.ResponseCode,
        //        Web_Requests_TS_.gid);
        //    return base.initSchemas(ref pRequestsVTS, ref pWebBrowsersSchema, ref pWebSessionsSchema);
        //}



        //TODO: move
        //protected override void init_Web_Accounts_Index()
        //{

        //    var dm = RootDM;
        //    if (dm == null || dm.IsDisposed) return;
        //    lock (dm)
        //    {
        //        if (s_Web_Accounts_EMailIndex != null) return;
        //        var lSchema = new ValueTableSchema(Web_Accounts_TS_.__MD,
        //            Web_Accounts_CTS_.gid,
        //            Web_Accounts_TS_.EMailConfirmTime,
        //            Web_Accounts_CTS_.e_mail_address,
        //            Web_Accounts_CTS_.object_status);
        //        s_Web_Accounts_EMail_FI = lSchema.IndexOf(Web_Accounts_CTS_.e_mail_address);
        //        s_Web_Accounts_Status_FI = lSchema.IndexOf(Web_Accounts_CTS_.object_status);
        //        s_WA_ConfirmTime_F = Web_Accounts_TS_.EMailConfirmTime;
        //        s_Web_Account_ConfirmTime_FI = lSchema.IndexOf(s_WA_ConfirmTime_F);
        //        dm.SubscribeDataUpdate(lSchema, onWebAccountUpdated, DBSubscripctionModes.General);
        //        s_Web_Accounts_VT = RootDM.ReadValueTable(lSchema, Web_Accounts_TS_.__MD.NewActiveStatusFilter(dm));
        //        if (s_Web_Accounts_VT.CountOfRows > 0)
        //            foreach (var r in s_Web_Accounts_VT)
        //                r[s_Web_Accounts_EMail_FI] = r.GetString(s_Web_Accounts_EMail_FI).ToLower();
        //        s_Web_Accounts_EMailIndex = s_Web_Accounts_VT.GetIndex(Web_Accounts_CTS_.e_mail_address);
        //        s_Web_Accounts_Key = s_Web_Accounts_VT.GetIndex(Web_Accounts_CTS_.gid);
        //    }
        //}

        //TODO: move
        //public override int ValidateRequestCode(string pCode, double pMaxTimeMinutes, out ValueTableRow pVTR)
        //{
        //    pVTR = null;
        //    var dm = RootDM;
        //    var lFilter = Web_Requests_TS_.__MD.NewFilter(Web_Requests_TS_.RequestCode, pCode, dm);
        //    var lData = dm.ReadValueTable(RequestsVTS, lFilter);
        //    if (lData == null || lData.CountOfRows <= 0) return 0;
        //    pVTR = lData[0];
        //    if (pVTR.GetByte(Web_Requests_TS_.object_status) == Metadata.MinActiveStatus)
        //        return (DateTime.Now - pVTR.GetDateTime(Web_Requests_TS_.reg_date)).TotalMinutes <= pMaxTimeMinutes ? 1 : 9;
        //    return 8;
        //}

        //protected override void onConnect()
        //{
        //    RootEntityCache = new EntitiesContainer("Domain", new EntityDataProxy(RootDM as DataManager));
        //    var lUIs = Metadata.UserInterfaces.ToArray();

        //    //var lJson = Newtonsoft.Json.JsonConvert.SerializeObject(lUIs[0]);
        //    //System.IO.File.WriteAllText("D:\\Test_UI.json", lJson);
        //    base.onConnect();
        //}

        //TODO: move to AccountsService
        //public bool RejectWebRequest(string pRequestCode)
        //{
        //    var cr = s_RequestsByCodeIndex?.GetRowByKey(pRequestCode);
        //    if (cr != null)
        //        cr[Web_Requests_TS_.object_status] = Object_statuses_CTS_._dr_Blocked_Gid;
        //    var lVT = new ValueTable(Web_Requests_TS_.__MD, Web_Requests_TS_.RequestCode);
        //    lVT.AddRow(pRequestCode);
        //    lVT = RootDM.ProcessCustomCommand((int)CustomCommands.RejectWebRequest, lVT);
        //    if (lVT == null || lVT.CountOfRows <= 0 || lVT.BaseTable != Web_Requests_TS_.__MD) return false;
        //    var r = lVT[0];
        //    return r.GetByte(Web_Requests_TS_.object_status) == Object_statuses_CTS_._dr_Blocked_Gid;
        //}

        protected override void saveWebSession(string ip, string browser, int pWAid, IDataManager pDM)
        {
            var lt = new ValueTable(new ValueTableSchema(WebSessions_TS_.__MD, WebSessions_TS_.WebAccount, WebSessions_TS_.IpAddress, WebSessions_TS_.HostName));
            lt.AddRow(pWAid, ip, browser);
            lt = pDM.ProcessCustomCommand((int)CustomCommands.SaveWebSession, lt);
        }

        //TODO: move to AccountsService
        //public UserLoginStatus CheckUserEmail(string pEmail)
        //{
        //    var rgx = new Regex(@"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");
        //    var lisEmail = rgx.IsMatch(pEmail);
        //    if (!lisEmail) return UserLoginStatus.WrongEmail;
        //    try
        //    {
        //        var r = checkAccountsEmail(pEmail);
        //        if (r == null)
        //            return UserLoginStatus.NotExists;
        //        return r.IsEmpty(Web_Accounts_TS_.EMailConfirmTime) ? UserLoginStatus.EmailNotConfirmed : UserLoginStatus.EmailConfirmed;
        //    }
        //    catch (Exception e)
        //    {
        //        Logger.LogException("AtlasAppContext.CheckUserEmail", e);
        //        return UserLoginStatus.InternalError;
        //    }
        //}

        //TODO: move to AccountsService
        //public CustomCommandsResponse SendChangePasswordRequest(int accounId, string email, out string token)
        //{
        //    token = "";
        //    var wrt = new ValueTable(new ValueTableSchema(Web_Requests_TS_.__MD, Web_Requests_TS_.WebAccount, Web_Requests_TS_.RequestCode));
        //    wrt.AddRow(accounId, email);
        //    try
        //    {
        //        var vt = RootDM.ProcessCustomCommand((int)CustomCommands.RequestPasswordReset, wrt);
        //        if (vt == null || vt.CountOfRows == 0)
        //            return CustomCommandsResponse.BadRequest;
        //        var r = vt[0];
        //        if (vt.BaseTable == System_log_CTS_.__MD)
        //        {
        //            var code = r.GetShort(0);
        //            switch (code)
        //            {
        //                case (short)ResultCodes.IncorrectNumerOfRows:
        //                    return CustomCommandsResponse.IncorrectNumerOfRows;

        //                case (short)ResultCodes.IncorrectBaseTable:
        //                    return CustomCommandsResponse.IncorrectBaseTable;

        //                case (short)ResultCodes.LimitHasExceeded:
        //                    return CustomCommandsResponse.LimitHasExceeded;

        //                case (short)ResultCodes.IncorrectEmail:
        //                    return CustomCommandsResponse.IncorrectEmail;
        //            }
        //            return CustomCommandsResponse.BadRequest;
        //        }
        //        token = r.GetString(Web_Requests_TS_.RequestCode);
        //        return vt.BaseTable == Web_Requests_TS_.__MD && !string.IsNullOrEmpty(token) ? CustomCommandsResponse.Ok : CustomCommandsResponse.BadRequest;
        //    }
        //    catch (Exception e)
        //    {
        //        Logger.LogException("AtlasAppContext.SendChangePasswordRequest", e);
        //        return CustomCommandsResponse.BadRequest;
        //    }
        //}

        //TODO: move to AccountsService
        //public CustomCommandsResponse ConfirmPasswordReset(string token, string newPassword, out string email)
        //{
        //    email = string.Empty;
        //    var wrt = new ValueTable(new ValueTableSchema(Web_Requests_TS_.__MD, Web_Requests_TS_.RequestCode, Web_Requests_TS_.ResponseCode));
        //    wrt.AddRow(token, newPassword);
        //    try
        //    {
        //        var vt = RootDM.ProcessCustomCommand((int)CustomCommands.ConfirmPasswordReset, wrt);
        //        if (vt == null || vt.CountOfRows == 0)
        //            return CustomCommandsResponse.BadRequest;
        //        var r = vt[0];
        //        if (vt.BaseTable == System_log_CTS_.__MD)
        //        {
        //            var code = r.GetShort(0);
        //            switch (code)
        //            {
        //                case (short)ResultCodes.IncorrectNumerOfRows:
        //                    return CustomCommandsResponse.IncorrectNumerOfRows;
        //                case (short)ResultCodes.IncorrectBaseTable:
        //                    return CustomCommandsResponse.IncorrectBaseTable;
        //                case (short)ResultCodes.IncorrectRequestCode:
        //                    return CustomCommandsResponse.IncorrectRequestCode;
        //                case (short)ResultCodes.DuplicateOperation:
        //                    return CustomCommandsResponse.AlreadyComplete;
        //                case (short)ResultCodes.InactualOperation:
        //                    return CustomCommandsResponse.InactualOperation;
        //                default:
        //                    return CustomCommandsResponse.BadRequest;
        //            }
        //        }
        //        email = r.GetString(Web_Requests_TS_.ResponseCode);
        //        return CustomCommandsResponse.Ok;
        //    }
        //    catch (Exception e)
        //    {
        //        Logger.LogException("AtlasAppContext.ConfirmPasswordReset", e);
        //        return CustomCommandsResponse.BadRequest;
        //    }
        //}

        //TODO: move to AccountsService
        //public bool UpdateUserInfo(UserProfileInfo profile)
        //{
        //    var lvt = new ValueTable(new ValueTableSchema(Web_Accounts_CTS_.__MD,
        //       Web_Accounts_CTS_.gid,
        //       Web_Accounts_TS_.Surname,
        //       Web_Accounts_TS_.FirstName,
        //       Web_Accounts_TS_.Middle_name,
        //       Web_Accounts_TS_.MainPhone,
        //       Web_Accounts_TS_.ExtraData));
        //    var r = lvt.AddRow();
        //    r[Web_Accounts_CTS_.gid] = profile.Id;
        //    r[Web_Accounts_TS_.Surname] = profile.Surname;
        //    r[Web_Accounts_TS_.FirstName] = profile.Name;
        //    r[Web_Accounts_TS_.Middle_name] = profile.Patronymic;
        //    r[Web_Accounts_TS_.MainPhone] = profile.Phone;
        //    try
        //    {
        //        var lData = RootDM.ProcessCustomCommand((int)CustomCommands.UpdateWebUserProfile, lvt);
        //        if (lData != null && lData.CountOfRows > 0)
        //            return true;
        //    }
        //    catch (Exception e)
        //    {
        //        Logger.LogException(e);
        //    }
        //    return false;
        //}

        //TODO: move to AccountsService
        //public CustomCommandsResponse ConfirmResetPassword(string requestCode)
        //{
        //    var wrtSchema = new ValueTableSchema(Web_Requests_TS_.__MD, Web_Requests_TS_.RequestCode);
        //    var wrt = new ValueTable(wrtSchema);
        //    var r = wrt.AddRow();
        //    r[Web_Requests_TS_.RequestCode] = requestCode;
        //    try
        //    {
        //        var vt = RootDM.ProcessCustomCommand((int)CustomCommands.ConfirmEmailAddress, wrt);
        //        if (vt == null || vt.CountOfRows == 0) return CustomCommandsResponse.BadRequest;
        //        r = vt[0];
        //        if (vt.BaseTable != System_log_CTS_.__MD)
        //            return vt.BaseTable == Web_Requests_TS_.__MD ? CustomCommandsResponse.Ok : CustomCommandsResponse.BadRequest;
        //        var code = r.GetShort(0);
        //        switch (code)
        //        {
        //            case (short)ResultCodes.DuplicateOperation:
        //                return CustomCommandsResponse.AlreadyComplete;
        //            case (short)ResultCodes.InactualOperation:
        //                return CustomCommandsResponse.InactualOperation;
        //        }
        //        return CustomCommandsResponse.BadRequest;
        //    }
        //    catch (Exception e)
        //    {
        //        Logger.LogException("AtlasAppContext.ConfirmResetPassword", e);
        //        return CustomCommandsResponse.BadRequest;
        //    }
        //}

        //TODO: move to AccountsService
        //public bool ResendEmailConfirm(string pEmail, out string pToken)
        //{
        //    pToken = null;
        //    var r = checkAccountsEmail(pEmail);
        //    if (r == null) return false;
        //    var gid = r.GetInt(Web_Accounts_CTS_.gid);
        //    return !string.IsNullOrEmpty(pToken);
        //}
    }
}