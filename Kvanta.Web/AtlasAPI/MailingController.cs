﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MD_Schema;
using Entity_Schema.MailingInfo;
using K7_Web.Controllers;
using GApi.Lib.Models;
using static Entity_Schema.RegInfo.UserEventsSet.S;

namespace K7_Web.AtlasAPI
{
    public class KvantaMailingService: MailingService
    {
       
        public KvantaMailingService(AtlasContextCore pAppContext)
            :base(pAppContext, pAppContext.RootDM.GetSPValue(_SP_Shema_.Base_Mail_Box).GetInt())
        {
            loadMailBoxes();
        }
        private void loadMailBoxes()
        {
            MailBox.Reset();
            var lMailBoxesInfo = AppContext.RootEntityCache.GetEntity<MailBoxesSet>();
            foreach (var b in lMailBoxesInfo.e_mail_boxes)
            {
                MailBox.Set(new MailBox(b.ID, b.e_mail, b.sender_name,
                    b.account_login, b.account_password, b.smtp_server, b.port, b.authentication, b.SSL,
                    MailBox.Kinds.Smtp));
            }
        }

    }
}