﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using appcore;
using appcore.DataProcessing;
using GApi.Lib.Controllers;

namespace K7_Web.AtlasAPI
{
    public class UnitTests
    {
        public static void TestG_Redis()
        {
            var lFileName = @"D:\GDataTest.xml";
            if (!File.Exists(lFileName))
                return;
            var lXML = XMLTree.ReadFromFile(lFileName);
            var lGraph = DataGraph.FromXMLTree(lXML);
            var lRedis = RedisManager.Instance;
            var lStorage = new GraphBinStorage(lRedis);
            lStorage.StoreAllData(lGraph, "SC");
        }
        public static void TestG_Read()
        {
            var lRedis = RedisManager.Instance;
            var lVals = lRedis.GetAllKeyValues("GraphSchema");
            if (lVals != null)
            {
                var lStorage = new GraphBinStorage(lRedis);
                foreach (var k in lVals)
                {
                    var lFName = @"D:\Temp\_Test_" + k.Key.Replace(":", "-");
                    if (File.Exists(lFName + ".xml"))
                    {
                        var lIx = 1;
                        var lFName2 = lFName + lIx + ".xml";
                        while (File.Exists(lFName2 + ".xml"))
                        {
                            lFName2 = lFName + (++lIx) + ".xml";
                        }
                        lFName = lFName2;
                    }
                    File.WriteAllText(@"D:\Temp\_Test_" + k.Key.Replace(":", "-") + ".xml", k.Value);
                }
                if (lVals.Count==1)
                {
                    var lGSchema = GraphXmlConvertor.DeserializeSchema(lVals.Values.First());
                    var lGraph = lStorage.ReadAllData(lGSchema, "SC");
                    if (lGraph != null)
                        GraphXmlConvertor.SerializeToFile(lGraph, @"D:\GDataTestResult" + DateTime.Now.ToString("yyyyMMdd_HH_mm_ss") + ".xml");
                }
            }
        }
    }
}