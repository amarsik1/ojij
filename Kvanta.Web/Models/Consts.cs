﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace K7_Web.Models
{
    public class Consts
    {
        public const string ConfirmRegistrationTemplateName = "7K.User.ConfirmRegistration";

        public const string AuthAppClientID = "KvantaApp";

        public class SessionKeys
        {
            public const string UAppContext = "7K.UAppContext";

            public const string PersonInfo = "7K.PersonInfo";
            public const string UserName = "7K.UserName";

            public const string UAppId = "7K.UAppID";

            
        }
    }
}