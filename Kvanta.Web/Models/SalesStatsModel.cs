using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using asb_ipc;
using MD_Schema;

namespace UDK.Web.Spa2.Model
{
    public class SalesStatsModel
    {
        /*private static ValueTableSchema s_ShopsStatsSchema = new ValueTableSchema(goods_sales_view_TS_.__MD,
            goods_sales_view_TS_.shop, goods_sales_view_TS_.Sale_amount, goods_sales_view_TS_.shop_view_);

        private static ValueTableSchema s_ByGoodsStatsSchema = new ValueTableSchema(goods_sales_view_TS_.__MD,
            goods_sales_view_TS_.goods, goods_sales_view_TS_.sale_quantity);

        private static ValueTableSchema s_ByGoodsDetailedStatsSchema = new ValueTableSchema(goods_sales_view_TS_.__MD,
            goods_sales_view_TS_.goods, goods_sales_view_TS_.price_with_tax, goods_sales_view_TS_.Discont_percent,
            goods_sales_view_TS_.sale_quantity, goods_sales_view_TS_.Sale_amount);

        private static Dictionary<int, Dictionary<int, decimal>> s_SalesByCDepartmentsMap = new Dictionary<int, Dictionary<int, decimal>>();

        public static Dictionary<int, decimal> GetSalesStatsByCDepartment(int pCDepartmentID)
        {
            Dictionary<int, decimal> lResult = null;
            if (s_SalesByCDepartmentsMap.TryGetValue(pCDepartmentID, out lResult))
                return lResult;

            var lDM = AppData.BackGroundDM;
            var lFilter = goods_sales_view_TS_.__MD.NewFilter(goods_sales_view_TS_.shop, pCDepartmentID, lDM);
            var lDate = DateTime.Now;
            lDM.SetSPValue(_SP_Shema_.Date_of_begin, lDate.AddDays(-31).Date);
            lDM.SetSPValue(_SP_Shema_.Date_of_end, lDate);
            var lVT = lDM.ReadValueTable(s_ByGoodsStatsSchema, lFilter);
            if (lVT != null && lVT.CountOfRows > 0)
                lResult = lVT.ToDictionary(x=>x.GetInt(goods_sales_view_TS_.goods), x=>x.GetDecimal(goods_sales_view_TS_.sale_quantity));
            
            s_SalesByCDepartmentsMap[pCDepartmentID] = lResult;
            return lResult;
        }*/
    }
}