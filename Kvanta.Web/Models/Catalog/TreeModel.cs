﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UDK.Web.Spa2.Models.Catalog
{
    public class TreeModel
    {
        public List<TreeItem> nodes { get; set; }
    }

    public class TreeItem {

        public int uuid { get; set; }
        public string name { get; set; }

        public string subTitle { get; set; }

        public List<TreeItem> children { get; set; }

        public bool hasChildren { get; set; }
    }
}