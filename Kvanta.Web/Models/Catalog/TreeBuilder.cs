﻿using asb_ipc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UDK.Web.Spa2.Model;

namespace UDK.Web.Spa2.Models.Catalog
{
    public class TreeBuilder
    {
        public static TreeModel GetTree(VRG_Tree pTreeData)
        {
            var tree = new TreeModel();
            if (pTreeData.RootItemsCount > 0)
            {
                tree.nodes = new List<TreeItem>();
                writeTreeItems(pTreeData.RootItems, tree.nodes);
            }
            return tree;

        }
        private static void writeTreeItems(IEnumerable<VRG_Tree.Item> pItems, List<TreeItem> nodes)
        {
            foreach (var i in pItems)
            {
                var ti = new TreeItem { uuid = i.ID, name = i.Name, subTitle = i.Name };
                ti.hasChildren = i.HasChilds;
                nodes.Add(ti);
                if (ti.hasChildren)
                {
                    ti.children = new List<TreeItem>();
                    writeTreeItems(i.Childs, ti.children);
                }

            }
        }

    }
}