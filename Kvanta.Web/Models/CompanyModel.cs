﻿namespace K7_Web.Models
{
    public class CompanyModel
    {
        public int CompanyGid { get; set; }
        public string Name { get; set; }
        public string FullName { get; set; }
        public int CountryID { get; set; }
        public string RegCode { get; set; }
        public string BusinessDescription { get; set; }
        public int LeaderPersonID { get; set; }
        public int AdminPersonID { get; set; }
    }

    public class RegCompanyModel
    {
        ///companies_TS_
        public string Name { get; set; }
        public string FullName { get; set; }
        public int CountryID { get; set; }
        public string RegCode { get; set; }
        public string BusinessDescription { get; set; }
        public string URLName { get; set; }

        ///OwnershipKinds_TS_
        public string OwnershipKind { get; set; }

        ///Persons_CTS_
        public string LeaderSurName { get; set; }
        public string LeaderName { get; set; }
        public string LeaderMiddleName { get; set; }
        public string LeaderEMail { get; set; }
        public string LeaderPhoneNumber { get; set; }
        public byte LeaderPersonGender { get; set; }
        public int LeaderPersonID { get; set; }

        ///company_persons_TS_
        public string LeaderPositionName { get; set; }
        public string AuthorPositionName { get; set; }
        public int AuthorRole { get; set; }

        //WebSessions_TS_
        public int AuthorWebAccountID { get; set; }
    }
}