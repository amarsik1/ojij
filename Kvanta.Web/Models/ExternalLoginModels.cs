﻿using Microsoft.Build.Framework;

namespace K7_Web.Models
{
    public class RegisterExternalBindingModel
    {
        [Required]
        public string UserName { get; set; }

        [Required]
        public string Provider { get; set; }

        [Required]
        public string ExternalAccessToken { get; set; }

    }

    public class ParsedExternalAccessToken
    {
        public string user_id { get; set; }
        public string app_id { get; set; }
        public string email { get; set; }
    }

    public class GoogleVerifiedTokenData
    {
        public string user_id { get; set; }
        public string audience { get; set; }
        public string email { get; set; }
    }

    public class FacebookVerifiedTokenDataItem
    {
        public string user_id { get; set; }
        public string app_id { get; set; }
        public string email { get; set; }
    }
    public class FacebookExtData
    {
        public string email { get; set; }
    }
    public class FacebookVerifiedTokenData
    {
        public FacebookVerifiedTokenDataItem data { get; set; }
    }
}