﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace UDK.Web.Spa2.Model
{
    public class FilesManager
    {
        private static readonly Lazy<FilesManager> s_lazy = new Lazy<FilesManager>(() => new FilesManager());
        public static FilesManager Instance { get { return s_lazy.Value; } }

        private static readonly object objLock = new object();
        private static readonly object objLock2 = new object();
        private static readonly object locker = new object();
        private static readonly object locker2 = new object();
        private static readonly object locker3 = new object();
        private static readonly object threadStartLocker = new object();
        private static readonly object threadStartLocker2 = new object();
        private static readonly object threadStartLocker3 = new object();
        private static readonly object lockerForMulty = new object();

        private Thread m_thread;
        private Thread m_threadToAddingRequestCode;
        private Thread m_threadToWriteAnswers;

        private readonly string s_working_directory = AppDomain.CurrentDomain.BaseDirectory + "Files";
        private readonly Dictionary<int, HashSet<string>> m_requestCodesToAddDic = new Dictionary<int, HashSet<string>>();
        private readonly Dictionary<int, HashSet<string>> m_templateRequestCodesDic = new Dictionary<int, HashSet<string>>();
        private readonly Dictionary<string, HashSet<int>> s_multyAnswersDic = new Dictionary<string, HashSet<int>>();
        private readonly Dictionary<string, HashSet<string>> m_filesToWriteDic = new Dictionary<string, HashSet<string>>();
        private readonly Dictionary<int, HashSet<string>> m_firstAnswersDic = new Dictionary<int, HashSet<string>>();
        private FilesManager()
        {
            Running = true;
            if (!Directory.Exists(s_working_directory))
                Directory.CreateDirectory(s_working_directory);
        }
        public bool Running { get; set; }


        #region IsAnonymousAndRestrictedByFirstQuestion

        public int CheckRestrictByFirstQuestion(int pTemplateId, int pAuthorId, int pQuestionId, string pAnswer)
        {
            lock (objLock)
            {
                var fpath = s_working_directory + $"/RestrictedByFirstQuestion_{pTemplateId}.dat";
                HashSet<string> hset;
                if (!m_firstAnswersDic.TryGetValue(pTemplateId, out hset))
                {
                    hset = new HashSet<string>();
                    m_firstAnswersDic.Add(pTemplateId, hset);
                    //read file
                    if (File.Exists(fpath))
                    {
                        using (var sr = new StreamReader(fpath))
                        {
                            while (!sr.EndOfStream)
                            {
                                string line = sr.ReadLine();
                                if (string.IsNullOrEmpty(line)) continue;
                                hset.Add(line);
                            }
                        }
                    }
                }
                var key = $"{pAuthorId}:{pQuestionId}:{pAnswer}";
                if (hset.Contains(key))
                    return -1;
                hset.Add(key);
                //write into the file
                if (!writeRestrictByFirstQuestion(pTemplateId, key)) return -2;
            }
            return 0;
        }

        private bool writeRestrictByFirstQuestion(int pTemplateId, string pDataLine)
        {
            if (!Running) return false;
            lock (objLock2)
            {
                HashSet<string> l;
                if (!m_firstAnswersDic.TryGetValue(pTemplateId, out l))
                    m_firstAnswersDic.Add(pTemplateId, l = new HashSet<string>());
                l.Add(pDataLine);
            }
            lock (threadStartLocker2)
            {
                if (m_thread != null && m_thread.IsAlive) return true;
                m_thread = new Thread(AttemptToWrite) { IsBackground = true };
                m_thread.Start();
            }
            return true;
        }

        private void AttemptToWrite()
        {
            var isDataToWrite = m_firstAnswersDic.Count > 0;
            while (Running && isDataToWrite)
            {
                var ldic = new Dictionary<int, HashSet<string>>();
                lock (objLock2)
                {
                    foreach (var p in m_firstAnswersDic)
                        ldic.Add(p.Key, p.Value);
                    m_firstAnswersDic.Clear();
                }
                if (ldic.Any())
                {
                    foreach (var p in ldic)
                    {
                        var fpath = s_working_directory + $"/RestrictedByFirstQuestion_{p.Key}.dat"; 
                        using (var sw = new StreamWriter(fpath, true, Encoding.UTF8))
                        {
                            foreach (var l in p.Value)
                                sw.WriteLine(l);
                        }
                    }
                    Thread.Sleep(1000);
                }
                lock (locker)
                {
                    isDataToWrite = m_firstAnswersDic.Count > 0;
                }
            }
        }

        #endregion

        #region CheckRequestsCodes

        public Dictionary<string, int> CheckRequestCode(HashSet<int> pTemplateIdSet)
        {
            var res = new Dictionary<string, int>();
            foreach (var i in pTemplateIdSet)
            {
                var fpath = s_working_directory + $"/requests_for_template_{i}.txt";
                if (!File.Exists(fpath)) continue;
                using (var sr = new StreamReader(fpath))
                {
                    while (!sr.EndOfStream)
                    {
                        string line = sr.ReadLine();
                        if (string.IsNullOrEmpty(line)) continue;
                        if (res.ContainsKey(line)) continue;
                        res.Add(line, i);
                    }
                }
            }
            return res;
        }
        #endregion


        #region rpr form 1 template id = 114
        public int CheckFileForUsingReqCode(int templId, string code, bool write)
        {
            lock (locker)
            {
                HashSet<string> hset;
                if (!m_templateRequestCodesDic.TryGetValue(templId, out hset))
                {
                    var lhset = new HashSet<string>();
                    if (!m_templateRequestCodesDic.ContainsKey(templId))
                    {
                        var fpath = s_working_directory + $"/form_{templId}_used_codes.dat";
                        if (File.Exists(fpath))
                        {
                            using (var sr = new StreamReader(fpath))
                            {
                                while (!sr.EndOfStream)
                                {
                                    string line = sr.ReadLine();
                                    if (!string.IsNullOrEmpty(line)) lhset.Add(line);
                                }
                            }
                        }
                        m_templateRequestCodesDic.Add(templId, hset = lhset);
                    }
                    else
                        hset = m_templateRequestCodesDic[templId];
                }
                if (hset.Contains(code))
                    return 1;
                if (!write) return 0;
                if (!AddRequestCodeToFile(templId, code))
                    return 2;
                hset.Add(code);
            }
            return 0;
        }
        private void AttemptToWriteCheckInAnswers()
        {
            var isDataToWrite = m_requestCodesToAddDic.Count > 0;
            while (Running && isDataToWrite)
            {
                var ldic = new Dictionary<int, HashSet<string>>();
                lock (locker2)
                {
                    foreach (var p in m_requestCodesToAddDic)
                    {
                        if (p.Value.Count > 0)
                            ldic.Add(p.Key, p.Value);
                    }
                    m_requestCodesToAddDic.Clear();
                }
                if (ldic.Any())
                {
                    foreach (var p in ldic)
                    {
                        var fpath = s_working_directory + $"/form_{p.Key}_used_codes.dat";
                        using (var sw = new StreamWriter(fpath, true, Encoding.UTF8))
                        {
                            foreach (var l in p.Value)
                                sw.WriteLine(l);
                        }
                    }
                    Thread.Sleep(1 * 1000);
                }
                lock (locker2)
                {
                    isDataToWrite = m_requestCodesToAddDic.Count > 0;
                }
            }
        }
        public bool AddRequestCodeToFile(int pTemplateId, string pRequestCode)
        {
            if (!Running || string.IsNullOrEmpty(pRequestCode)) return false;
            lock (locker2)
            {
                HashSet<string> l;
                if (!m_requestCodesToAddDic.TryGetValue(pTemplateId, out l))
                    m_requestCodesToAddDic.Add(pTemplateId, l = new HashSet<string>());
                l.Add(pRequestCode);
            }
            lock (threadStartLocker)
            {
                if (m_threadToAddingRequestCode != null && m_threadToAddingRequestCode.IsAlive) return true;
                m_threadToAddingRequestCode = new Thread(AttemptToWriteCheckInAnswers);
                m_threadToAddingRequestCode.Start();
            }
            return true;
        }
        #endregion

        #region  #region rpr form 2,3 template id = 115, 116
        public HashSet<int> GetAnswersForRequestCode(int pTemplateId, string pCode)
        {
            lock (lockerForMulty)
            {
                HashSet<int> hset;
                if (!s_multyAnswersDic.TryGetValue($"{pTemplateId}_{pCode}", out hset))
                {
                    hset = new HashSet<int>();
                    var fpath = s_working_directory + $"/form_{pTemplateId}_{pCode}_answers.dat";
                    if (File.Exists(fpath))
                    {
                        using (var sr = new StreamReader(fpath))
                        {
                            while (!sr.EndOfStream)
                            {
                                var line = sr.ReadLine();
                                if (string.IsNullOrEmpty(line)) continue;
                                int i;
                                if (int.TryParse(line, out i))
                                    hset.Add(i);
                            }
                        }
                    }
                    s_multyAnswersDic.Add($"{pTemplateId}_{pCode}", hset);
                }
                var res = new HashSet<int>();
                foreach (var i in hset)
                    res.Add(i);
                return res;
            }
        }

        public void SaveAnswers(int pTemplateId, string pCode, HashSet<int> pAnswersSet)
        {
            lock (lockerForMulty)
            {
                var list = new HashSet<string>();
                HashSet<int> hset;
                if (s_multyAnswersDic.TryGetValue($"{pTemplateId}_{pCode}", out hset))
                {
                    foreach (var i in pAnswersSet)
                    {
                        if (hset.Contains(i)) continue;
                        hset.Add(i);
                        list.Add(i.ToString());
                    }
                    if (list.Any())
                        WriteLineTofile($"form_{pTemplateId}_{pCode}_answers.dat", list);
                }
                else
                {
                    s_multyAnswersDic.Add($"{pTemplateId}_{pCode}", pAnswersSet);
                    foreach (var i in pAnswersSet)
                        list.Add(i.ToString());
                    if (list.Any())
                        WriteLineTofile($"form_{pTemplateId}_{pCode}_answers.dat", list);
                }
            }
        }
        private void AttemptToWrite3()
        {
            var isDataToWrite = m_filesToWriteDic.Count > 0;
            while (Running && isDataToWrite)
            {
                var ldic = new Dictionary<string, HashSet<string>>();
                lock (locker3)
                {
                    foreach (var p in m_filesToWriteDic)
                        ldic.Add(p.Key, p.Value);
                    m_filesToWriteDic.Clear();
                }
                if (ldic.Any())
                {
                    foreach (var p in ldic)
                    {
                        var fpath = s_working_directory + "/" + p.Key;
                        using (var sw = new StreamWriter(fpath, true, Encoding.UTF8))
                        {
                            foreach (var l in p.Value)
                                if (!string.IsNullOrEmpty(l))
                                    sw.WriteLine(l);
                        }
                    }
                    Thread.Sleep(1000);
                }
                lock (locker3)
                {
                    isDataToWrite = m_filesToWriteDic.Count > 0;
                }
            }
        }
        public bool WriteLineTofile(string pFilename, HashSet<string> pLines)
        {
            if (!Running) return false;
            lock (locker3)
            {
                HashSet<string> hset;
                if (!m_filesToWriteDic.TryGetValue(pFilename, out hset))
                    m_filesToWriteDic.Add(pFilename, pLines);
                else
                    foreach (var line in pLines)
                        hset.Add(line);
            }
            lock (threadStartLocker3)
            {
                if (m_threadToWriteAnswers != null && m_threadToWriteAnswers.IsAlive) return true;
                m_threadToWriteAnswers = new Thread(AttemptToWrite3);
                m_threadToWriteAnswers.Start();
            }
            return true;
        }
        #endregion

    }
}