﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace K7_Web.Models.Definition
{
    public class AppDefinition
    {
        public List<AppRoute> Routes { get; set; }

        public List<AppMenu> Menus { get; set; }

        public static AppDefinition Create(string data)
        {
            return JsonConvert.DeserializeObject<AppDefinition>(data);
        }
    }

    public class AppRoute
    {
        public string Path { get; set; }

        public string Component { get; set; }

        public List<AppRoute> Children { get; set; }

        public string LoadChildren { get; set; }
    }

    public class AppMenu
    {
        public string Text { get; set; }

        public string Link { get; set; }

        public string Icon { get; set; }

        public List<AppMenu> Submenu { get; set; }

        public bool Heading { get; set; }
    }
}