﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace K7_Web.Models
{
    public class OperationResult
    {
        public OperationResult()
        {
            Status = EnumOperationStatus.OK;
        }

        public OperationResult(EnumOperationStatus status)
        {
            Status = status;
        }

        public OperationResult(EnumOperationStatus status, string msg)
        {
            Status = status;
            Message = msg;
        }
        public EnumOperationStatus Status { get; set; }
        public string Message { get; set; }
        public bool IsOk { get { return Status == EnumOperationStatus.OK; } }
        public string Url { get; set; }
    }
    public class OperationResult<T> : OperationResult
    {
        public OperationResult()
        {
        }

        public OperationResult(EnumOperationStatus status) : base(status)
        {
        }

        public OperationResult(EnumOperationStatus status, string msg) : base(status, msg)
        {
        }

        public OperationResult(T res)
        {
            Status = EnumOperationStatus.OK;
            Result = res;
        }

        public T Result { get; set; }
    }

    public enum EnumOperationStatus
    {
        OK = 1,
        NotAuthenticated = 401,
        NoPermissions = 403,
        NotFound = 404,
        BadRequest = 400,
        InternalError = 500,
        NoData = 203
    }


}
