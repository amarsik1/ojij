﻿using System;
using asb_ipc;
using asb_ipc.Notifications;
using MD_Schema;

namespace K7_Web.Models
{
    public class WorkingMessageManager
    {

        private IDataManager m_dm;
        private ValueTable m_VTForWriting;
        public ValueTableRow NewMssgWriteVR { get; private set; }

        public WorkingMessageManager(IDataManager pDM)
        {
            m_dm = pDM;
            m_VTForWriting = new ValueTable(NotificationsModel.MessagesSchema);
        }

        public void AddMessage(int pObjTID, int pObjGid, int pAuthorGid, int pRecipientGid, string pMessage)
        {
            m_VTForWriting.Clear();
            NewMssgWriteVR = m_VTForWriting.AddRow();
            NewMssgWriteVR[PersonsMessages_CTS_.DataObject_tid] = pObjTID;
            NewMssgWriteVR[PersonsMessages_CTS_.DataObject] = pObjGid;
            NewMssgWriteVR[PersonsMessages_CTS_.Author] = pAuthorGid;
            NewMssgWriteVR[PersonsMessages_CTS_.Recipient] = pRecipientGid;
            NewMssgWriteVR[PersonsMessages_CTS_.MessageText] = pMessage;
            NewMssgWriteVR[PersonsMessages_CTS_.reg_date] = DateTime.Now;
            NewMssgWriteVR[PersonsMessages_CTS_.MessageType] = 0;
            NewMssgWriteVR[PersonsMessages_CTS_.State] = (int)NotificationStates.Active;
            NewMssgWriteVR[PersonsMessages_CTS_.gid] = m_dm.getNewObjectGid(PersonsMessages_CTS_.__MD);
        }
        public void SaveMessage()
        {
            m_dm.ProcessCustomCommand((int)SystemCustomCommands.WRITE_WORKING_MESSAGE, m_VTForWriting);
        }
        public void WriteNewMessage(ValueTableRow pNewMssgReadVR)
        {
            if (pNewMssgReadVR == null)
                return;
            var lData = new ValueTable(NotificationsModel.MessagesSchema);
            pNewMssgReadVR.CopyTo(lData.AddRow());

            var lVT = m_dm.ProcessCustomCommand((int)SystemCustomCommands.WRITE_WORKING_MESSAGE, lData);
            if (lData != null && lData.CountOfRows > 0 && pNewMssgReadVR.Schema.IndexOf(PersonsMessages_CTS_.gid) >= 0)
                pNewMssgReadVR[PersonsMessages_CTS_.gid] = lVT[0][PersonsMessages_CTS_.gid];
        }
    }
}