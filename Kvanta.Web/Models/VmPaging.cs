﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace K7_Web.Models
{
    public class VmPaging<T>
    {
        private int m_pagesCount = -1;
        public int PagesCount
        {
            get { return m_pagesCount; }
        }
        public int PageNum { get; set; }
        private int m_pageSize;
        public int PageSize { get { return m_pageSize; } }

        public VmPaging(int pageSize = 50)
        {
            m_pageSize = pageSize > 50 ? 50 : pageSize;
        }

        public void Reset(int pageSize = 0)
        {
            m_pagesCount = -1;
            PageNum = 0;
            if (pageSize > 0)
                m_pageSize = pageSize > 50 ? 50 : pageSize;
        }
        public List<T> GetData(List<T> pItems)
        {
            int tc = pItems.Count;
            int pc = tc / m_pageSize;
            m_pagesCount = pc * PageSize < tc ? pc + 1 : pc;
            if ((PageNum == 0 && m_pagesCount > 0) || PageNum > m_pagesCount)
                PageNum = 1;
            return pItems.Skip((PageNum - 1) * m_pageSize).Take(m_pageSize).ToList();
        }

        public string RenderPaginator()
        {
            StringBuilder sb = new StringBuilder();
            if (PagesCount > 1)
            {
                sb.Append("<ul class=\"pull - left pagerUL\"> ");
                if (PageNum > 1)
                    sb.Append("<li><a class=\"prev\" href=\"#\" onclick=\"hbl('pageNumber'," + (PageNum - 1) + ");return false;\">НАЗАД</a></li>");
                if (PageNum > 2 && PagesCount > 3)
                    sb.Append("<li><a class=\"current\" href=\"#\" onclick=\"hbl('pageNumber', 1); return false;\">1</a></li>");
                if (PageNum > 3 && PagesCount > 4)
                    sb.Append("<li><p>...</p></li>");
                if (PageNum == PagesCount && PagesCount > 2)
                    sb.Append("<li><a class=\"current\" href=\"#\" onclick=\"hbl('pageNumber'," + (PageNum - 2) + ");return false;\">" + (PageNum - 2) + "</a></li>");
                if (PagesCount > (PageNum - 1) && PageNum > 1)
                    sb.Append("<li><a class=\"current\" href=\"#\" onclick=\"hbl('pageNumber', " + (PageNum - 1) + ");return false;\">" + (PageNum - 1) + "</a></li>");

                sb.Append("<li><a href=\"#\" onclick=\"return false;\">" + PageNum + "</a></li>");

                if (PagesCount > (PageNum + 1))
                    sb.Append("<li><a class=\"current\" href=\"#\" onclick=\"hbl('pageNumber'," + (PageNum + 1) + ");return false;\">" + (PageNum + 1) + "</a></li>");
                if (PageNum == 1 && PagesCount > 3)
                    sb.Append("<li><a class=\"current\" href=\"#\" onclick=\"hbl('pageNumber',3); return false;\">3</a></li>");
                if (PagesCount > (PageNum + 2) && PagesCount > 4)
                    sb.Append("<li><p>...</p></li>");
                if (PagesCount > PageNum)
                {
                    sb.Append("<li><a class=\"current\" href=\"#\" onclick=\"hbl('pageNumber'," + PagesCount + ");return false;\">" + PagesCount + "</a></li>");
                    sb.Append("<li><a class=\"next\" href=\"#\" onclick=\"hbl('pageNumber', " + (PageNum + 1) + ");return false;\">ВПЕРЕД</a></li>");
                }
                sb.Append("</ul>");
            }
            return sb.ToString();
        }

    }
}