using System;
using System.Collections.Generic;
using System.Linq;
using AS_MetaData;
using asb_ipc;
using System.IO;

namespace K7_Web.Models
{
    public class _TableVM
    {
        public const string SELECTED_CMD = "Selected";

        public mdTableView ViewMD { get; private set; }
        private ValueTableSchema m_VTSchema;
        public string[] GetFieldsCaptions()
        {
            if (m_Captions == null && m_VisibleFields != null)
            {
                m_Captions = new string[m_VisibleFields.Length];
                for (int i = 0; i < m_VisibleFields.Length; i++)
                {
                    var f = m_VisibleFields[i];
                    if (m_FieldCaptions != null && m_FieldCaptions.ContainsKey(f))
                        m_Captions[i] = m_FieldCaptions[f];
                    else if (m_FieldsMap != null && m_FieldsMap.ContainsKey(f))
                        m_Captions[i] = m_FieldsMap[f].ShortName;
                    else
                        m_Captions[i] = f.ShortName;
                }
            }
            return m_Captions;
        }
        private mdTableField[] m_VisibleFields;
        private string[] m_Captions;
        public IEnumerable<mdTableField> VisibleFields { get { return m_VisibleFields; } }
        public mdTableField KeyField { get; private set; }
        public string OnSelectCmdID { get; set; }
        public Func<ValueTableRow, string> RowClassFunc { get; set; }
        private Dictionary<mdTableField, mdTableViewField> m_FieldsMap = null;
        private Dictionary<mdTableField, string> m_FieldCaptions = null;
        public Func<ValueTableRow, string> KeyGetter { get; private set; }

        public _TableVM(mdTableView pView)
        {
            ViewMD = pView;
            m_VTSchema = pView.Schema;
            KeyField = m_VTSchema.FirstOrDefault(x => x.IsPrimaryKeyField);

            m_VisibleFields = ViewMD.Fields.Where(f => f.TableField != null && f.ViewOrdinal > 0).OrderBy(v => v.ViewOrdinal).Select(x => x.TableField).ToArray();
            m_FieldsMap = ViewMD.Fields.ToDictionary(x => x.TableField);
            m_Captions = m_VisibleFields.Select(f => m_FieldsMap[f].ShortName).ToArray();

            RowClassFunc = null;
            OnSelectCmdID = SELECTED_CMD;
        }
        public _TableVM(ValueTableSchema pSchema, params mdTableField[] pVisibleFields)
        {
            m_VTSchema = pSchema;
            OnSelectCmdID = SELECTED_CMD;
            KeyField = m_VTSchema.FirstOrDefault(x => x.IsPrimaryKeyField);
            SetVisibleFields(pVisibleFields);
        }
        public _TableVM(ValueTableSchema pSchema, Func<ValueTableRow, string> pKeyGetter, params mdTableField[] pVisibleFields)
            :this(pSchema, pVisibleFields)
        {
            KeyGetter = pKeyGetter;
        }

        public void SetVisibleFields(params mdTableField[] pVisibleFields)
        {
            if (pVisibleFields != null && pVisibleFields.Length > 0)
                m_VisibleFields = pVisibleFields;
            else if (ViewMD != null)
                m_VisibleFields = ViewMD.Fields.Where(f => f.TableField != null && f.ViewOrdinal > 0).OrderBy(v => v.ViewOrdinal).Select(x => x.TableField).ToArray();
            else if (m_VTSchema != null)
                m_VisibleFields = m_VTSchema.Where(f => f.ViewOrdering > 0).OrderBy(v => v.ViewOrdering).ToArray();
        }

        public void SetFieldCaption(mdTableField pField, string pCaption)
        {
            if (pField == null)
                return;
            if (m_VTSchema.IndexOf(pField) < 0)
                throw new Exception(string.Format("���� {0} �� �������� � ����!", pField));

            if (m_FieldCaptions == null)
                m_FieldCaptions = new Dictionary<mdTableField, string>();
            if (pCaption == null)
            {
                if (m_FieldCaptions.ContainsKey(pField))
                    m_FieldCaptions.Remove(pField);
            }
            else
                m_FieldCaptions[pField] = pCaption;
            m_Captions = null;
        }

    }



    public class _TreeVM : VRG_Tree.Model
    {
        public string IdPrefix { get; set; }
        public string OnSelectCmdID { get; set; }
        public bool SkipWeightless { get; set; }
        public string SelfWeightLabel { get; set; }
        public Func<VRG_Tree.Item, string> ItemClassProvider { get; set; }

        public _TreeVM(mdTable pMDTable)
            : base(pMDTable)
        {
            OnSelectCmdID = _TableVM.SELECTED_CMD;
            IdPrefix = null;
            SelfWeightLabel = null;
        }
        public _TreeVM(mdTable pMDTable, mdTableField pKeyField, mdTableField pParentReferenceField)
            : base(pMDTable, pKeyField, pParentReferenceField)
        {
            OnSelectCmdID = _TableVM.SELECTED_CMD;
            IdPrefix = null;
            SelfWeightLabel = null;
        }
    }

    public class _ModelBase
    {
        public bool FilterIsPossible { get; set; }
        public virtual string FilterString { get; set; }
        public bool HasFilter { get { return !string.IsNullOrEmpty(FilterString); } }
        public _ModelBase()
        {
            FilterIsPossible = false;
        }
    }

    public static class ModelRooot
    {
        public static string S2(this decimal pVal)
        {
            return pVal.ToString("#,##0.00");
        }
        public static string S0S1(this decimal pVal)
        {
            if (pVal == 0m)
                return string.Empty;

            return (pVal / 1000).ToString("#,##0.0");
        }
        public static string S3(this decimal pVal)
        {
            return pVal.ToString("#0.0##", System.Globalization.CultureInfo.InvariantCulture);
        }
        public static string S03(this decimal pVal)
        {
            if (pVal == 0m)
                return string.Empty;
            return pVal.ToString("#0.###", System.Globalization.CultureInfo.InvariantCulture);
        }
        public static string SQ(this decimal pVal)
        {
            if (pVal == 0m)
                return "-";
            return pVal.ToString("#,##0.###");
        }
        public static string S01P(this decimal pVal)
        {
            if (pVal == 0m)
                return string.Empty;
            return pVal.ToString("#0.0", System.Globalization.CultureInfo.InvariantCulture) + "%";
        }
        public static string S1(this decimal pVal)
        {
            return pVal.ToString("#0.0", System.Globalization.CultureInfo.InvariantCulture);
        }


        // This method is REQUIRED if the template contains any Razor helpers, but you may choose to implement it differently
        //
        /// <summary>Writes an object value to the TextWriter, HTML escaping it if necessary.</summary>
        /// <param name="writer">The TextWriter to which to write the value.</param>
        /// <param name="value">The value.</param>
        /// <remarks>The value may be a Action<System.IO.TextWriter>, as returned by Razor helpers.</remarks>
        private static void WriteTo(System.IO.TextWriter writer, object value)
        {
            if (value == null)
                return;

            var write = value as Action<System.IO.TextWriter>;
            if (write != null)
            {
                write(writer);
                return;
            }

            //NOTE: a more sophisticated implementation would write safe and pre-escaped values directly to the
            //instead of double-escaping. See System.Web.IHtmlString in ASP.NET 4.0 for an example of this.
            writer.Write(System.Net.WebUtility.HtmlEncode(value.ToString()));
        }
        private static void WriteLiteralTo(System.IO.TextWriter writer, string value)
        {
            writer.Write(value);
        }
        private static void _WriteTable(TextWriter w, _TableVM pVM, IEnumerable<ValueTableRow> pRows, string pClass, string pIdAttr)
        {
            if (pVM == null)
                return;
            WriteLiteralTo(w, "<table");
            if (!string.IsNullOrEmpty(pIdAttr))
                WriteLiteralTo(w, " id='" + pIdAttr + "'");
            if (!string.IsNullOrEmpty(pClass))
                WriteLiteralTo(w, " class='" + pClass + "'");

            WriteLiteralTo(w, ">");
            WriteLiteralTo(w, "<thead><tr>");
            foreach (var vf in pVM.GetFieldsCaptions())
            {
                WriteLiteralTo(w, "<td>");
                WriteTo(w, vf);
                WriteLiteralTo(w, "</td>");
            }
            WriteLiteralTo(w, "</tr></thead>");
            WriteLiteralTo(w, "<tbody>");
            if (pRows != null)
                foreach (var r in pRows)
                {
                    if (pVM.KeyGetter != null && !string.IsNullOrEmpty(pVM.OnSelectCmdID))
                    {
                        WriteLiteralTo(w, "<tr onclick=\"hbl('");
                        WriteLiteralTo(w, pVM.OnSelectCmdID);
                        WriteLiteralTo(w, "','");
                        WriteTo(w, pVM.KeyGetter(r));
                        WriteLiteralTo(w, "')\"");
                    }
                    else if (pVM.KeyField != null && !string.IsNullOrEmpty(pVM.OnSelectCmdID))
                    {
                        WriteLiteralTo(w, "<tr onclick=\"hbl('");
                        WriteLiteralTo(w, pVM.OnSelectCmdID);
                        WriteLiteralTo(w, "','");
                        WriteTo(w, r[pVM.KeyField]);
                        WriteLiteralTo(w, "')\"");
                    }
                    else
                        WriteLiteralTo(w, "<tr");
                    if (pVM.RowClassFunc != null)
                    {
                        var c = pVM.RowClassFunc(r);
                        if (!string.IsNullOrEmpty(c))
                        {
                            w.Write(" class=\"");
                            w.Write(c);
                            w.Write("\"");
                        }
                    }
                    w.Write(">");
                    foreach (var vf in pVM.VisibleFields)
                    {
                        WriteLiteralTo(w, "<td>");
                        WriteTo(w, r.GetFormatedString(vf));
                        WriteLiteralTo(w, "</td>");
                    }
                    WriteLiteralTo(w, "</tr>");
                }
            WriteLiteralTo(w, "</tbody></table>");
        }

        private static void _WriteTreeItems(TextWriter w, _TreeVM pVM, IEnumerable<VRG_Tree.Item> pItems)
        {
            if (pItems == null)
                return;
            Func<VRG_Tree.Item, string> lIDGetter = null;
            if (string.IsNullOrEmpty(pVM.IdPrefix))
                lIDGetter = i => i.ID.ToString();
            else
                lIDGetter = i => pVM.IdPrefix + i.ID.ToString();
            WriteLiteralTo(w, "<ul>");
            foreach (var i in pItems)
            {
                if (pVM.SkipWeightless && i.TotalWeight == 0)
                    continue;

                string lID = lIDGetter(i);
                WriteLiteralTo(w, "<li><input type=\"checkbox\" id=\"");
                WriteLiteralTo(w, lID);
                WriteLiteralTo(w, i.IsOpen ? "\" checked />" : "\"/>");
                if (i.HasChilds)
                {
                    WriteLiteralTo(w, "<label for=\"");
                    WriteLiteralTo(w, lID);
                    WriteLiteralTo(w, "\" onclick=\"hba('TGL',");
                    WriteLiteralTo(w, i.ID.ToString());
                    WriteLiteralTo(w, ")\">");
                    WriteTo(w, i.VRow.GetString(pVM.RepresentationField));
                    WriteLiteralTo(w, "</label>");
                    if (i.SelfWeight > 0 && !string.IsNullOrEmpty(pVM.SelfWeightLabel))
                    {
                        WriteLiteralTo(w, "<a href=\"");
                        if (!string.IsNullOrEmpty(pVM.OnSelectCmdID))
                        {
                            WriteLiteralTo(w, pVM.OnSelectCmdID);
                            WriteLiteralTo(w, "?");
                            WriteLiteralTo(w, i.ID.ToString());
                        }
                        else
                            WriteLiteralTo(w, "#");
                        WriteLiteralTo(w, "\">");
                        WriteTo(w, pVM.SelfWeightLabel);
                        WriteLiteralTo(w, " (");
                        WriteLiteralTo(w, i.SelfWeight.ToString());
                        WriteLiteralTo(w, ")</a>");
                    }
                    _WriteTreeItems(w, pVM, i.Childs);
                }
                else
                {
                    WriteLiteralTo(w, "<a href=\"");
                    if (!string.IsNullOrEmpty(pVM.OnSelectCmdID))
                    {
                        WriteLiteralTo(w, pVM.OnSelectCmdID);
                        WriteLiteralTo(w, "/");
                        WriteLiteralTo(w, i.ID.ToString());
                    }
                    else
                        WriteLiteralTo(w, "#");
                    WriteLiteralTo(w, "\">");
                    WriteTo(w, i.VRow.GetString(pVM.RepresentationField));
                    WriteLiteralTo(w, " (");
                    WriteLiteralTo(w, i.SelfWeight.ToString());
                    WriteLiteralTo(w, ")</a>");
                }
                WriteLiteralTo(w, "</li>\n");
            }
            WriteLiteralTo(w, "</ul>");
        }
        private static void _WriteTree(TextWriter w, _TreeVM pVM, VRG_Tree pTreeData, string pClass, string pIdAttr)
        {
            if (pVM == null)
                return;
            WriteLiteralTo(w, "<div");
            if (!string.IsNullOrEmpty(pIdAttr))
                WriteLiteralTo(w, " id='" + pIdAttr + "'");
            if (!string.IsNullOrEmpty(pClass))
                WriteLiteralTo(w, " class='" + pClass + "'");

            WriteLiteralTo(w, ">");
            if (pTreeData.RootItemsCount > 0)
            {
                _WriteTreeItems(w, pVM, pTreeData.RootItems);
            }
            WriteLiteralTo(w, "</div>");
        }

        //public static System.Web.Mvc.MvcHtmlString WriteTable(this System.Web.Mvc.HtmlHelper pHelper, _TableVM pVM, IEnumerable<ValueTableRow> pRows, string pClass, string pIdAttr)
        //{
        //    using (var tw = new StringWriter())
        //    {
        //        _WriteTable(tw, pVM, pRows, pClass, pIdAttr);
        //        return System.Web.Mvc.MvcHtmlString.Create(tw.ToString());
        //    }
        //}
        //public static System.Web.Mvc.MvcHtmlString WriteTree(this System.Web.Mvc.HtmlHelper pHelper, _TreeVM pVM, VRG_Tree pTreeData, string pClass = null, string pIdAttr = null)
        //{
        //    using (var tw = new StringWriter())
        //    {
        //        _WriteTree(tw, pVM, pTreeData, pClass, pIdAttr);
        //        return System.Web.Mvc.MvcHtmlString.Create(tw.ToString());
        //    }
        //}

        //public static System.Web.Mvc.MvcHtmlString WCL(this System.Web.Mvc.HtmlHelper pHelper, bool pCondition, string pIfTrueStr, string pIfFalceStr = null)
        //{
        //    var lRes = string.Empty;
        //    if (pCondition)
        //    {
        //        if (!string.IsNullOrEmpty(pIfTrueStr))
        //            lRes = pIfTrueStr;
        //    }
        //    else if (!string.IsNullOrEmpty(pIfFalceStr))
        //        lRes = pIfFalceStr;

        //    return System.Web.Mvc.MvcHtmlString.Create(lRes);
        //}

    }

    /// <summary>
    /// ��������� ����� ������� �������� �����
    /// </summary>
    public interface ItemDetailsModel
    {
        #region Properties
        /// <summary>
        /// ����� ����� - �������� ��������� (���� ���������� ����� �����, 
        /// ������� ���� �������� ��� ���������� �������, �� ������� ���������� �������� ���� ��������� ���������� ��'����),
        /// ������� ���� ���������� � ��������
        /// </summary>
        string ModelName { get; }

        /// <summary>
        /// ������� ���������, ��� ������ �������� ����� ����� ��'���� � ��� ����� (������� �������, ��� ������������)
        /// </summary>
        mdTable BaseTable { get; }

        /// <summary>
        /// ������������� ��'���� �����
        /// </summary>
        int ItemID { get; }

        /// <summary>
        /// ������ ��������� ����������� ����� 
        /// </summary>
        bool IsEditable { get; }

        /// <summary>
        /// ������ �������� ������������ � �� �����
        /// </summary>
        bool HasUnsavedToDBData { get; }
        #endregion

        #region Methodes
        /// <summary>
        /// ������� �� ����� �����, ����������� ���� ����� ���� ������
        /// </summary>
        ValueTableSchema[] GetAllDataSchemas();

        /// <summary>
        /// ������� ������� � ������ ����� �����
        /// </summary>
        ValueTable GetData(ValueTableSchema pSchema);

        /// <summary>
        /// ����� ���� � ���� �����
        /// </summary>
        void ReadData(IDataManager pDM);

        /// <summary>
        /// �������� ���������� �����
        /// </summary>
        /// <returns>true ���� ���� �������� � �� ����� �������� � �.�., false ���� � �������� � ������ � �������� �� � �.�. �� �����.</returns>
        bool Validate();

        /// <summary>
        /// ������ ���������� � �.�.
        /// </summary>
        bool SaveToDB();
        #endregion
    }

    /// <summary>
    /// ������ �������� ����� (������, ������ �� ��������)
    /// </summary>
    public interface DataCollectionModel : IAtlasSessionParameterProvider
    {
        #region Properties
        /// <summary>
        /// ����� ����� - �������� ��������� (���� ���������� ����� �����, 
        /// ������� ���� �������� ��� ���������� �������, �� ������� ���������� �������� ���� ��������� ���������� ��'����),
        /// ������� ���� ���������� � ��������
        /// </summary>        
        string ModelName { get; }

        /// <summary>
        /// ������� ���������, ��� ������ ������� ���� �������� � ��� �����
        /// </summary>
        mdTable MainTable { get; }
        /// <summary>
        /// ������� �� ����������� � ����� ����� ��������� ������
        /// </summary>
        mdSessionParameter[] UsedSessionParameters { get; }
        /// <summary>
        /// ������ ��������� ������� �� ������ �� ���������� ��������
        /// </summary>
        bool FilterByStringIsPossible { get; }
        /// <summary>
        /// ����� ������� �� ������ � ��������.
        /// </summary>
        string FilterString { get; }

        /// <summary>
        /// ������ �������� ������� �� ������
        /// </summary>
        bool HasPeriod { get; }
        /// <summary>
        /// ���� ������� ������
        /// </summary>
        DateTime StartDate { get; }
        /// <summary>
        /// ���� ���������� ������
        /// </summary>
        DateTime EndDate { get; }
        #endregion Properties

        #region Methodes
        /// <summary>
        /// ������� �� ����� �����, ����������� ���� ����� ���� ������
        /// </summary>
        ValueTableSchema[] GetAllDataSchemas();
        /// <summary>
        /// ������� ������� � ������ ����� �����
        /// </summary>
        ValueTable GetData(ValueTableSchema pSchema);
        /// <summary>
        /// ������� ������ ������ ��������� ������� �� ������������ ��������
        /// </summary>
        Dictionary<string, mdDataType> GetAllParameters();
        /// <summary>
        /// ������� ������� �������� ��������� �����
        /// </summary>
        Dictionary<string, DataValue> GetParameterValues();
        /// <summary>
        /// ���������� �������� ��������� �� �����
        /// </summary>
        void SetParameterValue(string pParamName, DataValue pValue);
        /// <summary>
        /// ������� �������� ��������� �� �����
        /// </summary>
        DataValue GetParameterValue(string pParamName);
        #endregion
    }
}