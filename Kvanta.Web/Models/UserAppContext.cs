﻿using AS_MetaData;
using asb_cdm;
using asb_ipc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace K7_Web.Models
{
    public class UserAppContext: IDisposable
    {
        private AtlasSessionContext m_Session;
        private IDataManager dataManager;

        public string Key 
        { 
            get; 
            private set; 
        }

        public UserAppData  MetaData { get; private set; }


        public UserAppContext(AtlasSessionContext lSession) 
        {
            m_Session = lSession;
            Key = newKey();
            dataManager = lSession.GetDM();

            Dictionary<string, mdTable> grids = new Dictionary<string, mdTable>();
            MetaData = initMetadata(grids);
            MetaData.AppId = Key;
        }


        private UserAppData initMetadata(Dictionary<string, mdTable> grids)
        {
            
            var result = new UserAppData();
            result.UI = new UI();

            var ui = AS_MetaData.Metadata.UserInterfaces[m_Session.LoginInfo.MainInterfaceTypeGid];
            if (ui == null) {
                ui = AS_MetaData.Metadata.UserInterfaces.First();
            }
            var roots = ui.Elements.Where(x => x.ElementType == AS_MetaData.InterfaceElementTypes.SubMenu && x.ParentElement == null).OrderBy(x => x.MenuOrdering);
           
            foreach (var root in roots)
            {
                var mi = new MenuItem {Name = root.Name, Gid = root.Gid, OrderCode = root.MenuOrdering};
                result.UI.Menu.Add(mi);
                appendChildren(ui, root, mi, grids);   
            }
            return result;
        }

        private void appendChildren(AS_MetaData.mdUserInterface ui, mdInterfaceElement root, MenuItem pmi, Dictionary<string, mdTable> grids) 
        {
            var children = ui.Elements.Where(x => x.ParentElement != null && x.ParentElement == root).OrderBy(x => x.MenuOrdering);
            foreach (var c in children)
            {
                var mi = new MenuItem { Name = c.Name, Gid = c.Gid, OrderCode = c.MenuOrdering };
                pmi.Children.Add(mi);
                if (c.Table != null) {
                    mi.Page = buildReportPageInfo(c.Table);
                    grids[c.Table.Name] = c.Table;
                }
                appendChildren(ui, c, mi, grids);
            }
        }

        private GridInfo buildReportPageInfo(mdTable tbl) 
        {
            var result = new ReportInfo { Name = tbl.Name, Type="Table", Title = tbl.UserName };
            var sgColumns = tbl.Fields.Where(x => x.ViewOrdering > 0).OrderBy(x => x.ViewOrdering);
            foreach (var f in sgColumns)
            {
                result.Columns.Add(new ReportColumn {Field = f.Name, Title = f.UserName, Width = f.ColumnWidth });
            }
            return result;
        }

        private string newKey() {
            return Guid.NewGuid().ToString();
        }

        public void Dispose()
        {
            if (dataManager != null) {
                dataManager.Dispose();
                dataManager = null;
            }
        }
    }
}
