﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace UDK.Web.Spa2.Model
{
    public class ErrorViewModel
    {
        public int Id { get; set; }
        public string Message { get; set; }
    }
}