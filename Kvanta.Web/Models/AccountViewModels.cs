﻿namespace K7_Web.Models
{
    // Models returned by AccountController actions.

    public class ExternalLoginViewModel
    {
        public string Name { get; set; }

        public string Url { get; set; }

        public string State { get; set; }
    }
    public class UserInfoViewModel
    {
        public bool HasRegistered { get; set; }
        public string LoginProvider { get; set; }
        public string ProviderKey { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string DisplayName { get; set; }
        public string Email { get; set; }
        public string Gender { get; set; }
        public string Birthday { get; set; }
        public string RedirectURL { get; set; }
        public int WebAccountId { get; set; }
    }

    public class CachedUser
    {
        public int Id { get; set; }
        public string Provider { get; set; }
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool IsBot { get; set; }
    }
}