﻿using ADE.Core;
using K7P_sp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace K7_Web.Controllers.Mvc
{
    public class MvcControllerBase : Controller
    {
        //protected EnumUserRoleType userRoleType = EnumUserRoleType.Guest;

        //protected EnumUserGroupType userGroupType = EnumUserGroupType.Guest;

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //var identity = User?.Identity as ClaimsIdentity;
            //if (identity != null && identity.Claims.Count() > 0)
            //{
            //    //TODO: Get UserRole and Group From identity
            //    userRoleType = EnumUserRoleType.User;
            //    userGroupType = EnumUserGroupType.Student;
            //}
            //ViewData["UserRoleType"] = userRoleType;
            //ViewData["UserGroupType"] = userGroupType;
            ViewData["Server"] = ConfigurationManager.AppSettings["FeUrl"] ?? "";

            base.OnActionExecuting(filterContext);
        }

    }
}