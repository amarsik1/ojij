﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using appcore.DataProcessing;
using asb_ipc.GraphManagement;
using K7_Web.AtlasAPI;
using K7_Web.Models;
using K7_Web.Models.User;
using MD_Schema;
using asb_ipc.DataManagment;
using System.Web;
using System.IO;

namespace K7_Web.Controllers
{
    public class GPollController : ControllerBase
    {
        private static GModuleBase s_PollModule = null;
        public static GModuleBase PollModule
        {
            get
            {
                if (s_PollModule == null)
                {
                    var lGC = AtlasContextCore.GMController;
                    if (lGC != null)
                    {
                        var lID = lGC.GetModuleID("Poll");
                        if (lID > 0)
                        {
                            s_PollModule = lGC.GetNewModule(lID, WebGraphController.Instance);
                        }
                        else
                            return null;
                    }
                    else
                        return null;
                }
                return s_PollModule;
            }
        }

        private class WebPollSessionContext: WebSessionContext
        {
            public string SessionID { get; }
            public bool NeedSetCookie { get; set; } = false;
            public WebPollSessionContext(WebGraphController pController, K7UserSession pUserSession, string pSessionID)
                :base(pController, pUserSession)
            {
                SessionID = pSessionID;
                
            }

            protected override AtlasSessionContext restoreAtlasSession()
            {
                if (m_UserSessionKey?.SessionToken != null)
                    return base.restoreAtlasSession();
                else
                    return AtlasContextCore.Instance?.GetBgSession();
            }
            protected override Func<DataGraph.DataNode, DataContainer, DataContainer> getActionHandler(string pActionName)
            {
                if (pActionName == "RegisterTemplates")
                    return registerTemplates;
                else if (pActionName == "SetPLID")
                    return setPLID;
                else if (pActionName == "SetQTemplateID")
                    return setQTemplateID;

                return base.getActionHandler(pActionName);
            }

            private DataContainer setPLID(DataGraph.DataNode pDataNode, DataContainer pState)
            {
                if (pState == null)
                    return DataContainer.NULL;
                var lPTid = pState.GetInt();
                if (lPTid<=0)
                    return DataContainer.NULL;
                string lParamName = "PL-" + lPTid;
                var lCurID = GetParam(lParamName);
                if (!string.IsNullOrEmpty(lCurID))
                {
                    var lG = GetDataByName("SG-" + lCurID + "@QAW_Schema");
                    if (lG != null && lG.Root.GetValue("State") == IntContainer.Six)
                        lCurID = null;
                }
                if (string.IsNullOrEmpty(lCurID))
                {
                    lCurID = generateNewPLID(lPTid);
                    SetParam(lParamName, lCurID);
                }
                return DataContainer.Wrap(lCurID);
            }
            private DataContainer setQTemplateID(DataGraph.DataNode pDataNode, DataContainer pState)
            {
                if (pState == null)
                    return DataContainer.NULL;
                var lPLid = pState.GetString();
                if (!string.IsNullOrEmpty(lPLid) && lPLid.StartsWith("PL"))
                {
                    var lEndPos = lPLid.IndexOf('*');
                    uint lID = 0;
                    if (lEndPos>0 && appcore.AppCore.TryDecodeUint(lPLid.Substring(2, lEndPos-2), out lID))
                    {
                        var lVal = IntContainer.Wrap((int)lID);
                        if (pDataNode != null && pDataNode.NodeType.ValueBaseType == BaseTypesEnum.Int32)
                        {

                            pDataNode.SetValue(lVal);
                        }
                        return lVal;
                    }
                }
                return DataContainer.NULL;
            }

            private string makeReadOnly(string pBaseTemplate, string pTag)
            {
                
                var i = pBaseTemplate.IndexOf(pTag);
                if (i < 0)
                    return pBaseTemplate;

                var lSB = new StringBuilder();
                var lPos = 0;
                while(i>=0)
                {
                    if (i > lPos)
                        lSB.Append(pBaseTemplate.Substring(lPos, i - lPos));
                    lSB.Append(pTag);
                    
                    lPos = i + pTag.Length;
                    i = pBaseTemplate.IndexOf(">", lPos);
                    if (i<0)
                    {
                        break;
                    }
                    var j = pBaseTemplate.IndexOf("readonly", lPos);
                    if (j < 0 || j > i)
                        lSB.Append(" readonly='readonly' ");
                    lSB.Append(pBaseTemplate.Substring(lPos, i + 1 - lPos));
                    lPos = i + 1;
                    i = pBaseTemplate.IndexOf(pTag, lPos);
                }
                if (lPos<pBaseTemplate.Length)
                    lSB.Append(pBaseTemplate.Substring(lPos));

                return lSB.ToString();
            }

            private string makeDesidnModeTemplate(string pBaseTemplate)
            {
                var lRes = makeReadOnly(pBaseTemplate, "<input");
                lRes = makeReadOnly(lRes, "<select");
                lRes = makeReadOnly(lRes, "<textarea");
                var i = lRes.IndexOf("{{QText}}");
                if (i >= 0)
                    lRes = lRes.Substring(0, i) + "<input type='text' name='QText' value='{{QText}}' placeholder='Текст питання' alt='Текст питання'>" + lRes.Substring(i + "{{QText}}".Length);

                i = lRes.IndexOf("{{RComment}}");
                if (i >= 0)
                    lRes = lRes.Substring(0, i) + "<input type='text' name='RComment' value='{{RComment}}' placeholder='Коментар респондента' alt='Коментар респондента'>" + lRes.Substring(i + "{{RComment}}".Length);

                return lRes;
            }

            private DataContainer registerTemplates(DataGraph.DataNode pDataNode, DataContainer pState)
            {
                var lColl = pDataNode as DataGraph.CollectionDataNode;
                var lModule = PollModule;
                if (lColl  == null || lModule == null)
                    return DataContainer.NULL;
                var lDesignMode = pDataNode.Parent.GetResult("QTHeader.DesignMode", null).GetInt()==1;
                foreach (var sn in lColl)
                {
                    var lId = sn.GetValue("gid").GetInt();
                    if (lId <= 0)
                        continue;
                    var lTemplateBody = sn.GetValue("ViewTemplate").GetString();
                    if (string.IsNullOrEmpty(lTemplateBody))
                        continue;
                    //lModule.RegisterHtmlTemplate("QV@" + lId, makeDesidnModeTemplate(lTemplateBody));
                    if (lDesignMode)
                        lModule.RegisterHtmlTemplate("QVD@" + lId, makeDesidnModeTemplate(lTemplateBody));
                    else
                        lModule.RegisterHtmlTemplate("QV@" + lId, lTemplateBody);
                }
                return BoolContainer.True;
            }

        }

        private static ContextBag<WebPollSessionContext> s_WebSessionsBag = new ContextBag<WebPollSessionContext>();

        private readonly object m_lock = new object();
        private static string generateNewPLID(int pTid)
        {
            if (pTid > 0)
                return "PL" + appcore.AppCore.EncodeUInt((uint)pTid) + "*" + Convert.ToBase64String(Guid.NewGuid().ToByteArray()).TrimEnd('=').Replace("/", "_").Replace("+", "-");
            else
                return string.Empty;
        }
        private static string generateNewSessionID()
        {
            return "PS*" + Convert.ToBase64String(Guid.NewGuid().ToByteArray()).TrimEnd('=').Replace("/", "_").Replace("+", "-");
        }
        private WebPollSessionContext m_gsContext;
        public const string POLLSESSIONID_COOKIENAME = "PollSessionID";

        private WebPollSessionContext GetSessionContext(HttpRequestMessage pRequest, bool pDoCheck = false)
        {
            if (m_gsContext == null || (pDoCheck && CheckUserSession()))
            {
                lock (m_lock)
                {
                    if (m_gsContext == null)
                    {
                        var lSessionID = K7Session.SessionToken;
                        var lNeedSetCookie = false;
                        if (!string.IsNullOrEmpty(lSessionID))
                            m_gsContext = s_WebSessionsBag.Get(lSessionID);
                        else
                        {
                            lSessionID = Request.Headers.GetCookies(POLLSESSIONID_COOKIENAME)?.FirstOrDefault()?[POLLSESSIONID_COOKIENAME]?.Value;
                            if (!string.IsNullOrEmpty(lSessionID) && lSessionID.Length > 4)
                                m_gsContext = s_WebSessionsBag.Get(lSessionID);
                            else
                            {
                                lSessionID = generateNewSessionID();
                                lNeedSetCookie = true;
                            }
                            
                            lNeedSetCookie = true;
                        }

                        if (m_gsContext == null)
                        {
                            m_gsContext = new WebPollSessionContext(WebGraphController.Instance, K7Session, lSessionID);
                            m_gsContext.NeedSetCookie = lNeedSetCookie;
                            s_WebSessionsBag.Save(lSessionID, m_gsContext);
                        }
                    }
                }
            }
            
            return m_gsContext;
        }

        private void initList()
        {
            var lModule = PollModule;
            var lListAction = lModule.GetActionItem("GetList");
            var lActionSchemaNode = lModule.GetActionSourceNode(lListAction);
            var lCtr = WebGraphController.Instance;
            var lGraph = lCtr.GetData(lActionSchemaNode.OwningSchema);
            var lDataNode = lGraph.GetNodeByPath(lActionSchemaNode.FullName, true);
            var lContext = new GraphController.ActionContext(lListAction, lDataNode, lCtr);
            var lres = lListAction.Execute(lContext);
        }

        private BaseTypesEnum getValueType(int pQType)
        {
            if (pQType <= 0)
                return BaseTypesEnum.Null;
            switch(pQType)
            {
                case _DataTypes_SD_.QuestionDataTypes_enum_.Boolean:
                    return BaseTypesEnum.Bool;
                case _DataTypes_SD_.QuestionDataTypes_enum_.Integer:
                    return BaseTypesEnum.Int32;
                case _DataTypes_SD_.QuestionDataTypes_enum_.FractionalNumber:
                    return BaseTypesEnum.Decimal;
                case _DataTypes_SD_.QuestionDataTypes_enum_.DateTime:
                    return BaseTypesEnum.DateTime;
                case _DataTypes_SD_.QuestionDataTypes_enum_.AlternativeSelection:
                    return BaseTypesEnum.Int32;
                case _DataTypes_SD_.QuestionDataTypes_enum_.MultipleAlternativeSelection:
                case _DataTypes_SD_.QuestionDataTypes_enum_.MultiMatrix:
                case _DataTypes_SD_.QuestionDataTypes_enum_.AlternativesRank:
                case _DataTypes_SD_.QuestionDataTypes_enum_.AlternativesMatrix:
                    return BaseTypesEnum.IntArray;
                default:
                    return BaseTypesEnum.String;
            }
        }

        private string correctName(string pName)
        {
            var lRes = pName.Replace(".", "d").Replace("\t", "_").Replace("\t", "_").Replace(" ", "_");
            if (char.IsDigit(lRes[0]))
                lRes = "_q" + lRes;
            return lRes;
        }

        private void regPollSchema(int pId)
        {
            try
            {
                var lCtr = WebGraphController.Instance;
                var lPollGraph = lCtr.GetData("T" + pId + "@QnA_Schema");
                if (lPollGraph == null)
                {
                    return;
                }
                var lQItems = lPollGraph.GetNodeByPath("QnT_Items", false) as DataGraph.CollectionDataNode;
                if (lQItems == null)
                    return;

                var lSchema = new DataGraph.Schema("QnT_" + pId);
                var r = lSchema.RootNode;
                foreach (var n in lQItems)
                {
                    r.AddSubNode(correctName(n.GetValue("_Code").GetString()), getValueType(n.GetResult("QuestionKind.DataType", string.Empty).GetInt()));
                }
                var lConditions = lPollGraph.GetNodeByPath("QnT_Conditions", false) as DataGraph.CollectionDataNode;
                if (lConditions != null)
                {
                    foreach (var cnd in lConditions)
                    {
                        var lExpr = cnd.GetValue("expression").GetString();
                        var lCndID = cnd.GetValue("gid").GetInt();
                        if (!string.IsNullOrEmpty(lExpr) && lCndID > 0)
                        {
                            var lCN = r.AddSubNode("_C" + lCndID, BaseTypesEnum.Bool);
                            lCN.SetFormula(lExpr, null);
                        }

                    }
                }
                WebGraphController.Instance.RegisterSchema(lSchema.Name, lSchema);
                GraphXmlConvertor.SerializeToFile(lSchema,
                    System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory + "Logs", lSchema.Name + ".ags"));
            }
            catch(Exception ex)
            {
                Logger.LogException(ex);
            }
            //var lPollGraph = 
        }

        [HttpPost]
        [HttpGet]
        public HttpResponseMessage GetPoll(string pollname)
        {
            if (!string.IsNullOrEmpty(pollname))
            {
                int lId = 0;
                if (int.TryParse(pollname, out lId))
                    regPollSchema(lId);
            }

            var lAction = pollname;
            if (lAction == "List")
                lAction = "GetList";

            try
            {

                GActResponse lRes = null;
                var lSession = GetSessionContext(Request);
                if (!string.IsNullOrEmpty(Request.RequestUri.Query))
                {
                    var lParams = Request.RequestUri.ParseQueryString();
                    lRes = WebGraphController.Instance.ProcessRequest(PollModule, lAction, (n) => n.FromForm(lParams), lSession);
                }
                else
                {
                    var context = (HttpContextBase)Request.Properties["MS_HttpContext"];
                    var s = context.Request.InputStream;
                    string lRequestStr = string.Empty;
                    if (s != null && s.CanRead)
                    {
                        s.Seek(0, SeekOrigin.Begin);
                        using (var lReader = new StreamReader(s, Encoding.UTF8))
                            lRequestStr = lReader.ReadToEnd();
                    }
                    lRes = WebGraphController.Instance.ProcessRequest(PollModule, lAction, (n) => string.IsNullOrEmpty(lRequestStr) ? n : n.FromJson(lRequestStr), lSession);
                }
                if (lRes != null)
                {
                    if (lSession.NeedSetCookie)
                    {
                        var lResp = lRes.GetHttpResponse(Request);
                        lResp.Headers.AddCookies(new[] { new System.Net.Http.Headers.CookieHeaderValue(POLLSESSIONID_COOKIENAME, lSession.SessionID) });
                        lSession.NeedSetCookie = false;
                        return lResp;
                    }
                    else
                        return lRes.GetHttpResponse(Request);
                }



            }
            catch (Exception ex)
            {
                Logger.LogException(ex);
                var lResponse = Request.CreateResponse(HttpStatusCode.InternalServerError);
                lResponse.Content = new StringContent(ex.Message, Encoding.UTF8, "text/plain");
                return lResponse;
            }

            return new HttpResponseMessage(HttpStatusCode.NoContent);

            //return null;
        }

        public static void Reset()
        {
            s_PollModule = null;
        }
    }
}
