﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Web;
using appcore;
using System.Net;
using System.Text;
using K7_Web.AtlasAPI;

namespace K7_Web.Controllers
{
    public class PagesController: ControllerBase
    {
        private class GCommandInfo
        {
            public string Module { get;  }
            public string Command { get; }
            public bool WithParameters { get; }
            public GCommandInfo(string pModule, string pCommand, bool pWithParameters)
            {
                Module = pModule;
                Command = pCommand;
                WithParameters = pWithParameters;
            }
        }

        private static string s_RootDir = getRootDir();
        
        private static DuoDictionary<string, string, string> s_PagesMap = new DuoDictionary<string, string, string>();
        private static List<string> s_GraphPageModules = new List<string>();
        private static DuoDictionary<string, string, GCommandInfo> s_GraphPagesMap = initPageCommands();

        private static string getRootDir()
        {
            string root = AppDomain.CurrentDomain.BaseDirectory;
            return Path.Combine(root, "wwwroot");
        }
        private static DuoDictionary<string, string, GCommandInfo> initPageCommands()
        {
            string root = AppDomain.CurrentDomain.BaseDirectory;
            var lIniFileName = Path.Combine(root, "GraphPages.ini");
            var lMap = new DuoDictionary<string, string, GCommandInfo>();
            if (File.Exists(lIniFileName))
            {
                s_GraphPageModules.Clear();
                var lLines = File.ReadAllLines(lIniFileName, Encoding.UTF8);
                if (lLines != null && lLines.Length>0)
                {
                    foreach(var lStr in lLines)
                    {
                        var lParts = lStr.Split(':');
                        if (lParts.Length < 2)
                            continue;
                        var lPagePath = lParts[0];
                        var lGraphPath = lParts[1];

                        var lPageParts = lPagePath.Split('/');
                        var lGraphParts = lGraphPath.Split('/');
                        if (lPageParts.Length < 2 || lGraphParts.Length < 2)
                            continue;
                        var lFolderName = lPageParts[0];
                        var lModuleName = lGraphParts[0];
                        GCommandInfo lInfo = new GCommandInfo(lModuleName, lGraphParts[1], lGraphParts.Length > 2 && lGraphParts[2] == "?");
                        lMap.Set(lFolderName, lPageParts[1], lInfo);
                        if (lFolderName == lModuleName)
                            s_GraphPageModules.Add(lModuleName);
                    }
                }

            }
            return lMap;
        }

        internal static void Reset()
        {
            s_PagesMap = new DuoDictionary<string, string, string>();
            s_GraphPagesMap = initPageCommands();
        }

        private object m_lock = new object();
        private WebSessionContext m_GSContext;
        private WebSessionContext getSessionContext(bool pDoCheck = false)
        {
            if (m_GSContext == null)
            {
                lock (m_lock)
                {
                    if (m_GSContext == null)
                    {
                        m_GSContext = new WebSessionContext(WebGraphController.Instance, base.K7Session);
                    }
                }
            }
            else if (pDoCheck)
            {
                if (CheckUserSession())
                    m_GSContext = new WebSessionContext(WebGraphController.Instance, base.K7Session);
            }
            return m_GSContext;
        }

        public static HttpResponseMessage GetStaticPage(string folder, string pagename, HttpRequestMessage pRequest)
        {
            string lPage = s_PagesMap.Get(folder, pagename);
            HttpResponseMessage lResponse;
            if (lPage != null)
            {
                lResponse = pRequest.CreateResponse(HttpStatusCode.OK);
                lResponse.Content = new StringContent(lPage, Encoding.UTF8, "text/html");
                return lResponse;
            }

            var lFolder = string.IsNullOrEmpty(folder) ? s_RootDir : Path.Combine(s_RootDir, folder);
            if (!Directory.Exists(lFolder))
                return pRequest.CreateResponse(HttpStatusCode.NotFound);
            var lFileName = Path.Combine(lFolder, pagename + ".html");
            if (!File.Exists(lFileName))
                return pRequest.CreateResponse(HttpStatusCode.NotFound);
            lock (s_PagesMap)
            {
                lPage = s_PagesMap.Get(folder, pagename);
                if (lPage != null)
                {
                    lResponse = pRequest.CreateResponse(HttpStatusCode.OK);
                    lResponse.Content = new StringContent(lPage, Encoding.UTF8, "text/html");
                    return lResponse;
                }
                var lBody = File.ReadAllText(lFileName, Encoding.UTF8);
                if (!string.IsNullOrEmpty(lBody))
                {
                    lPage = lBody;
                    s_PagesMap.Set(folder, pagename, lPage);
                }
            }
            if (lPage != null)
            {
                lResponse = pRequest.CreateResponse(HttpStatusCode.OK);
                lResponse.Content = new StringContent(lPage, Encoding.UTF8, "text/html");
                return lResponse;
            }
            else
                return pRequest.CreateResponse(HttpStatusCode.NotFound);
        }

        public HttpResponseMessage GetPage(string folder, string pagename)
        {
            try
            {
                GCommandInfo lInfo = s_GraphPagesMap.Get(folder, pagename);
                if (lInfo != null)
                {
                    var lP = Request.Headers.GetCookies("TEST_TICKET_COOKIE");

                    GActResponse lRes = null;
                    if (lInfo.WithParameters)
                    {
                        var lParams = Request.RequestUri.ParseQueryString();
                        lRes = WebGraphController.Instance.ProcessRequest(lInfo.Module, lInfo.Command, lParams, getSessionContext());
                    }
                    else
                    {
                        lRes = WebGraphController.Instance.ProcessRequest(lInfo.Module, lInfo.Command, string.Empty, getSessionContext());
                    }
                    if (lRes != null)
                        return lRes.GetHttpResponse(Request);
                }
                else if (s_GraphPageModules.Contains(folder))
                {
                    GActResponse lRes = null;
                    if (!string.IsNullOrEmpty(Request.RequestUri.Query))
                    {
                        var lParams = Request.RequestUri.ParseQueryString();
                        lRes = WebGraphController.Instance.ProcessRequest(folder, pagename, lParams, getSessionContext());
                    }
                    else
                        lRes = WebGraphController.Instance.ProcessRequest(folder, pagename, string.Empty, getSessionContext());

                    if (lRes != null)
                        return lRes.GetHttpResponse(Request);
                }
                return GetStaticPage(folder, pagename, Request);
                
            }
            catch (Exception ex) {
                Logger.LogException(ex);
                var lResponse = Request.CreateResponse(HttpStatusCode.InternalServerError);
                lResponse.Content = new StringContent(ex.Message, Encoding.UTF8, "text/plain");
                return lResponse;
            }
        }
    }
}