﻿using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using System.IO;
using K7_Web.AtlasAPI;
using K7_Web.Models;
using ADE.EntityModel;
using Newtonsoft.Json;
using appcore.DataProcessing;

namespace K7_Web.Controllers
{
    public class GApiController : ControllerBase
    {
        private static ContextBag<WebSessionContext> s_WebSessionsBag = new ContextBag<WebSessionContext>();

        private readonly object m_lock = new object();
        private WebSessionContext m_gsContext;
        private WebSessionContext GetSessionContext(bool pDoCheck = false)
        {
            if (m_gsContext == null)
            {
                lock (m_lock)
                {
                    if (m_gsContext == null)
                    {
                        if (K7Session.SessionToken != null)
                            m_gsContext = s_WebSessionsBag.Get(K7Session.SessionToken);
                        if (m_gsContext == null)
                        {
                            m_gsContext = new WebSessionContext(WebGraphController.Instance, K7Session);
                            if (K7Session.SessionToken != null)
                                s_WebSessionsBag.Save(K7Session.SessionToken, m_gsContext);
                        }
                    }
                }
            }
            else if (pDoCheck)
            {
                if (CheckUserSession())
                {
                    if (K7Session.SessionToken != null)
                        m_gsContext = s_WebSessionsBag.Get(K7Session.SessionToken);
                    if (m_gsContext == null)
                    {
                        m_gsContext = new WebSessionContext(WebGraphController.Instance, K7Session);
                        if (K7Session.SessionToken != null)
                            s_WebSessionsBag.Save(K7Session.SessionToken, m_gsContext);
                    }
                }
            }
            return m_gsContext;
        }

        private static WebSessionContext s_PublicSessionContext = null;
        public static WebSessionContext PublicSessionContext { get {
                return s_PublicSessionContext ?? (s_PublicSessionContext = new WebSessionContext(WebGraphController.Instance, Models.User.K7UserSession.PublicSession));
            } }

        internal static void Reset()
        {
            s_PublicSessionContext = null;
        }

        [HttpPost]
        [HttpGet]
        public HttpResponseMessage DoCommand(string module, string command)
        {
            try
            {
                GActResponse lRes;
                if (ServiceBase.ContainsController(module))
                {
                    var lInfo = ServiceBase.GetControllerInfo(module);
                    var lContext = GetSessionContext();
                    var lController = lInfo.CreateInstance(lContext);
                    var lMethod = lInfo.GetActionInfo(command);
                    if (lMethod == null)
                        return Request.CreateResponse(HttpStatusCode.BadRequest, "Absent method: "+command);
                    var lTypeInfo = TypeSchemaInfo.GetInfo(lMethod.InputType);
                    var context = Request.Properties["MS_HttpContext"] as HttpContextBase;
                    var lFormData = context?.Request.Form;
                    object lInpObj;
                    if (lFormData != null && lFormData.Count > 0)
                    {
                        var lEntityObj = lTypeInfo.CreateNewInstanceObject() as EntityBase;
                        foreach (var k in lFormData.AllKeys)
                        {
                            lEntityObj[k] = lFormData.Get(k);
                        }
                        lInpObj = lEntityObj;
                    }
                    else
                    {
                        var s = context.Request.InputStream;
                        string lRequestStr;
                        s.Seek(0, SeekOrigin.Begin);
                        using (var lReader = new StreamReader(s, Encoding.UTF8))
                            lRequestStr = lReader.ReadToEnd();
                        lInpObj = JsonConvert.DeserializeObject(lRequestStr, lMethod.InputType);
                    }
                    var lResObj = lMethod.Method.Invoke(lController, new[] { lInpObj });
                    if (lResObj == null)
                        return Request.CreateResponse(HttpStatusCode.NoContent);
                    if (lMethod.OutFormat == "html")
                    {
                        var lModule = WebGraphController.Instance.GetModule(module);
                        var lTemplateProcessor = new ViewTemplateProcessor(lModule.HtmlTemplates, null, lContext);
                        lTemplateProcessor.Add(lResObj as EntityBase);
                        lRes = new GActResponse(ResponseTypes.HtmlResult, lTemplateProcessor.ToString());
                    }
                    else
                        lRes = new GActResponse(ResponseTypes.JsonResult, JsonConvert.SerializeObject(lResObj));
                }
                else if (Request.Method == HttpMethod.Get)
                    lRes = WebGraphController.Instance.ProcessGet(module, command, GetSessionContext());
                else
                {
                    var context = (HttpContextBase)Request.Properties["MS_HttpContext"];
                    var lFormData = context.Request.Form;

                    if (lFormData != null && lFormData.Count > 0)
                        lRes = WebGraphController.Instance.ProcessRequest(module, command, lFormData, GetSessionContext());
                    else if (command == "test")
                        return (new GActResponse(ResponseTypes.JsonResult, "{\"Message\":\"OK. your id = " + K7Session.WebAccountId + "\"}")).GetHttpResponse(Request);
                    else
                    {
                        var s = context.Request.InputStream;
                        string lRequestStr;
                        s.Seek(0, SeekOrigin.Begin);
                        using (var lReader = new StreamReader(s, Encoding.UTF8))
                            lRequestStr = lReader.ReadToEnd();
                        lRes = WebGraphController.Instance.ProcessRequest(module, command, lRequestStr, GetSessionContext());
                    }
                }
                return lRes == null ? Request.CreateResponse(HttpStatusCode.MethodNotAllowed) : lRes.GetHttpResponse(Request);
            }
            catch (InvalidDataException ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
            catch (UnauthorizedAccessException ex)
            {
                return Request.CreateResponse(HttpStatusCode.Unauthorized, ex.Message);
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    var lInner = ex.InnerException;
                    if (lInner is UnauthorizedAccessException)
                        return Request.CreateResponse(HttpStatusCode.Unauthorized, lInner.Message);
                    else if (lInner is InvalidDataException)
                        return Request.CreateResponse(HttpStatusCode.BadRequest, lInner.Message);
                }
                Logger.LogException(ex);
                Logger.LogError($"Error on command {module}.{command}");
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

    }
}