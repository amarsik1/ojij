﻿using System.Net.Http;
using asb_ipc;
using System.Web.Http;
using K7_Web.Models.User;
using System.Web.Http.Controllers;
using System.Security.Claims;
using K7_Web.AtlasAPI;

namespace K7_Web.Controllers
{
    public class ControllerBase : ApiController
    {
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            K7Session = User?.Identity is ClaimsIdentity ? new K7UserSession((ClaimsIdentity)User.Identity) : null;
            if (string.IsNullOrEmpty(K7Session?.SessionToken) && controllerContext.Request != null)
            {
                var context = controllerContext.Request.GetOwinContext();
                var token = context.Request.Cookies["_ast"];
                var sid = context.Request.Cookies["_awai"];
                int id;
                if (!string.IsNullOrEmpty(token) && !string.IsNullOrEmpty(sid) && int.TryParse(sid, out id) && id > 0)
                    K7Session = new K7UserSession(id, token);
            }
            base.Initialize(controllerContext);
        }

        protected bool CheckUserSession()
        {
            // Checks if session changed;
            var lIdentity = User?.Identity as ClaimsIdentity;
            if (lIdentity == null)
            {
                if (K7Session == null) return false;
                K7Session = null;
                return true;
            }
            if (K7Session == null)
            {
                K7Session = new K7UserSession(lIdentity);
                return true;
            }
            var lTokenClaim = lIdentity.FindFirst(K7Claims.ATLAS_SESSION_TOKEN);
            if (K7Session.SessionToken == lTokenClaim?.Value) return false;
            K7Session = new K7UserSession(lIdentity);
            return true;
        }

        protected static ValueTableSet RootDataCache { get { return KvantaAppContext.RootDataCache; } }

        public K7UserSession K7Session { get; private set; }
    }
}
