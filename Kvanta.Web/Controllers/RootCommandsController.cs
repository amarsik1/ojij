﻿using K7_Web.AtlasAPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace K7_Web.Controllers
{
    public class RootCommandsController : ControllerBase
    {
        public static void Reset()
        {
            AtlasContextCore.ClearCache();
            PagesController.Reset();
            WebGraphController.Reset();
            GApiController.Reset();
            GPollController.Reset();
        }

        [HttpGet]
        public async Task<HttpResponseMessage> DoCommand(string command)
        {
            if (command == "reset.x")
            {
                Reset();
                return await Redirect(Url.Content("~/")).ExecuteAsync(CancellationToken.None);
            }
            else if (command == "TestG-Redis")
            {
                UnitTests.TestG_Redis();
                return await Redirect(Url.Content("~/")).ExecuteAsync(CancellationToken.None);
            }
            else if (command == "TestG-Read")
            {
                UnitTests.TestG_Read();
                return await Redirect(Url.Content("~/")).ExecuteAsync(CancellationToken.None);
            }
            else if (command == "ConfirmEmail")
            {
                var lParams = Request.RequestUri.ParseQueryString();
                var lCode = lParams["Code"]; // Check Who is;
                string lPrefix;
                int lWAid = 0;
                Guid lGuid;
                K7_sp.Classes.WebHelper.TryParseRequestCode(lCode, out lPrefix, out lWAid, out lGuid);
                if (lPrefix != "MC" || lWAid <= 0)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest);
                }
                /*else if (K7Session== null || K7Session.WebAccountId != lWAid)
                    return await Redirect(Url.Content("~/User/Login")).ExecuteAsync(CancellationToken.None);*/

                MembersProcessor.ConfirmUserEmail(lCode);
            }
            else if (KvantaApplication.ContainsRootPage(command))
            {
                return PagesController.GetStaticPage(string.Empty, command, Request);
            }
            else if (KvantaApplication.ContainsRootGraphPage(command))
            {
                var lRes = WebGraphController.Instance.ProcessRequest(KvantaApplication.GetRootGraphPageModule(command), command,
                    Request.RequestUri.ParseQueryString(), GApiController.PublicSessionContext);

                return lRes == null ? Request.CreateResponse(HttpStatusCode.MethodNotAllowed) : lRes.GetHttpResponse(Request);
            }

            return await Redirect(Url.Content("~/")).ExecuteAsync(CancellationToken.None);
        }
    }
}
