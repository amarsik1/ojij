﻿using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Owin;
using Newtonsoft.Json;
using Viber.Bot;

namespace K7_Web.Providers
{
    public class ViberProvider
    {
        private const string AuthenticationToken = "479206bbf127d701-793b2d7d1d0e72b2-7ee2120a1c1d30ca";
        public readonly IViberBotClient ViberBotClient;
        private static readonly Lazy<ViberProvider> s_lazy = new Lazy<ViberProvider>(() => new ViberProvider());
        public static ViberProvider Instance { get { return s_lazy.Value; } }


        #region Methodes

        private ViberProvider()
        {
            ViberBotClient = new ViberBotClient(AuthenticationToken);
        }

        public async Task SetWebHook(string url)
        {
            try
            {
                var result = await ViberBotClient.SetWebhookAsync(url);
            }
            catch (Exception e)
            {
                Logger.LogException(e);
            }
        }

        //public bool ValidateWebhookHash(string signatureHeader, string jsonMessage)
        //{
        //    return ViberBotClient.ValidateWebhookHash(signatureHeader, jsonMessage);
        //}

        #endregion
    }

    public class ViberWebhookMiddleware : OwinMiddleware
    {
        public ViberWebhookMiddleware(OwinMiddleware next)
            : base(next)
        {
        }
        public override async Task Invoke(IOwinContext context)
        {
            var isEndpointValid = context.Request.Path.Value.ToLower().Equals("/api/account/vibbot");
            if (!isEndpointValid)
            {
                await Next.Invoke(context);
                return;
            }

            var client = ViberProvider.Instance.ViberBotClient;

            var body = new StreamReader(context.Request.Body).ReadToEnd();
            var isSignatureValid = client.ValidateWebhookHash(context.Request.Headers[ViberBotClient.XViberContentSignatureHeader], body);
            if (!isSignatureValid)
            {
                Logger.LogError("ViberWebhookMiddleware", "Invalid viber content signature");
                //throw new Exception("Invalid viber content signature");
            }

            try
            {
                var callbackData = JsonConvert.DeserializeObject<CallbackData>(body);
                // process callback
                if (callbackData != null)
                {
                    var result = await client.GetAccountInfoAsync();
                     //result = await client.GetUserDetailsAsync(_adminId);
                    if (callbackData.Message != null)
                    {
                        //client.
                    }
                }
            }
            catch (Exception e)
            {
                Logger.LogException(e);
            }
        }
    }
}