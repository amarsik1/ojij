﻿using GApi.Lib.Auth;

namespace K7_Web.Providers
{
    public class KvantaOAuthProvider : BaseApplicationOAuthProvider
    {
        public KvantaOAuthProvider(AppContextBase atlasContextCore, ClaimsParserBase claimsParser, string publicClientId) :
            base(atlasContextCore, claimsParser, publicClientId)
        { }

        
        //public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        //{
        //    var browser = Utils.GetMajorBrowserName(context.Request.Headers.Get("User-Agent"));
        //    var lUserInfo = await kvantaAppContext.UserLoginAsync(context.UserName, context.Password, context.Request.RemoteIpAddress, browser).ConfigureAwait(false);
        //    if (!lUserInfo.IsAuthenticated)
        //    {
        //        context.SetError("invalid_grant", lUserInfo.Message);
        //        Logger.LogError("7K.Web", "SignIn failed: " + lUserInfo.Message);
        //        return;
        //    }
        //    lUserInfo.SocNets = await AccountController.GetUserNetsList(lUserInfo.WebAccountId);
        //    var claims = new List<Claim>
        //    {
        //        new Claim(ClaimTypes.Authentication, context.UserName),
        //        new Claim(ClaimTypes.Email, context.UserName),
        //        new Claim(ClaimTypes.GivenName, lUserInfo.UserName),
        //        new Claim(K7Claims.WEB_ACCOUNT_ID, lUserInfo.WebAccountId.ToString()),
        //        new Claim(K7Claims.ATLAS_SESSION_TOKEN, lUserInfo.Token)
        //    };
        //    var oAuthIdentity = new ClaimsIdentity(claims, context.Options.AuthenticationType);
        //    var properties = CreateProperties(lUserInfo);
        //    var ticket = new AuthenticationTicket(oAuthIdentity, properties);
        //    context.Validated(ticket);

        //    var cookiesIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationType);
        //    context.Request.Context.Authentication.SignIn(cookiesIdentity);
        //    var co = new Microsoft.Owin.CookieOptions { Expires = DateTime.UtcNow.AddMinutes(20) };
        //    context.Response.Cookies.Append("_ast", lUserInfo.Token, co);
        //    context.Response.Cookies.Append("_awai", lUserInfo.WebAccountId.ToString(), co);
        //}

        //public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        //{
        //    foreach (var property in context.Properties.Dictionary)
        //        context.AdditionalResponseParameters.Add(property.Key, property.Value);
        //    return Task.FromResult<object>(null);
        //}

        //public override Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        //{
        //    // Resource owner password credentials does not provide a client ID.
        //    if (context.ClientId == null)
        //    {
        //        //Remove the comments from the below line context.SetError, and invalidate context 
        //        //if you want to force sending clientId/secrects once obtain access tokens. 
        //        context.Validated();
        //        //context.SetError("invalid_clientId", "ClientId should be sent.");
        //        return Task.FromResult<object>(null);
        //    }
        //    return Task.FromResult<object>(null);
        //}

        //public override Task ValidateClientRedirectUri(OAuthValidateClientRedirectUriContext context)
        //{
        //    if (context.ClientId != _publicClientId) return Task.FromResult<object>(null);
        //    var expectedRootUri = new Uri(context.Request.Uri, "/authcomplete.html");
        //    if (expectedRootUri.AbsoluteUri == context.RedirectUri)
        //        context.Validated();
        //    return Task.FromResult<object>(null);
        //}

        //public static AuthenticationProperties CreateProperties(UserInfo pUserInfo)
        //{
        //    var propsDic = new Dictionary<string, string>
        //    {
        //        {"userLogin", pUserInfo.UserLogin},
        //        {"userName", pUserInfo.UserName},
        //        {"webAccountId", pUserInfo.WebAccountId.ToString()},
        //        {"RedirectURL", "/Cabinet"},
        //        {"userNets", string.Join(";", pUserInfo.SocNets) }
        //    };
        //    var lAtlasContext = AtlasContextCore.Instance?.GetUserSession(pUserInfo.Token);
        //    if (lAtlasContext == null) return new AuthenticationProperties(propsDic);
        //    /*var lBizAgentID = lAtlasContext.GetSPValue(MD_Schema._SP_Shema_.BizAgent).GetInt();
        //    if (lBizAgentID > 0)
        //        propsDic.Add("BizAgent", lBizAgentID.ToString());*/
        //    propsDic.Add("PersonID", lAtlasContext.LoginInfo.PersonId.ToString());
        //    return new AuthenticationProperties(propsDic)
        //    {
        //        IssuedUtc = DateTime.UtcNow,
        //        ExpiresUtc = DateTime.UtcNow.Add(TokenExpiration)
        //    };
        //}
    }
}