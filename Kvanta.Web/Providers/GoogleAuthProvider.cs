﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.Owin.Security.Google;

namespace K7_Web.Providers
{
    public class GoogleAuthProvider : GoogleOAuth2AuthenticationProvider, IGoogleOAuth2AuthenticationProvider
    {
        public override  Task Authenticated(GoogleOAuth2AuthenticatedContext context)
        {
            context.Identity.AddClaim(new Claim("ExternalAccessToken", context.AccessToken));
            foreach (var claim in context.User)
            {
                var claimType = string.Empty;
                var bAddClaim = false;
                switch (claim.Key)
                {
                    case "given_name":
                        claimType = ClaimTypes.GivenName;
                        bAddClaim = true;
                        break;
                    case "family_name":
                        claimType = ClaimTypes.Surname;
                        bAddClaim = true;
                        break;
                    case "gender":
                        claimType = ClaimTypes.Gender;
                        bAddClaim = true;
                        break;
                }
                if (!bAddClaim) continue;
                var claimValue = claim.Value.ToString();
                if (!context.Identity.HasClaim(claimType, claimValue))
                    context.Identity.AddClaim(new Claim(claimType, claimValue,
                        "XmlSchemaString", "Google"));
            }
            return Task.FromResult<object>(null);
        }

        public override Task ReturnEndpoint(GoogleOAuth2ReturnEndpointContext context)
        {
            return Task.FromResult<object>(null);
        }

        public override void ApplyRedirect(GoogleOAuth2ApplyRedirectContext context)
        {
            context.Response.Redirect(context.RedirectUri);
        }
    }
}