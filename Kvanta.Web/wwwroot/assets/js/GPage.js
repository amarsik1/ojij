﻿
var GPage = function () {
    var _scope = {};
    var _build = null;
    function _nano(tt, d, scope) {
        var _html = tt.html;
        var _driver = tt.driver;
        if (_driver && typeof _driver.OnRender == 'function')
            _driver.OnRender.call(d);

        return _html.replace(/\{([\w\.\-]*)\|{0,1}([\w\.\-]*)\}/g, function (s, key, fm) {
			if (!d) return '';
			var v;
			if (key == "this") v=d;
			else{
				var keys = key.split("."), v;
				v = d[keys.shift()];
				for (var i = 0, l = keys.length; i < l; i++) v = v[keys[i]];
			}
            
            if (fm) {
                return _build(scope, fm, v);
            }
            return (typeof v !== "undefined" && v !== null) ? v : "";
        })
    };
    _build = function(scope, tName, data) {
        if (!scope || !tName) return '';
        var tColl = scope.templates;
        if (!tColl) return '';

        var r = '', t = '';
        if (tName.endsWith('-each')) {
            t = tColl[tName.substr(0, tName.length - 5)];
            if (t && Array.isArray(data)) {
                data.forEach(function (el) { r += _nano(t, el, scope); });
                return r;
            }
        } else { t = tColl[tName]; }
        return t ? _nano(t, data, scope) : '';
    };
    var pi = {};
    var lPBodyDom = "";
    var _Drivers = {};

    function pageDriver(pName) {
        var _pName = null;
        var t = this;
        t.GetName = function () { return _pName; }
        t._init = function (pName) {
            if (!_pName)
                _pName = pName;
        };
        t.onSetPage = function () {
            if (lPBodyDom) {
                var l = lPBodyDom;
                l.find(".modal").modal();
                l.find('select').material_select();
                if (typeof t.OnSetPage == 'function')
                    t.OnSetPage();
                l.find("[s-click]").each(function (i, el) {
                    var jl = $(el);
                    var f = t[jl.attr("s-click")];
                    if (f) jl.on("click", jl, f);
                });
            }
        };
        t.SetPage2 = function (pName, pData) {
            var d = _build(_scope, pName, pData);
            if (typeof d == "string") {
                var lInf = pi[pName];
                if (!lInf) pi[pName] = lInf = {};
                lInf.Html = d;
                lPBodyDom.html(d);
                t.onSetPage();
            }
        };
        t.SetPageH = function (pName, pHtml, pTemp) {
            var d = pHtml;
            if (typeof d == "string") {
                var lInf = pi[pName];
                if (!lInf) pi[pName] = lInf = {};
                if (!pTemp) lInf.Html = d;
                lPBodyDom.html(d);
                t.onSetPage();
            }
        }
        t._init(pName);

    };

    function _addDrv(pName, pCreator) {
        pCreator.prototype = new pageDriver();
        var _d = new pCreator();
        pageDriver.call(_d, pName);
        _Drivers[pName] = _d;
    };

	function _curHash()
	{
		var lHash = location.hash;
		if (lHash.startsWith("#/"))
			lHash = lHash.substr(2);
		else if (lHash.startsWith("#"))
			lHash = lHash.substr(1);
		if (lHash.startsWith("!"))
			lHash = lHash.substr(1)
		return lHash;
	};
	function resetH(pName){
		var lInf = pi[pName];
		if (lInf)
			lInf.Html = null;
		if (pName && pName == _curHash())
			setPage(pName);
	};
    function setPage(pName) {
        var lPName = pName;
        var lQ = "", lSubPage='';

        var ix = lPName.indexOf("?");
        if (ix > 0) {
            lQ = lPName.slice(ix + 1);
            lPName = lPName.substr(0, ix);
        }
        ix = lPName.indexOf("/");
        if (ix > 0) {
            lSubPage = lPName.slice(ix+1);
            lPName = lPName.substr(0, ix);
        }
        var lInf = pi[lPName];
        if (lInf && lInf.Html && !lQ && !lSubPage) {
            var _ok = 1;
            var _d = lInf.driver;
            if (lInf.Template) {
                
                if (_d && typeof _d.OnShow == 'function')
                    if (_d.OnShow() == false)
                        _ok = 0;
            }
            if (_ok) {
                if (!_d)
                    lInf.driver = _d = new pageDriver(lPName);
                lPBodyDom.html(lInf.Html);
				_d.onSetPage();
                return;
            }
        }
        GPage.CurPage = pName;

        var _drv = null;
        if (!lInf) pi[lPName] = lInf = {};
        var _t = _scope.templates[lPName];
        if (_t && _t.driver) {
            _drv = _t.driver;
            lInf.driver = _drv;
            GPage.CurDriver = _drv;
            if ((lQ || lSubPage) && typeof _drv.SetPage == 'function') {
                if (lQ) {
                    var lParts = lQ.split("&");
                    lQ = {};
                    for (var i = 0; i < lParts.length; i++) {
                        var a = lParts[i].split('=');

                        for (var j = 0; j < a.length; j++) {
                            lQ[a[0]] = a[1] === 'undefined' ? true : a[1];
                        }
                    }
                }
                return _drv.SetPage(lSubPage, lQ);
            }
            else if (typeof _drv.Init == 'function') {
                var d = _drv.Init();
                if (d) {
                    d = _build(_scope, pName, d);
                    lInf.Html = d;
                    lPBodyDom.html(d);
                    _drv.onSetPage();
                    return;
                }
                else if (d == false)
                    return;
            }
        }
        else
            lInf.driver = _drv = new pageDriver(lPName);
            
        app.GPost(_scope.module, pName, { user: _user.id },
            function (resp) {
            var d = '';
            if (typeof resp == "string")
                d = resp;
            else if (resp) {
				var dt = resp.data;
				if (!dt) dt = resp;
                if (_t) {
                    GPage.CurDriver = _drv;
                    if (_drv && typeof _drv.SetData == 'function')
                        _drv.SetData(dt);
                    d = _build(_scope, pName, dt);
                }
                lInf.data = resp.data;
            }
            if (_t)
                lInf.Template = _t;
            lInf.Html = d;
            lPBodyDom.html(d);
            if (_drv)
                _drv.onSetPage();
        },function(resp)
		{
			lPBodyDom.html('');
		}
		);

    }
    var _curModal = null;
    var _curAlert = null;
    function _onInput() {
        if (_curAlert) {
            _curAlert.hide();
            _curAlert = null;
        }
    };
    function _checkEdit(el, pData) {
        if (pData.ReadOnly === true) {
            el.addClass("ReadOnly");
            el.find("input[name]").attr("readonly", true);
            el.find("textarea[name]").attr("readonly", true);
            el.find("select[name]").attr("disabled", true);
        }
        else if (pData.ReadOnly === false) {
            el.removeClass("ReadOnly");
            el.find("input[name]").attr("readonly", false);
            el.find("textarea[name]").attr("readonly", false);
            el.find("select[name]").attr("disabled", false);
        }
    }
    function _Dlg(pName, pObj) {
        var _mName = "#" + pName +"Modal";
        var m = $(_mName + " .modal-content");
        m.html(_build(_scope, pName + "Dialog", pObj));
        var l = $(_mName);
        l.modal('open');
        $(_mName + " input").change(_onInput);
        $(_mName + " select").change(_onInput);
        $(_mName + " textarea").change(_onInput);
        l.find("[s-click]").each(function (i, el) {
            var jl = $(el);
            var f = pObj[jl.attr("s-click")];
            if (f) {
                jl.off("click");
                jl.on("click", jl, f);
            }
        });
        if (typeof pObj.OnShow == 'function')
            pObj.OnShow(l);
        
        _curModal = _mName;
        _checkEdit(l, pObj.data);
        Materialize.updateTextFields();
    }
    app.ShowError = function (pText) {
        var lAlertDiv = null;
        if (_curModal)
            lAlertDiv = $(_curModal + " .alert-danger");
        else
            lAlertDiv = $("#GPageBody .alert-danger");
        if (lAlertDiv && lAlertDiv.length) {
            if (!pText)
                lAlertDiv.hide();
            else {
                _curAlert = lAlertDiv;
                lAlertDiv.html(pText);
                lAlertDiv.show();
            }
        }
            
    };
    function _init() {
        $.fn.serializeObject = function () {
            var o = {};
            var a = this.serializeArray();
            $.each(a, function () {
                if (o[this.name]) {
                    if (!o[this.name].push) { o[this.name] = [o[this.name]]; }
                    o[this.name].push(this.value || '');
                } else { o[this.name] = this.value || '';}
            });
            return o;
        };

        lPBodyDom = $("#GPageBody");
        var lModule = lPBodyDom.attr("GModule");
        var tColl = {};
        var lTemplates = $("[TemplateID]");
        lTemplates.each(function (i, el) {
            var dl = $(el);
            var _to = { html: dl.html()};
            var _dn = dl.attr("Driver");
            if (_dn) {
                var _driver = _Drivers[_dn];
                if (_driver)
                    _to.driver = _driver;
            }
            tColl[dl.attr("TemplateID")] = _to;
            dl.html('');
        });
        var lLiPages = $("[li-page]");
        lLiPages.each(function (i, el) {
            var dl = $(el);
            var lPageID = dl.attr("li-page");
            var lDriver = dl.attr("Driver");
            if (lPageID && lDriver) {
                var _to = tColl[lPageID];
                if (!_to)
                    tColl[lPageID] = _to = {};
                var v = _Drivers[lDriver];
                if (v) _to.driver = v;
            }
        });
		var _loading=0;
        var lSources = lPBodyDom.attr("TSource");
        if (lSources) {
			_loading=1;
            $.get(lSources, function (res) {
                var lParts = res.split("\n");
                var lCurT = '';
                var lTName = '';
                var _to = null;
                lParts.forEach(function (el) {
                    var lM = el.trimRight().match(/^\[(\w[\w\-]*)\]$/);
                    if (lM) {
                        if (lTName && lCurT) {
                            _to = tColl[lTName];
                            if (!_to)
                                tColl[lTName] = _to = {};
                            _to.html = lCurT;
                        }
                        lCurT = '';
                        lTName = lM[1];
                    }
                    else
                        lCurT += (el + "\n");
                });
                if (lTName && lCurT) {
                    _to = tColl[lTName];
                    if (!_to)
                        tColl[lTName] = _to = {};
                    _to.html = lCurT;
                };
				setPage(_curHash());
            })
        };

        _scope = {
            module: lModule,
            templates: tColl
        };
        $(window).on('hashchange', function (e) {
            var lHash = _curHash();
			if (!lHash || lHash=='!')
				return;
            setPage(lHash);
        });
		if (!_loading)
			setPage(_curHash())
    };
    var s = {
        Init: _init,
        AddDriver: _addDrv,
        SetPage: setPage,
        CheckEdit: _checkEdit,
        /*SetPageH: setPageH,*/
        OpenDlg: _Dlg,
		ResetHtml: resetH,
		CurHash: _curHash,
		ShowMessage: function(ev, msg)
		{
			Materialize.toast(msg, 3000, 'rounded');
		},
		CloseModal:function(ev, msg){
			if (_curModal)
				$(_curModal).modal('close');
		}
    };
    return s;

}();

