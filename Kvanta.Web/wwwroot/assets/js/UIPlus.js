﻿var s = app;
s.ukMonths = [
    "Січень", "Лютий", "Березень", "Квітень", "Травень", "Червень", "Липень", "Серпень", "Вересень",
    "Жовтень", "Листопад", "Грудень"
];
s.ukMonthsR = [
    "Січня", "Лютого", "Березня", "Квітня", "Травня", "Червня", "Липня", "Серпня", "Вересня",
    "Жовтня", "Листопада", "Грудня"
];
s.ruMonths = [
    "Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь",
    "Ноябрь", "Декабрь"
];
s.ruMonthsR = [
    "Января", "Февраля", "Марта", "Апреля", "Мая", "Июня", "Июля", "Августа", "Сентября", "Октября",
    "Ноября", "Декабря"
];
s.MonthsDays = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
s.curYear = (new Date()).getFullYear();
s.toTitleCase = function (str) {
    if (!str) return str;
    else return str.charAt(0).toUpperCase() + str.substr(1).toLowerCase();
};
s.FormatDD = function (d) {
    if (!d) return '';
    if (typeof d == 'string')
        d = new Date(d);
    var dd = '' + d.getDate();
    var mm = '' + (d.getMonth() + 1);
    var yy = '' + (d.getYear() % 100);
    return dd.padStart(2, '0') + '.' + mm.padStart(2, '0') + '.' + yy.padStart(2, '0');
};
s.getMN = function (m) {
    var mn;
    if (isNaN(m)) {
        m = s.toTitleCase(m);
        mn = s.ukMonthsR.indexOf(m);
        if (mn == -1) {
            mn = s.ukMonths.indexOf(m);
            if (mn == -1) mn = s.ruMonthsR.indexOf(m);
            if (mn == -1) mn = s.ruMonths.indexOf(m);
        }
        mn += 1;
    }
    else
        mn = Number.parseInt(m);
    return mn;
}
s.checkBirthDate = function (p) {
    var mn = p.MonthNo;
    var d = p.BirthDD;
    var y = p.BirthYYYY;
    if (!d || !mn || !y)
        $('#BirthDate').removeClass('has-error');
    else {
        var md = s.MonthsDays[mn - 1];
        if (mn <= 0 || mn > 12 || d < 1 || d > md || y < 1850 || y >= s.curYear)
            $('#BirthDate').addClass('has-error');
        else {
            $('#BirthDate').removeClass('has-error');
            return true;
        }
    }
    return false;
};
s.onMonthChange = function (p) {
    p.error = '';
    var m = p.BirthMM;
    var mn = s.getMN(m);
    var mm = '';
    if (mn > 0) {
        mm = s.ukMonthsR[mn - 1];
        if (mm != m && (m.length > 1 || mn > 1))
            p.BirthMM = mm;
        p.error = '';
        p.MonthNo = mn;
    }
    if (mm) s.checkBirthDate(p);
    else $('#BirthDate').addClass('has-error');
};
s.wrapBirthDate = function (p) {
    var m = p.BirthMM;
    var d = p.BirthDD;
    var y = p.BirthYYYY;
    p.error = '';
    if (!m || !d || !y) {
        $('#BirthDate').addClass('has-error');
        p.error = 'Необхідно вказати дату народження!';
        return;
    }
    var mn = s.getMN(m);
    p.MonthNo = mn;
    var md = s.MonthsDays[mn - 1];
    if (!s.checkBirthDate(p)) {
        p.error = 'Необхідно вказати коректну народження!';
        return;
    }
    p.BirthDate = '' + y + '-' + ('' + mn).padStart(2, '0') + '-' + ('' + d).padStart(2, '0');
};
s.parseDate = function (p) {
    var bd = p.BirthDate;
    if (!bd) bd = p.birthday;
    if (!bd) {
        p.BirthMM = null;
        p.MonthNo = null;
        p.BirthDD = null;
        p.BirthYYYY = null;
    }
    else {
        var d = new Date(bd);
        p.MonthNo = d.getMonth();
        p.BirthMM = s.ukMonthsR[p.MonthNo];
        p.BirthDD = d.getDate();
        p.BirthYYYY = d.getFullYear();
    }
};

s.compare = function (o1, o2, attr) {
    if (!o1) { if (o2) return -1; else return 0; };
    if (!o2) { return 1; };
    var v1 = o1[attr];
    var v2 = o2[attr];
    if (v1 > v2) return 1;
    if (v1 == v2) return 0;
    return -1;
};
s.join_str = function (s1, s2, s3) {
    var r = '';
    if (s1) r += ('' + s1).trim();
    if (s2) r += ' ' + ('' + s2).trim();
    if (s3) r += ' ' + ('' + s3).trim();
    r = r.trimLeft();
    return r;
};