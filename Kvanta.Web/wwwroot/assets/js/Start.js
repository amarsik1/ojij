﻿function refreshUI() {
    if (_user.isAuth) {
        $(".loginMenuItem").hide();
        $(".registerMenuItem").hide();
        $(".logoutMenuItem").show();
        $(".userNameMenuItem a").text("Вітаємо " + _user.userName);
        $(".userNameMenuItem").show();
    } else {
        var now = new Date();
        var day = ("0" + now.getDate()).slice(-2);
        var month = ("0" + (now.getMonth() + 1)).slice(-2);
        var today = now.getFullYear() + "-" + (month) + "-" + (day);
        $("#birthday").val(today);
        $(".loginMenuItem").show();
        $(".registerMenuItem").show();
        $(".logoutMenuItem").hide();
        $(".userNameMenuItem").hide();
    }
}
function login(e) {
    //if (_user.isAuth) return;
    var l = $("#loginText").val();
    var pwd = $("#passwordText").val();
    if (!l || !pwd) return;
    app.Login(l, pwd, function () {
        $("#passwordText").val('');
        location.pathname = "/Cabinet"
        refreshUI();
    });
	if (e) e.preventDefault();
	return false;
}
function logOut() {
    if (!_user.isAuth) return;
    app.Logout();
    refreshUI();
}
function doRegister() {
    $(".alert-danger").hide();
    var d = {
        WebAccountId: 0,
        FirstName: $("#regName").val(),
        Middle_name: $("#regMiddleName").val(),
        Surname: $("#regLastName").val(),
        Email: $("#regEmail").val(),
        PhoneNo: $("#regPhoneNumber").val(),
        BirthDate: $("#regBirthday").val(),
        GenderRB: $('input[name=GenderRB]:checked').val(),
        password: $("#userPassword").val(),
        ConfirmPassword: $("#confirmPassword").val()
    };
    if (!d.BirthDate) {
        $(".alert-danger").html('Необхідно вказати дату народження!');
        $(".alert-danger").show();
        return;
    }
    if (d.password != d.ConfirmPassword) {
        $(".alert-danger").html('Паролі не співпадають!');
        $(".alert-danger").show();
        return;
    }
    app.Register(d, function (resp) {
        if (resp) {
            if (resp.Full_Name) {
                $("#register").hide();
                $("#reg_info").show();
                app.Login(d.Email, d.password, function () {
                    /*location.reload();*/
                });
            }
            else if (resp.Message) {
                $(".alert-danger").html(resp.Message);
                $(".alert-danger").show();
            }
        }
    });
}
$(window).on('load', function () {
	setTimeout(function () {
		document.querySelectorAll('input:-webkit-autofill')
			.forEach(function (x) {
				$(x).parent().find("label").addClass('active');
			});
	}, 100);

});
app.OnUserUpdated = refreshUI;

app.ShowError = function (pText) {
        var lAlertDiv = $("#GPageBody .alert-danger");
		if (!lAlertDiv || !lAlertDiv.length)
        lAlertDiv = $(".alert-danger");
        if (lAlertDiv && lAlertDiv.length) {
            if (!pText)
                lAlertDiv.hide();
            else {
                _curAlert = lAlertDiv;
                lAlertDiv.html(pText);
                lAlertDiv.show();
            }
        }
            
    };