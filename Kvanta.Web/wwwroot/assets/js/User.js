﻿GPage.AddDriver("ProfileDrv", function () {
    var t = this;
    t.Init = function () {
        return app.Me;
    };
    t.DoChangePass = function (ev) {
        var lPass = $("#newPassword").val();
        var lErr = null;
        if (!lPass)
            lErr = "Не вказано новий пароль!";
        else if (lPass.length < 4)
            lErr = "Новий пароль надто короткий!";
        else if (lPass != $("#confirmationPassword").val())
            lErr = "Пароль і підтвердження не співпадають!";
        if (lErr)
            app.ShowError(lErr);
        else {
            var d = $("#ChangePassModal Form").serializeObject();
            d.userID = _user.id;
            app.GPost("Cabinet", "ChangePassword", d, function (resp) {
                if (resp && resp.Code && resp.Code > 200)
                    app.ShowError(resp.Message);
                else {
                    GPage.CloseModal();
                    setTimeout(function () {
                        GPage.ShowMessage(ev, "Пароль змінено.")
                    }, 200);
                }
            });
        }
    };
});