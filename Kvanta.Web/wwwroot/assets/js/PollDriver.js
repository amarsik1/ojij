﻿GPage.AddDriver("PollDrv", function () {
    var t = this;
    t.Init = function () {
        app.JSonp("Poll/GetList", { userID: _user.id }, function (resp) {
            t.SetPageH("Poll", resp);
            /*setTimeout(function () {
            }, 100)*/
        });
        return false;
    };
    t.SetPage = function (pName, pParams) {
        var lPath = pName ? pName : "GetTemplate";
        var d = pParams;
        if (!d) d = {};
        if (d)
            d.userID = _user.id;
        var _drv = app.PollDrv;
        _drv.DesignMode = d.DesignMode;
        if (d.QTid) _drv.QTid = d.QTid;
        if (d.DesignMode == 1 && !_drv.DLoaded) {
            $.getScript("/assets/js/PollDesigner.js", function () {
                if (_drv.OnSetPage && GPage.CurDriver == _drv)
                    _drv.OnSetPage();
            });
            _drv.DLoaded = 1;
        }

        app.JSonp("Poll/" + lPath, d, function (resp) {
            t.SetPageH("Poll", resp, true);
        });
        return false;
    }
});
