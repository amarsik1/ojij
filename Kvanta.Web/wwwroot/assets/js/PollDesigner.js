﻿
function __start() {
    console.log("Designer inited");
    var _drv = app.PollDrv;
    var lTB = null;
    var _qItemsInit = function (pItems) {
        if (pItems.length && lTB) {
            pItems.hover(function (ev) {
                var _q = this;
                var _id = _q.getAttribute("q-id");
                _drv.QID = _id;
                if (lTB.parent != _q)
                    _q.insertBefore(lTB, _q.firstChild);
                $(lTB).show();
            }, function () { $(lTB).hide(); });
        }
    };
    _drv.OnSetPage = function () {
        var lItems = $("#GPageBody .q-item");
        lTB = $e("designToolBar");
        $(lTB).hide();
        _qItemsInit(lItems);
    };

    var _setQKindsModal = function (pHtml) {
        var m = $("#QKindsModal");
        m.html(pHtml);
        m.attr("_Inited", 1);
        $("#QKindsModal li").click(function () {
            var lKid = this.getAttribute("k-id");
            var _qid = _drv.QID;
            if (_drv.DesignMode && _qid)
                app.JSonp("Poll/SetQKind", { QTid: _drv.QTid, QID: _qid, QKind: lKid },
                    function (resp) {
                        var _sel = ".q-item[q-id=" + _qid + "]";
                        var lItem = $(_sel);
                        if (lItem.length && typeof resp == "string")
                        {
                            lItem.replaceWith(resp);
                            m.modal('close');
                            var _d = $(_sel);
                            _qItemsInit(_d);
                            _d.find('select').material_select();
                        }
                        else
                            console.log(resp);
                    });
        });
        m.modal('open');
    };

    _drv.SelectQKind = function () {
        if (!_drv.QKindsLoaded)
            app.JSonp("Poll/GetQKindsForSelect", { QTid: _drv.QTid, DesignMode: _drv.DesignMode },
                function (resp) {
                    if (typeof (resp) == "string") {
                        _drv.QKindsModalHtml = resp;
                        _setQKindsModal(resp);
                        _drv.QKindsLoaded = 1;
                    }
                });
        else {
            var m = $("#QKindsModal");
            if (m.attr("_Inited") == 1)
                m.modal('open');
            else
                _setQKindsModal(_drv.QKindsModalHtml);
        }
    }
};
__start();
console.log("Designer loaded");