﻿function $e(id) {
    return document.getElementById(id);
}

function formToJSon(frm) {
    var fData = {};
    var arr = $(frm).serializeArray();
    $.map(arr, function (n, i) {
        fData[n['name']] = n['value'];
    });
    return fData;
};
    var _phoneNo = function (pNum) {
        if (!pNum) return '';
        return pNum.replace('-', '').replace(' ', '').replace('(', '').replace(')', '');
    }

_user = {
    isAuth: false,
    id: 0,
    userName: "",
    provider: "",
    Reset: function () {
        _user.isAuth = false;
        _user.id = 0;
        _user.userName = "";
        _user.token = "";
        _user.provider = "";
    }
}
app = function () {
    var _token = "";
    var _HHead = {};
    var _initHHead = function () {
        if (_token)
            _HHead.Authorization = "Bearer " + _token;
        else
            _HHead.Authorization = "";
    };
    var _onUserUpd = function () {
        if (typeof app.OnUserUpdated == 'function')
            app.OnUserUpdated();
    };
    var _onError = function (pResp, pUrl) {
        var m = "";//"error from " + pUrl;
        if (pResp) {
			if (pResp.responseJSON && pResp.responseJSON.error_description)
				m = pResp.responseJSON.error_description;
			else if (pResp.Message) m += pResp.Message;
            else if (pResp.responseText) m += pResp.responseText;
            if (pResp.status && pResp.status != 400)
                m += " (Код: " + pResp.status + ")";
        }
		if (app.ShowError)
			app.ShowError(m);
		else
			console.error(m);
        if (pResp.status == 401)
            app.Logout();
    };
    var _jsonp = function (pUrl, pData, pCallback, pOnError) {
        $.ajax({
            type: 'POST',
            url: pUrl,
            data: JSON.stringify(pData), // or JSON.stringify ({name: 'jonas'}),
            headers: _HHead,
            contentType: "application/json",
            success: function (responce) {
                if (pCallback) pCallback(responce);
            }, error: function (resp) {
                if (pOnError) pOnError(resp);
                else _onError(resp, pUrl);
            }
        });
    }
    var _GPost = function (module, command, data, pCallback, pOnError) {
        _jsonp("/gapi/" + module + "/" + command, data, pCallback, pOnError);
    };
    var _init = function () {
        var str = localStorage.getItem("UserInfo");
        var _t = localStorage.getItem("U_Token");
        if (typeof _t == "string")
            _token = _t;
        else
            _token = null;

        if (str) {
            var user = JSON.parse(str);
            _user.provider = user.provider || "local";
            _user.userName = user.userName;
            _user.id = user.id;
            _user.isAuth = !!_token && !!user.id;
        }
        _initHHead();
        _onUserUpd();
        if (_user.isAuth) {
            _GPost("Cabinet", "GetMe", { userID: _user.id }, function (resp) {
                var d = resp;
                if (d && d.length) {
                    app.Me = d[0];
                    if (app.parseDate)
                        app.parseDate(app.Me);
                }
                app._Inited = true;
            });
        }
    };
    var _onAuth = function () {
        _user.isAuth = !!_token;
        localStorage.setItem("UserInfo", JSON.stringify(_user));
        localStorage.setItem("U_Token", _token);
        _initHHead();
    };
    var _onTelegramAuth = function (user) {
        _user.id = user.id;
        _user.userName = user.first_name + ' ' + user.last_name;
        _token = user.hash;
        _user.provider = user.provider = "telegram";
        _onAuth();
        _onUserUpd();
    };
    var _login = function (pLogin, password, callbabk) {
        if (_user.isAuth || !pLogin || !password) {
            if (callbabk)
                callbabk(_user.userName);
            return;
        }
		$.post({
        //$.ajax({
            url: location.origin+'/Token',
            contentType: 'application/x-www-form-urlencoded',
            async: true,
            data: "userName=" + encodeURIComponent(pLogin) + "&password=" + encodeURIComponent(password) + "&grant_type=password",
            success: function (rData) {
                if (rData && rData.access_token) {
                    localStorage.setItem("LastLoginName", pLogin);
                    _user.provider = rData.provider = "local";
                    _user.userName = rData.userName;
                    _user.id = Number(rData.webAccountId);
                    _token = rData.access_token;
                    _onAuth();
                    callbabk(rData.userName);
                }
                else
                    callbabk(null);
            },
			error: function (resp) {
                _onError(resp);
            }
        });
    };
    var _logout = function () {
        _user.Reset();
        localStorage.removeItem("UserInfo");
        localStorage.removeItem("U_Token");
        if (location.pathname && location.pathname.length > 1)
            location.href = location.origin;
        else
            _onUserUpd();
    }
    var _register = function (data, callback) {
        $.ajax({
            type: "POST",
            url: "/gapi/User/Register",
            contentType: 'application/json',
            async: true,
            data: JSON.stringify(data),
            success: function (resp) {
                callback(resp);
            },
            error: function (err) {
                callback(err);
            }
        });
    }
    return {
        Init: _init,
        Login: _login,
        Logout: _logout,
        Register: _register,
        OnTelegramAuth: _onTelegramAuth,
        GPost: _GPost,
        JSonp: _jsonp,
        LastLoginName: function () {
            return localStorage.getItem("LastLoginName");
        },
        OnError: _onError
    }
}();
app.Init();
