﻿'use strict';

if (typeof app !== 'undefined') {
    GPage.AddDriver("ContactsBox",
        function () {
            var _newGroupID = -11;
            function _Group(pObj, gid, pState) {
                var r = {name:''};
                if (pObj) {
                    if (pObj.Name) {
                        r.name = pObj.Name;
                        $.extend(r, pObj);
                    }
                    else if (typeof pObj == "string")
                        r.name = pObj;
                }
                if (gid)
                    r.gid = gid;
                else if (!r.gid)
                    r.gid = --_newGroupID;
                if (pState)
                    r._RowState_ = pState;
                return r;
            }


            var s = this;
            var _CurContact = null;
        s.Items = [];
        s.SelectedGroupItems = [];//TTY
        s.Groups = [
            _Group("Усі", -2),
            _Group("Без групи", -1)
        ]; //TTY
        s._GroupsMap = {};
        s.SelectedGroupGid = -2; // TTY
        s._byPhone = {};
        s._byEmail = {};
        s._byID = {};
        s._new = { gid: -1 };
        s._edit = {};
        s.ReadTime = null;
        s.hasItems = false;
        s.hasExtContacts = false;
        s.ExtContacts = [];
        s.FilterStr = "";

        s.NeedRead = function () {
            return !s.ReadTime || (Date.now() - s.ReadTime) < 300000;
        };
        s.Read = function () {
            var uInfo = _user;
            if (uInfo) {
                s.UserId = uInfo.id;//webAccountId
                s.ThePersonID = uInfo.PersonID;
            }
            else {
                s.UserId = 0;
                s.ReadTime = null;
                return;
            }
            app.GPost("Cabinet", "GetContacts", { userID: s.UserId }, function (response) {
                s.ReadTime = Date.now();
                s.SetData(response);
                resetSelectedGroupItems();  // TTY
                if (GPage.CurPage == "Contacts")
                    s.SetPage2("Contacts", s);
                updateSelectedGroupHtml(s.SelectedGroupGid); //TTY

            });
        };
        s.ui4New = { Caption: 'Реєстрація нового контакту', OkBtn: 'Зберегти', mode: 'New', readOnly: false };
        s.ui4View = { Caption: 'Дані контакту', OkBtn: 'Редагувати', mode: 'View', readOnly: true };
        s.ui4Edit = { Caption: 'Редагування контакту', OkBtn: 'Зберегти', mode: 'Edit', readOnly: false };

        s.HasItems = function () {
            return s.Items && s.Items.length;
        };
        s.Init = function () {
            if (s.ReadTime && s.ReadTime > Date.now() - 300000)
                return s;
            else {
                s.Read();
                return false;
            }
        };
        s.CoreGroupID = 0;
        s.SetData = function (pData) {
            s.hasItems = false;
            if (!pData) { s.Items = []; }
            else if (pData.ContactsList) {
                s.Items = pData.ContactsList;
                if (s.Items.length) s.hasItems = true;
            }
            else if (pData && pData.length) {
                s.Items = pData;
                s.hasItems = true;
            }
            else { s.Items = []; }

            var lByPhone = {};
            var lByMail = {};
            var lByID = {};
            var sc = app;
            s._byPhone = lByPhone;
            s._byEmail = lByMail;
            s._byID = lByID;
            s.Items.forEach(function (m) {
                if (m) {
                    var l_e_mail = '';
                    if (m.e_mail) {
                        l_e_mail = m.e_mail.toLowerCase();
                        lByMail[l_e_mail] = m;
                    }
                    if (m.gid) lByID['' + m.gid] = m;
                    m.f_name = sc.join_str(m.Surname, m.Name, m.Middle_name);
                    m.s_str = m.f_name.toLowerCase() + ' ' + l_e_mail;
                    if (m.phones) { lByPhone[_phoneNo(m.phones)] = m; m.s_str += ' ' + m.phones }
                }
            });
            s.Items.sort((a, b) => sc.compare(a, b, 'f_name'));
            var lGroups = pData.ContactGroups;
            s.Groups.splice(2, s.Groups.length);
            lGroups.forEach(function (g) {
                if (g && g.gid) {
                    if (g.Name == '#')
                        s.CoreGroupID = g.gid;
                    else {
                        var _gr = _Group(g);
                        s.Groups.push(_gr);
                        s._GroupsMap[_gr.gid] = _gr;
                    }
                }
            });
            var lGr_tp = pData.ContactsGroups_tp;
            if (Array.isArray(lGr_tp)) {
                lGr_tp.forEach(function (t) {
                    if (t.PersonsGroup != s.CoreGroupID) {
                        var p = t.Person;
                        var o = lByID[p];
                        if (o) {
                            if (!o.groups)
                                o.groups = [t.PersonsGroup];
                            else o.groups.push(t.PersonsGroup);
                        }
                    }
                });
            }

        };
        s.FilteredList = function () {
            if (!s.FilterStr)
                return s.Items;
            var r = [];
            var fs = s.FilterStr.toLowerCase();
            s.Items.forEach(function (m) {
                if (m) {
                    if (m.s_str.includes(fs))
                        r.push(m);
                }
            });
            return r;
        };

        var editController = {
            validator: s.validate, callback: s.afterEdit
        };
        function contactController(pData, pUI) {
            var _o = this;
            var _d = pData;
            var _ui = pUI;
            var _isNew = !_d.gid || _d.gid < 0;
            this.data = _d;
            this.ui = _ui;
            var _editedGroups = [];
            this.OnShow = function () {
                var lGroups = s.Groups.filter(function (item) { return item.gid > -1; });
                var _cGroups = Array.isArray(_d.groups) ? _d.groups : [];
                var jInput = $('#inputGroup');
                _CurContact = _o;
                jInput.select2({
                    data: lGroups.map(function (item) {
                        return {
                            id: item.gid,
                            text: item.name,
                            selected: _cGroups.indexOf("" + item.gid) > -1
                        }
                    })
                });
                
                jInput.on('select2:select', function (e) {
                    var _gr = e.params.data;
                    if (_gr && _gr.id) {
                        var _obj = _editedGroups.find(function (x) { x.PersonsGroup == _gr.id });
                        if (_obj) {
                            _obj._RowState_ = _cGroups.indexOf(el.gid) >= 0 ? 3 : 2;
                            if (!_obj.ordinal) _obj.ordinal = _cGroups.length + _editedGroups.length;
                        }
                        else
                            _editedGroups.push({ PersonsGroup: _gr.id, Person: _d.gid, ordinal: _cGroups.length + _editedGroups.length + 1, _RowState_: 2 });
                    }
                });
                jInput.on('select2:unselect', function (e) {
                    var _gr = e.params.data;
                    if (_gr && _gr.id) {
                        var _obj = _editedGroups.find(function (x) { x.PersonsGroup == _gr.id });
                        if (_obj) {
                            if (_obj._RowState_ != 4) _obj._RowState_ = 4;
                        }
                        else
                            _editedGroups.push({ PersonsGroup: _gr.id, Person: _d.gid, _RowState_: 4 });
                    }
                });
            };
            this.StartEdit = function (ev) {
                if (_d) {
                    _d.ReadOnly = false;
                    GPage.CheckEdit(ev.data.closest("form,.modal"), _d);
                }
            };
            this.CancelEdit = function (ev) {
                s.reset(_d.gid);
                GPage.CloseModal();
            };
            var _SData = null;
            function _onSave() {
                if (_editedGroups && _editedGroups.length) {
                    var _groups = _editedGroups;
                    if (_isNew) _groups.forEach(function (o) { o.Person = _SData.gid; });
                    _editedGroups = [];
                    app.GPost("Cabinet", "SaveGroups_tp", _groups, function (r) {
                        if (r.Code != 101)
                            console.log("Щось не так.");
                    });
                }
                _d = _SData;
                _isNew = false;
            }
            this.SaveContact = function (ev) {
                var lForm = $("#ContactDialog");
                _SData = lForm.serializeObject();
                var lId = lForm.attr("PersonID");
                if (_d.gid && lId != _d.gid) {
                    console.log("Incorrect person ID for save: " + lId + " expexted: " + _d.gid);
                    return;
                }
                if (lId)
                    _SData.gid = lId;
                else
                    _SData.gid = -1;

                if (!Array.isArray(_SData.groups))
                    _SData.groups = [_SData.groups];
                s.postContact(ev, _SData, _onSave);
            };
        };
        contactController.prototype = editController;

        s.forEdit = function (pId) {
            if (!pId) return null;
            var m, d, ui;
            if (pId < 0) {
                d = s._new;
                ui = s.ui4New;
                d.CanEdit = 1;
                d.ReadOnly = false;
                d.Net = "";
            }
            else {
                m = s._edit[pId];
                if (m) return m;
                d = s._byID[pId];
                ui = s.ui4View;
                if (!d)
                    d = { gid: -1 };
                else {
                    d = $.extend({}, d);
                }
                d.CanEdit = (!d.AuthorPerson || d.AuthorPerson == s.ThePersonID);
                d.ReadOnly = true;
            }

            app.parseDate(d);
            m = new contactController(d, ui);
            s._edit[pId] = m;
            return m;
        };

        // TTY
        function resetSelectedGroupItems() {
            s.SelectedGroupItems = [];
            s.Items.forEach(item => {
                if (s.SelectedGroupGid == -2) {
                    s.SelectedGroupItems.push(item);
                } else if ((!item.groups || !item.groups.length) && s.SelectedGroupGid == -1) {
                    s.SelectedGroupItems.push(item);
                } else if (Array.isArray(item.groups) && item.groups.indexOf("" + s.SelectedGroupGid) > -1) {
                    s.SelectedGroupItems.push(item)
                }
            })
        }
        function updateSelectedGroupHtml(gid) {
            const el = s.getElementByGid(gid);
            if (el) el.parentElement.classList.add("selected")
        }
        app.SaveContact = function () {
            if (_CurContact)
                _CurContact.SaveContact();
        };
        app.ViewGroups = function (div_id) {
            $('#' + div_id).toggleClass('open')
        }
        app.SelectContactGroup = function (gid) {
            if(gid == s.SelectedGroupGid) return
            s.SelectedGroupGid = gid;
            resetSelectedGroupItems();

            GPage.ResetHtml("Contacts");
            updateSelectedGroupHtml(gid)
        }
        app.CreateContactGroup = function () {
            if (s._currentEditableGroup) {
                app.FinishContentEdtable(s._currentEditableGroup.gid);
            }

            const group = _Group("Нова група", 0, 2);
            var _id = group.gid;
            s.Groups.push(group);
            s._GroupsMap[_id] = group;
            s.SelectedGroupGid = _id;
            resetSelectedGroupItems();
            GPage.ResetHtml("Contacts");
            updateSelectedGroupHtml(_id);
            const el = s.getElementByGid(_id);
            if (el) {
                app.StartContentEditable(el);
                s._currentEditableGroup = group;
                s.SelectedGroupGid = _id;
            }
        }
        app.EditSelectedGroup = function(){
            const el = s.getElementByGid(s.SelectedGroupGid) 
            app.StartContentEditable(el) 
        }
        app.DelSelectedGroup = function(){
            if(s.SelectedGroupGid==-1||s.SelectedGroupGid==-2) return
            s.Groups = s.Groups.filter(function(g){
                return g.gid != s.SelectedGroupGid
            })
            s.SelectedGroupGid = -2;
            resetSelectedGroupItems();
            GPage.ResetHtml("Contacts");
            updateSelectedGroupHtml(s.SelectedGroupGid);
        }
        app.StartContentEditable = function (el) {
            el.setAttribute("contenteditable", true);
            el.parentElement.classList.add("editable")
        }
        app.FinishContentEdtable = function (gid) {
            const el = s.getElementByGid(gid)
            if (!el) return;
            el.setAttribute("contenteditable", false);
            el.parentElement.classList.remove("editable")
            s.SaveContactGroup(el);
        }
        s.SaveContactGroup = function (htmlField) {
            const data = s._GroupsMap[s.SelectedGroupGid];
            if (!data) return;
            var _id = data.gid;
            var lName = htmlField.innerHTML;
            if (!data._RowState_) {
                if (data.gid < -2)
                    data._RowState_ = 2;
                else if (data.Name != lName)
                    data._RowState_ = 3;
            }
            if (data._RowState_ == 1 && data.Name != lName)
                data._RowState_ = 3;

            data.name = lName;
            data.Name = lName;
            app.GPost("Cabinet", "SaveContactGroup", data, function (resp) {
                if (resp.IDs) {
                    var lIDs = JSON.parse(resp.IDs);
                    if (lIDs && lIDs.length) {
                        data.gid = lIDs[0];
                        data._RowState_ = 1;
                        s._GroupsMap[data.gid] = data;
                        if (_id != data.gid)
                            delete s._GroupsMap[_id];
                    }
                    if (s.SelectedGroupGid == _id)
                        s.SelectedGroupGid = data.gid;
                    resetSelectedGroupItems();
                    htmlField.dataset.gid = data.gid;
                    updateSelectedGroupHtml(s.SelectedGroupGid);
                }
            });

            
        }
        s.getGroupByGid = function (gid) {
            var group = s._GroupsMap[gid];
            return group
        }
        s.getElementByGid = function (gid) {
            const els = document.querySelectorAll('[data-gid="' + gid + '"]')
            if (els.length) return els[0]
            return null
        }
        //tty

        s.editContact = function (ev, pId) {
            if (!ev || !ev.handled)
                s.MainScope.ShowDialog(ev, 'tplt/ContactDL', s.forEdit(pId));
        };
        s.AddContact = function (e) {
            // TTY
            if (s._currentEditableGroup) {
                app.FinishContentEdtable(s._currentEditableGroup.gid);
            }
            // tty
            GPage.OpenDlg('Contact', s.forEdit(-1));
            if (e) e.preventDefault();
            return false;
        };
        app.EditContact = function (e, id) {
            GPage.OpenDlg('Contact', s.forEdit(id));

            if (e) e.preventDefault();
            return false;
        };
        s.sendInvitation = function (ev, pData) {
            if (!ev || !ev.handled) {
                var sc = s.MainScope;
                var d = pData;
                if (!d || !d.ContactID)
                    return;
                if (!d.Subject) {
                    sc.ShowMessage(ev, 'Необхідно вказати тему листа!');
                    return;
                }
                if (!d.MBody) {
                    sc.ShowMessage(ev, 'Необхідно вказати тіло листа!');
                    return;
                }
                if (!d.MBody.includes("{7KPartner}")) {
                    sc.ShowDialog(ev, 'Тіло листа повинно містити позицію для реферального запрошення: {7KPartner}!');
                    return;
                }
                var c = s._byID[d.ContactID];
                d.ReceiverAddress = c.e_mail;
                d.ReceiverName = c.f_name;
                var ds = s.DataSrv;
                ds.GPost("Cabinet", "InviteContact", d).then(function (response) {
                    s.MainScope.ShowMessage(ev, 'Запрошення надіслано!');
                });
            }
        };
        s.inviteDialog = function (ev, pId) {
            var sc = app;
            var id = pId;
            if (!id) return;
            var c = s._byID[id];
            if (!c) return;
            if (!c.e_mail) {
                sc.ShowMessage(ev, 'Необхідно вказати адресу електронної пошти!');
                return;
            }
            var l = {
                ContactID: id,
                Subject: "" + sc.join_str(c.Name, c.Middle_name) + "! Запрошую до партнерства!",
                MBody: "Вітаю, " + sc.join_str(c.Name, c.Middle_name) + "!\nЗапрошую приєднатись до моїх партнерів за посиланням:\n {7KPartner}" +
                    "\n\nЗ повагою, " + (sc.User.userName) + "."
                //MBody: "Заходьте до нас: <a href='https://7kpartner.atlas-crm.info/wellcome?iid=" + id + "'> 7KPartner </a>"
            };
            var lInv = {
                ui: { Caption: 'Запрошення партнера', OkBtn: 'Надіслати' },
                callback: s.sendInvitation,
                data: l
            };
            sc.ShowDialog(ev, 'tplt/MailLetterDL', lInv);
        };
        s.afterEdit = function (ev, pData) {
            var d = pData;
            if (ev && d && d.gid)
                s.postContact(ev, d);
            else if (pData.startsWith('#invite:'))
                s.inviteDialog(ev, parseInt(pData.substr('#invite:'.length)));
            else {
                var id = parseInt(pData);
                if (id)
                    s.reset(id);
            }
        };
        
        s.postContact = function (ev, d, pOnOk) {
            var sc = app;
            var lId = d.gid;
            d.userID = s.UserId;
            sc.wrapBirthDate(d);
            d.birthday = d.BirthDate;
            d.f_name = sc.join_str(d.Surname, d.Name, d.Middle_name);
            if (lId && lId > 0) {
                app.GPost("Cabinet", "UpdateContact", d, function (response) {
                    var lData = response;
                    if (lData) {
                        var lResCode = lData.Code;
                        if (lResCode == 100 || lResCode == 101) {
                            var l = s._byID[lId];
                            if (l) {
                                $.extend(l, d);
                                if (l.phones) s._byPhone[_phoneNo(l.phones)] = l;
                                if (l.e_mail) s._byEmail[l.e_mail.toLowerCase()] = l;
                            }
                            if (pOnOk) pOnOk();
                            s.reset(lId);
                            // TTY
                            resetSelectedGroupItems();
                            // TTY
                            GPage.CloseModal();
                            if (pOnOk) pOnOk();
                            GPage.ResetHtml("Contacts");
                            GPage.ShowMessage(ev, 'Контакт успішно оновлено!');
                        }
                        else if (lData.Message) {
                            sc.ShowMessage(ev, lData.Message);
                        }
                    }
                });
            } else if (d.NetProfileId) {
                if (s._byPhone[d.phones] || s._byEmail[d.e_mail]) return;
                var contacts = [{
                    Name: d.Name,
                    Middle_name: d.Middle_name,
                    Surname: d.Surname,
                    e_mail: d.e_mail,
                    phones: d.phones,
                    comments: d.comments,//BirthDate: data[i].BirthDate,
                    genderStr: !d.gender || d.gender == 1 ? "male" : "female",
                    SocNet: d.Net,
                    SocNetProfileId: d.NetProfileId,
                    AuthorPerson: Number.parseInt(s.MainScope.UserInfo.PersonID),
                    // TTY
                    groups: d.groups
                }];
                app.GPost("Cabinet", "AddExtContacts", contacts, function (response) {
                    s.reset(lId);
                    var arr = response.data;
                    if (arr && arr.length) {
                        lId = arr[0].gid;
                        if (lId > 0) {
                            d.gid = lId;
                            s.Items.push(d);
                            s._byID[lId] = d;
                            if (d.phones) s._byPhone[_phoneNo(d.phones)] = d;
                            if (d.e_mail) s._byEmail[d.e_mail.toLowerCase()] = d;
                            GPage.CloseModal();
                            // TTY
                            resetSelectedGroupItems();
                            // TTY
                            GPage.ResetHtml("Contacts");
                            GPage.ShowMessage(ev, 'Новий контакт успішно збережено!');
                        } else
                            GPage.ShowMessage(ev, 'Помилка при збереженні нового контакта!');
                    } else
                        GPage.ShowMessage(ev, 'Помилка при збереженні нового контакта!');
                });
            } else {
                app.GPost("Cabinet", "AddContact", d, function (response) {
                    var lData = response;

                    if (lData) {
                        lId = lData.Person;
                        if (lId && lId > 0) {
                            d.gid = lId;
                            s.Items.push(d);
                            s._byID[lId] = d;
                            if (d.phones) s._byPhone[_phoneNo(d.phones)] = d;
                            if (d.e_mail) s._byEmail[d.e_mail.toLowerCase()] = d;
                            GPage.CloseModal();
                            if (pOnOk) pOnOk();
                            // TTY
                            resetSelectedGroupItems();
                            // TTY 
                            GPage.ResetHtml("Contacts");
                            s.reset(lId);
                            GPage.ShowMessage(ev, 'Новий контакт успішно збережено!');
                        }
                    }
                }, s.handleError);
            }
        };
        s.handleError = function (reason) {
            if (reason) {
                var sc = s.MainScope;
                if (reason.Message)
                    sc.ShowMessage(null, reason.Message, "Помилка!");
                else if (reason.Description)
                    sc.ShowMessage(null, reason.Description, "Помилка!");
                else if (typeof reason.data === 'string') {
                    sc.ShowMessage(null, reason.data, "Помилка!");
                }
            }
        };
        s.validate = function (pScope, pData) {
            if (pScope.ui && pScope.ui.readOnly && pScope.ui.mode == 'View') {
                pScope.ui = s.ui4Edit;
                pScope.data.ReadOnly = !pScope.data.CanEdit;
                pScope.dScope.ui = s.ui4Edit;
                return false;
            }
            if (!s.ValidateItem(pData, 'ctDLStatus'))
                return false;
            return true;
        };
        s.byPhone = function (phoneNo) {
            if (!phoneNo) return null;
            return s._byPhone[_phoneNo(phoneNo)];
        };
        s.byEmail = function (pEmail) {
            if (!pEmail) return null;
            return s._byEmail[pEmail.toLowerCase()];
        };
        s.getNew = function () { return s._new; };
        s.reset = function (id) {
            if (!id || id < 0) s._new = { gid: -1 };
            else
                delete s._edit[id];
        }
        s.Str = function (pItem) {
            var r = '';
            if (pItem.Name) r += pItem.Name;
            if (pItem.Surname) {
                if (r) r += ' ';
                r += pItem.Surname;
            }
            return r;
        };
        s.ValidateItem = function (pItem, pStatusDomID) {
            if (!pItem) return null;
            var l, t;
            if (pItem.phones) {
                l = s.byPhone(pItem.phones);
                if (l && l.gid != pItem.gid) {
                    t = $('#' + pStatusDomID);
                    if (t) t.text('Такий же номер телефону вказано для ' + s.Str(l));
                    return false;
                }
            }
            if (pItem.e_mail) {
                l = s.byEmail(pItem.e_mail);
                if (l && l.gid != pItem.gid) {
                    t = $('#' + pStatusDomID);
                    if (t) t.text('Така ж електронна адреса вказана для ' + s.Str(l));
                    return false;
                }
            }
            if (!pItem.Name) {
                t = $('#' + pStatusDomID);
                if (t) t.text("Необхідно вказати ім'я");
                return false;
            }
            return true;
        }

        s.BeforeExternalContactsSending = function (ev, data) {
            if (ev && data) {
                var netProvider = null;
                var resArray = [];
                var i;
                for (i = 0; i < data.length; i++) {
                    if (!data[i].checked) continue;
                    if (s._byPhone[data[i].phones] || s._byEmail[data[i].e_mail]) continue;
                    if (!netProvider)
                        netProvider = data[i].Net;
                    resArray.push({
                        gid: -1,
                        Surname: data[i].Surname,
                        Name: data[i].Name,
                        Middle_name: data[i].Middle_name,
                        f_name: app.join_str(data[i].Surname, data[i].Name, data[i].Middle_name),
                        e_mail: data[i].e_mail,
                        phones: data[i].phones,
                        dateBorn: data[i].dateBorn,
                        gender: data[i].gender,
                        comments: data[i].comments,
                        Net: data[i].Net,
                        NetProfileId: data[i].NetProfileId
                    });
                }
                if (resArray.length === 1) {
                    var vm = {
                        data: resArray[0],
                        validator: s.validate,
                        callback: s.afterEdit,
                        ui: s.ui4Edit,
                        MSC: s.MainScope
                    };
                    vm.data.gender = vm.data.gender === "male" ? "1" : "2";
                    s.MainScope.ShowDialog(ev, 'tplt/ContactDL', vm);
                } else if (resArray.length > 1) {
                    if (!netProvider) {
                        s.MainScope.ShowMessage(ev, "Невизначена соцмережа", "Помилка");
                        return null;
                    }
                    var contacts = [];
                    for (i = 0; i < resArray.length; i++) {
                        var c = {
                            Name: resArray[i].Name,
                            Middle_name: resArray[i].Middle_name,
                            Surname: resArray[i].Surname,
                            e_mail: resArray[i].e_mail,
                            phones: resArray[i].phones,
                            comments: resArray[i].comments,//BirthDate: data[i].BirthDate,
                            genderStr: resArray[i].gender,
                            SocNet: netProvider,
                            SocNetProfileId: resArray[i].NetProfileId,
                            AuthorPerson: Number.parseInt(s.MainScope.UserInfo.PersonID)
                        }
                        contacts.push(c);
                    }
                    s.DataSrv.GPost("Cabinet", "AddExtContacts", contacts).then(function (response) {
                        var arr = response.data;
                        if (arr && arr.length) {
                            var byEmail = {};
                            var byPhones = {};
                            var i;
                            for (i = 0; i < arr.length; i++) {
                                var o = arr[i];
                                byEmail[o.e_mail] = Number.parseInt(o.gid);
                                byPhones[o.phones] = Number.parseInt(o.gid);
                            }
                            for (i = 0; i < resArray.length; i++) {
                                var gid = 0;
                                var c = resArray[i];
                                if (c.e_mail) {
                                    if (byEmail[c.e_mail])
                                        gid = byEmail[c.e_mail];
                                }
                                else if (c.phones) {
                                    if (byPhones[c.phones])
                                        gid = byPhones[c.phones];
                                }
                                if (gid <= 0) continue;
                                c.gid = gid;
                                s._byID[gid] = c;
                                if (c.phones) s._byPhone[_phoneNo(c.phones)] = c;
                                if (c.e_mail) s._byEmail[c.e_mail.toLowerCase()] = c;
                                s.Items.push(c);
                            }
                            if (resArray.length == arr.length)
                                s.MainScope.ShowMessage(ev, 'Контакти успішно додано!');
                            else
                                s.MainScope.ShowMessage(ev, 'Не всі контакти вдалося додати');
                        } else
                            s.MainScope.ShowMessage(ev, 'Помилка при збереженні контактів!');
                    }, s.handleError);
                }
            }
        };
        s.beforeExternalContactsShow = function (extendData) {
            if (extendData)
                for (var i = 0; i < s.ExtContacts.length; i++)
                    angular.extend(s.ExtContacts[i], { checked: false });
            return {
                data: s.ExtContacts,
                callback: s.BeforeExternalContactsSending,
                ui: { Caption: 'Контакти ' + s.loginProvider, OkBtn: 'Додати', mode: 'New', readOnly: false, size: "lg" },
                MSC: s.MainScope
            }
        };
        s.GetFromExternal = function (ev, provider) {
            if (!ev || !ev.handled) {
                if (!provider) return;
                if (s.hasExtContacts) {
                    s.MainScope.ShowDialog(ev, 'tplt/ExternalContacts', s.beforeExternalContactsShow(false));
                } else {
                    authService.GetExternalContacts(provider).then(function (resp) {
                        if (resp.error) {
                            s.MainScope.ShowMessage(ev, resp.error);
                            return;
                        }
                        s.ExtContacts = resp;
                        s.hasExtContacts = s.ExtContacts.length > 0;
                        s.MainScope.ShowDialog(ev, 'tplt/ExternalContacts', s.beforeExternalContactsShow(true));
                    }, function (error) {
                        console.log(JSON.stringify(error));
                        s.MainScope.ShowMessage(ev, "Помилка!");
                    });
                }
            }
        };
        return s;
    });
}