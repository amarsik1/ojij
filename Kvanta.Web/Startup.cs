﻿using K7_Web.AtlasAPI;
using Microsoft.Owin;
using Microsoft.Owin.FileSystems;
using Microsoft.Owin.StaticFiles;
using Owin;
using System;
using System.IO;
using System.Web.Http;
using System.Web.Routing;

[assembly: OwinStartup(typeof(K7_Web.Startup))]
namespace K7_Web
{
    public class Startup
    {
       
        private static KvantaApplication appInstance = null;

        public void Configuration(IAppBuilder app)
        {
            if (appInstance != null)
            {
                return;
            }

            appInstance = new KvantaApplication();
            appInstance.Configure(app);

            //TODO: call appInstance.Dispose
            //app.RegisterForDisposal(appInstance);

            var dir = AppDomain.CurrentDomain.BaseDirectory;
            dir = Path.Combine(dir, "wwwroot");
            if (Directory.Exists(dir))
            {
                var physicalFileSystem = new PhysicalFileSystem(dir);
                var options = new FileServerOptions
                {
                    RequestPath = PathString.Empty,
                    EnableDefaultFiles = true,
                    FileSystem = physicalFileSystem,
                    StaticFileOptions =
                    {
                        FileSystem = physicalFileSystem,
                        ServeUnknownFileTypes = true
                    }
                };
                app.UseFileServer(options);
            }
            GlobalConfiguration.Configure(appInstance.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }
        
    }
}