import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { SettingsService } from '../../../core/settings/settings.service';
import { ApiClientService } from '../../../core/apiclient/apiclient.service';


@Component({
	selector: 'app-confirmed',
	templateUrl: './confirmed.component.html',
	styleUrls: ['./confirmed.component.scss']
})
export class ConfirmedComponent implements OnInit {

    confForm: FormGroup;
    email: string;
    isRunning: boolean;
    serverError: boolean;

	constructor(public settings: SettingsService, fb: FormBuilder, public apiclientservice: ApiClientService, private route: ActivatedRoute,
		private router: Router) {
        this.confForm = fb.group({
            'emailAddress': [null, Validators.required],
            'requestCode': [null, Validators.required]
        });
    }

    submitForm($ev, value: any) {
        if ($ev) {
            $ev.preventDefault();
        }
        if (!value) {
            value = this.confForm.value;
        }
        for (let c in this.confForm.controls) {
            this.confForm.controls[c].markAsTouched();
        }
        if (this.confForm.valid)
        {
            this.isRunning = true;
            this.serverError = false;
            this.apiclientservice.post('/bs/Account/ConfirmEmail', value).subscribe(x => {
                this.isRunning = false;
                window.location.href = '/#/login'
            }, error => {
                this.isRunning = false;
                this.serverError = true;
                });
        }
    }

	ngOnInit() {
		

        this.route
            .queryParamMap.subscribe(params => {
                this.confForm.controls['requestCode'].setValue(params.get('code') || '');
                this.confForm.controls['emailAddress'].setValue(params.get('email') || '');
                this.email = this.confForm.value.email;
                if (this.email && this.confForm.controls['requestCode']) {
                    this.submitForm(null, null);
                }
            }); 
    }

}
