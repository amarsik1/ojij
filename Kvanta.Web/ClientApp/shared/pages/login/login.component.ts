import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { SettingsService } from '../../../core/settings/settings.service';
import { ApiClientService } from '../../../core/apiclient/apiclient.service';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs';


@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    valForm: FormGroup;
    loginError: string;
    loading: boolean = true;
    loginErr: boolean;
    pageLoading: boolean = true;

    constructor(public settings: SettingsService, fb: FormBuilder, public apiclientservice: ApiClientService) {

        this.valForm = fb.group({
            //'email': [null, Validators.compose([Validators.required, CustomValidators.email])],
            'email': [null, Validators.compose([Validators.required])],
            //'email': [null, Validators.compose([Validators.required])],
            'password': [null, Validators.required],
            'remember': [null]
        });

    }

    submitForm($ev, value: any) {
        $ev.preventDefault();
        for (let c in this.valForm.controls) {
            this.valForm.controls[c].markAsTouched();
        }
        if (this.valForm.valid) {
            this.loading = true;
            this.loginErr = false;
            this.apiclientservice.login(this.valForm.value).subscribe(result => {
                
                console.log(result);
                if (!result.IsAuthenticated || !(result.IsAuthenticated.toLowerCase() === "true") || !result.Roles.length) 
                {
                    this.loginError = 'Wrong login/password';
                    this.loginErr = true;
                    return;
                }
                var roles = result.Roles.split(',');
                window.location.href = '/' + roles[0] + '/';
            },
                err => {
                    this.loginError = 'Wrong login/password';
                    this.loginErr = true;
                    this.loading = false;
                },
                () => {

                    //this.loading = false;
                }
            );

            //{
            //	
            //}
            //else {
            //	console.log('Error!');
            //}




            //console.log('Valid!');
            //console.log(value);
        }
    }

    ngOnInit() {

        this.apiclientservice.getAccountInfo().subscribe(result => {
            this.pageLoading = false;
            if (!result.isOK) {
                return;
            }
            var data = result.data; 
            if (!data.isAuthenticated || !data.isAuthenticated || !data.roles.length) {
                return;
            }
            //var roles = data.roles.split(',');
            window.location.href = '/' + data.roles[0] + '/';
        }, err => {
            this.pageLoading = false;
            this.loading = false;
            }, () => {
                this.pageLoading = false;
                this.loading = false;
            });
    }

}
