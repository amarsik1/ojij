import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ValidationComponent } from '../../forms/validation/validation.component';
import { group } from '@angular/animations';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { CustomValidators } from 'ng2-validation';
import { SettingsService } from '../../../core/settings/settings.service';
import { ApiClientService } from '../../../core/apiclient/apiclient.service';


@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

    valForm: FormGroup;
    passwordForm: FormGroup;
    eduGroups: any = [];
    regError: boolean;
    isRunning: boolean;
    reqGroupId: any;

    constructor(public settings: SettingsService, fb: FormBuilder, public apiClientService: ApiClientService, private route: ActivatedRoute,
        private router: Router) {

        let password = new FormControl('', Validators.compose([Validators.required, Validators.pattern('^[a-zA-Z0-9]{6,10}$')]));
        let certainPassword = new FormControl('', [Validators.required, CustomValidators.equalTo(password)]);

        this.passwordForm = fb.group({
            'password': password,
            'confirmPassword': certainPassword
        });


        this.valForm = fb.group({
            'email': [null, Validators.compose([Validators.required, CustomValidators.email])],
            'passwordGroup': this.passwordForm,
            'firstName': [null, Validators.required],
            'surname': [null, Validators.required],
            'eduGroup': ["", Validators.required],
            'phoneNo': [null, Validators.compose([Validators.required, Validators.pattern('^[0-9]{10,13}$')])]
        });
    }

    submitForm($ev, value: any) {
        $ev.preventDefault();
        for (let c in this.valForm.controls) {
            this.valForm.controls[c].markAsTouched();
        }
        for (let c in this.passwordForm.controls) {
            this.passwordForm.controls[c].markAsTouched();
        }


        if (this.valForm.valid) {

            this.regError = false;
            this.isRunning = true;

            var rq = this.valForm.value as any;
            rq.password = rq.passwordGroup.password;
            rq.inviteCode = rq.eduGroup;
            //delete (rq.eduGroup);
            //delete (rq.passwordGroup)
            console.log(rq);

            this.apiClientService.register(rq).subscribe(result => {
                if (result) {


                    window.location.href = '/#/confirmed?email=' + rq.email;
                }
                else {
                    console.log('Error!');
                }
            }, errer => {
                this.isRunning = false;
                this.regError = true;
            });
        }

    }

    ngOnInit() {
        this.route
            .queryParamMap.subscribe(params => {
                this.reqGroupId = params.get('InviteCode') || '';
                this.valForm.controls['eduGroup'].setValue(this.reqGroupId);
            });

        this.apiClientService.get('/bs/AppRoot/StartInfo').subscribe(data => {

            this.eduGroups = data.publicInvitations;
			this.valForm.controls['eduGroup'].setValue(this.reqGroupId);
			console.log(this.eduGroups);
        });


    }

}