import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';

import { UserblockService } from './userblock.service';
import { Subscription } from 'rxjs';
import { IUserInfo } from '@model/userInfo';

@Component({
    selector: 'app-userblock',
    templateUrl: './userblock.component.html',
    styleUrls: ['./userblock.component.scss']
})
export class UserblockComponent implements OnInit {
    user: IUserInfo;
    subscription: Subscription;
    constructor(public userblockService: UserblockService, private change: ChangeDetectorRef) {

        //TODO: fix
        this.subscription = this.userblockService.userLoadedSource.subscribe(u => {
            if (u) {
                this.user = u;
                this.change.markForCheck();
            }
        });
    }

    ngOnInit()
    {
        

    }

    userBlockIsVisible() {
        return this.userblockService.getVisibility();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

}
