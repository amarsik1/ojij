import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject } from 'rxjs';
import { IUserInfo } from '../../../model/userInfo';

@Injectable()
export class UserblockService {


    userLoadedSource: BehaviorSubject<IUserInfo | null> = new BehaviorSubject<IUserInfo | null>(null);
    
    setUser(usr: IUserInfo) {
        this.userLoadedSource.next(usr);
    }

    public userBlockVisible: boolean;
    constructor() {
        // initially visible
        this.userBlockVisible = true;
    }

    getVisibility() {
        return this.userBlockVisible;
    }

    setVisibility(stat = true) {
        this.userBlockVisible = stat;
    }

    toggleVisibility() {
        this.userBlockVisible = !this.userBlockVisible;
    }

}
