﻿import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { window } from 'rxjs/operators';

export class RoboStudent {
    
    constructor(public id: number, public lastName: string, public firstName: string, public lessons: Array<number>) {
     
    }

    getRank(): number {
        let result = 0;
        for (var i = 0; i < this.lessons.length; i++) {
            result += this.lessons[i];
        }
        return result;
    }
}

@Injectable()
export class RoboService {

    name: string = 'Robo';
    className = 'Назва класу';
    students: Array<RoboStudent> = [
        new RoboStudent(1,'Воропай', 'Аня', [1, 1, 0]),
        new RoboStudent(2,'Денисюк', 'Іра', [1, 1, 0]),
        new RoboStudent(3,'Дідовець', 'Марія', [1, 1, 0]),
        new RoboStudent(4,'Закревський', 'Ілля', [1, 1, 0]), 
        new RoboStudent(5,'Мордвінов', 'Андрій', [1, 1, 0]), 
        new RoboStudent(6,'Павленко', 'Нікіта', [1, 1, 0]), 
        new RoboStudent(7,'Пашковська', 'Юля', [1, 1, 0]),
        new RoboStudent(8,'Петренко', 'Саша', [1, 1, 0]), 
        new RoboStudent(9,'Поліщук', 'Ілля', [1, 1, 0]), 
        new RoboStudent(10,'Ременков', 'Женя', [1, 1, 0]), 
        new RoboStudent(11,'Рог', 'Влад', [1, 1, 0]), 
        new RoboStudent(12,'Романов', 'Рома', [1, 1, 0]), 
        new RoboStudent(13,'Феофанова', 'Вероніка', [1, 1, 0]), 
        new RoboStudent(14,'Чуйко', 'Софія', [1, 1, 0])


    ];

    constructor() {
        var w = <any>global;
        var studData = w.localStorage.getItem('roboStudents');
        if (studData) {
            this.students = JSON.parse(studData);
        }
    }

    getStudents(): Array<RoboStudent> {
        return this.students;
    }

    getGroupName(): string {
        return 'Невідома група';
    }

    getCourseName(): string {
        return 'Невідомий курс';
    }

    like(index) {
        let lastLessonNum = this.students[index - 1].lessons.length - 1; 
        this.students[index - 1].lessons[lastLessonNum]++;
        var w = <any>global;
        w.localStorage.setItem('roboStudents', JSON.stringify(this.students));
    }

    

}
