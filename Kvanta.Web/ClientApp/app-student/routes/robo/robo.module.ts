import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';

import { RoboplusnikComponent } from './roboplusnik/roboplusnik.component';
import { RoboService } from './roboplusnik/robo.service';


const routes: Routes = [
    { path: '', component: RoboplusnikComponent  },
   
    
];

@NgModule({
    imports: [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    providers: [
        RoboService
    ],
    declarations: [
        RoboplusnikComponent,
        
    ],
    exports: [
        RouterModule 
    ]
})
export class RoboModule { }
