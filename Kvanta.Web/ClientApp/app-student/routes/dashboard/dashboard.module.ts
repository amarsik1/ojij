import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../../../shared/shared.module';

import { Dashboardv1Component } from './dashboard/dashboard.component';


const routes: Routes = [
    { path: '',  component: Dashboardv1Component  },
   
    
];

@NgModule({
    imports: [
        SharedModule,
        RouterModule.forChild(routes)
    ],
    declarations: [
        Dashboardv1Component,
        
    ],
    exports: [
        RouterModule 
    ]
})
export class DashboardModule { }
