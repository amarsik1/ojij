﻿export const menu = [
    {
        text: 'Студент',
        heading: true
    },
	{
		text: 'Classes',
		link: '/admin/classes',
		icon: 'icon-home'
	},
    {
        text: 'Dashboard',
		link: '/dashboard',
        icon: 'icon-speedometer'
    },
    {
        text: 'Robo',
        link: '/robo',
        icon: 'icon-speedometer'
    },
];
