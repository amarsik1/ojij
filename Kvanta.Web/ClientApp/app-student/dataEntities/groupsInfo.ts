﻿export interface IPerson {
	id: number;
	firstName: string;
}

export interface ITeacher extends IPerson {
	
}

export interface IEduGroup {
	comments: string;
	groupLevel: number;
	id: number;
	name: string;
	object_status: number;
	responsibleTeacher: number;
	startYear: number;
	teachers: ITeacher[];
	version_no: number;
}