import { Component, HostBinding, OnInit, ViewEncapsulation } from '@angular/core';
declare var $: any;

import { SettingsService } from '../core/settings/settings.service';
import { ApiClientService } from '../core/apiclient/apiclient.service';
import { IUserInfo } from '../model/userInfo';
import { UserblockService } from '../layout/sidebar/userblock/userblock.service';


@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit {
    

    @HostBinding('class.layout-fixed') get isFixed() { return this.settings.getLayoutSetting('isFixed'); };
    @HostBinding('class.aside-collapsed') get isCollapsed() { return this.settings.getLayoutSetting('isCollapsed'); };
    @HostBinding('class.layout-boxed') get isBoxed() { return this.settings.getLayoutSetting('isBoxed'); };
    @HostBinding('class.layout-fs') get useFullLayout() { return this.settings.getLayoutSetting('useFullLayout'); };
    @HostBinding('class.hidden-footer') get hiddenFooter() { return this.settings.getLayoutSetting('hiddenFooter'); };
    @HostBinding('class.layout-h') get horizontal() { return this.settings.getLayoutSetting('horizontal'); };
    @HostBinding('class.aside-float') get isFloat() { return this.settings.getLayoutSetting('isFloat'); };
    @HostBinding('class.offsidebar-open') get offsidebarOpen() { return this.settings.getLayoutSetting('offsidebarOpen'); };
    @HostBinding('class.aside-toggled') get asideToggled() { return this.settings.getLayoutSetting('asideToggled'); };
    @HostBinding('class.aside-collapsed-text') get isCollapsedText() { return this.settings.getLayoutSetting('isCollapsedText'); };

    constructor(public settings: SettingsService, public apiClient: ApiClientService, public userBlockService: UserblockService) {
        console.debug('Student app component loaded');
    }

    ngOnInit()
    {
        $(document).on('click', '[href="#"]', e => e.preventDefault());

        let w = <any>window;
        console.debug('----userblockService.userLoadedSource.subscribe');
        this.apiClient.getAccountInfo().subscribe(result => {
            console.debug('----userblockService.userLoadedSource.subscribe', result);

            if (!result.isOK) {
                window.location.href = '/#login';
                return;
            }
            var data = result.data;
            if (!data.isAuthenticated || !data.isAuthenticated || !data.roles.length) {
                window.location.href = '/#login';
                return;
            }
            this.userBlockService.setUser(data);
            if (w.appBootstrap) {
                w.appBootstrap();
            }
        });


        //this.apiClient.getAccountInfo().subscribe(res => {
        //    if (!res.isAuthenticated) {
        //        w.location.href = '/#/login';
        //    }
        //}, err => {

            
        //}, () => {
        //    if (w.appBootstrap) {
        //        w.appBootstrap();
        //    }
        //});
        //a.then(() => { (<any>window).appBootstrap && (<any>window).appBootstrap(); })
    }
}
