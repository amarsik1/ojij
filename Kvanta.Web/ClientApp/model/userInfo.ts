﻿export interface IUserInfo {
    userName: string;
    socNets: any[];
    roles: any[];
    userLogin: string;
    webAccountId: number;
    picture: string;
}