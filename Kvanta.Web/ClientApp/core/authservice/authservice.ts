﻿import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';


import { Observable } from 'rxjs/Observable';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        req = req.clone({
            setHeaders: {
                'Content-Type': 'application/json; charset=utf-8',
                'Accept': 'application/json',
                'Authorization': `Bearer ${this.getAuthToken()}`,
            },
        });

        return next.handle(req);
    }

    getLocalStorage() {
        return (typeof window !== "undefined") ? window.localStorage : null;
    }

    getAuthToken(): string {
        var authToken = '';
        var authDataStr = this.getLocalStorage().getItem('auth:data');
        var authData = JSON.parse(authDataStr);
        if (authData && authData["access_token"]) {
            authToken = authData["access_token"];
        }
        return authToken;
    }

}