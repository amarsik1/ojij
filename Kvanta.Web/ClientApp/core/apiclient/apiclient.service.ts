﻿import { Injectable } from '@angular/core';
import { HttpClientModule, HttpClient, HttpHeaders } from '@angular/common/http';
import { CookieService } from 'angular2-cookie/services/cookies.service';

import { Observable } from 'rxjs';
import { of as observableOf } from 'rxjs/observable/of'
import { catchError, map, tap } from 'rxjs/operators';
import { AuthInterceptor } from '../authservice/authservice';

@Injectable()
export class ApiClientService {

	readonly apiServer = ''; 



	constructor(public http: HttpClient, public cookie: CookieService) {

	}

	getLocalStorage() {
		return (typeof window !== "undefined") ? window.localStorage : null;
	}

    

    getHttpOptions(): any {
        //let authToken = this.getAuthToken();

		let res = {
			headers: new HttpHeaders({
				'Content-Type': 'application/json'

			})
		};
		//if (authToken) {
		//	res.headers.append('Authorization', 'Bearer ' + authToken);
  //      }
        return res;

	}

	setAuthCookie(data: any, remember): void {
		let accountType = data.accountType;
		window.localStorage.setItem('auth:data', JSON.stringify(data));
		if (!remember) {
			this.cookie.put('accountType', accountType);
		}
		else {
			var now = new Date();
			var exp = new Date(now.getFullYear(), now.getMonth() + 6, now.getDate());
			this.cookie.put('accountType', accountType, {
				expires: exp
			});
		}
	}

	login(user): Observable<any> {
		var rq = 'scope=' + (user.remember ? 'rememberMe' : '') + '&userName=' + user.email + '&password=' + encodeURIComponent(user.password) + '&grant_type=password&culture=';
		const url = this.apiServer + '/Token';
		return this.http.post(url, rq, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } })
			.pipe<any,any,any>(
			tap(data => { this.setAuthCookie(data, user.remember) }),
            map(data => data),
				catchError(this.handleError('login',null))
			) as Observable<any>;
	}

	private handleError<T>(operation = 'operation', result?: T) {
		return (error: any): Observable<T> => {

			// TODO: send the error to remote logging infrastructure

			console.info(error); // log to console instead

			// TODO: better job of transforming error for user consumption
			//this.log(`${operation} failed: ${error.message}`);

			// Let the app keep running by returning an empty result.
			return observableOf(result)
		};
	}

	register(/*url,*/ data) {
        return this.http.post(this.apiServer + '/bs/Account/Register', JSON.stringify(data), { headers: { 'Content-Type': 'application/json' } });
	}

	delete(url, http): Observable<any> {
		return this.http.delete(this.apiServer + url, this.getHttpOptions());
	}

	get(url): Observable<any> {
		return this.http.get<any>(this.apiServer + url, this.getHttpOptions());
    }

    getAccountInfo(): Observable<any> {
        return this.http.get<any>(this.apiServer + '/bs/Account/Info', this.getHttpOptions());
    }

	post(url: string, data: any): Observable<any> {

		return this.http.post(this.apiServer + url, data, this.getHttpOptions());
	}




}


