export const menu = [
    {
        text: 'Admin Panel',
        heading: true
    },
	{
		text: 'Classes',
		link: '/admin/classes',
		icon: 'icon-home'
	},
    {
        text: 'Dashboard',
		link: '/dashboard',
        icon: 'icon-speedometer'
    },
];
