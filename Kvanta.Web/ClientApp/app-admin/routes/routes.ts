import { Routes } from '@angular/router';
import { LayoutComponent } from '../../layout/layout.component';

import { LoginComponent } from '../../shared/pages/login/login.component';
import { RegisterComponent } from '../../shared/pages/register/register.component';
import { RecoverComponent } from '../../shared/pages/recover/recover.component';
import { LockComponent } from '../../shared/pages/lock/lock.component';
import { MaintenanceComponent } from '../../shared/pages/maintenance/maintenance.component';
import { Error404Component } from '../../shared/pages/error404/error404.component';
import { Error500Component } from '../../shared/pages/error500/error500.component';
import { ConfirmedComponent } from '../../shared/pages/confirmed/confirmed.component';

export const routes : Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            { path: 'home', loadChildren: './home/home.module#HomeModule' },
			{ path: 'admin', loadChildren: './admin/admin.module#AdminModule' },
			{ path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule' }
        ]
    },

    // Not lazy-loaded routes
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'recover', component: RecoverComponent },
    { path: 'lock', component: LockComponent },
    { path: 'maintenance', component: MaintenanceComponent },
	{ path: '404', component: Error404Component },
	{ path: '500', component: Error500Component },
	{ path: 'confirmed', component: ConfirmedComponent },

    // Not found
    { path: '**', redirectTo: 'admin/classes' }

];

export const Components =
{
        "LayoutComponent": LayoutComponent,
        "LoginComponent": LoginComponent,
        "RegisterComponent": RegisterComponent,
        "RecoverComponent": RecoverComponent,
        "LockComponent": LockComponent,
        "MaintenanceComponent": MaintenanceComponent,
        "Error404Component": Error404Component,
        "Error500Component": Error500Component
}
