﻿
export interface IEduGroups {
	gid: number;
	name: string;
	groupLevel: number;
	startYear: number;
	object_status: number;
	comments: string;
	version_no: number;
	responsibleTeacher: number;
	groupKind: number;
	personsGroup: number;
}

export interface ITeachers {
	person: number;
	group: number;
	comments: string;
}

export interface IEduStudents {
	object_status: number;
	person: number;
	group: number;
	comments: string;
}

export interface IPersons {
	gid: number;
	object_status: number;
	surname: string;
	name: string;
	middle_name: string;
	gender: number;
	full_Name: string;
	phones: string;
	e_mail: string;
	birthday: Date;
	version_no: number;
}

export interface IGroupJoiningRequests {
	joiningPersonID: string;
	joiningWebAccount: number;
	joiningPerson: number;
	personsGroup: number;
	reg_date: Date;
	approver: number;
	state: number;
	decisionTime: Date;
}

