import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ClassesComponent } from './classes/classes.component';
import { CommonModule } from '@angular/common';


const routes: Routes = [
	{ path: 'classes', component: ClassesComponent },
];

@NgModule({
	imports: [
		CommonModule,
        RouterModule.forChild(routes)
    ],
	declarations: [ClassesComponent],
    exports: [
        RouterModule
    ]
})
export class AdminModule { }
