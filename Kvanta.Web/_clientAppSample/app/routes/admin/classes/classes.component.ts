import { Component, OnInit } from '@angular/core';
import { ApiClientService } from '../../../core/apiclient/apiclient.service';
import { BehaviorSubject } from 'rxjs';
import { IEduGroup } from '../../../dataEntities/groupsInfo';
//import { asd } from '../../../core/apiclient/apiclient.service';

//export interface ITeacher { }

@Component({
	selector: 'app-classes',
	templateUrl: './classes.component.html',
	styleUrls: ['./classes.component.scss']
})
export class ClassesComponent implements OnInit {


	eduGroups: IEduGroup[] = [];

	constructor(public apiClientService: ApiClientService) { }

	//groupsName = this.eduGroups.name + ' test';

	ngOnInit() {
		this.apiClientService.get('/bs/Groups/GetAll').subscribe(data => {
			for (let asd of data) {
				this.eduGroups.push(asd);

			}
			console.log(this.eduGroups);
		});
  }

}