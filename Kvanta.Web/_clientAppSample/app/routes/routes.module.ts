import { NgModule, OnInit } from '@angular/core';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { RouterModule, Router, Routes } from '@angular/router';
import { HttpClientModule, HttpClient, HttpHeaders } from '@angular/common/http';

import { TranslatorService } from '../core/translator/translator.service';
import { MenuService } from '../core/menu/menu.service';
import { ApiClientService } from '../core/apiClient/apiclient.service';
import { SharedModule } from '../shared/shared.module';
import { PagesModule } from '../shared/pages/pages.module';




import { menu } from './menu';
import { routes } from './routes';
import { Components } from './routes';

@NgModule({
    imports: [
        SharedModule,
		RouterModule.forRoot(routes, { useHash: true }),
        PagesModule
    ],
    declarations: [],
    exports: [
        RouterModule
    ]
})

export class RoutesModule implements OnInit {

    apiClient: ApiClientService;

    constructor(public menuService: MenuService, tr: TranslatorService, public router: Router, public http: HttpClient, public cookie: CookieService) {
        menuService.addMenu(menu);
    }

    parseComponentsJson(json: Array<any>): Routes {
        for (var i = 0; i < json.length; i++) {
            var obj = json[i];
            if (!obj.component || !Components[obj.component]) {
                continue;
            }
            obj.component = Components[obj.component];
        }
        return json;
    }

    ngOnInit()
    {
        //this.apiClient = new ApiClientService(this.http, this.cookie);

        //this.apiClient.get('/api/app/definition').subscribe(def => {
        //    var routes = this.parseComponentsJson(def.routes);
        //    this.router.resetConfig(routes);
        //    this.menuService.addMenu(def.menu);
        //});
    }
}
