import { Component, OnInit } from '@angular/core';
import { SettingsService } from '../../../core/settings/settings.service';
import { ApiClientService } from '../../../core/apiclient/apiclient.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

	valForm: FormGroup;
	loginError: string;
	loading: boolean;
	loginErr: boolean;

	constructor(public settings: SettingsService, fb: FormBuilder, public apiclientservice: ApiClientService) {

		this.valForm = fb.group({
			'email': [null, Validators.compose([Validators.required, CustomValidators.email])],
            //'email': [null, Validators.compose([Validators.required])],
			'password': [null, Validators.required],
			'remember': [null]
        });

    }

    submitForm($ev, value: any) {
        $ev.preventDefault();
        for (let c in this.valForm.controls) {
            this.valForm.controls[c].markAsTouched();
        }
		if (this.valForm.valid) {
			this.loading = true;
			this.loginErr = false;
			this.apiclientservice.login(this.valForm).subscribe(result => {
				this.loading = false;
				if (result) {
					window.location.href = '/#/';
				}
				else {
					this.loginError = 'Wrong login/password';
					this.loginErr = true;
				}
			});



			//{
			//	
			//}
			//else {
			//	console.log('Error!');
			//}
			



			//console.log('Valid!');
            //console.log(value);
        }
    }

    ngOnInit() {

    }

}
