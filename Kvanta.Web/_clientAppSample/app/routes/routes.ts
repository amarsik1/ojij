import { Routes } from '@angular/router';
import { LayoutComponent } from '../layout/layout.component';

import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { RecoverComponent } from './pages/recover/recover.component';
import { LockComponent } from './pages/lock/lock.component';
import { MaintenanceComponent } from './pages/maintenance/maintenance.component';
import { Error404Component } from './pages/error404/error404.component';
import { Error500Component } from './pages/error500/error500.component';
import { ConfirmedComponent } from './pages/confirmed/confirmed.component';

export const routes : Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
			{ path: '', redirectTo: 'home', pathMatch: 'full' },
			{ path: 'home', loadChildren: './home/home.module#HomeModule' },
			{ path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule' },
            { path: 'widgets', loadChildren: './widgets/widgets.module#WidgetsModule' },
            { path: 'elements', loadChildren: './elements/elements.module#ElementsModule' },
            { path: 'forms', loadChildren: './forms/forms.module#FormsModule' },
            { path: 'charts', loadChildren: './charts/charts.module#ChartsModule' },
            { path: 'tables', loadChildren: './tables/tables.module#TablesModule' },
            { path: 'maps', loadChildren: './maps/maps.module#MapsModule' },
            { path: 'blog', loadChildren: './blog/blog.module#BlogModule' },
            { path: 'ecommerce', loadChildren: './ecommerce/ecommerce.module#EcommerceModule' },
			{ path: 'extras', loadChildren: './extras/extras.module#ExtrasModule' },
			{ path: 'admin', loadChildren: './admin/admin.module#AdminModule' }
        ]
    },

    // Not lazy-loaded routes
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'recover', component: RecoverComponent },
    { path: 'lock', component: LockComponent },
    { path: 'maintenance', component: MaintenanceComponent },
	{ path: '404', component: Error404Component },
	{ path: '500', component: Error500Component },
	{ path: 'confirmed', component: ConfirmedComponent },

    // Not found
    { path: '**', redirectTo: 'home' }

];

export const Components =
{
        "LayoutComponent": LayoutComponent,
        "LoginComponent": LoginComponent,
        "RegisterComponent": RegisterComponent,
        "RecoverComponent": RecoverComponent,
        "LockComponent": LockComponent,
        "MaintenanceComponent": MaintenanceComponent,
        "Error404Component": Error404Component,
        "Error500Component": Error500Component
}
