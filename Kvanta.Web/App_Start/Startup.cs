﻿using System;
using K7_Web.Providers;
using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Owin;
using Microsoft.Owin.Security.Facebook;
using Microsoft.Owin.Security.Google;
using Microsoft.Owin.Security.OAuth;
using Microsoft.Owin.Security.Cookies;

namespace K7_Web
{
    public partial class Startup
    {
        public static OAuthBearerAuthenticationOptions OAuthBearerOptions { get; private set; }
        //public static GoogleOAuth2AuthenticationOptions GoogleAuthOptions { get; private set; }
        //public static FacebookAuthenticationOptions facebookAuthOptions { get; private set; }

        public const string PublicClientId = "AtlasCRM.Web";
        public const string PublicAuthCookieName = "AtlasCRM.Web_UTN";

        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureAuth(IAppBuilder app)
        {
            // Enable the application to use a cookie to store information for the signed in user
            // and to use a cookie to temporarily store information about a user logging in with a third party login provider
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);
            OAuthBearerOptions = new OAuthBearerAuthenticationOptions();

            // Configure the application for OAuth based flow
            var OAuthOptions = new OAuthAuthorizationServerOptions
            {
                // In production mode set AllowInsecureHttp = false
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/Token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(30),
                Provider = new Providers.KvantaOAuthProvider(PublicClientId),
                AuthorizeEndpointPath = new PathString("/api/Account/ExternalLogin"),
                //RefreshTokenProvider = new SimpleRefreshTokenProvider()
            };

            // Enable the application to use bearer tokens to authenticate users
            app.UseOAuthAuthorizationServer(OAuthOptions);//app.UseOAuthBearerTokens(OAuthOptions);
            app.UseOAuthBearerAuthentication(OAuthBearerOptions);

            //app.UseCookieAuthentication(new CookieAuthenticationOptions());
            app.UseCookieAuthentication(new CookieAuthenticationOptions { CookieName = PublicAuthCookieName });
            //app.SetDefaultSignInAsAuthenticationType(CookieAuthenticationDefaults.AuthenticationType);

            // Uncomment the following lines to enable logging in with third party login providers
            //app.UseMicrosoftAccountAuthentication(
            //    clientId: "",
            //    clientSecret: "");

            //app.UseTwitterAuthentication(
            //    consumerKey: "",
            //    consumerSecret: "");
            //facebookAuthOptions = new FacebookAuthenticationOptions()
            //{
            //    AppId = "1500702356710246",
            //    AppSecret = "53fc3ff0cb3960c57388a35d566af9c4",
            //    Provider = new FacebookAuthProvider(),
            //    Scope = { "email", "public_profile", "user_birthday", "user_friends" }
            //};
            //app.UseFacebookAuthentication(facebookAuthOptions);

            //GoogleAuthOptions = new GoogleOAuth2AuthenticationOptions
            //{
            //    ClientId = "307323717805-3nctf17bc3a1r0194j0dn2c775btthnf.apps.googleusercontent.com",
            //    ClientSecret = "FYEk1SeUATfvBPKp8zn4xccV",
            //    Scope =
            //    {
            //        "https://www.googleapis.com/auth/contacts",
            //        "https://www.googleapis.com/auth/contacts.readonly",
            //        "https://www.googleapis.com/auth/plus.login",
            //        "https://www.googleapis.com/auth/userinfo.profile",
            //        "https://www.googleapis.com/auth/userinfo.email",
            //        "https://www.googleapis.com/auth/user.emails.read",
            //        "https://www.googleapis.com/auth/user.birthday.read",
            //        "https://www.googleapis.com/auth/user.addresses.read",
            //        "https://www.googleapis.com/auth/user.phonenumbers.read",
            //        "https://www.googleapis.com/auth/calendar"
            //    },
            //    Provider = new GoogleAuthProvider()
            //};
            //app.UseGoogleAuthentication(GoogleAuthOptions);

            //app.Use(typeof(ViberWebhookMiddleware));

            app.UseCors(CorsOptions.AllowAll);

            //var config = new HttpConfiguration();
            //WebApiConfig.Register(config);
            //app.UseWebApi(config);
        }
    }
}