﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace K7_Web
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
            config.Routes.MapHttpRoute(
                name: "OpendatabotSearchApi",
                routeTemplate: "odb/{action}",
                defaults: new { controller = "Opendatabot" }
            );
            //config.Routes.MapHttpRoute(
            //    name: "OpendatabotApi",
            //    routeTemplate: "odb/{method}/{parameter}",
            //    defaults: new { controller = "Opendatabot", action = "Command" }
            //);
            config.Routes.MapHttpRoute(
                name: "FileApi",
                routeTemplate: "file/{action}/{id}",
                defaults: new { controller = "Files" }
            );
            config.Routes.MapHttpRoute(
                name: "GraphApi",
                routeTemplate: "gapi/{module}/{command}",
                defaults: new { controller = "GApi", action = "DoCommand" }
            );
            config.Routes.MapHttpRoute(
                name: "GPollApi",
                routeTemplate: "GPoll/{pollname}",
                defaults: new { controller = "GPoll", action = "GetPoll" }
            );
            config.Routes.MapHttpRoute(
                name: "PollApi",
                routeTemplate: "Poll/{pollname}",
                defaults: new { controller = "GPoll", action = "GetPoll" }
            );

            config.Routes.MapHttpRoute(
                name: "PagesApi",
                routeTemplate: "{folder}/{pagename}",
                defaults: new { controller = "Pages", action = "GetPage" }
            );
            config.Routes.MapHttpRoute(
                name: "RootCommands",
                routeTemplate: "{command}",
                defaults: new { controller = "RootCommands", action = "DoCommand" }
            );
        }
    }
}
