﻿using ADE.Core.Model;
using ADE.EntityModel;
using appcore;
using appcore.Log;
using asb_cdm;
using Entity_Schema.Education;
using Entity_Schema.MailingInfo;
using GApi.Lib.Models;
using K7_Web.AtlasAPI;
using K7_Web.Controllers;
using K7_Web.Models;
using MD_Schema;
using System;
using System.Collections.Generic;
using System.Configuration;

namespace Kvanta_ut
{
    public class KvantaTestBase
    {
        private class TestAppContext: AtlasContextCore
        {
            public TestAppContext(string settingsString, IMessageManager messageManager, string projName, short projId)
            : base(settingsString, messageManager, projName, projId)
            {
                MailService = new KvantaMailingService(this);
            }
            public class KvantaMailingService : MailingService
            {

                public KvantaMailingService(AtlasContextCore pAppContext)
                    : base(pAppContext, pAppContext.RootDM.GetSPValue(_SP_Shema_.Base_Mail_Box).GetInt())
                {
                    loadMailBoxes();
                }
                private void loadMailBoxes()
                {
                    MailBox.Reset();
                    var lMailBoxesInfo = AppContext.RootEntityCache.GetEntity<MailBoxesSet>();
                    foreach (var b in lMailBoxesInfo.e_mail_boxes)
                    {
                        MailBox.Set(new MailBox(b.ID, b.e_mail, b.sender_name,
                            b.account_login, b.account_password, b.smtp_server, b.port, b.authentication, b.SSL,
                            MailBox.Kinds.Smtp));
                    }
                }

            }

        }
        public static AtlasContextCore ContextCore
        {
            get
            {
                return contextCore;
            }
        }

        private static AtlasContextCore contextCore;

        private static UserInfo adminSession;

        static KvantaTestBase()
        {
            var logger = new PlainTextLoger();
            K7P_sp.ModulesLoader.LoadModules();

            contextCore = new TestAppContext(ConfigurationManager.ConnectionStrings["Atlas.Cn"].ConnectionString, logger, AppDefinition.PRODUCT_NAME, AppDefinition.PRODUCT_ID);

            //contextCore = new AtlasContextCore(ConfigurationManager.ConnectionStrings["Atlas.Cn"].ConnectionString,  typeof(GroupsInfo), typeof(MaterialsBook), typeof(AppRoot));
            adminSession = contextCore.UserLoginAsync("admin", "admin", "127.0.0.1", "UnitTest").Result;
        }

        public IExecutionContext GetAdminContext()
        {
            return contextCore.GetUserSession(adminSession.Token);
        }

        private static Dictionary<string, UserInfo> userSessions = new Dictionary<string, UserInfo>();

        public IExecutionContext GetUserContext(string login, string password)
        {
            if (!userSessions.TryGetValue(login, out UserInfo ui))
            {
                ui = contextCore.UserLoginAsync(login, password, "127.0.0.1", "UnitTest").Result;
                userSessions.Add(login, ui);
            }
            return contextCore.GetUserSession(ui.Token);
        }

    }
}
