﻿using Entity_Schema.Education;
using K7P_sp.Service;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Entity_Schema.Education.LessonsInfo.S;
using static Entity_Schema.Education.MaterialsInfo.S;

namespace Kvanta_ut
{
    [TestFixture]
    public class AppRootServiceTest: KvantaTestBase
    {
        [TestCase("1")]
        public void TestMethod(string arg1)
        {

            AppRootService cabService = new AppRootService(GetUserContext("some7@person.com", "111"));

            var cabRequest = new DataRequest();

            var result = cabService.Cabinet(cabRequest);

            Assert.NotNull(result);
            Assert.NotNull(result.Groups);
            Assert.IsTrue(result.Groups.Count() > 0);
        }
        [TestCase("Json")]
        public void TestMethod2(string arg2)
        {
            var hv = new EduMaterials();

            hv.Caption = "Парність";
            hv.MaterialsText = "NK_Borys";
            
        }
    }
}
