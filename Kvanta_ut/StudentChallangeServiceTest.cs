﻿using K7P_sp.Service;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kvanta_ut
{
    [TestFixture]
    public class StudentChallangeServiceTest: KvantaTestBase
    {
        [TestCase(139, "admin", "admin")]
        public void TestGetChocoList(int groupId, string login, string password)
        {
            var userContext = GetUserContext(login, password);
            var service = new StudentChallageService(userContext);
            var tResult = service.GetChocoList(new Entity_Schema.Education.TaskListsRequest {GroupId = 139 });

            Assert.NotNull(tResult);
        }
    }
}
