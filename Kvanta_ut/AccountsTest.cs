﻿using Entity_Schema.RegInfo;
using K7P_sp.Service;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Entity_Schema.RegInfo.UserEventsSet.S;

namespace Kvanta_ut
{
    public class AccountsTest : KvantaTestBase
    {
        [TestCase("nmv2005@ukr.net", "Nk", "Mike", "811", "088-588-99-17")]
        public void TestRegisterAndConfirm(string pEmail, string pSurName, string pFirstName, string pPass, string pPhone)
        {
            var lRegInfo = new RegisterAccount()
            {
                Email = pEmail,
                Surname = pSurName,
                FirstName = pFirstName,
                password = pPass,
                PhoneNo = pPhone
            };
            var lAccSrv = new AccountService(GetAdminContext());
            var lInvitation = lAccSrv.Context.EntitiesCache.GetEntity<RegStart>()?.PublicInvitations?.FirstOrDefault();
            if (lInvitation != null)
                lRegInfo.InviteCode = lInvitation.ID.ToString();

            var lAcc = lAccSrv.Register(lRegInfo);
            Assert.NotNull(lAcc);
            Assert.NotNull(lAcc.Data);
            Assert.IsTrue(lAcc.Data.e_mail_address == pEmail);

            /*var lConfirmation = new ConfirmEmailResult()
            {
                EmailAddress = lAcc.Data.e_mail_address,
                RequestCode = lAcc.Data.RequestCode
            };
            var lConfirmResult = lAccSrv.ConfirmEmail(lConfirmation);
            Assert.IsTrue(lAcc.Data.gid == lConfirmResult.Data.WebAccountID);*/
        }

        [TestCase("some7@person.com", "111")]
        public void TestLogin(string login, string pwd)
        {
            var usrContext = GetUserContext(login, pwd);

            Assert.IsTrue(usrContext.User.WebAccountId > 0);
        }

        [TestCase("some7@person.com", "111")]
        public void TestGetAccountInfo(string login, string pwd)
        {
            var usrContext = GetUserContext(login, pwd);
            var accService = new AccountService(usrContext);
            var result = accService.Info();
            Assert.IsTrue(result.IsOK);
        }
    }
}
